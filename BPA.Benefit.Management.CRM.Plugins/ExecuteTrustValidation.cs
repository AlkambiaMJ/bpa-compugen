﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BPA.Benefit.Management.CRM.Plugins.Helper;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;

namespace BPA.Benefit.Management.CRM.Plugins
{
    public class ExecuteTrustValidation : IPlugin
    {
        public void Execute(IServiceProvider serviceProvider)
        {
            IPluginExecutionContext context = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));
            IOrganizationServiceFactory serviceFactory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));

            // Create service with context of current user
            IOrganizationService service = serviceFactory.CreateOrganizationService(context.UserId);

            //create tracing service
            ITracingService tracingService = (ITracingService)serviceProvider.GetService(typeof(ITracingService));

            //To get access to the image of the Period of Service record
            EntityReference trust = context.InputParameters["Target"] as EntityReference;

            Entity entity = TrustHelper.FechAllInformation(service, tracingService, trust.Id);
            // Create Trsut Validation Record
            int trustConfigurtion = 922070000;
            Guid trustValidationId = TrustHelper.CreatTrustValidation(service, tracingService, trust, trustConfigurtion);

            // Get All Plans related to Trust
            EntityCollection planCollection = PlanHelper.FetchAllPlanByTrustId(service, tracingService, trust.Id);
            EntityCollection fundCollection = FundHelper.FetchAllPlanByTrustId(service, tracingService, trust.Id);

            string error = string.Empty, result = string.Empty;
            StringBuilder sbTrace = new StringBuilder();
            try
            {
                sbTrace.AppendLine("Trsut Level Start");
                // Trust Related Entity Checking
                CheckingTrustLevel(service, tracingService, entity, trustValidationId, planCollection, fundCollection);
                sbTrace.AppendLine("Trsut Level Completed");
                // Plan Related Checking
                CheckingPlanLevel(service, tracingService, entity, trustValidationId, planCollection);
                sbTrace.AppendLine("Plan Level Completed");
            }
            catch(Exception ex)
            {
                error = $@"{ex.Message} \n\n {ex.StackTrace} \n\n {sbTrace.ToString()}";
            }
            #region ---------- UPDATE Trust Validation -------------
            // Updat Trust Validation (Parent) record if one Trust Detail Record is "BAD"
            if (!string.IsNullOrEmpty(error))
            {
                result = "ERROR";
            }
            else
            {
                EntityCollection detailCollection = TrustHelper.FetchAllTrustValidationDetail(service, tracingService, trustValidationId, "BAD");
                if (detailCollection != null && detailCollection.Entities.Count > 0)
                    result = "BAD";
                else
                    result = "GOOD";
            }
            TrustHelper.UpdateTrustValidation(service, tracingService, trustValidationId, result);

            #endregion
        }


        void CheckingTrustLevel(IOrganizationService service, ITracingService tracingService, Entity trust, Guid trustValidationId,
            EntityCollection planCollection, EntityCollection fundCollection)
        {
            EntityReference trustOwner = trust.Contains("ownerid") ? trust.GetAttributeValue<EntityReference>("ownerid") : null;
            string trustName = trust.Contains("bpa_name") ? trust.GetAttributeValue<string>("bpa_name") : string.Empty;

            // Checking Trust Owner
            #region --------- Checking Trust Owner ---------

            if (trustOwner.LogicalName == "team") { }
            //TrustHelper.CreatTrustValidationDetail(service, tracingService, trustValidationId, "Trust", trustName, string.Empty, string.Empty, "Trust Owner is Team", "GOOD");
            else
                TrustHelper.CreatTrustValidationDetail(service, tracingService, trustValidationId, "Trust", trustName, string.Empty, string.Empty, "Trust Owner is not Team", "BAD");

            #endregion

            // Checking Plan and Plan Owner
            #region ---------- Plan ANd Plan Owner -----------------
            if (planCollection != null && planCollection.Entities.Count > 0)
            {
                //TrustHelper.CreatTrustValidationDetail(service, tracingService, trustValidationId, "Trust", trustName,
                //    "Plan", string.Empty, "At least one is defined", "GOOD");

                foreach (Entity plan in planCollection.Entities)
                {
                    string planName = plan.GetAttributeValue<string>("bpa_name");
                    EntityReference planOwner = plan.GetAttributeValue<EntityReference>("ownerid");

                    if (planOwner.Id == trustOwner.Id)
                    {
                        //TrustHelper.CreatTrustValidationDetail(service, tracingService, trustValidationId, "Trust", trustName,
                        //    "Plan", planName, "Owner is the same as Trust", "GOOD");
                    }
                    else
                    {
                        TrustHelper.CreatTrustValidationDetail(service, tracingService, trustValidationId, "Trust", trustName,
                            "Plan", planName, "Owner is the same as Trust", "BAD");
                    }
                }
            }
            else
            {
                TrustHelper.CreatTrustValidationDetail(service, tracingService, trustValidationId, "Trust", trustName,
                    "Plan", string.Empty, "At least one is defined", "BAD");
            }
            #endregion

            // Checking Fund and Fund Owner
            #region ---------- Fund and Fund Owner -----------------
            Dictionary<string, int> dictionaryFundType = new Dictionary<string, int>();
            Dictionary<string, Guid> dictionaryIssuePayment = new Dictionary<string, Guid>();

            if (fundCollection != null && fundCollection.Entities.Count > 0)
            {
                //TrustHelper.CreatTrustValidationDetail(service, tracingService, trustValidationId, "Trust", trustName,
                //    "Fund", string.Empty, "At least one is defined", "GOOD");
                
                
                foreach (Entity fund in fundCollection.Entities)
                {
                    string fundName = fund.GetAttributeValue<string>("bpa_name");
                    EntityReference fundOwner = fund.GetAttributeValue<EntityReference>("ownerid");
                    OptionSetValue fundType = fund.GetAttributeValue<OptionSetValue>("bpa_fundtype");
                    string fundTypeName = fund.FormattedValues["bpa_fundtype"].ToString();
                    bool issuePayment = fund.GetAttributeValue<bool>("bpa_issuepayment");

                    if (!dictionaryFundType.ContainsKey(fundTypeName))
                        dictionaryFundType.Add(fundTypeName, fundType.Value);

                    if (!dictionaryFundType.ContainsKey(fundName) && issuePayment)
                        dictionaryIssuePayment.Add(fundName, fund.Id);

                    if (fundOwner.Id == trustOwner.Id)
                    {
                        //TrustHelper.CreatTrustValidationDetail(service, tracingService, trustValidationId, "Trust", trustName,
                        //    "Fund", fundName, "Owner is the same as Trust", "GOOD");
                    }
                    else
                    {
                        TrustHelper.CreatTrustValidationDetail(service, tracingService, trustValidationId, "Trust", trustName,
                            "Fund", fundName, "Owner is the same as Trust", "BAD");
                    }
                }
            }
            else
            {
                TrustHelper.CreatTrustValidationDetail(service, tracingService, trustValidationId, "Trust", trustName,
                    "Fund", string.Empty, "At least one is defined", "BAD");
            }


            // Checking Default Fund with each Fund Type
            foreach (KeyValuePair<string, int> item in dictionaryFundType)
            {
                List<Entity> defaultFund = fundCollection.Entities.Where(e => e.GetAttributeValue<bool>("bpa_defaultfund") == true &&
                                                                    e.GetAttributeValue<OptionSetValue>("bpa_fundtype").Value == item.Value).ToList();

                if (defaultFund.Count == 0)
                {
                    TrustHelper.CreatTrustValidationDetail(service, tracingService, trustValidationId, "Trust", trustName,
                        "Fund", $@"Fund Type: {item.Key}", "at least and only one fund is default", "BAD");
                }
                else if (defaultFund.Count == 1)
                {
                    //TrustHelper.CreatTrustValidationDetail(service, tracingService, trustValidationId, "Trust", trustName,
                    //    "Fund", $@"Fund Type: {item.Key}", "at least and only one fund is default", "GOOD");
                }
                else
                {
                    TrustHelper.CreatTrustValidationDetail(service, tracingService, trustValidationId, "Trust", trustName,
                        "Fund", $@"Fund Type: {item.Key}", "more than one default fund ", "BAD");
                }

            }

            // Fund Payment Type
            if (dictionaryIssuePayment.Count > 0)
            {
                foreach (KeyValuePair<string, Guid> item in dictionaryIssuePayment)
                {
                    // Get all payment type by fund id
                    EntityCollection paymentTypeCollection = FundHelper.FetchAllPaymentTypesByFundId(service, tracingService, item.Value);
                    if (paymentTypeCollection != null && paymentTypeCollection.Entities.Count > 0)
                    {

                        //TrustHelper.CreatTrustValidationDetail(service, tracingService, trustValidationId, "Trust", trustName,
                        //    "Fund Payment Type", item.Key, "At least one is defined", "GOOD");
                        foreach (Entity fundPaymentType in paymentTypeCollection.Entities)
                        {
                            string fundPaymentTypeName = fundPaymentType.GetAttributeValue<string>("bpa_name");
                            EntityReference fundPaymentTypeOwner = fundPaymentType.GetAttributeValue<EntityReference>("ownerid");

                            if (fundPaymentTypeOwner.Id == trustOwner.Id)
                            {
                                //TrustHelper.CreatTrustValidationDetail(service, tracingService, trustValidationId, "Trust", trustName,
                                //    "Fund Payment Type", fundPaymentTypeName, "Owner is the same as Trust", "GOOD");
                            }
                            else
                            {
                                TrustHelper.CreatTrustValidationDetail(service, tracingService, trustValidationId, "Trust", trustName,
                                    "Fund Payment Type", fundPaymentTypeName, "Owner is the same as Trust", "BAD");
                            }
                        }
                    }
                    else
                    {
                        TrustHelper.CreatTrustValidationDetail(service, tracingService, trustValidationId, "Trust", trustName,
                            "Fund Payment Type", item.Key, "At least one is defined", "BAD");
                    }
                }
            }
            #endregion

            // Checking Trust and Benefit
            #region -------- TRUST BENEFIT ---------

            EntityCollection benefitCollection = TrustHelper.FetchAllBenefitByTrustId(service, tracingService, trust.Id);
            if(benefitCollection != null && benefitCollection.Entities.Count > 0)
            {
                foreach (Entity benefit in benefitCollection.Entities)
                {
                    string benefitName = benefit.GetAttributeValue<string>("bpa_name");
                    EntityReference benefitOwner = benefit.GetAttributeValue<EntityReference>("ownerid");

                    if (benefitOwner.Id == trustOwner.Id)
                    {
                        //TrustHelper.CreatTrustValidationDetail(service, tracingService, trustValidationId, "Trust", trustName,
                        //    "Benefit", benefitName, "Owner is the same as Trust", "GOOD");
                    }
                    else
                    {
                        TrustHelper.CreatTrustValidationDetail(service, tracingService, trustValidationId, "Trust", trustName,
                            "Benefit", benefitName, "Owner is the same as Trust", "BAD");
                    }
                }
            }
            #endregion
        }

        void CheckingPlanLevel(IOrganizationService service, ITracingService tracingService, Entity trust, Guid trustValidationId,
            EntityCollection planCollection)
        {
            if (planCollection != null && planCollection.Entities.Count == 0) return;
            EntityReference trustOwner = trust.Contains("ownerid") ? trust.GetAttributeValue<EntityReference>("ownerid") : null;

            foreach (Entity plan in planCollection.Entities)
            {
                // Validating plan is Benefit Plan Or Pension Plan
                string planName = plan.GetAttributeValue<string>("bpa_name");
                EntityReference planOwner = plan.GetAttributeValue<EntityReference>("ownerid");
                bool benefitPlan = plan.GetAttributeValue<bool>("bpa_benefitplan");
                bool pensionPlan = plan.GetAttributeValue<bool>("bpa_pensionplan");

                if (!benefitPlan && !pensionPlan)
                {
                    TrustHelper.CreatTrustValidationDetail(service, tracingService, trustValidationId, "Plan", planName,
                        string.Empty, string.Empty, "At least Benefit or Pension Plan Select", "BAD");
                }

                // Validate Agreement Group
                #region ---------- Agreement Group ---------
                EntityCollection agroupCollection = AgreementHelper.FetchAllAgreementGroupByPlanId(service, tracingService, plan.Id);
                if (agroupCollection != null && agroupCollection.Entities.Count > 0)
                {
                    foreach (Entity agreementGroup in agroupCollection.Entities)
                    {
                        string agName = agreementGroup.GetAttributeValue<string>("bpa_name");
                        EntityReference agOwner = agreementGroup.GetAttributeValue<EntityReference>("ownerid");

                        if (agOwner.Id == trustOwner.Id)
                        {
                            //TrustHelper.CreatTrustValidationDetail(service, tracingService, trustValidationId, "Plan", planName,
                            //    "Agreement Group", agName, "Owner is the same as Trust", "GOOD");
                        }
                        else
                        {
                            TrustHelper.CreatTrustValidationDetail(service, tracingService, trustValidationId, "Plan", planName,
                                "Agreement Group", agName, "Owner is the same as Trust", "BAD");
                        }
                    }
                }
                else
                {
                    TrustHelper.CreatTrustValidationDetail(service, tracingService, trustValidationId, "Plan", planName,
                        "Agreement Group", string.Empty, "At least one is defined", "BAD");
                }
                #endregion

                // Validate Agreement 
                #region -------- Agreement -------------
                EntityCollection agreementCollection = AgreementHelper.FetchAllAgreementsByPlanId(service, tracingService, plan.Id);
                if (agreementCollection != null && agreementCollection.Entities.Count > 0)
                {
                    foreach (Entity agreement in agreementCollection.Entities)
                    {
                        string agName = agreement.GetAttributeValue<string>("bpa_name");
                        EntityReference agOwner = agreement.GetAttributeValue<EntityReference>("ownerid");

                        if (agOwner.Id == trustOwner.Id)
                        {
                            //TrustHelper.CreatTrustValidationDetail(service, tracingService, trustValidationId, "Plan", planName,
                            //    "Agreement", agName, "Owner is the same as Trust", "GOOD");
                        }
                        else
                        {
                            TrustHelper.CreatTrustValidationDetail(service, tracingService, trustValidationId, "Plan", planName,
                                "Agreement", agName, "Owner is the same as Trust", "BAD");
                        }
                    }
                }
                else
                {
                    TrustHelper.CreatTrustValidationDetail(service, tracingService, trustValidationId, "Plan", planName,
                        "Agreement", string.Empty, "At least one is defined", "BAD");
                }
                #endregion
                
                
                
                #region ------------ BENEFIT PLAN ----------------
                
                // Checking Plan is Benefit Plan then each Eligibility Category must has Detail Record
                EntityCollection eligibilityCategoryDetailCollection = null;
                if (benefitPlan)
                {
                    // Checking for Plan is Benefit Plan
                    #region ------------ Eligibility Category ---------------
                    EntityCollection eligibilityCategoryCollection = EligibilityCategoryHelper.FetchAllEligibilityCategoryByPlanId(service, tracingService, plan.Id);
                    if (eligibilityCategoryCollection != null && eligibilityCategoryCollection.Entities.Count > 0)
                    {
                        foreach (Entity eligibilityCategory in eligibilityCategoryCollection.Entities)
                        {
                            string ecName = eligibilityCategory.GetAttributeValue<string>("bpa_name");
                            EntityReference ecOwner = eligibilityCategory.GetAttributeValue<EntityReference>("ownerid");

                            if (ecOwner.Id == trustOwner.Id)
                            {
                                //TrustHelper.CreatTrustValidationDetail(service, tracingService, trustValidationId, "Plan", planName,
                                //    "Eligibility Category", ecName, "Owner is the same as Trust", "GOOD");
                            }
                            else
                            {
                                TrustHelper.CreatTrustValidationDetail(service, tracingService, trustValidationId, "Plan", planName,
                                    "Eligibility Category", ecName, "Owner is the same as Trust", "BAD");
                            }
                        }
                    }
                    else
                    {
                        TrustHelper.CreatTrustValidationDetail(service, tracingService, trustValidationId, "Plan", planName,
                            "Eligibility Category", string.Empty, "At least one is defined", "BAD");
                    }
                    #endregion

                    #region ------- Eligibility Category Detail --------

                    if (eligibilityCategoryCollection != null && eligibilityCategoryCollection.Entities.Count > 0)
                    {
                        foreach (Entity eligibilityCategory in eligibilityCategoryCollection.Entities)
                        {
                            string ecName = eligibilityCategory.GetAttributeValue<string>("bpa_name");
                            EntityReference ecOwner = eligibilityCategory.GetAttributeValue<EntityReference>("ownerid");

                            // Get all eligibility category detail
                            eligibilityCategoryDetailCollection = EligibilityCategoryHelper.FetchAllEligibilityDetail(service, tracingService, eligibilityCategory.Id);
                            if (eligibilityCategoryDetailCollection != null && eligibilityCategoryDetailCollection.Entities.Count > 0)
                            {
                                foreach (Entity eligibilityCategoryDetail in eligibilityCategoryDetailCollection.Entities)
                                {
                                    string ecdName = eligibilityCategory.GetAttributeValue<string>("bpa_name");
                                    EntityReference ecdOwner = eligibilityCategory.GetAttributeValue<EntityReference>("ownerid");

                                    if (ecdOwner.Id == trustOwner.Id)
                                    {
                                        //TrustHelper.CreatTrustValidationDetail(service, tracingService, trustValidationId, "Eligibility Category", ecName,
                                        //    "Eligibility Category Detail", ecdName, "Owner is the same as Trust", "GOOD");
                                    }
                                    else
                                    {
                                        TrustHelper.CreatTrustValidationDetail(service, tracingService, trustValidationId, "Eligibility Category", ecName,
                                            "Eligibility Category Detail", ecdName, "Owner is the same as Trust", "BAD");
                                    }
                                }
                            }
                            else
                            {
                                TrustHelper.CreatTrustValidationDetail(service, tracingService, trustValidationId, "Eligibility Category",
                                    ecName, "Eligibility Category Detail", string.Empty, "At least one is defined", "BAD");
                            }
                        }
                    }
                    #endregion

                    #region -------- Benefit Eligibility Category Detail --------------

                    if (eligibilityCategoryDetailCollection != null && eligibilityCategoryDetailCollection.Entities.Count > 0)
                    {
                        foreach (Entity eligibilityCategoryDetail in eligibilityCategoryDetailCollection.Entities)
                        {
                            string ecdName = eligibilityCategoryDetail.GetAttributeValue<string>("bpa_name");
                            EntityReference ecdOwner = eligibilityCategoryDetail.GetAttributeValue<EntityReference>("ownerid");

                            // Fetch All Benefit related to this Eligibility Category Detail
                            EntityCollection benefitCollection = EligibilityCategoryHelper.FetchAllBenefitByEligibilityDetailId(service, tracingService, eligibilityCategoryDetail.Id);
                            if (benefitCollection != null && benefitCollection.Entities.Count > 0)
                            {
                                foreach (Entity benefit in benefitCollection.Entities)
                                {
                                    string benefitName = benefit.GetAttributeValue<string>("bpa_name");
                                    EntityReference benefitOwner = benefit.GetAttributeValue<EntityReference>("ownerid");

                                    if (benefitOwner.Id == trustOwner.Id)
                                    {
                                        //TrustHelper.CreatTrustValidationDetail(service, tracingService, trustValidationId, "Eligibility Category Detail", ecdName,
                                        //    "Benefit Eligibility Category Detail", benefitName, "Owner is the same as Trust", "GOOD");
                                    }
                                    else
                                    {
                                        TrustHelper.CreatTrustValidationDetail(service, tracingService, trustValidationId, "Eligibility Category Detail", ecdName,
                                            "Benefit Eligibility Category Detail", benefitName, "Owner is the same as Trust", "BAD");
                                    }
                                }
                            }

                        }
                    }

                    #endregion
                }

                #endregion

                #region ------------ PENSION PLAN ------------

                if (pensionPlan)
                {
                    List<DateRange> lstDateRange = new List<DateRange>();

                    // Check DB or DC Define
                    int pensionPlanType = plan.Contains("bpa_pensionplantype") ? plan.GetAttributeValue<OptionSetValue>("bpa_pensionplantype").Value : 0;
                    if(pensionPlanType == 0)
                    {
                        TrustHelper.CreatTrustValidationDetail(service, tracingService, trustValidationId, "Plan", planName,
                            string.Empty, string.Empty, "Pension Plan Type Select", "BAD");
                    }

                    #region ----------  Pension Vesing Rules ----------
                    EntityCollection vestingCollection = PlanHelper.FetchAllPlanVestingRulesByPlanId(service, tracingService, plan.Id);
                    if(vestingCollection != null && vestingCollection.Entities.Count > 0)
                    {
                        foreach(Entity vestingrule in vestingCollection.Entities)
                        {
                            string name = vestingrule.GetAttributeValue<string>("bpa_name");
                            EntityReference ownerid = vestingrule.GetAttributeValue<EntityReference>("ownerid");
                            DateTime startDate = vestingrule.GetAttributeValue<DateTime>("bpa_startdate");
                            DateTime endDate = vestingrule.GetAttributeValue<DateTime>("bpa_enddate");
                            lstDateRange.Add(new DateRange(startDate, endDate));
                            
                            if (ownerid.Id == trustOwner.Id)
                            {
                                //TrustHelper.CreatTrustValidationDetail(service, tracingService, trustValidationId, "Plan", planName,
                                //    "Pension Vesting Rule", name, "Owner is the same as Trust", "GOOD");
                            }
                            else
                            {
                                TrustHelper.CreatTrustValidationDetail(service, tracingService, trustValidationId, "Plan", planName,
                                    "Pension Vesting Rule", name, "Owner is the same as Trust", "BAD");
                            }
                        }
                    }
                    else
                    {
                        TrustHelper.CreatTrustValidationDetail(service, tracingService, trustValidationId, "Plan", planName,
                            "Pension Vesting Rule", string.Empty, "At least one is defined", "BAD");
                    }
                    // Check No gap between 2 date range
                    if (DateRange.CheckGap(lstDateRange))
                    {
                        TrustHelper.CreatTrustValidationDetail(service, tracingService, trustValidationId, "Plan", planName,
                            "Pension Vesting Rule", string.Empty, "Gap found in Date Range", "BAD");
                    }
                    else
                    {
                        //TrustHelper.CreatTrustValidationDetail(service, tracingService, trustValidationId, "Plan", planName,
                        //    "Pension Vesting Rule", string.Empty, "No Gap found in Date Range", "GOOD");
                    }
                    #endregion

                    #region ----------  Pension Eligibility Rules ----------
                    lstDateRange = new List<DateRange>();
                    EntityCollection eligibilityCollection = PlanHelper.FetchAllPlanEligibilityRulesByPlanId(service, tracingService, plan.Id);
                    if (eligibilityCollection != null && eligibilityCollection.Entities.Count > 0)
                    {
                        foreach (Entity elibiglityRule in eligibilityCollection.Entities)
                        {
                            string name = elibiglityRule.GetAttributeValue<string>("bpa_name");
                            EntityReference ownerid = elibiglityRule.GetAttributeValue<EntityReference>("ownerid");
                            DateTime startDate = elibiglityRule.GetAttributeValue<DateTime>("bpa_startdate");
                            DateTime endDate = elibiglityRule.GetAttributeValue<DateTime>("bpa_enddate");
                            lstDateRange.Add(new DateRange(startDate, endDate));

                            if (ownerid.Id == trustOwner.Id)
                            {
                                //TrustHelper.CreatTrustValidationDetail(service, tracingService, trustValidationId, "Plan", planName,
                                //    "Pension Eligibility Rule", name, "Owner is the same as Trust", "GOOD");
                            }
                            else
                            {
                                TrustHelper.CreatTrustValidationDetail(service, tracingService, trustValidationId, "Plan", planName,
                                    "Pension Eligibility Rule", name, "Owner is the same as Trust", "BAD");
                            }
                        }
                    }
                    else
                    {
                        TrustHelper.CreatTrustValidationDetail(service, tracingService, trustValidationId, "Plan", planName,
                            "Pension Eligibility Rule", string.Empty, "At least one is defined", "BAD");
                    }
                    // Check No gap between 2 date range
                    if (DateRange.CheckGap(lstDateRange))
                    {
                        TrustHelper.CreatTrustValidationDetail(service, tracingService, trustValidationId, "Plan", planName,
                            "Pension Eligibility Rule", string.Empty, "Gap found in Date Range", "BAD");
                    }
                    else
                    {
                        //TrustHelper.CreatTrustValidationDetail(service, tracingService, trustValidationId, "Plan", planName,
                        //    "Pension Eligibility Rule", string.Empty, "No Gap found in Date Range", "GOOD");
                    }
                    #endregion

                    #region ----------  Pension Solvency Rates ----------
                    lstDateRange = new List<DateRange>();
                    EntityCollection solvencyCollection = PlanHelper.FetchAllPlanSolvencyRatesByPlanId(service, tracingService, plan.Id);
                    if (solvencyCollection != null && solvencyCollection.Entities.Count > 0)
                    {
                        foreach (Entity solvencyRate in solvencyCollection.Entities)
                        {
                            string name = solvencyRate.GetAttributeValue<string>("bpa_name");
                            EntityReference ownerid = solvencyRate.GetAttributeValue<EntityReference>("ownerid");
                            DateTime startDate = solvencyRate.GetAttributeValue<DateTime>("bpa_startdate");
                            DateTime endDate = solvencyRate.GetAttributeValue<DateTime>("bpa_enddate");
                            lstDateRange.Add(new DateRange(startDate, endDate));

                            if (ownerid.Id == trustOwner.Id)
                            {
                                //TrustHelper.CreatTrustValidationDetail(service, tracingService, trustValidationId, "Plan", planName,
                                //    "Pension Solvency Rate", name, "Owner is the same as Trust", "GOOD");
                            }
                            else
                            {
                                TrustHelper.CreatTrustValidationDetail(service, tracingService, trustValidationId, "Plan", planName,
                                    "Pension Solvency Rate", name, "Owner is the same as Trust", "BAD");
                            }
                        }
                    }
                    else
                    {
                        TrustHelper.CreatTrustValidationDetail(service, tracingService, trustValidationId, "Plan", planName,
                            "Pension Solvency Rate", string.Empty, "At least one is defined", "BAD");
                    }

                    // Check No gap between 2 date range
                    if (DateRange.CheckGap(lstDateRange))
                    {
                        TrustHelper.CreatTrustValidationDetail(service, tracingService, trustValidationId, "Plan", planName,
                            "Pension Solvency Rate", string.Empty, "Gap found in Date Range", "BAD");
                    }
                    else
                    {
                        //TrustHelper.CreatTrustValidationDetail(service, tracingService, trustValidationId, "Plan", planName,
                        //    "Pension Solvency Rate", string.Empty, "No Gap found in Date Range", "GOOD");
                    }
                    #endregion

                    #region ----------  Pension Benefit Rates ----------
                    lstDateRange = new List<DateRange>();
                    EntityCollection benefitRateCollection = PlanHelper.FetchAllPlanBenefitRatesByPlanId(service, tracingService, plan.Id);
                    if (benefitRateCollection != null && benefitRateCollection.Entities.Count > 0)
                    {
                        foreach (Entity benefitRate in benefitRateCollection.Entities)
                        {
                            string name = benefitRate.GetAttributeValue<string>("bpa_name");
                            EntityReference ownerid = benefitRate.GetAttributeValue<EntityReference>("ownerid");
                            DateTime startDate = benefitRate.GetAttributeValue<DateTime>("bpa_effectivedate");
                            DateTime endDate = benefitRate.GetAttributeValue<DateTime>("bpa_enddate");
                            lstDateRange.Add(new DateRange(startDate, endDate));

                            if (ownerid.Id == trustOwner.Id)
                            {
                                //TrustHelper.CreatTrustValidationDetail(service, tracingService, trustValidationId, "Plan", planName,
                                //    "Pension Benefit Rate", name, "Owner is the same as Trust", "GOOD");
                            }
                            else
                            {
                                TrustHelper.CreatTrustValidationDetail(service, tracingService, trustValidationId, "Plan", planName,
                                    "Pension Benefit Rate", name, "Owner is the same as Trust", "BAD");
                            }
                        }
                    }
                    else
                    {
                        TrustHelper.CreatTrustValidationDetail(service, tracingService, trustValidationId, "Plan", planName,
                            "Pension Benefit Rate", string.Empty, "At least one is defined", "BAD");
                    }

                    // Check No gap between 2 date range
                    if (DateRange.CheckGap(lstDateRange))
                    {
                        TrustHelper.CreatTrustValidationDetail(service, tracingService, trustValidationId, "Plan", planName,
                            "Pension Benefit Rate", string.Empty, "Gap found in Date Range", "BAD");
                    }
                    else
                    {
                        //TrustHelper.CreatTrustValidationDetail(service, tracingService, trustValidationId, "Plan", planName,
                        //    "Pension Benefit Rate", string.Empty, "No Gap found in Date Range", "GOOD");
                    }
                    #endregion

                    #region ----------  Pension Statement Detail ----------
                    lstDateRange = new List<DateRange>();
                    EntityCollection sdCollection = PlanHelper.FetchAllPlanStatementDetailByPlanId(service, tracingService, plan.Id);
                    if (sdCollection != null && sdCollection.Entities.Count > 0)
                    {
                        foreach (Entity statementDetail in sdCollection.Entities)
                        {
                            string name = statementDetail.GetAttributeValue<string>("bpa_name");
                            EntityReference ownerid = statementDetail.GetAttributeValue<EntityReference>("ownerid");
                            DateTime startDate = statementDetail.GetAttributeValue<DateTime>("bpa_startdate");
                            DateTime endDate = statementDetail.GetAttributeValue<DateTime>("bpa_enddate");
                            lstDateRange.Add(new DateRange(startDate, endDate));

                            if (ownerid.Id == trustOwner.Id)
                            {
                                //TrustHelper.CreatTrustValidationDetail(service, tracingService, trustValidationId, "Plan", planName,
                                //    "Plan Statement Detail", name, "Owner is the same as Trust", "GOOD");
                            }
                            else
                            {
                                TrustHelper.CreatTrustValidationDetail(service, tracingService, trustValidationId, "Plan", planName,
                                    "Plan Statement Detail", name, "Owner is the same as Trust", "BAD");
                            }
                        }
                    }
                    else
                    {
                        TrustHelper.CreatTrustValidationDetail(service, tracingService, trustValidationId, "Plan", planName,
                            "Plan Statement Detail", string.Empty, "At least one is defined", "BAD");
                    }

                    // Check No gap between 2 date range
                    if (DateRange.CheckGap(lstDateRange))
                    {
                        TrustHelper.CreatTrustValidationDetail(service, tracingService, trustValidationId, "Plan", planName,
                            "Plan Statement Detail", string.Empty, "Gap found in Date Range", "BAD");
                    }
                    else
                    {
                        //TrustHelper.CreatTrustValidationDetail(service, tracingService, trustValidationId, "Plan", planName,
                        //    "Plan Statement Detail", string.Empty, "No Gap found in Date Range", "GOOD");
                    }
                    #endregion
                }
                
                #endregion

                // Checking Agreement Level
                CheckingAgreementLevel(service, tracingService, trust, trustValidationId, agreementCollection);

            } // end of foreach of Plan
        }

        public void CheckingAgreementLevel(IOrganizationService service, ITracingService tracingService, Entity trust, Guid trustValidationId,
            EntityCollection agreementCollection)
        {
            if (agreementCollection != null && agreementCollection.Entities.Count == 0) return;
            EntityReference trustOwner = trust.Contains("ownerid") ? trust.GetAttributeValue<EntityReference>("ownerid") : null;

            List<DateRange> lstDateRange = new List<DateRange>();
            EntityCollection rateCollection = null;
            foreach (Entity agreement in agreementCollection.Entities)
            {
                string agName = agreement.GetAttributeValue<string>("bpa_name");

                #region ---------- Job Classification ---------------
                EntityCollection jcCollection = LabourRoleHelper.FetchAllJobClassificationByAgreementId(service, tracingService, agreement.Id);
                if (jcCollection != null && jcCollection.Entities.Count > 0)
                {
                    foreach (Entity jobClassification in jcCollection.Entities)
                    {
                        string name = jobClassification.GetAttributeValue<string>("bpa_name");
                        EntityReference owner = jobClassification.GetAttributeValue<EntityReference>("ownerid");

                        if (owner.Id == trustOwner.Id)
                        {
                            //TrustHelper.CreatTrustValidationDetail(service, tracingService, trustValidationId, "Agreement", agName,
                            //    "Job Classification", name, "Owner is the same as Trust", "GOOD");
                        }
                        else
                        {
                            TrustHelper.CreatTrustValidationDetail(service, tracingService, trustValidationId, "Agreement", agName,
                                "Job Classification", name, "Owner is the same as Trust", "BAD");
                        }
                    }
                }
                else
                {
                    TrustHelper.CreatTrustValidationDetail(service, tracingService, trustValidationId, "Agreement", agName,
                        "Job Classification", string.Empty, "At least one is defined", "BAD");
                }

                // Checking Atlest one Is Default Job Classification
                List<Entity> lstDefault = jcCollection.Entities.Where(e => (e.GetAttributeValue<bool>("bpa_isdefault") == true)).ToList();
                if(lstDefault.Count > 0)
                {
                    //TrustHelper.CreatTrustValidationDetail(service, tracingService, trustValidationId, "Agreement", agName,
                    //    "Job Classification", string.Empty, "At least one is default", "GOOD");
                }
                else
                {
                    TrustHelper.CreatTrustValidationDetail(service, tracingService, trustValidationId, "Agreement", agName,
                        "Job Classification", string.Empty, "At least one is default", "BAD");
                }
                #endregion

                #region ---------- Rate ---------------
                lstDateRange = new List<DateRange>();
                rateCollection = RateHelper.FetchAllRateByAgreementId(service, tracingService, agreement.Id);
                if (rateCollection != null && rateCollection.Entities.Count > 0)
                {
                    foreach (Entity rate in rateCollection.Entities)
                    {
                        string name = rate.GetAttributeValue<string>("bpa_name");
                        EntityReference owner = rate.GetAttributeValue<EntityReference>("ownerid");
                        DateTime startDate = rate.GetAttributeValue<DateTime>("bpa_effectivedate");
                        DateTime endDate = rate.GetAttributeValue<DateTime>("bpa_expirydate");
                        lstDateRange.Add(new DateRange(startDate, endDate));

                        if (owner.Id == trustOwner.Id)
                        {
                            //TrustHelper.CreatTrustValidationDetail(service, tracingService, trustValidationId, "Agreement", agName,
                            //    "Rate", name, "Owner is the same as Trust", "GOOD");
                        }
                        else
                        {
                            TrustHelper.CreatTrustValidationDetail(service, tracingService, trustValidationId, "Agreement", agName,
                                "Rate", name, "Owner is the same as Trust", "BAD");
                        }
                    }
                }
                else
                {
                    TrustHelper.CreatTrustValidationDetail(service, tracingService, trustValidationId, "Agreement", agName,
                        "Rate", string.Empty, "At least one is defined", "BAD");
                }

                // Check No gap between 2 date range
                if (DateRange.CheckGap(lstDateRange))
                {
                    TrustHelper.CreatTrustValidationDetail(service, tracingService, trustValidationId, "Agreement", agName,
                        "Rate", string.Empty, "Gap found in Date Range", "BAD");
                }
                else
                {
                    //TrustHelper.CreatTrustValidationDetail(service, tracingService, trustValidationId, "Agreement", agName,
                    //    "Rate", string.Empty, "No Gap found in Date Range", "GOOD");
                }

                #endregion

                #region --------- PRE-PRINT RATE --------------------
                EntityCollection pprCollection = RateHelper.FetchAllPrePrintRateByAgreementId(service, tracingService, agreement.Id);
                if (pprCollection != null && pprCollection.Entities.Count > 0)
                {
                    lstDateRange = new List<DateRange>();
                    foreach (Entity prePrintRate in pprCollection.Entities)
                    {
                        string name = prePrintRate.GetAttributeValue<string>("bpa_name");
                        EntityReference owner = prePrintRate.GetAttributeValue<EntityReference>("ownerid");
                        DateTime startDate = prePrintRate.GetAttributeValue<DateTime>("bpa_effectivedate");
                        DateTime endDate = prePrintRate.GetAttributeValue<DateTime>("bpa_enddate");
                        lstDateRange.Add(new DateRange(startDate, endDate));

                        if (owner.Id == trustOwner.Id)
                        {
                            //TrustHelper.CreatTrustValidationDetail(service, tracingService, trustValidationId, "Agreement", agName,
                            //    "Pre Print Rate", name, "Owner is the same as Trust", "GOOD");
                        }
                        else
                        {
                            TrustHelper.CreatTrustValidationDetail(service, tracingService, trustValidationId, "Agreement", agName,
                                "Pre Print Rate", name, "Owner is the same as Trust", "BAD");
                        }
                    }

                    // Check No gap between 2 date range
                    if (DateRange.CheckGap(lstDateRange))
                    {
                        TrustHelper.CreatTrustValidationDetail(service, tracingService, trustValidationId, "Agreement", agName,
                            "Pre Print Rate", string.Empty, "Gap found in Date Range", "BAD");
                    }
                    else
                    {
                        //TrustHelper.CreatTrustValidationDetail(service, tracingService, trustValidationId, "Agreement", agName,
                        //    "Pre Print Rate", string.Empty, "No Gap found in Date Range", "GOOD");
                    }

                }

                #endregion

                #region ----------- DELINQUENCY RULE ------------------

                EntityCollection drCollection = AgreementHelper.FetchAllDeliquncyRulesByAgreementId(service, tracingService, agreement.Id);
                if (drCollection != null && drCollection.Entities.Count > 0)
                {
                    lstDateRange = new List<DateRange>();
                    foreach (Entity dr in drCollection.Entities)
                    {
                        string name = dr.GetAttributeValue<string>("bpa_name");
                        EntityReference owner = dr.GetAttributeValue<EntityReference>("ownerid");
                        DateTime startDate = dr.GetAttributeValue<DateTime>("bpa_startdate");
                        DateTime endDate = dr.GetAttributeValue<DateTime>("bpa_enddate");
                        lstDateRange.Add(new DateRange(startDate, endDate));

                        if (owner.Id == trustOwner.Id)
                        {
                            //TrustHelper.CreatTrustValidationDetail(service, tracingService, trustValidationId, "Agreement", agName,
                            //    "Delinquency Rule", name, "Owner is the same as Trust", "GOOD");
                        }
                        else
                        {
                            TrustHelper.CreatTrustValidationDetail(service, tracingService, trustValidationId, "Agreement", agName,
                                "Delinquency Rule", name, "Owner is the same as Trust", "BAD");
                        }
                    }

                    // Check No gap between 2 date range
                    if (DateRange.CheckGap(lstDateRange))
                    {
                        TrustHelper.CreatTrustValidationDetail(service, tracingService, trustValidationId, "Agreement", agName,
                            "Delinquency Rule", string.Empty, "Gap found in Date Range", "BAD");
                    }
                    else
                    {
                        //TrustHelper.CreatTrustValidationDetail(service, tracingService, trustValidationId, "Agreement", agName,
                        //    "Delinquency Rule", string.Empty, "No Gap found in Date Range", "GOOD");
                    }

                }

                #endregion

                // Checking Rate Level
                CheckingRateLevel(service, tracingService, trust, trustValidationId, rateCollection);
            }
        }

        public void CheckingRateLevel(IOrganizationService service, ITracingService tracingService, Entity trust, Guid trustValidationId,
            EntityCollection rateCollection)
        {
            if (rateCollection != null && rateCollection.Entities.Count == 0) return;
            EntityReference trustOwner = trust.Contains("ownerid") ? trust.GetAttributeValue<EntityReference>("ownerid") : null;

            foreach (Entity rate in rateCollection.Entities)
            {
                string rateName = rate.GetAttributeValue<string>("bpa_name");

                #region ------- RATE JOB CLASSIFICATION ----------------
                EntityCollection rjcCollection = RateHelper.FetchAllLabourRolesByRateId(service, tracingService, rate.Id);
                if (rjcCollection != null && rjcCollection.Entities.Count > 0)
                {
                    foreach (Entity rateJobClassification in rjcCollection.Entities)
                    {
                        string name = rateJobClassification.GetAttributeValue<string>("bpa_name");
                        EntityReference owner = rateJobClassification.GetAttributeValue<EntityReference>("ownerid");

                        if (owner.Id == trustOwner.Id)
                        {
                            //TrustHelper.CreatTrustValidationDetail(service, tracingService, trustValidationId, "Rate", rateName,
                            //    "Rate Job Classification", name, "Owner is the same as Trust", "GOOD");
                        }
                        else
                        {
                            TrustHelper.CreatTrustValidationDetail(service, tracingService, trustValidationId, "Rate", rateName,
                                "Rate Job Classification", name, "Owner is the same as Trust", "BAD");
                        }
                    }
                }
                else
                {
                    TrustHelper.CreatTrustValidationDetail(service, tracingService, trustValidationId, "Rate", rateName,
                        "Rate Job Classification", string.Empty, "At least one is defined", "BAD");
                }
                #endregion
            }
        }

    }
}
 