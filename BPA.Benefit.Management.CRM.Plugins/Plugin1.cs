﻿#region

using System;
using System.Xml;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.ServiceModel;

using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Client;
using Microsoft.Xrm.Sdk.Discovery;


#endregion

namespace BPA.Benefit.Management.CRM.Plugins
{
    /// <summary>
    ///     Base class for all Plugins.
    /// </summary>
    public class Plugin : IPlugin
    {
        private Collection<Tuple<int, string, string, Action<LocalPluginContext>>> _registeredEvents;

        public static Dictionary<string, string> PluginConfiguration = new Dictionary<string, string>();
        private static string organizationName = string.Empty;
        private static string unsecuredXML = string.Empty;
        private static string securedXML = string.Empty;

        /// <summary>
        ///     Initializes a new instance of the <see cref="Plugin" /> class.
        /// </summary>
        /// <param name="childClassName">The <see cref="Plugin"/> of the derived class.</param>
        internal Plugin(Type childClassName, string unsecureString, string secureString)
        {
            ChildClassName = childClassName.ToString();

            unsecuredXML = unsecureString;
            securedXML = secureString;           
        }

        internal Plugin(Type childClassName)
        {
            ChildClassName = childClassName.ToString();
        }

        /// <summary>
        ///     Gets the List of events that the plug-in should fire for. Each List
        ///     Item is a <see cref="System.Tuple" /> containing the Pipeline Stage, Message and (optionally) the Primary Entity.
        ///     In addition, the fourth parameter provide the delegate to invoke on a matching registration.
        /// </summary>
        protected Collection<Tuple<int, string, string, Action<LocalPluginContext>>> RegisteredEvents
        {
            get
            {
                // ReSharper disable once ConvertIfStatementToNullCoalescingExpression
                if (_registeredEvents == null)
                {
                    _registeredEvents = new Collection<Tuple<int, string, string, Action<LocalPluginContext>>>();
                }

                return _registeredEvents;
            }
        }

        /// <summary>
        ///     Gets or sets the name of the child class.
        /// </summary>
        /// <value>The name of the child class.</value>
        protected string ChildClassName { get; private set; }

        /// <summary>
        ///     Executes the plug-in.
        /// </summary>
        /// <param name="serviceProvider">The service provider.</param>
        /// <remarks>
        ///     For improved performance, Microsoft Dynamics CRM caches plug-in instances.
        ///     The plug-in's Execute method should be written to be stateless as the constructor
        ///     is not called for every invocation of the plug-in. Also, multiple system threads
        ///     could execute the plug-in at the same time. All per invocation state information
        ///     is stored in the context. This means that you should not use global variables in plug-ins.
        /// </remarks>
        public void Execute(IServiceProvider serviceProvider)
        {
            if (serviceProvider == null)
            {
                throw new ArgumentNullException($"serviceProvider");
            }

            // Construct the Local plug-in context.
            LocalPluginContext localcontext = new LocalPluginContext(serviceProvider);

            localcontext.Trace(string.Format(CultureInfo.InvariantCulture, "Entered {0}.Execute()", ChildClassName));

            try
            {
                // Iterate over all of the expected registered events to ensure that the plugin
                // has been invoked by an expected event
                // For any given plug-in event at an instance in time, we would expect at most 1 result to match.
                Action<LocalPluginContext> entityAction =
                    (from a in RegisteredEvents
                        where a.Item1 == localcontext.PluginExecutionContext.Stage &&
                              a.Item2 == localcontext.PluginExecutionContext.MessageName &&
                              (string.IsNullOrWhiteSpace(a.Item3)
                                  ? true
                                  : a.Item3 == localcontext.PluginExecutionContext.PrimaryEntityName)
                        select a.Item4).FirstOrDefault();

                if (entityAction != null)
                {
                    localcontext.Trace(string.Format(
                        CultureInfo.InvariantCulture,
                        "{0} is firing for Entity: {1}, Message: {2}",
                        ChildClassName,
                        localcontext.PluginExecutionContext.PrimaryEntityName,
                        localcontext.PluginExecutionContext.MessageName));

                    entityAction.Invoke(localcontext);

                    // now exit - if the derived plug-in has incorrectly registered overlapping event registrations,
                    // guard against multiple executions.
                }
            }
            catch (FaultException<OrganizationServiceFault> e)
            {
                localcontext.Trace(string.Format(CultureInfo.InvariantCulture, "Exception: {0}", e.ToString()));

                // Handle the exception.
                throw;
            }
            finally
            {
                localcontext.Trace(string.Format(CultureInfo.InvariantCulture, "Exiting {0}.Execute()", ChildClassName));
            }
        }

        protected class LocalPluginContext
        {
            // ReSharper disable once UnusedMember.Local
            private LocalPluginContext()
            {
            }

            internal LocalPluginContext(IServiceProvider serviceProvider)
            {
                if (serviceProvider == null)
                {
                    throw new ArgumentNullException($"serviceProvider");
                }

                // Obtain the execution context service from the service provider.
                PluginExecutionContext =
                    (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));

                // Obtain the tracing service from the service provider.
                TracingService = (ITracingService)serviceProvider.GetService(typeof(ITracingService));

                // Obtain the Organization Service factory service from the service provider
                IOrganizationServiceFactory factory =
                    (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));

                // Use the factory to generate the Organization Service.
                OrganizationService = factory.CreateOrganizationService(PluginExecutionContext.UserId);

                ServiceProvider = serviceProvider;

                Plugin.organizationName =  PluginExecutionContext.OrganizationName;

                if (PluginExecutionContext.OrganizationName.ToUpper() == "CRMPRODDEV2")
                    Plugin.organizationName = PluginExecutionContext.OrganizationName.ToUpper();
                else
                    Plugin.organizationName = PluginExecutionContext.OrganizationName;

                if (!String.IsNullOrEmpty(Plugin.securedXML))
                {
                    try
                    {
                        // Cleared 

                        PluginConfiguration.Clear();

                        //Load Secured XML String
                        XmlDocument _pluginConfiguration = new XmlDocument();
                        _pluginConfiguration.LoadXml(Plugin.securedXML);

                        //Find Root Node With This Organization Name
                        XmlNode node = _pluginConfiguration.SelectSingleNode($"PluginSettings/{Plugin.organizationName}/Settings");

                        if (node != null)
                        {
                            //Foreach node, get the name attribute and the value chile node
                            foreach (XmlNode childNode in node.ChildNodes)
                            {
                                string settingName = childNode.Attributes["name"].Value;
                                string settingValue = childNode.Attributes["value"].Value;

                                PluginConfiguration.Add(settingName, settingValue);
                            }

                            if(!PluginConfiguration.ContainsKey("ExecuteMultipleRequest"))
                                PluginConfiguration.Add("ExecuteMultipleRequest", "1");
                        }

                    }
                    catch (Exception e)
                    {

                    }
                }
            }

            internal IServiceProvider ServiceProvider { get; private set; }

            internal IOrganizationService OrganizationService { get; private set; }

            internal IPluginExecutionContext PluginExecutionContext { get; private set; }

            internal ITracingService TracingService { get; private set; }

            internal void Trace(string message)
            {
                if (string.IsNullOrWhiteSpace(message) || TracingService == null)
                {
                    return;
                }

                if (PluginExecutionContext == null)
                {
                    TracingService.Trace(message);
                }
                else
                {
                    TracingService.Trace(
                        "{0}, Correlation Id: {1}, Initiating User: {2}",
                        message,
                        PluginExecutionContext.CorrelationId,
                        PluginExecutionContext.InitiatingUserId);
                }
            }
        }

        
    }
}
#region Plugin Configuration XML
/*
<PluginSettings>
    <BPADev>
        <Settings>
           <setting name = "CRM Admin" value="26F147A6-1BD7-E511-80CD-005056010DE7"/>
           <setting name = "Forgot password Email Template" value ="4051CF2D-FE3D-E611-80EF-005056010DE7"/>
           <setting name="Submission - Business Process Flow" value="5a5100a2-9c7d-400b-be94-21c6ae3ebde7"/>
           <setting name="Submission - Business Process Flow Stage" value="E3978A8B-8183-2CE0-0A05-48EAB8A50442"/>
           <setting name="Default Currency" value="E3F74334-8BD6-E511-80CD-005056010DE7"/>
           <setting name="Default Job Classification" value="Not Specified"/>
        </Settings>
    </BPADev>
</PluginSettings>
*/
#endregion