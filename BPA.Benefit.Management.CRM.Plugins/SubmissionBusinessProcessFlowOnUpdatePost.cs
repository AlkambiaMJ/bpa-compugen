﻿#region

using System;
using System.Diagnostics;
using BPA.Benefit.Management.CRM.Plugins.Helper;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xrm.Sdk.Messages;
using System.ServiceModel;
using System.Threading.Tasks;
using Microsoft.Crm.Sdk.Messages;

#endregion


namespace BPA.Benefit.Management.CRM.Plugins
{
    public class SubmissionBusinessProcessFlowOnUpdatePost : Plugin
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="SubmissionOnUpdatePre" /> class.
        /// </summary>
        /// 
        //public SubmissionBusinessProcessFlowOnUpdatePost()
        //    : base(typeof(SubmissionBusinessProcessFlowOnUpdatePost))
        public SubmissionBusinessProcessFlowOnUpdatePost(string unsecureString, string secureString)
            : base(typeof(SubmissionBusinessProcessFlowOnUpdatePost), unsecureString, secureString)

        {
            RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(40, "Update",
                PluginHelper.BpaSubmssionBusinessProcessFlow, ExecuteUpdatePost));

            // Note : you can register for more events here if this plugin is not specific to an individual entity and message combination.
            // You may also need to update your RegisterFile.crmregister plug-in registration file to reflect any change.
        }

        /// <summary>
        ///     Executes the plug-in.
        /// </summary>
        /// <param name="localContext">
        ///     The <see cref="Plugin.LocalPluginContext" /> which contains the
        ///     <see cref="IPluginExecutionContext" />,
        ///     <see cref="IOrganizationService" />
        ///     and <see cref="ITracingService" />
        /// </param>
        /// <remarks>
        ///     For improved performance, Microsoft Dynamics CRM caches plug-in instances.
        ///     The plug-in's Execute method should be written to be stateless as the constructor
        ///     is not called for every invocation of the plug-in. Also, multiple system threads
        ///     could execute the plug-in at the same time. All per invocation state information
        ///     is stored in the context. This means that you should not use global variables in plug-ins.
        /// </remarks>
        /// 

        protected void ExecuteUpdatePost(LocalPluginContext localContext)
        {
            // If local context not passed in, raise error
            if (localContext == null)
                throw new ArgumentNullException($"Local context not passed in.");

            // Create the context variables
            IPluginExecutionContext context = localContext.PluginExecutionContext;
            ITracingService tracingService = localContext.TracingService;
            IOrganizationService service = localContext.OrganizationService;
            IServiceProvider serviceProvider = localContext.ServiceProvider;

            // If Target is not passed in, raise error
            tracingService.Trace("If Target is not passed in, raise error");
            if (!context.InputParameters.Contains(PluginHelper.Target) ||
                !(context.InputParameters[PluginHelper.Target] is Entity))
                throw new ArgumentNullException($"Target is not of type Entity.");

            // Create the target entity & preImage
            tracingService.Trace("Create the target entity & preImage");
            Entity entity = (Entity)context.InputParameters[PluginHelper.Target];
            Entity preImageEntity = context.PreEntityImages != null &&
                                    context.PreEntityImages.Contains(PluginHelper.PreImageAlias)
                ? context.PreEntityImages[PluginHelper.PreImageAlias]
                : null;
            if (preImageEntity == null)
                throw new ArgumentNullException($"preImage entity is not defined.");


            if (entity.Contains("activestageid"))
            {
                //Get Execute user, Get Crm Admin User and create IORganizationService for CrM ADmin 
                Guid executeUser = context.UserId;
                //string crmAdminUserId = PluginConfiguration[PluginHelper.ConfigCrmAdmin];
                string crmAdminUserId = string.Empty;
                try
                {
                    crmAdminUserId = PluginConfiguration[PluginHelper.ConfigCrmAdmin];
                }
                catch (Exception ex)
                {
                    // Get value from Database
                    crmAdminUserId = ConfigurationHelper.FetchValueId(service, PluginHelper.ConfigCrmAdmin);
                }
                if (string.IsNullOrEmpty(crmAdminUserId))
                {
                    tracingService.Trace("ERROR FROM SubmissionBusinessProcessFlowOnUpdatePost Plugin - CRM Admin USer Could not Found.");
                    throw new InvalidPluginExecutionException(OperationStatus.Failed, $@"The given key was not present in the dictionary. Please contact BPA Administrator");
                }

                IOrganizationServiceFactory serviceFactory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
                IOrganizationService serviceAdmin = serviceFactory.CreateOrganizationService(new Guid(crmAdminUserId));
                tracingService.Trace("Inside entity.Contains('activestageid')");



                string executeMultipleRequest = "0";
                try
                {
                    executeMultipleRequest = PluginConfiguration[PluginHelper.ConfigExecuteMultipleRequest];
                }
                catch (Exception ex)
                {
                    executeMultipleRequest = "0";
                }
                // Get Execute User
                bool executeMultiple = (executeMultipleRequest == "1");





                string defaultCurrency = string.Empty;
                try
                {
                    defaultCurrency = PluginConfiguration[PluginHelper.ConfigDefaultCurrency];
                }
                catch (Exception ex)
                {
                    // Get value from Database
                    defaultCurrency = ConfigurationHelper.FetchValueId(service, PluginHelper.ConfigDefaultCurrency);
                }
                if (string.IsNullOrEmpty(defaultCurrency))
                {
                    tracingService.Trace("ERROR FROM SubmissionBusinessProcessFlowOnUpdatePost Plugin - CRM Default Currency Could not Found.");
                    throw new InvalidPluginExecutionException(OperationStatus.Failed, $@"The given key was not present in the dictionary. Please contact BPA Administrator");
                }


                Guid Current_StageId = entity.GetAttributeValue<EntityReference>("activestageid").Id;
                Guid Pre_StageId = preImageEntity.GetAttributeValue<EntityReference>("activestageid").Id;

                EntityReference submission = null;
                //Get Submission Information
                if (entity.Contains("bpf_bpa_submissionid"))
                    submission = entity.GetAttributeValue<EntityReference>("bpf_bpa_submissionid");
                else if (preImageEntity.Contains("bpf_bpa_submissionid"))
                    submission = preImageEntity.GetAttributeValue<EntityReference>("bpf_bpa_submissionid");

                if (submission == null)
                    return;
                
                // Get Default Funds from TRUST
                List<Entity> defaultFundList = new List<Entity>();

                // Find and Get Default Welfare Fund
                EntityReference defaultFundRef = null;

                // Fetch All fields of Submission
                Entity SubmissionEntity = service.Retrieve(PluginHelper.BpaSubmission, submission.Id, new ColumnSet(true));
                OptionSetValue submissiontype = SubmissionEntity.GetAttributeValue<OptionSetValue>("bpa_submissiontype");
                bool PlanIsBenefitPlan = false;
                #region ---------------- Get Default Fund -------------------
                if (SubmissionEntity.Contains("bpa_trustid"))
                {
                    tracingService.Trace($"Find out default fund");
                    
                    // Fetch all default funds
                    EntityReference trustRef = SubmissionEntity.Contains("bpa_trustid") ? SubmissionEntity.GetAttributeValue<EntityReference>("bpa_trustid") : null;
                    if (trustRef != null)
                        defaultFundList = TrustHelper.FetchAllDefaultFunds(service, tracingService, trustRef.Id);
                    Entity defaultWelFareFund = null;

                    // Get Plan from submission
                    EntityReference planRef = SubmissionEntity.Contains("bpa_plan") ? SubmissionEntity.GetAttributeValue<EntityReference>("bpa_plan") : null;
                    if (planRef != null)
                    {
                        tracingService.Trace($"Inside Plan Infor");

                        // Get plan Info
                        Entity planInfo = PlanHelper.FetchPlanInformation(service, tracingService, planRef.Id);
                        bool isBenefitPlan = planInfo.Contains("bpa_benefitplan") ? planInfo.GetAttributeValue<bool>("bpa_benefitplan") : false;
                        bool isPensionPlan = planInfo.Contains("bpa_pensionplan") ? planInfo.GetAttributeValue<bool>("bpa_pensionplan") : false;
                        tracingService.Trace($"Benefit Plan {isBenefitPlan}, Pension Plan {isPensionPlan}");

                        if (!isBenefitPlan && !isPensionPlan)
                            throw new InvalidPluginExecutionException(OperationStatus.Failed, $@"Plan must be either 'Benefit Plan' or 'Pension Plan'. Please contact BPA Administrator.");

                        if (defaultFundList != null)
                        {
                            if (isBenefitPlan)
                            {
                                defaultWelFareFund = defaultFundList.Where(x => x.GetAttributeValue<OptionSetValue>("bpa_fundtype").Value == (int)BpaFundbpaFundType.Welfare).FirstOrDefault();
                                PlanIsBenefitPlan = true;
                            }
                            else if (isPensionPlan)
                            {
                                defaultWelFareFund = defaultFundList.Where(x => x.GetAttributeValue<OptionSetValue>("bpa_fundtype").Value == (int)BpaFundbpaFundType.Pension).FirstOrDefault();
                            }
                            if (defaultWelFareFund != null)
                                defaultFundRef = new EntityReference(PluginHelper.BpaFund, defaultWelFareFund.Id);
                        }
                    }
                    else if(submissiontype.Value == (int)SubmissionSubmissionType.RetireeMonthlySubmission)
                    {
                        tracingService.Trace($"Plan is Empty");
                        if (defaultFundList != null)
                        {
                            defaultWelFareFund = defaultFundList.Where(x => x.GetAttributeValue<OptionSetValue>("bpa_fundtype").Value == (int)BpaFundbpaFundType.Welfare).FirstOrDefault();
                            
                            if (defaultWelFareFund != null)
                                defaultFundRef = new EntityReference(PluginHelper.BpaFund, defaultWelFareFund.Id);
                        }
                    }
                }
                // Set default fund if null
                if (defaultFundRef == null)
                {
                    throw new InvalidPluginExecutionException(OperationStatus.Failed, $@"Trust does not have default fund assigned. Please contact BPA Administrator.");
                }

                #endregion
                
                if ((Current_StageId == PluginHelper.SubmissionStageIdPending) && (Pre_StageId == PluginHelper.SubmissionStageIdVerify))
                {
                    #region ----------------- PENDING --------------

                    tracingService.Trace($@"SubmissionBusinessProcessFlowOnUpdatePost: Current Stage 'Pending' START");

                    #region ------------ DELETE Contribution and Transaction --------------
                    // Fetch All Contributions and DELETE 
                    tracingService.Trace("SubmissionBusinessProcessFlowOnUpdatePost: Before Delete Contribution Delete");
                    EntityCollection contributions = ContributionHelper.FetchAllContributionsBySubmissionId(serviceAdmin, SubmissionEntity.Id);
                    if (contributions != null && contributions.Entities.Count > 0)
                    {
                        foreach (Entity c in contributions.Entities)
                            serviceAdmin.Delete(PluginHelper.BpaContribution, c.Id);
                    }

                    // Fetch All Transactions and DELETE 
                    tracingService.Trace("SubmissionBusinessProcessFlowOnUpdatePost: Before Delete Transaction Delete");
                    EntityCollection transactions = TransactionHelper.FetchAllTransactionsBySubmissionId(serviceAdmin, tracingService, SubmissionEntity.Id);
                    if (transactions != null && transactions.Entities.Count > 0)
                    {
                        foreach (Entity t in transactions.Entities)
                            serviceAdmin.Delete(PluginHelper.BpaTransaction, t.Id);
                    }

                    #endregion

                    #region ------------- UPDATE SUBMISSION TOTAL HOURS/ DOLLARS AND GROSS WAGES, OPENING VARIANCE, TOTAL SUBMISSION DUE ---------------------
                    
                    // Update Submission 
                    tracingService.Trace("SubmissionBusinessProcessFlowOnUpdatePost: Before Update Submission");
                    Entity Submission11 = new Entity(PluginHelper.BpaSubmission);
                    Submission11.Id = SubmissionEntity.Id;
                    Submission11["bpa_totalhourssubmitted"] = 0.00;
                    Submission11["bpa_totalhoursearnedsubmitted"] = 0.00;

                    Submission11["bpa_totaldollarssubmitted"] = new Money(0);
                    Submission11["bpa_totalgrosswagessubmitted"] = new Money(0);
                    Submission11["bpa_submissionamount"] = new Money(0);
                    Submission11["bpa_openingvariance"] = new Money(0);
                    Submission11["bpa_totalsubmissiondue"] = new Money(0);
                    Submission11["bpa_paymentmethod"] = null;
                    Submission11["bpa_submissionpaid"] = null;
                    Submission11["bpa_paymentreferencenumber"] = string.Empty;
                    Submission11["bpa_paymentrecieveddate"] = null;
                    Submission11["bpa_submissionvariance"] = null;
                    Submission11["bpa_closingvariance"] = null;
                    Submission11["bpa_depositid"] = null;
                    Submission11["bpa_previousinteresetbalance"] = null;
                    Submission11["bpa_totalsubmissiondueincludinginterest"] = null;
                    service.Update(Submission11);

                    tracingService.Trace("SubmissionBusinessProcessFlowOnUpdatePost: AFter Update Submission");

                    if (SubmissionEntity.Contains("bpa_accountagreementid"))
                    {
                        EntityReference employerId = SubmissionEntity.GetAttributeValue<EntityReference>("bpa_accountagreementid");
                        DateTime createdOn = preImageEntity.GetAttributeValue<DateTime>("createdon");
                        tracingService.Trace("Update the Account (Employer) variance");
                        decimal openingBalance = 0;
                        openingBalance = SubmissionHelper.GetOpeningVariance(service, tracingService, employerId.Id, createdOn);
                        tracingService.Trace("2 Opening Balance - " + openingBalance);
                        AccountAgreementHelper.UpdateAccountVariance(service, tracingService, employerId.Id, openingBalance);
                        tracingService.Trace("Account Update Successfully Completed.");
                    }

                    // Update Deposit Total
                    tracingService.Trace($@"SubmissionBusinessProcessFlowOnUpdatePost: Update Rollup Deposit");
                    EntityReference Pre_depositeId = preImageEntity.Contains("bpa_depositid") ? preImageEntity.GetAttributeValue<EntityReference>("bpa_depositid") : null;
                    if (Pre_depositeId != null)
                        PluginHelper.TriggerRollup(service, tracingService, PluginHelper.BpaDeposit, Pre_depositeId.Id, "bpa_submissiontotal");

                    // Activate Submission Detail Record
                    tracingService.Trace($@"SubmissionBusinessProcessFlowOnUpdatePost: Activate Submssion Details");
                    EntityCollection SubmissionDetails1 = SubmissionHelper.FetchAllSubmissionDetailBySubmissionId(service, tracingService, SubmissionEntity.Id, 
                        false, true);
                    foreach (Entity submissiondetail in SubmissionDetails1.Entities)
                        PluginHelper.ActiveInactivateRecord(service, PluginHelper.BpaSubmissiondetail, submissiondetail.Id, 0, 1); // Active

                    #endregion

                    tracingService.Trace($@"SubmissionBusinessProcessFlowOnUpdatePost: Current Stage 'Pending' COMPLETED");
                    #endregion
                }
                else if (Current_StageId == PluginHelper.SubmissionStageIdVerify)
                {
                    #region ----------- Verification --------------

                    if (Pre_StageId == PluginHelper.SubmissionStageIdPending)
                    {
                        tracingService.Trace($@"SubmissionBusinessProcessFlowOnUpdatePost: START Current Stage 'Verification' AND Previous Stage 'Pending'");
                        #region ----------------- Current Stage: 'VERIFICATION' Previous Stage: 'PENDING' --------------
                        // Set Contribution Owner
                        EntityReference owner = null;
                        if (SubmissionEntity.Contains("ownerid"))
                            owner = SubmissionEntity.GetAttributeValue<EntityReference>("ownerid");


                        EntityReference Employer = null;
                        if (SubmissionEntity.Contains("bpa_accountagreementid"))
                            Employer = SubmissionEntity.GetAttributeValue<EntityReference>("bpa_accountagreementid");
                        OptionSetValue SubmissionType = SubmissionEntity.Contains("bpa_submissiontype")
                            ? SubmissionEntity.GetAttributeValue<OptionSetValue>("bpa_submissiontype")
                            : new OptionSetValue(0);

                        if (SubmissionType.Value == (int)SubmissionSubmissionType.EmployerMoneyOnly)
                        {
                            DateTime submissionDate = SubmissionEntity.Contains("bpa_submissiondate")
                                ? SubmissionEntity.GetAttributeValue<DateTime>("bpa_submissiondate")
                                : DateTime.MinValue;

                            //CREATE CONTRIBUTION
                            Entity contribution = new Entity("bpa_contribution");
                            contribution["bpa_submission"] = new EntityReference(PluginHelper.BpaSubmission, SubmissionEntity.Id);
                            if (Employer != null)
                                contribution["bpa_accountagreementid"] = new EntityReference(PluginHelper.AccountAgreement, Employer.Id);
                            if (submissionDate != DateTime.MinValue)
                                contribution["bpa_contributiondate"] = submissionDate;
                            contribution["bpa_fund"] = defaultFundRef;
                            contribution["bpa_contributionamount"] = new Money(0);
                            contribution["bpa_contributiontype"] = new OptionSetValue(922070000); //Regular
                            contribution["bpa_name"] = Guid.NewGuid().ToString();

                            serviceAdmin.Create(contribution);
                        }
                        else
                        {
                            tracingService.Trace("SubmissionBusinessProcessFlowOnUpdatePost: Generating Contribution ");
                            EntityCollection SubmissionDetails = SubmissionHelper.FetchAllSubmissionDetailBySubmissionId(service, tracingService,
                                    SubmissionEntity.Id, true, true);

                            List<EmployeeSubmissionDetails> Ls_EmployeeDetails = new List<EmployeeSubmissionDetails>();

                            int SubSector_CalculationType = 0;
                            SubSector_CalculationType = AccountAgreementHelper.GetCalculationTypeByAccountId(service, tracingService, Employer.Id);

                            DateTime WorkMonth = DateTime.MinValue;
                            if (SubmissionEntity.Contains("bpa_submissiondate"))
                                WorkMonth = SubmissionEntity.GetAttributeValue<DateTime>("bpa_submissiondate");
                            WorkMonth = new DateTime(WorkMonth.Year, WorkMonth.Month, 1);
                            DateTime Sub_WorkMonth = new DateTime(WorkMonth.Year, WorkMonth.Month, WorkMonth.Day);

                            //create
                            int cntSubmissionDetailRecords = 0; //NEED TO Cound nuber of employee which has not 0
                            foreach (Entity submissiondetail in SubmissionDetails.Entities)
                            {
                                #region ------ Fetch all values from Submission Detail --------------------

                                EntityReference Submission = null, Contact = null, Employment = null, LabourRole = null;

                                decimal icihours = 0, nonicihours = 0, totalhours = 0, grosswages = 0, dollars = 0, pensionCredited = 0,
                                        pensionMemberRequired = 0, pensionMemberVoluntary = 0, hoursEarned = 0;
                                OptionSetValue FlatRateRule = null;

                                if (submissiondetail.Contains("bpa_submissionid"))
                                    Submission = submissiondetail.GetAttributeValue<EntityReference>("bpa_submissionid");
                                if (submissiondetail.Contains("bpa_employmentid"))
                                    Employment = submissiondetail.GetAttributeValue<EntityReference>("bpa_employmentid");
                                if (submissiondetail.Contains("bpa_memberplanid"))
                                    Contact = submissiondetail.GetAttributeValue<EntityReference>("bpa_memberplanid");
                                if (submissiondetail.Contains("bpa_labourroleid"))
                                    LabourRole = submissiondetail.GetAttributeValue<EntityReference>("bpa_labourroleid");

                                if (submissiondetail.Contains("bpa_icihours"))
                                    icihours = submissiondetail.GetAttributeValue<decimal>("bpa_icihours");
                                if (submissiondetail.Contains("bpa_nonicihours"))
                                    nonicihours = submissiondetail.GetAttributeValue<decimal>("bpa_nonicihours");
                                if (submissiondetail.Contains("bpa_totalhours"))
                                    totalhours = submissiondetail.GetAttributeValue<decimal>("bpa_totalhours");

                                if (submissiondetail.Contains("bpa_hoursearned"))
                                    hoursEarned = submissiondetail.GetAttributeValue<decimal>("bpa_hoursearned");
                                
                                if (submissiondetail.Contains("bpa_grosswages") &&
                                    submissiondetail.GetAttributeValue<Money>("bpa_grosswages") != null)
                                    grosswages = submissiondetail.GetAttributeValue<Money>("bpa_grosswages").Value;
                                if (submissiondetail.Contains("bpa_dollars") &&
                                    submissiondetail.GetAttributeValue<Money>("bpa_dollars") != null)
                                    dollars = submissiondetail.GetAttributeValue<Money>("bpa_dollars").Value;

                                if (submissiondetail.Contains("bpa_flatraterule"))
                                    FlatRateRule = submissiondetail.GetAttributeValue<OptionSetValue>("bpa_flatraterule");
                                
                                if (submissiondetail.Contains("bpa_pensioncredited"))
                                    pensionCredited = submissiondetail.GetAttributeValue<decimal>("bpa_pensioncredited");
                                if (submissiondetail.Contains("bpa_pensionmemberrequired") &&
                                            submissiondetail.GetAttributeValue<Money>("bpa_pensionmemberrequired") != null)
                                    pensionMemberRequired = submissiondetail.GetAttributeValue<Money>("bpa_pensionmemberrequired").Value;
                                if (submissiondetail.Contains("bpa_pensionmembervoluntary") &&
                                            submissiondetail.GetAttributeValue<Money>("bpa_pensionmembervoluntary") != null)
                                    pensionMemberVoluntary = submissiondetail.GetAttributeValue<Money>("bpa_pensionmembervoluntary").Value;


                                #endregion


                                #region --- Create List with Employeee Submission Detail -----

                                //FETCH LABOUR ROLE AND WORK MONTH
                                if ((LabourRole == null) || (WorkMonth == DateTime.MinValue))
                                    return;

                                if(FlatRateRule == null)
                                {
                                    if ((totalhours == 0) && (hoursEarned == 0) && (grosswages == 0) && (dollars == 0))
                                        FlatRateRule = new OptionSetValue((int)BpaSubmissionDetailBpaFlatRateRule.Ignore);
                                    else
                                        FlatRateRule = new OptionSetValue((int)BpaSubmissionDetailBpaFlatRateRule.Charge);
                                }
                                
                                EmployeeSubmissionDetails esd = new EmployeeSubmissionDetails(); //Get the value - LR
                                esd.LabourRoleId = LabourRole.Id; //Get the value - LR
                                esd.SubmissionDetailId = submissiondetail.Id;
                                esd.FlatRateRule = FlatRateRule.Value;

                                if ((totalhours != 0) || (hoursEarned != 0) || (grosswages != 0) || (dollars != 0)
                                        || (SubSector_CalculationType == (int)BpaSubSectorBpaCalculationType.Flat)
                                        || (SubSector_CalculationType == (int)BpaSubSectorBpaCalculationType.Corporate))
                                {
                                    cntSubmissionDetailRecords++;
                                    Ls_EmployeeDetails.Add(esd);
                                }
                                else
                                {
                                    if ((totalhours == 0) && (hoursEarned == 0) && (grosswages == 0) && (dollars == 0)
                                        && ((FlatRateRule.Value == (int)BpaSubmissionDetailBpaFlatRateRule.Remove) ||
                                            (FlatRateRule.Value == (int)BpaSubmissionDetailBpaFlatRateRule.Charge)))
                                    {
                                        Ls_EmployeeDetails.Add(esd);
                                    }
                                }
                                #endregion
                            }

                            tracingService.Trace("Total Submission Detail Record =" + Ls_EmployeeDetails.Count.ToString());

                            #region ------------------ THIS IS THE LGGIC FOR CONTRIBUTIONS ------------------------------

                            //GET ALL LABOUR ROLES
                            List<Guid> Ls_LabourRole = RateHelper.FetchAllLabourRoles(service, tracingService, Employer.Id);
                            tracingService.Trace($@"Get All Unique Labour role {Ls_LabourRole.Count()}");

                            if(Ls_LabourRole.Count() == 0)
                            {
                                throw new InvalidPluginExecutionException(OperationStatus.Failed, $@"There are not rates assigned to the Job Classifications selected. Please review rates before proceeding.");
                            }

                            List<Rate> rates1 = RateHelper.GetRatesByEmployerIdUsingFetchXml(service, tracingService, Employer.Id, Sub_WorkMonth);
                            if (rates1 == null || rates1.Count == 0)
                                throw new InvalidPluginExecutionException(OperationStatus.Failed, $@"There is no Rate applicable to this agreement for the specified Work Month.");

                            tracingService.Trace($@"Get All Rates(Master List) {rates1.Count()}");
                            tracingService.Trace("this is done");
                            decimal totalamount = 0;
                            decimal submissiondue = 0;

                            foreach (Guid labourrole in Ls_LabourRole) // - LR
                            {
                                tracingService.Trace($@"Labour role: {labourrole}");
                                //Employer = SubmissionEntity.GetAttributeValue<EntityReference>("bpa_accountagreementid");
                                var tmpLs_EmployeeDetails = Ls_EmployeeDetails.Where(ed => ed.LabourRoleId == labourrole);
                                tracingService.Trace($@"Number of Employee Details: {tmpLs_EmployeeDetails.Count()}");

                                Entity SumResult = SubmissionHelper.FetchSumOfAllSubmissionDetails(service, tracingService, SubmissionEntity.Id,
                                    Guid.Empty, labourrole, SubmissionType.Value);
                                decimal SumDollars = 0, SumHours = 0, SumGrossWages = 0, SumHoursEarned = 0,
                                    SumPensionCredited = 0, SumPensionMemberRequired = 0, SumPensionMemberVoluntary = 0;
                                if (SumResult != null)
                                {
                                    if (SumResult.Contains("bpa_dollars") &&
                                        ((AliasedValue)SumResult.Attributes["bpa_dollars"]).Value != null)
                                        SumDollars = ((Money)((AliasedValue)SumResult.Attributes["bpa_dollars"]).Value).Value;
                                    if (SumResult.Contains("bpa_grosswages") &&
                                        ((AliasedValue)SumResult.Attributes["bpa_grosswages"]).Value != null)
                                        SumGrossWages = ((Money)((AliasedValue)SumResult.Attributes["bpa_grosswages"]).Value).Value;
                                    if (SumResult.Contains("bpa_totalhours") &&
                                        ((AliasedValue)SumResult.Attributes["bpa_totalhours"]).Value != null)
                                        SumHours = (decimal)((AliasedValue)SumResult.Attributes["bpa_totalhours"]).Value;

                                    if (SumResult.Contains("bpa_hoursearned") &&
                                        ((AliasedValue)SumResult.Attributes["bpa_hoursearned"]).Value != null)
                                        SumHoursEarned = (decimal)((AliasedValue)SumResult.Attributes["bpa_hoursearned"]).Value;

                                    if (SumResult.Contains("bpa_pensioncredited") &&
                                        ((AliasedValue)SumResult.Attributes["bpa_pensioncredited"]).Value != null)
                                        SumPensionCredited = (decimal)((AliasedValue)SumResult.Attributes["bpa_pensioncredited"]).Value;
                                    if (SumResult.Contains("bpa_pensionmemberrequired") &&
                                        ((AliasedValue)SumResult.Attributes["bpa_pensionmemberrequired"]).Value != null)
                                        SumPensionMemberRequired = ((Money)((AliasedValue)SumResult.Attributes["bpa_pensionmemberrequired"]).Value).Value;
                                    if (SumResult.Contains("bpa_pensionmembervoluntary") &&
                                        ((AliasedValue)SumResult.Attributes["bpa_pensionmembervoluntary"]).Value != null)
                                        SumPensionMemberVoluntary = ((Money)((AliasedValue)SumResult.Attributes["bpa_pensionmembervoluntary"]).Value).Value;
                                }
                                //tracingService.Trace("SumDollar = " + SumDollars.ToString());
                                //tracingService.Trace("SumHours = " + SumHours.ToString());

                                //tracingService.Trace("SumHoursEarned = " + SumHoursEarned.ToString());

                                //tracingService.Trace("SumGrossWages = " + SumGrossWages.ToString());
                                //tracingService.Trace("SumPensionCredited = " + SumPensionCredited.ToString());
                                //tracingService.Trace("SumPensionMemberRequired = " + SumPensionMemberRequired.ToString());
                                //tracingService.Trace("SumPensionMemberVoluntary = " + SumPensionMemberVoluntary.ToString());
                                //First of all without Taxable Fund
                                var regrates1 = rates1.Where(r => (r.CalculationType != (int)RateCalculationType.Tax) &&
                                            (r.LabourId.Value == labourrole));
                                tracingService.Trace("Count = " + regrates1.Count());

                                foreach (Rate rateC in regrates1)
                                {
                                    //tracingService.Trace("rateC.RateValue.Value = " + rateC.RateValue.Value.ToString());
                                    //tracingService.Trace("rateC.CalculationType = " + ((int)rateC.CalculationType).ToString());

                                    decimal contributionamount = 0, dollarshourscounts = 0;

                                    if (rateC.CalculationType == (int)RateCalculationType.Dollar)
                                    {
                                        //if ((SubSector_CalculationType == (int)BpaSubSectorBpaCalculationType.DollarBased) ||
                                        //    (SubSector_CalculationType == (int)BpaSubSectorBpaCalculationType.Corporate)) //added jwalin
                                        //{
                                        contributionamount = SumDollars;
                                        dollarshourscounts = SumDollars;
                                        //}
                                        //else
                                        //{
                                        //    contributionamount = rateC.RateValue.Value;
                                        //    dollarshourscounts = 0;
                                        //}
                                    }
                                    else if (rateC.CalculationType == (int)RateCalculationType.Hourly)
                                    {
                                        contributionamount = SumHours * rateC.RateValue.Value;
                                        dollarshourscounts = SumHours;
                                    }
                                    else if (rateC.CalculationType == (int)RateCalculationType.HourEarned)
                                    {
                                        contributionamount = SumHoursEarned * rateC.RateValue.Value;
                                        dollarshourscounts = SumHoursEarned;
                                    }

                                    else if (rateC.CalculationType == (int)RateCalculationType.PercentageGrossWages)
                                    {
                                        contributionamount = SumGrossWages * rateC.RateValue.Value;
                                        dollarshourscounts = SumGrossWages;
                                    }
                                    else if (rateC.CalculationType == (int)RateCalculationType.FlatRate)
                                    {
                                        tracingService.Trace("tmpLs_EmployeeDetails.Count() = " + tmpLs_EmployeeDetails.Count().ToString());
                                        var charge = Ls_EmployeeDetails.Where(ed => (ed.FlatRateRule == (int)BpaSubmissionDetailBpaFlatRateRule.Charge) && (ed.LabourRoleId == labourrole));

                                        contributionamount = rateC.RateValue.Value * charge.Count();

                                        // employees.Count;
                                        dollarshourscounts = charge.Count(); // employees.Count;

                                    }
                                    else if (rateC.CalculationType == (int)RateCalculationType.PensionMemberRequiredDollar)
                                    {
                                        contributionamount = SumPensionMemberRequired * rateC.RateValue.Value;
                                        dollarshourscounts = SumPensionMemberRequired;
                                    }
                                    else if (rateC.CalculationType == (int)RateCalculationType.PensionMemberVoluntaryDollar)
                                    {
                                        contributionamount = SumPensionMemberVoluntary * rateC.RateValue.Value;
                                        dollarshourscounts = SumPensionMemberVoluntary;
                                    }
                                    tracingService.Trace("dollarshourscounts = " + dollarshourscounts.ToString());


                                    int calculationtype = 0;
                                    if (rateC.CalculationType == null)
                                        calculationtype = 0;
                                    else
                                        calculationtype = rateC.CalculationType.Value;

                                    Guid fundid = Guid.Empty;
                                    if (rateC.FundId == null)
                                        fundid = Guid.Empty;
                                    else
                                        fundid = rateC.FundId.Value;

                                    //CHECK IF SUBMISSION TYPE = Member Self-Pay Refund
                                    OptionSetValue Sub_SubmisionType = SubmissionEntity.GetAttributeValue<OptionSetValue>("bpa_submissiontype");
                                    decimal HowManyCharge = 0;
                                    if (rateC.CalculationType == (int)RateCalculationType.FlatRate)
                                    {
                                        tracingService.Trace($@"Inside Flat Rate");
                                        //Find out how many 
                                        var charge = Ls_EmployeeDetails.Where(ed => (ed.FlatRateRule == (int)BpaSubmissionDetailBpaFlatRateRule.Charge) && (ed.LabourRoleId == labourrole));
                                        HowManyCharge = charge.Count();
                                        var remove = Ls_EmployeeDetails.Where(ed => (ed.FlatRateRule == (int)BpaSubmissionDetailBpaFlatRateRule.Remove) && (ed.LabourRoleId == labourrole));
                                        HowManyCharge = HowManyCharge + remove.Count() * -1;
                                        var ignore = Ls_EmployeeDetails.Where(ed => (ed.FlatRateRule == (int)BpaSubmissionDetailBpaFlatRateRule.Ignore) && (ed.LabourRoleId == labourrole));

                                        tracingService.Trace($@"Charge {charge.Count()}, Remove {remove.Count()}, Ignore {ignore.Count()}");


                                        if ((Sub_SubmisionType.Value == (int)SubmissionSubmissionType.EmployerAdjustment) ||
                                            (Sub_SubmisionType.Value == (int)SubmissionSubmissionType.EmployerRegular))
                                        {
                                            if (HowManyCharge > 0)
                                                contributionamount = rateC.RateValue.Value * HowManyCharge;
                                            else if (HowManyCharge < 0)
                                                contributionamount = rateC.RateValue.Value * HowManyCharge;
                                            else
                                                contributionamount = 0;
                                            tracingService.Trace($@"inside HowManyCharge {HowManyCharge}, contributionamount {contributionamount} ");

                                            dollarshourscounts = HowManyCharge;
                                        }
                                        else
                                            contributionamount = rateC.RateValue.Value * HowManyCharge;


                                    }
                                    else if (((rateC.CalculationType == (int)RateCalculationType.Hourly) || (rateC.CalculationType == (int)RateCalculationType.HourEarned)) &&
                                        (SubSector_CalculationType == (int)BpaSubSectorBpaCalculationType.Flat))
                                    {
                                        HowManyCharge = dollarshourscounts;
                                    }
                                    else
                                        HowManyCharge = dollarshourscounts;
                                    //tracingService.Trace($@"Howmany charge {HowManyCharge}, contributionamount: { contributionamount } ");

                                    //CHECK Contribution exists or not if exists used update existing one
                                    // Where Submission = Current Submission AND Fund = Contribution fundId AND Rate = rate.Rate.Value
                                    Entity update = ContributionHelper.FetchContributionBySubmissionFundIdAndRate(service, SubmissionEntity.Id,
                                            fundid, rateC.RateValue.Value, false);
                                    if (update == null)
                                    {
                                        ContributionHelper.CreateContribution(serviceAdmin, Employer.Id, SubmissionEntity.Id,
                                            Sub_WorkMonth, fundid, (int)BpaContributionbpaContributionType.Regular,
                                            calculationtype, rateC.RateValue.Value, contributionamount, rateC.Id.Value,
                                            rateC.LabourId.Value, HowManyCharge, owner);
                                    }
                                    else
                                    {
                                        decimal tmpAmount = 0, tmpDollarsHoursCount = 0;
                                        if (update.Contains("bpa_contributionamount"))
                                            decimal.TryParse(update.GetAttributeValue<Money>("bpa_contributionamount").Value.ToString(), out tmpAmount);
                                        if (update.Contains("bpa_dollarshourscount"))
                                            decimal.TryParse(update.GetAttributeValue<decimal>("bpa_dollarshourscount").ToString(), out tmpDollarsHoursCount);
                                        
                                        tmpDollarsHoursCount = tmpDollarsHoursCount + dollarshourscounts;
                                        tmpAmount = (tmpDollarsHoursCount * rateC.RateValue.Value);
                                        ContributionHelper.UpdateContribution(serviceAdmin, update.Id, SubmissionEntity.Id, tmpAmount, tmpDollarsHoursCount);
                                    }
                                    totalamount = totalamount + contributionamount;
                                }
                            } //end of labour loop
                            //throw new InvalidPluginExecutionException(OperationStatus.Failed, "Error Throw by jwalin khatri to validate the Few things.");


                            submissiondue = totalamount;

                            #endregion

                            //CACLUATE FOR TAXABLE
                            #region ------------ Taxable contribution calculation -----------------

                            var taxrates1 = rates1.Where(r => r.CalculationType == (int)RateCalculationType.Tax);
                            foreach (Guid labourrole in Ls_LabourRole) // - LR
                            {
                                Entity SumResult = SubmissionHelper.FetchSumOfAllSubmissionDetails(service, tracingService, SubmissionEntity.Id,
                                    Guid.Empty, labourrole, SubmissionType.Value);
                                decimal SumDollars = 0, SumHours = 0, SumHoursEarned = 0, SumGrossWages = 0, SumPensionCredited = 0, SumPensionMemberRequired = 0,
                                    SumPensionMemberVoluntary = 0;
                                if (SumResult != null)
                                {
                                    if (SumResult.Contains("bpa_dollars") &&
                                            ((AliasedValue)SumResult.Attributes["bpa_dollars"]).Value != null)
                                        SumDollars = ((Money)((AliasedValue)SumResult.Attributes["bpa_dollars"]).Value).Value;
                                    if (SumResult.Contains("bpa_grosswages") &&
                                            ((AliasedValue)SumResult.Attributes["bpa_grosswages"]).Value != null)
                                        SumGrossWages = ((Money)((AliasedValue)SumResult.Attributes["bpa_grosswages"]).Value).Value;
                                    if (SumResult.Contains("bpa_totalhours") &&
                                            ((AliasedValue)SumResult.Attributes["bpa_totalhours"]).Value != null)
                                        SumHours = (decimal)((AliasedValue)SumResult.Attributes["bpa_totalhours"]).Value;

                                    if (SumResult.Contains("bpa_hoursearned") && ((AliasedValue)SumResult.Attributes["bpa_hoursearned"]).Value != null)
                                        SumHoursEarned = (decimal)((AliasedValue)SumResult.Attributes["bpa_hoursearned"]).Value;


                                    if (SumResult.Contains("bpa_pensioncredited") &&
                                            ((AliasedValue)SumResult.Attributes["bpa_pensioncredited"]).Value != null)
                                        SumPensionCredited = (decimal)((AliasedValue)SumResult.Attributes["bpa_pensioncredited"]).Value;
                                    if (SumResult.Contains("bpa_pensionmemberrequired") &&
                                            ((AliasedValue)SumResult.Attributes["bpa_pensionmemberrequired"]).Value != null)
                                        SumPensionMemberRequired = ((Money)((AliasedValue)SumResult.Attributes["bpa_pensionmemberrequired"]).Value).Value;
                                    if (SumResult.Contains("bpa_pensionmembervoluntary") &&
                                            ((AliasedValue)SumResult.Attributes["bpa_pensionmembervoluntary"]).Value != null)
                                        SumPensionMemberVoluntary = ((Money)((AliasedValue)SumResult.Attributes["bpa_pensionmembervoluntary"]).Value).Value;
                                };

                                var taxrates = taxrates1.Where(r => r.LabourId.Value == labourrole);
                                foreach (Rate rateC in taxrates)
                                {
                                    decimal contributionamount = 0;
                                    decimal totalotheramount = 0;
                                    totalotheramount = ContributionHelper.GetContributionsAmountByFundIdLabourRoleId(service, rateC.TaxableFundId.Value,
                                        SubmissionEntity.Id, rateC.LabourId.Value);
                                    contributionamount = totalotheramount * rateC.RateValue.Value;

                                    int calculationtype = 0;
                                    if (rateC.CalculationType == null)
                                        calculationtype = 0;
                                    else
                                        calculationtype = rateC.CalculationType.Value;

                                    Guid fundid = Guid.Empty;
                                    if (rateC.FundId == null)
                                        fundid = Guid.Empty;
                                    else
                                        fundid = rateC.FundId.Value;

                                    Entity update = ContributionHelper.FetchContributionBySubmissionFundIdAndRate(service, SubmissionEntity.Id, fundid, rateC.RateValue.Value, true);
                                    if (update == null)
                                    {
                                        ContributionHelper.CreateContribution(serviceAdmin, Employer.Id, SubmissionEntity.Id, Sub_WorkMonth, fundid,
                                            (int)BpaContributionbpaContributionType.Regular, calculationtype, rateC.RateValue.Value, contributionamount, rateC.Id.Value,
                                            rateC.LabourId.Value, totalotheramount, owner);
                                    }
                                    else
                                    {
                                        decimal tmpAmount = 0, tmpDollarsHoursCount = 0;
                                        if (update.Attributes.Contains("bpa_contributionamount"))
                                            decimal.TryParse(update.GetAttributeValue<Money>("bpa_contributionamount").Value.ToString(), out tmpAmount);
                                        if (update.Attributes.Contains("bpa_dollarshourscount"))
                                            decimal.TryParse(update.GetAttributeValue<decimal>("bpa_dollarshourscount").ToString(), out tmpDollarsHoursCount);

                                        tmpAmount = tmpAmount + contributionamount;
                                        tmpDollarsHoursCount = tmpDollarsHoursCount + totalotheramount;
                                        ContributionHelper.UpdateContribution(serviceAdmin, update.Id, SubmissionEntity.Id, tmpAmount, tmpDollarsHoursCount);
                                    }
                                    submissiondue = submissiondue + contributionamount;
                                }
                            }

                            #endregion

                            // Deactivate Submission Detail Records
                            #region ----------- Deactivate Submission Detail Records -----------

                            tracingService.Trace($@"SubmissionBusinessProcessFlowOnUpdatePost: Deactivate Submssion Details");
                            foreach (Entity submissiondetail in SubmissionDetails.Entities)
                                PluginHelper.DeactivateRecord(serviceAdmin, PluginHelper.BpaSubmissiondetail, submissiondetail.Id, 2); // Inactive

                            #endregion


                        } //end of else

                        //UPDATE SUBMISSION TOTALS
                        Entity Submission1 = new Entity(PluginHelper.BpaSubmission)
                        {
                            Id = SubmissionEntity.Id,
                            ["bpa_recalucatetotal"] = true
                        };
                        serviceAdmin.Update(Submission1);

                        #endregion
                        tracingService.Trace($@"SubmissionBusinessProcessFlowOnUpdatePost: COMPLETED Current Stage 'Verification' AND Previous Stage 'Pending'");
                    }
                    else if (Pre_StageId == PluginHelper.SubmissionStageIdDepositPending)
                    {
                        tracingService.Trace($@"SubmissionBusinessProcessFlowOnUpdatePost: START Current Stage 'Verification' AND Previous Stage 'Deposit Pending'");
                        #region ----------------- Current Stage: 'VERIFICATION' Previous Stage: 'DEPOSIT PENDING' --------------
                        // Create the Transaction Request object to hold all of our changes in
                        ExecuteTransactionRequest requestRetroprocessing = new ExecuteTransactionRequest()
                        {
                            // Create an empty organization request collection.
                            Requests = new OrganizationRequestCollection(),
                            ReturnResponses = true
                        };


                        //Fetch All Contribution and Transaction for Delete
                        EntityCollection contributions = ContributionHelper.FetchAllContributionsBySubmissionId(service, SubmissionEntity.Id);
                        if (contributions != null && contributions.Entities.Count > 0)
                        {
                            foreach (Entity c in contributions.Entities)
                            {
                                Entity contribution = new Entity(PluginHelper.BpaContribution)
                                {
                                    Id = c.Id,
                                    ["bpa_contributionpaid"] = null
                                };
                                if (executeMultiple)
                                {
                                    UpdateRequest updateReq = new UpdateRequest() { Target = contribution };
                                    requestRetroprocessing.Requests.Add(updateReq);
                                }
                                else
                                    serviceAdmin.Update(contribution);
                            }
                        }
                        tracingService.Trace($@"After update contibution paid amount = null");

                        // Delete Benefit Transaction
                        EntityCollection transactions = TransactionHelper.FetchAllTransactionsBySubmissionId(serviceAdmin, tracingService, SubmissionEntity.Id);
                        if (transactions != null && transactions.Entities.Count > 0)
                        {
                            foreach (Entity t in transactions.Entities)
                            {
                                if (executeMultiple)
                                {
                                    EntityReference transactionRef = new EntityReference(PluginHelper.BpaTransaction, t.Id);
                                    DeleteRequest deleteReq = new DeleteRequest() { Target = transactionRef };
                                    requestRetroprocessing.Requests.Add(deleteReq);
                                }
                                else
                                    serviceAdmin.Delete(PluginHelper.BpaTransaction, t.Id);

                            }
                        }
                        tracingService.Trace($@"After delete all transaction created");

                        // Delete Pension Transaction
                        EntityCollection pensionTransactions = PensionTransactionHelper.FetchAllTransactionsBySubmissionId(serviceAdmin, tracingService, SubmissionEntity.Id);
                        if (pensionTransactions != null && pensionTransactions.Entities.Count > 0)
                        {
                            foreach (Entity t in pensionTransactions.Entities)
                            {
                                if (executeMultiple)
                                {
                                    EntityReference transactionRef = new EntityReference(PluginHelper.BpaPensionTransaction, t.Id);
                                    DeleteRequest deleteReq = new DeleteRequest() { Target = transactionRef };
                                    requestRetroprocessing.Requests.Add(deleteReq);
                                }
                                else
                                    serviceAdmin.Delete(PluginHelper.BpaPensionTransaction, t.Id);
                            }
                        }
                        tracingService.Trace($@"After delete all Pension transaction created");

                        if (executeMultiple)
                        {
                            #region --------------- Execute Multiple Request --------------

                            try
                            {
                                int pageSize = 1000;
                                // TESTING - record the number of requests
                                int requestCount = requestRetroprocessing.Requests.Count;

                                tracingService.Trace($"TOTAL REQUEST IN requestPostedTransactions {requestCount}");
                                if (requestCount > pageSize)
                                {
                                    tracingService.Trace($"INSIDE if (requestCount > pageSize)");

                                    decimal maxPage = Math.Ceiling((decimal)requestCount / pageSize);
                                    ExecuteTransactionRequest tmp = new ExecuteTransactionRequest()
                                    {
                                        // Create an empty organization request collection.
                                        Requests = new OrganizationRequestCollection(),
                                        ReturnResponses = true
                                    };

                                    for (int pageNumber = 1; pageNumber <= maxPage; pageNumber++)
                                    {
                                        tracingService.Trace($"INSIDE LOOP {pageNumber} / {maxPage}");

                                        List<OrganizationRequest> listRequest = requestRetroprocessing.Requests.Skip((pageNumber - 1) * pageSize)
                                                                                                        .Take(pageSize).ToList();
                                        OrganizationRequestCollection orc = new OrganizationRequestCollection();
                                        listRequest.ForEach(orc.Add);
                                        tmp.Requests = orc;
                                        ExecuteTransactionResponse responsePostingTransaction = (ExecuteTransactionResponse)serviceAdmin.Execute(tmp);
                                    }
                                }
                                else
                                {
                                    tracingService.Trace($"INSIDE ELSE OF if (requestCount >  pageSize)");
                                    ExecuteTransactionResponse responsePostingTransaction = (ExecuteTransactionResponse)serviceAdmin.Execute(requestRetroprocessing);
                                }
                            }
                            catch (FaultException<OrganizationServiceFault> ex)
                            {
                                throw new InvalidPluginExecutionException($@"Update/Delete request failed for the transaction {((ExecuteTransactionFault)(ex.Detail)).FaultedRequestIndex + 1} and the reason being: {ex.Detail.Message}");
                            }
                            #endregion
                        }

                        // Remove Association with Deposit and Update Submission variance, Amount Paid makeing by NULL
                        tracingService.Trace($@"Update Submission Deposit, Submssion Paid, Submission Variance and Closing Variance make null");
                        Entity Submission11 = new Entity(PluginHelper.BpaSubmission)
                        {
                            Id = SubmissionEntity.Id,
                            ["bpa_depositid"] = null,
                            ["bpa_submissionpaid"] = null,
                            ["bpa_submissionvariance"] = null,
                            ["bpa_closingvariance"] = null
                        };
                        service.Update(Submission11);

                        // Update the Roll ups of Associated Deposit
                        EntityReference deposit = SubmissionEntity.GetAttributeValue<EntityReference>("bpa_depositid");
                        if (deposit != null)
                        {
                            // Update Deposit Total
                            tracingService.Trace($@"SubmissionOnUpdatePre: Update Rollup Deposit");
                            PluginHelper.TriggerRollup(service, tracingService, PluginHelper.BpaDeposit, deposit.Id, "bpa_submissiontotal");
                        }
                        #endregion
                        tracingService.Trace($@"SubmissionBusinessProcessFlowOnUpdatePost: COMPLETED Current Stage 'Verification' AND Previous Stage 'Deposit Pending'");
                    }

                    #endregion
                }
                else if (Current_StageId == PluginHelper.SubmissionStageIdDepositPending)
                {
                    if (submissiontype.Value == (int)SubmissionSubmissionType.RetireeMonthlySubmission)
                        return;
                    #region ------------- DEPOSIT PENDING -----------

                    if (Pre_StageId == PluginHelper.SubmissionStageIdVerify)
                    {
                        #region ------- Previous statge is Verify -------------
                        tracingService.Trace($@"SubmissionBusinessProcessFlowOnUpdatePost: START Current Stage 'Deposit Pending' AND Previous Stage 'Verification'");

                        #region ------------ GET ALL ELIGIBILITY CATEGORY AND ELIGIBILITY CATEGORY DETAILS ----------------
                        EntityReference planRef = SubmissionEntity.Contains("bpa_plan") ? SubmissionEntity.GetAttributeValue<EntityReference>("bpa_plan") : null;
                        bool isOnlyPensionPlan = false;
                        Entity planInfo = PlanHelper.FetchPlanInformation(service, tracingService, planRef.Id);
                        if (planInfo != null)
                        {
                            bool isPensionPlan = planInfo.GetAttributeValue<bool>("bpa_pensionplan");
                            bool isBenefitPlan = planInfo.GetAttributeValue<bool>("bpa_benefitplan");

                            if (!isBenefitPlan && isPensionPlan)
                                isOnlyPensionPlan = true;
                        }
                        // Get all Eligibility Categories for this Member's Plan
                        EntityCollection eligibilityCategories = MemberPlanRetroprocessingHelper_Optimized.FetchAllEligibilityCategoryByPlan(service,
                            tracingService, planRef.Id);
                        if (eligibilityCategories.Entities.Count == 0)
                        {
                            // Raise an error that Eligibility Categories record is null
                            throw new InvalidPluginExecutionException(OperationStatus.Failed, @"Cannot retrieve Eligibility Category records.");
                        }

                        EntityCollection eligibilityCategoryDetails = null;
                        if (!isOnlyPensionPlan)
                        {
                            // Get all Eligibility Category Details for this Member's Plan
                            eligibilityCategoryDetails = MemberPlanRetroprocessingHelper_Optimized.FetchAllEligibilityDetails(service, tracingService, planRef.Id);
                            if (eligibilityCategoryDetails.Entities.Count == 0)
                            {
                                // Raise an error that Eligibility Categories record is null
                                throw new InvalidPluginExecutionException(OperationStatus.Failed, @"Cannot retrieve Eligibility Category Detail records.");
                            }
                        }

                        #endregion

                        //Fetch all Submission Detail Records
                        EntityCollection SubmissionDetails = SubmissionHelper.FetchAllSubmissionDetailBySubmissionId(service, tracingService, 
                            SubmissionEntity.Id, false, false);
                        List<EmployeeSubmissionDetails> Ls_EmployeeDetails = new List<EmployeeSubmissionDetails>();
                        if (SubmissionDetails != null && SubmissionDetails.Entities.Count > 0)
                        {
                            tracingService.Trace("Inside All Submission Detail - " + SubmissionDetails.Entities.Count);
                            EntityReference Employer = null;
                            if (SubmissionEntity.Contains("bpa_accountagreementid"))
                                Employer = SubmissionEntity.GetAttributeValue<EntityReference>("bpa_accountagreementid");

                            OptionSetValue SubmissionType = new OptionSetValue(0);
                            if (SubmissionEntity.Contains("bpa_submissiontype"))
                                SubmissionType = SubmissionEntity.GetAttributeValue<OptionSetValue>("bpa_submissiontype");

                            int SubSector_CalculationType = 0;
                            SubSector_CalculationType = AccountAgreementHelper.GetCalculationTypeByAccountId(service, tracingService, Employer.Id);
                            tracingService.Trace("Fetch SubSector_CalculationType - " + SubSector_CalculationType);

                            DateTime WorkMonth = DateTime.MinValue;
                            if (SubmissionEntity.Attributes.Contains("bpa_submissiondate"))
                                WorkMonth = SubmissionEntity.GetAttributeValue<DateTime>("bpa_submissiondate");
                            WorkMonth = new DateTime(WorkMonth.Year, WorkMonth.Month, WorkMonth.Day);
                            tracingService.Trace("Inside WorkMonth - " + WorkMonth.ToShortDateString());

                            EntityReference plan = SubmissionEntity.Contains("bpa_plan") ? SubmissionEntity.GetAttributeValue<EntityReference>("bpa_plan") : null;
                            bool isPensionPlan = false;
                            OptionSetValue pensionPlanType = new OptionSetValue(0);
                            OptionSetValue pensionBenefitCalculationType = new OptionSetValue(0);

                            decimal benefitRate = 1;
                            if (plan != null)
                            {
                                // Fetch Pension Benefit Rate and Plan
                                Entity planInfoWithBenefitRate = PlanHelper.FetchPlanInfoWithPensionBenfitRate(service, tracingService, plan.Id, WorkMonth);
                                if (planInfoWithBenefitRate != null)
                                {
                                    isPensionPlan = planInfoWithBenefitRate.GetAttributeValue<bool>("bpa_pensionplan");
                                    pensionPlanType = planInfoWithBenefitRate.Contains("bpa_pensionplantype") ?
                                        planInfoWithBenefitRate.GetAttributeValue<OptionSetValue>("bpa_pensionplantype") : new OptionSetValue(0);

                                    benefitRate = planInfoWithBenefitRate.Contains("bpa_planpensionbenefitrate1.bpa_dollaramountrate") ?
                                        (decimal)planInfoWithBenefitRate.GetAttributeValue<AliasedValue>("bpa_planpensionbenefitrate1.bpa_dollaramountrate").Value : 0;

                                    pensionBenefitCalculationType = planInfoWithBenefitRate.Contains("bpa_planpensionbenefitrate1.bpa_calculationtype") ?
                                        (OptionSetValue)planInfoWithBenefitRate.GetAttributeValue<AliasedValue>("bpa_planpensionbenefitrate1.bpa_calculationtype").Value : new OptionSetValue(0);
                                    if (pensionPlanType.Value == (int)PensionPlanType.DefinedContribution)
                                        benefitRate = 0; // Task# 1292 - For DC plans the Accrued Benefit should be populated with $0
                                }

                            }

                            #region ------------- Employer Regular Submission ---------------------

                            //FETCH All RATES
                            List<Rate> rates11 = RateHelper.GetRatesByEmployerIdUsingFetchXml(service, tracingService, Employer.Id, WorkMonth);

                            bool IsLateORMultipleSubmission = false;
                            DateTime _currentWorkMonth = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1).AddMonths(-1);
                            int currentmonthsubmission = SubmissionHelper.GetSubmissionCountForMonth(service, tracingService, Employer.Id, WorkMonth);

                            if ((_currentWorkMonth > WorkMonth) || (currentmonthsubmission > 1))
                            {
                                IsLateORMultipleSubmission = true;
                            }


                            // Need to Count number of employee which has not 0
                            int cntSubmissionDetailRecords = 0;
                            foreach (Entity submissiondetail in SubmissionDetails.Entities)
                            {
                                tracingService.Trace("Inside SubmissionDetail");

                                #region ------ Fetch all values from Submission Detail --------------------

                                EntityReference Submission = null, Contact = null, Employment = null, LabourRole = null, Agreement = null, owner = null;

                                if (submissiondetail.Contains("bpa_submissionid"))
                                {
                                    Submission = submissiondetail.GetAttributeValue<EntityReference>("bpa_submissionid");
                                    tracingService.Trace("Inside SubmissionId - " + Submission.Id);
                                }

                                if (submissiondetail.Contains("bpa_employmentid"))
                                {
                                    Employment = submissiondetail.GetAttributeValue<EntityReference>("bpa_employmentid");
                                    tracingService.Trace("Inside EmploymentId - " + Employment.Id);
                                }

                                if (submissiondetail.Contains("bpa_memberplanid"))
                                {
                                    Contact = submissiondetail.GetAttributeValue<EntityReference>("bpa_memberplanid");
                                    tracingService.Trace("Inside ContactID - " + Contact.Id);
                                }

                                if (submissiondetail.Contains("bpa_labourroleid"))
                                {
                                    LabourRole = submissiondetail.GetAttributeValue<EntityReference>("bpa_labourroleid");
                                    tracingService.Trace("Inside LabourRoleID - " + LabourRole.Id);
                                }

                                // set the owner of transaction
                                if (submissiondetail.Contains("ownerid"))
                                {
                                    owner = submissiondetail.GetAttributeValue<EntityReference>("ownerid");
                                    tracingService.Trace("Inside OWNER - " + owner.Id);
                                }

                                decimal icihours = 0, nonicihours = 0, totalhours = 0, hoursEarned = 0, grosswages = 0, dollars = 0;
                                if (submissiondetail.Contains("bpa_icihours"))
                                    icihours = submissiondetail.GetAttributeValue<decimal>("bpa_icihours");
                                tracingService.Trace("Inside icihours - " + icihours);

                                if (submissiondetail.Contains("bpa_nonicihours"))
                                    nonicihours = submissiondetail.GetAttributeValue<decimal>("bpa_nonicihours");
                                tracingService.Trace("Inside nonicihours - " + nonicihours);

                                // for Local 506: we added hours Earned so "total hours" become "Hours Worked"
                                if (submissiondetail.Contains("bpa_totalhours"))
                                    totalhours = submissiondetail.GetAttributeValue<decimal>("bpa_totalhours");
                                tracingService.Trace("Inside totalhours - " + totalhours);

                                if (submissiondetail.Contains("bpa_hoursearned"))
                                    hoursEarned = submissiondetail.GetAttributeValue<decimal>("bpa_hoursearned");
                                tracingService.Trace("Inside hoursEarned - " + hoursEarned);


                                if (submissiondetail.Contains("bpa_grosswages") &&
                                    submissiondetail.GetAttributeValue<Money>("bpa_grosswages") != null)
                                    grosswages = submissiondetail.GetAttributeValue<Money>("bpa_grosswages").Value;
                                tracingService.Trace("Inside grosswages - " + grosswages);

                                if (submissiondetail.Contains("bpa_dollars") &&
                                    submissiondetail.GetAttributeValue<Money>("bpa_dollars") != null)
                                    dollars = submissiondetail.GetAttributeValue<Money>("bpa_dollars").Value;
                                tracingService.Trace("Inside dollars - " + dollars);

                                OptionSetValue FlatRateRule = null;
                                OptionSetValue tmpFlatRateRule = null;
                                if (submissiondetail.Contains("bpa_flatraterule"))
                                    tmpFlatRateRule = submissiondetail.GetAttributeValue<OptionSetValue>("bpa_flatraterule");
                                
                                if (submissiondetail.Contains("bpa_agreementid"))
                                {
                                    Agreement = submissiondetail.GetAttributeValue<EntityReference>("bpa_agreementid");
                                    tracingService.Trace("Agreement " + Agreement.Name);
                                }

                                decimal pensionCredited = 0, pensionMemberRequired = 0, pensionMemberVoluntary = 0;
                                if (submissiondetail.Contains("bpa_pensioncredited"))
                                    pensionCredited = submissiondetail.GetAttributeValue<decimal>("bpa_pensioncredited");
                                tracingService.Trace("Inside Pension Credited - " + pensionCredited);
                                if (submissiondetail.Contains("bpa_pensionmemberrequired") &&
                                            submissiondetail.GetAttributeValue<Money>("bpa_pensionmemberrequired") != null)
                                    pensionMemberRequired = submissiondetail.GetAttributeValue<Money>("bpa_pensionmemberrequired").Value;
                                tracingService.Trace("Inside Pension Member Required - " + pensionMemberRequired);
                                if (submissiondetail.Contains("bpa_pensionmembervoluntary") &&
                                            submissiondetail.GetAttributeValue<Money>("bpa_pensionmembervoluntary") != null)
                                    pensionMemberVoluntary = submissiondetail.GetAttributeValue<Money>("bpa_pensionmembervoluntary").Value;
                                tracingService.Trace("Inside Pension Credited - " + pensionMemberVoluntary);

                                // Get member Eligibility Category
                                EntityReference mpEligibilityCategory = submissiondetail.Contains("bpa_memberplan1.bpa_eligibilitycategoryid") ?
                                    (EntityReference)submissiondetail.GetAttributeValue<AliasedValue>("bpa_memberplan1.bpa_eligibilitycategoryid").Value : null;


                                if ((totalhours == 0) && (hoursEarned == 0) && (grosswages == 0) && (dollars == 0) && (tmpFlatRateRule == null))
                                    FlatRateRule = new OptionSetValue((int)BpaSubmissionDetailBpaFlatRateRule.Ignore);
                                else
                                {
                                    if (tmpFlatRateRule == null)
                                        FlatRateRule = new OptionSetValue((int)BpaSubmissionDetailBpaFlatRateRule.Charge);
                                    else
                                        FlatRateRule = tmpFlatRateRule;
                                }

                                #endregion

                                #region --- Create List with Employeee Submission Detail -----
                                if ((LabourRole == null) || (WorkMonth == DateTime.MinValue))
                                    throw new InvalidPluginExecutionException(OperationStatus.Failed, $@"Either Job Classification or Work Month are null for Member '{Contact.Name}'. Please verified Data.");

                                #endregion

                                tracingService.Trace("Before Inside If - 2");
                                if (((totalhours != 0) || (hoursEarned != 0) || (grosswages != 0) || (dollars != 0)
                                        || (pensionCredited != 0) || (pensionMemberRequired != 0) || (pensionMemberVoluntary != 0)
                                        || (SubSector_CalculationType == (int)BpaSubSectorBpaCalculationType.Flat)
                                        || (SubSector_CalculationType == (int)BpaSubSectorBpaCalculationType.Corporate)) ||
                                    ((totalhours == 0) && (hoursEarned == 0) && (grosswages == 0) && (dollars == 0)
                                        && (pensionCredited == 0) && (pensionMemberRequired == 0) && (pensionMemberVoluntary == 0)))

                                    {
                                    tracingService.Trace("Inside If - 2");
                                    cntSubmissionDetailRecords++;

                                    #region ----------------- THIS IS THE LOGIC FOR TRANSACTIONS ----------------------------------

                                    tracingService.Trace("BEfore Fetch VacationFundId");
                                    Entity VacationFund = defaultFundList.Where(x =>
                                               x.GetAttributeValue<OptionSetValue>("bpa_fundtype").Value == (int)BpaFundbpaFundType.VacationPay).FirstOrDefault();
                                    Guid VacationFundId = Guid.Empty;
                                    if (VacationFund != null)
                                        VacationFundId = VacationFund.Id;
                                    tracingService.Trace("FETCH VacationFundId - " + VacationFundId);

                                    List<Rate>  rates = rates11.Where(r => r.LabourId.Value == LabourRole.Id).ToList();
                                    tracingService.Trace("Inside rates Count - " + rates.Count);

                                    #region ---------- Rate loop and create Transaction  -----------
                                    // Loop all rate 
                                    foreach (Rate rateT in rates)
                                    {
                                        tracingService.Trace("Rate Fund Id - " + rateT.FundId);
                                        tracingService.Trace("Rate Value - " + rateT.RateValue);
                                        tracingService.Trace("Rate Fund Type - " + rateT.FundType);
                                        decimal dollarhours = 0;

                                        if (SubSector_CalculationType == (int)BpaSubSectorBpaCalculationType.DollarBased ||
                                            SubSector_CalculationType == (int)BpaSubSectorBpaCalculationType.Piecework ||
                                            SubSector_CalculationType == (int)BpaSubSectorBpaCalculationType.Corporate)
                                        {
                                            dollarhours = Convert.ToDecimal(dollars);
                                            tracingService.Trace("1. Dollar  - " + dollarhours);
                                        }
                                        else if (SubSector_CalculationType == (int)BpaSubSectorBpaCalculationType.Flat)
                                        {
                                            if (rateT.RateValue != null)
                                                dollarhours = Convert.ToDecimal(totalhours);
                                            tracingService.Trace("2. Dollar  - " + dollarhours);
                                        }
                                        else
                                        {
                                            if (rateT.RateValue != null)
                                            {
                                                if (rateT.CalculationType.Value == (int)RateCalculationType.HourEarned) // Added for LOCAL 506
                                                    dollarhours = Convert.ToDecimal(hoursEarned);
                                                else
                                                    dollarhours = Convert.ToDecimal(totalhours);
                                            }
                                            tracingService.Trace("3. Dollar  - " + dollarhours);
                                        }

                                        if ((rateT.FundType.Value == (int)BpaFundbpaFundType.Welfare) && rateT.IsDefaultFund)
                                        {
                                            tracingService.Trace("Inside DefaultFund - " + rateT.FundId);
                                            if (SubSector_CalculationType == (int)BpaSubSectorBpaCalculationType.Flat)
                                            {
                                                if (rateT.RateValue != null)
                                                    dollarhours = 1; // Convert.ToDecimal(rateT.RateValue.Value);
                                            }
                                            
                                            #region ------------------ Employer Contribution / Late Transfer In Contribution ------------------------ 

                                            bool HasTransferIn = false;
                                            DateTime TransferInWorkMonth = DateTime.MinValue;
                                            EntityReference CurrentEligibilityCategory = null;

                                            //Following If condition added because for the Late Submission we have to used that month Eligibility Category not Current Eligibility Category
                                            if (IsLateORMultipleSubmission)
                                            {
                                                //Get the Same Eligibility Category if member already has employer contribution
                                                //if (currentmonthsubmission > 1)
                                                //{
                                                    Guid _transId = TransactionHelper.FetchEligibilityTransactionForWorkMonth(service, WorkMonth, Contact.Id);
                                                    if (_transId != Guid.Empty)
                                                        CurrentEligibilityCategory = TransactionHelper.FetchEligiblityCategorty(service, _transId);
                                                //}
                                                

                                                //CHECKING IS MEMBER HAS TRANSFER IN STATUS
                                                if (_currentWorkMonth > WorkMonth)
                                                {
                                                    Entity Entity_TransferIn = TransactionHelper.IsMemberTranferIn(service, Employer.Id, WorkMonth, (int)BpaTransactionbpaTransactionType.TransferIn);

                                                    if ((Entity_TransferIn != null) && (Entity_TransferIn.Attributes.Contains("bpa_transactiondate")))
                                                    {
                                                        TransferInWorkMonth = Entity_TransferIn.GetAttributeValue<DateTime>("bpa_transactiondate");
                                                        if ((TransferInWorkMonth != DateTime.MinValue) && (new DateTime(TransferInWorkMonth.Year, TransferInWorkMonth.Month, 1) > WorkMonth))
                                                            HasTransferIn = true;
                                                        else
                                                            HasTransferIn = false;
                                                    }

                                                }
                                            }

                                            //FETCH ELIGIBILITY CATEGORY - if nothing find
                                            if (CurrentEligibilityCategory == null)
                                                CurrentEligibilityCategory = TransactionHelper.FetchCurrentEligibilityCategory(service, tracingService, Contact.Id, WorkMonth); //  .GetCurrentEligibilityCategory(employee.Id);

                                            // GET and SET Eligibility Category and Eligibility Category Detail 
                                            Entity EC = eligibilityCategories.Entities.Where(x => x.Id == mpEligibilityCategory.Id).FirstOrDefault();

                                            Entity ECD = null;
                                            if (!isOnlyPensionPlan)
                                            {
                                                ECD = MemberPlanRetroprocessingHelper_Optimized.FetchEligibilityDetail(tracingService, eligibilityCategoryDetails, EC.Id, WorkMonth);

                                                //Member Eligibility Category Null So need to skip
                                                if (EC == null || ECD == null)
                                                    throw new InvalidPluginExecutionException(OperationStatus.Failed, $@"Member Eligibility Category or Eligibility Category Detail is null for '{Contact.Name}'");
                                            }
                                            int eligibilityOffset = EC.Contains("bpa_eligibilityoffsetmonths") ? EC.GetAttributeValue<int>("bpa_eligibilityoffsetmonths") : 0;
                                            DateTime BenefitMonth = DateTime.MinValue;

                                            if (WorkMonth != DateTime.MinValue)
                                            {
                                                BenefitMonth = WorkMonth.AddMonths(eligibilityOffset);
                                                BenefitMonth = new DateTime(BenefitMonth.Year, BenefitMonth.Month, 1);
                                            }

                                            if (((totalhours != 0) || (hoursEarned != 0) || (grosswages != 0) || (dollars != 0)
                                                    || (SubSector_CalculationType == (int)BpaSubSectorBpaCalculationType.Flat)
                                                    || (SubSector_CalculationType == (int)BpaSubSectorBpaCalculationType.Corporate)) ||
                                                ((totalhours == 0) && (hoursEarned == 0) && (grosswages == 0) && (dollars == 0)) && 
                                                    ((FlatRateRule.Value == (int)BpaSubmissionDetailBpaFlatRateRule.Remove) ||
                                                     (FlatRateRule.Value == (int)BpaSubmissionDetailBpaFlatRateRule.Charge))
                                               )
                                            {
                                                if ((SubmissionType.Value == (int)SubmissionSubmissionType.Member)
                                                    || (SubmissionType.Value == (int)SubmissionSubmissionType.MemberSelfPayRefund))
                                                {
                                                    #region ---------------------- Submission : Member Self-Pay ---------------------

                                                    int MaxConsecutiveMonthsSelfPay = EC.GetAttributeValue<int>("bpa_maxconsecutivemonthsselfpay");

                                                    EntityCollection NotInBenefitTransactions = TransactionHelper.FetchNotInBenefitTransaction(service, WorkMonth, DateTime.MinValue, Contact.Id);
                                                    EntityReference CurrentEligibility = null;
                                                    Entity transaction = null;
                                                    if (NotInBenefitTransactions != null && NotInBenefitTransactions.Entities.Count > 0)
                                                    {
                                                        transaction = NotInBenefitTransactions.Entities[0];
                                                        if (transaction.Contains("bpa_currenteligibilitycategory"))
                                                            CurrentEligibility = transaction.GetAttributeValue<EntityReference>("bpa_currenteligibilitycategory");
                                                    }
                                                    else
                                                    {
                                                        CurrentEligibility = CurrentEligibilityCategory;
                                                    }

                                                    //Self-Pay Transaction
                                                    TransactionHelper.CreateContributionTransaction(serviceAdmin, tracingService, Submission.Id,
                                                        Contact.Id, WorkMonth, BenefitMonth, totalhours, new Money(grosswages), LabourRole.Id,
                                                        new Money(dollarhours * rateT.RateValue.Value), defaultFundRef.Id, 0,
                                                        (int)BpaTransactionbpaTransactionCategory.Contributions,
                                                        (int)BpaTransactionbpaTransactionType.SelfPayDeposit, CurrentEligibility, 0, 0, 0, null,
                                                        null, true, Agreement, Guid.Empty, (int)TransactionStatus.Pending, owner, submissiondetail.Id, totalhours, hoursEarned);

                                                    //Trigger Roll ups
                                                    PluginHelper.TriggerRollup(service, tracingService, PluginHelper.BpaMemberplan, Contact.Id, "bpa_selfpaybank");

                                                    #endregion
                                                }
                                                else if ((SubmissionType.Value == (int)SubmissionSubmissionType.EmployerRegular) ||
                                                         (SubmissionType.Value == (int)SubmissionSubmissionType.EmployerAdjustment))
                                                {
                                                    #region ---------------- Submission: Employer ----------------
                                                    bool isIgnore = false;
                                                    if (rateT.CalculationType == (int)RateCalculationType.FlatRate)
                                                    {
                                                        if (FlatRateRule.Value == (int)BpaSubmissionDetailBpaFlatRateRule.Charge)
                                                            dollarhours = dollarhours * 1;
                                                        else if (FlatRateRule.Value == (int)BpaSubmissionDetailBpaFlatRateRule.Ignore)
                                                        {
                                                            dollarhours = 0;
                                                            isIgnore = true;
                                                        }
                                                        if (FlatRateRule.Value == (int)BpaSubmissionDetailBpaFlatRateRule.Remove)
                                                            dollarhours = dollarhours * -1;
                                                    }

                                                    tracingService.Trace("Inside SubmissionType.EmployerRegular");
                                                    int currentEligibility = ContactHelper.FetchCurrentEligibility(service, tracingService, Contact.Id);
                                                    Guid EmployerContributionId = Guid.Empty;

                                                    if (!isIgnore)
                                                    {
                                                        tracingService.Trace("BEFORE ELSE EmployerContributionId");

                                                        EmployerContributionId = TransactionHelper.CreateContributionTransaction(serviceAdmin, tracingService,
                                                                Submission.Id, Contact.Id, WorkMonth, BenefitMonth, dollarhours, new Money(grosswages), LabourRole.Id,
                                                                new Money(dollarhours * rateT.RateValue.Value), defaultFundRef.Id, rateT.RateValue.Value,
                                                                (int)BpaTransactionbpaTransactionCategory.Contributions,
                                                                (int)BpaTransactionbpaTransactionType.EmployerContribution,
                                                                CurrentEligibilityCategory, icihours, nonicihours, 0, null, null, true, Agreement,
                                                                Guid.Empty, (int)TransactionStatus.Pending, owner, submissiondetail.Id, totalhours, hoursEarned);
                                                        tracingService.Trace("AFTER ELSE EmployerContributionId - " + EmployerContributionId);

                                                        #region ------------ MEMBER CURRENT ELIGIBILITY = 'Transfer OUT'
                                                        //1. Create Adjustment
                                                        //2. Create Transfer
                                                        //3. Create Reciprocal Transfer

                                                        if (currentEligibility == (int)ContactbpaCurrentEligibility.TransferOut)
                                                        {
                                                            tracingService.Trace("Inside MEMBER CURRENT ELIGIBILITY = 'Transfer OUT' CHECKING");
                                                            //Fetch Home Local from Member Plan 
                                                            EntityReference HomeLocal = submissiondetail.Contains("bpa_memberplan1.bpa_unionhomelocalid") ?
                                                                (EntityReference)submissiondetail.GetAttributeValue<AliasedValue>("bpa_memberplan1.bpa_unionhomelocalid").Value : null;
                                                            tracingService.Trace("Before creating MEmber Adjustment when Member Transfer OUT Status");

                                                            //Get ending Balance
                                                            Dictionary<string, decimal> endingBalances1 = new Dictionary<string, decimal>();

                                                            endingBalances1.Add("hours", (dollarhours));
                                                            endingBalances1.Add("dollarhours", (dollarhours * rateT.RateValue.Value));

                                                            DateTime lastDate = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddMonths(-1);

                                                            //Create Adjustment
                                                            Guid memberAdjustmentId = Guid.Empty;
                                                            if ((endingBalances1.ContainsKey("hours") && endingBalances1["hours"] > 0) || (endingBalances1.ContainsKey("dollarhours") && endingBalances1["dollarhours"] > 0))
                                                            {
                                                                if (plan == null)
                                                                    plan = SubmissionEntity.Contains("bpa_plan") ? SubmissionEntity.GetAttributeValue<EntityReference>("bpa_plan") : null;

                                                                memberAdjustmentId = MemberAdjustmentHelper.CrateMemberAdjustmentTypeTransferINOUT(service, tracingService, Contact, plan,
                                                                    (int)MemberPlanBenefitAdjustmentType.TransferReciprocalOut, "Late Employer Contribution received", HomeLocal, lastDate,
                                                                    0, endingBalances1, defaultCurrency);
                                                            }

                                                            //CREATE Transfer Out transaction
                                                            Guid reciprocalId = TransactionHelper.CreateTransactionReciprocal(serviceAdmin, tracingService, Contact.Id, WorkMonth,
                                                                (int)BpaTransactionbpaTransactionType.TransferOut, HomeLocal.Id, (int)BpaTransactionbpaTransactionCategory.Eligibility,
                                                                CurrentEligibilityCategory, memberAdjustmentId, BenefitMonth, endingBalances1, Agreement, SubmissionEntity.Id, false, (int)TransactionStatus.Pending); // Reciprocal - Transfer Out

                                                            //Update Ending balance from that month and onward
                                                            DateTime startDate = WorkMonth;
                                                            startDate = new DateTime(startDate.Year, startDate.Month, 1);
                                                            while (startDate <= lastDate)
                                                            {
                                                                //Fetch All Transactions which are eligibility status of that month and Update Ending Dollar and Ending Hours
                                                                EntityCollection transactions = TransactionHelper.FetchAllTransactionsByMonth(serviceAdmin, tracingService, Contact.Id, startDate);
                                                                if (transactions != null && transactions.Entities.Count > 0)
                                                                {
                                                                    Dictionary<string, decimal> endingBalances11 = TransactionHelper.FetchEndingBalanceWhenLateContribution(serviceAdmin, Contact.Id, startDate.AddDays(1));
                                                                    foreach (Entity tran in transactions.Entities)
                                                                    {
                                                                        //Update Transaction
                                                                        Entity t = new Entity(PluginHelper.BpaTransaction)
                                                                        {
                                                                            Id = tran.Id
                                                                        };
                                                                        if (endingBalances11.ContainsKey("hours"))
                                                                            t["bpa_endinghourbalance"] = endingBalances11["hours"];
                                                                        if (endingBalances11.ContainsKey("dollarhours"))
                                                                            t["bpa_endingdollarbalance"] = endingBalances11["dollarhours"];
                                                                        if (endingBalances11.ContainsKey("selfpaydollar"))
                                                                            t["bpa_endingselfpaybalance"] = new Money(endingBalances11["selfpaydollar"]);

                                                                        if (endingBalances1.ContainsKey("secondarydollar"))
                                                                            t["bpa_secondarydollarbalance"] = new Money(endingBalances1["secondarydollar"]);

                                                                        serviceAdmin.Update(t);
                                                                    }
                                                                }
                                                                startDate = startDate.AddMonths(1);
                                                            }
                                                        }

                                                        #endregion

                                                        #region ------------------------ MEMBER CURRENT ELIGIBILITY = 'INACTIVE' ---------------
                                                        tracingService.Trace("BEFORE INACTIVE 112111- ");
                                                        if (currentEligibility == (int)ContactbpaCurrentEligibility.Inactive)
                                                        {
                                                            tracingService.Trace("In side checking INACTIVE");
                                                            DateTime _inactiveWorkMonth = TransactionHelper.FetchInactiveWorkMonth(serviceAdmin, Contact.Id,
                                                                    (int)BpaTransactionbpaTransactionType.Inactive, WorkMonth);
                                                            DateTime inactiveWorkMonth = new DateTime(_inactiveWorkMonth.Year, _inactiveWorkMonth.Month, 1);
                                                            tracingService.Trace("In side checking INACTIVE = " +
                                                                                 inactiveWorkMonth.ToShortDateString());

                                                            if ((WorkMonth < inactiveWorkMonth) && (inactiveWorkMonth != DateTime.MinValue))
                                                            {
                                                                tracingService.Trace("In side checking WorkMonth");
                                                                EntityCollection existingTransactionCollection =
                                                                    TransactionHelper.FetchInactiveTransactions(serviceAdmin, Contact.Id, WorkMonth);
                                                                if (existingTransactionCollection != null && existingTransactionCollection.Entities.Count > 0)
                                                                {
                                                                    tracingService.Trace("In side checking INACTIVE " +
                                                                                         existingTransactionCollection.Entities.Count);

                                                                    foreach (Entity e in existingTransactionCollection.Entities)
                                                                    {
                                                                        serviceAdmin.Delete(PluginHelper.BpaTransaction, e.Id);
                                                                    }
                                                                }
                                                            }
                                                            if ((WorkMonth >= inactiveWorkMonth) && (inactiveWorkMonth != DateTime.MinValue))
                                                            {
                                                                tracingService.Trace("else INACTIVE " + WorkMonth.ToShortDateString());
                                                                ContactHelper.SetCurrentEligibility(service, tracingService,
                                                                    Contact.Id, (int)ContactbpaCurrentEligibility.NewMember, DateTime.MinValue);
                                                            }
                                                        }

                                                        #endregion

                                                    }
                                                    #endregion 
                                                } // end of else if ((SubmissionType.Value == (int)SubmissionSubmissionType.EmployerRegular) || (SubmissionType.Value == (int)SubmissionSubmissionType.EmployerAdjustment))
                                            }

                                            #endregion
                                        }
                                        else if ((rateT.FundType.Value == (int)BpaFundbpaFundType.VacationPay) && rateT.IsDefaultFund)
                                        {
                                            #region ------------- Vacation Fund logic ----------------

                                            tracingService.Trace("in vacation");
                                            decimal? vacationRate = null;
                                            vacationRate = rateT.RateValue.Value;

                                            if (vacationRate != null && vacationRate != 0 && grosswages != 0)
                                            {
                                                decimal VacationAmount = Convert.ToDecimal(grosswages) * vacationRate.Value;
                                                bool isIgnore = false;
                                                if (rateT.CalculationType == (int)RateCalculationType.FlatRate)
                                                {
                                                    if (FlatRateRule.Value == (int)BpaSubmissionDetailBpaFlatRateRule.Charge)
                                                        VacationAmount = VacationAmount * 1;
                                                    else if (FlatRateRule.Value == (int)BpaSubmissionDetailBpaFlatRateRule.Ignore)
                                                    {
                                                        VacationAmount = 0;
                                                        isIgnore = true;
                                                    }
                                                    if (FlatRateRule.Value == (int)BpaSubmissionDetailBpaFlatRateRule.Remove)
                                                        VacationAmount = VacationAmount * -1;
                                                }
                                                if (!isIgnore)
                                                {
                                                    Guid VacationContributionId =
                                                        TransactionHelper.CreateContributionTransaction(serviceAdmin, tracingService,
                                                            Submission.Id, Contact.Id, WorkMonth, DateTime.MinValue,
                                                            dollarhours, new Money(grosswages), LabourRole.Id,
                                                            new Money(dollarhours * vacationRate.Value), VacationFundId,
                                                            vacationRate.Value,
                                                            (int)BpaTransactionbpaTransactionCategory.Contributions,
                                                            (int)BpaTransactionbpaTransactionType.VacationContribution,
                                                            null, 0, 0, VacationAmount, null, null, false, Agreement, Guid.Empty,
                                                            (int)TransactionStatus.Pending, owner, submissiondetail.Id, totalhours, hoursEarned);
                                                }
                                            }

                                            #endregion
                                        }
                                        else if ((rateT.FundType.Value == (int)BpaFundbpaFundType.Pension))
                                        {
                                            #region ---------------- PENSION TRANSACTION -------------------
                                            int transactionType = 0, pensionTransactionCategory = 0;
                                            decimal actualHours = 0, actualDollar = 0, adjustedHours = 0, accruedBenefitAmount = 0;
                                            decimal tmpGrosswages = grosswages;
                                            decimal accrualMultiplier = 0, pensionableHours = 0, pensionableContribution = 0 ;

                                            // Set Value for just for Pension Purpose
                                            actualHours = dollarhours; actualDollar = dollars;


                                            if ((rateT.RateValue != null) && (rateT.CalculationType.Value != (int)RateCalculationType.Tax))
                                            {
                                                tracingService.Trace("rateT.RateValue = " + rateT.RateValue.Value);
                                                tracingService.Trace("rateT.CalculationType = " + rateT.CalculationType.Value);
                                                bool isIgnore = false;
                                                bool calculateAccuredBenefit = false;
                                                if (rateT.CalculationType == (int)RateCalculationType.PensionMemberRequiredDollar)
                                                {
                                                    transactionType = (int)PensionTransactionType.RequiredContribution;
                                                    pensionTransactionCategory = (int)PensionTransactionCategory.Employee;
                                                    tmpGrosswages = 0;
                                                    accrualMultiplier = rateT.HourMultiplier.Value;

                                                    dollarhours = pensionMemberRequired * rateT.RateValue.Value; //Contribution Amount
                                                    pensionableHours = 0;
                                                    pensionableContribution = dollarhours * accrualMultiplier;
                                                }
                                                else if (rateT.CalculationType == (int)RateCalculationType.PensionMemberVoluntaryDollar)
                                                {
                                                    transactionType = (int)PensionTransactionType.VoluntaryContribution;
                                                    pensionTransactionCategory = (int)PensionTransactionCategory.Employee;
                                                    tmpGrosswages = 0;
                                                    accrualMultiplier = rateT.HourMultiplier.Value;

                                                    dollarhours = pensionMemberVoluntary * rateT.RateValue.Value; //Contribution Amount
                                                    pensionableHours = 0;
                                                    pensionableContribution = dollarhours * accrualMultiplier;
                                                }
                                                else if (rateT.CalculationType == (int)RateCalculationType.PercentageGrossWages)
                                                {
                                                    transactionType = (int)PensionTransactionType.PensionContribution;
                                                    pensionTransactionCategory = (int)PensionTransactionCategory.Employer;
                                                    dollarhours = tmpGrosswages * rateT.RateValue.Value;  //Contribution Amount
                                                    accrualMultiplier = rateT.HourMultiplier.Value;
                                                    pensionableHours = 0;
                                                    pensionableContribution = dollarhours;
                                                }
                                                else if ((rateT.CalculationType == (int)RateCalculationType.Hourly)) 
                                                {
                                                    transactionType = (int)PensionTransactionType.PensionContribution;
                                                    pensionTransactionCategory = (int)PensionTransactionCategory.Employer;
                                                    dollarhours = actualHours * rateT.RateValue.Value;//Contribution Amount
                                                    accrualMultiplier = rateT.HourMultiplier.Value;
                                                    pensionableHours = actualHours * accrualMultiplier;
                                                    pensionableContribution = pensionableHours * rateT.RateValue.Value;
                                                }
                                                else if ((rateT.CalculationType == (int)RateCalculationType.HourEarned)) 
                                                {
                                                    transactionType = (int)PensionTransactionType.PensionContribution;
                                                    pensionTransactionCategory = (int)PensionTransactionCategory.Employer;
                                                    dollarhours = actualHours * rateT.RateValue.Value;//Contribution Amount
                                                    accrualMultiplier = rateT.HourMultiplier.Value;
                                                    pensionableHours = actualHours * rateT.HourMultiplier.Value;
                                                    pensionableContribution = pensionableHours * rateT.RateValue.Value;
                                                }
                                                else if (rateT.CalculationType == (int)RateCalculationType.FlatRate)
                                                {
                                                    if ((SubmissionType.Value == (int)SubmissionSubmissionType.EmployerAdjustment) ||
                                                        (SubmissionType.Value == (int)SubmissionSubmissionType.EmployerRegular))
                                                    {
                                                        if (FlatRateRule.Value == (int)BpaSubmissionDetailBpaFlatRateRule.Charge)
                                                            dollarhours = rateT.RateValue.Value * 1;
                                                        else if (FlatRateRule.Value == (int)BpaSubmissionDetailBpaFlatRateRule.Ignore)
                                                            dollarhours = 0;
                                                        else if (FlatRateRule.Value == (int)BpaSubmissionDetailBpaFlatRateRule.Remove)
                                                            dollarhours = rateT.RateValue.Value * -1;
                                                    }
                                                    else
                                                        dollarhours = rateT.RateValue.Value;

                                                    transactionType = (int)PensionTransactionType.PensionContribution;
                                                    pensionTransactionCategory = (int)PensionTransactionCategory.Employer;
                                                    accrualMultiplier = rateT.HourMultiplier.Value;
                                                    pensionableHours = 0;
                                                    pensionableContribution = dollarhours;
                                                }
                                                else
                                                {
                                                    // Dollar Base
                                                    transactionType = (int)PensionTransactionType.PensionContribution;
                                                    pensionTransactionCategory = (int)PensionTransactionCategory.Employer;
                                                    dollarhours = dollars * rateT.RateValue.Value;
                                                    accrualMultiplier = rateT.HourMultiplier.Value;
                                                    pensionableHours = 0;
                                                    pensionableContribution = dollarhours;
                                                }

                                                // Create Transaction 
                                                if (!isIgnore)
                                                {
                                                    // Check fund type is Pesion ?
                                                    Guid periodOfServiceId = Guid.Empty;
                                                    tracingService.Trace("dollarhours = " + dollarhours);
                                                    tracingService.Trace("Adjusted Hours = " + adjustedHours);

                                                    if (isPensionPlan)
                                                    {
                                                        if (calculateAccuredBenefit)
                                                        {
                                                            if (pensionBenefitCalculationType.Value == (int)PensionBenefitCalculationType.Hour)
                                                                accruedBenefitAmount = benefitRate * adjustedHours;
                                                            else
                                                                accruedBenefitAmount = benefitRate * dollarhours;
                                                        }
                                                        decimal _actualHours = 0;

                                                        if ((transactionType == (int)PensionTransactionType.RequiredContribution) || (transactionType == (int)PensionTransactionType.VoluntaryContribution))
                                                        {
                                                            _actualHours = 0;
                                                        }
                                                        else
                                                        {
                                                            _actualHours = actualHours;
                                                        }

                                                        
                                                        PensionTransactionHelper.Create(serviceAdmin, tracingService, pensionTransactionCategory, transactionType,
                                                            Contact, periodOfServiceId, rateT.FundId.Value, defaultCurrency,
                                                            Submission, null, WorkMonth, _actualHours, rateT.RateValue.Value,
                                                            new Money(dollarhours), new Money(tmpGrosswages), accrualMultiplier, pensionableHours, pensionableContribution);
                                                    }

                                                    TransactionHelper.CreateContributionTransaction(serviceAdmin, tracingService,
                                                            Submission.Id, Contact.Id, WorkMonth, DateTime.MinValue,
                                                            adjustedHours, new Money(tmpGrosswages), LabourRole.Id,
                                                            new Money(dollarhours), rateT.FundId.Value, rateT.RateValue.Value,
                                                            (int)BpaTransactionbpaTransactionCategory.Contributions,
                                                            (int)BpaTransactionbpaTransactionType.PensionContribution,
                                                            null, icihours, nonicihours, 0, null, null, false, Agreement, periodOfServiceId,
                                                            (int)TransactionStatus.Pending, owner, submissiondetail.Id, totalhours, hoursEarned);
                                                }
                                            }

                                            #endregion
                                        }
                                        else
                                        {
                                            #region ----------- Else logic --------------------

                                            tracingService.Trace("in Else");
                                            if (rateT.RateValue != null)
                                            {
                                                tracingService.Trace("rateT.RateValue = " + rateT.RateValue.Value);
                                                tracingService.Trace("rateT.CalculationType = " + rateT.CalculationType.Value);
                                                if (rateT.CalculationType.Value != (int)RateCalculationType.Tax)
                                                {
                                                    bool isIgnore = false;

                                                    if (rateT.CalculationType == (int)RateCalculationType.FlatRate)
                                                    {
                                                        if ((SubmissionType.Value == (int)SubmissionSubmissionType.EmployerAdjustment) ||
                                                            (SubmissionType.Value == (int)SubmissionSubmissionType.EmployerRegular))
                                                        {
                                                            if (FlatRateRule.Value == (int)BpaSubmissionDetailBpaFlatRateRule.Charge)
                                                                dollarhours = 1;
                                                            else if (FlatRateRule.Value == (int)BpaSubmissionDetailBpaFlatRateRule.Ignore)
                                                            {
                                                                dollarhours = 0;
                                                                isIgnore = true;
                                                            }
                                                            if (FlatRateRule.Value == (int)BpaSubmissionDetailBpaFlatRateRule.Remove)
                                                                dollarhours = -1;
                                                        }
                                                        else
                                                            dollarhours = 1;
                                                    }
                                                    else if (rateT.CalculationType.Value == (int)RateCalculationType.PercentageGrossWages)
                                                    {
                                                        dollarhours = grosswages;
                                                    }
                                                    else
                                                    {

                                                        if ((totalhours == 0) && (hoursEarned == 0) && (grosswages == 0) && (dollars == 0) && (tmpFlatRateRule == null))
                                                            isIgnore = true;
                                                    }


                                                    if (!isIgnore)
                                                    {
                                                        // Check fund type is Pesion ?
                                                        int transactionType = 0;
                                                        Guid periodOfServiceId = Guid.Empty;

                                                        if (rateT.FundType.Value == (int)BpaFundbpaFundType.Pension)
                                                            transactionType = (int)BpaTransactionbpaTransactionType.PensionContribution;
                                                        else
                                                            transactionType = (int)BpaTransactionbpaTransactionType.EmployerContribution;

                                                        //Added this if condition because do not want to change anythhing as this is production issue
                                                        if (rateT.CalculationType.Value == (int)RateCalculationType.PercentageGrossWages)
                                                        {
                                                            tracingService.Trace("dollarhours = " + dollarhours);
                                                            TransactionHelper.CreateContributionTransaction(serviceAdmin, tracingService,
                                                                Submission.Id, Contact.Id, WorkMonth, DateTime.MinValue,
                                                                0, new Money(grosswages), LabourRole.Id,
                                                                new Money(rateT.RateValue.Value * dollarhours),
                                                                rateT.FundId.Value, rateT.RateValue.Value,
                                                                (int)BpaTransactionbpaTransactionCategory.Contributions,
                                                                transactionType, null, 0, 0, 0, null, null, false, Agreement, periodOfServiceId,
                                                                (int)TransactionStatus.Pending, owner, submissiondetail.Id, totalhours, hoursEarned);
                                                        }
                                                        else
                                                        {
                                                            tracingService.Trace("dollarhours = " + dollarhours);
                                                            TransactionHelper.CreateContributionTransaction(serviceAdmin, tracingService,
                                                                Submission.Id, Contact.Id, WorkMonth, DateTime.MinValue,
                                                                dollarhours, new Money(grosswages), LabourRole.Id,
                                                                new Money(rateT.RateValue.Value * dollarhours),
                                                                rateT.FundId.Value, rateT.RateValue.Value,
                                                                (int)BpaTransactionbpaTransactionCategory.Contributions,
                                                                transactionType, null, 0, 0, 0, null, null, false, Agreement, periodOfServiceId,
                                                                (int)TransactionStatus.Pending, owner, submissiondetail.Id, totalhours, hoursEarned);
                                                        }
                                                    }
                                                }
                                            }

                                            #endregion
                                        }

                                        tracingService.Trace("end of rate loop");
                                    }
                                    #endregion

                                    #endregion

                                    tracingService.Trace("Before  Last Work Month ");
                                }

                                tracingService.Trace("=================================");
                            } // end of foreach submission detail loop

                            #endregion
                        }


                        tracingService.Trace($@"SubmissionBusinessProcessFlowOnUpdatePost: COMPLETED Current Stage 'Deposit Pending' AND Previous Stage 'Verification'");

                        #endregion
                    }

                    #endregion
                }
                else if (Current_StageId == PluginHelper.SubmissionStageIdComplete)
                {
                    if (submissiontype.Value == (int)SubmissionSubmissionType.RetireeMonthlySubmission)
                        return;

                    #region ------------- COMPLETED -----------

                    if (Pre_StageId == PluginHelper.SubmissionStageIdDepositPending)
                    {
                        #region ------------- Previous Stage 'Deposit Pending' -----------------

                        tracingService.Trace($@"SubmissionBusinessProcessFlowOnUpdatePost: START Current Stage 'Completed' AND Previous Stage 'Deposit Pending'");
                        string submissionname = SubmissionEntity.GetAttributeValue<string>("bpa_name");
                        tracingService.Trace($@"============================= {submissionname} ===================================");

                        #region ------------------- COMPLETED --------------------

                        #region --------- UPDATE CONTRIBUTION AS 'INACTIVE' ------------
                        EntityCollection contributions = ContributionHelper.FetchAllContributionsBySubmissionId(serviceAdmin, SubmissionEntity.Id);
                        if (contributions != null && contributions.Entities.Count > 0)
                        {
                            if (executeMultiple)
                            {
                                ExecuteTransactionRequest requestContributions = new ExecuteTransactionRequest()
                                {
                                    // Create an empty organization request collection.
                                    Requests = new OrganizationRequestCollection(),
                                    ReturnResponses = true
                                };
                                foreach (Entity c in contributions.Entities)
                                {
                                    Entity contribution = new Entity(PluginHelper.BpaContribution)
                                    {
                                        Id = c.Id,
                                        ["bpa_posteddate"] = DateTime.Now
                                    };

                                    UpdateRequest ur = new UpdateRequest() { Target = contribution };
                                    requestContributions.Requests.Add(ur);
                                }
                                try
                                {
                                    ExecuteTransactionResponse responsePostingContribution = (ExecuteTransactionResponse)serviceAdmin.Execute(requestContributions);
                                }
                                catch { }
                            }
                            else
                            {
                                foreach (Entity c in contributions.Entities)
                                {
                                    Entity contribution = new Entity(PluginHelper.BpaContribution)
                                    {
                                        Id = c.Id,
                                        ["bpa_posteddate"] = DateTime.Now
                                    };

                                    UpdateRequest ur = new UpdateRequest() { Target = contribution };
                                    serviceAdmin.Execute(ur);
                                }
                            }
                                
                        }

                        #endregion


                        #region ----------- UPDATE TRANSACTION AS 'POSTED' ---------------
                        // Create the Transaction Request object to hold all of our changes in
                        ExecuteTransactionRequest requestPostedTransactions = null;

                        // Fetch All Transaction and update Posted Date and change status
                        EntityCollection transactions = TransactionHelper.FetchAllTransactionsBySubmissionId(serviceAdmin, tracingService, SubmissionEntity.Id);

                        // Fetch All Transaction for Pension
                        EntityCollection pensionTransactions = PensionTransactionHelper.FetchAllTransactionsBySubmissionId(serviceAdmin, tracingService, SubmissionEntity.Id);

                        if (executeMultiple)
                        {
                            tracingService.Trace("SUBMISSIONBUSINESSPROCESSFLOWONUPDATEPOST: Inside Execute Multiple Request = TRUE");
                            #region ------------Execute Multiple Request -------------------------
                            try
                            {
                                requestPostedTransactions = new ExecuteTransactionRequest()
                                {
                                    // Create an empty organization request collection.
                                    Requests = new OrganizationRequestCollection(),
                                    ReturnResponses = true
                                };

                                if (transactions != null && transactions.Entities.Count > 0)
                                {
                                    int tranCnt = 1;
                                    tracingService.Trace($@"Number of Transacations: {transactions.Entities.Count}");
                                    foreach (Entity t in transactions.Entities)
                                    {
                                        tracingService.Trace($@"{tranCnt.ToString()} - Transaction ID {t.Id}");
                                        Entity transaction = new Entity(PluginHelper.BpaTransaction)
                                        {
                                            Id = t.Id,
                                            ["statuscode"] = new OptionSetValue((int)TransactionStatus.Posted),
                                        };

                                        UpdateRequest ur = new UpdateRequest() { Target = transaction };
                                        requestPostedTransactions.Requests.Add(ur);
                                        tranCnt++;
                                    }
                                }

                                tracingService.Trace($@"SUBMISSIONBUSINESSPROCESSFLOWONUPDATEPOST: BENEFIT Transacations POSTED Completed.");

                                if (pensionTransactions != null && pensionTransactions.Entities.Count > 0)
                                {
                                    tracingService.Trace($@"Number of Pension Transacations: {pensionTransactions.Entities.Count}");
                                    int tranCnt = 1;
                                    foreach (Entity pt in pensionTransactions.Entities)
                                    {
                                        EntityReference transactionMemberPlan = pt.GetAttributeValue<EntityReference>("bpa_memberplanid");
                                        tracingService.Trace($@"{tranCnt.ToString()} - Pension Transaction ID {pt.Id}, Member Plan: {transactionMemberPlan.Name}");
                                        Entity ptransaction = new Entity(PluginHelper.BpaPensionTransaction)
                                        {
                                            Id = pt.Id,
                                            ["statuscode"] = new OptionSetValue((int)TransactionStatus.Posted),
                                        };
                                        UpdateRequest ur = new UpdateRequest() { Target = ptransaction };
                                        requestPostedTransactions.Requests.Add(ur);

                                        tranCnt++;
                                    }
                                }
                                tracingService.Trace($@"SUBMISSIONBUSINESSPROCESSFLOWONUPDATEPOST: PENSION Transacations POSTED Completed.");

                                if (executeMultiple)
                                {
                                    #region --------------- Execute Multiple Request --------------

                                    try
                                    {
                                        int pageSize = 1000;
                                        // TESTING - record the number of requests
                                        int requestCount = requestPostedTransactions.Requests.Count;

                                        tracingService.Trace($"TOTAL REQUEST IN requestPostedTransactions {requestCount}");
                                        if (requestCount > pageSize)
                                        {
                                            tracingService.Trace($"INSIDE if (requestCount > pageSize)");

                                            decimal maxPage = Math.Ceiling((decimal)requestCount / pageSize);
                                            ExecuteTransactionRequest tmp = new ExecuteTransactionRequest()
                                            {
                                                // Create an empty organization request collection.
                                                Requests = new OrganizationRequestCollection(),
                                                ReturnResponses = true
                                            };

                                            for (int pageNumber = 1; pageNumber <= maxPage; pageNumber++)
                                            {
                                                tracingService.Trace($"INSIDE LOOP {pageNumber} / {maxPage}");

                                                List<OrganizationRequest> listRequest = requestPostedTransactions.Requests.Skip((pageNumber - 1) * pageSize)
                                                                                                                .Take(pageSize).ToList();
                                                OrganizationRequestCollection orc = new OrganizationRequestCollection();
                                                listRequest.ForEach(orc.Add);
                                                tmp.Requests = orc;
                                                ExecuteTransactionResponse responsePostingTransaction = (ExecuteTransactionResponse)serviceAdmin.Execute(tmp);
                                            }
                                        }
                                        else
                                        {
                                            tracingService.Trace($"INSIDE ELSE OF if (requestCount >  pageSize)");
                                            ExecuteTransactionResponse responsePostingTransaction = (ExecuteTransactionResponse)serviceAdmin.Execute(requestPostedTransactions);
                                        }
                                    }
                                    catch (FaultException<OrganizationServiceFault> ex)
                                    {
                                        throw new InvalidPluginExecutionException($@"Update request failed for the transaction {((ExecuteTransactionFault)(ex.Detail)).FaultedRequestIndex + 1} and the reason being: {ex.Detail.Message}");
                                    }
                                    #endregion
                                }
                            }
                            catch (FaultException<OrganizationServiceFault> fault)
                            {

                                if (executeMultiple)
                                {
                                    //Check if the maximum batch size has been exceeded. The maximum batch size is only included in the fault if it
                                    // the input request collection count exceeds the maximum batch size.
                                    if (fault.Detail.ErrorDetails.Contains("MaxBatchSize"))
                                    {
                                        int maxBatchSize = Convert.ToInt32(fault.Detail.ErrorDetails["MaxBatchSize"]);
                                        if (maxBatchSize < requestPostedTransactions.Requests.Count)
                                        {
                                            // Here you could reduce the size of your request collection and re-submit the ExecuteTransaction request.
                                            // For this sample, that only issues a few requests per batch, we will just print out some info. However,
                                            // this code will never be executed because the default max batch size is 1000.
                                            throw new InvalidPluginExecutionException(OperationStatus.Failed, $@"The input request collection contains {requestPostedTransactions.Requests.Count} requests, 
                                                                which exceeds the maximum allowed {maxBatchSize}");
                                        }
                                    }
                                }
                                else
                                {
                                    throw new InvalidPluginExecutionException(OperationStatus.Failed, $@"SUBMISSIONBUSINESSPROCESSFLOWONUPDATEPOST: Deposit cannnot completed. Please contact BPA Administrator - {fault.Message} - \n - {fault.StackTrace}");
                                }
                            }

                            #endregion
                        }
                        else
                        {
                            tracingService.Trace("SUBMISSIONBUSINESSPROCESSFLOWONUPDATEPOST: else Execute Multiple Request = FALSE");
                            #region -----------  else Execute Multiple Request = FALSE -----------------
                            if (transactions != null && transactions.Entities.Count > 0)
                            {
                                tracingService.Trace($@"Number of Transacations: {transactions.Entities.Count}");
                                int tranCnt = 1;

                                foreach (Entity t in transactions.Entities)
                                {
                                    tracingService.Trace($@"{tranCnt.ToString()} - Transaction ID {t.Id}");
                                    Entity transaction = new Entity(PluginHelper.BpaTransaction)
                                    {
                                        Id = t.Id,
                                        ["statuscode"] = new OptionSetValue((int)TransactionStatus.Posted),
                                    };

                                    service.Update(transaction);
                                    tranCnt++;
                                }
                            }

                            tracingService.Trace($@"SubmissionOnPost: BENEFIT Transacations POSTED Completed.");
                            if (pensionTransactions != null && pensionTransactions.Entities.Count > 0)
                            {
                                tracingService.Trace($@"Number of Pension Transacations: {pensionTransactions.Entities.Count}");
                                int tranCnt = 1;
                                foreach (Entity pt in pensionTransactions.Entities)
                                {
                                    EntityReference transactionMemberPlan = pt.GetAttributeValue<EntityReference>("bpa_memberplanid");
                                    tracingService.Trace($@"{tranCnt.ToString()} - Pension Transaction ID {pt.Id}, Member Plan: {transactionMemberPlan.Name}");
                                    Entity ptransaction = new Entity(PluginHelper.BpaPensionTransaction)
                                    {
                                        Id = pt.Id,
                                        ["statuscode"] = new OptionSetValue((int)TransactionStatus.Posted),
                                    };
                                    
                                    service.Update(ptransaction);

                                    tranCnt++;
                                }
                            }
                            tracingService.Trace($@"SubmissionOnPost: PENSION Transacations POSTED Completed.");
                            #endregion
                        }

                        #endregion

                        //Fetch all Submission Detail Records and run Retro Processing
                        EntityCollection SubmissionDetails = SubmissionHelper.FetchAllSubmissionDetailBySubmissionId(service, tracingService, SubmissionEntity.Id, false, false);
                        List<EmployeeSubmissionDetails> Ls_EmployeeDetails = new List<EmployeeSubmissionDetails>();
                        if (SubmissionDetails != null && SubmissionDetails.Entities.Count > 0)
                        {
                            tracingService.Trace("Inside All Submission Detail - " + SubmissionDetails.Entities.Count);

                            DateTime WorkMonth = DateTime.MinValue;
                            if (SubmissionEntity.Attributes.Contains("bpa_submissiondate"))
                                WorkMonth = SubmissionEntity.GetAttributeValue<DateTime>("bpa_submissiondate");
                            WorkMonth = new DateTime(WorkMonth.Year, WorkMonth.Month, WorkMonth.Day);
                            tracingService.Trace("Inside WorkMonth - " + WorkMonth.ToShortDateString());

                            foreach (Entity submissiondetail in SubmissionDetails.Entities)
                            {
                                tracingService.Trace("Inside SubmissionDetail");

                                EntityReference Contact = null;
                                if (submissiondetail.Contains("bpa_memberplanid"))
                                {
                                    Contact = submissiondetail.GetAttributeValue<EntityReference>("bpa_memberplanid");
                                    tracingService.Trace("Inside ContactID - " + Contact.Id);
                                }
                                #region ------ Fetch all values from Submission Detail --------------------


                                decimal icihours = 0, nonicihours = 0, totalhours = 0, hoursEarned = 0, grosswages = 0, dollars = 0;
                                decimal pensionCredited = 0, pensionMemberRequired = 0, pensionMemberVoluntary = 0;

                                if (submissiondetail.Contains("bpa_icihours"))
                                    icihours = submissiondetail.GetAttributeValue<decimal>("bpa_icihours");
                                tracingService.Trace("Inside icihours - " + icihours);

                                if (submissiondetail.Contains("bpa_nonicihours"))
                                    nonicihours = submissiondetail.GetAttributeValue<decimal>("bpa_nonicihours");
                                tracingService.Trace("Inside nonicihours - " + nonicihours);

                                // for Local 506: we added hours Earned so "total hours" become "Hours Worked"
                                if (submissiondetail.Contains("bpa_totalhours"))
                                    totalhours = submissiondetail.GetAttributeValue<decimal>("bpa_totalhours");
                                tracingService.Trace("Inside totalhours - " + totalhours);

                                if (submissiondetail.Contains("bpa_hoursearned"))
                                    hoursEarned = submissiondetail.GetAttributeValue<decimal>("bpa_hoursearned");
                                tracingService.Trace("Inside hoursEarned - " + hoursEarned);


                                if (submissiondetail.Contains("bpa_grosswages") &&
                                    submissiondetail.GetAttributeValue<Money>("bpa_grosswages") != null)
                                    grosswages = submissiondetail.GetAttributeValue<Money>("bpa_grosswages").Value;
                                tracingService.Trace("Inside grosswages - " + grosswages);

                                if (submissiondetail.Contains("bpa_dollars") &&
                                    submissiondetail.GetAttributeValue<Money>("bpa_dollars") != null)
                                    dollars = submissiondetail.GetAttributeValue<Money>("bpa_dollars").Value;
                                tracingService.Trace("Inside dollars - " + dollars);

                                OptionSetValue FlatRateRule = null;
                                if (submissiondetail.Contains("bpa_flatraterule"))
                                    FlatRateRule = submissiondetail.GetAttributeValue<OptionSetValue>("bpa_flatraterule");

                                if (FlatRateRule == null)
                                    FlatRateRule = new OptionSetValue((int)BpaSubmissionDetailBpaFlatRateRule.Charge);

                                if (submissiondetail.Contains("bpa_pensioncredited"))
                                    pensionCredited = submissiondetail.GetAttributeValue<decimal>("bpa_pensioncredited");
                                tracingService.Trace("Inside Pension Credited - " + pensionCredited);
                                if (submissiondetail.Contains("bpa_pensionmemberrequired") &&
                                            submissiondetail.GetAttributeValue<Money>("bpa_pensionmemberrequired") != null)
                                    pensionMemberRequired = submissiondetail.GetAttributeValue<Money>("bpa_pensionmemberrequired").Value;
                                tracingService.Trace("Inside Pension Member Required - " + pensionMemberRequired);
                                if (submissiondetail.Contains("bpa_pensionmembervoluntary") &&
                                            submissiondetail.GetAttributeValue<Money>("bpa_pensionmembervoluntary") != null)
                                    pensionMemberVoluntary = submissiondetail.GetAttributeValue<Money>("bpa_pensionmembervoluntary").Value;
                                tracingService.Trace("Inside Pension Credited - " + pensionMemberVoluntary);

                                bool runRetroProcessing = false;
                                if ((totalhours != 0) || (grosswages != 0) || (dollars != 0) || hoursEarned != 0)
                                {
                                    runRetroProcessing = true;
                                }
                                #endregion

                                #region ---- Update contact Last Work Month & Last Agreement ---------------


                                    if (SubmissionEntity.Contains("bpa_submissiontype") &&
                                    SubmissionEntity.GetAttributeValue<OptionSetValue>("bpa_submissiontype").Value ==
                                    (int)SubmissionSubmissionType.EmployerRegular)
                                {
                                    

                                    if ((totalhours != 0) || (hoursEarned != 0) || (grosswages != 0) || (dollars != 0)
                                        || (pensionCredited != 0) || (pensionMemberRequired != 0) || (pensionMemberVoluntary != 0))
                                    {

                                       
                                        tracingService.Trace("Inside Update contact Last Work Month & Last Agreement");

                                        //Fetch Last Work Month from Member Plan
                                        Entity _MP = service.Retrieve(PluginHelper.BpaMemberplan, Contact.Id,
                                            new ColumnSet("bpa_lastworkmonth", "bpa_lastagreementid"));

                                        DateTime DB_LastWorkMonth = DateTime.MinValue;
                                        EntityReference DB_LastAgreement = null;

                                        if (_MP != null)
                                        {
                                            DB_LastWorkMonth = _MP.GetAttributeValue<DateTime>("bpa_lastworkmonth");
                                            DB_LastAgreement = _MP.Contains("bpa_lastagreementid") ? _MP.GetAttributeValue<EntityReference>("bpa_lastagreementid") : null;
                                        }

                                        if (DB_LastWorkMonth != DateTime.MinValue)
                                            DB_LastWorkMonth = new DateTime(DB_LastWorkMonth.Year, DB_LastWorkMonth.Month, 1);

                                        bool isUpdateMemberPlan = false, isWorkMonthChange = false, isAgreementChange = false;

                                        //EntityReference lastAgreement = TransactionHelper.FetchLastContributionAgreement(service, tracingService, Contact.Id, WorkMonth);
                                        EntityReference Sub_Agreement = SubmissionEntity.Contains("bpa_agreementid") ? SubmissionEntity.GetAttributeValue<EntityReference>("bpa_agreementid") : null;


                                        if (WorkMonth > DB_LastWorkMonth)
                                        {
                                            isUpdateMemberPlan = true;
                                            isWorkMonthChange = true;
                                        }
                                        if (DB_LastAgreement == null || (DB_LastAgreement.Id != Sub_Agreement.Id))
                                        {
                                            isUpdateMemberPlan = true;
                                            isAgreementChange = true;
                                        }

                                        if (isUpdateMemberPlan)
                                        {
                                            Entity U_Contact = new Entity(PluginHelper.BpaMemberplan);
                                            U_Contact.Id = Contact.Id;
                                            if (isWorkMonthChange)
                                                U_Contact["bpa_lastworkmonth"] = WorkMonth;

                                            if (isAgreementChange)
                                                U_Contact["bpa_lastagreementid"] = Sub_Agreement;

                                            service.Update(U_Contact);
                                            tracingService.Trace("After Updating Member Plan Last Work Month and Agreement");
                                        }
                                    }
                                }

                                #endregion

                                
                                #region ------------- Checking Member Plan Current Eligiblity = Frozen or Reciprocal Transfer Out ------------
                                // Get Member Current Eligiblity 
                                int currentEligibility = ContactHelper.FetchCurrentEligibility(service, tracingService, Contact.Id);
                                if (currentEligibility == (int)MemberPlanBpaCurrentEligibility.TransferOut)
                                {
                                    #region -------------------- RECIPROCAL TRANSFER OUT ---------------

                                    #region --------------- UPDATE ALL Ending Balance ---------------

                                    // We need to update the each Eligibility status transaction for Ending Balance
                                    EntityCollection transactions1 = TransactionHelper.FetchAllEligibilityStatusTransaction(serviceAdmin, tracingService, Contact.Id, WorkMonth);

                                    if (transactions1 != null && transactions1.Entities.Count > 0)
                                    {
                                        foreach (Entity transaction in transactions1.Entities)
                                        {
                                            DateTime transactionWorkMonth = transaction.Contains("bpa_transactiondate") ? transaction.GetAttributeValue<DateTime>("bpa_transactiondate") : DateTime.MinValue;
                                            if (transactionWorkMonth == DateTime.MinValue)
                                                transactionWorkMonth = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1);

                                            Dictionary<string, decimal> endingBalances = TransactionHelper.FetchEndingBalance(service, tracingService, Contact.Id, transactionWorkMonth);
                                            // Get total Balance upto that month 
                                            Entity update = new Entity(PluginHelper.BpaTransaction)
                                            { Id = transaction.Id };
                                            if (endingBalances.ContainsKey("hours"))
                                            {
                                                tracingService.Trace($@"Hours: {endingBalances["hours"]}");
                                                update["bpa_endinghourbalance"] = endingBalances["hours"];
                                            }
                                            if (endingBalances.ContainsKey("dollarhours"))
                                            {
                                                tracingService.Trace($@"Dollarhours: {endingBalances["dollarhours"]}");
                                                update["bpa_endingdollarbalance"] = endingBalances["dollarhours"];
                                            }
                                            if (endingBalances.ContainsKey("selfpaydollar"))
                                            {
                                                tracingService.Trace($@"Selfpay Dollar: {endingBalances["selfpaydollar"]}");
                                                update["bpa_endingselfpaybalance"] = new Money(endingBalances["selfpaydollar"]);
                                            }
                                            if (endingBalances.ContainsKey("secondarydollar"))
                                            {
                                                tracingService.Trace($@"Secondary Dollar: {endingBalances["secondarydollar"]}");
                                                update["bpa_secondarydollarbalance"] = new Money(endingBalances["secondarydollar"]);
                                            }

                                            serviceAdmin.Update(update);
                                        }
                                    }
                                    #endregion

                                    #region ------------ Delete All Extra Transaction for that same month 


                                    // Check Member Current Eligibity Status
                                    // if It is Transfer Out Delete Existing Record
                                    // If Transcation(s) is exists DELETE transaction
                                    EntityCollection isExistsTrans = TransactionHelper.IsTransactionExists(serviceAdmin, tracingService, Contact.Id, WorkMonth,
                                        (int)BpaTransactionbpaTransactionType.TransferOut, submission.Id);
                                    if (isExistsTrans != null && isExistsTrans.Entities.Count > 0)
                                    {
                                        foreach (Entity trn in isExistsTrans.Entities)
                                        {
                                            tracingService.Trace("Inside if (IsReciprocalTransferOutExists)");
                                            serviceAdmin.Delete(PluginHelper.BpaTransaction, trn.Id);
                                        }
                                    }

                                    #endregion


                                    #endregion

                                }
                                else if (currentEligibility == (int)MemberPlanBpaCurrentEligibility.Inactive && runRetroProcessing)
                                {
                                    // TFS# 1380 - Inactive Logic - Records not calculateing correctly
                                    #region ------------------ INACTIVE -------------------

                                    Entity U_MP = new Entity(PluginHelper.BpaMemberplan)
                                    {
                                        Id = Contact.Id,
                                        ["bpa_benefitstatus"] = new OptionSetValue((int)MemberPlanBpaMemberPlanStatus.Active)
                                    };
                                    service.Update(U_MP);
                                    tracingService.Trace("After Updating Member Plan Status to Active");

                                    #endregion
                                }
                                #region --------------- Run Retro Processing Workflow ------------
                                if (runRetroProcessing && PlanIsBenefitPlan)
                                {
                                    // Calling the Action - bpa_RetroProcessingAsync
                                    OrganizationRequest req = new OrganizationRequest("bpa_RetroProcessingAsync");
                                    req["RecordId"] = Contact;
                                    req["RetroProcessingDate"] = WorkMonth;
                                    req["Target"] = new EntityReference("bpa_memberplan", Contact.Id);
                                    //execute the request
                                    OrganizationResponse response = service.Execute(req);
                                }
                                #endregion

                                #endregion
                                if (runRetroProcessing)
                                {
                                    // Trigger Rollup recalculations after Retroprocessing is complete
                                    PluginHelper.TriggerRollup(service, tracingService, PluginHelper.BpaMemberplan, Contact.Id, "bpa_contributiondollarbank");
                                    PluginHelper.TriggerRollup(service, tracingService, PluginHelper.BpaMemberplan, Contact.Id, "bpa_contributionhourbank");
                                    PluginHelper.TriggerRollup(service, tracingService, PluginHelper.BpaMemberplan, Contact.Id, "bpa_selfpaybank");
                                    PluginHelper.TriggerRollup(service, tracingService, PluginHelper.BpaMemberplan, Contact.Id, "bpa_secondarydollarbank");
                                    PluginHelper.TriggerRollup(service, tracingService, PluginHelper.BpaMemberplan, Contact.Id, "bpa_vacationpaybank");
                                }
                                tracingService.Trace("=================================");
                            } // end of foreach submission detail loop
                        }

                        #endregion
                        
                        tracingService.Trace($@"SubmissionBusinessProcessFlowOnUpdatePost: COMPLETED Current Stage 'Completed' AND Previous Stage 'Deposit Pending'");

                        #endregion
                    }
                    else if (Pre_StageId == PluginHelper.SubmissionStageIdVerify)
                    {
                        #region ------------------ NO Payment -----------
                        string submissionname = SubmissionEntity.GetAttributeValue<string>("bpa_name");
                        tracingService.Trace($@"============================={submissionname}===================================");
                        tracingService.Trace($@"INSIDE: Current Stage Completed and Pre Stage Verification");


                        OptionSetValue pt = new OptionSetValue(0);
                        if (SubmissionEntity.Contains("bpa_paymentmethod"))
                            pt = SubmissionEntity.GetAttributeValue<OptionSetValue>("bpa_paymentmethod");
                        else if (SubmissionEntity.Contains("bpa_paymentmethod"))
                            pt = SubmissionEntity.GetAttributeValue<OptionSetValue>("bpa_paymentmethod");

                        if (pt.Value == (int)SubmissionPaymentType.NoPayment)
                            SubmissionHelper.DeactivateSubmission(service, tracingService, SubmissionEntity.Id);
                    }
                    #endregion

                    #endregion
                }

            } // end of if (entity.Contains("activestageid"))
        } // end of ExecuteUpdatePost

    }
}
