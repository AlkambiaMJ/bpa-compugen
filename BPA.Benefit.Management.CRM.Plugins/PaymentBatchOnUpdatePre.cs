﻿#region

using System;
using BPA.Benefit.Management.CRM.Plugins.Helper;
using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;

#endregion

namespace BPA.Benefit.Management.CRM.Plugins
{
    /// <summary>
    ///     PaymentBatchOnUpdatePre PlugIn.
    /// </summary>
    public class PaymentBatchOnUpdatePre : Plugin
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="PaymentBatchOnUpdatePre" /> class.
        /// </summary>
        public PaymentBatchOnUpdatePre()
            : base(typeof(PaymentBatchOnUpdatePre))
        {
            RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(20, "Update",
                "bpa_paymentbatch", ExecutePaymentBatchOnUpdatePre));

            // Note : you can register for more events here if this plugin is not specific to an individual entity and message combination.
            // You may also need to update your RegisterFile.crmregister plug-in registration file to reflect any change.
        }

        /// <summary>
        ///     Executes the plug-in.
        /// </summary>
        /// <param name="localContext">
        ///     The <see cref="Plugin.LocalPluginContext" /> which contains the
        ///     <see cref="IPluginExecutionContext" />,
        ///     <see cref="IOrganizationService" />
        ///     and <see cref="ITracingService" />
        /// </param>
        /// <remarks>
        ///     For improved performance, Microsoft Dynamics CRM caches plug-in instances.
        ///     The plug-in's Execute method should be written to be stateless as the constructor
        ///     is not called for every invocation of the plug-in. Also, multiple system threads
        ///     could execute the plug-in at the same time. All per invocation state information
        ///     is stored in the context. This means that you should not use global variables in plug-ins.
        /// </remarks>
        protected void ExecutePaymentBatchOnUpdatePre(LocalPluginContext localContext)
        {
            //// If local context not passed in, raise error
            //if (localContext == null)
            //    throw new ArgumentNullException($"Local context not passed in.");

            //// Create the context variables
            //IPluginExecutionContext context = localContext.PluginExecutionContext;
            //IOrganizationService service = localContext.OrganizationService;
            //ITracingService tracingService = localContext.TracingService;

            //// If Target is not passed in, raise error
            //tracingService.Trace("If Target is not passed in, raise error");
            //if (!context.InputParameters.Contains(PluginHelper.Target) ||
            //    !(context.InputParameters[PluginHelper.Target] is Entity))
            //    throw new ArgumentNullException($"Target is not of type Entity.");

            //// Create the target entity
            //tracingService.Trace("Create the target entity");
            //Entity entity = (Entity)context.InputParameters[PluginHelper.Target];

            //// If the Complete Batch flag is checked
            //localContext.Trace("Looking to see if the batch is marked complete.");
            //if (!entity.Contains("statuscode") || entity.GetAttributeValue<OptionSetValue>("statuscode").Value != (int)PaymentBatchStatus.Completed) return;

            //// The user has requested to close the batch
            //// Get a list of all of the payment for this batch
            //// Loop through all of the related Payments and set them to Processing
            //localContext.Trace("Query the Payment records.");
            //QueryExpression queryExpression = new QueryExpression
            //{
            //    EntityName = "bpa_payment",
            //    ColumnSet = new ColumnSet(true),
            //    Criteria =
            //    {
            //        Filters =
            //        {
            //            new FilterExpression
            //            {
            //                FilterOperator = LogicalOperator.And,
            //                Conditions =
            //                {
            //                    new ConditionExpression("bpa_paymentbatchid", ConditionOperator.Equal, context.PrimaryEntityId),
            //                    new ConditionExpression("statecode", ConditionOperator.In, new object[] { 0, (int)BpaPaymentStatusCode.Completed })
                                
            //                }
            //            }
            //        }
            //    }
            //};

            //EntityCollection payments = service.RetrieveMultiple(queryExpression);

            //// Look through them and generate cheque records for each one
            //localContext.Trace("Looping through all linked Payment records.");
            //foreach (Entity payment in payments.Entities)
            //{
            //    EntityReference memberPlanRef = null;
            //    if (payment.Contains("bpa_memberplanid"))
            //        memberPlanRef = (EntityReference)payment.Attributes["bpa_memberplanid"];

            //    EntityReference payeeRef = null;
            //    if (payment.Contains("bpa_payeeid"))
            //        payeeRef = (EntityReference)payment.Attributes["bpa_payeeid"];

            //    DateTime activityStartDate = payment.Contains("bpa_activitystartdate")
            //        ? payment.GetAttributeValue<DateTime>("bpa_activitystartdate")
            //        : DateTime.MinValue;

            //    Money paymentAmount = payment.Contains("bpa_paymentamount")
            //        ? payment.GetAttributeValue<Money>("bpa_paymentamount")
            //        : new Money(0);

            //    // TODO DB - Need to see if this is not needed
            //    //EntityReference chequeRef = null;
            //    //if (payment.Contains("bpa_chequeid"))
            //    //    chequeRef = payment.GetAttributeValue<EntityReference>("bpa_chequeid");

            //    Entity payee = new Entity();
            //    if (payeeRef != null)
            //        payee = service.Retrieve("contact", payeeRef.Id, new ColumnSet(true));

            //    // Set the variable values from the payment record
            //    localContext.Trace("Set the variable values from the payment record.");
            //    if (!payment.Contains("bpa_paymenttype")) continue;

            //    OptionSetValue paymentType = (OptionSetValue)payment.Attributes["bpa_paymenttype"];

            //    // Find the fund first the cheque should come from
            //    localContext.Trace("Find the proper Fund to get the bank details.");
            //    QueryExpression queryFund = new QueryExpression
            //    {
            //        EntityName = "bpa_fund",
            //        ColumnSet = new ColumnSet(true),
            //        Criteria =
            //        {
            //            Filters =
            //            {
            //                new FilterExpression
            //                {
            //                    FilterOperator = LogicalOperator.And,
            //                    Conditions =
            //                    {
            //                        new ConditionExpression("bpa_trustid", ConditionOperator.Equal,
            //                            ((EntityReference)payment.Attributes["bpa_trustid"]).Id)
            //                    }
            //                }
            //            }
            //        }
            //    };

            //    localContext.Trace("Adding Link to the FundPaymentType Entity.");
            //    LinkEntity link = queryFund.AddLink("bpa_fundpaymenttype", "bpa_fundid", "bpa_fundid",
            //        JoinOperator.Inner);
            //    link.EntityAlias = "fundpaymentype";
            //    queryFund.Criteria = new FilterExpression();
            //    queryFund.Criteria.AddCondition("fundpaymentype", "bpa_paymenttype", ConditionOperator.Equal,
            //        paymentType.Value);
            //    EntityCollection funds = service.RetrieveMultiple(queryFund);

            //    if (funds.Entities == null)
            //    {
            //        // There is a cheque that needs to be printed,
            //        // But there is no payment type linked to a fund
            //        // for this Trust
            //        localContext.Trace("No Fund found for Cheque to come out of.  Raise Error.");
            //        throw new InvalidPluginExecutionException(OperationStatus.Failed,
            //            "No Fund found for Cheque to come out of.  Please ensure that all payment types types have been assigned to a fund.");
            //    }
            //    Entity fund = funds.Entities[0];

            //    //Get the last cheque number generated. If it's 999999 then reset to 1 otherwise increment +1
            //    int newChequeNumber = 0;

            //    if (fund.GetAttributeValue<Int32>("bpa_lastchequenumber") == 999999)
            //        newChequeNumber = 1;
            //    else
            //        newChequeNumber = fund.GetAttributeValue<Int32>("bpa_lastchequenumber") + 1;

            //    EntityReference fundOwner = null;
            //    if (fund.Contains("owner"))
            //        fundOwner = fund.GetAttributeValue<EntityReference>("owner");
                
            //    //Update the fund with the new latest cheque number. Only update one field.
            //    Entity updateFund = new Entity(PluginHelper.BpaFund);
            //    updateFund.Id = fund.Id;
            //    updateFund["bpa_lastchequenumber"] = newChequeNumber;
            //    service.Update(updateFund);


            //    // Generate the Cheque record
            //    localContext.Trace("Generate the Cheque record");
            //    Entity cheque = new Entity(PluginHelper.BpaCheque);

            //    // Populate Member and Cheque details
            //    localContext.Trace("Populate Member and Cheque details");
            //    cheque.Attributes.Add("bpa_payeeid", payeeRef);
            //    cheque.Attributes.Add("bpa_chequedate", DateTime.Now.Date);
            //    cheque.Attributes.Add("bpa_chequeamount", payment.Attributes["bpa_paymentamount"]);
            //    cheque.Attributes.Add("transactioncurrencyid", payment.Attributes["transactioncurrencyid"]);
            //    cheque.Attributes.Add("bpa_chequenumber",newChequeNumber.ToString("D6"));

            //    // Populate Pay To Details
            //    localContext.Trace("Populate the Pay details");
            //    cheque.Attributes.Add("bpa_payto",
            //        payee.Contains("fullname") ? (string)payee.Attributes["fullname"] : string.Empty);
            //    cheque.Attributes.Add("bpa_addressline1",
            //        payee.Contains("address1_line1") ? (string)payee.Attributes["address1_line1"] : string.Empty);
            //    cheque.Attributes.Add("bpa_addressline2",
            //        payee.Contains("address1_line2") ? (string)payee.Attributes["address1_line2"] : string.Empty);
            //    cheque.Attributes.Add("bpa_addressline3",
            //        payee.Contains("address1_line3") ? (string)payee.Attributes["address1_line3"] : string.Empty);
            //    cheque.Attributes.Add("bpa_city",
            //        payee.Contains("address1_city") ? (string)payee.Attributes["address1_city"] : string.Empty);
            //    cheque.Attributes.Add("bpa_province",
            //        payee.Contains("address1_stateorprovince")
            //            ? (string)payee.Attributes["address1_stateorprovince"]
            //            : string.Empty);
            //    cheque.Attributes.Add("bpa_postalcode",
            //        payee.Contains("address1_postalcode")
            //            ? (string)payee.Attributes["address1_postalcode"]
            //            : string.Empty);

            //    // Populate Bank Details (from Fund)
            //    // Found the proper fund.  Add the Bank information
            //    localContext.Trace("Found the Fund and adding bank details");
            //    cheque.Attributes.Add("bpa_bankname",
            //        fund.Contains("bpa_bankname") ? (string)fund.Attributes["bpa_bankname"] : string.Empty);
            //    cheque.Attributes.Add("bpa_bankemail",
            //        fund.Contains("bpa_bankemail") ? (string)fund.Attributes["bpa_bankemail"] : string.Empty);
            //    cheque.Attributes.Add("bpa_bankphonenumber",
            //        fund.Contains("bpa_bankphone") ? (string)fund.Attributes["bpa_bankphone"] : string.Empty);
            //    cheque.Attributes.Add("bpa_banknumber",
            //        fund.Contains("bpa_banknumber") ? (string)fund.Attributes["bpa_banknumber"] : string.Empty);
            //    cheque.Attributes.Add("bpa_banktransitnumber",
            //        fund.Contains("bpa_banktransitnumber")
            //            ? (string)fund.Attributes["bpa_banktransitnumber"]
            //            : string.Empty);
            //    cheque.Attributes.Add("bpa_bankaccountnumber",
            //        fund.Contains("bpa_bankaccountnumber")
            //            ? (string)fund.Attributes["bpa_bankaccountnumber"]
            //            : string.Empty);
            //    cheque.Attributes.Add("bpa_designationnumber",
            //        fund.Contains("bpa_designationnumber")
            //            ? (string)fund.Attributes["bpa_designationnumber"]
            //            : string.Empty);
            //    cheque.Attributes.Add("bpa_chequeamountspelled", FundHelper.TranslateCurrency(payment.GetAttributeValue<Money>("bpa_paymentamount").Value));


            //    // Create the new cheque
            //    localContext.Trace("Create the new cheque");
            //    Guid chequeId = service.Create(cheque);
            //    if (fundOwner != null)
            //        PluginHelper.AssignRecord(service, tracingService, fundOwner, PluginHelper.BpaCheque, chequeId);



            //    // Link the new cheque record back to the payment
            //    localContext.Trace("Link the new cheque record back to the payment");
            //    Entity p = new Entity(PluginHelper.BpaPayment)
            //    {
            //        Id = payment.Id,
            //        ["bpa_chequeid"] = new EntityReference(PluginHelper.BpaCheque, chequeId)
            //    };
            //    service.Update(p);
            //    if (fundOwner != null)
            //        PluginHelper.AssignRecord(service, tracingService, fundOwner, PluginHelper.BpaPayment, payment.Id);


            //    // Generate Transaction and update Rollups
            //    switch (paymentType.Value)
            //    {
            //        case (int)PaymentBatchPaymentType.ParentalLeave: // Parental Leave
            //        case (int)PaymentBatchPaymentType.JuryDuty: // Jury Duty
            //        case (int)PaymentBatchPaymentType.BereavmentLeave: // Bereavment Leave
            //        case (int)PaymentBatchPaymentType.PrePaidLegal: // Pre-Paid Legal
            //        case (int)PaymentBatchPaymentType.DentalClinic: // Dental Clinic

            //            // There is no bank activity, so nothing else to do
            //            localContext.Trace("Payment is for Parental Leave, Jury Duty, Pre-Paid Legal, Dental Clinic or Bereavment Leave.");
            //            break;

            //        case (int)PaymentBatchPaymentType.InterimVacation: // Interim Vacation
            //        case (int)PaymentBatchPaymentType.AnnualVacation: // Annual Vacation

            //            // Update the vacation bank and recalcuate
            //            localContext.Trace("Payment is for Interim and Annual Vacation.");

            //            // DONE - TODO JK - Create the member transaction to decrement their vacation bank
            //            // Use TransactionHelper.CreateTransaction and link back to the payment record
            //            TransactionHelper.CreateTransactionWithPayment(service, tracingService, memberPlanRef, activityStartDate.AddMonths(-1), 
            //                (int)BpaTransactionbpaTransactionCategory.Member, (int)BpaTransactionbpaTransactionType.VacationDraw, new EntityReference(PluginHelper.BpaFund, fund.Id),
            //                new Money(paymentAmount.Value*-1), new EntityReference(PluginHelper.BpaPayment, payment.Id), DateTime.Now, 
            //                new EntityReference(PluginHelper.BpaCheque, chequeId));

            //            // Trigger the recalcuate of the vacation bank
            //            PluginHelper.TriggerRollup(service, tracingService, PluginHelper.BpaMemberplan, memberPlanRef.Id, "bpa_vacationpaybank");
            //            break;

            //        case (int)PaymentBatchPaymentType.EmployerRefund: // Employer Refund

            //            // Update the employer bank and recalcuate
            //            localContext.Trace("Payment is for Employer Refund.");

            //            // DONE - TODO JK - Create Contribution record to decrement their submission bank
            //            // Use TransactionHelper.CreateTransaction and link back to the payment record
            //            ContributionHelper.CreateContributionWithPayment(service, tracingService, employerRef.Id,
            //                activityStartDate.AddMonths(-1), fund.Id,
            //                (int)BpaContributionbpaContributionType.Disbursement, paymentAmount.Value);

            //            // Trigger the recalcuate of the employer bank
            //            PluginHelper.TriggerRollup(service, tracingService, PluginHelper.Account, employerRef.Id, "bpa_totalamountowed");
            //            break;

            //        case (int)PaymentBatchPaymentType.SelfPayRefund: // Self-Pay Refund

            //            // Update the Member's self-pay bank and recalcuate
            //            localContext.Trace("Payment is for Member's Self-pay Refund.");

            //            // TODO JK - Create the member transaction to decrement their self-pay bank
            //            // Use TransactionHelper.CreateTransaction and link back to the payment record

            //            // Trigger the recalcuate of the self-pay bank
            //            PluginHelper.TriggerRollup(service, tracingService, PluginHelper.BpaMemberplan, memberPlanRef.Id, "bpa_selfpaybank");
            //            break;
            //    }

            //    // Set the Payment record to Complete
            //    // Create the Request Object
            //    localContext.Trace("Set the Payment record to complete");

            //    SetStateRequest stateToCancelled = new SetStateRequest
            //    {
            //        // Set the Request Object’s Properties
            //        EntityMoniker = new EntityReference(payment.LogicalName, payment.Id),
            //        State = new OptionSetValue(0), // active state
            //        Status = new OptionSetValue(922070000) // Processing status
            //    };

            //    // Execute the Request
            //    service.Execute(stateToCancelled);
            //    PluginHelper.DeactivateRecord(service, PluginHelper.BpaPayment, payment.Id, 922070001); //Completed status

            //}

            //// Update the Batch Process Date
            //localContext.Trace("Update the Batch Process Date");
            //entity.Attributes.Add("bpa_batchprocessdate", DateTime.Now);

            //// Set the Payment Batch record to processing
            //// Create the Request Object
            //localContext.Trace("Setting batch to Processing");

            //// Deactivate the Payment Batch record
            //// Create the Request Object
            //localContext.Trace("Set the Payment Batch record to complete");

            //var cols = new ColumnSet("statecode", "statuscode");
            ////Check if it is Active or not
            //var entityStatus = service.Retrieve(PluginHelper.BpaPaymentBatch, entity.Id, cols);

            //if (entity != null && entity.GetAttributeValue<OptionSetValue>("statecode").Value == 0)
            //{
            //    PluginHelper.DeactivateRecordWithChecking(service, PluginHelper.BpaPaymentBatch, entity.Id, (int)PaymentBatchStatus.Completed);
            //}

            //    //SetStateRequest statePaymentBatchToComplete = new SetStateRequest
            //    //{
            //    //    // Set the Request Object’s Properties
            //    //    EntityMoniker = new EntityReference(entity.LogicalName, entity.Id),
            //    //    State = new OptionSetValue(1), // Inactive state
            //    //    Status = new OptionSetValue((int)PaymentBatchStatus.Completed) // Complete status
            //    //};

            //    //// Execute the Request
            //    //service.Execute(statePaymentBatchToComplete);
            //    localContext.Trace("Execute the Request");
        }
    }
}