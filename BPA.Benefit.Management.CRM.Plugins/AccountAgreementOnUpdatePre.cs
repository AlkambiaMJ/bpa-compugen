﻿#region

using System;
using System.Linq;
using BPA.Benefit.Management.CRM.Plugins.Helper;
using Microsoft.Xrm.Sdk;

#endregion

namespace BPA.Benefit.Management.CRM.Plugins
{
    public class AccountAgreementOnUpdatePre : Plugin
    {


        /// <summary>
        ///     Initializes a new instance of the <see cref="AccountAgreementOnUpdatePre" /> class.
        /// </summary>
        public AccountAgreementOnUpdatePre()
            : base(typeof(AccountAgreementOnUpdatePre))
        {
            RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(20, "Update", PluginHelper.AccountAgreement, ExecuteAccountAgreementUpdatePre));

            // Note : you can register for more events here if this plugin is not specific to an individual entity and message combination.
            // You may also need to update your RegisterFile.crmregister plug-in registration file to reflect any change.
        }

        /// <summary>
        ///     Executes the plug-in.
        /// </summary>
        /// <param name="localContext">
        ///     The <see cref="Plugin.LocalPluginContext" /> which contains the
        ///     <see cref="IPluginExecutionContext" />,
        ///     <see cref="IOrganizationService" />
        ///     and <see cref="ITracingService" />
        /// </param>
        /// <remarks>
        ///     For improved performance, Microsoft Dynamics CRM caches plug-in instances.
        ///     The plug-in's Execute method should be written to be stateless as the constructor
        ///     is not called for every invocation of the plug-in. Also, multiple system threads
        ///     could execute the plug-in at the same time. All per invocation state information
        ///     is stored in the context. This means that you should not use global variables in plug-ins.
        /// </remarks>
        protected void ExecuteAccountAgreementUpdatePre(LocalPluginContext localContext)
        {

            // If local context not passed in, raise error
            if (localContext == null)
                throw new ArgumentNullException($"Local context not passed in.");

            // Create the context variables
            IPluginExecutionContext context = localContext.PluginExecutionContext;
            IOrganizationService service = localContext.OrganizationService;
            ITracingService tracingService = localContext.TracingService;

            // If Target is not passed in, raise error
            tracingService.Trace("If Target is not passed in, raise error");
            if (!context.InputParameters.Contains(PluginHelper.Target) || !(context.InputParameters[PluginHelper.Target] is Entity))
                throw new ArgumentNullException($"Target is not of type Entity.");

            // Create the target entity
            tracingService.Trace("Create the target entity");
            Entity entity = (Entity)context.InputParameters[PluginHelper.Target];

            //Check is there any active submission for this account/employer
            if (entity.Contains("bpa_accountstatus"))
            {
                OptionSetValue accountStatus = entity.GetAttributeValue<OptionSetValue>("bpa_accountstatus");
                if ((accountStatus.Value == (int)AccountStatus.Closed) && SubmissionHelper.IsActiveSubmissionForAccount(service, tracingService, entity.Id))
                    throw new InvalidPluginExecutionException(OperationStatus.Failed, @"Three are active submission(s) for this employer agreement. Employer Agreement closure cannot be completed.");
            }

            // Validate Introduction Date and Termination Date
            Entity preImageEntity = context.PreEntityImages != null && context.PreEntityImages.Contains(PluginHelper.PreImageAlias)
                                        ? context.PreEntityImages[PluginHelper.PreImageAlias] : null;
            if (preImageEntity == null) throw new InvalidPluginExecutionException(OperationStatus.Canceled, $@"Pre Image cannot be null");

            DateTime introductionDate = DateTime.MinValue;
            if (entity.Contains("bpa_introductiondate"))
                introductionDate = entity.GetAttributeValue<DateTime>("bpa_introductiondate");
            else if (preImageEntity.Contains("bpa_introductiondate"))
                introductionDate = preImageEntity.GetAttributeValue<DateTime>("bpa_introductiondate");

            DateTime terminationDate = DateTime.MinValue;
            if (entity.Contains("bpa_terminationdate"))
                terminationDate = entity.GetAttributeValue<DateTime>("bpa_terminationdate");
            else if (preImageEntity.Contains("bpa_terminationdate"))
                terminationDate = preImageEntity.GetAttributeValue<DateTime>("bpa_terminationdate");

            if (introductionDate != DateTime.MinValue && terminationDate != DateTime.MinValue)
            {
                if (terminationDate < introductionDate)
                    throw new InvalidPluginExecutionException(OperationStatus.Failed, $@"Termination Date cannot be earlier than Introduction Date");
            }
            
        }


    }
}
