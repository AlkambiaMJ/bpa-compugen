﻿#region

using System;
using System.Linq;
using BPA.Benefit.Management.CRM.Plugins.Helper;
using Microsoft.Xrm.Sdk;

#endregion

namespace BPA.Benefit.Management.CRM.Plugins
{
    public class AccountAgreementOnCreatePre : Plugin
    {


        /// <summary>
        ///     Initializes a new instance of the <see cref="AccountAgreementOnUpdatePre" /> class.
        /// </summary>
        public AccountAgreementOnCreatePre()
            : base(typeof(AccountAgreementOnCreatePre))
        {
            RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(20, "Create", PluginHelper.AccountAgreement, ExecuteAccountAgreementCreatePre));

            // Note : you can register for more events here if this plugin is not specific to an individual entity and message combination.
            // You may also need to update your RegisterFile.crmregister plug-in registration file to reflect any change.
        }

        /// <summary>
        ///     Executes the plug-in.
        /// </summary>
        /// <param name="localContext">
        ///     The <see cref="Plugin.LocalPluginContext" /> which contains the
        ///     <see cref="IPluginExecutionContext" />,
        ///     <see cref="IOrganizationService" />
        ///     and <see cref="ITracingService" />
        /// </param>
        /// <remarks>
        ///     For improved performance, Microsoft Dynamics CRM caches plug-in instances.
        ///     The plug-in's Execute method should be written to be stateless as the constructor
        ///     is not called for every invocation of the plug-in. Also, multiple system threads
        ///     could execute the plug-in at the same time. All per invocation state information
        ///     is stored in the context. This means that you should not use global variables in plug-ins.
        /// </remarks>
        protected void ExecuteAccountAgreementCreatePre(LocalPluginContext localContext)
        {

            // If local context not passed in, raise error
            if (localContext == null)
                throw new ArgumentNullException($"Local context not passed in.");

            // Create the context variables
            IPluginExecutionContext context = localContext.PluginExecutionContext;
            IOrganizationService service = localContext.OrganizationService;
            ITracingService tracingService = localContext.TracingService;

            // If Target is not passed in, raise error
            tracingService.Trace("If Target is not passed in, raise error");
            if (!context.InputParameters.Contains(PluginHelper.Target) || !(context.InputParameters[PluginHelper.Target] is Entity))
                throw new ArgumentNullException($"Target is not of type Entity.");

            // Create the target entity
            tracingService.Trace("Create the target entity");
            Entity entity = (Entity)context.InputParameters[PluginHelper.Target];

            OptionSetValue accountType = new OptionSetValue(0);
            //Validate Account Type is either "Employer" OR "Self-Pay"
            if (entity.Contains("bpa_accounttype"))
            {
                accountType = entity.GetAttributeValue<OptionSetValue>("bpa_accounttype");
                if ((accountType.Value == (int)AccountType.Employer) || (accountType.Value == (int)AccountType.SelfPay))
                {
                    switch (accountType.Value)
                    {
                        case (int)AccountType.SelfPay:
                            //Validation: Can not have more than one active self pay employer agreement per plan
                            if (entity.Contains("bpa_planid"))
                            {
                                EntityReference plan = entity.GetAttributeValue<EntityReference>("bpa_planid");
                                Entity accountAgreement = AccountAgreementHelper.FetchSelePayAccountAgreementByPlanId(service, tracingService, plan.Id);

                                if (accountAgreement != null)
                                    throw new InvalidPluginExecutionException(OperationStatus.Failed, @"Self-Pay employer agreement already exists for this plan.");
                            }
                            break;
                        case (int)AccountType.Employer:
                            if (entity.Contains("bpa_planid") && entity.Contains("bpa_agreementid"))
                            {
                                EntityReference plan = entity.GetAttributeValue<EntityReference>("bpa_planid");
                                EntityReference agreement = entity.GetAttributeValue<EntityReference>("bpa_agreementid");
                                EntityReference account = entity.GetAttributeValue<EntityReference>("bpa_accountid");


                                if (AccountAgreementHelper.FetchSelePayAccountAgreementByPlanIdAndAgreementId(service, tracingService, plan.Id, agreement.Id, account.Id))
                                    throw new InvalidPluginExecutionException(OperationStatus.Failed, @"Employer agreement already exists for selected plan and agreement.");
                            }
                            break;
                    }

                    // Validate Introduction Date and Termination Date
                    DateTime introductionDate = entity.Contains("bpa_introductiondate") ? entity.GetAttributeValue<DateTime>("bpa_introductiondate") : DateTime.MinValue;
                    DateTime terminationDate = entity.Contains("bpa_terminationdate") ? entity.GetAttributeValue<DateTime>("bpa_terminationdate") : DateTime.MinValue;

                    if(introductionDate != DateTime.MinValue && terminationDate != DateTime.MinValue)
                    {
                        if (terminationDate < introductionDate)
                            throw new InvalidPluginExecutionException(OperationStatus.Failed, $@"Termination Date cannot be earlier than Introduction Date");
                    }
                }
                else
                    throw new InvalidPluginExecutionException(OperationStatus.Failed, @"Account type must be 'Employer' or 'Self-Pay' to create an employer agreement.");
            }


        }


    }
}
