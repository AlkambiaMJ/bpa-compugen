﻿#region

using System;
using BPA.Benefit.Management.CRM.Plugins.Helper;
using Microsoft.Xrm.Sdk;

#endregion

namespace BPA.Benefit.Management.CRM.Plugins
{
    /// <summary>
    ///     AdjustmentOnCreatePost Plugin.
    /// </summary>
    public class AdjustmentOnStateChangePost : Plugin
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="AdjustmentOnStateChangePost" /> class.
        /// </summary>
        public AdjustmentOnStateChangePost(string unsecureString, string secureString)
            : base(typeof(AdjustmentOnPostUpdate), unsecureString, secureString)
        {
            RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(40, "SetState", PluginHelper.BpaSubmissionadjustment, ExecuteAdjustmentOnStateChangePost));
            RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(40, "SetStateDynamicEntity", PluginHelper.BpaSubmissionadjustment, ExecuteAdjustmentOnStateChangePost));

            // Note : you can register for more events here if this plugin is not specific to an individual entity and message combination.
            // You may also need to update your RegisterFile.crmregister plug-in registration file to reflect any change.
        }

        /// <summary>
        ///     Executes the plug-in.
        /// </summary>
        /// <param name="localContext">
        ///     The <see cref="Plugin.LocalPluginContext" /> which contains the
        ///     <see cref="IPluginExecutionContext" />,
        ///     <see cref="IOrganizationService" />
        ///     and <see cref="ITracingService" />
        /// </param>
        /// <remarks>
        ///     For improved performance, Microsoft Dynamics CRM caches plug-in instances.
        ///     The plug-in's Execute method should be written to be stateless as the constructor
        ///     is not called for every invocation of the plug-in. Also, multiple system threads
        ///     could execute the plug-in at the same time. All per invocation state information
        ///     is stored in the context. This means that you should not use global variables in plug-ins.
        /// </remarks>
        protected void ExecuteAdjustmentOnStateChangePost(LocalPluginContext localContext)
        {
            // If local context not passed in, raise error
            if (localContext == null)
                throw new ArgumentNullException($"Local context not passed in.");

            // Create the context variables
            IPluginExecutionContext context = localContext.PluginExecutionContext;
            IOrganizationService service = localContext.OrganizationService;
            ITracingService tracingService = localContext.TracingService;
            IServiceProvider serviceProvider = localContext.ServiceProvider;

            // I ned to put following condition specially when type is either INTERESET REDIRECT or INTEREST PAID. otherwise it will go in loop
            if (context.Depth > 1)
                return;

            EntityReference targetEntity = (EntityReference)context.InputParameters[PluginHelper.EntityMoniker];
            if (targetEntity == null)
                throw new ArgumentNullException($"EntityReference is not defined.");

            //Get Pre Image
            Entity entity = context.PreEntityImages != null && context.PreEntityImages.Contains(PluginHelper.PreImageAlias) ? context.PreEntityImages[PluginHelper.PreImageAlias] : null;

            if (entity == null)
                throw new ArgumentNullException($"preImage entity is not defined. (MemberPlanAdjustmentOnStateChangePost Plguin) ");

            tracingService.Trace("Get the record's state & status");
            OptionSetValue status = (OptionSetValue)context.InputParameters["Status"];

            //If 
            if (status.Value != (int)AdjustmentStatusCode.Complete)
                return;
            
            // If Type is NSF
            tracingService.Trace("If Type is NSF");
            if (entity.Attributes.Contains("bpa_type"))
            {
                Guid executeUser = context.UserId;
                string crmAdminUserId = string.Empty;
                try
                {
                    crmAdminUserId = PluginConfiguration[PluginHelper.ConfigCrmAdmin];
                }
                catch (Exception ex)
                {
                    // Get value from Database
                    crmAdminUserId = ConfigurationHelper.FetchValueId(service, PluginHelper.ConfigCrmAdmin);
                }
                if (string.IsNullOrEmpty(crmAdminUserId))
                {
                    tracingService.Trace("ERROR FROM AdjustmentOnStateChangePost Plugin - CRM Admin USer Could not Found.");
                    throw new InvalidPluginExecutionException(OperationStatus.Failed, $@"The given key was not present in the dictionary. Please contact BPA Administrator");
                }
                IOrganizationServiceFactory serviceFactory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
                IOrganizationService serviceAdmin = serviceFactory.CreateOrganizationService(new Guid(crmAdminUserId));

                int type = entity.GetAttributeValue<OptionSetValue>("bpa_type").Value;

                decimal amount = 0;
                if (entity.Contains("bpa_amount"))
                    amount = (entity.GetAttributeValue<Money>("bpa_amount").Value * -1);

                EntityReference employer = null;
                if (entity.Contains("bpa_accountagreementid"))
                    employer = entity.GetAttributeValue<EntityReference>("bpa_accountagreementid");

                EntityReference submission = null;
                if (entity.Contains("bpa_submission"))
                    submission = entity.GetAttributeValue<EntityReference>("bpa_submission");
                DateTime workMonth = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddMonths(-1);


                if (type == (int)AdjustmentbpaType.Nsf)
                {
                    bool IsNSFPaid = false;
                    if (entity.Contains("bpa_nsfpaid"))
                        IsNSFPaid = entity.GetAttributeValue<bool>("bpa_nsfpaid");

                    DateTime NSFPaidDate = DateTime.MinValue;
                    if (entity.Contains("bpa_nsfpaiddate"))
                        NSFPaidDate = entity.GetAttributeValue<DateTime>("bpa_nsfpaiddate");

                    if (!IsNSFPaid || NSFPaidDate == DateTime.MinValue)
                        throw new InvalidPluginExecutionException(OperationStatus.Failed, $@"The NSF Paid and Date fields must be populated before deactivating adjustment");

                    // See if there is an amount to the adjustment
                    tracingService.Trace("See if there is an amount to the adjustment");
                    if (employer != null)
                        PluginHelper.TriggerRollup(service, tracingService, PluginHelper.AccountAgreement, employer.Id, "bpa_totalnsfamount");
                }
                else if(type == (int)AdjustmentbpaType.InterestPaid)
                {
                    //Create Intereset Redirect Adjustment
                    Guid adjustmentid = SubmissionAdjustmentHelper.CreateAdjustment(serviceAdmin, tracingService, workMonth, employer, 
                        (int)AdjustmentbpaType.InterestRedirection, amount, "Interest Redirect from Paid", submission);
                    
                    //Make record completed
                    PluginHelper.DeactivateRecord(serviceAdmin, PluginHelper.BpaSubmissionadjustment, adjustmentid, (int)AdjustmentStatusCode.Complete);
                    
                    //update employment rollup trigger
                    PluginHelper.TriggerRollup(service, tracingService, PluginHelper.AccountAgreement, employer.Id, "bpa_totalinterestcharge");
                }
                else if (type == (int)AdjustmentbpaType.InterestRedirection)
                {
                    //Create Intereset Paid Adjustment
                    Guid adjustmentid = SubmissionAdjustmentHelper.CreateAdjustment(serviceAdmin, tracingService, workMonth, employer, 
                        (int)AdjustmentbpaType.InterestPaid, amount, "Interest Paid from Redirection", submission);
                    
                    //Make record completed
                    PluginHelper.DeactivateRecord(serviceAdmin, PluginHelper.BpaSubmissionadjustment, adjustmentid, (int)AdjustmentStatusCode.Complete);

                    //update employment rollup trigger
                    PluginHelper.TriggerRollup(service, tracingService, PluginHelper.AccountAgreement, employer.Id, "bpa_totalinterestcharge");
                }
                else if (type == (int)AdjustmentbpaType.InterestWaived)
                {
                    //update employment rollup trigger
                    PluginHelper.TriggerRollup(service, tracingService, PluginHelper.AccountAgreement, employer.Id, "bpa_totalinterestcharge");
                }

                //update account Variance
                tracingService.Trace("If the amount value has changed with the update");
                SubmissionAdjustmentHelper.UpdateAccountVariance(serviceAdmin, tracingService, entity, null);
                
            }
        }
    }
}