﻿#region

using System;
using BPA.Benefit.Management.CRM.Plugins.Helper;
using Microsoft.Xrm.Sdk;

#endregion

namespace BPA.Benefit.Management.CRM.Plugins
{
    /// <summary>
    ///     LabourRoleOnUpdatePost PlugIn.
    /// </summary>
    public class LabourRoleOnUpdatePost : Plugin
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="LabourRoleOnUpdatePost" /> class.
        /// </summary>
        public LabourRoleOnUpdatePost() : base(typeof(LabourRoleOnUpdatePost))
        {
           
            RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(40, "Update",
                PluginHelper.BpaLabourrole, ExecuteOnUpdatePost));

            // Note : you can register for more events here if this plugin is not specific to an individual entity and message combination.
            // You may also need to update your RegisterFile.crmregister plug-in registration file to reflect any change.
        }

        /// <summary>
        ///     Executes the plug-in.
        /// </summary>
        /// <param name="localContext">
        ///     The <see cref="Plugin.LocalPluginContext" /> which contains the
        ///     <see cref="IPluginExecutionContext" />,
        ///     <see cref="IOrganizationService" />
        ///     and <see cref="ITracingService" />
        /// </param>
        /// <remarks>
        ///     For improved performance, Microsoft Dynamics CRM caches plug-in instances.
        ///     The plug-in's Execute method should be written to be stateless as the constructor
        ///     is not called for every invocation of the plug-in. Also, multiple system threads
        ///     could execute the plug-in at the same time. All per invocation state information
        ///     is stored in the context. This means that you should not use global variables in plug-ins.
        /// </remarks>

        protected void ExecuteOnUpdatePost(LocalPluginContext localContext)
        {
            // If local context not passed in, raise error
            if (localContext == null)
                throw new ArgumentNullException($"Local context not passed in.");

            // Create the context variables
            IPluginExecutionContext context = localContext.PluginExecutionContext;
            IOrganizationService service = localContext.OrganizationService;
            ITracingService tracingService = localContext.TracingService;

            // If Target is not passed in, raise error
            tracingService.Trace("If Target is not passed in, raise error");
            if (!context.InputParameters.Contains(PluginHelper.Target) ||
                !(context.InputParameters[PluginHelper.Target] is Entity))
                throw new ArgumentNullException($"Target is not of type Entity.");

            // Create the target entity & preImage
            tracingService.Trace("Create the target entity & PostImage");
            Entity entity = (Entity)context.InputParameters[PluginHelper.Target];
            Entity postImage = context.PostEntityImages != null && context.PostEntityImages.Contains(PluginHelper.PostImageAlias) ? context.PostEntityImages[PluginHelper.PostImageAlias] : null;
            Entity preImage = context.PreEntityImages != null && context.PreEntityImages.Contains(PluginHelper.PreImageAlias) ? context.PreEntityImages[PluginHelper.PreImageAlias] : null;

            if (postImage == null)
                throw new ArgumentNullException($"PostImage entity is not defined.");

            if (preImage == null)
                throw new ArgumentNullException($"PreImage entity is not defined.");

            string PostName = string.Empty;
            if (postImage.Contains("bpa_name"))
                PostName = postImage.GetAttributeValue<string>("bpa_name");

            string PreName = string.Empty;
            if (preImage.Contains("bpa_name"))
                PreName = preImage.GetAttributeValue<string>("bpa_name");


            if (PostName != PreName)
            {
                // Fetch All Rate Job Classification and Update
                EntityCollection collection = LabourRoleHelper.FetchAllRateJobClassificationByJobClassificationId(service, tracingService, entity.Id);

                if(collection != null && collection.Entities.Count > 0)
                {
                    foreach(Entity e in collection.Entities)
                    {
                        Entity update = new Entity(PluginHelper.BpaRateJobClassification)
                        {
                            Id = e.Id,
                            ["bpa_name"] = PostName
                        };
                        service.Update(update);
                    }
                }
            }
        }
    }
}