﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BPA.Benefit.Management.CRM.Plugins.Helper;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;

namespace BPA.Benefit.Management.CRM.Plugins
{
    public class GeneratePaymentEFT : IPlugin
    {
        public void Execute(IServiceProvider serviceProvider)
        {

            IPluginExecutionContext context = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));

            IOrganizationServiceFactory serviceFactory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));

            // Create service with context of current user
            IOrganizationService service = serviceFactory.CreateOrganizationService(context.UserId);

            //create tracing service
            ITracingService tracingService = (ITracingService)serviceProvider.GetService(typeof(ITracingService));


            //To get access to the image of the Quote record
            EntityReference entityRef = context.InputParameters["Target"] as EntityReference;

            //1. Do basic validation that the batch is ready for processing
            //2. Get bank information from fund
            //3. Loop over all payments below
            //4. Create a new file
            //5. Add header row
            //6. Add customer information row
            //7. Add row for each payment
            //8. Add row for footer with totals
            //9. Create Payment Print Audit recorc on successfull EFT operation
            //10. Create note and attach file to Payment Print Audit

            #region Starting File Creation

            //string folderLocation = @"E:\Positive Pay Files";
            //System.IO.Directory.CreateDirectory(folderLocation);
            string filename = "EFT - Credit - " + DateTime.Now.ToString("ddMMMyyyy hhmmsstt") + ".txt";
            //string logFilePath = folderLocation + "\\" + filename;

            string header = string.Empty;
            string footer = string.Empty;
            string fileCreationNumber = string.Empty;
            string customerInformation = string.Empty;
            string fileContents = string.Empty;
            StringBuilder paymentRows = new StringBuilder();
            StringBuilder footerRow = new StringBuilder();

            //Initialize variables for file footer
            string customerNumber = string.Empty;
            int numberOfCreditPayments = 0;
            int numberOfDebitPayments = 0;
            decimal totalCreditTransactions = decimal.Zero;
            decimal totalDebitTransactions = decimal.Zero;

            #endregion

            try
            {
                //Getting banking information from Trust and Fund
                

                string headerFetchXml = $@"
                <fetch>
                  <entity name='bpa_paymentbatch' >
                    <attribute name='bpa_name' />
                    <attribute name='bpa_paymentmethod' />
                    <filter>
                      <condition attribute='bpa_paymentbatchid' operator='eq' value='{entityRef.Id.ToString()}' />
                    </filter>
                    <link-entity name='bpa_trust' from='bpa_trustid' to='bpa_trustid' link-type='inner' >
                      <attribute name='bpa_originatorshortname' alias='bpa_originatorshortname' />
                      <attribute name='bpa_returninstitutioncode' alias='bpa_returninstitutioncode' />
                      <attribute name='bpa_originatorlongname' alias='bpa_originatorlongname' />
                      <attribute name='bpa_returnbranchtransitnumber' alias='bpa_returnbranchtransitnumber' />
                      <attribute name='bpa_eftcustomernumber' alias='bpa_eftcustomernumber' />
                      <attribute name='bpa_returnaccountnumber' alias='bpa_returnaccountnumber' />
                    </link-entity>
                  </entity>
                </fetch>";

                EntityCollection headerResult = service.RetrieveMultiple(new FetchExpression(headerFetchXml));
                Entity headerInfo = null;

                if (headerResult != null && headerResult.Entities.Count > 0)
                    headerInfo = headerResult.Entities[0];

               

                if (headerInfo != null)
                {
                    #region A - Header Row

                    string batchName = headerInfo.GetAttributeValue<String>("bpa_name");

                    customerNumber = headerInfo.GetAttributeValue<String>("bpa_eftcustomernumber");
                    fileCreationNumber = "0000";// Value has to be 0000 while uploading test files then switch over to fileCreationNumber; //batchName.Substring(batchName.Length - 4, 4);

                    header = "A";
                    header += "000000001";
                    header += headerInfo.GetAttributeValue<String>("bpa_eftcustomernumber");
                    header += fileCreationNumber;
                    header += "0" + DateTime.Now.Year.ToString().Substring(2, 2) + DateTime.Now.DayOfYear.ToString().PadLeft(3, '0');
                    header += "00220";
                    header += "D";
                    header += new string(' ', 6);
                    header += new string(' ', 11);
                    header += new string(' ', 52);

                    #endregion

                    //Getting Customer Record
                    #region Y - Customer Information Row

                    customerInformation = "Y";
                    customerInformation += headerInfo.GetAttributeValue<String>("bpa_originatorshortname").ToUpper().PadRight(15,' ');//from the trust, get originator short name
                    customerInformation += headerInfo.GetAttributeValue<String>("bpa_originatorlongname").ToUpper().PadRight(30, ' ');//from the trust, get originator long name
                    customerInformation += headerInfo.GetAttributeValue<String>("bpa_returninstitutioncode").PadRight(3,' ');;//Return institution code
                    customerInformation += headerInfo.GetAttributeValue<String>("bpa_returnbranchtransitnumber").PadRight(5, ' '); ;//Return branch transit number
                    customerInformation += headerInfo.GetAttributeValue<String>("bpa_returnaccountnumber").PadRight(12, ' '); ;//Return account number
                    customerInformation += new string(' ', 39);

                    #endregion

                }

                //Getting individual payment information
                #region Body Rows

          

                #region Credit Transaction

                if (headerInfo.GetAttributeValue<OptionSetValue>("bpa_paymentmethod").Value == (int)PaymentMethod.EFT_ToMember)
                {
                    string paymentFetchXml = $@"
                                    <fetch>
                                        <entity name='bpa_payment' >
                                        <attribute name='bpa_name' />
                                        <attribute name='bpa_paymentamount' />
                                        <attribute name='bpa_paymentto' />
                                        <filter>
                                            <condition attribute='bpa_paymentbatchid' operator='eq' value='{entityRef.Id}' />
                                        </filter>
                                        <link-entity name='bpa_memberplan' from='bpa_memberplanid' to='bpa_memberplanid' link-type='inner' alias='MP' >
                                            <link-entity name='contact' from='contactid' to='bpa_contactid' link-type='inner' alias='C' >
                                            <attribute name='bpa_bankaccountnumber' />
                                            <attribute name='bpa_banktransitnumber' />
                                            <attribute name='bpa_accountholdername' />
                                            <attribute name='bpa_banknumber' />
                                            </link-entity>
                                        </link-entity>
                                        </entity>
                                    </fetch>";

                    EntityCollection payments = service.RetrieveMultiple(new FetchExpression(paymentFetchXml));

                    numberOfCreditPayments = payments.Entities.Count;

                    if (payments != null && payments.Entities.Count > 0)
                    {
                        foreach (Entity payment in payments.Entities)
                        {
                            totalCreditTransactions += payment.GetAttributeValue<Money>("bpa_paymentamount").Value;
                            string paymentRow = string.Empty;

                            paymentRow = "C";
                            paymentRow += "200";//CPA transaction type - Payroll Deposit
                            paymentRow += payment.GetAttributeValue<Money>("bpa_paymentamount").Value.ToString("0.00").Replace(".", "").PadLeft(10, '0');
                            paymentRow += ("0" + DateTime.Now.Year.ToString().Substring(2, 2) + DateTime.Now.DayOfYear.ToString().PadLeft(3, '0')).PadRight(6,'0');//Payment Due Date
                            paymentRow += (payment.Attributes.Contains("C.bpa_banknumber") ? (string)payment.GetAttributeValue<AliasedValue>("C.bpa_banknumber").Value : string.Empty).PadRight(3,'0');
                            paymentRow += (payment.Attributes.Contains("C.bpa_banktransitnumber") ? (string)payment.GetAttributeValue<AliasedValue>("C.bpa_banktransitnumber").Value : string.Empty).PadRight(5, '0');
                            paymentRow += (payment.Attributes.Contains("C.bpa_bankaccountnumber") ? ((string)payment.GetAttributeValue<AliasedValue>("C.bpa_bankaccountnumber").Value).PadRight(12, '0') : string.Empty);
                            paymentRow += payment.GetAttributeValue<String>("bpa_paymentto").ToUpper().PadRight(30, '0');
                            paymentRow += payment.GetAttributeValue<String>("bpa_name").PadLeft(19, '0');//Cross-Reference
                            paymentRow += payment.GetAttributeValue<String>("bpa_name").PadLeft(15, '0');//Sundry Info
                            paymentRow += " ";


                            paymentRows.AppendLine(paymentRow);
                        }
                    }

                }

                #endregion

                #region Debit Transaction

                if (headerInfo.GetAttributeValue<OptionSetValue>("bpa_paymentmethod").Value == (int)PaymentMethod.EFT_FromEmployer)
                {
                    string paymentFetchXml = $@"
                    <fetch>
                        <entity name='bpa_payment' >
                        <attribute name='bpa_paymentto' />
                        <attribute name='bpa_banktransitnumber' />
                        <attribute name='bpa_name' />
                        <attribute name='bpa_banknumber' />
                        <attribute name='bpa_bankaccountnumber' />
                        <attribute name='bpa_paymentamount' />
                        <filter>
                            <condition attribute='bpa_paymentbatchid' operator='eq' value='{entityRef.Id.ToString()}' />
                        </filter>
                        </entity>
                    </fetch>";

                    EntityCollection payments = service.RetrieveMultiple(new FetchExpression(paymentFetchXml));

                    numberOfDebitPayments = payments.Entities.Count;

                    if (payments != null && payments.Entities.Count > 0)
                    {
                        foreach (Entity payment in payments.Entities)
                        {
                            totalDebitTransactions += payment.GetAttributeValue<Money>("bpa_paymentamount").Value;
                            string paymentRow = string.Empty;

                            paymentRow = "D";
                            paymentRow += "200";//CPA transaction type - Payroll Deposit
                            paymentRow += payment.GetAttributeValue<Money>("bpa_paymentamount").Value.ToString().PadLeft(10, '0');
                            paymentRow += ("0" + DateTime.Now.Year.ToString().Substring(2, 2) + DateTime.Now.DayOfYear.ToString().PadLeft(3, '0')).PadRight(6, '0');//Payment Due Date
                            paymentRow += " ";
                            paymentRow += (payment.Attributes.Contains("C.bpa_banknumber") ? (string)payment.GetAttributeValue<AliasedValue>("C.bpa_banknumber").Value : string.Empty).PadRight(3, '0');
                            paymentRow += (payment.Attributes.Contains("C.bpa_banktransitnumber") ? (string)payment.GetAttributeValue<AliasedValue>("C.bpa_banktransitnumber").Value : string.Empty).PadRight(5, '0');
                            paymentRow += (payment.Attributes.Contains("C.bpa_bankaccountnumber") ? ((string)payment.GetAttributeValue<AliasedValue>("C.bpa_bankaccountnumber").Value).PadRight(12, '0') : string.Empty);
                            paymentRow += payment.GetAttributeValue<String>("bpa_paymentto").ToUpper().PadRight(30, ' ');
                            paymentRow += payment.GetAttributeValue<String>("bpa_name").PadRight(19, ' ');//Cross-Reference
                            paymentRow += payment.GetAttributeValue<String>("bpa_name").PadRight(15, ' ');//Sundry Info
                            paymentRow += " ";


                            paymentRows.AppendLine(paymentRow);
                        }
                    }

                }

                #endregion

                #endregion

                //Getting footer info
                #region Footer Row

                footer = "Z";
                footer += new string(' ', 9);
                footer += customerNumber.PadRight(10, '0');
                footer += fileCreationNumber;
                footer += totalDebitTransactions.ToString("0.00").Replace(".","").PadLeft(14, '0');
                footer += numberOfDebitPayments.ToString().PadLeft(8, '0');
                footer += totalCreditTransactions.ToString("0.00").Replace(".", "").PadLeft(14, '0');
                footer += numberOfCreditPayments.ToString().PadLeft(8, '0');
                footer += new string(' ', 37);

                paymentRows.AppendLine(footer);

                #endregion

                #region Pushing rows to Memory Stream

                using (MemoryStream memoryStream = new MemoryStream())
                {
                    StreamWriter streamWriter = new StreamWriter(memoryStream);

                    streamWriter.WriteLine(header);

                    streamWriter.WriteLine(customerInformation);

                    streamWriter.Write(paymentRows.ToString());

                    //streamWriter.WriteLine(footerRow.ToString());

                    streamWriter.Flush();

                    memoryStream.Position = 0;
                    var streamReader = new StreamReader(memoryStream);
                    fileContents = Convert.ToBase64String(memoryStream.ToArray());

                }

                #endregion

                //Create Payment Print Audit
                #region Payment Print Audit Record

                Entity paymentPrintAudit = new Entity("bpa_paymentprintaudit");
                paymentPrintAudit.Attributes["bpa_paymentbatchid"] = entityRef;
                paymentPrintAudit.Attributes["bpa_operationtype"] = new OptionSetValue(922070002);// EFT To Member
                

                Guid paymentPrintAuditId = service.Create(paymentPrintAudit);

                #endregion

                //Create Payment Print Audit Annotation + Attachment
                #region Payment Print Audit Note

                //Byte[] bytes = File.ReadAllBytes(logFilePath);
                //String file = Convert.ToBase64String(bytes);

                Entity paymentAuditNote = new Entity("annotation");

                paymentAuditNote.Attributes.Add("subject","Positive Pay File");
                paymentAuditNote["filename"] = filename;
                paymentAuditNote["documentbody"]= fileContents;
                //paymentAuditNote.Attributes.Add("notetext" , "This is a test");
                paymentAuditNote.Attributes.Add("mimetype" , "text/plain");
                paymentAuditNote.Attributes.Add("objectid" , new EntityReference("bpa_paymentprintaudit", paymentPrintAuditId));
                paymentAuditNote.Attributes.Add("objecttypecode" , 10073);

                service.Create(paymentAuditNote);

                #endregion

                //Copy the Positive Pay file to the network 
                #region Copy the EFT file to the network

                //File.Copy(logFilePath, ppFolderLocation + "\\" + filename,true);

                #endregion

                context.OutputParameters["Status"] = true;
            }
            catch(Exception ex)
            {
                context.OutputParameters["Status"] = false;
            }
            finally
            {
                //if (streamWriter != null)
                //    streamWriter.Close();
            }
        }

    }
}
