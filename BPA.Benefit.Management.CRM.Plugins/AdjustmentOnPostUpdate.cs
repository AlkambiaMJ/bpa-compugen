﻿#region

using System;
using BPA.Benefit.Management.CRM.Plugins.Helper;
using Microsoft.Xrm.Sdk;

#endregion

namespace BPA.Benefit.Management.CRM.Plugins
{
    /// <summary>
    ///     AdjustmentOnPostUpdate Plugin.
    /// </summary>
    public class AdjustmentOnPostUpdate : Plugin
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="AdjustmentOnPostUpdate" /> class.
        /// </summary>
        public AdjustmentOnPostUpdate(string unsecureString, string secureString)
            : base(typeof(AdjustmentOnPostUpdate), unsecureString, secureString)
        {
            RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(40, "Update",
                PluginHelper.BpaSubmissionadjustment, ExecutePostAdjustmentCreate));

            // Note : you can register for more events here if this plugin is not specific to an individual entity and message combination.
            // You may also need to update your RegisterFile.crmregister plug-in registration file to reflect any change.
        }

        /// <summary>
        ///     Executes the plug-in.
        /// </summary>
        /// <param name="localContext">
        ///     The <see cref="Plugin.LocalPluginContext" /> which contains the
        ///     <see cref="IPluginExecutionContext" />,
        ///     <see cref="IOrganizationService" />
        ///     and <see cref="ITracingService" />
        /// </param>
        /// <remarks>
        ///     For improved performance, Microsoft Dynamics CRM caches plug-in instances.
        ///     The plug-in's Execute method should be written to be stateless as the constructor
        ///     is not called for every invocation of the plug-in. Also, multiple system threads
        ///     could execute the plug-in at the same time. All per invocation state information
        ///     is stored in the context. This means that you should not use global variables in plug-ins.
        /// </remarks>
        protected void ExecutePostAdjustmentCreate(LocalPluginContext localContext)
        {
            // If local context not passed in, raise error
            if (localContext == null)
                throw new ArgumentNullException($"Local context not passed in.");

            // Create the context variables
            IPluginExecutionContext context = localContext.PluginExecutionContext;
            IOrganizationService service = localContext.OrganizationService;
            ITracingService tracingService = localContext.TracingService;
            IServiceProvider serviceProvider = localContext.ServiceProvider;

            // If Target is not passed in, raise error
            tracingService.Trace("If Target is not passed in, raise error.");
            if (!context.InputParameters.Contains(PluginHelper.Target) ||
                !(context.InputParameters[PluginHelper.Target] is Entity))
                throw new ArgumentNullException($"Target is not of type Entity.");

            // Create the target entity and postimage
            tracingService.Trace("Create the target entity and postimage");
            Entity entity = (Entity)context.InputParameters["Target"];
            Entity postImageEntity = context.PostEntityImages != null &&
                                     context.PostEntityImages.Contains(PluginHelper.PostImageAlias)
                ? context.PostEntityImages[PluginHelper.PostImageAlias]
                : null;

            int type = 0;

            if (entity.Contains("bpa_type"))
                type = entity.GetAttributeValue<OptionSetValue>("bpa_type").Value;
            else if (postImageEntity.Contains("bpa_type"))
                type = postImageEntity.GetAttributeValue<OptionSetValue>("bpa_type").Value;

            if(type == (int)AdjustmentbpaType.Nsf)
            {
                EntityReference account = postImageEntity.GetAttributeValue<EntityReference>("bpa_accountagreementid");
                PluginHelper.TriggerRollup(service, tracingService, PluginHelper.AccountAgreement, account.Id, "bpa_totalnsfamount");
            }
            Guid executeUser = context.UserId;
            string crmAdminUserId = string.Empty;
            try
            {
                crmAdminUserId = PluginConfiguration[PluginHelper.ConfigCrmAdmin];
            }
            catch(Exception ex)
            {
                // Get value from Database
                crmAdminUserId = ConfigurationHelper.FetchValueId(service, PluginHelper.ConfigCrmAdmin);
            }
            if (string.IsNullOrEmpty(crmAdminUserId))
            {
                tracingService.Trace("ERROR FROM AdjustmentOnPostUpdate Plugin - CRM Admin USer Could not Found.");
                throw new InvalidPluginExecutionException(OperationStatus.Failed, $@"The given key was not present in the dictionary. Please contact BPA Administrator");
            }
            IOrganizationServiceFactory serviceFactory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
            IOrganizationService serviceAdmin = serviceFactory.CreateOrganizationService(new Guid(crmAdminUserId));

            // If the amount value has changed with the update
            tracingService.Trace("If the amount value has changed with the update");
            SubmissionAdjustmentHelper.UpdateAccountVariance(serviceAdmin, tracingService, entity, null);
        }
    }
}