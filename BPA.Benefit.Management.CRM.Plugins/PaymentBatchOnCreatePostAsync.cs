﻿#region

using System;
using BPA.Benefit.Management.CRM.Plugins.Helper;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;

#endregion

namespace BPA.Benefit.Management.CRM.Plugins
{
    /// <summary>
    ///     PaymentBatchOnCreatePostAsync PlugIn.
    /// </summary>
    public class PaymentBatchOnCreatePostAsync : Plugin
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="PaymentBatchOnCreatePostAsync" /> class.
        /// </summary>
        public PaymentBatchOnCreatePostAsync()
            : base(typeof(PaymentBatchOnCreatePostAsync))
        {
            RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(40, "Create",
                "bpa_paymentbatch", ExecutePaymentBatchOnCreatePostAsync));

            // Note : you can register for more events here if this plugin is not specific to an individual entity and message combination.
            // You may also need to update your RegisterFile.crmregister plug-in registration file to reflect any change.
        }

        /// <summary>
        ///     Executes the plug-in.
        /// </summary>
        /// <param name="localContext">
        ///     The <see cref="Plugin.LocalPluginContext" /> which contains the
        ///     <see cref="IPluginExecutionContext" />,
        ///     <see cref="IOrganizationService" />
        ///     and <see cref="ITracingService" />
        /// </param>
        /// <remarks>
        ///     For improved performance, Microsoft Dynamics CRM caches plug-in instances.
        ///     The plug-in's Execute method should be written to be stateless as the constructor
        ///     is not called for every invocation of the plug-in. Also, multiple system threads
        ///     could execute the plug-in at the same time. All per invocation state information
        ///     is stored in the context. This means that you should not use global variables in plug-ins.
        /// </remarks>
        protected void ExecutePaymentBatchOnCreatePostAsync(LocalPluginContext localContext)
        {
            //// If local context not passed in, raise error
            //if (localContext == null)
            //    throw new ArgumentNullException($"Local context not passed in.");

            //// Create the context variables
            //IPluginExecutionContext context = localContext.PluginExecutionContext;
            //IOrganizationService service = localContext.OrganizationService;
            //ITracingService tracingService = localContext.TracingService;

            //// If Target is not passed in, raise error
            //tracingService.Trace("If Target is not passed in, raise error");
            //if (!context.InputParameters.Contains(PluginHelper.Target) ||
            //    !(context.InputParameters[PluginHelper.Target] is Entity))
            //    throw new ArgumentNullException($"Target is not of type Entity.");

            //// Create the target entity
            //tracingService.Trace("Create the target entity & preImage");
            //Entity entity = (Entity)context.InputParameters[PluginHelper.Target];

            //// If Annual Payment is selected
            //// Set the record to Processing
            //// Calcluate how many Payments need to be created
            //// Have Async process generate Payments and update the count on the Payment Batch
            //tracingService.Trace("If Annual Payment is selected");
            //if (entity.Contains("bpa_paymentbatchtype") != true ||
            //    ((OptionSetValue)entity.Attributes["bpa_paymentbatchtype"]).Value != 922070001) return;

            //// Calcluate how many Payments need to be created
            //localContext.Trace("Calcluate how many Payments n1  `eed to be created.");
            //EntityReference trustRef = (EntityReference)entity.Attributes["bpa_trustid"];
            ////QueryExpression queryMemberPlans = new QueryExpression
            ////{
            ////    EntityName = "bpa_memberplan",
            ////    ColumnSet = new ColumnSet(true),
            ////    Criteria =
            ////    {
            ////        Filters =
            ////        {
            ////            new FilterExpression
            ////            {
            ////                FilterOperator = LogicalOperator.And,
            ////                Conditions =
            ////                {
            ////                    new ConditionExpression("bpa_vacationpaybank", ConditionOperator.GreaterThan, (decimal)0)
            ////                }
            ////            }
            ////        }
            ////    }
            ////};

            ////// Adding Link to the Plan Entity
            ////localContext.Trace("Adding Link to the Plan Entity");
            ////LinkEntity link = queryMemberPlans.AddLink("bpa_sector", "bpa_planid", "bpa_sectorid", JoinOperator.Inner);
            ////link.EntityAlias = "plan";
            ////queryMemberPlans.Criteria = new FilterExpression();
            ////queryMemberPlans.Criteria.AddCondition("plan", "bpa_trust", ConditionOperator.Equal, trustRef.Id);
            ////queryMemberPlans.PageInfo.ReturnTotalRecordCount = true;
            ////EntityCollection memberPlans = service.RetrieveMultiple(queryMemberPlans);

            //DateTime vacationEndDate = entity.Contains("bpa_vacationpayend") ? entity.GetAttributeValue<DateTime>("bpa_vacationpayend") : DateTime.MinValue;
            //if (vacationEndDate == DateTime.MinValue) return;
            //else
            //    vacationEndDate = new DateTime(vacationEndDate.Year, vacationEndDate.Month, vacationEndDate.Day);

            ////GEt all Member who has vacation in his bank
            //EntityCollection transactions = TransactionHelper.FetchAllMemberVacationPayByTrustId(service, tracingService, vacationEndDate.AddDays(1), trustRef.Id);
            //if ((transactions != null && transactions.Entities.Count == 0) || (transactions == null))
            //    return;
            //localContext.Trace("Number of Payment(s) to be generated: " + transactions.TotalRecordCount);

            //// Generate a Payment record for each Member Plan that matches the criteria 
            //// Creation of the Payment record will update the Payment Batch record's counter
            //foreach (Entity transaction in transactions.Entities)
            //{
            //    EntityReference memberPlan = transaction.Contains("bpa_memberplanid") ? (EntityReference)transaction.GetAttributeValue<AliasedValue>("bpa_memberplanid").Value : null;
            //    EntityReference planRef = transaction.Contains("bpa_planid") ? (EntityReference)transaction.GetAttributeValue<AliasedValue>("bpa_planid").Value : null;

            //    if ((memberPlan == null) || (planRef == null))
            //    {
            //        localContext.Trace("Either Plan or Member NOT defined, skip record.");
            //        continue;
            //    }


            //    // Verify that there isn't already a Vacation request for this Member Plan
            //    // if there is, skip them
            //    localContext.Trace("Verify that there isn't already a pending vacation payment for this member.");
            //    QueryExpression queryVacationPayment = new QueryExpression
            //    {
            //        EntityName = "bpa_payment",
            //        ColumnSet = new ColumnSet(true),
            //        Criteria =
            //        {
            //            Filters =
            //            {
            //                new FilterExpression
            //                {
            //                    FilterOperator = LogicalOperator.And,
            //                    Conditions =
            //                    {
            //                        new ConditionExpression("statecode", ConditionOperator.Equal, 0),
            //                        new ConditionExpression("bpa_memberplanid", ConditionOperator.Equal, memberPlan.Id)
            //                    }
            //                },
            //                new FilterExpression
            //                {
            //                    FilterOperator = LogicalOperator.Or,
            //                    Conditions =
            //                    {
            //                        new ConditionExpression("bpa_paymenttype", ConditionOperator.Equal, (int)BpaPaymentPaymentType.InterimVacation),
            //                        new ConditionExpression("bpa_paymenttype", ConditionOperator.Equal, (int)BpaPaymentPaymentType.AnnualVacation)
            //                    }
            //                }
            //            }
            //        },
            //        PageInfo = {ReturnTotalRecordCount = true}
            //    };

            //    EntityCollection vacationPayments = service.RetrieveMultiple(queryVacationPayment);

            //    // If there is already a vacation payment
            //    // Don't allow them to create a new one
            //    if (vacationPayments.Entities.Count > 0)
            //    {
            //        localContext.Trace("A vacation payment is already pending for Member:" + memberPlan.Name);
            //        continue;
            //    }

            //    localContext.Trace("Getting the vacation pay");
            //    //decimal vacationBank = memberPlan.Contains("bpa_vacationpaybank") ? memberPlan.GetAttributeValue<Money>("bpa_vacationpaybank").Value : 0;
            //    decimal vacationBank = transaction.Contains("bpa_dollarhour") ? ((Money)transaction.GetAttributeValue<AliasedValue>("bpa_dollarhour").Value).Value : 0;

            //    // Check if there is any money in the vacation bank
            //    localContext.Trace("Check if there is any money in the vacation bank");
            //    if (vacationBank <= 0)
            //    {
            //        localContext.Trace("No vacation money for Member:");
            //        continue;
            //    }

            //    // Get the payment amount from either the target or PreImage
            //    // Set the payment amount to be the same as the vacation bank
            //    localContext.Trace("Getting the payment amount.");
            //    Money paymentAmount = new Money {Value = vacationBank};

            //    // Create an Annual Vacation Payment record
            //    localContext.Trace("Make the new Payment record.");
            //    Guid paymentId = Guid.Empty;
            //    Entity payment = new Entity("bpa_payment");

            //    // Populate Payment record with Annual Vacation Details
            //    localContext.Trace("Add Trust ID");
            //    payment.Attributes.Add("bpa_trustid", (EntityReference)entity.Attributes["bpa_trustid"]);
            //    localContext.Trace("Add Payment Type");
            //    payment.Attributes.Add("bpa_paymenttype", new OptionSetValue((int)BpaPaymentPaymentType.AnnualVacation));
            //    localContext.Trace("Add Plan");
            //    //EntityReference planRef = (EntityReference)memberPlan.Attributes["bpa_planid"];
            //    //if (planRef == null)
            //    //{
            //    //    localContext.Trace("No Plan defined for this Member, skip record.");
            //    //    continue;
            //    //}
            //    localContext.Trace("Plan: " + planRef.Name);
            //    payment.Attributes.Add("bpa_planid", planRef);
            //    localContext.Trace("Add Member Plan");
            //    payment.Attributes.Add("bpa_memberplanid", memberPlan);
            //    localContext.Trace("Add Payment Date");
            //    payment.Attributes.Add("bpa_paymentdate", DateTime.Today);
            //    localContext.Trace("Add Activity Start Date");
            //    payment.Attributes.Add("bpa_activitystartdate", DateTime.Today);
            //    localContext.Trace("Add Payment Amount: " + paymentAmount.Value);
            //    payment.Attributes.Add("bpa_paymentamount", paymentAmount);
            //    localContext.Trace("Add Currency");
            //    if (entity.Contains("transactioncurrencyid")
            //        && entity.Attributes["transactioncurrencyid"] != null
            //        && ((EntityReference)entity.Attributes["transactioncurrencyid"]).Id != Guid.Empty)
            //    {
            //        localContext.Trace("Got Currency From Payment Batch");
            //        payment.Attributes.Add("transactioncurrencyid",
            //            (EntityReference)entity.Attributes["transactioncurrencyid"]);
            //    }
            //    payment.Attributes.Add("bpa_name", Guid.NewGuid().ToString());
            //    payment.Attributes.Add("bpa_paymentbatchid", entity.ToEntityReference());
            //    // Create the new Payment record
            //    localContext.Trace("Save the new Payment record.");
            //    paymentId = localContext.OrganizationService.Create(payment);


            //    localContext.Trace($@"After Payment Created: {paymentId.ToString()}");
            //    //Get all Transaction and update Payment 
            //    EntityCollection vacationTransactions = TransactionHelper.FetchAllVacationTransactionByMemberPlanId(service, tracingService, vacationEndDate.AddDays(1), memberPlan.Id);

            //    if(vacationTransactions != null && vacationTransactions.Entities.Count > 0)
            //    {
            //        foreach (Entity vacationTransaction in vacationTransactions.Entities)
            //        {
            //            TransactionHelper.UpdatePayment(service, tracingService, vacationTransaction.Id, paymentId);
            //        }
            //    }

            //} //end of  foreach (Entity transaction in transactions.Entities)


            //// Set the Generation Complete Date
            //Entity paymentBatch = new Entity("bpa_paymentbatch", entity.Id)
            //{
            //    Attributes = {["bpa_generatecompletedate"] = DateTime.Now}
            //};
            //service.Update(paymentBatch);

            ////Update BAtch Total
            //PluginHelper.TriggerRollup(service, tracingService, PluginHelper.BpaPaymentBatch, entity.Id, "bpa_batchtotal");
        }
    }
}