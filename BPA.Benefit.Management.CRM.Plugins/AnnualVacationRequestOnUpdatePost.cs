﻿//#region

//using System;
//using BPA.Benefit.Management.CRM.Plugins.Helper;
//using Microsoft.Xrm.Sdk;
//using Microsoft.Xrm.Sdk.Query;
//using System.Collections.Generic;

//#endregion

//namespace BPA.Benefit.Management.CRM.Plugins
//{
//    /// <summary>
//    ///     PaymentBatchOnCreatePostAsync PlugIn.
//    /// </summary>
//    public class AnnualVacationRequestOnUpdatePost : Plugin
//    {
//        /// <summary>
//        ///     Initializes a new instance of the <see cref="AnnualVacationRequestOnUpdatePost" /> class.
//        /// </summary>
//        public AnnualVacationRequestOnUpdatePost()
//            : base(typeof(AnnualVacationRequestOnUpdatePost))
//        {
//            RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(40, "Update", PluginHelper.BpaAnnualCacationRequest, ExecuteAnnualVacationRequestUpdatePost));
//            // Note : you can register for more events here if this plugin is not specific to an individual entity and message combination.
//            // You may also need to update your RegisterFile.crmregister plug-in registration file to reflect any change.
//        }

//        /// <summary>
//        ///     Executes the plug-in.
//        /// </summary>
//        /// <param name="localContext">
//        ///     The <see cref="Plugin.LocalPluginContext" /> which contains the
//        ///     <see cref="IPluginExecutionContext" />,
//        ///     <see cref="IOrganizationService" />
//        ///     and <see cref="ITracingService" />
//        /// </param>
//        /// <remarks>
//        ///     For improved performance, Microsoft Dynamics CRM caches plug-in instances.
//        ///     The plug-in's Execute method should be written to be stateless as the constructor
//        ///     is not called for every invocation of the plug-in. Also, multiple system threads
//        ///     could execute the plug-in at the same time. All per invocation state information
//        ///     is stored in the context. This means that you should not use global variables in plug-ins.
//        /// </remarks>
//        protected void ExecuteAnnualVacationRequestUpdatePost(LocalPluginContext localContext)
//        {
//            // If local context not passed in, raise error
//            if (localContext == null)
//                throw new ArgumentNullException($"Local context not passed in.");

//            // Create the context variables
//            IPluginExecutionContext context = localContext.PluginExecutionContext;
//            IOrganizationService service = localContext.OrganizationService;
//            ITracingService tracingService = localContext.TracingService;

//            tracingService.Trace("If Target is not passed in, raise error");
//            if (!context.InputParameters.Contains(PluginHelper.Target) ||
//                !(context.InputParameters[PluginHelper.Target] is Entity))
//                throw new ArgumentNullException($"Target is not of type Entity.");

//            // Create the target entity & preImage
//            tracingService.Trace("Create the target entity & preImage");
//            Entity entity = (Entity)context.InputParameters[PluginHelper.Target];
            
//            // Get the record's state & status
//            tracingService.Trace("Get the record's state & status");
//            OptionSetValue status = entity.Contains("statuscode") ? entity.GetAttributeValue<OptionSetValue>("statuscode") : new OptionSetValue(0);
//            EntityReference paymentBatch = entity.Contains("bpa_paymentbatchid") ? entity.GetAttributeValue<EntityReference>("bpa_paymentbatchid") : null;

//            if ((status.Value == (int)BpaAnnualVacationRequestStatusCode.Processing) && (paymentBatch != null) )
//            {
//                PluginHelper.DeactivateRecord(service, PluginHelper.BpaAnnualCacationRequest, entity.Id, (int)BpaAnnualVacationRequestStatusCode.Completed);
//            }
//        }
//    }
//}