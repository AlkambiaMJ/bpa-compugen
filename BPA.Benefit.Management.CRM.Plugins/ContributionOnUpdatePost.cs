﻿#region

using System;
using BPA.Benefit.Management.CRM.Plugins.Helper;
using Microsoft.Xrm.Sdk;

#endregion

namespace BPA.Benefit.Management.CRM.Plugins
{
    public class ContributionOnUpdatePost : Plugin
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="ContributionOnUpdatePost" /> class.
        /// </summary>
        public ContributionOnUpdatePost()
            : base(typeof(ContributionOnUpdatePost))
        {
            RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(40, "Update",
                PluginHelper.bpa_contribution, ExecutePostContributionUpdate));

            // Note : you can register for more events here if this plugin is not specific to an individual entity and message combination.
            // You may also need to update your RegisterFile.crmregister plug-in registration file to reflect any change.
        }

        /// <summary>
        ///     Executes the plug-in.
        /// </summary>
        /// <param name="localContext">
        ///     The <see cref="Plugin.LocalPluginContext" /> which contains the
        ///     <see cref="IPluginExecutionContext" />,
        ///     <see cref="IOrganizationService" />
        ///     and <see cref="ITracingService" />
        /// </param>
        /// <remarks>
        ///     For improved performance, Microsoft Dynamics CRM caches plug-in instances.
        ///     The plug-in's Execute method should be written to be stateless as the constructor
        ///     is not called for every invocation of the plug-in. Also, multiple system threads
        ///     could execute the plug-in at the same time. All per invocation state information
        ///     is stored in the context. This means that you should not use global variables in plug-ins.
        /// </remarks>
        protected void ExecutePostContributionUpdate(LocalPluginContext localContext)
        {
            // TODO: Can be removed, no longer needed
            // Check the ContributionHelper Class and clean up

            // If local context not passed in, raise error
            //if (localContext == null)
            //    throw new ArgumentNullException($"Local context not passed in.");

            //IPluginExecutionContext context = localContext.PluginExecutionContext;
            //IOrganizationService service = localContext.OrganizationService;
            //ITracingService tracingService = localContext.TracingService;
            //IServiceProvider serviceProvider = localContext.ServiceProvider;
            //if (context.InputParameters.Contains(PluginHelper.Target) && context.InputParameters[PluginHelper.Target] is Entity)
            //{
            //    Entity entity = (Entity)context.InputParameters["Target"];
            //    //ContributionHelper.CreateAndUpdateContribution(context, service, tracingService);
            //}
        }
    }
}