﻿#region

using System;
using BPA.Benefit.Management.CRM.Plugins.Helper;
using Microsoft.Xrm.Sdk;

#endregion

namespace BPA.Benefit.Management.CRM.Plugins
{
    /// <summary>
    ///     MemberPlanOnStateChangePreValidation Plugin.
    /// </summary>
    public class MemberPlanOnStateChangePreValidation : Plugin
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="AdjustmentOnStateChangePost" /> class.
        /// </summary>
        public MemberPlanOnStateChangePreValidation(string unsecureString, string secureString)
            : base(typeof(MemberPlanOnStateChangePreValidation), unsecureString, secureString)
        {
            RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(20, "SetState", PluginHelper.BpaMemberplan, ExecuteOnStateChangePreValidation));
            RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(20, "SetStateDynamicEntity", PluginHelper.BpaMemberplan, ExecuteOnStateChangePreValidation));

            // Note : you can register for more events here if this plugin is not specific to an individual entity and message combination.
            // You may also need to update your RegisterFile.crmregister plug-in registration file to reflect any change.
        }

        /// <summary>
        ///     Executes the plug-in.
        /// </summary>
        /// <param name="localContext">
        ///     The <see cref="Plugin.LocalPluginContext" /> which contains the
        ///     <see cref="IPluginExecutionContext" />,
        ///     <see cref="IOrganizationService" />
        ///     and <see cref="ITracingService" />
        /// </param>
        /// <remarks>
        ///     For improved performance, Microsoft Dynamics CRM caches plug-in instances.
        ///     The plug-in's Execute method should be written to be stateless as the constructor
        ///     is not called for every invocation of the plug-in. Also, multiple system threads
        ///     could execute the plug-in at the same time. All per invocation state information
        ///     is stored in the context. This means that you should not use global variables in plug-ins.
        /// </remarks>
        protected void ExecuteOnStateChangePreValidation(LocalPluginContext localContext)
        {
            // If local context not passed in, raise error
            if (localContext == null)
                throw new ArgumentNullException($"Local context not passed in.");

            // Create the context variables
            IPluginExecutionContext context = localContext.PluginExecutionContext;
            IOrganizationService service = localContext.OrganizationService;
            ITracingService tracingService = localContext.TracingService;
            IServiceProvider serviceProvider = localContext.ServiceProvider;
                        
            EntityReference targetEntity = (EntityReference)context.InputParameters[PluginHelper.EntityMoniker];
            if (targetEntity == null)
                throw new ArgumentNullException($"EntityReference is not defined.");

            ////Get Pre Image
            //Entity entity = context.PreEntityImages != null && context.PreEntityImages.Contains(PluginHelper.PreImageAlias) ? context.PreEntityImages[PluginHelper.PreImageAlias] : null;

            //if (entity == null)
            //    throw new ArgumentNullException($"preImage entity is not defined. (ContactOnStateChangePreValidation Plguin) ");

            tracingService.Trace("Get the record's state & status");
            OptionSetValue status = (OptionSetValue)context.InputParameters["Status"];

            // Get all Employment Records and Deactivate records
            EntityCollection collection = EmploymentHelper.FetchAllEmploymentRecords(service, tracingService, targetEntity.Id);

            if (collection != null && collection.Entities.Count > 0)
            {
                foreach (Entity er in collection.Entities)
                {
                    PluginHelper.DeactivateRecord(PluginHelper.BpaEmployment, er.Id, service);
                }
            }
        }
    }
}