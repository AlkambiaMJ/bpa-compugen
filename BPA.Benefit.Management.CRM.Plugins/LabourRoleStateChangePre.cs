﻿#region

using System;
using BPA.Benefit.Management.CRM.Plugins.Helper;
using Microsoft.Xrm.Sdk;

#endregion

namespace BPA.Benefit.Management.CRM.Plugins
{
    /// <summary>
    ///     LabourRoleOnCreateUpdatePre PlugIn.
    /// </summary>
    public class LabourRoleStateChangePre : Plugin
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="LabourRoleStateChangePre" /> class.
        /// </summary>
        public LabourRoleStateChangePre()
            : base(typeof(LabourRoleStateChangePre))
        {
            //base.RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(20, "SetState", PluginHelper.bpa_labourrole, new Action<LocalPluginContext>(ExecutePreLabourRoleStateChange)));
            RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(20, "SetStateDynamicEntity",
                PluginHelper.BpaLabourrole, ExecutePreLabourRoleStateChange));

            // Note : you can register for more events here if this plugin is not specific to an individual entity and message combination.
            // You may also need to update your RegisterFile.crmregister plug-in registration file to reflect any change.
        }

        /// <summary>
        ///     Executes the plug-in.
        /// </summary>
        /// <param name="localContext">
        ///     The <see cref="Plugin.LocalPluginContext" /> which contains the
        ///     <see cref="IPluginExecutionContext" />,
        ///     <see cref="IOrganizationService" />
        ///     and <see cref="ITracingService" />
        /// </param>
        /// <remarks>
        ///     For improved performance, Microsoft Dynamics CRM caches plug-in instances.
        ///     The plug-in's Execute method should be written to be stateless as the constructor
        ///     is not called for every invocation of the plug-in. Also, multiple system threads
        ///     could execute the plug-in at the same time. All per invocation state information
        ///     is stored in the context. This means that you should not use global variables in plug-ins.
        /// </remarks>

        protected void ExecutePreLabourRoleStateChange(LocalPluginContext localContext)
        {
            // If local context not passed in, raise error
            if (localContext == null)
                throw new ArgumentNullException($"Local context not passed in.");

            // Create the context variables
            IPluginExecutionContext context = localContext.PluginExecutionContext;
            IOrganizationService service = localContext.OrganizationService;
            ITracingService tracingService = localContext.TracingService;

            // If EntityReference is not passed in, raise error
            tracingService.Trace("If Target is not passed in, raise error");
            if (!context.InputParameters.Contains(PluginHelper.EntityMoniker) ||
                !(context.InputParameters[PluginHelper.EntityMoniker] is EntityReference))
                throw new ArgumentNullException($"EntityReference is not passed in.");

            // Create the EntityReference
            tracingService.Trace("Create the EntityReference");
            EntityReference targetEntity = (EntityReference)context.InputParameters[PluginHelper.EntityMoniker];
            if (targetEntity == null)
                throw new ArgumentNullException($"EntityReference is not defined.");
            if (targetEntity.LogicalName != PluginHelper.BpaLabourrole)
                return;

            // Get the record's status
            tracingService.Trace("Get the record's status");
            OptionSetValue status = (OptionSetValue)context.InputParameters["Status"];

            if (status.Value != 2) return;

            // Check to see if this record has active emloyment records associated with it
            tracingService.Trace("Check to see if this record has active emloyment records associated with it");
            if (EmploymentHelper.HasEmploymentRecordAssociated(service, tracingService, targetEntity.Id))
                throw new InvalidPluginExecutionException(OperationStatus.Canceled,
                    @"The job classification you are attempting to deactivate is currently used in active employment records. Please remove this job classification from these records prior to deactivating the record.");
        }
    }
}