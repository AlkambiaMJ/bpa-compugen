﻿#region

using System;
using BPA.Benefit.Management.CRM.Plugins.Helper;
using Microsoft.Xrm.Sdk;

#endregion

namespace BPA.Benefit.Management.CRM.Plugins
{
    /// <summary>
    ///     EmployerOnCreatePost PlugIn.
    /// </summary>
    public class EmployerOnCreatePost : Plugin
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="EmployerOnCreatePost" /> class.
        /// </summary>
        public EmployerOnCreatePost()
            : base(typeof(EmployerOnCreatePost))
        {
            RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(40, "Create",
                PluginHelper.account, ExecutePostAccountCreate));

            // Note : you can register for more events here if this plugin is not specific to an individual entity and message combination.
            // You may also need to update your RegisterFile.crmregister plug-in registration file to reflect any change.
        }

        /// <summary>
        ///     Executes the plug-in.
        /// </summary>
        /// <param name="localContext">
        ///     The <see cref="Plugin.LocalPluginContext" /> which contains the
        ///     <see cref="IPluginExecutionContext" />,
        ///     <see cref="IOrganizationService" />
        ///     and <see cref="ITracingService" />
        /// </param>
        /// <remarks>
        ///     For improved performance, Microsoft Dynamics CRM caches plug-in instances.
        ///     The plug-in's Execute method should be written to be stateless as the constructor
        ///     is not called for every invocation of the plug-in. Also, multiple system threads
        ///     could execute the plug-in at the same time. All per invocation state information
        ///     is stored in the context. This means that you should not use global variables in plug-ins.
        /// </remarks>
        protected void ExecutePostAccountCreate(LocalPluginContext localContext)
        {
            // TODO JK - Is this function needed anymore?  It isn't doing anything now...

            // If local context not passed in, raise error
            if (localContext == null)
                throw new ArgumentNullException($"Local context not passed in.");

            // Create the context variables
            IPluginExecutionContext context = localContext.PluginExecutionContext;
            ITracingService tracingService = localContext.TracingService;

            // If Target is not passed in, raise error
            tracingService.Trace("If Target is not passed in, raise error");
            if (!context.InputParameters.Contains(PluginHelper.Target) ||
                !(context.InputParameters[PluginHelper.Target] is Entity))
                throw new ArgumentNullException($"Target is not of type Entity.");

            // Create the target entity
            tracingService.Trace("Create the target entity");
            //Entity entity = (Entity)context.InputParameters[PluginHelper.Target];

            //OnCreateEmployer(service, entity);
        }

        //protected void OnCreateEmployer(IOrganizationService service, Entity entity)
        //{
        //    OptionSetValue categoryType = null;
        //    OptionSetValue accountStatus = null;
        //    EntityReference sector = null;

        //    if (entity.Attributes.Contains("bpa_sector"))
        //        sector = entity.GetAttributeValue<EntityReference>("bpa_sector");
        //    if (sector == null) return;

        //    if (entity.Attributes.Contains("accountcategorycode"))
        //        categoryType = entity.GetAttributeValue<OptionSetValue>("accountcategorycode");
        //    if (categoryType == null) return;

        //    if (entity.Attributes.Contains("bpa_accountstatus"))
        //        accountStatus = entity.GetAttributeValue<OptionSetValue>("bpa_accountstatus");
        //    if (accountStatus == null) return;

        //    //if ((categoryType.Value == (int)Employer_CategoryType.Union) &&
        //    //    ((categoryType.Value == (int)Employer_CategoryType.Payee) ||
        //    //     accountStatus.Value == (int)Employer_AccountStatus.Close)) return;

        //    DateTime introductiondate = DateTime.MinValue;
        //    DateTime currentMonth = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddMonths(-1);

        //    if (entity.Attributes.Contains("bpa_introductiondate"))
        //    {
        //        introductiondate = entity.GetAttributeValue<DateTime>("bpa_introductiondate");
        //        introductiondate = new DateTime(introductiondate.Year, introductiondate.Month, 1);
        //    }
        //    if (entity.Attributes.Contains("bpa_terminationdate"))
        //    {
        //        DateTime terminationDate = entity.GetAttributeValue<DateTime>("bpa_terminationdate");
        //        terminationDate = new DateTime(terminationDate.Year, terminationDate.Month, 1);

        //        if (terminationDate <= currentMonth)
        //            currentMonth = terminationDate;
        //    }

        //    if (introductiondate == DateTime.MinValue || (currentMonth < introductiondate)) return;
            
        //    //// Obtain the organization service reference.
        //    //IPluginExecutionContext context = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));
        //    //IOrganizationServiceFactory serviceFactory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
        //    //IOrganizationService service = serviceFactory.CreateOrganizationService(context.UserId);
        //    //Guid _callerId = PluginHelper.FetchDefaultUser(service, Sector.Id);
        //    //service = serviceFactory.CreateOrganizationService(_callerId); //Impersonate Service

        //    while (introductiondate <= currentMonth)
        //        introductiondate = introductiondate.AddMonths(1);
        //}
    }
}