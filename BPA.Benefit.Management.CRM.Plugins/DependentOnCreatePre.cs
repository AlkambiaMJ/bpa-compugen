﻿#region

using System;
using BPA.Benefit.Management.CRM.Plugins.Helper;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;

#endregion

namespace BPA.Benefit.Management.CRM.Plugins
{
    /// <summary>
    ///     DependentOnCreatePre Plugin.
    /// </summary>
    public class DependentOnCreatePre : Plugin
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="DependentOnCreatePre" /> class.
        /// </summary>
        public DependentOnCreatePre()
            : base(typeof(DependentOnCreatePre))
        {
            RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(20, "Create",
                PluginHelper.BpaDependant, ExecutePreDependentCreate));

            // Note : you can register for more events here if this plugin is not specific to an individual entity and message combination.
            // You may also need to update your RegisterFile.crmregister plug-in registration file to reflect any change.
        }

        /// <summary>
        ///     Executes the plug-in.
        /// </summary>
        /// <param name="localContext">
        ///     The <see cref="Plugin.LocalPluginContext" /> which contains the
        ///     <see cref="IPluginExecutionContext" />,
        ///     <see cref="IOrganizationService" />
        ///     and <see cref="ITracingService" />
        /// </param>
        /// <remarks>
        ///     For improved performance, Microsoft Dynamics CRM caches plug-in instances.
        ///     The plug-in's Execute method should be written to be stateless as the constructor
        ///     is not called for every invocation of the plug-in. Also, multiple system threads
        ///     could execute the plug-in at the same time. All per invocation state information
        ///     is stored in the context. This means that you should not use global variables in plug-ins.
        /// </remarks>
        protected void ExecutePreDependentCreate(LocalPluginContext localContext)
        {
            // If local context not passed in, raise error
            if (localContext == null)
                throw new ArgumentNullException($"Local context not passed in.");

            // Create the context variables
            IPluginExecutionContext context = localContext.PluginExecutionContext;
            IOrganizationService service = localContext.OrganizationService;
            ITracingService tracingService = localContext.TracingService;

            // If Target is not passed in, raise error
            tracingService.Trace("If Target is not passed in, raise error");
            if (!context.InputParameters.Contains(PluginHelper.Target) ||
                !(context.InputParameters[PluginHelper.Target] is Entity))
                throw new ArgumentNullException($"Target is not of type Entity.");

            // Create the target entity & preImage
            tracingService.Trace("Create the target entity & preImage");
            Entity entity = (Entity)context.InputParameters[PluginHelper.Target];

            // Get the parent Contact
            tracingService.Trace("Get the parent Member Plan");
            EntityReference member = null;
            if (entity.Attributes.Contains("bpa_contactid"))
                member = entity.GetAttributeValue<EntityReference>("bpa_contactid");
            if (member == null)
                throw new ArgumentNullException($"Member (Contact) is missing on this Dependant.");

            // Total up the number of dependants for the Contact record and add the Dependent Number to this record
            tracingService.Trace("Total up the number of dependants for the Contact record and add the Dependent Number to this record");
            int maxDependantNumber = ContactHelper.GetMaxDependantNumber(service, tracingService, member.Id) + 1;
            entity["bpa_dependantnumber"] = (maxDependantNumber).ToString();

            //Using the contact Id, get the member's SIN No Format and use that as the new "Legacy Identifier" This is used by the AS/400 
            string sinNoFormat = service.Retrieve("contact",member.Id,new ColumnSet(new string[] {"bpa_sinnoformat"})).GetAttributeValue<String>("bpa_sinnoformat");
            //Concatenate the dependant number to the SIN no format to make up the legacy identifier
            entity["bpa_legacyidentifier"] = sinNoFormat+ (maxDependantNumber).ToString();
        }
    }
}