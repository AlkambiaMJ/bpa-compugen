﻿#region

using System;
using BPA.Benefit.Management.CRM.Plugins.Helper;
using Microsoft.Xrm.Sdk;
using System.Collections.Generic;
using Microsoft.Xrm.Sdk.Query;

#endregion

namespace BPA.Benefit.Management.CRM.Plugins
{
    /// <summary>
    ///     MemberPlanOnUpdatePost PlugIn.
    /// </summary>
    public class MemberPlanOnUpdatePre : Plugin
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="MemberPlanOnUpdatePre" /> class.
        /// </summary>
        public MemberPlanOnUpdatePre(string unsecureString, string secureString)
            : base(typeof(MemberPlanOnUpdatePre), unsecureString, secureString)
        {
            RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(20, PluginHelper.Update,
                PluginHelper.BpaMemberplan, PreExecuteUpdate));

            // Note : you can register for more events here if this plugin is not specific to an individual entity and message combination.
            // You may also need to update your RegisterFile.crmregister plug-in registration file to reflect any change.
        }

        /// <summary>
        ///     Executes the plug-in.
        /// </summary>
        /// <param name="localContext">
        ///     The <see cref="Plugin.LocalPluginContext" /> which contains the
        ///     <see cref="IPluginExecutionContext" />,
        ///     <see cref="IOrganizationService" />
        ///     and <see cref="ITracingService" />
        /// </param>
        /// <remarks>
        ///     For improved performance, Microsoft Dynamics CRM caches plug-in instances.
        ///     The plug-in's Execute method should be written to be stateless as the constructor
        ///     is not called for every invocation of the plug-in. Also, multiple system threads
        ///     could execute the plug-in at the same time. All per invocation state information
        ///     is stored in the context. This means that you should not use global variables in plug-ins.
        /// </remarks>
        protected void PreExecuteUpdate(LocalPluginContext localContext)
        {
            // If local context not passed in, raise error
            if (localContext == null)
                throw new ArgumentNullException($"Local context not passed in.");

            // Create the context variables
            IPluginExecutionContext context = localContext.PluginExecutionContext;
            IOrganizationService service = localContext.OrganizationService;
            ITracingService tracingService = localContext.TracingService;
            IServiceProvider serviceProvider = localContext.ServiceProvider;

            tracingService.Trace(" Plugin Called - MemberPlanOnUpdatePre");

            // If Target is not passed in, raise error
            tracingService.Trace("If Target is not passed in, raise error");
            if (!context.InputParameters.Contains(PluginHelper.Target) ||
                !(context.InputParameters[PluginHelper.Target] is Entity))
                throw new ArgumentNullException($"Target is not of type Entity.");
            
            // Get the current user id
            Guid runningUserId = localContext.PluginExecutionContext.UserId;
            
            // Create the target entity & postImage
            tracingService.Trace("Create the target entity & postImage");
            Entity entity = (Entity)context.InputParameters[PluginHelper.Target];

            Entity preImageEntity = context.PreEntityImages != null && context.PreEntityImages.Contains(PluginHelper.PreImageAlias)
                                        ? context.PreEntityImages[PluginHelper.PreImageAlias] : null;
            if (preImageEntity == null)
                throw new ArgumentNullException($"preImage entity is not defined.");

            //Logic for making sure the drug card Id is unique
            if (entity.Contains("bpa_drugcardid"))
            {
                if (!MemberHelper.isDrugCardIdUnique(service, tracingService, entity.GetAttributeValue<String>("bpa_drugcardid").Trim()))
                    throw new InvalidPluginExecutionException(OperationStatus.Failed, "Drug Card ID Already Present in The System. Please Enter a Unique Drug Card ID.");
            }

            //Fetch Admin REcord
            //Assigned Organization service to CRM Admin User for Deletion of transaction
            //string crmAdminUserId = PluginConfiguration[PluginHelper.ConfigCrmAdmin];
            string crmAdminUserId = string.Empty;
            try
            {
                crmAdminUserId = PluginConfiguration[PluginHelper.ConfigCrmAdmin];
            }
            catch (Exception ex)
            {
                // Get value from Database
                crmAdminUserId = ConfigurationHelper.FetchValueId(service, PluginHelper.ConfigCrmAdmin);
            }
            if (string.IsNullOrEmpty(crmAdminUserId))
            {
                tracingService.Trace("ERROR FROM MemberPlanOnUpdatePre Plugin - CRM Admin USer Could not Found.");
                throw new InvalidPluginExecutionException(OperationStatus.Failed, $@"The given key was not present in the dictionary. Please contact BPA Administrator");
            }

            IOrganizationServiceFactory serviceFactory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
            IOrganizationService serviceAdmin = serviceFactory.CreateOrganizationService(new Guid(crmAdminUserId));
            tracingService.Trace($"Assigned to Organization Service");
            //bool executeMultiple = (PluginConfiguration[PluginHelper.ConfigExecuteMultipleRequest] == "1");
           
            string executeMultipleRequest = "0";
            try
            {
                executeMultipleRequest = PluginConfiguration[PluginHelper.ConfigExecuteMultipleRequest];
            }
            catch (Exception ex)
            {
                executeMultipleRequest = "0";
            }
            // Get Execute User
            bool executeMultiple = (executeMultipleRequest == "1");


            // Following method called only when Member Plan Status Change specially Reciprocal Transfer Out and Employed
            tracingService.Trace("Start logic ");
            DateTime workMonth = DateTime.MinValue;
            EntityReference plan = preImageEntity.Contains("bpa_planid") ? preImageEntity.GetAttributeValue<EntityReference>("bpa_planid") : null;

            #region -------------- RECIPROCAL TRANSFER IN/OUT ---------------
            //Need to put this checking because Retro processing also updaet the member plan status and we want to run this logic only when member transfer in BACK
            if (entity.Contains("bpa_benefitstatus") &&  entity.Contains("bpa_unionlocaleffectivedate"))
            {
                OptionSetValue memberPlanStatus = entity.GetAttributeValue<OptionSetValue>("bpa_benefitstatus");
                workMonth = entity.GetAttributeValue<DateTime>("bpa_unionlocaleffectivedate");

                if (workMonth == DateTime.MinValue)
                    return;
                
                Guid homeLocalId = Guid.Empty;
                if (entity.Contains("bpa_unionhomelocalid"))
                    homeLocalId = entity.GetAttributeValue<EntityReference>("bpa_unionhomelocalid").Id;

                EntityReference currentEligibility = ContactHelper.FetchCurrentEligibilityCategory(service, tracingService, entity.Id);
                int eligibilityOffset = EligibilityCategoryHelper.FetchEligibilityOffSet(service, currentEligibility.Id);
                //WorkMonth is equal to Union Local Effective Date
                workMonth = new DateTime(workMonth.Year, workMonth.Month, 1);

                string defaultCurrency = string.Empty;
                try
                {
                    defaultCurrency = PluginConfiguration[PluginHelper.ConfigDefaultCurrency];
                }
                catch (Exception ex)
                {
                    // Get value from Database
                    defaultCurrency = ConfigurationHelper.FetchValueId(service, PluginHelper.ConfigDefaultCurrency);
                }
                if (string.IsNullOrEmpty(defaultCurrency))
                {
                    tracingService.Trace("ERROR FROM MemberPlanOnUpdatePre Plugin - CRM Default Currency Could not Found.");
                    throw new InvalidPluginExecutionException(OperationStatus.Failed, $@"The given key was not present in the dictionary. Please contact BPA Administrator");
                }

                switch (memberPlanStatus.Value)
                {
                    case (int)MemberPlanBpaMemberPlanStatus.ReciprocalTransferOut:
                        #region ------------ TRANSFER OUT ------------------  
                        
                        //1]. Delete Eligibility status and Draw Transactions
                        //3]. Create Reciprocal Transfer Out Transaction for each month 

                        DateTime startWorkMonth = workMonth;
                        startWorkMonth = new DateTime(startWorkMonth.Year, startWorkMonth.Month, 1);

                        //Get the last workmonth
                        DateTime lastWorkMonth = TransactionHelper.GetLastWorkMonth(service, tracingService, entity.Id);
                        lastWorkMonth = new DateTime(lastWorkMonth.Year, lastWorkMonth.Month, 1);

                        tracingService.Trace($"LAst Work Month = {lastWorkMonth.ToString()}");

                        //Remove all Draw and eligibility status and eligibility Draw transactions
                        while (startWorkMonth <= lastWorkMonth)
                        {
                            EntityCollection removeTransactions = TransactionHelper.FetchAllTransactionsForReciprocal(service, tracingService, entity.Id, startWorkMonth);
                            if (removeTransactions != null && removeTransactions.Entities.Count > 0)
                            {
                                foreach (Entity e in removeTransactions.Entities)
                                {
                                    int transactionType = e.Contains("bpa_transactiontype") ? e.GetAttributeValue<OptionSetValue>("bpa_transactiontype").Value : 0;
                                    EntityReference MemberAdjustmentRef = (e.Contains("bpa_memberplanadjustmentid") ? e.GetAttributeValue<EntityReference>("bpa_memberplanadjustmentid") : null);

                                    tracingService.Trace($@"Transactin ID = {e.Id} AND transaction Type: {transactionType.ToString()}");
                                    int isAjustmentPaid = -1;
                                    if (MemberAdjustmentRef != null)
                                    {
                                        Entity MemberAdjustment = service.Retrieve(PluginHelper.BpaMemberPlanadjustment, MemberAdjustmentRef.Id, new ColumnSet(true));
                                        isAjustmentPaid = MemberAdjustment.Contains("statuscode") ? MemberAdjustment.GetAttributeValue<OptionSetValue>("statuscode").Value : -1;
                                    }
                                    
                                    if ((transactionType == (int)BpaTransactionbpaTransactionType.Reciprocal) && (isAjustmentPaid == (int)MemberAdjustmentStatusReason.Completed))
                                    {
                                        continue;
                                    }
                                    else
                                    {
                                        tracingService.Trace("Before Delete Transaction");
                                        //service.Delete("bpa_transaction", e.Id);
                                        serviceAdmin.Delete(PluginHelper.BpaTransaction, e.Id);
                                    }
                                    
                                }
                            }
                            startWorkMonth = startWorkMonth.AddMonths(1);
                        }

                        //Find Ending Balance for Adjustment
                        Dictionary<string, decimal> endingBalances = TransactionHelper.FetchEndingBalanceForReciprocal(service, entity.Id, lastWorkMonth.AddDays(1));
                        //Create Adjustment
                        //1]. Create Member Ajustment record with Type = 'Reciprocal Transfer OUT'

                        Guid memberAdjustmentId = Guid.Empty;
                        if ((endingBalances.ContainsKey("hours") && endingBalances["hours"] > 0) || (endingBalances.ContainsKey("dollarhours") && endingBalances["dollarhours"] > 0))
                        {
                            memberAdjustmentId = MemberAdjustmentHelper.CrateMemberAdjustmentTypeTransferINOUT(service, tracingService,
                                new EntityReference(PluginHelper.BpaMemberplan, entity.Id), plan, (int)MemberPlanBenefitAdjustmentType.TransferReciprocalOut,
                                "Member Reciprocal Transfer Out Requested", new EntityReference(PluginHelper.Account, homeLocalId), lastWorkMonth, 0, 
                                endingBalances, defaultCurrency);
                        }
                        //CREATE Transaction
                        startWorkMonth = workMonth;
                        while (startWorkMonth <= lastWorkMonth)
                        {
                            Dictionary<string, decimal> endingBalances1 = TransactionHelper.FetchEndingBalanceWhenLateContribution(service, entity.Id, 
                                startWorkMonth);

                            //Update Ending balance 
                            EntityCollection transactions = TransactionHelper.FetchAllTransactionsByMonth(service, tracingService, entity.Id, startWorkMonth);
                            if (transactions != null && transactions.Entities.Count > 0)
                            {
                                foreach (Entity tran in transactions.Entities)
                                {
                                    //Update Transaction ending balance
                                    Entity t = new Entity(PluginHelper.BpaTransaction)
                                    {
                                        Id = tran.Id
                                    };
                                    if (endingBalances1.ContainsKey("hours"))
                                        t["bpa_endinghourbalance"] = endingBalances1["hours"];
                                    if (endingBalances1.ContainsKey("dollarhours"))
                                        t["bpa_endingdollarbalance"] = endingBalances1["dollarhours"];
                                    if (endingBalances1.ContainsKey("selfpaydollar"))
                                        t["bpa_endingselfpaybalance"] = new Money(endingBalances1["selfpaydollar"]);
                                    if (endingBalances1.ContainsKey("secondarydollar"))
                                        t["bpa_secondarydollarbalance"] = new Money(endingBalances1["secondarydollar"]);

                                    serviceAdmin.Update(t);
                                }

                            }

                            //CREATE Transfer Out transaction
                            tracingService.Trace("Inside if (IsReciprocalTransferOutExists)");
                            TransactionHelper.CreateTransactionReciprocal(serviceAdmin, tracingService, entity.Id, startWorkMonth, (int)BpaTransactionbpaTransactionType.TransferOut,
                                homeLocalId, (int)MemberPlanBenefitAdjustmentType.TransferReciprocalOut, currentEligibility, memberAdjustmentId, 
                                startWorkMonth.AddMonths(eligibilityOffset), endingBalances1, null, Guid.Empty, true, (int)TransactionStatus.Posted); // Reciprocal - Transfer Out
                            
                            startWorkMonth = startWorkMonth.AddMonths(1);
                        }//end of while loop

                        entity["bpa_currentmontheligibility"] = new OptionSetValue((int)MemberPlanBpaCurrentEligibility.TransferOut);


                        #endregion  
                        break;
                    case (int)MemberPlanBpaMemberPlanStatus.Active:

                        #region ------------- TRANSFER IN -----------------------
                       
                        //Cancle all Member ADjustmet which are not complated (Update Status)
                        EntityCollection adjustments = MemberAdjustmentHelper.FetchAllUnPaidTransferOutAdjustments(service, tracingService, Guid.Empty, entity.Id,
                            workMonth);
                        if (adjustments != null && adjustments.Entities.Count > 0)
                        {
                            foreach (Entity adjustment in adjustments.Entities)
                            {
                                //Delete related Transactions
                                EntityCollection reciprocalTransactions = TransactionHelper.FetchAllTransactionByAdjustmentId(service, tracingService, adjustment.Id, 
                                    workMonth);
                                if(reciprocalTransactions != null && reciprocalTransactions.Entities.Count > 0)
                                {
                                    //Before delete transcation assigned organization service to CRM admin user
                                    //service = serviceFactory.CreateOrganizationService(new Guid(crmAdminUserId));

                                    foreach (Entity tran in reciprocalTransactions.Entities)
                                        serviceAdmin.Delete(PluginHelper.BpaTransaction, tran.Id);

                                    //TransactionHelper.DeleteTransactions(service, tran.Id);
                                }

                                //Assigned service back to atual running user
                                service = serviceFactory.CreateOrganizationService(runningUserId);
                                PluginHelper.DeactivateRecord(service, PluginHelper.BpaMemberPlanadjustment, adjustment.Id, (int)MemberAdjustmentStatusReason.Cancelled);
                            }
                        }

                        //Get ending Balance
                        Dictionary<string, decimal> endingBalancesIn = TransactionHelper.FetchEndingBalanceWhenLateContribution(service, entity.Id, workMonth.AddDays(1));

                        //Create Member Adjustment type = Transfer IN
                        Guid memberAdjustmentId1 = Guid.Empty;
                        if ((endingBalancesIn.ContainsKey("hours") && endingBalancesIn["hours"] > 0) || (endingBalancesIn.ContainsKey("dollarhours") && endingBalancesIn["dollarhours"] > 0))
                        {
                            memberAdjustmentId1 = MemberAdjustmentHelper.CrateMemberAdjustmentTypeTransferINOUT(service, tracingService,
                                new EntityReference(PluginHelper.BpaMemberplan, entity.Id), plan, (int)MemberPlanBenefitAdjustmentType.TransferReciprocalIn,
                                "Member Reciprocal Transfer IN Requested", new EntityReference(PluginHelper.Account, homeLocalId), workMonth, 0, endingBalancesIn,
                                defaultCurrency);
                        }
                        //CREATE Transfer IN
                        TransactionHelper.CreateTransactionReciprocal(serviceAdmin, tracingService, entity.Id, workMonth, (int)BpaTransactionbpaTransactionType.TransferIn, 
                            homeLocalId, (int)MemberPlanBenefitAdjustmentType.TransferReciprocalIn, currentEligibility, memberAdjustmentId1,
                            workMonth.AddMonths(eligibilityOffset), endingBalancesIn, null, Guid.Empty, false, (int)TransactionStatus.Posted);// Reciprocal - Transfer In
                        
                        // CREATE New Member if previous month does not have any elibigilty
                        Entity previousMonthEligibility = TransactionHelper.FetchPreviousMonthEligibility(service, entity.Id, workMonth.AddMonths(-1));
                        if (previousMonthEligibility == null)
                        {
                            TransactionHelper.CreateTransactionNewMember(service, tracingService, entity.Id, workMonth, workMonth.AddMonths(eligibilityOffset),
                                (int)BpaTransactionbpaTransactionType.NewMember, homeLocalId, currentEligibility); 
                        }

                        //Deactivate Member Adjustment
                        if(memberAdjustmentId1 != Guid.Empty)
                            PluginHelper.DeactivateRecord(service, PluginHelper.BpaMemberPlanadjustment, memberAdjustmentId1, (int)MemberAdjustmentStatusReason.Completed);
                        bool isRunFromMonthlyTrigger = false;

                        // Automatic Retro-Processing
                        // Optimized Process
                        //crmAdminUserId = PluginConfiguration[PluginHelper.ConfigCrmAdmin];
                        
                        try
                        {
                            crmAdminUserId = PluginConfiguration[PluginHelper.ConfigCrmAdmin];
                        }
                        catch (Exception ex)
                        {
                            // Get value from Database
                            crmAdminUserId = ConfigurationHelper.FetchValueId(service, PluginHelper.ConfigCrmAdmin);
                        }
                        if (string.IsNullOrEmpty(crmAdminUserId))
                        {
                            tracingService.Trace("ERROR FROM MemberPlanOnUpdatePre Plugin - CRM Admin USer Could not Found.");
                            throw new InvalidPluginExecutionException(OperationStatus.Failed, $@"The given key was not present in the dictionary. Please contact BPA Administrator");
                        }

                        string returnValue = MemberPlanRetroprocessing.RecalculateTransactions(
                            context, serviceProvider, service, tracingService, crmAdminUserId, executeMultiple,
                            entity.ToEntityReference(), workMonth, false, isRunFromMonthlyTrigger, null);

                        // Old Process
                        //string returnValue = MemberRetroprocessing_New.RecalculateTransaction(service, tracingService, new EntityReference(PluginHelper.BpaMemberplan,
                        //    entity.Id), workMonth, false, isRunFromMonthlyTrigger, null);

                        // FOR TESTING ONLY
                        //if (!string.IsNullOrEmpty(returnValue))
                        //    throw new InvalidPluginExecutionException(OperationStatus.Failed, returnValue);

                        entity["bpa_isretroprocessingrunfrommonthlytrigger"] = false;
                        entity["bpa_retroprocessingdate"] = null;

                        #endregion
                        break;

                    case (int)MemberPlanBpaMemberPlanStatus.Inactive:
                        entity["bpa_currentmontheligibility"] = new OptionSetValue((int)MemberPlanBpaCurrentEligibility.Inactive);
                        break;
                }
            }

            #endregion

            #region -------------- Run RETRO PROCESSNG MANUALLY -------
            DateTime retroProcessingDate = DateTime.MinValue;
            if (entity.Contains("bpa_retroprocessingdate") || entity.Contains("bpa_eligibilitycategoryid"))
            {
                Entity preImage = context.PreEntityImages != null && context.PreEntityImages.Contains(PluginHelper.PreImageAlias) ? context.PreEntityImages[PluginHelper.PreImageAlias] : null;
                retroProcessingDate = entity.Contains("bpa_retroprocessingdate") ? entity.GetAttributeValue<DateTime>("bpa_retroprocessingdate") : DateTime.MinValue;

                if (retroProcessingDate != DateTime.MinValue)
                    retroProcessingDate = new DateTime(retroProcessingDate.Year, retroProcessingDate.Month, 1);
                else
                    retroProcessingDate = preImage.Contains("bpa_retroprocessingdate") ? preImage.GetAttributeValue<DateTime>("bpa_retroprocessingdate") : DateTime.MinValue;

                #region ---------------------- RETRO PROCESSING VALIDATION -----------------

                OptionSetValue preBenefitStatus = preImage.Contains("bpa_benefitstatus") ? preImage.GetAttributeValue<OptionSetValue>("bpa_benefitstatus") : new OptionSetValue(0);
                if(preBenefitStatus.Value == (int)MemberPlanBpaMemberPlanStatus.Inactive)
                {
                    //Check Previous month is there any thing?
                    Entity previousMonthEligibility = RecalculationTransactionHelper.FetchPreviousMonthEligibility(service, entity.Id, retroProcessingDate);

                    if(previousMonthEligibility == null)
                    {
                        Entity maxWorkMonth = RecalculationTransactionHelper.FetchMaxEligibilityWorkMonth(service, entity.Id);
                        if(maxWorkMonth != null && maxWorkMonth.Contains("bpa_transactiondate") && maxWorkMonth.GetAttributeValue<AliasedValue>("bpa_transactiondate") != null)
                        {
                            DateTime lasteligibilitymonth = (DateTime)maxWorkMonth.GetAttributeValue<AliasedValue>("bpa_transactiondate").Value;
                            lasteligibilitymonth = new DateTime(lasteligibilitymonth.Year, lasteligibilitymonth.Month, 1);

                            if (lasteligibilitymonth != DateTime.MinValue && retroProcessingDate > lasteligibilitymonth)
                                throw new InvalidPluginExecutionException(OperationStatus.Failed, @"The Retro Processing Date you have selected is invalid. Please select the last Work month that this member was active for processing.");
                        }
                    }
                }

                #endregion
                
                EntityReference preEligibilityCategory = preImage.Contains("bpa_eligibilitycategoryid") ? preImage.GetAttributeValue<EntityReference>("bpa_eligibilitycategoryid") : null;
                EntityReference CurrentEligibilityCategory = null;

                if (entity.Contains("bpa_eligibilitycategoryid"))
                    CurrentEligibilityCategory = entity.GetAttributeValue<EntityReference>("bpa_eligibilitycategoryid");
                else
                    CurrentEligibilityCategory = preEligibilityCategory;
                
                bool isCategoryChanged = false;
                if (preEligibilityCategory.Id != CurrentEligibilityCategory.Id)
                    isCategoryChanged = true;

                if (retroProcessingDate != DateTime.MinValue)
                {
                    retroProcessingDate = new DateTime(retroProcessingDate.Year, retroProcessingDate.Month, 1);
                    //isCategoryChanged = true;
                }
                else
                    retroProcessingDate = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddMonths(-1);

                if (!ContactHelper.FetchMemberStatus(service, entity.Id))
                {
                    // Run Member Retro processing
                    bool isRunFromMonthlyTrigger = false;
                    EntityReference memberPlan = new EntityReference(PluginHelper.BpaMemberplan, entity.Id);

                    tracingService.Trace("Starting retroprocessing ");

                    // New optimized process
                    //string crmAdminUserId = PluginConfiguration[PluginHelper.ConfigCrmAdmin];
                    string retroprocessingMessage = MemberPlanRetroprocessing.RecalculateTransactions(context, serviceProvider, service, tracingService, crmAdminUserId, executeMultiple, memberPlan, retroProcessingDate, isCategoryChanged, isRunFromMonthlyTrigger, CurrentEligibilityCategory);

                    // Old Process
                    //string retroprocessingMessage = MemberRetroprocessing_Old.RecalculateTransaction(service, tracingService, memberPlan, retroProcessingDate, isCategoryChanged, isRunFromMonthlyTrigger, CurrentEligibilityCategory);
                    
                    // FOR TESTING ONLY
                    //if (!string.IsNullOrEmpty(retroprocessingMessage))
                        //throw new InvalidPluginExecutionException(retroprocessingMessage);
                }
            }
            #endregion
        }
    }
}