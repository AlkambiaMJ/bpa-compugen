﻿#region

using System;
using BPA.Benefit.Management.CRM.Plugins.Helper;
using Microsoft.Xrm.Sdk;

#endregion

namespace BPA.Benefit.Management.CRM.Plugins
{
    /// <summary>
    ///     DepositOnUpdatePre PlugIn.
    /// </summary>
    public class DepositOnUpdatePre : Plugin
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="DepositOnUpdatePre" /> class.
        /// </summary>
        public DepositOnUpdatePre()
            : base(typeof(DepositOnUpdatePre))
        {
            RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(20, "Update",
                PluginHelper.BpaDeposit, ExecutePreDepositUpdate));

            // Note : you can register for more events here if this plugin is not specific to an individual entity and message combination.
            // You may also need to update your RegisterFile.crmregister plug-in registration file to reflect any change.
        }

        /// <summary>
        ///     Executes the plug-in.
        /// </summary>
        /// <param name="localContext">
        ///     The <see cref="Plugin.LocalPluginContext" /> which contains the
        ///     <see cref="IPluginExecutionContext" />,
        ///     <see cref="IOrganizationService" />
        ///     and <see cref="ITracingService" />
        /// </param>
        /// <remarks>
        ///     For improved performance, Microsoft Dynamics CRM caches plug-in instances.
        ///     The plug-in's Execute method should be written to be stateless as the constructor
        ///     is not called for every invocation of the plug-in. Also, multiple system threads
        ///     could execute the plug-in at the same time. All per invocation state information
        ///     is stored in the context. This means that you should not use global variables in plug-ins.
        /// </remarks>
        protected void ExecutePreDepositUpdate(LocalPluginContext localContext)
        {
            // If local context not passed in, raise error
            if (localContext == null)
                throw new ArgumentNullException($"Local context not passed in.");

            // Create the context variables
            IPluginExecutionContext context = localContext.PluginExecutionContext;
            ITracingService tracingService = localContext.TracingService;
            IOrganizationService service = localContext.OrganizationService;

            // If Target is not passed in, raise error
            tracingService.Trace("If Target is not passed in, raise error");
            if (!context.InputParameters.Contains(PluginHelper.Target) ||
                !(context.InputParameters[PluginHelper.Target] is Entity))
                throw new ArgumentNullException($"Target is not of type Entity.");

            // Create the target entity & preImage
            tracingService.Trace("Create the target entity & preImage");
            Entity entity = (Entity)context.InputParameters[PluginHelper.Target];
            Entity preImageEntity = context.PreEntityImages != null &&
                                    context.PreEntityImages.Contains(PluginHelper.PreImageAlias)
                ? context.PreEntityImages[PluginHelper.PreImageAlias]
                : null;
            if (preImageEntity == null)
                throw new ArgumentNullException($"preImage entity is not defined.");

            // Complete the checks to see if we can complete the Deposit
            // Get the Deposit Status value
            tracingService.Trace("Get the Deposit Status value");
            if (!entity.Attributes.Contains("bpa_depositstatus")) return;
            int depositstatus = entity.GetAttributeValue<OptionSetValue>("bpa_depositstatus").Value;

            // If the Deposit Status isn't Complete, return
            tracingService.Trace("If the Deposit Status isn't Complete, return");
            if (depositstatus != 922070001) return;

            // Check that the Submission Total is populated
            tracingService.Trace("Check that the Submission Total is populated");
            Money submissionTotal = null;
            /*if (entity.Contains("bpa_submissiontotal"))
                submissionTotal = entity.GetAttributeValue<Money>("bpa_submissiontotal");
            else if (preImageEntity.Contains("bpa_submissiontotal"))
                submissionTotal = preImageEntity.GetAttributeValue<Money>("bpa_submissiontotal");
            */
            // Fetch from Database
            submissionTotal = DepositHelper.GetSumOfSubmissionsByDepositId(service, tracingService, entity.Id);

            if (submissionTotal == null)
                throw new InvalidPluginExecutionException(
                    "Error: You cannot save a record as either Submission Total or Total Deposit amount is blank.");
            
            // Check that the Total Cheque Amount is populated
            tracingService.Trace("Check that the Total Cheque Amount is populated");
            Money totalChequeAmount = null;
            if (entity.Contains("bpa_totalchequeamount"))
                totalChequeAmount = entity.GetAttributeValue<Money>("bpa_totalchequeamount");
            else if (preImageEntity.Contains("bpa_totalchequeamount"))
                totalChequeAmount = preImageEntity.GetAttributeValue<Money>("bpa_totalchequeamount");
            if (totalChequeAmount == null)
                throw new InvalidPluginExecutionException("You cannot save a record as Total Cheque Amount is blank.");

            // Check that the variance on the Deposit is zero
            tracingService.Trace("Check that the variance on the Deposit is zero");
            decimal variance = submissionTotal.Value - totalChequeAmount.Value;
            if (variance != 0)
                throw new InvalidPluginExecutionException(
                    "Deposit Amount is not Balanced. Please Balance deposit prior to changing status to \'Deposited\'. Please Refresh the screen.");
        }
    }
}