﻿#region

using System;
using System.Linq;
using BPA.Benefit.Management.CRM.Plugins.Helper;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;

#endregion

namespace BPA.Benefit.Management.CRM.Plugins
{
    /// <summary>
    ///     ContactOnCreatePre Plugin.
    /// </summary>
    public class ContactOnCreatePre : Plugin
    {
        private static object _lockingObject = new object();
        /// <summary>
        ///     Initializes a new instance of the <see cref="ContactOnCreatePre" /> class.
        /// </summary>
        public ContactOnCreatePre(string unsecureString, string secureString)
            : base(typeof(ContactOnCreatePre), unsecureString, secureString)
        {
            RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(20, "Create",
                PluginHelper.Contact, PreExecuteOnCreate));

            // Note : you can register for more events here if this plugin is not specific to an individual entity and message combination.
            // You may also need to update your RegisterFile.crmregister plug-in registration file to reflect any change.
        }

        /// <summary>
        ///     Executes the plug-in.
        /// </summary>
        /// <param name="localContext">
        ///     The <see cref="Plugin.LocalPluginContext" /> which contains the
        ///     <see cref="IPluginExecutionContext" />,
        ///     <see cref="IOrganizationService" />
        ///     and <see cref="ITracingService" />
        /// </param>
        /// <remarks>
        ///     For improved performance, Microsoft Dynamics CRM caches plug-in instances.
        ///     The plug-in's Execute method should be written to be stateless as the constructor
        ///     is not called for every invocation of the plug-in. Also, multiple system threads
        ///     could execute the plug-in at the same time. All per invocation state information
        ///     is stored in the context. This means that you should not use global variables in plug-ins.
        /// </remarks>
        protected void PreExecuteOnCreate(LocalPluginContext localContext)
        {
            // If local context not passed in, raise error
            if (localContext == null)
                throw new ArgumentNullException($"Local context not passed in.");

            // Create the context variables
            IPluginExecutionContext context = localContext.PluginExecutionContext;
            IOrganizationService service = localContext.OrganizationService;
            ITracingService tracingService = localContext.TracingService;
            IServiceProvider serviceProvider = localContext.ServiceProvider;

            // If Target is not passed in, raise error
            tracingService.Trace("If Target is not passed in, raise error");
            if (!context.InputParameters.Contains(PluginHelper.Target) ||
                !(context.InputParameters[PluginHelper.Target] is Entity))
                throw new ArgumentNullException($"Target is not of type Entity.");

            // Create the target entity
            tracingService.Trace("Create the target entity");
            Entity entity = (Entity)context.InputParameters[PluginHelper.Target];

            // Get the Contact Type and SIN Number
            tracingService.Trace("Get the Contact Type and SIN Number");
            OptionSetValue contactType = new OptionSetValue();
            if (entity.Contains("bpa_contacttype"))
                contactType = (OptionSetValue)entity.Attributes["bpa_contacttype"];
            string sin = string.Empty;
            if (entity.Contains("bpa_socialinsurancenumber"))
                sin = (string)entity.Attributes["bpa_socialinsurancenumber"];

            // If the contact type isn't a Member, return
            tracingService.Trace("If the contact type isn't a Member, return");
            if (contactType.Value != (int)ContactContactType.Member) return;

            // Get the local user ID
            tracingService.Trace("Get the local user ID");
            Guid executeCallerId = context.UserId;

            // Impersonate the Admin Account so Config value can be updated
            tracingService.Trace("Impersonate the Admin Account so Config value can be updated");
            //string callerId = (PluginConfiguration != null && PluginConfiguration.ContainsKey(PluginHelper.ConfigCrmAdmin) ? PluginConfiguration[PluginHelper.ConfigCrmAdmin] : string.Empty);
            string callerId = string.Empty;
            try
            {
                callerId = PluginConfiguration[PluginHelper.ConfigCrmAdmin];
            }
            catch (Exception ex)
            {
                // Get value from Database
                callerId = ConfigurationHelper.FetchValueId(service, PluginHelper.ConfigCrmAdmin);
            }
            if (string.IsNullOrEmpty("crmAdminUserId"))
            {
                tracingService.Trace("ERROR FROM ContactOnCreatePre Plugin - CRM Admin USer Could not Found.");
                throw new InvalidPluginExecutionException(OperationStatus.Failed, $@"The given key was not present in the dictionary. Please contact BPA Administrator");
            }
            tracingService.Trace($@"CRM ADmin: {callerId}");
            IOrganizationServiceFactory serviceFactory =
                (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));

            Guid adminId = Guid.Empty;
            if (!Guid.TryParse(callerId, out adminId))
                throw new InvalidPluginExecutionException(OperationStatus.Failed, $@"ContactOnCreatePre: System is not able to imparsonate Administrator");

            service = serviceFactory.CreateOrganizationService(adminId);
            string contactNumber = string.Empty;


            #region ---------- SIN Validation -----------------
            if (!string.IsNullOrEmpty(sin))
            {
                EntityCollection existsContacts = MemberHelper.FetchContactBySIN(service, tracingService, sin);
                if (existsContacts != null && existsContacts.Entities.Count > 0)
                    throw new InvalidPluginExecutionException(OperationStatus.Failed, "Social Insurance Number entered already exists. Record can not be created.");
            }
            #endregion



            int nextNumber = 0;
            lock (_lockingObject)
            {

                QueryExpression query = new QueryExpression("bpa_configuration");
                query.Criteria.AddCondition("statuscode", ConditionOperator.Equal, 1);
                query.Criteria.AddCondition("bpa_name", ConditionOperator.Equal, PluginHelper.SINNumber);
                query.ColumnSet = new ColumnSet("bpa_value", "bpa_prefix");

                EntityCollection ec = service.RetrieveMultiple(query);
                if (ec.Entities.Count != 0)
                {
                    // Take the first result
                    Entity config = ec.Entities[0];
                    if (config.Contains("bpa_prefix") && config.Contains("bpa_value"))
                    {
                        // Get Contact Prefix & Number
                        string prefix = (string)config["bpa_prefix"];
                        string value = (string)config["bpa_value"];

                        int contactValue = 0;
                        int.TryParse(value, out contactValue);
                        string generatedSin = contactValue.ToString().PadLeft(5, '0');
                        generatedSin = generatedSin.Substring(0, 2) + "-" +
                                       generatedSin.Substring(2, generatedSin.Length - 2);

                        // Concatenate prefix and number together
                        contactNumber = prefix + "" + generatedSin; // contactValue.ToString().PadLeft(3, '0');

                        // Set the Next number to update the configuration
                        nextNumber = contactValue + 1;
                    }
                    else
                        throw new InvalidPluginExecutionException("Configuration Entity is missing Next Contact Number");
                }
                else
                    throw new InvalidPluginExecutionException(
                        "Configuration Entity is missing.  Please add a Configuration record and a Next Contact Number.");
            }

            // Back to Execute Caller Id
            tracingService.Trace("Back to Execute Caller Id");
            
            // ReSharper disable once RedundantAssignment
            service = serviceFactory.CreateOrganizationService(executeCallerId);

            // If the SIN is empty
            tracingService.Trace("If the SIN is empty");
            if (string.IsNullOrEmpty(sin))
            {
                // Write out the generated SIN if it is Empty
                tracingService.Trace("Write out the generated SIN if it is Empty");
                entity["bpa_socialinsurancenumber"] = contactNumber;
                //Regardless of what the SIN number is coming from, strip out the formatting and add it to field for searching
                entity["bpa_sinnoformat"] = new String(contactNumber.ToCharArray().Where(char.IsDigit).ToArray());

                //Update last Sin Number
                //string crmAdminUserId = (PluginConfiguration != null && PluginConfiguration.ContainsKey(PluginHelper.ConfigCrmAdmin) ? PluginConfiguration[PluginHelper.ConfigCrmAdmin] : string.Empty);
                string crmAdminUserId = string.Empty;
                try
                {
                    crmAdminUserId = PluginConfiguration[PluginHelper.ConfigCrmAdmin];
                }
                catch (Exception ex)
                {
                    // Get value from Database
                    crmAdminUserId = ConfigurationHelper.FetchValueId(service, PluginHelper.ConfigCrmAdmin);
                }
                if (string.IsNullOrEmpty("crmAdminUserId"))
                {
                    tracingService.Trace("ERROR FROM ContactOnCreatePre Plugin - CRM Admin USer Could not Found.");
                    throw new InvalidPluginExecutionException(OperationStatus.Failed, $@"The given key was not present in the dictionary. Please contact BPA Administrator");
                }

                serviceFactory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
                service = serviceFactory.CreateOrganizationService(new Guid(crmAdminUserId));

                ConfigurationHelper.UpdateNextSINNumber(service, nextNumber.ToString());
                // Re-Assignment back to Service
                service = serviceFactory.CreateOrganizationService(executeCallerId);
            }
            else
            {
                // If the SIN typed in is higher than the generated SIN, use the generated one
                tracingService.Trace("If the SIN typed in is higher than the generated SIN, use the generated one");
                decimal enteredSin;
                decimal.TryParse(sin.Replace("-", string.Empty), out enteredSin);
                decimal tempSin;
                decimal.TryParse(contactNumber.Replace("-", string.Empty), out tempSin);
                if ((enteredSin >= tempSin) && (sin.Substring(0, 3) == "999"))
                {
                    entity["bpa_socialinsurancenumber"] = contactNumber;
                    entity["bpa_sinnoformat"] = new String(contactNumber.ToCharArray().Where(char.IsDigit).ToArray());

                    //Update last sin number
                    //string crmAdminUserId = (PluginConfiguration != null && PluginConfiguration.ContainsKey(PluginHelper.ConfigCrmAdmin) ? PluginConfiguration[PluginHelper.ConfigCrmAdmin] : string.Empty);
                    string crmAdminUserId = string.Empty;
                    try
                    {
                        crmAdminUserId = PluginConfiguration[PluginHelper.ConfigCrmAdmin];
                    }
                    catch (Exception ex)
                    {
                        // Get value from Database
                        crmAdminUserId = ConfigurationHelper.FetchValueId(service, PluginHelper.ConfigCrmAdmin);
                    }
                    if (string.IsNullOrEmpty("crmAdminUserId"))
                    {
                        tracingService.Trace("ERROR FROM ContactOnCreatePre Plugin - CRM Admin USer Could not Found.");
                        throw new InvalidPluginExecutionException(OperationStatus.Failed, $@"The given key was not present in the dictionary. Please contact BPA Administrator");
                    }
                    serviceFactory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
                    service = serviceFactory.CreateOrganizationService(new Guid(crmAdminUserId));

                    ConfigurationHelper.UpdateNextSINNumber(service, nextNumber.ToString());
                    // Re-Assignment back to Service
                    service = serviceFactory.CreateOrganizationService(executeCallerId);
                }
                else
                    //Strip out the formatting and add it to field for searching
                    entity["bpa_sinnoformat"] = new String(sin.ToCharArray().Where(char.IsDigit).ToArray());
            }


            #region Strip out formatting

            if (entity.Contains("mobilephone"))
                entity["bpa_mobilenoformat"] = new String(entity.GetAttributeValue<String>("mobilephone").ToCharArray().Where(char.IsDigit).ToArray());

            if (entity.Contains("telephone1"))
                entity["bpa_telephone1noformat"] = new String(entity.GetAttributeValue<String>("telephone1").ToCharArray().Where(char.IsDigit).ToArray());

            if (entity.Contains("telephone2"))
                entity["bpa_telephone2noformat"] = new String(entity.GetAttributeValue<String>("telephone2").ToCharArray().Where(char.IsDigit).ToArray());

            if (entity.Contains("bpa_hashsin"))
            {
                string hashSIN = entity.GetAttributeValue<String>("bpa_hashsin").Replace("-","").Replace(","," ");

                entity["bpa_hashsinnoformat"] = hashSIN;
            }

            #endregion


        }
    }
}