﻿#region

using System;
using BPA.Benefit.Management.CRM.Plugins.Helper;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;

#endregion

namespace BPA.Benefit.Management.CRM.Plugins
{
    /// <summary>
    ///     AdjustmentOnCreatePost Plugin.
    /// </summary>
    public class AdjustmentOnCreatePre : Plugin
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="AdjustmentOnCreatePre" /> class.
        /// </summary>
        public AdjustmentOnCreatePre()
            : base(typeof(AdjustmentOnCreatePre))
        {
            RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(20, PluginHelper.Create, PluginHelper.BpaSubmissionadjustment, ExecuteOnCreatePre));

            // Note : you can register for more events here if this plugin is not specific to an individual entity and message combination.
            // You may also need to update your RegisterFile.crmregister plug-in registration file to reflect any change.
        }

        /// <summary>
        ///     Executes the plug-in.
        /// </summary>
        /// <param name="localContext">
        ///     The <see cref="Plugin.LocalPluginContext" /> which contains the
        ///     <see cref="IPluginExecutionContext" />,
        ///     <see cref="IOrganizationService" />
        ///     and <see cref="ITracingService" />
        /// </param>
        /// <remarks>
        ///     For improved performance, Microsoft Dynamics CRM caches plug-in instances.
        ///     The plug-in's Execute method should be written to be stateless as the constructor
        ///     is not called for every invocation of the plug-in. Also, multiple system threads
        ///     could execute the plug-in at the same time. All per invocation state information
        ///     is stored in the context. This means that you should not use global variables in plug-ins.
        /// </remarks>
        protected void ExecuteOnCreatePre(LocalPluginContext localContext)
        {
            // If local context not passed in, raise error
            if (localContext == null)
                throw new ArgumentNullException($"Local context not passed in.");

            // Create the context variables
            IPluginExecutionContext context = localContext.PluginExecutionContext;
            IOrganizationService service = localContext.OrganizationService;
            ITracingService tracingService = localContext.TracingService;

            if (context.Depth > 1)
                return;
            // If Target is not passed in, raise error
            tracingService.Trace("If Target is not passed in, raise error.");
            if (!context.InputParameters.Contains(PluginHelper.Target) ||
                !(context.InputParameters[PluginHelper.Target] is Entity))
                throw new ArgumentNullException($"Target is not of type Entity.");

            // Create the target entity
            tracingService.Trace("Create the target entity");
            Entity entity = (Entity)context.InputParameters[PluginHelper.Target];
            
            //Get Type
            OptionSetValue type = new OptionSetValue(0);
            if (entity.Contains("bpa_type"))
                type = entity.GetAttributeValue<OptionSetValue>("bpa_type");

            //if type is INTEREST PAID amount must be negetive
            //if ((type.Value == (int)AdjustmentbpaType.InterestPaid) || (type.Value == (int)AdjustmentbpaType.InterestWaived))
            //{
            //    string errorMessage = string.Empty;
            //    if (type.Value == (int)AdjustmentbpaType.InterestPaid)
            //        errorMessage = ;
            //    else
            //        errorMessage = $@"Intereset Waived Amount must be negetive.";

            //    if (entity.Contains("bpa_amount") && entity.GetAttributeValue<Money>("bpa_amount").Value > 0)
            //        throw new InvalidPluginExecutionException(OperationStatus.Failed, $@"Intereset Paid Amount must be negetive.");

            //}

            // If you update the message please make sure you also update on "ON Update PRE' Plugin
            if (type.Value == (int)AdjustmentbpaType.InterestPaid)
            {
                if (entity.Contains("bpa_amount") && entity.GetAttributeValue<Money>("bpa_amount").Value > 0)
                    throw new InvalidPluginExecutionException(OperationStatus.Failed, $@"Interest Paid amount must be less than 0.");
            }
            else if (type.Value == (int)AdjustmentbpaType.InterestWaived)
            {
                if (entity.Contains("bpa_amount") && entity.GetAttributeValue<Money>("bpa_amount").Value > 0)
                    throw new InvalidPluginExecutionException(OperationStatus.Failed, $@"Intereset Waived amount must be less than 0.");
            }
            else if (type.Value == (int)AdjustmentbpaType.InterestRedirection)
            {
                if (entity.Contains("bpa_amount") && entity.GetAttributeValue<Money>("bpa_amount").Value < 0)
                    throw new InvalidPluginExecutionException(OperationStatus.Failed, $@"Interest Redirection amount must be greater than 0.");
            }
            else if (type.Value == (int)AdjustmentbpaType.Nsf)
            {
                //Validated Selected Submission is Completed.
                EntityReference submission = (entity.Contains("bpa_submission") ? entity.GetAttributeValue<EntityReference>("bpa_submission") : null);
                if (submission == null)
                    throw new InvalidPluginExecutionException(OperationStatus.Failed, $@"Plese select Submission");

                //Retrive Submission Information
                Entity submisisonRetrieve = service.Retrieve(PluginHelper.BpaSubmission, submission.Id, new ColumnSet(new string[] { "bpa_submissionpaid", "bpa_submissionstatus", "bpa_paymentmethod" }));
                //Validate Submission Completed or not?
                OptionSetValue submissionstatus = (submisisonRetrieve.Contains("bpa_submissionstatus") ? submisisonRetrieve.GetAttributeValue<OptionSetValue>("bpa_submissionstatus") : new OptionSetValue(0));
                OptionSetValue paymentmethod = (submisisonRetrieve.Contains("bpa_paymentmethod") ? submisisonRetrieve.GetAttributeValue<OptionSetValue>("bpa_paymentmethod") : new OptionSetValue(0));

                //Check Submission Status
                bool IsError = false;
                if ((submissionstatus.Value != (int)SubmissionStatus.Completed))
                    IsError = true;
                if (paymentmethod.Value != (int)SubmissionPaymentType.Cheque)
                    IsError = true;

                if (IsError)
                    throw new InvalidPluginExecutionException(OperationStatus.Failed, "Selected Submission must be completed and Payment method must be Cheque.");

                //Set Adjustment Amount if user did not entered
                if (!entity.Contains("bpa_amount"))
                {
                    Money paidAmount = (submisisonRetrieve.Contains("bpa_submissionpaid") ? submisisonRetrieve.GetAttributeValue<Money>("bpa_submissionpaid") : new Money(0));
                    entity["bpa_amount"] = paidAmount;
                }
            }
            
            //Work Month does not contain then populate workmonth
            tracingService.Trace("Checking WorkMonth exists or not?");
            if (!entity.Contains("bpa_workmonth"))
            {
                entity["bpa_workmonth"] = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddMonths(-1);
            }

            //Generate name
            if (!entity.Contains("bpa_name"))
                entity["bpa_name"] = Guid.NewGuid().ToString();
        }
    }
}