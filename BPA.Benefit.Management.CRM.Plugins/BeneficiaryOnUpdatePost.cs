﻿#region

using System;
using BPA.Benefit.Management.CRM.Plugins.Helper;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using System.Globalization;

#endregion

namespace BPA.Benefit.Management.CRM.Plugins
{
    /// <summary>
    ///     DependentOnCreatePre Plugin.
    /// </summary>
    public class BeneficiaryOnUpdatePost : Plugin
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="DependentOnCreatePre" /> class.
        /// </summary>
        public BeneficiaryOnUpdatePost()
            : base(typeof(BeneficiaryOnUpdatePost))
        {
            RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(40, "Update",
                PluginHelper.BpaBeneficiary, ExecutePostBeneficiaryUpdate));

            // Note : you can register for more events here if this plugin is not specific to an individual entity and message combination.
            // You may also need to update your RegisterFile.crmregister plug-in registration file to reflect any change.
        }

        /// <summary>
        ///     Executes the plug-in.
        /// </summary>
        /// <param name="localContext">
        ///     The <see cref="Plugin.LocalPluginContext" /> which contains the
        ///     <see cref="IPluginExecutionContext" />,
        ///     <see cref="IOrganizationService" />
        ///     and <see cref="ITracingService" />
        /// </param>
        /// <remarks>
        ///     For improved performance, Microsoft Dynamics CRM caches plug-in instances.
        ///     The plug-in's Execute method should be written to be stateless as the constructor
        ///     is not called for every invocation of the plug-in. Also, multiple system threads
        ///     could execute the plug-in at the same time. All per invocation state information
        ///     is stored in the context. This means that you should not use global variables in plug-ins.
        /// </remarks>
        protected void ExecutePostBeneficiaryUpdate(LocalPluginContext localContext)
        {
            // If local context not passed in, raise error
            if (localContext == null)
                throw new ArgumentNullException($"Local context not passed in.");

            // Create the context variables
            IPluginExecutionContext context = localContext.PluginExecutionContext;
            IOrganizationService service = localContext.OrganizationService;
            ITracingService tracingService = localContext.TracingService;

            // If Target is not passed in, raise error
            tracingService.Trace("If Target is not passed in, raise error");
            if (!context.InputParameters.Contains(PluginHelper.Target) ||
                !(context.InputParameters[PluginHelper.Target] is Entity))
                throw new ArgumentNullException($"Target is not of type Entity.");

            // Create the target entity & preImage
            tracingService.Trace("Create the target entity & preImage");
            Entity entity = (Entity)context.InputParameters[PluginHelper.Target];

            // PRE IMAGE AND get Name
            Entity preImageEntity = context.PreEntityImages != null &&
                                     context.PreEntityImages.Contains(PluginHelper.PreImageAlias)
                ? context.PreEntityImages[PluginHelper.PreImageAlias] : null;
            if (preImageEntity == null)
                throw new InvalidPluginExecutionException(OperationStatus.Failed, $@"Beneficiary PRE Image does not define.");

            string pre_FullName = string.Empty;
            if (preImageEntity.Contains("bpa_name"))
                pre_FullName = preImageEntity.GetAttributeValue<string>("bpa_name");

            // POST IMAGE AND get Name
            Entity postImageEntity = context.PostEntityImages != null &&
                                     context.PostEntityImages.Contains(PluginHelper.PostImageAlias)
                ? context.PostEntityImages[PluginHelper.PostImageAlias] : null;
            if (postImageEntity == null)
                throw new InvalidPluginExecutionException(OperationStatus.Failed, $@"Beneficiary Post Image does not define.");

            string post_FullName = string.Empty;
            if (postImageEntity.Contains("bpa_name"))
                post_FullName = postImageEntity.GetAttributeValue<string>("bpa_name");

            //If PRE and POST name are not same then update all Plan Dependant Name
            if(pre_FullName.ToLower() != post_FullName.ToLower())
            {
                //TextInfo textInfo = new CultureInfo("en-US", false).TextInfo;
                string properName = System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(post_FullName.ToLower());

                // Get All Plan Dependant 
                EntityCollection collection = BeneficiaryHelper.FetchAllPlanBenefit(service, tracingService, entity.Id);
                if(collection != null && collection.Entities.Count > 0)
                {
                    foreach(Entity e in collection.Entities)
                    {
                        Entity update = new Entity(PluginHelper.BpaPlanBeneficiary)
                        {
                            Id = e.Id,
                            ["bpa_name"] = properName
                        };
                        service.Update(update);
                    }
                }
            }
        }
    }
}