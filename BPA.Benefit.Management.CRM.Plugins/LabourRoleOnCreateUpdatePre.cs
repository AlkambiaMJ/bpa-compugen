﻿#region

using System;
using BPA.Benefit.Management.CRM.Plugins.Helper;
using Microsoft.Xrm.Sdk;

#endregion

namespace BPA.Benefit.Management.CRM.Plugins
{
    /// <summary>
    ///     LabourRoleOnCreateUpdatePre PlugIn.
    /// </summary>
    public class LabourRoleOnCreateUpdatePre : Plugin
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="LabourRoleOnCreateUpdatePre" /> class.
        /// </summary>
        public LabourRoleOnCreateUpdatePre() : base(typeof(LabourRoleOnCreateUpdatePre))
        {
            RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(20, "Create",
                PluginHelper.BpaLabourrole, ExecuteOnCreateUpdatePre));
            RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(20, "Update",
                PluginHelper.BpaLabourrole, ExecuteOnCreateUpdatePre));

            // Note : you can register for more events here if this plugin is not specific to an individual entity and message combination.
            // You may also need to update your RegisterFile.crmregister plug-in registration file to reflect any change.
        }

        /// <summary>
        ///     Executes the plug-in.
        /// </summary>
        /// <param name="localContext">
        ///     The <see cref="Plugin.LocalPluginContext" /> which contains the
        ///     <see cref="IPluginExecutionContext" />,
        ///     <see cref="IOrganizationService" />
        ///     and <see cref="ITracingService" />
        /// </param>
        /// <remarks>
        ///     For improved performance, Microsoft Dynamics CRM caches plug-in instances.
        ///     The plug-in's Execute method should be written to be stateless as the constructor
        ///     is not called for every invocation of the plug-in. Also, multiple system threads
        ///     could execute the plug-in at the same time. All per invocation state information
        ///     is stored in the context. This means that you should not use global variables in plug-ins.
        /// </remarks>

        protected void ExecuteOnCreateUpdatePre(LocalPluginContext localContext)
        {
            // If local context not passed in, raise error
            if (localContext == null)
                throw new ArgumentNullException($"Local context not passed in.");

            // Create the context variables
            IPluginExecutionContext context = localContext.PluginExecutionContext;
            IOrganizationService service = localContext.OrganizationService;
            ITracingService tracingService = localContext.TracingService;

            // If Target is not passed in, raise error
            tracingService.Trace("If Target is not passed in, raise error");
            if (!context.InputParameters.Contains(PluginHelper.Target) ||
                !(context.InputParameters[PluginHelper.Target] is Entity))
                throw new ArgumentNullException($"Target is not of type Entity.");

            // Create the target entity & preImage
            tracingService.Trace("Create the target entity & preImage");
            Entity entity = (Entity)context.InputParameters[PluginHelper.Target];
            Entity preImage = context.PreEntityImages != null &&
                                    context.PreEntityImages.Contains(PluginHelper.PreImageAlias)
                ? context.PreEntityImages[PluginHelper.PreImageAlias]
                : null;
            if (context.MessageName.ToLower() == PluginHelper.Update.ToLower() && preImage == null)
                throw new ArgumentNullException($"preImage entity is not defined.");

            if (!entity.Attributes.Contains("bpa_isdefault")) return;
            bool isDefault = false;
            if (entity.Contains("bpa_isdefault"))
                isDefault = entity.GetAttributeValue<bool>("bpa_isdefault");

            if (!isDefault) return;

            // Find out Is there any Default Fund for current Fund Type for current Trust
            tracingService.Trace("Find out Is there any Default Fund for current Fund Type for current Trust");
            EntityReference agreementRef = null;
            if (entity.Attributes.Contains("bpa_agreementid"))
                agreementRef = entity.GetAttributeValue<EntityReference>("bpa_agreementid");
            else if (preImage.Contains("bpa_agreementid"))
                agreementRef = preImage.GetAttributeValue<EntityReference>("bpa_agreementid");

            if (agreementRef == null) return;
            bool isUpdate = context.MessageName.ToLower() == "update";

            bool isDefaultExists = LabourRoleHelper.IsDefaultLabourRoleExists(service, agreementRef.Id,
                isUpdate, entity.Id);
            if (isDefaultExists)
                throw new InvalidPluginExecutionException(OperationStatus.Failed,
                    @"A default job classification already exists for selected agreement. If you wish to change the current job classification to the default job classification, please change the default flag to 'NO' on the original job classification.");
        }
    }
}