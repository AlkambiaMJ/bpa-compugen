﻿#region

using System;
using BPA.Benefit.Management.CRM.Plugins.Helper;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;

#endregion

namespace BPA.Benefit.Management.CRM.Plugins
{
    /// <summary>
    ///     DelinquencyRuleOnCreateUpdatePre Plugin.
    /// </summary>
    public class DelinquencyRuleOnCreateUpdatePre : Plugin
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="DelinquencyRuleOnCreateUpdatePre" /> class.
        /// </summary>
        public DelinquencyRuleOnCreateUpdatePre() : base(typeof(DelinquencyRuleOnCreateUpdatePre))
        {
            RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(20, "Create",
                PluginHelper.BpaDelinquencyrule, ExecuteOnCreateUpdatePre));
            RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(20, "Update",
                PluginHelper.BpaDelinquencyrule, ExecuteOnCreateUpdatePre));

            // Note : you can register for more events here if this plugin is not specific to an individual entity and message combination.
            // You may also need to update your RegisterFile.crmregister plug-in registration file to reflect any change.
        }

        /// <summary>
        ///     Executes the plug-in.
        /// </summary>
        /// <param name="localContext">
        ///     The <see cref="Plugin.LocalPluginContext" /> which contains the
        ///     <see cref="IPluginExecutionContext" />,
        ///     <see cref="IOrganizationService" />
        ///     and <see cref="ITracingService" />
        /// </param>
        /// <remarks>
        ///     For improved performance, Microsoft Dynamics CRM caches plug-in instances.
        ///     The plug-in's Execute method should be written to be stateless as the constructor
        ///     is not called for every invocation of the plug-in. Also, multiple system threads
        ///     could execute the plug-in at the same time. All per invocation state information
        ///     is stored in the context. This means that you should not use global variables in plug-ins.
        /// </remarks>
        protected void ExecuteOnCreateUpdatePre(LocalPluginContext localContext)
        {
            // Creation of a delinquency rule
            // - verify that there is a fund of type delinquency before allowing create
            // - verify that dates do not overlap (gaps are ok)

            // Update of a delinquency rule
            // - verify that dates do not overlap (gaps are ok)
            // - Logic: bool overlap = a.start < b.end && b.start < a.end;
            // http://stackoverflow.com/questions/13513932/algorithm-to-detect-overlapping-periods

            // If local context not passed in, raise error
            if (localContext == null)
                throw new ArgumentNullException($"Local context not passed in.");

            // Create the context variables
            IPluginExecutionContext context = localContext.PluginExecutionContext;
            IOrganizationService service = localContext.OrganizationService;
            ITracingService tracingService = localContext.TracingService;

            // If Target is not passed in, raise error
            tracingService.Trace("If Target is not passed in, raise error");
            if (!context.InputParameters.Contains(PluginHelper.Target) ||
                !(context.InputParameters[PluginHelper.Target] is Entity))
                throw new ArgumentNullException($"Target is not of type Entity.");

            // Create the target entity & preImage
            tracingService.Trace("Create the target entity & preImage");
            Entity entity = (Entity)context.InputParameters[PluginHelper.Target];
            Entity preImage = context.PreEntityImages != null &&
                              context.PreEntityImages.Contains(PluginHelper.PreImageAlias)
                ? context.PreEntityImages[PluginHelper.PreImageAlias]
                : null;

             EntityReference agreementRef;
            if (entity.Contains("bpa_agreementid"))
                agreementRef = (EntityReference)entity.Attributes["bpa_agreementid"];
            else if (preImage != null && preImage.Contains("bpa_agreementid"))
                agreementRef = (EntityReference)preImage.Attributes["bpa_agreementid"];
            else
                // If the Agreement is null, raise an error
               throw new ArgumentNullException($"Agreement is missing on this delinquency rule.");

            #region ---Ref. TFS Task 1200. Deprecated Code - No longer need to check for a fund of type "Delinquency"
            //// If create, verify that there is a fund of type delinquency before allowing create
            //// Get the Agreement
            //localContext.Trace(
            //    "If create, verify that there is a fund of type delinquency before allowing create.  Get the Agreement");

            //// Get the Agreement record and the Trust from it
            //localContext.Trace("Get the Agreement record and the Trust from it");
            //Entity agreement = service.Retrieve("bpa_subsector", agreementRef.Id, new ColumnSet(true));
            //if (agreement != null && agreement.Contains("bpa_trustid"))
            //{
            //    EntityReference trustRef = (EntityReference)agreement.Attributes["bpa_trustid"];

            //    // List all Funds in the Trust, verify one is of type Delinquency
            //    localContext.Trace("List all Funds in the Trust, verify one is of type Delinquency.");
            //    QueryExpression queryFundType = new QueryExpression
            //    {
            //        EntityName = "bpa_fund",
            //        ColumnSet = new ColumnSet(true),
            //        Criteria =
            //        {
            //            Filters =
            //            {
            //                new FilterExpression
            //                {
            //                    FilterOperator = LogicalOperator.And,
            //                    Conditions =
            //                    {
            //                        new ConditionExpression("bpa_trust", ConditionOperator.Equal, trustRef.Id),
            //                        new ConditionExpression("bpa_fundtype", ConditionOperator.Equal, 922070006),

            //                        // Delinquency Interest
            //                        new ConditionExpression("statecode", ConditionOperator.Equal, 0)
            //                    }
            //                }
            //            }
            //        }
            //    };

            //    // Query the fund entity
            //    localContext.Trace("Query the fund entity.");
            //    queryFundType.PageInfo.ReturnTotalRecordCount = true;
            //    EntityCollection funds = service.RetrieveMultiple(queryFundType);

            //    // If no Fund exist, raise an error message
            //    localContext.Trace("If no Fund exist, raise an error message.");
            //    if (funds.TotalRecordCount <= 0)
            //        throw new InvalidPluginExecutionException(OperationStatus.Failed,
            //            @"No 'Delinquency Interest' Fund is created for the Trust this Agreement is linked to.  Create a new Fund for delinquency amounts to be deposited into before creating a Delinquency Rule.");
            //}

            #endregion

            // Verify that dates do not overlap (gaps are ok)
            // Get the added/updated start date
            localContext.Trace("Get the added/updated start date");
            DateTime currentStartDate;
            if (entity.Contains("bpa_startdate"))
                currentStartDate = (DateTime)entity.Attributes["bpa_startdate"];
            else if (preImage != null && preImage.Contains("bpa_startdate"))
                currentStartDate = (DateTime)preImage.Attributes["bpa_startdate"];
            else
                // If the Start date is null, raise an error
                throw new ArgumentNullException($"Start date is missing on this delinquency rule.");

            // Get the added/updated end date
            localContext.Trace("Get the added/updated end date");
            DateTime currentEndDate;
            if (entity.Contains("bpa_enddate"))
                currentEndDate = (DateTime)entity.Attributes["bpa_enddate"];
            else if (preImage != null && preImage.Contains("bpa_enddate"))
                currentEndDate = (DateTime)preImage.Attributes["bpa_enddate"];
            else
                // If the End date is null, raise an error
                throw new ArgumentNullException($"End date is missing on this delinquency rule.");

            // Get a list of all of the existing Delinquency Rules
            localContext.Trace("Get a list of all of the existing Delinquency Rules.");
            QueryExpression queryDelinquencyRules = new QueryExpression
            {
                EntityName = "bpa_delinquencyrule",
                ColumnSet = new ColumnSet(true),
                Criteria =
                {
                    Filters =
                    {
                        new FilterExpression
                        {
                            FilterOperator = LogicalOperator.And,
                            Conditions =
                            {
                                new ConditionExpression("bpa_agreementid", ConditionOperator.Equal, agreementRef.Id),
                                new ConditionExpression("statecode", ConditionOperator.Equal, 0)
                            }
                        }
                    }
                }
            };

            if (context.MessageName.ToLower() == PluginHelper.Update.ToLower())
                queryDelinquencyRules.Criteria.AddCondition(new ConditionExpression("bpa_delinquencyruleid", ConditionOperator.NotEqual, entity.Id));

            // Query the Delinquency Rule entity
            localContext.Trace("Query the Delinquency Rule entity.");
            EntityCollection delinquencyrules = service.RetrieveMultiple(queryDelinquencyRules);

            // Loop through them and see if any overlap
            localContext.Trace("Loop through them and see if any overlap.");
            foreach (Entity delinquencyrule in delinquencyrules.Entities)
            {
                // Get the loop record's start date
                localContext.Trace("Get the loop record's start date");
                DateTime loopStartDate = new DateTime();
                if (delinquencyrule.Contains("bpa_startdate"))
                    loopStartDate = (DateTime)delinquencyrule.Attributes["bpa_startdate"];

                // Get the loop record's end date
                localContext.Trace("Get the loop record's end date");
                DateTime loopEndDate = new DateTime();
                if (delinquencyrule.Contains("bpa_enddate"))
                    loopEndDate = (DateTime)delinquencyrule.Attributes["bpa_enddate"];

                // If overlaping Delinquency Rules exist, raise an error message
                localContext.Trace("If overlaping Delinquency Rules exist, raise an error message");
                bool overlap = currentStartDate < loopEndDate && loopStartDate < currentEndDate;
                if (overlap)
                {
                    throw new InvalidPluginExecutionException(OperationStatus.Failed,
                        @"The start and end dates for this Delinquency Rule overlap with an existing Delinquency Rule's dates.  Please change the dates so they do not overlap with any existing Delinquency Rules for this Agreement.");
                }
            }
        }
    }
}