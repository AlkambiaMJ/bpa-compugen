﻿#region

using System;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using System.Collections.Generic;
using System.Text;
using BPA.Benefit.Management.CRM.Plugins.Helper;

#endregion

namespace BPA.Benefit.Management.CRM.Plugins
{
    /// <summary>
    ///     MemberPlanAdjustmentOnCreatePost PlugIn.
    /// </summary>
    public class DepositOnStateChangePreValidation : Plugin
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="MemberPlanAdjustmentOnCreatePost" /> class.
        /// </summary>
        public DepositOnStateChangePreValidation()
            : base(typeof(DepositOnStateChangePreValidation))
        {
            RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(10, "SetState", PluginHelper.BpaDeposit, ExecutePluginLogic));
            RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(10, "SetStateDynamicEntity", PluginHelper.BpaDeposit, ExecutePluginLogic));

            // Note : you can register for more events here if this plugin is not specific to an individual entity and message combination.
            // You may also need to update your RegisterFile.crmregister plug-in registration file to reflect any change.
        }

        /// <summary>
        ///     Executes the plug-in.
        /// </summary>
        /// <param name="localContext">
        ///     The <see cref="Plugin.LocalPluginContext" /> which contains the
        ///     <see cref="IPluginExecutionContext" />,
        ///     <see cref="IOrganizationService" />
        ///     and <see cref="ITracingService" />
        /// </param>
        /// <remarks>
        ///     For improved performance, Microsoft Dynamics CRM caches plug-in instances.
        ///     The plug-in's Execute method should be written to be stateless as the constructor
        ///     is not called for every invocation of the plug-in. Also, multiple system threads
        ///     could execute the plug-in at the same time. All per invocation state information
        ///     is stored in the context. This means that you should not use global variables in plug-ins.
        /// </remarks>
        /// 
        protected void ExecutePluginLogic(LocalPluginContext localContext)
        {
            // If local context not passed in, raise error
            if (localContext == null)
                throw new ArgumentNullException($"Local context not passed in.");

            // Create the context variables
            IPluginExecutionContext context = localContext.PluginExecutionContext;
            IOrganizationService service = localContext.OrganizationService;
            ITracingService tracingService = localContext.TracingService;
            IServiceProvider serviceProvider = localContext.ServiceProvider;

            // If Target is not passed in, raise error
            tracingService.Trace("If Target is not passed in, raise error");
            if (!context.InputParameters.Contains(PluginHelper.EntityMoniker) ||
                !(context.InputParameters[PluginHelper.EntityMoniker] is EntityReference))
                throw new ArgumentNullException($"EntityReference is not passed in.");

            // Create the EntityReference
            tracingService.Trace("Create the EntityReference");
            EntityReference targetEntity = (EntityReference)context.InputParameters[PluginHelper.EntityMoniker];
            if (targetEntity == null)
                throw new ArgumentNullException($"EntityReference is not defined.");

            // Get the record's state & status
            tracingService.Trace("Get the record's state & status");
            OptionSetValue status = (OptionSetValue)context.InputParameters["Status"];
            // Get execute User
            Guid executeUser = context.InitiatingUserId;

            // Verify all associated submissions are active?
            if (status.Value == 2) //Inactive
            {
                // Fetch all submission 
                EntityCollection submissions = SubmissionHelper.FetchAllSubmissionsByDepositId(service, tracingService, targetEntity.Id);
                if (submissions != null && submissions.Entities.Count > 0)
                {
                    throw new InvalidPluginExecutionException(OperationStatus.Failed, $@"You are not able to deactivate deposit as associated submissions are still active.");
                }

            }
        }
    }
}