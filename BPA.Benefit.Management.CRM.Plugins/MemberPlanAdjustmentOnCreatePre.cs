﻿#region

using System;
using BPA.Benefit.Management.CRM.Plugins.Helper;
using Microsoft.Xrm.Sdk;

#endregion

namespace BPA.Benefit.Management.CRM.Plugins
{
    /// <summary>
    ///     MemberPlanAdjustmentOnCreatePre PlugIn.
    /// </summary>
    public class MemberPlanAdjustmentOnCreatePre : Plugin
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="MemberPlanAdjustmentOnCreatePre" /> class.
        /// </summary>
        public MemberPlanAdjustmentOnCreatePre()
            : base(typeof(MemberPlanAdjustmentOnCreatePre))
        {
            RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(20, "Create", PluginHelper.BpaMemberPlanadjustment, PostExecuteCreate));
            
            // Note : you can register for more events here if this plugin is not specific to an individual entity and message combination.
            // You may also need to update your RegisterFile.crmregister plug-in registration file to reflect any change.
        }

        /// <summary>
        ///     Executes the plug-in.
        /// </summary>
        /// <param name="localContext">
        ///     The <see cref="Plugin.LocalPluginContext" /> which contains the
        ///     <see cref="IPluginExecutionContext" />,
        ///     <see cref="IOrganizationService" />
        ///     and <see cref="ITracingService" />
        /// </param>
        /// <remarks>
        ///     For improved performance, Microsoft Dynamics CRM caches plug-in instances.
        ///     The plug-in's Execute method should be written to be stateless as the constructor
        ///     is not called for every invocation of the plug-in. Also, multiple system threads
        ///     could execute the plug-in at the same time. All per invocation state information
        ///     is stored in the context. This means that you should not use global variables in plug-ins.
        /// </remarks>
        /// 
        protected void PostExecuteCreate(LocalPluginContext localContext)
        {
            // If local context not passed in, raise error
            if (localContext == null)
                throw new ArgumentNullException($"Local context not passed in.");

            // Create the context variables
            IPluginExecutionContext context = localContext.PluginExecutionContext;
            IOrganizationService service = localContext.OrganizationService;
            ITracingService tracingService = localContext.TracingService;
            IServiceProvider serviceProvider = localContext.ServiceProvider;

            // If Target is not passed in, raise error
            tracingService.Trace("If Target is not passed in, raise error");
            if (!context.InputParameters.Contains(PluginHelper.Target) ||
                !(context.InputParameters[PluginHelper.Target] is Entity))
                throw new ArgumentNullException($"Target is not of type Entity.");

            // Create the target entity & postImage
            tracingService.Trace("Create the target entity & postImage");
            Entity entity = (Entity)context.InputParameters[PluginHelper.Target];
            // Get the current user id
            Guid runningUserId = localContext.PluginExecutionContext.UserId;

            // Following method called only when new Member Plan Details created 
            tracingService.Trace("Get all data from Entity or Post Image");

            EntityReference memberPlan = null;
            if (entity.Contains("bpa_memberplanid"))
                memberPlan = entity.GetAttributeValue<EntityReference>("bpa_memberplanid");
            tracingService.Trace($"After Member Plan {memberPlan.Id}");

            //Benefit Adjustment Type
            OptionSetValue benefitAdjustmentType = new OptionSetValue(0);
            if (entity.Contains("bpa_benefitadjustmenttype"))
                benefitAdjustmentType = entity.GetAttributeValue<OptionSetValue>("bpa_benefitadjustmenttype");

            // Pension Adjustment Type
            OptionSetValue pensionAdjustmentType = new OptionSetValue(0);
            if (entity.Contains("bpa_pensionadjustmenttype"))
                pensionAdjustmentType = entity.GetAttributeValue<OptionSetValue>("bpa_pensionadjustmenttype");

            //bpa_adjustmentcategory
            OptionSetValue adjustmentCategory = new OptionSetValue(0);
            if (entity.Contains("bpa_adjustmentcategory"))
                adjustmentCategory = entity.GetAttributeValue<OptionSetValue>("bpa_adjustmentcategory");

            if (adjustmentCategory.Value == (int)MemberPlanAdjustmentCategory.PensionAdjustment)
                return;

            #region ----------- FUNTURE MONTH VALIDATION ---------------------

            DateTime WorkMonth = DateTime.MinValue;
            if (benefitAdjustmentType.Value == (int)MemberPlanBenefitAdjustmentType.OtherBenefit)
            {
                if (entity.Contains("bpa_adjustmentdate"))
                    WorkMonth = entity.GetAttributeValue<DateTime>("bpa_adjustmentdate");
                tracingService.Trace($"After Adjustment Date {WorkMonth.ToString()}");
            }
            //else if (pensionAdjustmentType.Value == (int)MemberPlanPensionAdjustmentType.Other)
            //{
            //    if (entity.Contains("bpa_adjustmentdate"))
            //        WorkMonth = entity.GetAttributeValue<DateTime>("bpa_adjustmentdate");
            //    tracingService.Trace($"After Adjustment Date {WorkMonth.ToString()}");
            //}
            else
            {
                if (entity.Contains("bpa_workmonth"))
                    WorkMonth = entity.GetAttributeValue<DateTime>("bpa_workmonth");
                tracingService.Trace($"After Work Month {WorkMonth.ToString()}");
            }



            //Checking for Adjustment date is future month throw error
            if ((WorkMonth != DateTime.MinValue) && (new DateTime(WorkMonth.Year, WorkMonth.Month, 1) > new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddMonths(-1)))
                throw new InvalidPluginExecutionException(OperationStatus.Failed,
                    "The Adjustment Month you are selecting is for a future month. Your Adjustment can not be processed. Please select a different month for your adjustment.");

            #endregion
            
            /*
                * If member is in 'Frozen' or 'Reciprocal Transfer Out' current eligibility and user try to creat adjustment record with Hour and Dollar throw error
                * */
            #region ----- Cannot create Adjustment if Member is in Reciprocal Transfer Out Status -----------------
            int currentEligibility = ContactHelper.FetchCurrentEligibility(service, tracingService, memberPlan.Id);
            if (currentEligibility == (int)MemberPlanBpaCurrentEligibility.Frozen || currentEligibility == (int)MemberPlanBpaCurrentEligibility.TransferOut)
            {
                if ((benefitAdjustmentType.Value == (int)MemberPlanBenefitAdjustmentType.OtherBenefit) && ((entity.Contains("bpa_hours")) || (entity.Contains("bpa_dollars"))))
                {
                    throw new InvalidPluginExecutionException(OperationStatus.Failed,
                        "You are not allowe to create member adjustment Hour or Dollar as member current elibigility either 'Frozen' or 'Reciprocal Transfer Out'.");
                }
            }

            #endregion
                    
            
        }
    }
}