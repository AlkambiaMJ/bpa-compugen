﻿#region

using System;
using System.Linq;
using BPA.Benefit.Management.CRM.Plugins.Helper;
using Microsoft.Xrm.Sdk;
using System.Collections.Generic;
using Microsoft.Xrm.Sdk.Query;

#endregion

namespace BPA.Benefit.Management.CRM.Plugins
{
    /// <summary>
    ///     MemberPlanOnUpdatePost PlugIn.
    /// </summary>
    public class MemberPlanOnCreatePre : Plugin
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="MemberPlanOnUpdatePre" /> class.
        /// </summary>
        public MemberPlanOnCreatePre()
            : base(typeof(MemberPlanOnUpdatePre))
        {
            RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(20, PluginHelper.Create, PluginHelper.BpaMemberplan, PreExecuteCreate));

            // Note : you can register for more events here if this plugin is not specific to an individual entity and message combination.
            // You may also need to update your RegisterFile.crmregister plug-in registration file to reflect any change.
        }

        /// <summary>
        ///     Executes the plug-in.
        /// </summary>
        /// <param name="localContext">
        ///     The <see cref="Plugin.LocalPluginContext" /> which contains the
        ///     <see cref="IPluginExecutionContext" />,
        ///     <see cref="IOrganizationService" />
        ///     and <see cref="ITracingService" />
        /// </param>
        /// <remarks>
        ///     For improved performance, Microsoft Dynamics CRM caches plug-in instances.
        ///     The plug-in's Execute method should be written to be stateless as the constructor
        ///     is not called for every invocation of the plug-in. Also, multiple system threads
        ///     could execute the plug-in at the same time. All per invocation state information
        ///     is stored in the context. This means that you should not use global variables in plug-ins.
        /// </remarks>
        protected void PreExecuteCreate(LocalPluginContext localContext)
        {
            // If local context not passed in, raise error
            if (localContext == null)
                throw new ArgumentNullException($"Local context not passed in.");

            // Create the context variables
            IPluginExecutionContext context = localContext.PluginExecutionContext;
            IOrganizationService service = localContext.OrganizationService;
            ITracingService tracingService = localContext.TracingService;
            IServiceProvider serviceProvider = localContext.ServiceProvider;

            // If Target is not passed in, raise error
            tracingService.Trace("If Target is not passed in, raise error");
            if (!context.InputParameters.Contains(PluginHelper.Target) ||
                !(context.InputParameters[PluginHelper.Target] is Entity))
                throw new ArgumentNullException($"Target is not of type Entity.");

           
            // Create the target entity & postImage
            tracingService.Trace("Create the target entity & postImage");
            Entity entity = (Entity)context.InputParameters[PluginHelper.Target];

            #region ---------------- Validation -----------------------

            if(entity.Contains("bpa_contactid") && entity.Contains("bpa_planid"))
            {
                Guid contactid = entity.Contains("bpa_contactid") ? entity.GetAttributeValue<EntityReference>("bpa_contactid").Id : Guid.Empty;
                Guid planid = entity.Contains("bpa_planid") ? entity.GetAttributeValue<EntityReference>("bpa_planid").Id : Guid.Empty;

                if (ContactHelper.FetchMemberPlan(service, tracingService, contactid, planid) != null)
                    throw new InvalidPluginExecutionException(OperationStatus.Failed, "Member already exists in selected plan. Please verify.");

                Entity contact = service.Retrieve(PluginHelper.Contact, contactid, new ColumnSet(new string[] {"bpa_contacttype" }));
                OptionSetValue contactType = contact.Contains("bpa_contacttype") ? contact.GetAttributeValue<OptionSetValue>("bpa_contacttype") : new OptionSetValue(0);
                if(contactType.Value != (int)ContactContactType.Member)
                    throw new InvalidPluginExecutionException(OperationStatus.Failed, "You are not able to create Member Plan as Selected member contact type is not a 'Member'.");

            }

            #endregion

            //Strip out the formatting and add it to field for searching
            if (entity.Contains("bpa_socialinsurancenumber"))
            entity["bpa_sinnoformat"] = new String(entity.GetAttributeValue<String>("bpa_socialinsurancenumber").ToCharArray().Where(char.IsDigit).ToArray());

            //Logic for making sure the drug card Id is unique
            if(entity.Contains("bpa_drugcardid"))
            {
                if (!MemberHelper.isDrugCardIdUnique(service,tracingService, entity.GetAttributeValue<String>("bpa_drugcardid").Trim()))
                    throw new InvalidPluginExecutionException(OperationStatus.Failed, "Drug Card ID Already Present in The System. Please Enter a Unique Drug Card ID.");
            }
        }
    }
}