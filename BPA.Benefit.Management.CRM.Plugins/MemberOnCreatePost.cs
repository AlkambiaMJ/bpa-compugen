﻿using BPA.Benefit.Management.CRM.Plugins.Helper;
using Microsoft.Xrm.Sdk;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BPA.Benefit.Management.CRM.Plugins
{
    public class MemberOnCreatePost : Plugin
    {
        private static object _lockingObject = new object();
        /// <summary>
        /// Initializes a new instance of the <see cref="PreAttendanceCreate"/> class.
        /// </summary>
        public MemberOnCreatePost()
            : base(typeof(MemberOnCreatePost))
        {
            base.RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(40, "Update", PluginHelper.bpa_member, new Action<LocalPluginContext>(ExecutePostMemberCreate)));

            // Note : you can register for more events here if this plugin is not specific to an individual entity and message combination.
            // You may also need to update your RegisterFile.crmregister plug-in registration file to reflect any change.
        }

        /// <summary>
        /// Executes the plug-in.
        /// </summary>
        /// <param name="localContext">The <see cref="LocalPluginContext"/> which contains the
        /// <see cref="IPluginExecutionContext"/>,
        /// <see cref="IOrganizationService"/>
        /// and <see cref="ITracingService"/>
        /// </param>
        /// <remarks>
        /// For improved performance, Microsoft Dynamics CRM caches plug-in instances.
        /// The plug-in's Execute method should be written to be stateless as the constructor
        /// is not called for every invocation of the plug-in. Also, multiple system threads
        /// could execute the plug-in at the same time. All per invocation state information
        /// is stored in the context. This means that you should not use global variables in plug-ins.
        /// </remarks>
        protected void ExecutePostMemberCreate(LocalPluginContext localContext)
        {
            return;

            //if (localContext == null)
            //    throw new ArgumentNullException("localContext");

            //// Create the context variables
            //IPluginExecutionContext context = localContext.PluginExecutionContext;
            //if (context.Depth > 1)
            //    return;
            //IOrganizationService service = localContext.OrganizationService;
            //ITracingService tracingService = localContext.TracingService;

            //if (context.InputParameters.Contains(PluginHelper.Target) && context.InputParameters[PluginHelper.Target] is Entity)
            //{
            //    // Create the target entity and postimage
            //    Entity entity = (Entity)context.InputParameters["Target"];
            //    Entity postImageEntity = (context.PostEntityImages != null && context.PostEntityImages.Contains(PluginHelper.postImageAlias)) ? context.PostEntityImages[PluginHelper.postImageAlias] : null;

            //    // Update the Get all values
            //    // ContactHelper.UpdateMemberPlanDetails(service, postimage);
            //}
        }
    }
}
