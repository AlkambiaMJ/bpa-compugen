﻿#region

using System;
using BPA.Benefit.Management.CRM.Plugins.Helper;
using Microsoft.Xrm.Sdk;

#endregion

namespace BPA.Benefit.Management.CRM.Plugins
{
    /// <summary>
    ///     FundOnCreateUpdatePre PlugIn.
    /// </summary>
    public class FundOnCreateUpdatePre : Plugin
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="FundOnCreateUpdatePre" /> class.
        /// </summary>
        public FundOnCreateUpdatePre(string unsecureString, string secureString) : base(typeof(FundOnCreateUpdatePre), unsecureString,  secureString)
        {
            RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(20, "Create",
                PluginHelper.BpaFund, ExecuteOnCreateUpdatePre));
            RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(20, "Update",
                PluginHelper.BpaFund, ExecuteOnCreateUpdatePre));
            
            // Note : you can register for more events here if this plugin is not specific to an individual entity and message combination.
            // You may also need to update your RegisterFile.crmregister plug-in registration file to reflect any change.
        }

        /// <summary>
        ///     Executes the plug-in.
        /// </summary>
        /// <param name="localContext">
        ///     The <see cref="Plugin.LocalPluginContext" /> which contains the
        ///     <see cref="IPluginExecutionContext" />,
        ///     <see cref="IOrganizationService" />
        ///     and <see cref="ITracingService" />
        /// </param>
        /// <remarks>
        ///     For improved performance, Microsoft Dynamics CRM caches plug-in instances.
        ///     The plug-in's Execute method should be written to be stateless as the constructor
        ///     is not called for every invocation of the plug-in. Also, multiple system threads
        ///     could execute the plug-in at the same time. All per invocation state information
        ///     is stored in the context. This means that you should not use global variables in plug-ins.
        /// </remarks>
        protected void ExecuteOnCreateUpdatePre(LocalPluginContext localContext)
        {
            // DONE - TODO JK - Add more comments and trace statements

            // If local context not passed in, raise error
            if (localContext == null)
                throw new ArgumentNullException($"Local context not passed in.");

            // Create the context variables
            IPluginExecutionContext context = localContext.PluginExecutionContext;
            IOrganizationService service = localContext.OrganizationService;
            ITracingService tracingService = localContext.TracingService;

            // If Target is not passed in, raise error
            tracingService.Trace("If Target is not passed in, raise error");
            if (!context.InputParameters.Contains(PluginHelper.Target) ||
                !(context.InputParameters[PluginHelper.Target] is Entity))
                throw new ArgumentNullException($"Target is not of type Entity.");

            // Create the target entity & preImage
            tracingService.Trace("Create the target entity & preImage");
            Entity entity = (Entity)context.InputParameters[PluginHelper.Target];
            Entity preImage = context.PreEntityImages != null &&
                                    context.PreEntityImages.Contains(PluginHelper.PreImageAlias)
                ? context.PreEntityImages[PluginHelper.PreImageAlias] : null;
            
            if (context.MessageName.ToLower() == PluginHelper.Update.ToLower() && preImage == null)
                throw new ArgumentNullException($"preImage entity is not defined.");
           
            //Get all data
            OptionSetValue fundType = new OptionSetValue(0);
            if (entity.Contains("bpa_fundtype"))
                fundType = entity.GetAttributeValue<OptionSetValue>("bpa_fundtype");
            else if (preImage.Contains("bpa_fundtype"))
                fundType = preImage.GetAttributeValue<OptionSetValue>("bpa_fundtype");
            tracingService.Trace($"fund Type {fundType.Value}");
            
            // Find out Is there any Default Fund for current Fund Type for current Trust
            tracingService.Trace("Find out Is there any Default Fund for current Fund Type for current Trust");
            EntityReference trustRef = null;
            if (entity.Attributes.Contains("bpa_trust"))
                trustRef = entity.GetAttributeValue<EntityReference>("bpa_trust");
            else if (preImage.Contains("bpa_trust"))
                trustRef = preImage.GetAttributeValue<EntityReference>("bpa_trust");
            if (trustRef == null) return;
            tracingService.Trace($"Trust exists {trustRef.Id} - {trustRef.Name}");


            // Check Fund type is pension then user cannot create more than one fund
            //if ((fundType.Value == (int)BpaFundbpaFundType.Pension) && (FundHelper.IsPensionFundExists(service, tracingService, trustRef.Id, fundType.Value, entity.Id)))
            //    throw new InvalidPluginExecutionException(OperationStatus.Failed, @"Pension type fund already exists for current trust. Please select different type and try again.");

            if (!entity.Attributes.Contains("bpa_defaultfund")) return;
            bool isDefaultFund = false;
            if (entity.Contains("bpa_defaultfund"))
                isDefaultFund = entity.GetAttributeValue<bool>("bpa_defaultfund");

            if (!isDefaultFund) return;

            bool isUpdate = (context.MessageName.ToLower() == PluginHelper.Update.ToLower());
            tracingService.Trace($"Before Query");
            
            //Fetch does this trust already had Default fund?
            bool isDefaultFundExists = FundHelper.IsDefaultFundExists(service, tracingService, trustRef.Id, fundType.Value, isUpdate, entity.Id);
            tracingService.Trace($"Is Default Fund Exists {isDefaultFundExists}");

            if (isDefaultFundExists)
                throw new InvalidPluginExecutionException(OperationStatus.Failed,
                    @"A default fund already exists for selected Fund Type. If you wish to change the current fund to the default fund, please change the default fund flag to 'NO' on the original fund.");
        }
    }
}