﻿#region

using System;
using Microsoft.Xrm.Sdk;
using BPA.Benefit.Management.CRM.Plugins.Helper;
#endregion

namespace BPA.Benefit.Management.CRM.Plugins
{
    public class TransactionOnCreateUpdatePre : Plugin
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="TransactionOnCreateUpdatePre" /> class.
        /// </summary>
        public TransactionOnCreateUpdatePre()
            : base(typeof(TransactionOnCreateUpdatePre))
        {
            base.RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(20, "Create", PluginHelper.BpaTransaction, 
                new Action<LocalPluginContext>(ExecutePreTransactionCreate)));
            base.RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(20, "Update", PluginHelper.BpaTransaction, 
                new Action<LocalPluginContext>(ExecutePreTransactionCreate)));

            // Note : you can register for more events here if this plugin is not specific to an individual entity and message combination.
            // You may also need to update your RegisterFile.crmregister plug-in registration file to reflect any change.
        }

        /// <summary>
        ///     Executes the plug-in.
        /// </summary>
        /// <param name="localContext">
        ///     The <see cref="Plugin.LocalPluginContext" /> which contains the
        ///     <see cref="IPluginExecutionContext" />,
        ///     <see cref="IOrganizationService" />
        ///     and <see cref="ITracingService" />
        /// </param>
        /// <remarks>
        ///     For improved performance, Microsoft Dynamics CRM caches plug-in instances.
        ///     The plug-in's Execute method should be written to be stateless as the constructor
        ///     is not called for every invocation of the plug-in. Also, multiple system threads
        ///     could execute the plug-in at the same time. All per invocation state information
        ///     is stored in the context. This means that you should not use global variables in plug-ins.
        /// </remarks>
        protected void ExecutePreTransactionCreate(LocalPluginContext localContext)
        {
            /**************************************************
            WE MOVED THIS CODE TO AUTO NUMBER GENERIC PLUGINS
            ***************************************************/
            // If local context not passed in, raise error
            if (localContext == null)
                throw new ArgumentNullException($"Local context not passed in.");

            IPluginExecutionContext context = localContext.PluginExecutionContext;
            ITracingService tracingService = localContext.TracingService;

            // If Target is not passed in, raise error
            tracingService.Trace("If Target is not passed in, raise error");
            if (!context.InputParameters.Contains(PluginHelper.Target) ||
                !(context.InputParameters[PluginHelper.Target] is Entity))
                throw new ArgumentNullException($"Target is not of type Entity.");

            // Create the target entity
            tracingService.Trace("Create the target entity");
            Entity entity = (Entity)context.InputParameters[PluginHelper.Target];

            // If it is a Create Message
            if (context.MessageName == PluginHelper.Create)
            {
                // Set the Transaction's Name if it hasn't been set already 
                if (!entity.Contains("bpa_name"))
                    entity["bpa_name"] = Guid.NewGuid().ToString();

                // If status code is passed in
                if (entity.Contains("statuscode"))
                {
                    int statuscode = entity.GetAttributeValue<OptionSetValue>("statuscode").Value;

                    // If it is Posted, set the Posted Date
                    if (statuscode == (int)TransactionStatus.Posted)
                        entity["bpa_posteddate"] = DateTime.Now;
                }
                else
                {
                    // The status code isn't passed in, so we will set it to Posted by default and
                    // set the Posted Date
                    entity["statuscode"] = new OptionSetValue((int)TransactionStatus.Posted);
                    entity["bpa_posteddate"] = DateTime.Now; 
                }
            }
            else if (context.MessageName == PluginHelper.Update)
            {
                // If status code is passed in
                if (entity.Contains("statuscode"))
                {
                    int statuscode = entity.GetAttributeValue<OptionSetValue>("statuscode").Value;

                    // If it is Posted, set the Posted Date
                    if (statuscode == (int)TransactionStatus.Posted)
                        entity["bpa_posteddate"] = DateTime.Now;
                }
            }
        }
    }
}