﻿#region

using System;
using BPA.Benefit.Management.CRM.Plugins.Helper;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;

#endregion

namespace BPA.Benefit.Management.CRM.Plugins
{
    /// <summary>
    ///     TransactionOnCreatePost PlugIn.
    /// </summary>
    public class TransactionOnCreatePost : Plugin
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="TransactionOnCreatePost" /> class.
        /// </summary>
        public TransactionOnCreatePost(string unsecureString, string secureString)
            : base(typeof(TransactionOnCreatePost), unsecureString, secureString)
        {
            RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(40, "Create",
                PluginHelper.BpaTransaction, ExecutePostTransactionCreate));

            // Note : you can register for more events here if this plugin is not specific to an individual entity and message combination.
            // You may also need to update your RegisterFile.crmregister plug-in registration file to reflect any change.
        }

        /// <summary>
        ///     Executes the plug-in.
        /// </summary>
        /// <param name="localContext">
        ///     The <see cref="Plugin.LocalPluginContext" /> which contains the
        ///     <see cref="IPluginExecutionContext" />,
        ///     <see cref="IOrganizationService" />
        ///     and <see cref="ITracingService" />
        /// </param>
        /// <remarks>
        ///     For improved performance, Microsoft Dynamics CRM caches plug-in instances.
        ///     The plug-in's Execute method should be written to be stateless as the constructor
        ///     is not called for every invocation of the plug-in. Also, multiple system threads
        ///     could execute the plug-in at the same time. All per invocation state information
        ///     is stored in the context. This means that you should not use global variables in plug-ins.
        /// </remarks>
        protected void ExecutePostTransactionCreate(LocalPluginContext localContext)
        {
            // If local context not passed in, raise error
            if (localContext == null)
                throw new ArgumentNullException($"Local context not passed in.");

            // Create the context variables
            IPluginExecutionContext context = localContext.PluginExecutionContext;
            ITracingService tracingService = localContext.TracingService;
            IOrganizationService service = localContext.OrganizationService;
            IServiceProvider serviceProvider = localContext.ServiceProvider;

            // If Target is not passed in, raise error
            tracingService.Trace("If Target is not passed in, raise error");
            if (!context.InputParameters.Contains(PluginHelper.Target) ||
                !(context.InputParameters[PluginHelper.Target] is Entity))
                throw new ArgumentNullException($"Target is not of type Entity.");

            // Create the target entity
            tracingService.Trace("Create the target entity & postImage");
            Entity entity = (Entity)context.InputParameters[PluginHelper.Target];

            // Check transaction Status = 'Posted'
            // 
            //Submission type transactions are created 'Pending', 
            // so if it is a transaction from a submission,
            // we can skip the rest of the code
            int status = 0;
            if (entity.Contains("statuscode"))
                status = entity.GetAttributeValue<OptionSetValue>("statuscode").Value;
            if (status != (int)TransactionStatus.Posted) return;

            // Create the postImage
            Entity postImageEntity = context.PostEntityImages != null &&
                context.PostEntityImages.Contains(PluginHelper.PostImageAlias)
                ? context.PostEntityImages[PluginHelper.PostImageAlias]
                : null;
            if (postImageEntity == null)
                throw new ArgumentNullException($"postImage entity is not defined.");

            // Create a service object that is using the admin account
            Guid executeUser = context.UserId;
            //string crmAdminUserId = PluginConfiguration[PluginHelper.ConfigCrmAdmin];
            string crmAdminUserId = string.Empty;
            try
            {
                crmAdminUserId = PluginConfiguration[PluginHelper.ConfigCrmAdmin];
            }
            catch (Exception ex)
            {
                // Get value from Database
                crmAdminUserId = ConfigurationHelper.FetchValueId(service, PluginHelper.ConfigCrmAdmin);
            }
            if (string.IsNullOrEmpty(crmAdminUserId))
            {
                tracingService.Trace("ERROR FROM AdjustmentOnPostUpdate Plugin - CRM Admin USer Could not Found.");
                throw new InvalidPluginExecutionException(OperationStatus.Failed, $@"The given key was not present in the dictionary. Please contact BPA Administrator");
            }
            IOrganizationServiceFactory serviceFactory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
            IOrganizationService serviceAdmin = serviceFactory.CreateOrganizationService(new Guid(crmAdminUserId));

            // Create the Benefit Audit & Event Activity records (if required)
            // These are only applicable to "Status" type transactions
            // "Submission" type transactions would be filtered out above 
            // to speed up transaction creation
            CreateBenefitAudit(service, serviceAdmin, tracingService, entity, postImageEntity);
            CreateEventActivity(service, tracingService, entity);
        }

        protected void CreateBenefitAudit(IOrganizationService service, IOrganizationService serviceAdmin, ITracingService tracingService, Entity entity, Entity imageEntity)
        {
            tracingService.Trace("1. In CreateBenefitAudit Method."); 
            EntityReference contact = null;
            EntityReference NewEligibilityCategory = null;
            OptionSetValue NewTransactionType = null;
            DateTime workMonth = DateTime.MinValue;

            // Transaction Type
            if (entity.Attributes.Contains("bpa_transactiontype") && imageEntity == null)
                NewTransactionType = entity.GetAttributeValue<OptionSetValue>("bpa_transactiontype");
            else if (imageEntity != null && imageEntity.Attributes.Contains("bpa_transactiontype"))
                NewTransactionType = imageEntity.GetAttributeValue<OptionSetValue>("bpa_transactiontype");

            // Member Plan
            if (entity.Attributes.Contains("bpa_memberplanid") && imageEntity == null)
                contact = entity.GetAttributeValue<EntityReference>("bpa_memberplanid");
            else if (imageEntity != null && imageEntity.Attributes.Contains("bpa_memberplanid"))
                contact = imageEntity.GetAttributeValue<EntityReference>("bpa_memberplanid");
            if (contact == null)
                throw new ArgumentNullException($"Contact is a required attribute.");

            if ((NewTransactionType != null) &&
                 ((NewTransactionType.Value == (int)BpaTransactionbpaTransactionType.Frozen)
                 || (NewTransactionType.Value == (int)BpaTransactionbpaTransactionType.Inactive)
                 || (NewTransactionType.Value == (int)BpaTransactionbpaTransactionType.FundAssistedInBenefit)
                 || (NewTransactionType.Value == (int)BpaTransactionbpaTransactionType.NewMember)
                 || (NewTransactionType.Value == (int)BpaTransactionbpaTransactionType.NotInBenefit)
                 || (NewTransactionType.Value == (int)BpaTransactionbpaTransactionType.ReInstating)
                 || (NewTransactionType.Value == (int)BpaTransactionbpaTransactionType.UnderReview)
                 || (NewTransactionType.Value == (int)BpaTransactionbpaTransactionType.TransferOut)
                 || (NewTransactionType.Value == (int)BpaTransactionbpaTransactionType.InBenefit)
                 || (NewTransactionType.Value == (int)BpaTransactionbpaTransactionType.SelfPayInBenefit)))
            {
                // Work Month
                if (entity.Attributes.Contains("bpa_transactiondate") && imageEntity == null)
                    workMonth = entity.GetAttributeValue<DateTime>("bpa_transactiondate");
                else if (imageEntity != null && imageEntity.Attributes.Contains("bpa_transactiondate"))
                    workMonth = imageEntity.GetAttributeValue<DateTime>("bpa_transactiondate");

                // New Eligibility Category
                if (entity.Attributes.Contains("bpa_currenteligibilitycategory") && imageEntity == null)
                    NewEligibilityCategory = entity.GetAttributeValue<EntityReference>("bpa_currenteligibilitycategory");
                else if (imageEntity != null && imageEntity.Attributes.Contains("bpa_currenteligibilitycategory"))
                    NewEligibilityCategory = imageEntity.GetAttributeValue<EntityReference>("bpa_currenteligibilitycategory");
                if (NewEligibilityCategory == null)
                    throw new ArgumentNullException($"Current Eligibility Category is a required attribute.");

                // Benefit Work Month
                DateTime benefitWorkMonth = DateTime.MinValue; 
                if (entity.Attributes.Contains("bpa_benefitmonth") && imageEntity == null)
                    benefitWorkMonth = entity.GetAttributeValue<DateTime>("bpa_benefitmonth");
                else if (imageEntity != null && imageEntity.Attributes.Contains("bpa_benefitmonth"))
                    benefitWorkMonth = imageEntity.GetAttributeValue<DateTime>("bpa_benefitmonth");

                // Find the Date of Birth from Contact - MEmber Plan
                Entity contactInfo = MemberHelper.FetchContactByMemberPlanId(service, tracingService, contact.Id);
                DateTime dateofBirth = contactInfo.Contains("bpa_dateofbirth") ? contactInfo.GetAttributeValue<DateTime>("bpa_dateofbirth") : DateTime.MinValue;
                EntityReference ownerid = contactInfo.Contains("ownerid") ? contactInfo.GetAttributeValue<EntityReference>("ownerid") : null;
                int memberAge = PluginHelper.CalculateAge(tracingService, benefitWorkMonth, dateofBirth);

                if (ownerid == null)
                    throw new InvalidPluginExecutionException($@"Member Plan owner is null please contact administrator;");


                // Fetch Eligibility Category Detail 
                Entity NewECDetail = EligibilityCategoryHelper.FetchEligibilityCategoryDetails(service, tracingService, NewEligibilityCategory.Id, workMonth);
                EntityReference NewEligibilityCategoryDetail = null;
                if (NewECDetail != null)
                    NewEligibilityCategoryDetail = new EntityReference(PluginHelper.BpaEligibilitycategorydetail, NewECDetail.Id);
                tracingService.Trace("After New ECDEtail");
                // Find Is Benefit Audit Record Exists Or not ?
                Entity ifExists = BenefitAuditHelper.IsEligibilityTransactionExists(service, tracingService, contact.Id, workMonth);
                tracingService.Trace("After ifExists");

                EntityReference OldEligibilityCategory = null;
                EntityReference OldEligibilityCategoryDetail = null;
                int OldEligibility = 0;
                int cntLives = 0;
                bool isCategoryChange = false, isBenefitStatusChange = false;
                
                // If Not Exists then All Old Fields are empty 
                if (ifExists != null)
                {
                    // Get all fields values
                    if (ifExists.Contains("bpa_neweligibilitycategoryid"))
                        OldEligibilityCategory = ifExists.GetAttributeValue<EntityReference>("bpa_neweligibilitycategoryid");
                    if (ifExists.Contains("bpa_neweligibilitycategorydetailid"))
                        OldEligibilityCategoryDetail = ifExists.GetAttributeValue<EntityReference>("bpa_neweligibilitycategorydetailid");
                    if (ifExists.Contains("bpa_neweligibility"))
                        OldEligibility = ifExists.GetAttributeValue<OptionSetValue>("bpa_neweligibility").Value;

                    // Check if Category Change
                    if (NewEligibilityCategory.Id != OldEligibilityCategory.Id)
                        isCategoryChange = true;

                    if ((OldEligibility == (int)BpaTransactionbpaTransactionType.FundAssistedInBenefit || OldEligibility == (int)BpaTransactionbpaTransactionType.InBenefit || OldEligibility == (int)BpaTransactionbpaTransactionType.SelfPayInBenefit)
                        && (NewTransactionType.Value == (int)BpaTransactionbpaTransactionType.NotInBenefit || NewTransactionType.Value == (int)BpaTransactionbpaTransactionType.Inactive
                                || NewTransactionType.Value == (int)BpaTransactionbpaTransactionType.NewMember || NewTransactionType.Value == (int)BpaTransactionbpaTransactionType.ReInstating
                                || NewTransactionType.Value == (int)BpaTransactionbpaTransactionType.UnderReview || NewTransactionType.Value == (int)BpaTransactionbpaTransactionType.TransferOut
                                || NewTransactionType.Value == (int)BpaTransactionbpaTransactionType.Frozen))
                    {
                        isBenefitStatusChange = true;
                    }
                    else if ((OldEligibility == (int)BpaTransactionbpaTransactionType.FundAssistedInBenefit || OldEligibility == (int)BpaTransactionbpaTransactionType.InBenefit)
                        && (NewTransactionType.Value == (int)BpaTransactionbpaTransactionType.SelfPayInBenefit))
                    {
                        isBenefitStatusChange = true;

                    }
                    else if ((NewTransactionType.Value == (int)BpaTransactionbpaTransactionType.FundAssistedInBenefit || NewTransactionType.Value == (int)BpaTransactionbpaTransactionType.InBenefit)
                        && (OldEligibility == (int)BpaTransactionbpaTransactionType.SelfPayInBenefit))
                    {
                        isBenefitStatusChange = true;
                    }


                }
                else
                    isBenefitStatusChange = true;

                Guid benefitAuditHeaderId = Guid.Empty;
                tracingService.Trace("Before if (isCategoryChange || isBenefitStatusChange)");
                if (isCategoryChange || isBenefitStatusChange)
                {
                    tracingService.Trace("Inside if (isCategoryChange || isBenefitStatusChange)");

                    if ((NewEligibilityCategory != null) && (NewTransactionType.Value == (int)BpaTransactionbpaTransactionType.FundAssistedInBenefit
                                    || NewTransactionType.Value == (int)BpaTransactionbpaTransactionType.InBenefit
                                    || NewTransactionType.Value == (int)BpaTransactionbpaTransactionType.SelfPayInBenefit))
                    {
                        cntLives = 1;

                        // Create Benefit Audit - HEADER record
                        benefitAuditHeaderId = BenefitAuditHelper.CreateBenefitAudit(serviceAdmin, tracingService, contact, benefitWorkMonth, workMonth, OldEligibilityCategory, OldEligibilityCategoryDetail, OldEligibility,
                            NewEligibilityCategory, NewEligibilityCategoryDetail, NewTransactionType.Value, isCategoryChange, isBenefitStatusChange, ownerid);

                        // Fetch All Benefit Records
                        EntityCollection benefitCollection = BenefitAuditHelper.FetchAllBenefitsByCategoryDetailId(serviceAdmin, tracingService, NewEligibilityCategoryDetail.Id);
                        tracingService.Trace("AFTER benefitCollection");

                        if (benefitCollection != null && benefitCollection.Entities.Count > 0)
                        {

                            foreach (Entity benefit in benefitCollection.Entities)
                            {
                                // Get All Values
                                bool isSkip = false;
                                decimal premiumRate = benefit.Contains("bpa_premiumrate") ? benefit.GetAttributeValue<decimal>("bpa_premiumrate") : 0;
                                int benefitStatusType = benefit.Contains("bpa_benefit1.bpa_benefitstatustype") ? ((OptionSetValue)benefit.GetAttributeValue<AliasedValue>("bpa_benefit1.bpa_benefitstatustype").Value).Value : 0;
                                EntityReference benefitid = benefit.Contains("bpa_benefitid") ? benefit.GetAttributeValue<EntityReference>("bpa_benefitid"): null;
                                int maximumAge = benefit.Contains("bpa_benefit1.bpa_maximumagerestriction") ? ((int)benefit.GetAttributeValue<AliasedValue>("bpa_benefit1.bpa_maximumagerestriction").Value) : -1;
                                
                                if (maximumAge != -1 && dateofBirth != DateTime.MinValue)
                                {
                                    if (benefitWorkMonth >= dateofBirth.AddYears(maximumAge))
                                        isSkip = true;
                                }

                                if ((benefitStatusType == (int)BpaBenefiStatusType.Active) &&
                                        ((NewTransactionType.Value == (int)BpaTransactionbpaTransactionType.SelfPayInBenefit) ||
                                            (NewTransactionType.Value == (int)BpaTransactionbpaTransactionType.FundAssistedInBenefit)))
                                        isSkip = true;
                                if ((benefitStatusType == (int)BpaBenefiStatusType.SelfPay) && ((NewTransactionType.Value == (int)BpaTransactionbpaTransactionType.InBenefit)))
                                    isSkip = true;
                                

                                if (!isSkip)
                                {
                                    // Create Benefit Audit - DETAIL Record(s)
                                    Guid benefitDetail = BenefitAuditHelper.CreateBenefitAuditDetail(serviceAdmin, tracingService, benefitAuditHeaderId, benefitid, premiumRate, cntLives,
                                        NewEligibilityCategoryDetail, ownerid);
                                }
                            } // end of foreach
                        }
                    }


                    if ((OldEligibilityCategory != null) && (OldEligibility == (int)BpaTransactionbpaTransactionType.FundAssistedInBenefit
                                    || OldEligibility == (int)BpaTransactionbpaTransactionType.InBenefit
                                    || OldEligibility == (int)BpaTransactionbpaTransactionType.SelfPayInBenefit) && (isBenefitStatusChange || isCategoryChange))
                    {
                        cntLives = -1;
                        if (benefitAuditHeaderId == Guid.Empty)
                        {
                            // Create Benefit audit - HEADER record
                            benefitAuditHeaderId = BenefitAuditHelper.CreateBenefitAudit(serviceAdmin, tracingService, contact, benefitWorkMonth, workMonth, OldEligibilityCategory, OldEligibilityCategoryDetail, OldEligibility,
                                NewEligibilityCategory, NewEligibilityCategoryDetail, NewTransactionType.Value, isCategoryChange, isBenefitStatusChange, ownerid);
                        }

                        // If Old Benefit Header Exists then used that Benefit Audit Detail Records 
                        if (ifExists != null && OldEligibilityCategoryDetail != null)
                        {
                            EntityCollection ExistsAuditDetails = BenefitAuditHelper.FetchAllBenefitAuditDetails(serviceAdmin, tracingService, ifExists.Id, OldEligibilityCategoryDetail.Id);
                            if (ExistsAuditDetails != null && ExistsAuditDetails.Entities.Count > 0)
                            {
                                foreach (Entity detail in ExistsAuditDetails.Entities)
                                {
                                    decimal premiumRate = detail.Contains("bpa_rate") ? detail.GetAttributeValue<decimal>("bpa_rate") : 0;
                                    EntityReference benefitid = detail.Contains("bpa_benefitid") ? detail.GetAttributeValue<EntityReference>("bpa_benefitid") : null;

                                    Guid benefitDetail = BenefitAuditHelper.CreateBenefitAuditDetail(serviceAdmin, tracingService, benefitAuditHeaderId, benefitid, premiumRate, cntLives,
                                            OldEligibilityCategoryDetail, ownerid);
                                }
                            }
                        }

                        else
                        {
                            // Fetch All Benefit Record
                            EntityCollection benefitCollection = BenefitAuditHelper.FetchAllBenefitsByCategoryDetailId(serviceAdmin, tracingService, OldEligibilityCategoryDetail.Id);
                            if (benefitCollection != null && benefitCollection.Entities.Count > 0)
                            {
                                foreach (Entity benefit in benefitCollection.Entities)
                                {
                                    // Get All Values
                                    bool isSkip = false;
                                    decimal premiumRate = benefit.Contains("bpa_premiumrate") ? benefit.GetAttributeValue<decimal>("bpa_premiumrate") : 0;
                                    int benefitStatusType = benefit.Contains("bpa_benefit1.bpa_benefitstatustype") ? ((OptionSetValue)benefit.GetAttributeValue<AliasedValue>("bpa_benefit1.bpa_benefitstatustype").Value).Value : 0;
                                    EntityReference benefitid = benefit.Contains("bpa_benefitid") ? benefit.GetAttributeValue<EntityReference>("bpa_benefitid") : null;
                                    int maximumAge = benefit.Contains("bpa_benefit1.bpa_maximumagerestriction") ? ((int)benefit.GetAttributeValue<AliasedValue>("bpa_benefit1.bpa_maximumagerestriction").Value) : -1;

                                    if (maximumAge != -1 && dateofBirth != DateTime.MinValue)
                                    {
                                        if (benefitWorkMonth >= dateofBirth.AddYears(maximumAge))  //if (memberAge > maximumAge)
                                            isSkip = true;
                                    }
                                    else
                                    {
                                        if ((benefitStatusType == (int)BpaBenefiStatusType.Active) &&
                                        ((OldEligibility == (int)BpaTransactionbpaTransactionType.SelfPayInBenefit) ||
                                         (OldEligibility == (int)BpaTransactionbpaTransactionType.FundAssistedInBenefit)))
                                            isSkip = true;
                                        else if ((benefitStatusType == (int)BpaBenefiStatusType.SelfPay) && ((OldEligibility == (int)BpaTransactionbpaTransactionType.InBenefit)))
                                            isSkip = true;
                                    }
                                    if (!isSkip)
                                    {
                                        // Create Benefit Audit - DETAIL Record(s)
                                        Guid benefitDetail = BenefitAuditHelper.CreateBenefitAuditDetail(serviceAdmin, tracingService, benefitAuditHeaderId, benefitid, premiumRate, cntLives,
                                                OldEligibilityCategoryDetail, ownerid);
                                    }
                                } // End of foreach
                            }
                        } // end of else
                    }
                }
                tracingService.Trace("Before Point 1");
                // Last Insurance Effective Date
                if (NewTransactionType.Value == (int)BpaTransactionbpaTransactionType.FundAssistedInBenefit 
                        || NewTransactionType.Value == (int)BpaTransactionbpaTransactionType.InBenefit
                        || NewTransactionType.Value == (int)BpaTransactionbpaTransactionType.SelfPayInBenefit)
                {
                    tracingService.Trace("Inside IN Beneit");

                    // Find previous month eligibility status
                    if (TransactionHelper.IsMemberNotInBenefitPreviousMonth(serviceAdmin, contact.Id, workMonth))
                    {
                        ContactHelper.UpdateLastInsuranceEffectiveDate(service, tracingService, contact.Id, benefitWorkMonth);
                    } 
                    else
                    {
                        // TFS# 1353 - Insurance Effective Date
                        tracingService.Trace("ELSE Insurance Effective Date");

                        // Get the Member Plan's Insurance Effective Date
                        DateTime effectiveDate = ContactHelper.GetInsuranceEffectiveDate(service, tracingService, contact.Id);
                        // get MAx(Benefit Month) of 'NOT IN BENEFIT' And Add(+1) 
                        DateTime maxBenefitMonth = TransactionHelper.GetMaxBenefitMonth(service, tracingService, contact.Id, workMonth);
                        //throw new InvalidPluginExecutionException("MAxium benefit month");
                        // Compare both date and if not both are same than do not update otherwise update
                        if(effectiveDate.Date != maxBenefitMonth.Date)
                            ContactHelper.UpdateLastInsuranceEffectiveDate(service, tracingService, contact.Id, maxBenefitMonth);

                    }
                }
            } // End of if (transactionType != null)
        }
        
        protected void CreateEventActivity(IOrganizationService service, ITracingService tracingService, Entity entity)
        { 
            tracingService.Trace("1. In CreateEventActivity Method.");

            const string bpaTransactiontype = "bpa_transactiontype";
            string bpaMember = "bpa_memberplanid";
            EntityReference memberPlanRef = null;
            OptionSetValue transactionType = null;
            DateTime workMonth = DateTime.MinValue;
            DateTime benefitMonth = DateTime.MinValue;

            if (entity.Attributes.Contains(bpaTransactiontype))
                transactionType = entity.GetAttributeValue<OptionSetValue>(bpaTransactiontype);
            if (transactionType == null) return;

            switch (transactionType.Value)
            {
                case (int)BpaTransactionbpaTransactionType.FundAssistedInBenefit:
                case (int)BpaTransactionbpaTransactionType.InBenefit:
                case (int)BpaTransactionbpaTransactionType.SelfPayInBenefit:
                    if (entity.Attributes.Contains(bpaMember))
                        memberPlanRef = entity.GetAttributeValue<EntityReference>(bpaMember);
                    if (memberPlanRef == null) return;

                    if (entity.Attributes.Contains("bpa_transactiondate"))
                        workMonth = entity.GetAttributeValue<DateTime>("bpa_transactiondate");
                    if (entity.Attributes.Contains("bpa_benefitmonth"))
                        benefitMonth = entity.GetAttributeValue<DateTime>("bpa_benefitmonth");

                    // Check Member previous month Eligiblity Status
                    bool isNotInBenefitForPreviousMonth = TransactionHelper.IsMemberNotInBenefitForPreviousMonth(service, memberPlanRef.Id, workMonth.AddMonths(-1));

                    // Check any Open activity Exists or not?
                    bool IsOpenActivityExists = EventHelper.IsExistsOpenEventActivity(service, memberPlanRef.Id, 922070002, benefitMonth);

                    if (!isNotInBenefitForPreviousMonth || IsOpenActivityExists)
                        return;

                    // Create Event Activity
                    Entity bpaEvent = new Entity(PluginHelper.BpaEvent);
                    Entity memberPlan = service.Retrieve(PluginHelper.BpaMemberplan, memberPlanRef.Id, new ColumnSet(new string[] { "bpa_contactid" }));
                    EntityReference contactRef = null;
                    if (memberPlan != null && memberPlan.Contains("bpa_contactid"))
                        contactRef = memberPlan.GetAttributeValue<EntityReference>("bpa_contactid");
                    bpaEvent["subject"] = "Notice of Coverage";
                    if (contactRef != null)
                    {
                        EntityReference regardingObjectId = new EntityReference(PluginHelper.Contact, contactRef.Id);
                        bpaEvent["regardingobjectid"] = regardingObjectId;
                    }

                    if (benefitMonth != DateTime.MinValue)
                        bpaEvent["bpa_benefitmonth"] = benefitMonth;

                    //if (workMonth != DateTime.MinValue)
                    //    bpaEvent["bpa_workmonth"] = workMonth;
                    bpaEvent["bpa_eventtype"] = new OptionSetValue(922070002);
                    bpaEvent["bpa_memberplanid"] = new EntityReference(PluginHelper.BpaMemberplan, memberPlanRef.Id);
                    service.Create(bpaEvent);
                    break;
            }
        }
    }
}