﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using BPA.Benefit.Management.CRM.Plugins.Helper;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using System.Threading.Tasks;

#endregion

namespace BPA.Benefit.Management.CRM.Plugins
{
    /// <summary>
    ///     SubmissionOnCreatePost PlugIn.
    /// </summary>
    public class SubmissionOnCreatePost : Plugin
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="SubmissionOnCreatePost" /> class.
        /// </summary>
        public SubmissionOnCreatePost(string unsecureString, string secureString)
            : base(typeof(SubmissionOnCreatePost), unsecureString, secureString)
        {
            RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(40, "Create",
                PluginHelper.BpaSubmission, ExecutePostSubmissionCreate));

            // Note : you can register for more events here if this plugin is not specific to an individual entity and message combination.
            // You may also need to update your RegisterFile.crmregister plug-in registration file to reflect any change.
        }

        /// <summary>
        ///     Executes the plug-in.
        /// </summary>
        /// <param name="localContext">
        ///     The <see cref="Plugin.LocalPluginContext" /> which contains the
        ///     <see cref="IPluginExecutionContext" />,
        ///     <see cref="IOrganizationService" />
        ///     and <see cref="ITracingService" />
        /// </param>
        /// <remarks>
        ///     For improved performance, Microsoft Dynamics CRM caches plug-in instances.
        ///     The plug-in's Execute method should be written to be stateless as the constructor
        ///     is not called for every invocation of the plug-in. Also, multiple system threads
        ///     could execute the plug-in at the same time. All per invocation state information
        ///     is stored in the context. This means that you should not use global variables in plug-ins.
        /// </remarks>
        protected void ExecutePostSubmissionCreate(LocalPluginContext localContext)
        {
            // If local context not passed in, raise error
            if (localContext == null)
                throw new ArgumentNullException($"Local context not passed in.");

            DateTime submissionStartTime = DateTime.Now;

            // Create the context variables
            IPluginExecutionContext context = localContext.PluginExecutionContext;
            ITracingService tracingService = localContext.TracingService;
            IOrganizationService service = localContext.OrganizationService;
            IServiceProvider serviceProvider = localContext.ServiceProvider;

            // If Target is not passed in, raise error
            tracingService.Trace("If Target is not passed in, raise error");
            if (!context.InputParameters.Contains(PluginHelper.Target) ||
                !(context.InputParameters[PluginHelper.Target] is Entity))
                throw new ArgumentNullException($"Target is not of type Entity.");

            // Create the target entity
            tracingService.Trace("Create the target entity");
            Entity entity = (Entity)context.InputParameters[PluginHelper.Target];

            //Get Execute user, Get Crm Admin User and create IORganizationService for CrM ADmin 
            Guid executeUser = context.UserId;
            //string crmAdminUserId = PluginConfiguration[PluginHelper.ConfigCrmAdmin];
            string crmAdminUserId = string.Empty;
            try
            {
                crmAdminUserId = PluginConfiguration[PluginHelper.ConfigCrmAdmin];
            }
            catch (Exception ex)
            {
                // Get value from Database
                crmAdminUserId = ConfigurationHelper.FetchValueId(service, PluginHelper.ConfigCrmAdmin);
            }
            if (string.IsNullOrEmpty(crmAdminUserId))
            {
                tracingService.Trace("ERROR FROM AdjustmentOnPostUpdate Plugin - CRM Admin USer Could not Found.");
                throw new InvalidPluginExecutionException(OperationStatus.Failed, $@"The given key was not present in the dictionary. Please contact BPA Administrator");
            }
            IOrganizationServiceFactory serviceFactory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
            IOrganizationService serviceAdmin = serviceFactory.CreateOrganizationService(new Guid(crmAdminUserId));




            string executeMultipleRequest = "0";
            try
            {
                executeMultipleRequest = PluginConfiguration[PluginHelper.ConfigExecuteMultipleRequest];
            }
            catch (Exception ex)
            {
                executeMultipleRequest = "0";
            }
            // Get Execute User
            bool executeMultiple = (executeMultipleRequest == "1");
            
            string defaultCurrency = string.Empty;
            try
            {
                defaultCurrency = PluginConfiguration[PluginHelper.ConfigDefaultCurrency];
            }
            catch (Exception ex)
            {
                // Get value from Database
                defaultCurrency = ConfigurationHelper.FetchValueId(service, PluginHelper.ConfigDefaultCurrency);
            }
            if (string.IsNullOrEmpty(defaultCurrency))
            {
                tracingService.Trace("ERROR FROM AdjustmentOnPostUpdate Plugin - CRM Default Currency Could not Found.");
                throw new InvalidPluginExecutionException(OperationStatus.Failed, $@"The given key was not present in the dictionary. Please contact BPA Administrator");
            }

            //Get all data 
            int submissionType = 0;
            if (entity.Attributes.Contains("bpa_submissiontype"))
                submissionType = entity.GetAttributeValue<OptionSetValue>("bpa_submissiontype").Value;
            tracingService.Trace($"Submission Type {submissionType.ToString()}");

            DateTime workMonth = DateTime.MinValue;
            if (entity.Attributes.Contains("bpa_submissiondate"))
                workMonth = entity.GetAttributeValue<DateTime>("bpa_submissiondate");
            tracingService.Trace($"Work Month {workMonth.ToString()}");

            EntityReference agreementRef = null;
            if (entity.Attributes.Contains("bpa_agreementid"))
            {
                agreementRef = entity.GetAttributeValue<EntityReference>("bpa_agreementid");
                string agreementName =  !string.IsNullOrEmpty(agreementRef.Name) ? agreementRef.Name : string.Empty; 
                tracingService.Trace($@"Agreement {agreementRef.Id.ToString()} - {agreementName}");
            }

            EntityReference employer = null;
            if (entity.Attributes.Contains("bpa_accountagreementid"))
            {
                employer = entity.GetAttributeValue<EntityReference>("bpa_accountagreementid");
                tracingService.Trace($"Employer {employer.Id.ToString()}");
            }
            if (employer == null)
                return;
            EntityReference owner = entity.Contains("ownerid") ? entity.GetAttributeValue<EntityReference>("ownerid") : null;

            //REGULAR / Adjustment EMPLOYER SUBMISSION
            if ((submissionType == (int)SubmissionSubmissionType.EmployerRegular) ||
                (submissionType == (int)SubmissionSubmissionType.EmployerAdjustment))
            {
                // we only create submission detail record if 'Submssion From File' is 'FALSE'
                bool isSubmissionFromFile = entity.Contains("bpa_submissionfromfile") ? entity.GetAttributeValue<bool>("bpa_submissionfromfile") : false;

                if (!isSubmissionFromFile) 
                {
                    #region ------------------ REGULAR SUBMISSION CREATED -------------------

                    // Get Ownership

                    tracingService.Trace($"In side Regular or Adjustment Submision ");

                    //Get all employment records
                    EntityCollection employments = SubmissionHelper.FetchAllEmployeesByEmployerId(service, tracingService, employer.Id);
                    tracingService.Trace($"after fetch all employment records");

                    //#Bug 829 - Flat Contribution charging Multiple times for the same member.
                    Guid flatFund = Guid.Empty;
                    bool isSubmissionExists = SubmissionHelper.IsSubmissionExistsForMonth(service, tracingService, employer.Id, workMonth);
                    tracingService.Trace($"After Checking Submission Exists for that month or not");

                    if ((submissionType == (int)SubmissionSubmissionType.EmployerRegular) && isSubmissionExists)
                    {
                        tracingService.Trace("if ((submissionType == (int)Submission_SubmissionType.EmployerRegular) && isSubmissionExists)");

                        //Find out Flat Fund from Agreement
                        List<Rate> rates = RateHelper.GetRatesByEmployerIdUsingFetchXml(service, tracingService, employer.Id, workMonth);
                        tracingService.Trace($"rates {rates.Count}");

                        Rate rate = rates.FirstOrDefault(r => r.CalculationType.Value == (int)RateCalculationType.FlatRate);
                        if (rate != null)
                        {
                            tracingService.Trace($"Flat rate {rate.FundId} - {rate.FundName}");
                            flatFund = rate.FundId.Value;
                        }
                    }

                    //Checking employment already paid flat rate for same employee or not 
                    if (employments == null || employments.Entities.Count <= 0) return;


                    // Create the Transaction Request object to hold all of our changes in
                    ExecuteTransactionRequest requestRetroprocessing = null;
                    requestRetroprocessing = new ExecuteTransactionRequest()
                    {
                        // Create an empty organization request collection.
                        Requests = new OrganizationRequestCollection(),
                        ReturnResponses = true
                    };


                    tracingService.Trace($"Before Employment Loop");


                    foreach (Entity employment in employments.Entities)
                    {
                        EntityReference memberPlan = null;
                        Guid member = Guid.Empty;
                        EntityReference labourRoleRef = null;
                        string socialInsuranceNumber = string.Empty;

                        if (employment.Contains("bpa_memberplan1.bpa_memberplanid"))
                        {
                            member = (Guid)employment.GetAttributeValue<AliasedValue>("bpa_memberplan1.bpa_memberplanid").Value;
                            memberPlan = new EntityReference(PluginHelper.BpaMemberplan, member);
                        }

                        if (employment.Contains("bpa_labourrole"))
                            labourRoleRef = employment.GetAttributeValue<EntityReference>("bpa_labourrole");
                        if (employment.Contains("bpa_memberplan1.bpa_socialinsurancenumber"))
                            socialInsuranceNumber = (string)employment.GetAttributeValue<AliasedValue>("bpa_memberplan1.bpa_socialinsurancenumber").Value;


                        // Check flat rate Transaction already Exists for current member 
                        tracingService.Trace($"SubmissionOnCreatePost: checking flat rate transaction exists for current member or not?");

                        bool isFlatRuleIgnore = false;
                        if (isSubmissionExists && flatFund != Guid.Empty)
                        {
                            isFlatRuleIgnore = TransactionHelper.IsFlatRateTransactionExists(service, tracingService, member, workMonth, flatFund, employer.Id);
                            tracingService.Trace($"SubmissionOnCreatePost: is Flat rule exist {isFlatRuleIgnore}");
                        }

                        tracingService.Trace($@"SubmissionOnCreatePost: Employment ID: {employment.Id}, MEmber Plan ID: {member}");

                        Entity crSd = SubmissionHelper.CreateSubmissionDetailPre(serviceAdmin, tracingService,
                        entity.Id, employer, employment.Id, memberPlan, labourRoleRef, workMonth, agreementRef, submissionType,
                        isFlatRuleIgnore, null, defaultCurrency, socialInsuranceNumber, owner);

                        if (executeMultiple)
                        {
                            CreateRequest createRequest = new CreateRequest { Target = crSd };
                            requestRetroprocessing.Requests.Add(createRequest);
                        }
                        else
                            service.Create(crSd);

                        tracingService.Trace($"SubmissionOnCreatePost: after createing Submission detail record");
                        tracingService.Trace($"============== ");
                    }
                    

                    if (executeMultiple)
                    {
                        tracingService.Trace($"SubmissionOnCreatePost: Inside Execute Multiple");

                        ExecuteTransactionResponse responsePostingTransaction = (ExecuteTransactionResponse)service.Execute(requestRetroprocessing);
                    }
                    #endregion
                }
            }
            else if ((submissionType == (int)SubmissionSubmissionType.Member) ||
                     (submissionType == (int)SubmissionSubmissionType.MemberSelfPayRefund)) //MEMEBER SELF-PAY
            {
                #region ---------- SELF PAY SUBMISSIONS --------------
                tracingService.Trace($"In side Member Self-Pay or Member Self-Pay Refund Submision ");

                EntityReference memberPlan = null;
                if (entity.Contains("bpa_memberplanid"))
                    memberPlan = entity.GetAttributeValue<EntityReference>("bpa_memberplanid");
                if (memberPlan == null)
                    throw new InvalidPluginExecutionException(OperationStatus.Failed, "Member Plan detail field cannot be blank.");
                
                //Fetch all information
                tracingService.Trace($"Inside (memberPlan != null) ");
                Entity contact = ContactHelper.FetchMemberPlan(service,  memberPlan.Id);

                tracingService.Trace($"Fetch Member Plan");

                EntityReference labourRoleRef = null;
                if (entity.Contains("bpa_labourroleid"))
                    labourRoleRef = entity.GetAttributeValue<EntityReference>("bpa_labourroleid");
                
                Money selfpayAmount = new Money(0);
                if (entity.Contains("bpa_selfpayamount"))
                    selfpayAmount = entity.GetAttributeValue<Money>("bpa_selfpayamount");

                tracingService.Trace($"Fetch Member Plan");

                int offset = (int)contact.GetAttributeValue<AliasedValue>("bpa_eligibilitycategory1.bpa_eligibilityoffsetmonths").Value;
                tracingService.Trace($"Eligibility Offset Months {offset}");

                //Set Start date and end date
                DateTime startDate = workMonth;
                tracingService.Trace($"Start Date {startDate}\t");
                
                tracingService.Trace($"Befor Create Submssion detail ");

                Guid SubmissionDetailId = SubmissionHelper.CreateSubmissionDetail(serviceAdmin, tracingService,
                    entity.Id, employer, Guid.Empty, memberPlan.Id, labourRoleRef, startDate, agreementRef,
                    submissionType, false, selfpayAmount,defaultCurrency, owner);
                #endregion
            }


            DateTime submissionEndTime = DateTime.Now;
            double retroprocessingTotalMinutes = submissionEndTime.Subtract(submissionStartTime).Minutes;
            double retroprocessingTotalSeconds = submissionEndTime.Subtract(submissionStartTime).Seconds;
            double retroprocessingTotalMilliSeconds = submissionEndTime.Subtract(submissionStartTime).Milliseconds;

            //throw new InvalidPluginExecutionException(OperationStatus.Failed, "CREATE Submission Completion Time \nMinutes: " + retroprocessingTotalMinutes.ToString() +
            //    "\n Seconds: " + retroprocessingTotalSeconds.ToString() +
            //    "\n Milliseconds: " + retroprocessingTotalMilliSeconds.ToString());

        }
    }
}