﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BPA.Benefit.Management.CRM.Plugins.Helper;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;

namespace BPA.Benefit.Management.CRM.Plugins
{
    public class ExecutePositivePay : IPlugin
    {
        public void Execute(IServiceProvider serviceProvider)
        {

            IPluginExecutionContext context = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));

            IOrganizationServiceFactory serviceFactory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));

            // Create service with context of current user
            IOrganizationService service = serviceFactory.CreateOrganizationService(context.UserId);

            //create tracing service
            ITracingService tracingService = (ITracingService)serviceProvider.GetService(typeof(ITracingService));


            //To get access to the image of the Quote record
            EntityReference entityRef = context.InputParameters["Target"] as EntityReference;

            //To read the input parameter
            //Money money1 = context.InputParameters["Money1"] as Money;
            //Money money2 = context.InputParameters["Money2"] as Money;

            //Money sum = new Money(money1.Value + money2.Value);

            //using this as a response output
            //context.OutputParameters["MoneySum"] = sum;

            //throw new Exception("Hey There !");

            //1. Get all cheques for this batch
            //2. Create new file
            //3. Add row for account header
            //4. Add new row for each cheque
            //5. Add row for footer and total
            //6. Create Payment Print Audit recorc on successfull PP operation
            //7. Create note and attach file to Payment Print Audit
            //8. Get Positive Pay service config parameters from fund
            //9. Call Positive Pay service and push file.

            #region Starting File Creation

            string folderLocation = @"E:\Positive Pay Files";
            System.IO.Directory.CreateDirectory(folderLocation);
            string filename = "PP" + DateTime.Now.ToString("ddMMMyyyy hhmmsstt") + ".RSAHCHIN";
            string logFilePath = folderLocation + "\\" + filename;

            StreamWriter streamWriter = new StreamWriter(logFilePath);

            #endregion

            try
            {
                //Getting banking information from Fund
                #region Header Row

                string fetchXml = $@"
                <fetch>
                  <entity name='bpa_fund' >
                    <attribute name='bpa_bankaccountnumber' />
                    <attribute name='bpa_banktransitnumber' />
                    <attribute name='bpa_positivepayserviceurl' />
                    <attribute name='bpa_positivepayfolderlocation' />
                    <attribute name='bpa_positivepaycustomernumber' />
                    <attribute name='bpa_banknumber' />
                    <link-entity name='bpa_paymentbatch' from='bpa_fundid' to='bpa_fundid' alias='PB' >
                      <attribute name='bpa_batchtotal'/>
                      <filter>
                        <condition attribute='bpa_paymentbatchid' operator='eq' value='{entityRef.Id}' />
                      </filter>
                    </link-entity>
                  </entity>
                </fetch>";

                Entity bankInfo = null;
                decimal batchTotal = decimal.Zero;

                string header = string.Empty;

                EntityCollection bankInfoResult = service.RetrieveMultiple(new FetchExpression(fetchXml));
                if (bankInfoResult != null && bankInfoResult.Entities.Count > 0)
                    bankInfo = bankInfoResult.Entities[0];

                if (bankInfo != null)
                {
                    batchTotal = ((Money)bankInfo.GetAttributeValue<AliasedValue>("PB.bpa_batchtotal").Value).Value;

                    header = "A";
                    header += bankInfo.GetAttributeValue<String>("bpa_positivepaycustomernumber").PadLeft(10, '0');
                    header += bankInfo.GetAttributeValue<String>("bpa_banktransitnumber").PadLeft(5, '0');
                    header += bankInfo.GetAttributeValue<String>("bpa_banknumber").PadLeft(4, '0');
                    header += new string(' ', 7);
                    header += bankInfo.GetAttributeValue<String>("bpa_bankaccountnumber").PadLeft(7, '0');
                    header += "CAD";
                    header += "CA";
                    header += new string(' ', 30);
                    header += DateTime.Now.ToString("yyyyMMdd");
                    header += "ISSUED";
                    header += new string(' ', 10);
                    header += "BNS";

                    streamWriter.WriteLine(header);
                }

                #endregion

                //Getting individual cheque information
                #region Body Rows

                fetchXml = $@"
                <fetch>
                 <entity name='bpa_payment' >
                    <attribute name='bpa_chequeamount' />
                    <attribute name='bpa_chequedate' />
                    <attribute name='bpa_banktransitnumber' />
                    <attribute name='bpa_chequenumber' />
                    <attribute name='bpa_paymentto' />
                    <attribute name='bpa_bankaccountnumber' />
                    <attribute name='bpa_banknumber' />
                      <link-entity name='bpa_paymentbatch' from='bpa_paymentbatchid' to='bpa_paymentbatchid' alias='PB' >
                        <filter>
                          <condition attribute='bpa_paymentbatchid' operator='eq' value='{entityRef.Id}' />
                        </filter>
                      </link-entity>
                  </entity>
                </fetch>";

                EntityCollection cheques = service.RetrieveMultiple(new FetchExpression(fetchXml));

                int numberOfCheques = cheques.Entities.Count;

                if (cheques != null && cheques.Entities.Count > 0)
                {
                    foreach (Entity cheque in cheques.Entities)
                    {
                        string chequeRow = string.Empty;
                        chequeRow += "J";
                        chequeRow += cheque.GetAttributeValue<String>("bpa_banktransitnumber").PadLeft(5, '0');
                        chequeRow += cheque.GetAttributeValue<String>("bpa_banknumber").PadLeft(4, '0');
                        chequeRow += new string(' ', 7);
                        chequeRow += cheque.GetAttributeValue<String>("bpa_bankaccountnumber").PadLeft(7, '0');
                        chequeRow += cheque.GetAttributeValue<String>("bpa_chequenumber").PadLeft(14, '0');
                        chequeRow += cheque.GetAttributeValue<Money>("bpa_chequeamount").Value.ToString("0.00").Replace(".", "").PadLeft(10, '0');
                        chequeRow += "ISSUED      ";
                        chequeRow += cheque.GetAttributeValue<DateTime>("bpa_chequedate").ToString("yyyyMMdd");
                        chequeRow += new string(' ', 30);
                        chequeRow += new string(' ', 30);
                        chequeRow += new string(' ', 30);
                        chequeRow += "CAD";
                        chequeRow += cheque.GetAttributeValue<String>("bpa_paymentto").ToUpper();

                        streamWriter.WriteLine(chequeRow);
                    }
                }

                #endregion

                //Getting footer info
                #region Footer Row

                string footer = string.Empty;
                footer += "Z";
                footer += new string(' ', 22);
                footer += numberOfCheques.ToString().PadLeft(8, '0');
                footer += batchTotal.ToString("0.00").Replace(".", "").PadLeft(14, '0');
                //footer += new string('0', 8);
                footer += new string('0', 22);

                streamWriter.WriteLine(footer);

                streamWriter.Close();

                #endregion

                //Create Payment Print Audit
                #region Payment Print Audit Record

                Entity paymentPrintAudit = new Entity("bpa_paymentprintaudit");
                paymentPrintAudit.Attributes["bpa_paymentbatchid"] = entityRef;
                paymentPrintAudit.Attributes["bpa_operationtype"] = new OptionSetValue(922070001);// Positive Pay Operation

                Guid paymentPrintAuditId = service.Create(paymentPrintAudit);

                #endregion

                //TODO: Only create the audit record if the PP service call is sucessful

                //Gather PP service call config parameters from the fund record
                #region Positive Pay Web Service Parameters

                //Getting Positive Pay credentials from fund record
                string ppCustomerNumber = bankInfo.GetAttributeValue<String>("bpa_positivepaycustomernumber");
                string ppServiceURL = bankInfo.GetAttributeValue<String>("bpa_positivepayserviceurl");
                string ppFolderLocation = bankInfo.GetAttributeValue<String>("bpa_positivepayfolderlocation");
                
                #endregion

                //Make the web service call
                #region Positive Pay Service Call

                
                #endregion

                //Create Payment Print Audit Annotation + Attachment
                #region Payment Print Audit Note

                Byte[] bytes = File.ReadAllBytes(logFilePath);
                String file = Convert.ToBase64String(bytes);

                Entity paymentAuditNote = new Entity("annotation");

                paymentAuditNote.Attributes.Add("subject","Positive Pay File");
                paymentAuditNote["filename"]=filename;
                paymentAuditNote["documentbody"]= file;
                //paymentAuditNote.Attributes.Add("notetext" , "This is a test");
                paymentAuditNote.Attributes.Add("mimetype" , "text/plain");
                paymentAuditNote.Attributes.Add("objectid" , new EntityReference("bpa_paymentprintaudit", paymentPrintAuditId));
                paymentAuditNote.Attributes.Add("objecttypecode" , 10073);

                service.Create(paymentAuditNote);

                #endregion

                //Copy the Positive Pay file to the network 
                #region Copy the Positive Pay file to the network

                File.Copy(logFilePath, ppFolderLocation + "\\" + filename,true);

                #endregion

                context.OutputParameters["Status"] = true;
            }
            catch(Exception ex)
            {
                context.OutputParameters["Status"] = false;
            }
            finally
            {
                if (streamWriter != null)
                    streamWriter.Close();
            }
        }

    }
}
