﻿#region

using System;
using System.Text;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;

#endregion

namespace BPA.Benefit.Management.CRM.Plugins.Helper
{
    public class FundHelper
    {
        public static bool IsDefaultFundExists(IOrganizationService service, ITracingService tracing, Guid trustId,
            int fundType, bool isFromUpdate, Guid fundId)
        {
            bool isExists = false;
            string fetchXml = @"
                <fetch mapping='logical' output-format='xml-platform' version='1.0' >
                  <entity name='bpa_fund' >
                    <filter>
                      <condition attribute='bpa_trust' operator='eq' value='" + trustId + @"' />
                      <condition attribute='statecode' operator='eq' value='0' />
                      <condition attribute='bpa_fundtype' operator='eq' value='" + fundType + @"' />
                      <condition attribute='bpa_defaultfund' operator='eq' value='1' />";

            if (isFromUpdate)
            {
                fetchXml += @" <condition attribute='bpa_fundid' operator='neq' value='" + fundId + @"' />";
            }

            fetchXml += @"        </filter>
                  </entity>
                </fetch>
            ";
            tracing.Trace($"Fetch Query\n {fetchXml}");

            EntityCollection funds = service.RetrieveMultiple(new FetchExpression(fetchXml));
            if (funds != null && funds.Entities.Count > 0)
            {
                tracing.Trace($"Fetch Query\n {funds.Entities.Count}");
                isExists = true;
            }
            return isExists;
        }


        public static Guid FindDefaultFundByFundType(IOrganizationService service, ITracingService tracing, int fundType, Guid memberPlanId)
        {
            Guid fundId = Guid.Empty;
            string fetchXml = $@"
                <fetch top='50' >
                  <entity name='bpa_memberplan' >
                    <filter>
                      <condition attribute='bpa_memberplanid' operator='eq' value='{memberPlanId.ToString()}' />
                    </filter>
                    <link-entity name='bpa_sector' from='bpa_sectorid' to='bpa_planid' alias='P' >
                      <link-entity name='bpa_trust' from='bpa_trustid' to='bpa_trust' alias='T'>
                        <link-entity name='bpa_fund' from='bpa_trust' to='bpa_trustid' alias='F'>
                          <attribute name='bpa_name' />
                          <attribute name='bpa_fundid' />
                          <attribute name='bpa_defaultfund' />
                          <filter>
                            <condition attribute='statecode' operator='eq' value='0' />
                            <condition attribute='bpa_fundtype' operator='eq' value='{fundType.ToString()}' />
                          </filter>
                          <order attribute='bpa_defaultfund' descending='true' />
                        </link-entity>
                      </link-entity>
                    </link-entity>
                  </entity>
                </fetch>
            ";

            tracing.Trace($"Fetch Query\n {fetchXml}");

            EntityCollection funds = service.RetrieveMultiple(new FetchExpression(fetchXml));
            if (funds != null && funds.Entities.Count > 0)
            {
                tracing.Trace($"Fetch Query\n {funds.Entities.Count}");
                Entity fund = funds.Entities[0];
                if (fund.Contains("F.bpa_fundid"))
                    fundId = ((Guid)fund.GetAttributeValue<AliasedValue>("F.bpa_fundid").Value);
            }
            return fundId;
        }

        public static bool IsPensionFundExists(IOrganizationService service, ITracingService tracing, Guid trustId, int fundType, Guid fundId)
        {
            bool isExists = false;

            string fundIdCondition = string.Empty;

            if(fundId != Guid.Empty)
                fundIdCondition = $@"<condition attribute='bpa_fundid' operator='neq' value='{fundId}' />";
            
            string fetchXml = $@"
                <fetch mapping='logical' output-format='xml-platform' version='1.0' >
                  <entity name='bpa_fund' >
                    <filter>
                      <condition attribute='bpa_trust' operator='eq' value='{trustId}' />
                      <condition attribute='statecode' operator='eq' value='0' />
                      <condition attribute='bpa_fundtype' operator='eq' value='{fundType}' />
                      {fundIdCondition}
                    </filter>
                  </entity>
                </fetch>
            ";
            tracing.Trace($"Fetch Query\n {fetchXml}");

            EntityCollection funds = service.RetrieveMultiple(new FetchExpression(fetchXml));
            if (funds != null && funds.Entities.Count > 0)
            {
                tracing.Trace($"Fetch Query\n {funds.Entities.Count}");
                isExists = true;
            }
            return isExists;
        }


        public static EntityCollection FetchAllFundByAgreementIdAndFundId(IOrganizationService service, ITracingService tracing, Guid agreementId, bool isTaxable, Guid fundId)
        {
            string taxCondition = string.Empty;
            if (isTaxable)
                taxCondition = $@"<condition attribute='bpa_taxablefund' operator='eq' value='{fundId}' />";
            else
                taxCondition = $@"<condition attribute='bpa_fund' operator='eq' value='{fundId}' />";

            string fetchXml = $@"
            <fetch>
              <entity name='bpa_rate' >
                <attribute name='bpa_expirydate' />
                <attribute name='bpa_calculationtype' />
                <attribute name='bpa_effectivedate' />
                <attribute name='bpa_name' />
                <filter>
                  <condition attribute='statecode' operator='eq' value='0' />
                  <condition attribute='bpa_subsector' operator='eq' value='{agreementId}' />
                  {taxCondition}
                </filter>
              </entity>
            </fetch>";


            return service.RetrieveMultiple(new FetchExpression(fetchXml));
        }

        #region Helper Functions For Spelling out Dollar Amounts

        // Array of sting to hold the words from one to nineteen 
        private static string[] _arrayOfOnes = { string.Empty, "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight",
        "Nine", "Ten", "Eleven", "Twelve", "Thirteen", "Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eighteen",
        "Nineteen" };
        // Array of string to hold the words of tens - 10, 20,.., 90
        private static string[] _arrayOfTens = { string.Empty , "Ten", "Twenty", "Thirty", "Forty", "Fifty", "Sixty",
        "Seventy", "Eighty", "Ninety"};

        // Converts the decimal to currency
        public static string TranslateCurrency(decimal currencyValue)
        {
            string numericCurrency = currencyValue.ToString().Trim();
            //Check for the max capacity limit of the input 
            if (numericCurrency.Length > 18)
                return "Invalid input format";

            string leftValue, decimalWord;
            //Right align the charecters with padding of "0" to length the of 18 charecters
            if (numericCurrency.IndexOf(".") >= 0)
            {
                leftValue = numericCurrency.Substring(0, numericCurrency.IndexOf(".")).PadLeft(18, Convert.ToChar("0"));
                decimalWord = numericCurrency.Substring(numericCurrency.IndexOf(".") + 1).PadRight(2, Convert.ToChar("0"));
                decimalWord = (decimalWord.Length > 2 ? decimalWord.Substring(0, 2) : decimalWord);
            }
            else
            {
                leftValue = numericCurrency.PadLeft(18, Convert.ToChar("0"));
                decimalWord = "0";
            }

            string quadrillionWord, trillionWord, billionWord, millionWord, thousandWord, hundredWord;
            quadrillionWord = TranslateHundreds(Convert.ToInt32(leftValue.Substring(0, 3))); // One Quadrillion - Number of zeros 15
            trillionWord = TranslateHundreds(Convert.ToInt32(leftValue.Substring(3, 3)));    // One Trillion - Number of zeros 12
            billionWord = TranslateHundreds(Convert.ToInt32(leftValue.Substring(6, 3)));     // One Billion - Number of zeros 9
            millionWord = TranslateHundreds(Convert.ToInt32(leftValue.Substring(9, 3)));    // One Million - Number of zeros 6
            thousandWord = TranslateHundreds(Convert.ToInt32(leftValue.Substring(12, 3)));
            hundredWord = TranslateHundreds(Convert.ToInt32(leftValue.Substring(15, 3)));
            //Do not translate cents amount on cheque, show as number.
            //decimalWord = TranslateDecimal(decimalWord);

            // Start building the currency
            StringBuilder currencyInEnglish = new StringBuilder();
            currencyInEnglish.Append((quadrillionWord.Trim() == string.Empty ? string.Empty : quadrillionWord + " Quadrillion "));
            currencyInEnglish.Append((trillionWord.Trim() == string.Empty ? string.Empty : trillionWord + " Trillion "));
            currencyInEnglish.Append((billionWord.Trim() == string.Empty ? string.Empty : billionWord + " Billion "));
            currencyInEnglish.Append((millionWord.Trim() == string.Empty ? string.Empty : millionWord + " Million "));
            currencyInEnglish.Append((thousandWord.Trim() == string.Empty ? string.Empty : thousandWord + " Thousand "));
            currencyInEnglish.Append((hundredWord.Trim() == string.Empty ? string.Empty : hundredWord));

            currencyInEnglish.Append(currencyInEnglish.ToString() == string.Empty ? "Zero Dollars " : " Dollars");
            if (currencyInEnglish.ToString().StartsWith("One Dollars"))
            {
                currencyInEnglish.Replace("One Dollars", "One Dollar");
            }
            currencyInEnglish.Append((decimalWord == string.Empty ? " and 00 cents" : " and " + decimalWord + " Cents"));
            return currencyInEnglish.ToString();
        }

        private static string TranslateHundreds(int value)
        {
            string result;
            // If the value is less than hundred then convert it as tens 
            if (value <= 99)
            {
                result = (TranslateTens(value));
            }
            else
            {
                result = _arrayOfOnes[Convert.ToInt32(Math.Floor(Convert.ToDecimal(value / 100)))];
                // Math.Floor method is used to get the largest integer from the decial value
                result += " Hundred ";
                // The rest of the decimal is converted into tens
                if (value - Math.Floor(Convert.ToDecimal((value / 100) * 100)) == 0)
                {
                    result += string.Empty;
                }
                else
                {
                    result += string.Empty + TranslateTens(Convert.ToInt32(value - Math.Floor(Convert.ToDecimal((value / 100) * 100))));
                }
            }
            return result;
        }


        private static string TranslateTens(int value)
        {
            string result;
            // If the value is less than 20 then get the word directly from the array
            if (value < 20)
            {
                result = _arrayOfOnes[value];
                value = 0;
            }
            else
            {
                result = _arrayOfTens[(int)Math.Floor(Convert.ToDecimal(value / 10))];
                value -= Convert.ToInt32(Math.Floor(Convert.ToDecimal((value / 10) * 10)));
            }
            result = result + (_arrayOfOnes[value].Trim() == string.Empty ? string.Empty : "-" + _arrayOfOnes[value]);
            return result;
        }

        // Translates the decimal part of the currency to words
        private static string TranslateDecimal(string value)
        {
            string result = string.Empty;
            // Check whether the decimal part starts with a zero. Example : 12.05
            if (value != "0")
            {
                if (value.StartsWith("0"))
                {
                    result = "Zero ";
                    result += _arrayOfOnes[Convert.ToInt32(value.Substring(1, 1))];
                }
                else
                {
                    result = TranslateTens(Convert.ToInt32(value));
                }

            }
            return result;
        }

        #endregion


        public static EntityCollection FetchAllPlanByTrustId(IOrganizationService service, ITracingService tracingService, Guid trustId)
        {
            string fetchXml = $@"
            <fetch>
              <entity name='bpa_fund' >
                <all-attributes/>
                <filter>
                  <condition attribute='bpa_trust' operator='eq' value='{trustId}' />
                  <condition attribute='statecode' operator='eq' value='0' />
                </filter>
              </entity>
            </fetch>
            ";
            return service.RetrieveMultiple(new FetchExpression(fetchXml));
        }

        public static EntityCollection FetchAllPaymentTypesByFundId(IOrganizationService service, ITracingService tracingService, Guid fundId)
        {
            string fetchXml = $@"
            <fetch>
              <entity name='bpa_fundpaymenttype' >
                <all-attributes/>
                <filter>
                  <condition attribute='bpa_fundid' operator='eq' value='{fundId}' />
                  <condition attribute='statecode' operator='eq' value='0' />
                </filter>
              </entity>
            </fetch>
            ";
            return service.RetrieveMultiple(new FetchExpression(fetchXml));
        }


    }
}