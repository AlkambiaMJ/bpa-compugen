﻿using System;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;


namespace BPA.Benefit.Management.CRM.Plugins.Helper
{
    public static class AnnualVacationRequestHelper
    {
        public static EntityCollection FetchAllPendingVacationRequest(IOrganizationService service, ITracingService tracingService)
        {
            string fetchXml = @"
                <fetch version='1.0' >
                  <entity name='bpa_annualvacationrequest' >
                    <all-attributes/>
                    <filter>
                      <condition attribute='statecode' operator='eq' value='0' />
                      <condition attribute='statuscode' operator='eq' value='1' />
                    </filter>
                  </entity>
                </fetch>                
            ";

            tracingService.Trace(fetchXml);
            return service.RetrieveMultiple(new FetchExpression(fetchXml));
        }


        public static void UpdateStatus(IOrganizationService service, ITracingService tracingService, Guid id, int statusCode)
        {
            Entity annualVacation = new Entity(PluginHelper.BpaAnnualCacationRequest)
            {
                Id = id,
                ["statuscode"] = new OptionSetValue(statusCode) 
            };

            service.Update(annualVacation);
        }

    }
}
