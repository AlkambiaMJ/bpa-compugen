﻿#region

using System;
using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Query;

#endregion

// DONE - TODOJK: Add Comments and use the trace service

namespace BPA.Benefit.Management.CRM.Plugins.Helper
{
    //This class used speicaly when Submission goes From 'Pending' to 'Verification' to generate Contribution
    public class EmployeeSubmissionDetails
    {
        public Guid LabourRoleId { get; set; }
        public Guid SubmissionDetailId { get; set; }
        public int FlatRateRule { get; set; }
    }

    //This is  the main Helper class where all the method related to Submission Entity
    public static class SubmissionHelper
    {
        //public static string EntitySubmission = "bpa_submission";
        //public static string EntityContribution = "bpa_contribution";
        
        //Get Employer Openining Variance for submssion
        public static decimal GetOpeningVariance(IOrganizationService service, ITracingService tracingService,
            Guid employerId, DateTime createdon)
        {
            decimal openingVariance = 0;

            string fetchXml =
                $@"
                    <fetch aggregate='true' >
                      <entity name='bpa_contribution' >
                        <attribute name='bpa_contributionamount' alias='bpa_contributionamount' aggregate='sum' />
                        <attribute name='bpa_contributionpaid' alias='bpa_contributionpaid' aggregate='sum' />
                        <filter>
                          <condition attribute='bpa_contributionpaid' operator='not-null' />
                          <condition attribute='bpa_accountagreementid' operator='eq' value='{employerId}' />
                        </filter>
                      </entity>
                    </fetch>
                                    ";
            EntityCollection sums = service.RetrieveMultiple(new FetchExpression(fetchXml));
            if (sums != null && sums.Entities.Count > 0)
            {
                decimal bpaSubmissionamount = 0, bpaSubmissionpaid = 0; //, bpa_totalsubmissiondue = 0;

                Entity sum = sums.Entities[0];
                if (sum.Attributes.Contains("bpa_contributionamount") &&
                    (sum.GetAttributeValue<AliasedValue>("bpa_contributionamount").Value != null))
                    bpaSubmissionamount =
                        ((Money)sum.GetAttributeValue<AliasedValue>("bpa_contributionamount").Value).Value;
                if (sum.Attributes.Contains("bpa_contributionpaid") &&
                    (sum.GetAttributeValue<AliasedValue>("bpa_contributionpaid").Value != null))
                    bpaSubmissionpaid =
                        ((Money)sum.GetAttributeValue<AliasedValue>("bpa_contributionpaid").Value).Value;

                //if (sum.Attributes.Contains("bpa_totalsubmissiondue") && (sum.GetAttributeValue<AliasedValue>("bpa_totalsubmissiondue").Value != null))
                //    bpa_totalsubmissiondue = ((Money)(sum.GetAttributeValue<AliasedValue>("bpa_totalsubmissiondue").Value)).Value;

                //Get S
                decimal sumOfAdjustment = SubmissionAdjustmentHelper.GetSumOfSubmissionAdjustment(service,
                    tracingService, employerId, createdon);

                openingVariance = (bpaSubmissionamount - bpaSubmissionpaid) + sumOfAdjustment;
            }

            return openingVariance;
        }

        public static EntityCollection FetchAllSubmissionsByDepositId(IOrganizationService service, ITracingService tracingService, Guid depositId, bool isActive)
        {
            tracingService.Trace("Entered: FetchAllSubmissionsByDepositId");
            int statecode = 0;

            if (!isActive)
                statecode = 1;

            // Get all of the Submissions linked to a Deposit as an EntityCollection
            tracingService.Trace("Get all of the Submissions linked to a Deposit as an EntityCollection");
            string[] columset = { "bpa_submissionid", "bpa_submissionpaid", "bpa_hasdeposited", "bpa_submissiondate", "bpa_submissionstatus", "stageid", "processid", "statecode", "statuscode" };
            QueryExpression query = new QueryExpression
            {
                EntityName = "bpa_submission",
                ColumnSet = new ColumnSet(columset),
                Criteria =
                {
                    Filters =
                    {
                        new FilterExpression
                        {
                            FilterOperator = LogicalOperator.And,
                            Conditions =
                            {
                                new ConditionExpression("bpa_depositid", ConditionOperator.Equal, depositId),
                                new ConditionExpression("statecode", ConditionOperator.Equal, statecode)
                            }
                        }
                    }
                }
            };

            // Run the Query
            tracingService.Trace("Run the Query");
            EntityCollection collection = service.RetrieveMultiple(query);

            // Return the collection
            tracingService.Trace("Return the collection");
            return collection;
        }

        public static EntityCollection FetchAllEmployeesByEmployerId(IOrganizationService service,
            ITracingService tracingService, Guid accountId)
        {
            tracingService.Trace($"In side SubmisionHelper.FetchAllEmployeesByEmployerId ");

            EntityCollection employees = new EntityCollection();

            try
            {
                string fetchXml = @"
                <fetch mapping='logical' output-format='xml-platform' version='1.0' >
                  <entity name='bpa_employment' >
                    <attribute name='bpa_labourrole' />
                    <attribute name='bpa_memberplanid' />
                    <filter>
                      <condition attribute='statecode' operator='eq' value='0' />
                      <condition attribute='bpa_accountagreementid' operator='eq' value='" + accountId + @"' />
                      <condition attribute='bpa_workstatus' operator='eq' value='" + (int)EmployerWorkStatus.Employed + @"' />
                    </filter>
                    <link-entity name='bpa_memberplan' from='bpa_memberplanid' to='bpa_memberplanid'>
                      <attribute name='bpa_memberplanid' />
                      <attribute name='bpa_socialinsurancenumber' />
					  <link-entity name='contact' from='contactid' to='bpa_contactid'>
						<order attribute='lastname' />
						<order attribute='firstname' />
					  </link-entity>
                    </link-entity>
                  </entity>
                </fetch>
                ";
                tracingService.Trace($"Fetch xml \n {fetchXml}");

                FetchXmlToQueryExpressionRequest conversionRequest = new FetchXmlToQueryExpressionRequest
                {
                    FetchXml = fetchXml
                };

                FetchXmlToQueryExpressionResponse conversionResponse =
                    (FetchXmlToQueryExpressionResponse)service.Execute(conversionRequest);

                // Use the newly converted query expression to make a retrieve multiple
                // request to Microsoft Dynamics CRM.
                QueryExpression queryServicios = conversionResponse.Query;
                int pageNumber = 1;
                RetrieveMultipleResponse multiResponse = new RetrieveMultipleResponse();

                do
                {
                    queryServicios.PageInfo.Count = 500;
                    queryServicios.PageInfo.PagingCookie = pageNumber == 1
                        ? null
                        : multiResponse.EntityCollection.PagingCookie;
                    queryServicios.PageInfo.PageNumber = pageNumber++;

                    RetrieveMultipleRequest multiRequest = new RetrieveMultipleRequest {Query = queryServicios};
                    multiResponse = (RetrieveMultipleResponse)service.Execute(multiRequest);

                    employees.Entities.AddRange(multiResponse.EntityCollection.Entities);
                } while (multiResponse.EntityCollection.MoreRecords);
            }
            catch (Exception)
            {
                employees = null;
            }
            return employees;
        }

        public static Guid CreateSubmissionDetail(IOrganizationService service, ITracingService tracingService,
            Guid submissionId, EntityReference employer, Guid employmentId, Guid memberPlanDetail,
            EntityReference labourRole, DateTime workMonth, EntityReference agreement, int submissionType,
            bool isFlatRuleIgnore, Money selfpayAmount,string defaultCurrency, EntityReference owner)
        {
            Entity sd = new Entity("bpa_submissiondetail");
            if (submissionId != null && submissionId != Guid.Empty)
                sd["bpa_submissionid"] = new EntityReference(PluginHelper.BpaSubmission, submissionId);

            sd["bpa_accountagreementid"] = employer;

            if (employmentId != null && employmentId != Guid.Empty)
                sd["bpa_employmentid"] = new EntityReference(PluginHelper.BpaEmployment, employmentId);
            if (memberPlanDetail != Guid.Empty)
                sd["bpa_memberplanid"] = new EntityReference(PluginHelper.BpaMemberplan, memberPlanDetail);

            //if (MemberPlanDetail != Guid.Empty)
            //    sd["bpa_memberplandetailid"] = new EntityReference(PluginHelper.contact, MemberPlanDetail);
            //if (workMonth != DateTime.MinValue)
            //    sd["bpa_submissiondate"] = new DateTime(workMonth.Year, workMonth.Month, workMonth.Day);
            if (labourRole != null)
                sd["bpa_labourroleid"] = labourRole;
            if (agreement != null)
                sd["bpa_agreementid"] = agreement;
            if (submissionType != 0)
                sd["bpa_submissiontype"] = new OptionSetValue(submissionType);
            //string defaultCurrency = ConfigurationHelper.FetchValueId(service, PluginHelper.ConfigDefaultCurrency);
            if (defaultCurrency != Guid.Empty.ToString())
                sd["transactioncurrencyid"] = new EntityReference(PluginHelper.Transactioncurrency,
                    new Guid(defaultCurrency));

            if (isFlatRuleIgnore)
                sd["bpa_flatraterule"] = new OptionSetValue((int)SubmissionDetailFlatRateRule.Ignore);

            if (selfpayAmount != null && selfpayAmount.Value != 0)
                sd["bpa_dollars"] = selfpayAmount;

            if (owner != null)
                sd["ownerid"] = owner;

            return service.Create(sd);
        }



        public static Entity CreateSubmissionDetailPre(IOrganizationService service, ITracingService tracingService,
            Guid submissionId, EntityReference employer, Guid employmentId, EntityReference memberPlanDetail,
            EntityReference labourRole, DateTime workMonth, EntityReference agreement, int submissionType,
            bool isFlatRuleIgnore, Money selfpayAmount, string defaultCurrency, string sin, EntityReference owner)
        {
            Entity sd = new Entity("bpa_submissiondetail");
            if (submissionId != null && submissionId != Guid.Empty)
                sd["bpa_submissionid"] = new EntityReference(PluginHelper.BpaSubmission, submissionId);

            sd["bpa_accountagreementid"] = employer;

            if (employmentId != null && employmentId != Guid.Empty)
                sd["bpa_employmentid"] = new EntityReference(PluginHelper.BpaEmployment, employmentId);
            if (memberPlanDetail != null)
            {
                sd["bpa_memberplanid"] = memberPlanDetail;
                sd["bpa_name"] = memberPlanDetail.Name;

            }

            if (labourRole != null)
                sd["bpa_labourroleid"] = labourRole;
            if (agreement != null)
                sd["bpa_agreementid"] = agreement;
            if (submissionType != 0)
                sd["bpa_submissiontype"] = new OptionSetValue(submissionType);
            //string defaultCurrency = ConfigurationHelper.FetchValueId(service, PluginHelper.ConfigDefaultCurrency);
            if (defaultCurrency != Guid.Empty.ToString())
                sd["transactioncurrencyid"] = new EntityReference(PluginHelper.Transactioncurrency, new Guid(defaultCurrency));

            if (!string.IsNullOrEmpty(sin))
                sd["bpa_membersin"] = sin;

            sd["transactioncurrencyid"] = new EntityReference(PluginHelper.Transactioncurrency, new Guid(defaultCurrency));

            if (isFlatRuleIgnore)
                sd["bpa_flatraterule"] = new OptionSetValue((int)SubmissionDetailFlatRateRule.Ignore);

            if (selfpayAmount != null && selfpayAmount.Value != 0)
                sd["bpa_dollars"] = selfpayAmount;

            if (owner != null)
                sd["ownerid"] = owner;

            return sd;
        }


        public static int GetSubmissionCountForMonth(IOrganizationService service, ITracingService tracingService,
            Guid employerId, DateTime workmonth)
        {
            string fetchXml = @"
                <fetch mapping='logical' output-format='xml-platform' version='1.0' >
                    <entity name='bpa_submission' >
                    <filter>
                        <condition attribute='statecode' operator='eq' value='0' />
                        <condition attribute='bpa_accountagreementid' operator='eq' value='" + employerId + @"' />
                        <condition attribute='bpa_submissiondate' operator='ge' value='" + workmonth.ToShortDateString() + @"' />
                        <condition attribute='bpa_submissiondate' operator='gt' value='" + workmonth.ToShortDateString() + @"' />
                    </filter>
                    </entity>
                </fetch>            ";
            EntityCollection submissions = service.RetrieveMultiple(new FetchExpression(fetchXml));
            return submissions.Entities.Count;
        }

        public static Entity FetchSumOfAllSubmissionDetails(IOrganizationService service, ITracingService tracingService,
            Guid submissionId, Guid submissionDetailId, Guid labourRoleId, int SubmissionType)
        {
            Entity sumofall = null;
            string fetchXml = string.Empty;

            if ((SubmissionType == (int)SubmissionSubmissionType.MemberSelfPayRefund) || (SubmissionType == (int)SubmissionSubmissionType.Member))
            {
                fetchXml = $@"
                    <fetch version='1.0' aggregate='true' > 
                      <entity name='bpa_submissiondetail' >
                        <attribute name='bpa_dollars' alias='bpa_dollars' aggregate='sum' />
                        <attribute name='bpa_grosswages' alias='bpa_grosswages' aggregate='sum' />
                        <attribute name='bpa_totalhours' alias='bpa_totalhours' aggregate='sum' />
                        <attribute name='bpa_hoursearned' alias='bpa_hoursearned' aggregate='sum' />
                        <attribute name='bpa_pensioncredited' alias='bpa_pensioncredited' aggregate='sum' />
                        <attribute name='bpa_pensionmemberrequired' alias='bpa_pensionmemberrequired' aggregate='sum' />
                        <attribute name='bpa_pensionmembervoluntary' alias='bpa_pensionmembervoluntary' aggregate='sum' />

                        <filter>
                          <condition attribute='statecode' operator='eq' value='0' />
                          <condition attribute='bpa_submissionid' operator='eq' value='{submissionId}' />
                          <condition attribute='bpa_labourroleid' operator='eq' value='{labourRoleId}' />
                        </filter>
                      </entity>
                    </fetch>";
                /*
                 *  <link-entity name='bpa_employment' from='bpa_employmentid' to='bpa_employmentid' link-type='outer' >
                        <filter>
                            
                        </filter>
                    </link-entity>                        

                 */
            }
            else
            {
                fetchXml = $@"
                    <fetch version='1.0' aggregate='true' > 
                      <entity name='bpa_submissiondetail' >
                        <attribute name='bpa_dollars' alias='bpa_dollars' aggregate='sum' />
                        <attribute name='bpa_grosswages' alias='bpa_grosswages' aggregate='sum' />
                        <attribute name='bpa_totalhours' alias='bpa_totalhours' aggregate='sum' />
                        <attribute name='bpa_hoursearned' alias='bpa_hoursearned' aggregate='sum' />
                        <attribute name='bpa_pensioncredited' alias='bpa_pensioncredited' aggregate='sum' />
                        <attribute name='bpa_pensionmemberrequired' alias='bpa_pensionmemberrequired' aggregate='sum' />
                        <attribute name='bpa_pensionmembervoluntary' alias='bpa_pensionmembervoluntary' aggregate='sum' />
                        <filter>
                          <condition attribute='statecode' operator='eq' value='0' />
                          <condition attribute='bpa_submissionid' operator='eq' value='{submissionId}' />
                          <condition attribute='bpa_labourroleid' operator='eq' value='{labourRoleId}' />
                        </filter>
                      </entity>
                    </fetch>";
                /*
                *  <link-entity name='bpa_employment' from='bpa_employmentid' to='bpa_employmentid' link-type='inner' >
                       <filter>

                       </filter>
                   </link-entity>                        

                */
            }
            tracingService.Trace(fetchXml);
            //<condition attribute='bpa_submissiondetailid' operator='neq' value='" + SubmissionDetailId.ToString() + @"' />
            EntityCollection submissions = service.RetrieveMultiple(new FetchExpression(fetchXml));
            if (submissions != null && submissions.Entities.Count > 0)
            {
                sumofall = submissions.Entities[0];
            }

            return sumofall;
        }

        public static Entity FetchSubmissionTotal(IOrganizationService service, ITracingService tracingService,
            Guid submissionId)
        {
            tracingService.Trace("Inside SubmissionHelper.FetchSubmissionTotal");
            Entity sum = null;
            string fetchXml = $@"
                <fetch version='1.0' aggregate='true' >
                  <entity name='bpa_submissiondetail' >
                    <attribute name='bpa_dollars' alias='bpa_dollars' aggregate='sum' />
                    <attribute name='bpa_grosswages' alias='bpa_grosswages' aggregate='sum' />
                    <attribute name='bpa_totalhours' alias='bpa_totalhours' aggregate='sum' />
                    <attribute name='bpa_hoursearned' alias='bpa_hoursearned' aggregate='sum' />

                    <filter>
                      <condition attribute='bpa_submissionid' operator='eq' value='{submissionId}' />
                    </filter>
                  </entity>
                </fetch>
            ";
            tracingService.Trace($"Fetch Xml:\n {fetchXml}");

            EntityCollection submissions = service.RetrieveMultiple(new FetchExpression(fetchXml));
            if (submissions != null && submissions.Entities.Count > 0)
            {
                tracingService.Trace($"Found Record");

                sum = submissions.Entities[0];
            }

            return sum;
        }

        public static EntityCollection FetchAllSubmissionDetailBySubmissionId(IOrganizationService service,
            ITracingService tracingService, Guid submissionId, bool isActive, bool includeUnemployed)
        {
            EntityCollection submissiondetail = new EntityCollection();
            int statecode = 0;

            if (!isActive)
                statecode = 1;

            string condition = "<condition attribute='bpa_unemployed' operator='eq' value='0' />";
            if (includeUnemployed)
                condition = string.Empty;

            string fetchXml = $@"
                <fetch version='1.0'>
                  <entity name='bpa_submissiondetail' >
                    <attribute name='bpa_submissionid' />
                    <attribute name='bpa_accountagreementid' />
                    <attribute name='bpa_employmentid' />
                    <attribute name='bpa_memberplanid'/>
                    <attribute name='bpa_totalhours' />
                    <attribute name='bpa_hoursearned' />
                    <attribute name='bpa_grosswages' />
                    <attribute name='bpa_dollars' />
                    <attribute name='bpa_icihours' />
                    <attribute name='bpa_nonicihours' />
                    <attribute name='bpa_labourroleid' />
                    <attribute name='bpa_flatraterule' />
                    <attribute name='bpa_agreementid' />
                    <attribute name='bpa_pensioncredited' />
                    <attribute name='bpa_pensionmemberrequired' />
                    <attribute name='bpa_pensionmembervoluntary' />
                    <attribute name='ownerid' />
                    <filter>
                      <condition attribute='statecode' operator='eq' value='{statecode}' />
                      <condition attribute='bpa_submissionid' operator='eq' value='{submissionId}' />
                      {condition}
                    </filter>
                    <link-entity name='bpa_memberplan' from='bpa_memberplanid' to='bpa_memberplanid' >
                      <attribute name='bpa_eligibilitycategoryid' />
                      <attribute name='bpa_unionhomelocalid' />
                    </link-entity>
                  </entity>
                </fetch>
            ";
            tracingService.Trace(fetchXml);
            var conversionRequest = new FetchXmlToQueryExpressionRequest
            {
                FetchXml = fetchXml
            };

            var conversionResponse = (FetchXmlToQueryExpressionResponse)service.Execute(conversionRequest);

            // Use the newly converted query expression to make a retrieve multiple
            // request to Microsoft Dynamics CRM.
            QueryExpression queryServicios = conversionResponse.Query;
            int pageNumber = 1;
            RetrieveMultipleRequest multiRequest;
            RetrieveMultipleResponse multiResponse = new RetrieveMultipleResponse();

            do
            {
                queryServicios.PageInfo.Count = 1500;
                queryServicios.PageInfo.PagingCookie = pageNumber == 1
                    ? null
                    : multiResponse.EntityCollection.PagingCookie;
                queryServicios.PageInfo.PageNumber = pageNumber++;

                multiRequest = new RetrieveMultipleRequest();
                multiRequest.Query = queryServicios;
                multiResponse = (RetrieveMultipleResponse)service.Execute(multiRequest);

                submissiondetail.Entities.AddRange(multiResponse.EntityCollection.Entities);
            } while (multiResponse.EntityCollection.MoreRecords);

            return submissiondetail;
        }

        public static Entity FetchSubmissionDetail(IOrganizationService service, ITracingService tracingService,
            Guid submissionId)
        {
            return service.Retrieve(PluginHelper.BpaSubmission, submissionId, new ColumnSet(true));
        }


        public static Entity FetchSubmissionOwner(IOrganizationService service, ITracingService tracingService,
            Guid submissionId)
        {
            return service.Retrieve(PluginHelper.BpaSubmission, submissionId, new ColumnSet("ownerid"));
        }
        public static bool IsSubmissionExistsForward(IOrganizationService service, ITracingService tracingService,
            Guid employerId, DateTime createdOn)
        {
            tracingService.Trace("Inside SubmissionHelper.IsSubmissionExistsForward Method");
            bool isExists = false;
            string fetchXml = @"
                <fetch>
                  <entity name='bpa_submission' >
                    <attribute name='bpa_name' />
                    <filter>
                      <condition attribute='bpa_submissionstatus' operator='in' >
                        <value>922070001</value>
                        <value>922070002</value>
                        <value>922070003</value>
                      </condition>
                      <condition attribute='bpa_accountagreementid' operator='eq' value='" + employerId + @"' />
                      <condition attribute='createdon' operator='gt' value='" + createdOn + @"' />
                      <condition attribute='statuscode' operator='neq' value='922070002' />
                    </filter>
                  </entity>
                </fetch>
            ";
            tracingService.Trace(fetchXml);
            EntityCollection transactions = service.RetrieveMultiple(new FetchExpression(fetchXml));
            if (transactions != null && transactions.Entities.Count > 0)
            {
                tracingService.Trace("Future Record found");
                isExists = true;
            }

            return isExists;
        }

        public static bool IsSubmissionExistsBackward(IOrganizationService service, ITracingService tracingService,
            Guid employerId, DateTime createdOn)
        {
            tracingService.Trace("Inside SubmissionHelper.IsSubmissionExistsBackward Method");

            bool isExists = false;
            string fetchXml = @"
                <fetch>
                  <entity name='bpa_submission' >
                    <attribute name='bpa_name' />
                    <filter>
                      <condition attribute='bpa_submissionstatus' operator='in' >
                        <value>922070001</value>
                        <value>922070000</value>
                      </condition>
                      <condition attribute='bpa_accountagreementid' operator='eq' value='" + employerId + @"' />
                      <condition attribute='createdon' operator='lt' value='" + createdOn + @"' />
                      <condition attribute='statuscode' operator='neq' value='922070002' />
                    </filter>
                  </entity>
                </fetch>
            ";
            tracingService.Trace(fetchXml);
            EntityCollection transactions = service.RetrieveMultiple(new FetchExpression(fetchXml));
            if (transactions != null && transactions.Entities.Count > 0)
            {
                tracingService.Trace("BAckward Record found");
                isExists = true;
            }

            return isExists;
        }

        public static bool IsSubmissionExistsForMonth(IOrganizationService service, ITracingService tracingService,
            Guid employerId, DateTime workMonth)
        {
            tracingService.Trace("Inside SubmissionHelper.IsSubmissionExistsForMonth");

            DateTime firstOfMonth = new DateTime(workMonth.Year, workMonth.Month, 1);
            DateTime firstOfNextMonth = new DateTime(workMonth.Year, workMonth.Month, 1).AddMonths(1);
            ;
            DateTime lastOfMonth = firstOfNextMonth.AddDays(-1);

            bool isExists = false;
            string fetchXml = @"
                <fetch>
                  <entity name='bpa_submission' >
                    <attribute name='bpa_name' />
                    <filter>
                      <condition attribute='statuscode' operator='eq' value='2' />
                      <condition attribute='bpa_accountagreementid' operator='eq' value='" + employerId + @"' />
                      <condition attribute='bpa_submissiondate' operator='ge' value='" +
                              firstOfMonth.ToShortDateString() + @"' />
                      <condition attribute='bpa_submissiondate' operator='le' value='" + lastOfMonth.ToShortDateString() +
                              @"' />
                    </filter>
                  </entity>
                </fetch>
            ";
            tracingService.Trace($"fetch Xml\n {fetchXml}");
            EntityCollection submissions = service.RetrieveMultiple(new FetchExpression(fetchXml));
            if (submissions != null && submissions.Entities.Count > 0)
            {
                tracingService.Trace("Submission Exists Record found");
                isExists = true;
            }

            return isExists;
        }


        //Execute query to get the correct Delinquency Rule record for this submission
        //We need to filter on  submission type of "Regular" only for delinquincy

        public static Entity GetDelinquencyRuleBySubmissionId(IOrganizationService service,
            ITracingService tracingService, Guid submissionId)
        {
            tracingService.Trace("Executing GetDelinquencyRuleBySubmissionId()");

            /*
               <filter>
                                      <condition attribute='bpa_startdate' operator='le' value='01/01/2014' />
                                      <condition attribute='bpa_enddate' operator='ge' value='01/01/2014' />
                                    </filter>
             */
            string fetchXml = $@"<fetch>
                                  <entity name='bpa_delinquencyrule' >
                                    <attribute name='bpa_period1interestrate' />
                                    <attribute name='bpa_remittancedays' />
                                    <attribute name='bpa_period2interestapplicableon' />
                                    <attribute name='bpa_period1interestapplicableon' />
                                    <attribute name='bpa_period2interestrate' />
                                    <attribute name='bpa_initialpenaltyinterestrate' />
                                    <attribute name='bpa_period1grace' />
                                    <link-entity name='bpa_subsector' from='bpa_subsectorid' to='bpa_agreementid' link-type='inner' alias='A' >
                                      <link-entity name='bpa_accountagreement' from='bpa_agreementid' to='bpa_subsectorid' link-type='inner' alias='E' >
                                        <link-entity name='bpa_submission' from='bpa_accountagreementid' to='bpa_accountagreementid' link-type='inner' alias='S' >
                                          <filter>
                                            <condition attribute='bpa_submissionid' operator='eq' value='{submissionId.ToString()}' />
                                            <condition attribute='bpa_submissiontype' operator='eq' value='922070000' />
                                          </filter>
                                        </link-entity>
                                      </link-entity>
                                    </link-entity>
                                  </entity>
                                </fetch>";

            EntityCollection submissions = service.RetrieveMultiple(new FetchExpression(fetchXml));
            if (submissions != null && submissions.Entities.Count > 0)
                return submissions.Entities[0];
            else
                return null;

        }

        public static bool IsActiveSubmissionForAccount(IOrganizationService service, ITracingService tracingService, Guid accountId)
        {
            string fetchXml = $@"<fetch>
                                  <entity name='bpa_submission' >
                                    <attribute name='bpa_name' />
                                        <filter>
                                        <condition attribute='bpa_accountagreementid' operator='eq' value='{accountId.ToString()}' />
                                        <condition attribute='statecode' operator='eq' value='0' />
                                        </filter>
                                  </entity>
                                </fetch>";

            EntityCollection submissions = service.RetrieveMultiple(new FetchExpression(fetchXml));
            if (submissions != null && submissions.Entities.Count > 0)
                return true;
            else
                return false;
        }

        public static void DeactivateSubmission(IOrganizationService service, ITracingService tracingService, Guid submissionId)
        {
            var cols = new ColumnSet("statecode", "statuscode");

            //Check if it is Active or not
            var _entity = service.Retrieve(PluginHelper.BpaSubmission, submissionId, cols);

            if (_entity != null && _entity.GetAttributeValue<OptionSetValue>("statecode").Value == 0)
            {
                tracingService.Trace("starting Set State");
                SetStateRequest setStateRequest = new SetStateRequest();
                setStateRequest.EntityMoniker = new EntityReference(PluginHelper.BpaSubmission, submissionId);
                setStateRequest.State = new OptionSetValue(0); // Inactive state
                setStateRequest.Status = new OptionSetValue(922070003);

                // Complete status //922,070,003 - Processing
                service.Execute(setStateRequest);
                tracingService.Trace("ending Set State - Processing");

                tracingService.Trace("starting Set State - Completed");
                SetStateRequest setStateRequest1 = new SetStateRequest();
                setStateRequest1.EntityMoniker = new EntityReference(PluginHelper.BpaSubmission, submissionId);
                setStateRequest1.State = new OptionSetValue(1); // Inactive state
                setStateRequest1.Status = new OptionSetValue(2); // Complete status 
                service.Execute(setStateRequest1);
                tracingService.Trace("ending Set State - Completed");
            }
        }


        public static EntityCollection FetchAllSubmissionsByDepositId(IOrganizationService service, ITracingService tracingService, Guid depositId)
        {
            tracingService.Trace("Entered: FetchAllSubmissionsByDepositId with 3 parameters");
            
            // Get all of the Submissions linked to a Deposit as an EntityCollection
            tracingService.Trace("Get all of the Submissions linked to a Deposit as an EntityCollection");
            string[] columset = {"bpa_submissionid", "bpa_submissionpaid", "bpa_hasdeposited", "bpa_submissiondate"};
            QueryExpression query = new QueryExpression
            {
                EntityName = "bpa_submission",
                ColumnSet = new ColumnSet(columset),
                Criteria = {
                    Filters = {
                        new FilterExpression {
                            FilterOperator = LogicalOperator.And,
                            Conditions = {
                                new ConditionExpression("bpa_depositid", ConditionOperator.Equal, depositId),
                                new ConditionExpression("statecode", ConditionOperator.Equal, 0)
                            }
                        }
                    }
                }
            };

            // Run the Query
            tracingService.Trace("Run the Query");
            EntityCollection collection = service.RetrieveMultiple(query);

            // Return the collection
            tracingService.Trace("Return the collection");
            return collection;
        }


    }
}