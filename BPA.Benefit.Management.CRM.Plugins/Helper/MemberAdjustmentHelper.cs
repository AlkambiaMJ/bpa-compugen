﻿#region

using System;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using System.Collections.Generic;

#endregion


namespace BPA.Benefit.Management.CRM.Plugins.Helper
{
    public class MemberAdjustmentHelper
    {
        public static Guid CrateMemberAdjustmentTypeTransferINOUT(IOrganizationService service, ITracingService tracingService, EntityReference memberPlan, EntityReference plan,
            int adjustmentType, string description, EntityReference homeLocal, DateTime workMonth, int transferAmountPaid, Dictionary<string, decimal> endingBalances,string defaultCurrency)
        {
            tracingService.Trace("Inside MemberAdjustmentHelper.CrateMemberAdjustmentTypeTransferINOUT Method");
            Guid adjustmentId = Guid.Empty;
            Entity adjustment = new Entity(PluginHelper.BpaMemberPlanadjustment)
            {
                ["bpa_memberplanid"] = memberPlan,
                ["bpa_pensionadjustmenttype"] = null,
                ["bpa_adjustmentcategory"] = new OptionSetValue((int)MemberPlanAdjustmentCategory.BenefitAdjustment),
                ["bpa_benefitadjustmenttype"] = new OptionSetValue(adjustmentType),
                ["bpa_description"] = description,
                ["bpa_homelocalid"] = homeLocal,
                ["bpa_workmonth"] = workMonth
            };

            //Fetch Default Currency
            //string defaultCurrency = ConfigurationHelper.FetchValueId(service, PluginHelper.ConfigDefaultCurrency);
            if (defaultCurrency != Guid.Empty.ToString())
                adjustment["transactioncurrencyid"] = new EntityReference("transactioncurrency", new Guid(defaultCurrency));
            if (transferAmountPaid != 0)
                adjustment["bpa_transferreciprocalamount"] = new OptionSetValue(transferAmountPaid);

            if (endingBalances.ContainsKey("hours"))
                adjustment["bpa_reciprocalhours"] = endingBalances["hours"];

            decimal totalDollar = 0;
            if (endingBalances.ContainsKey("dollarhours"))
                totalDollar += endingBalances["dollarhours"];
            if (endingBalances.ContainsKey("secondarydollar"))
                totalDollar += endingBalances["secondarydollar"];
            
            adjustment["bpa_reciprocaldollars"] = new Money(totalDollar);

            if (plan == null)
                plan = ContactHelper.GetMemberPlan(service, tracingService, memberPlan.Id);
            adjustment["bpa_planid"] = plan;


            adjustmentId = service.Create(adjustment);

            tracingService.Trace($@"Member Adjustment ID = '{adjustmentId.ToString()}' ");
            return adjustmentId;
        }


        public static EntityCollection FetchAllUnPaidTransferOutAdjustments(IOrganizationService service, ITracingService tracingService, Guid currentAdjustmentId, Guid memberPlanId, DateTime workMonth )
        {
            string fetchXml = $@"
                <fetch>
                  <entity name='bpa_memberplanadjustment' >
                    <attribute name='bpa_name' />
                    <attribute name='bpa_transferreciprocalamount' />
                    <filter>
                      <condition attribute='bpa_benefitadjustmenttype' operator='eq' value='{(int)MemberPlanBenefitAdjustmentType.TransferReciprocalOut}' />
                      <condition attribute='bpa_memberplanid' operator='eq' value='{memberPlanId.ToString()}' />
                      <condition attribute='bpa_workmonth' operator='ge' value='{workMonth.ToShortDateString()}' />
                      <condition attribute='statecode' operator='eq' value='0' />
                      <condition attribute='bpa_memberplanadjustmentid' operator='neq' value='{currentAdjustmentId.ToString()}' />
                    </filter>
                  </entity>
                </fetch>
            ";
            return service.RetrieveMultiple(new FetchExpression(fetchXml));
        }
        
    }
}
