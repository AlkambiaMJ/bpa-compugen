﻿#region

using System;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;

#endregion

namespace BPA.Benefit.Management.CRM.Plugins.Helper
{
    public class EmploymentHelper
    {
        public static Entity IsEmploymentExists(IOrganizationService service, Guid memberPlanId, Guid accountId)
        {
            Entity employment = null;
            string fetchXml = @"
                <fetch mapping='logical' version='1.0' >
                  <entity name='bpa_employment' >
                    <attribute name='bpa_name' />
                    <attribute name='bpa_labourrole' />
                    <filter>
                      <condition attribute='bpa_memberplanid' operator='eq' value='" + memberPlanId + @"' />
                      <condition attribute='bpa_accountagreementid' operator='eq' value='" + accountId + @"' />
                      <condition attribute='statecode' operator='eq' value='0' />
                    </filter>
                    <order attribute='createdon' descending='true' />
                  </entity>
                </fetch>
            ";
            EntityCollection employments = service.RetrieveMultiple(new FetchExpression(fetchXml));
            if (employments != null && employments.Entities.Count > 0)
            {
                employment = employments.Entities[0];
            }
            return employment;
        }

        public static Guid CreateEmployment(IOrganizationService service, EntityReference employer,
            EntityReference employee, DateTime startDate, EntityReference labourRole, OptionSetValue workStatus, EntityReference agreement, EntityReference plan)
        {
            // Create Employment Record
            Entity employment = new Entity(PluginHelper.BpaEmployment);
            employment["bpa_accountagreementid"] = employer;
            employment["bpa_memberplanid"] = employee;
            employment["bpa_startdate"] = new DateTime(startDate.Year, startDate.Month, startDate.Day);
            employment["bpa_labourrole"] = labourRole;
            employment["bpa_workstatus"] = workStatus;
            if(agreement != null)
                employment["bpa_agreementid"] = agreement;
            if (plan != null)
                employment["bpa_planid"] = plan;
            return service.Create(employment);
        }

        public static bool HasEmploymentRecordAssociated(IOrganizationService service, ITracingService tracingService,
            Guid labourRoleId)
        {
            bool isFound = false;
            string fetchXml = @"
                <fetch mapping='logical' version='1.0' >
                  <entity name='bpa_employment' >
                    <attribute name='bpa_name' />
                    <filter>
                      <condition attribute='bpa_labourrole' operator='eq' value='" + labourRoleId + @"' />
                      <condition attribute='statecode' operator='eq' value='0' />
                    </filter>
                    <order attribute='createdon' descending='true' />
                  </entity>
                </fetch>
            ";
            tracingService.Trace("After Query: \n" + fetchXml);
            EntityCollection employments = service.RetrieveMultiple(new FetchExpression(fetchXml));
            if (employments != null && employments.Entities.Count > 0)
            {
                tracingService.Trace("Record Found: " + employments.Entities.Count);
                isFound = true;
            }

            return isFound;
        }

        public static Entity FetchDetail(IOrganizationService service, ITracingService tracingService, Guid employmentId)
        {
            tracingService.Trace("In EmploymentHelper.FetchDetail MEthod");
            return service.Retrieve(PluginHelper.BpaEmployment, employmentId, new ColumnSet(true));
        }


        public static EntityCollection FetchAllEmploymentRecords(IOrganizationService service, ITracingService tracingService, Guid memberPlanId)
        {
            string fetchXml = @"
                <fetch mapping='logical' version='1.0' >
                  <entity name='bpa_employment' >
                    <attribute name='bpa_name' />
                    <attribute name='bpa_labourrole' />
                    <filter>
                      <condition attribute='bpa_memberplanid' operator='eq' value='" + memberPlanId + @"' />
                    </filter>
                    <order attribute='createdon' descending='true' />
                  </entity>
                </fetch>
            ";
            return service.RetrieveMultiple(new FetchExpression(fetchXml));
            
        }
    }
}