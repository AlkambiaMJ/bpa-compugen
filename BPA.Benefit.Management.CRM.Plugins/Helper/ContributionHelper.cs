﻿#region

using System;
using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Query;

#endregion

namespace BPA.Benefit.Management.CRM.Plugins.Helper
{
    public static class ContributionHelper
    {
        public static EntityCollection FetchAllContributions(IOrganizationService service, ITracingService tracing,
            Guid submissionId, bool hasNsf)
        {
            tracing.Trace("Inside ContributionHelper.FetchAllContributions Method");

            //Get All Contributions for related Submission
            QueryExpression queryExpression = new QueryExpression
            {
                EntityName = "bpa_contribution",
                ColumnSet = new ColumnSet(true),
                Criteria =
                {
                    Filters =
                    {
                        new FilterExpression
                        {
                            FilterOperator = LogicalOperator.And,
                            Conditions =
                            {
                                new ConditionExpression("bpa_submission", ConditionOperator.Equal, submissionId)
                            }
                        }
                    }
                }
            };

            if (hasNsf)
            {
                tracing.Trace("Inside if HasNSF");

                queryExpression.Criteria.AddCondition(new ConditionExpression("bpa_contributiontype",
                    ConditionOperator.Equal, 922070005));
            }
            EntityCollection contributions = service.RetrieveMultiple(queryExpression);
            tracing.Trace("After Retrive Multiple ");

            return contributions;
        }

        public static void UpdateContribution(IOrganizationService service, Guid contributionId,
            decimal contributionPaid)
        {
            Entity updateContribution = new Entity("bpa_contribution");
            updateContribution["bpa_contributionpaid"] = new Money(contributionPaid);
            updateContribution.Id = contributionId;
            service.Update(updateContribution);
        }

        public static Entity FetchContributionBySubmissionFundIdAndRate(IOrganizationService service, Guid submissionId,
            Guid fundId, decimal rate, bool isFromTaxable)
        {
            Entity exists = null;
            if (submissionId != null && submissionId != Guid.Empty)
            {
                string fetchXml = string.Empty;

                if (isFromTaxable)
                {
                    fetchXml = string.Format(@"
                        <fetch mapping='logical' version='1.0'>
	                        <entity name='bpa_contribution'>
                                <attribute name='bpa_name' />
                                <attribute name='createdon' />
                                <attribute name='bpa_fund' />
                                <attribute name='bpa_contributiontype' />
                                <attribute name='bpa_contributionpaid' />
                                <attribute name='bpa_contributionamount' />
                                <attribute name='bpa_ratevalue' />
                                <attribute name='bpa_calculationtype' />
                                <attribute name='bpa_dollarshourscount' />
                                <order attribute='bpa_name' descending='false' />
                                <filter type='and'>
                                    <condition attribute='bpa_submission' operator='eq' value='{0}' />
                                    <condition attribute='bpa_fund' operator='eq' value='{1}' />
                                </filter>
	                        </entity>
                        </fetch>", submissionId, fundId);
                }
                else
                {
                    fetchXml = string.Format(@"
                        <fetch mapping='logical' version='1.0'>
	                        <entity name='bpa_contribution'>
                                <attribute name='bpa_name' />
                                <attribute name='createdon' />
                                <attribute name='bpa_fund' />
                                <attribute name='bpa_contributiontype' />
                                <attribute name='bpa_contributionpaid' />
                                <attribute name='bpa_contributionamount' />
                                <attribute name='bpa_ratevalue' />
                                <attribute name='bpa_calculationtype' />
                                <attribute name='bpa_dollarshourscount' />
                                <order attribute='bpa_name' descending='false' />
                                <filter type='and'>
                                    <condition attribute='bpa_submission' operator='eq' value='{0}' />
                                    <condition attribute='bpa_ratevalue' operator='eq' value='{1}' />
                                    <condition attribute='bpa_fund' operator='eq' value='{2}' />
                                </filter>
	                        </entity>
                        </fetch>", submissionId, rate, fundId);
                }
                EntityCollection contributions = service.RetrieveMultiple(new FetchExpression(fetchXml));
                if (contributions != null && contributions.Entities.Count > 0)
                {
                    exists = contributions.Entities[0];
                }
            }
            return exists;
        }

        public static decimal GetContributionsAmountByFundIdLabourRoleId(IOrganizationService service, Guid fundId,
            Guid submissionId, Guid labourRoleId)
        {
            decimal totalamount = 0;
            string fetchXml = string.Format(@"
                    <fetch aggregate='true' >
                        <entity name='bpa_contribution' >
                        <attribute name='bpa_contributionamount' alias='bpa_contributionamount' aggregate='sum' />
                        <filter>
                            <condition attribute='statecode' operator='eq' value='0' />
                            <condition attribute='bpa_fund' operator='eq' value='{0}' />
                            <condition attribute='bpa_submission' operator='eq' value='{1}' />
                            <condition attribute='bpa_labourrole' operator='eq' value='{2}' />
                        </filter>
                        </entity>
                    </fetch>", fundId, submissionId, labourRoleId);

            EntityCollection contributions = service.RetrieveMultiple(new FetchExpression(fetchXml));
            if (contributions.Entities.Count > 0)
            {
                Entity contribution = contributions.Entities[0];

                if (contribution.Attributes.Contains("bpa_contributionamount") &&
                    contribution.GetAttributeValue<AliasedValue>("bpa_contributionamount").Value != null)
                {
                    totalamount =
                        ((Money)contribution.GetAttributeValue<AliasedValue>("bpa_contributionamount").Value).Value;
                }
            }
            return totalamount;
        }

        public static Guid CreateContribution(IOrganizationService service, Guid accountId, Guid submissionId,
            DateTime workMonth, Guid fundId, int contributionType, int calculationType, decimal rateValue,
            decimal amount,
            Guid rateId, Guid labourRoleId, decimal dollarsHoursCount, EntityReference owner)
        {
            Guid contributionId = Guid.Empty;
            Entity contribution = new Entity(PluginHelper.BpaContribution);
            contribution["bpa_submission"] = new EntityReference(PluginHelper.BpaSubmission, submissionId);
            contribution["bpa_accountagreementid"] = new EntityReference(PluginHelper.AccountAgreement, accountId);
            contribution["bpa_contributiondate"] = workMonth;
            if (fundId != Guid.Empty)
                contribution["bpa_fund"] = new EntityReference(PluginHelper.BpaFund, fundId);
            if (contributionType != 0)
                contribution["bpa_contributiontype"] = new OptionSetValue(contributionType);
            contribution["bpa_calculationtype"] = new OptionSetValue(calculationType);
            if (rateValue != 0)
                contribution["bpa_ratevalue"] = rateValue;
            contribution["bpa_contributionamount"] = new Money(amount);
            if (rateId != Guid.Empty)
                contribution["bpa_rate"] = new EntityReference(PluginHelper.BpaRate, rateId);
            contribution["bpa_labourrole"] = new EntityReference(PluginHelper.BpaLabourrole, labourRoleId);
            contribution["bpa_dollarshourscount"] = dollarsHoursCount;

            contribution["bpa_name"] = Guid.NewGuid().ToString();
            if(owner != null)
                contribution["ownerid"] = owner;

            contributionId = service.Create(contribution);
            return contributionId;
        }

        public static Guid CreateContributionWithPayment(IOrganizationService service, ITracingService tracingService,
            Guid accountId, DateTime workMonth, Guid fundId, int contributionType, decimal amount)
        {
            Guid contributionId = Guid.Empty;
            Entity contribution = new Entity(PluginHelper.BpaContribution);
            contribution["bpa_accountagreementid"] = new EntityReference(PluginHelper.AccountAgreement, accountId);
            contribution["bpa_contributiondate"] = workMonth;
            if (fundId != Guid.Empty)
                contribution["bpa_fund"] = new EntityReference(PluginHelper.BpaFund, fundId);
            if (contributionType != 0)
                contribution["bpa_contributiontype"] = new OptionSetValue(contributionType);
            contribution["bpa_contributionamount"] = new Money(0);
            contribution["bpa_contributionpaid"] = amount < 0 ? new Money(amount*-1) : new Money(amount);
            contribution["bpa_name"] = Guid.NewGuid().ToString();

            contributionId = service.Create(contribution);
            return contributionId;
        }

        public static void UpdateContribution(IOrganizationService service, Guid contributionId, Guid submissionId,
            decimal amount, decimal dollarsHoursCount)
        {
            Entity contribution = new Entity(PluginHelper.BpaContribution);
            contribution.Id = contributionId;
            contribution["bpa_submission"] = new EntityReference("bpa_submission", submissionId);

            //contribution["bpa_contributiondate"] = WorkMonth;
            contribution["bpa_contributionamount"] = new Money(amount);
            contribution["bpa_dollarshourscount"] = dollarsHoursCount;
            service.Update(contribution);
        }

        public static Money FetchContributionTotalBySubmissionId(IOrganizationService service, Guid submissionId)
        {
            Money total = new Money(0);
            string fetchXml = @"
                <fetch version='1.0' aggregate='true' >
                  <entity name='bpa_contribution' >
                    <attribute name='bpa_contributionamount' alias='bpa_contributionamount' aggregate='sum' />
                    <filter>
                      <condition attribute='statecode' operator='eq' value='0' />
                      <condition attribute='bpa_submission' operator='eq' value='" + submissionId + @"' />
                    </filter>
                  </entity>
                </fetch>
            ";
            EntityCollection contributions = service.RetrieveMultiple(new FetchExpression(fetchXml));
            if (contributions != null && contributions.Entities.Count > 0)
            {
                Entity sumResult = contributions.Entities[0];
                if (sumResult.Attributes.Contains("bpa_contributionamount") &&
                    ((AliasedValue)sumResult.Attributes["bpa_contributionamount"]).Value != null)
                    total = (Money)((AliasedValue)sumResult.Attributes["bpa_contributionamount"]).Value;
            }
            return total;
        }

        public static EntityCollection FetchAllContributionsBySubmissionId(IOrganizationService service, Guid submissionId)
        {
            EntityCollection transactions = new EntityCollection();

            string fetchXml = @"
                <fetch version='1.0'>
                  <entity name='bpa_contribution' >
                    <attribute name='bpa_name' />
                    <filter>
                      <condition attribute='statecode' operator='eq' value='0' />
                      <condition attribute='bpa_submission' operator='eq' value='" + submissionId + @"' />
                    </filter>
                  </entity>
                </fetch>
            ";

            //EntityCollection contrubutions = service.RetrieveMultiple(new FetchExpression(fetchXml));

            var conversionRequest = new FetchXmlToQueryExpressionRequest
            {
                FetchXml = fetchXml
            };

            var conversionResponse = (FetchXmlToQueryExpressionResponse)service.Execute(conversionRequest);

            // Use the newly converted query expression to make a retrieve multiple
            // request to Microsoft Dynamics CRM.
            QueryExpression queryServicios = conversionResponse.Query;
            int pageNumber = 1;
            RetrieveMultipleRequest multiRequest;
            RetrieveMultipleResponse multiResponse = new RetrieveMultipleResponse();

            do
            {
                queryServicios.PageInfo.Count = 1500;
                queryServicios.PageInfo.PagingCookie = pageNumber == 1
                    ? null
                    : multiResponse.EntityCollection.PagingCookie;
                queryServicios.PageInfo.PageNumber = pageNumber++;

                multiRequest = new RetrieveMultipleRequest();
                multiRequest.Query = queryServicios;
                multiResponse = (RetrieveMultipleResponse)service.Execute(multiRequest);

                transactions.Entities.AddRange(multiResponse.EntityCollection.Entities);
            } while (multiResponse.EntityCollection.MoreRecords);

            return transactions;
        }
    }
}