﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;

namespace BPA.Benefit.Management.CRM.Plugins.Helper
{
    public static class InvoiceHelper
    {
        // TODO: Class can be deleted

    //    public static EntityCollection FetchInvoiceDetails(IOrganizationService service, Guid invoiceid)
    //    {
    //        //Get All Contributions for related Submission
    //        QueryExpression queryExpression = new QueryExpression()
    //        {
    //            EntityName = "invoicedetail",
    //            ColumnSet = new ColumnSet(true),
    //            Criteria =
    //            {
    //                Filters =
    //                    {
    //                        new FilterExpression
    //                        {
    //                            FilterOperator = LogicalOperator.And,
    //                            Conditions =
    //                            {
    //                                new ConditionExpression("invoiceid", ConditionOperator.Equal, invoiceid),
    //                            }
    //                        }
    //                    }
    //            }
    //        };

    //        EntityCollection invoicedetails = service.RetrieveMultiple(queryExpression);
    //        return invoicedetails;
    //    }


    //    public static Dictionary<String, decimal> FetchEndingBalance(IOrganizationService service, Guid ContactId, DateTime BenefitMonth)
    //    {
    //        Dictionary<String, decimal> endingbalance = new Dictionary<String, decimal>();
    //        string fetchXml = @"
    //        <fetch aggregate='true' >
    //            <entity name='bpa_transaction' >
    //            <attribute name='bpa_hours' alias='bpa_hours' aggregate='sum' />
	   //         <attribute name='bpa_dollarhour' alias='bpa_dollarhour' aggregate='sum' />
    //            <filter type='and' >
    //                <condition attribute='bpa_member' operator='eq' value='" + ContactId.ToString() + @"' />
    //                <condition attribute='statecode' operator='eq' value='0' />
    //                <condition attribute='bpa_transactiondate' operator='not-null' />
    //                <condition attribute='bpa_transactiondate' operator='le' value='" + BenefitMonth.ToShortDateString() + @"' />
    //            </filter>
    //            </entity>
    //        </fetch>            ";
    //        EntityCollection Sums = service.RetrieveMultiple(new FetchExpression(fetchXml));

    //        if (Sums != null && Sums.Entities.Count > 0)
    //        {
    //            Entity sum = Sums.Entities[0];
    //            decimal hours = 0, dollarhours = 0;
    //            //if ((fetchedRows[0].attributes['bpa_vacationpayamount'] != null) && (fetchedRows[0].attributes['bpa_vacationpayamount'].value != ""))

    //            if (sum.Attributes.Contains("bpa_hours") && ((AliasedValue)sum.Attributes["bpa_hours"]).Value != null)
    //                hours = ((decimal)((AliasedValue)sum.Attributes["bpa_hours"]).Value);
    //            endingbalance.Add("hours", hours);

    //            //if (sum.Attributes.Contains("bpa_dollarhour"))
    //            if (sum.Attributes.Contains("bpa_dollarhour") && ((AliasedValue)sum.Attributes["bpa_dollarhour"]).Value != null)
    //                dollarhours = ((Money)((AliasedValue)sum.Attributes["bpa_dollarhour"]).Value).Value;
    //            endingbalance.Add("dollarhours", dollarhours);
    //        }

    //        return endingbalance;
    //    }


    //    public static EntityReference GetCurrentEligibilityCategory(IOrganizationService service, Guid MemberId)
    //    {

    //        Entity contact = service.Retrieve("bpa_memberplan", MemberId, new ColumnSet(new string[] { "bpa_eligibilitycategoryid" }));

    //        if (contact != null && contact.Attributes.Contains("bpa_eligibilitycategoryid"))
    //            return contact.GetAttributeValue<EntityReference>("bpa_eligibilitycategoryid");
    //        else
    //            return null;
    //    }
    }
}
