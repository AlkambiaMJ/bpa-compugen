﻿#region

using System;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;

#endregion

namespace BPA.Benefit.Management.CRM.Plugins.Helper
{
    public class EligibilityCategoryHelper
    {
        //public static string EntityName = "bpa_eligibilitycategory";

        public static int FetchEligibilityOffSet(IOrganizationService service, Guid eligibilityId)
        {
            int offset = 0;

            Entity eligibility = service.Retrieve(PluginHelper.BpaEligibilitycategory, eligibilityId,
                new ColumnSet("bpa_eligibilityoffsetmonths"));
            if (eligibility != null && eligibility.Attributes.Contains("bpa_eligibilityoffsetmonths"))
                offset = eligibility.GetAttributeValue<int>("bpa_eligibilityoffsetmonths");
            return offset;
        }

        public static Entity FetchEligibilityCategory(IOrganizationService service, Guid eligibilityId)
        {
            Entity eligibility = service.Retrieve(PluginHelper.BpaEligibilitycategory, eligibilityId, new ColumnSet(true));
            return eligibility;
        }
        public static Entity FetchEligibilityCategoryDetails(IOrganizationService service, ITracingService tracingService, Guid eligibilityCategoryId,
            DateTime effectiveDate)
        {
            tracingService.Trace("EligibilityCategoryHelper.FetchEligibilityCategoryDetails");
            //<attribute name='bpa_selfpaydeductionrate' />
            Entity eligibilityCategoryDetail = null;
            string fetchxml = $@"
                <fetch>
                    <entity name='bpa_eligibilitycategorydetail' >
                    <attribute name='bpa_deductionrate' />
                    <attribute name='bpa_reinstatementdollar' />
                    <attribute name='bpa_effectivedate' />
                    <attribute name='bpa_fundassistanceamount' />
                    <attribute name='bpa_reinstatementhours' />
                    <attribute name='bpa_hoursdeductionrate' />
                    <attribute name='bpa_hoursfundassistanceamount' />
                    <attribute name='bpa_hoursmaxmemberbank' />
                    <attribute name='bpa_hoursminimuminitialeligibility' />
                    <attribute name='bpa_maximummemberbank' />
                    <attribute name='bpa_minimumeligibility' />
                    <filter>
                        <condition attribute='statecode' operator='eq' value='0' />
                        <condition attribute='bpa_eligibilitycategory' operator='eq' value='{eligibilityCategoryId}' />
                        <condition attribute='bpa_effectivedate' operator='le' value='{effectiveDate.ToShortDateString()}' />
                    </filter>
                    <order attribute='bpa_effectivedate' descending='true' />
                    </entity>
                </fetch>
            ";
            tracingService.Trace(fetchxml);
            EntityCollection eligibilityCategorys = service.RetrieveMultiple(new FetchExpression(fetchxml));
            if (eligibilityCategorys != null && eligibilityCategorys.Entities.Count > 0)
            {
                eligibilityCategoryDetail = eligibilityCategorys.Entities[0];
            }
            return eligibilityCategoryDetail;
        }

        public static Entity FetchEligibilityCategoryByMemberId(IOrganizationService service, Guid memberId)
        {
            Entity eligibilityCategory = null;
            string fetchxml = @"
                <fetch mapping='logical' output-format='xml-platform' version='1.0' >
                  <entity name='bpa_eligibilitycategory' >
                    <attribute name='bpa_eligibilityoffsetmonths' />
                    <attribute name='bpa_maxconsecutivemonthsselfpay' />
                    <attribute name='bpa_name' />
                    <attribute name='bpa_planeligibilitytype' />
                    <filter>
                      <condition attribute='statecode' operator='eq' value='0' />
                    </filter>
                    <link-entity name='bpa_memberplan' from='bpa_eligibilitycategoryid' to='bpa_eligibilitycategoryid' >
                      <attribute name='bpa_currenteligibility' />
                      <filter>
                        <condition attribute='bpa_memberplanid' operator='eq' value='" + memberId + @"' />
                      </filter>
                    </link-entity>
                  </entity>
                </fetch>
            ";
            EntityCollection eligibilityCategorys = service.RetrieveMultiple(new FetchExpression(fetchxml));
            if (eligibilityCategorys != null && eligibilityCategorys.Entities.Count > 0)
            {
                eligibilityCategory = eligibilityCategorys.Entities[0];
            }
            return eligibilityCategory;
        }

        public static Entity FetchEligibilityDetail(IOrganizationService service, Guid eligibilityCategoryId,
            DateTime effectiveDate)
        {
            Entity detail = null;
            string fetchxml = $@"
                    <fetch count='1' > 
                      <entity name='bpa_eligibilitycategorydetail' >
                        <attribute name='bpa_eligibilitycategorydetailid' />
                        <attribute name='bpa_eligibilitycategory' />
                        <attribute name='bpa_name' />
                        <attribute name='bpa_effectivedate' />
                        <attribute name='bpa_expirationdate' />
                        <attribute name='bpa_deductionrate' />
                        <attribute name='bpa_minimumeligibility' />
                        <attribute name='bpa_maximummemberbank' />
                        <attribute name='bpa_fundassistanceamount' />
                        <attribute name='bpa_reinstatementdollar' />
                        <attribute name='bpa_selfpayamount' />
                        
                        <attribute name='bpa_hoursdeductionrate' />
                        <attribute name='bpa_hoursminimuminitialeligibility' />
                        <attribute name='bpa_hoursmaxmemberbank' />
                        <attribute name='bpa_hoursfundassistanceamount' />
                        <attribute name='bpa_reinstatementhours' />

                        <filter type='and' >
                          <condition attribute='statecode' operator='eq' value='0' />
                          <condition attribute='bpa_eligibilitycategory' operator='eq' value='{eligibilityCategoryId}' />
                          <condition attribute='bpa_effectivedate' operator='le' value='{effectiveDate.ToShortDateString()}' />
                        </filter>
                        <order attribute='bpa_effectivedate' descending='true' />
                      </entity>
                    </fetch>";
            EntityCollection detailCollection = service.RetrieveMultiple(new FetchExpression(fetchxml));

            if (detailCollection != null && detailCollection.Entities.Count > 0)
            {
                detail = detailCollection.Entities[0];
            }
            return detail;
        }
        

        public static bool IsCategoryReinstatement(IOrganizationService service, Guid eligibilityId)
        {
            bool isReinstatement = false;
            string[] columns = {"bpa_reinstatement"};
            Entity eligibility = service.Retrieve(PluginHelper.BpaEligibilitycategory, eligibilityId, new ColumnSet(columns));

            isReinstatement = eligibility.GetAttributeValue<bool>("bpa_reinstatement");
            return isReinstatement;
        }

        public static decimal GetReinstatementHours(IOrganizationService service, Guid eligibilityId,
            DateTime effectiveDate)
        {
            decimal reinstatementhours = 0;
            Entity eligibility = FetchEligibilityDetail(service, eligibilityId, effectiveDate);

            if (eligibility != null && eligibility.Attributes.Contains("bpa_reinstatementhours"))
                reinstatementhours = eligibility.GetAttributeValue<decimal>("bpa_reinstatementhours");

            return reinstatementhours;
        }

        public static decimal GetReinstatementDollars(IOrganizationService service, Guid eligibilityId,
            DateTime effectiveDate)
        {
            decimal reinstatementdollars = 0;
            Entity eligibility = FetchEligibilityDetail(service, eligibilityId, effectiveDate);

            if (eligibility != null && eligibility.Attributes.Contains("bpa_reinstatementdollar"))
                reinstatementdollars = eligibility.GetAttributeValue<Money>("bpa_reinstatementdollar").Value;

            return reinstatementdollars;
        }

        public static int GetMaxConsecutiveMonthsSelfPay(IOrganizationService service, Guid eligibilityId)
        {
            int maxSelfPay = 0;
            Entity eligibility = service.Retrieve(PluginHelper.BpaEligibilitycategory, eligibilityId,
                new ColumnSet("bpa_maxconsecutivemonthsselfpay"));

            if (eligibility != null && eligibility.Attributes.Contains("bpa_maxconsecutivemonthsselfpay"))
                maxSelfPay = eligibility.GetAttributeValue<int>("bpa_maxconsecutivemonthsselfpay");

            return maxSelfPay;
        }


        public static decimal GetMaximumMemberBank(IOrganizationService service, Guid eligibilityId, DateTime effectiveDate)
        {
            decimal maxReserveAmt = 0;
            Entity eligibility = FetchEligibilityDetail(service, eligibilityId, effectiveDate);

            if (eligibility != null && eligibility.Attributes.Contains("bpa_maximummemberbank"))
                maxReserveAmt = eligibility.GetAttributeValue<Money>("bpa_maximummemberbank").Value;

            return maxReserveAmt;
        }


        public static decimal GetMaximumMemberBank_Hour(IOrganizationService service, Guid eligibilityId, DateTime effectiveDate)
        {
            decimal maxReserveAmt = 0;
            Entity eligibility = FetchEligibilityDetail(service, eligibilityId, effectiveDate);

            if (eligibility != null && eligibility.Attributes.Contains("bpa_hoursmaxmemberbank"))
                maxReserveAmt = eligibility.GetAttributeValue<int>("bpa_hoursmaxmemberbank");
            return maxReserveAmt;
        }

        public static EntityCollection FetchAllEligibilityDetail(IOrganizationService service, ITracingService tracingService, Guid eligibilityCategoryId)
        {
            string fetchXml = $@"
			<fetch>
			  <entity name='bpa_eligibilitycategorydetail' >
				<attribute name='bpa_effectivedate' />
				<attribute name='bpa_expirationdate' />
				<attribute name='bpa_name' />
				<attribute name='ownerid' />

				<filter>
				  <condition attribute='bpa_eligibilitycategory' operator='eq' value='{eligibilityCategoryId}' />
				  <condition attribute='statecode' operator='eq' value='0' />
				</filter>
			  </entity>
			</fetch>
            ";
            return service.RetrieveMultiple(new FetchExpression(fetchXml));

        }

        public static EntityCollection FetchAllEligibilityCategoryByPlanId(IOrganizationService service, ITracingService tracingService, Guid planId)
        {
            string fetchXml = $@"
                <fetch>
                  <entity name='bpa_eligibilitycategory' >
                    <attribute name='bpa_name' />
                    <attribute name='ownerid' />
                    <filter>
                      <condition attribute='bpa_sector' operator='eq' value='{planId}' />
                      <condition attribute='statecode' operator='eq' value='0' />
                    </filter>
                  </entity>
                </fetch>
            ";
            return service.RetrieveMultiple(new FetchExpression(fetchXml));
        }

        public static EntityCollection FetchAllBenefitByEligibilityDetailId(IOrganizationService service, ITracingService tracingService, Guid eligibilityCategoryId)
        {
            string fetchXml = $@"
                    <fetch>
                      <entity name='bpa_benefiteligibilitycategorydetail' >
                        <attribute name='bpa_name' />
                        <attribute name='ownerid' />
                        <filter>
                          <condition attribute='statuscode' operator='eq' value='1' />
                          <condition attribute='bpa_eligibilitycategorydetailid' operator='eq' value='{eligibilityCategoryId}' />
                        </filter>
                      </entity>
                    </fetch>";
            return service.RetrieveMultiple(new FetchExpression(fetchXml));

        }
    }
}