﻿#region

using System;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;

#endregion

namespace BPA.Benefit.Management.CRM.Plugins.Helper
{
    public static class AccountAgreementHelper
    {
        public static void UpdateAccountVariance(IOrganizationService service, ITracingService tracingService,
            Guid employerId, decimal variance)
        {
            Entity account = new Entity(PluginHelper.AccountAgreement)
            {
                Id = employerId,
                ["bpa_totalvariance"] = new Money(variance)
            };
            service.Update(account);
        }
        
        public static Guid FetchTrustByAccountId(IOrganizationService service, Guid employerId)
        {
            Guid trustId = Guid.Empty;
            string fetchXml = @"
                <fetch>
                  <entity name='bpa_sector' >
                    <attribute name='bpa_trust' />
                    <link-entity name='bpa_accountagreement' from='bpa_planid' to='bpa_sectorid' >
                      <filter>
                        <condition attribute='bpa_accountagreementid' operator='eq' value='" + employerId + @"' />
                      </filter>
                    </link-entity>
                  </entity>
                </fetch>
            ";
            EntityCollection sectors = service.RetrieveMultiple(new FetchExpression(fetchXml));

            if (sectors == null || sectors.Entities.Count <= 0) return trustId;
            Entity e = sectors.Entities[0];
            if (e.Attributes.Contains("bpa_trust"))
                trustId = e.GetAttributeValue<EntityReference>("bpa_trust").Id;
            return trustId;
        }
        
        public static Entity FetchNsfFundDetailByEmployerId(IOrganizationService service, Guid employerId)
        {
            Entity nsfFund = null;
            string fetchXml = @"
                <fetch>
                    <entity name='bpa_trust' >
                    <attribute name='bpa_nsffund' />
                    <attribute name='bpa_nsffee' />
                    <link-entity name='bpa_sector' from='bpa_trust' to='bpa_trustid' >
                        <link-entity name='bpa_accountagreement' from='bpa_agreementid' to='bpa_sectorid' >
                        <filter>
                            <condition attribute='bpa_accountagreementid' operator='eq' value='" + employerId + @"' />
                        </filter>
                        </link-entity>
                    </link-entity>
                    </entity>
                </fetch>
            ";
            EntityCollection trusts = service.RetrieveMultiple(new FetchExpression(fetchXml));
            if (trusts != null && trusts.Entities.Count > 0)
                nsfFund = trusts.Entities[0];
            return nsfFund;
        }

        public static int GetCalculationTypeByAccountId(IOrganizationService service, ITracingService tracingService, Guid accountid)
        {
            int calculationtypeid = 0;

            string fetchxml = @"
                <fetch count='50' >
                  <entity name='bpa_subsector' >
                    <attribute name='bpa_calculationtype' />
                    <filter type='and' >
                      <condition attribute='statecode' operator='eq' value='0' />
                    </filter>
                    <link-entity name='bpa_accountagreement' from='bpa_agreementid' to='bpa_subsectorid' >
                      <filter>
                        <condition attribute='bpa_accountagreementid' operator='eq' value='" + accountid + @"' />
                      </filter>
                    </link-entity>
                  </entity>
                </fetch>
                ";
            tracingService.Trace(fetchxml);
            EntityCollection calculationtypes = service.RetrieveMultiple(new FetchExpression(fetchxml));
            if (calculationtypes == null || calculationtypes.Entities.Count <= 0) return calculationtypeid;
            Entity calculationtype = calculationtypes.Entities[0];

            if (calculationtype.Attributes.Contains("bpa_calculationtype"))
                calculationtypeid = calculationtype.GetAttributeValue<OptionSetValue>("bpa_calculationtype").Value;

            return calculationtypeid;
        }

        public static Entity FetchAccountDetail(IOrganizationService service, ITracingService tracingService, Guid accountId)
        {
            return service.Retrieve(PluginHelper.AccountAgreement, accountId, new ColumnSet(true));
        }

        public static Entity FetchSelePayAccountAgreementByPlanId(IOrganizationService service, ITracingService tracingService, Guid planId)
        {
            Entity accountAgreement = null;
            string fetchXml = $@"
                <fetch>
                  <entity name='bpa_accountagreement' >
                    <attribute name='bpa_name' />
                    <attribute name='bpa_agreementid' />
                    <filter>
                        <condition attribute='statecode' operator='eq' value='0' />
                        <condition attribute='bpa_accounttype' operator='eq' value='{(int)AccountType.SelfPay}' />
                        <condition attribute='bpa_planid' operator='eq' value='{planId.ToString()}' />
                      </filter>
                      <link-entity name='bpa_sector' from='bpa_sectorid' to='bpa_planid' >
                          <attribute name='bpa_trust' />
                      </link-entity>
                  </entity>
                </fetch>
            ";
            EntityCollection accountAgreements = service.RetrieveMultiple(new FetchExpression(fetchXml));

            if (accountAgreements != null && accountAgreements.Entities.Count > 0)
            {
                accountAgreement = accountAgreements.Entities[0];
            }

            return accountAgreement;
        }


        public static bool FetchSelePayAccountAgreementByPlanIdAndAgreementId(IOrganizationService service, ITracingService tracingService, Guid planId, Guid agreementId, Guid accountId)
        {
            bool isExists = false;
            string fetchXml = $@"
                <fetch>
                  <entity name='bpa_accountagreement' >
                    <attribute name='bpa_name' />
                    <attribute name='bpa_agreementid' />
                    <filter>
                        <condition attribute='statecode' operator='eq' value='0' />
                        <condition attribute='bpa_accounttype' operator='eq' value='{(int)AccountType.Employer}' />
                        <condition attribute='bpa_planid' operator='eq' value='{planId.ToString()}' />
                        <condition attribute='bpa_agreementid' operator='eq' value='{agreementId.ToString()}' />
                        <condition attribute='bpa_accountid' operator='eq' value='{accountId.ToString()}' />

                      </filter>
                  </entity>
                </fetch>
            ";
            EntityCollection accountAgreements = service.RetrieveMultiple(new FetchExpression(fetchXml));

            if (accountAgreements != null && accountAgreements.Entities.Count > 0)
            {
                isExists = true;
            }

            return isExists;
        }

        public static EntityCollection FetchAllAccountAgreementByAccountId(IOrganizationService service, ITracingService tracingService, Guid accountId)
        {
            

            string fetchXml = $@"
                <fetch>
                  <entity name='bpa_accountagreement' >
                    <attribute name='bpa_name' />
                    <filter>
                        <condition attribute='bpa_accountid' operator='eq' value='{accountId.ToString()}' />
                      </filter>
                  </entity>
                </fetch>
            ";
            return service.RetrieveMultiple(new FetchExpression(fetchXml));
            
        }
    }
}