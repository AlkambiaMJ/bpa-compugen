﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BPA.Benefit.Management.CRM.Plugins.Helper
{
    public class BeneficiaryHelper
    {
        public static EntityCollection FetchAllPlanBenefit(IOrganizationService service, ITracingService tracingService, Guid beneficiaryId)
        {
            string fetchXml = $@"
                <fetch>
                  <entity name='bpa_beneficiaryplan' >
                    <attribute name='bpa_name' />
                    <filter>
                      <condition attribute='bpa_beneficiaryid' operator='eq' value='{beneficiaryId}' />
                    </filter>
                  </entity>
                </fetch>
            ";
            return service.RetrieveMultiple(new FetchExpression(fetchXml));

        }
    }
}
