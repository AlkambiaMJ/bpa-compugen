﻿#region

using System;
using System.Collections.Generic;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;

#endregion

namespace BPA.Benefit.Management.CRM.Plugins.Helper
{
    public class Rate
    {
        #region Properties

        public Guid? Id { get; set; }
        public Guid? FundId { get; set; }
        public string FundName { get; set; }
        public int? FundType { get; set; }
        public Guid? TaxableFundId { get; set; }
        public string TaxableFundIdName { get; set; }
        public int? CalculationType { get; set; }
        public decimal? RateValue { get; set; }
        public DateTime EffectiveDate { get; set; }
        public Guid? LabourId { get; set; }
        public string LabourIdName { get; set; }
        public bool IsDefaultFund { get; set; }
        public decimal? HourMultiplier { get; set; }

        #endregion
    }

    public class RateHelper
    {
        public static List<Rate> GetRatesByEmployerIdUsingFetchXml(IOrganizationService service,
            ITracingService tracingservice, Guid employerId, DateTime workMonth)
        {
            if (employerId != Guid.Empty)
            {
                List<Rate> rates = new List<Rate>();

                string fetchXml = $@"
                        <fetch distinct='true' >
                          <entity name='bpa_accountagreement' >
                            <attribute name='bpa_agreementid' />
                            <filter>
                              <condition attribute='bpa_accountagreementid' operator='eq' value='{employerId}' />
                            </filter>
                            <link-entity name='bpa_subsector' from='bpa_subsectorid' to='bpa_agreementid' >
                              <link-entity name='bpa_ratelabourrole' from='bpa_agreementid' to='bpa_subsectorid' >
                                <attribute name='bpa_labourroleid' />
                                <filter>
                                    <condition attribute='statuscode' operator='eq' value='1' />
                                </filter>
                              </link-entity>
                            </link-entity>
                          </entity>
                        </fetch>";
                tracingservice.Trace(fetchXml);
                EntityCollection labouroleCollection = service.RetrieveMultiple(new FetchExpression(fetchXml));

                EntityReference erSubsector = null, erLabourrole = null;
                List<Guid> listErLabourrole = new List<Guid>();
                if (labouroleCollection != null && labouroleCollection.Entities.Count > 0)
                {
                    foreach (Entity labourRole in labouroleCollection.Entities)
                    {
                        ////Entity labourRole = labouroleCollection.Entities[0];
                        //if (labourRole.Attributes.Contains("bpa_region"))
                        //    ER_region = labourRole.GetAttributeValue<EntityReference>("bpa_region");

                        if (labourRole.Attributes.Contains("bpa_agreementid"))
                            erSubsector = labourRole.GetAttributeValue<EntityReference>("bpa_agreementid");

                        if (labourRole.Attributes.Contains("bpa_ratelabourrole2.bpa_labourroleid"))
                        {
                            erLabourrole =
                                (EntityReference)
                                    labourRole.GetAttributeValue<AliasedValue>("bpa_ratelabourrole2.bpa_labourroleid")
                                        .Value;
                            listErLabourrole.Add(erLabourrole.Id);
                        }
                    }
                    fetchXml = $@"
                            <fetch distinct='true' >
                              <entity name='bpa_rate' >
                                <attribute name='bpa_calculationtype' />
                                <attribute name='bpa_calculationtypename' />
                                <attribute name='bpa_effectivedate' />
                                <attribute name='bpa_fund' />
                                <attribute name='bpa_fundname' />
                                <attribute name='bpa_ratevalue' />
                                <attribute name='bpa_rateid' />
                                <attribute name='bpa_taxablefund' />
                                <attribute name='bpa_taxablefundname' />
                                <attribute name='bpa_hoursmultiplier' />
                                
                                <filter>
                                  <condition attribute='bpa_effectivedate' operator='le' value='{workMonth.ToShortDateString()}' />
                                  <condition attribute='statecode' operator='eq' value='0' />
                                  <condition attribute='bpa_subsector' operator='eq' value='{erSubsector.Id}' />
                                  <filter type='or' >
                                    <condition attribute='bpa_expirydate' operator='ge' value='{workMonth.ToShortDateString()}' />
                                  </filter>
                                </filter>
                                <order attribute='bpa_effectivedate' descending='true' />
                                <link-entity name='bpa_fund' from='bpa_fundid' to='bpa_fund' >
                                  <attribute name='bpa_defaultfund' />
                                  <attribute name='bpa_fundtype' />
                                  <attribute name='bpa_name' />
                                </link-entity>";
                    string strLabourRole = string.Empty;
                    if (listErLabourrole.Count > 0)
                    {
                        strLabourRole =
                            @"<link-entity name='bpa_ratelabourrole' from='bpa_rateid' to='bpa_rateid' intersect='true' >
                                  <filter>
                                    <condition attribute='bpa_labourroleid' operator='in' >";
                        foreach (Guid er in listErLabourrole)
                        {
                            strLabourRole += "<value>" + er + "</value>";
                        }
                        strLabourRole += @"</condition>
                                  </filter>
                                  <link-entity name='bpa_labourrole' from='bpa_labourroleid' to='bpa_labourroleid' >
                                    <attribute name='bpa_name' />
                                    <attribute name='bpa_labourroleid' />
                                  </link-entity>
                                </link-entity>
                              </entity>";
                    }
                    fetchXml = fetchXml + strLabourRole + @"</fetch>";

                    tracingservice.Trace(fetchXml);
                    EntityCollection rateCollection = service.RetrieveMultiple(new FetchExpression(fetchXml));
                    if (rateCollection != null && rateCollection.Entities.Count > 0)
                    {
                        foreach (Entity rate in rateCollection.Entities)
                        {
                            Rate r = new Rate();
                            r.Id = rate.Id;
                            if (rate.Contains("bpa_fund"))
                            {
                                r.FundId = rate.GetAttributeValue<EntityReference>("bpa_fund").Id;
                                r.FundName = rate.GetAttributeValue<EntityReference>("bpa_fund").Name;
                            }
                            else
                            {
                                r.FundId = Guid.Empty;
                                r.FundName = string.Empty;
                            }
                            if (rate.Contains("bpa_fund1.bpa_defaultfund"))
                                r.IsDefaultFund = (bool)rate.GetAttributeValue<AliasedValue>("bpa_fund1.bpa_defaultfund").Value;
                            else
                                r.IsDefaultFund = false;
                            if (rate.Contains("bpa_fund1.bpa_fundtype"))
                                r.FundType = ((OptionSetValue)rate.GetAttributeValue<AliasedValue>("bpa_fund1.bpa_fundtype").Value).Value;

                            if (rate.Contains("bpa_taxablefund"))
                            {
                                r.TaxableFundId = rate.GetAttributeValue<EntityReference>("bpa_taxablefund").Id;
                                r.TaxableFundIdName = rate.GetAttributeValue<EntityReference>("bpa_taxablefund").Name;
                            }
                            else
                            {
                                r.TaxableFundId = Guid.Empty;
                                r.TaxableFundIdName = string.Empty;
                            }

                            if (rate.Contains("bpa_calculationtype"))
                                r.CalculationType = rate.GetAttributeValue<OptionSetValue>("bpa_calculationtype").Value;

                            if (rate.Contains("bpa_ratevalue"))
                                r.RateValue = rate.GetAttributeValue<decimal>("bpa_ratevalue");
                            if (rate.Contains("bpa_effectivedate"))
                                r.EffectiveDate = rate.GetAttributeValue<DateTime>("bpa_effectivedate");
                            else
                                r.EffectiveDate = DateTime.MinValue;

                            decimal hourMultiplier = rate.Contains("bpa_hoursmultiplier") ? rate.GetAttributeValue<decimal>("bpa_hoursmultiplier") : 1;
                            r.HourMultiplier = hourMultiplier;

                            if (rate.Contains("bpa_labourrole3.bpa_name"))
                                r.LabourIdName = (string)rate.GetAttributeValue<AliasedValue>("bpa_labourrole3.bpa_name").Value;

                            if (rate.Contains("bpa_labourrole3.bpa_labourroleid"))
                                r.LabourId = (Guid)rate.GetAttributeValue<AliasedValue>("bpa_labourrole3.bpa_labourroleid").Value;



                            rates.Add(r);
                        }
                        return rates;
                    }
                } //end of if (labouroleCollection != null && labouroleCollection.Entities.Count > 0)
            }
            return null;
        }

        public static List<Guid> FetchAllLabourRoles(IOrganizationService service, ITracingService tracingService, Guid employerId)
        {
            tracingService.Trace("RateHelper.FetchAllLabourRoles");
            List<Guid> labourRoles = new List<Guid>();
            if (employerId != null && employerId != Guid.Empty)
            {
                string fetchXml = @"
                    <fetch distinct='true'>
                        <entity name='bpa_accountagreement' >
                            <attribute name='bpa_agreementid' />
                        <filter>
                            <condition attribute='bpa_accountagreementid' operator='eq' value='" + employerId + @"' />
                        </filter>
                        <link-entity name='bpa_subsector' from='bpa_subsectorid' to='bpa_agreementid' >
                            <link-entity name='bpa_ratelabourrole' from='bpa_agreementid' to='bpa_subsectorid' >
                                <attribute name='bpa_labourroleid' />
                                <filter>
                                    <condition attribute='statuscode' operator='eq' value='1' />
                                </filter>
                            </link-entity>
                        </link-entity>
                        </entity>
                    </fetch>                    ";
                tracingService.Trace(fetchXml);
                EntityCollection labouroleCollection = service.RetrieveMultiple(new FetchExpression(fetchXml));
                if (labouroleCollection != null && labouroleCollection.Entities.Count > 0)
                {
                    tracingService.Trace($@"Total Labour Role Found {labouroleCollection.Entities.Count}");
                    foreach (Entity labourRole in labouroleCollection.Entities)
                    {
                        EntityReference erLabourrole = null;

                        //Entity labourRole = labouroleCollection.Entities[0];
                        if (labourRole.Attributes.Contains("bpa_ratelabourrole2.bpa_labourroleid"))
                        {
                            erLabourrole =
                                (EntityReference)
                                    labourRole.GetAttributeValue<AliasedValue>("bpa_ratelabourrole2.bpa_labourroleid")
                                        .Value;
                            labourRoles.Add(erLabourrole.Id);
                        }
                    }
                }
            }
            return labourRoles;
        }

        public static Guid FetchLabourRoleByAgreementId(IOrganizationService service, Guid agreementId)
        {
            Guid recordId = Guid.Empty;
            string fetchXml = @"
                    <fetch top='1' >
                      <entity name='bpa_labourrole' >
                        <all-attributes/>
                        <filter>
                          <condition attribute='bpa_agreementid' operator='eq' value='" + agreementId + @"' />
                          <condition attribute='statecode' operator='eq' value='0' />
                          <condition attribute='bpa_isdefault' operator='eq' value='1' />
                        </filter>
                        <order attribute='createdon' />
                      </entity>
                    </fetch>";
            EntityCollection collection = service.RetrieveMultiple(new FetchExpression(fetchXml));
            if (collection != null && collection.Entities.Count > 0)
            {
                recordId = collection.Entities[0].Id;
            }
            return recordId;
        }

        public static EntityCollection FetchAllRateByAgreementId(IOrganizationService service, ITracingService tracingService, Guid agreementId)
        {
            string fetchXml = $@"
                <fetch mapping='logical' output-format='xml-platform' version='1.0' >
                  <entity name='bpa_rate' >
                    <attribute name='bpa_name' />
                    <attribute name='ownerid' />
                    <attribute name='bpa_effectivedate' />
                    <attribute name='bpa_expirydate' />
                    <filter>
                      <condition attribute='bpa_subsector' operator='eq' value='{agreementId}' />
                      <condition attribute='statecode' operator='eq' value='0' />
                    </filter>
                  </entity>
                </fetch>
            ";
            return service.RetrieveMultiple(new FetchExpression(fetchXml));

        }

        public static EntityCollection FetchAllPrePrintRateByAgreementId(IOrganizationService service, ITracingService tracingService, Guid agreementId)
        {
            string fetchXml = $@"
                <fetch mapping='logical' output-format='xml-platform' version='1.0' >
                  <entity name='bpa_preprintrate' >
                    <attribute name='bpa_name' />
                    <attribute name='ownerid' />
                    <attribute name='bpa_effectivedate' />
                    <attribute name='bpa_enddate' />
                    <filter>
                      <condition attribute='bpa_agreement' operator='eq' value='{agreementId}' />
                      <condition attribute='statecode' operator='eq' value='0' />
                    </filter>
                  </entity>
                </fetch>
            ";
            return service.RetrieveMultiple(new FetchExpression(fetchXml));
        }

        public static EntityCollection FetchAllLabourRolesByRateId(IOrganizationService service, ITracingService tracingService, Guid rateId)
        {
            string fetchXml = $@"
                <fetch mapping='logical' output-format='xml-platform' version='1.0' >
                  <entity name='bpa_ratelabourrole' >
                    <attribute name='bpa_name' />
                    <attribute name='ownerid' />
                    <filter>
                      <condition attribute='bpa_rateid' operator='eq' value='{rateId}' />
                      <condition attribute='statecode' operator='eq' value='0' />
                    </filter>
                  </entity>
                </fetch>
            ";
            return service.RetrieveMultiple(new FetchExpression(fetchXml));
        }

        public static List<Rate> GetRatesByLabourRoleId(IOrganizationService service, ITracingService tracingservice, Guid employerId,
            DateTime workMonth, Guid agreementId, Guid labourRoleId)
        {
            if (employerId != Guid.Empty)
            {
                List<Rate> rates = new List<Rate>();

                string fetchXml = $@"
                        <fetch distinct='true' >
                            <entity name='bpa_rate' >
                            <attribute name='bpa_calculationtype' />
                            <attribute name='bpa_calculationtypename' />
                            <attribute name='bpa_effectivedate' />
                            <attribute name='bpa_fund' />
                            <attribute name='bpa_fundname' />
                            <attribute name='bpa_ratevalue' />
                            <attribute name='bpa_rateid' />
                            <attribute name='bpa_taxablefund' />
                            <attribute name='bpa_taxablefundname' />
                            <attribute name='bpa_hoursmultiplier' />
                                
                            <filter>
                                <condition attribute='bpa_effectivedate' operator='le' value='{workMonth.ToShortDateString()}' />
                                <condition attribute='statecode' operator='eq' value='0' />
                                <condition attribute='bpa_subsector' operator='eq' value='{agreementId}' />
                                <filter type='or' >
                                <condition attribute='bpa_expirydate' operator='ge' value='{workMonth.ToShortDateString()}' />
                                </filter>
                            </filter>
                            <order attribute='bpa_effectivedate' descending='true' />
                            <link-entity name='bpa_fund' from='bpa_fundid' to='bpa_fund' >
                                <attribute name='bpa_defaultfund' />
                                <attribute name='bpa_fundtype' />
                                <attribute name='bpa_name' />
                            </link-entity>
                            <link-entity name='bpa_ratelabourrole' from='bpa_rateid' to='bpa_rateid' intersect='true' >
                                <filter>
                                    <condition attribute='bpa_labourroleid' operator='eq' value='{labourRoleId}'  />
                                    <condition attribute='statuscode' operator='eq' value='1' />
                                </filter>
                                <link-entity name='bpa_labourrole' from='bpa_labourroleid' to='bpa_labourroleid' >
                                    <attribute name='bpa_name' />
                                    <attribute name='bpa_labourroleid' />
                                </link-entity>
                            </link-entity>
                            </entity>
                        </fetch>";

                    tracingservice.Trace(fetchXml);
                    EntityCollection rateCollection = service.RetrieveMultiple(new FetchExpression(fetchXml));
                    if (rateCollection != null && rateCollection.Entities.Count > 0)
                    {
                        foreach (Entity rate in rateCollection.Entities)
                        {
                            Rate r = new Rate();
                            r.Id = rate.Id;
                            if (rate.Contains("bpa_fund"))
                            {
                                r.FundId = rate.GetAttributeValue<EntityReference>("bpa_fund").Id;
                                r.FundName = rate.GetAttributeValue<EntityReference>("bpa_fund").Name;
                            }
                            else
                            {
                                r.FundId = Guid.Empty;
                                r.FundName = string.Empty;
                            }
                            if (rate.Contains("bpa_fund1.bpa_defaultfund"))
                                r.IsDefaultFund = (bool)rate.GetAttributeValue<AliasedValue>("bpa_fund1.bpa_defaultfund").Value;
                            else
                                r.IsDefaultFund = false;
                            if (rate.Contains("bpa_fund1.bpa_fundtype"))
                                r.FundType = ((OptionSetValue)rate.GetAttributeValue<AliasedValue>("bpa_fund1.bpa_fundtype").Value).Value;

                            if (rate.Contains("bpa_taxablefund"))
                            {
                                r.TaxableFundId = rate.GetAttributeValue<EntityReference>("bpa_taxablefund").Id;
                                r.TaxableFundIdName = rate.GetAttributeValue<EntityReference>("bpa_taxablefund").Name;
                            }
                            else
                            {
                                r.TaxableFundId = Guid.Empty;
                                r.TaxableFundIdName = string.Empty;
                            }

                            if (rate.Contains("bpa_calculationtype"))
                                r.CalculationType = rate.GetAttributeValue<OptionSetValue>("bpa_calculationtype").Value;

                            if (rate.Contains("bpa_ratevalue"))
                                r.RateValue = rate.GetAttributeValue<decimal>("bpa_ratevalue");
                            if (rate.Contains("bpa_effectivedate"))
                                r.EffectiveDate = rate.GetAttributeValue<DateTime>("bpa_effectivedate");
                            else
                                r.EffectiveDate = DateTime.MinValue;

                            decimal hourMultiplier = rate.Contains("bpa_hoursmultiplier") ? rate.GetAttributeValue<decimal>("bpa_hoursmultiplier") : 1;
                            r.HourMultiplier = hourMultiplier;

                            if (rate.Contains("bpa_labourrole3.bpa_name"))
                                r.LabourIdName = (string)rate.GetAttributeValue<AliasedValue>("bpa_labourrole3.bpa_name").Value;

                            if (rate.Contains("bpa_labourrole3.bpa_labourroleid"))
                                r.LabourId = (Guid)rate.GetAttributeValue<AliasedValue>("bpa_labourrole3.bpa_labourroleid").Value;



                            rates.Add(r);
                        }
                        return rates;
                    }
                
            }
            return null;
        }
    }
}