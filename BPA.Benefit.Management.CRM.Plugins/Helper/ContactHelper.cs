﻿#region

using System;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using System.Collections.Generic;

#endregion

//DONE - TODOJK: use the trace service (pass it in along with the service object)

namespace BPA.Benefit.Management.CRM.Plugins.Helper
{
    public class ContactHelper
    {
        public static Entity FetchLastMemberPlan(IOrganizationService service, ITracingService tracingService,
            Guid contactId, Guid memberPlanId)
        {
            string fetchXml = @"
                <fetch>
                  <entity name='bpa_memberplan' >
                    <attribute name='createdon' />
                    <attribute name='bpa_name' />
                    <filter>
                      <condition attribute='bpa_contactid' operator='eq' value='" + contactId + @"' />
                      <condition attribute='statecode' operator='eq' value='0' />
                      <condition attribute='bpa_memberplanid' operator='neq' value='" + memberPlanId + @"' />
                    </filter>
                    <order attribute='createdon' descending='true' />
                  </entity>
                </fetch>";
            tracingService.Trace(fetchXml);

            EntityCollection profiles = service.RetrieveMultiple(new FetchExpression(fetchXml));
            Entity e = null;
            if (profiles != null && profiles.Entities.Count > 0)
            {
                e = profiles.Entities[0];
            }
            return e;
        }

        public static EntityCollection FetchAllDependents(IOrganizationService service, ITracingService tracingService,
            Guid memberId)
        {
            string fetchXml = $@"
                <fetch>
                    <entity name='bpa_dependant' >
                    <attribute name='bpa_dependantid' />
                    <attribute name='bpa_dependantnumber' />
                    <attribute name='bpa_dependantrelation' />
                    <attribute name='bpa_firstname' />
                    <attribute name='bpa_lastname' />
                    <attribute name='bpa_middlename' />
                    <attribute name='bpa_name' />
                    <filter>
                        <condition attribute='statecode' operator='eq' value='0' />
                    </filter>
                    <link-entity name='contact' from='contactid' to='bpa_contactid' >
                        <attribute name='contactid' />
                        <filter>
                        <condition attribute='statecode' operator='eq' value='0' />
                        <condition attribute='contactid' operator='eq' value='{memberId}' />
                        </filter>
                    </link-entity>
                    </entity>
                </fetch>
            ";
            tracingService.Trace("Called FetchAllDependents");
            tracingService.Trace(fetchXml);
            EntityCollection dependents = service.RetrieveMultiple(new FetchExpression(fetchXml));
            
            return dependents;
        }

        public static int GetMaxDependantNumber(IOrganizationService service, ITracingService tracingService, Guid memberPlanId)
        {
            int maxDependantNumber = 0;
            string fetchXml = $@"
                <fetch aggregate='true' >
                    <entity name='bpa_dependant' >
                    <attribute name='bpa_dependantnumber' alias='bpa_dependantnumber' aggregate='max' />
                    <filter>
                        <condition attribute='statecode' operator='eq' value='0' />
                    </filter>
                    <link-entity name='contact' from='contactid' to='bpa_contactid' >
                        <filter>
                        <condition attribute='statecode' operator='eq' value='0' />
                        <condition attribute='contactid' operator='eq' value='{memberPlanId}' />
                        </filter>
                    </link-entity>
                    </entity>
                </fetch>";
            tracingService.Trace("Called GetMaxDependantNumber");
            tracingService.Trace(fetchXml);
            EntityCollection dependents = service.RetrieveMultiple(new FetchExpression(fetchXml));

            if (dependents != null && dependents.Entities.Count > 0)
            {
                Entity dependant = dependents.Entities[0];
                object tmpNumber = dependant.Contains("bpa_dependantnumber") ? dependant.GetAttributeValue<AliasedValue>("bpa_dependantnumber").Value : null;

                if (tmpNumber != null)
                    maxDependantNumber = (int)tmpNumber;
                   
            }

            return maxDependantNumber;
        }

        public static int GetConsecutiveInactiveMonth(IOrganizationService service, ITracingService tracingService,
            Guid memberId)
        {
            int inactiveMonths = 0;

            Entity conact = service.Retrieve("bpa_memberplan", memberId, new ColumnSet("bpa_consecutiveinactivemonths"));
            if (conact != null && conact.Attributes.Contains("bpa_consecutiveinactivemonths"))
                inactiveMonths = conact.GetAttributeValue<int>("bpa_consecutiveinactivemonths");

            return inactiveMonths;
        }
        
        public static void SetCurrentEligibility(IOrganizationService service, ITracingService tracingService,
            Guid employeeId, int currenEligibilityValue, DateTime BenefitMonth)
        {
            tracingService.Trace("Inside SetCurrentEligibility Method");
            Entity member = new Entity(PluginHelper.BpaMemberplan)
            {
                Id = employeeId,
                ["bpa_currenteligibility"] = new OptionSetValue(currenEligibilityValue)
            };
            if (currenEligibilityValue == (int)MemberPlanBpaCurrentEligibility.Inactive)
                member["bpa_benefitstatus"] = new OptionSetValue((int)MemberPlanBpaMemberPlanStatus.Inactive);
            else if (currenEligibilityValue == (int)MemberPlanBpaCurrentEligibility.TransferOut)
                member["bpa_benefitstatus"] = new OptionSetValue((int)MemberPlanBpaMemberPlanStatus.ReciprocalTransferOut);
            else
                member["bpa_benefitstatus"] = new OptionSetValue((int)MemberPlanBpaMemberPlanStatus.Active);

            if (BenefitMonth != DateTime.MinValue)
            {
                //Set Current Month Eligibility
                DateTime calenderMonth = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1);
                BenefitMonth = new DateTime(BenefitMonth.Year, BenefitMonth.Month, 1);

                if (BenefitMonth == calenderMonth)
                {
                    member["bpa_currentbenefitmonth"] = BenefitMonth;
                    member["bpa_currentmontheligibility"] = new OptionSetValue(currenEligibilityValue);
                }
            }
            service.Update(member);
        }
        
        public static int FetchCurrentEligibility(IOrganizationService service, ITracingService tracingService,
            Guid memberId)
        {
            int currentEligibility = 0;
            string bpaCurrenteligibility = "bpa_currenteligibility";
            Entity contact = service.Retrieve(PluginHelper.BpaMemberplan, memberId, new ColumnSet(bpaCurrenteligibility));
            if (contact != null)
            {
                if (contact.Contains(bpaCurrenteligibility))
                    currentEligibility = contact.GetAttributeValue<OptionSetValue>(bpaCurrenteligibility).Value;
            }
            return currentEligibility;
        }

        public static EntityCollection FetchMemberPlans(IOrganizationService service, ITracingService tracingService,
            Guid memberId)
        {
            string fetchXml = @"
                <fetch mapping='logical' version='1.0' >
                  <entity name='bpa_memberplan' >
                    <attribute name='bpa_name' />
                    <filter>
                      <condition attribute='bpa_contactid' operator='eq' value='" + memberId + @"' />
                    </filter>
                  </entity>
                </fetch>
            ";
            tracingService.Trace(fetchXml);
            return service.RetrieveMultiple(new FetchExpression(fetchXml));
        }

        public static Entity FetchMemberPlan(IOrganizationService service, ITracingService tracingService, Guid memberId)
        {
            Entity memberPlan = null;
            string fetchXml = @"
                <fetch mapping='logical' version='1.0' >
                  <entity name='bpa_memberplan' >
                    <attribute name='bpa_consecutiveinactivemonths' />
                    <attribute name='bpa_consecutiveselfpaymonths' />
					<attribute name='bpa_currenteligibility' />
                    <filter>
                      <condition attribute='bpa_memberplanid' operator='eq' value='" + memberId + @"' />
                    </filter>
                    <link-entity name='bpa_eligibilitycategory' from='bpa_eligibilitycategoryid' to='bpa_eligibilitycategoryid' >
                      <attribute name='bpa_maxconsecutivemonthsselfpay' />
                      <attribute name='bpa_eligibilityoffsetmonths' />
                    </link-entity>
                  </entity>
                </fetch>
            ";
            EntityCollection memberPlans = service.RetrieveMultiple(new FetchExpression(fetchXml));
            if (memberPlans != null && memberPlans.Entities.Count > 0)
            {
                memberPlan = memberPlans.Entities[0];
            }
            return memberPlan;
        }

        public static Entity FetchMemberPlan(IOrganizationService service, Guid memberId)
        {
            Entity memberPlan = null;
            string fetchXml = @"
                <fetch mapping='logical' version='1.0' >
                  <entity name='bpa_memberplan' >
                    <attribute name='bpa_consecutiveinactivemonths' />
                    <attribute name='bpa_consecutiveselfpaymonths' />
					<attribute name='bpa_currenteligibility' />
                    <filter>
                      <condition attribute='bpa_memberplanid' operator='eq' value='" + memberId + @"' />
                    </filter>
                    <link-entity name='bpa_eligibilitycategory' from='bpa_eligibilitycategoryid' to='bpa_eligibilitycategoryid' >
                      <attribute name='bpa_maxconsecutivemonthsselfpay' />
                      <attribute name='bpa_eligibilityoffsetmonths' />
                    </link-entity>
                  </entity>
                </fetch>
            ";
            EntityCollection memberPlans = service.RetrieveMultiple(new FetchExpression(fetchXml));
            if (memberPlans != null && memberPlans.Entities.Count > 0)
            {
                memberPlan = memberPlans.Entities[0];
            }
            return memberPlan;
        }

        public static Entity FetchMemberPlan(IOrganizationService service, ITracingService tracingService, Guid memberId,
            Guid planId)
        {
            Entity memberPlan = null;
            string fetchXml = @"
                <fetch mapping='logical' version='1.0' >
                  <entity name='bpa_memberplan' >
                    <attribute name='bpa_consecutiveinactivemonths' />
                    <attribute name='bpa_name' />
                    <filter>
                      <condition attribute='bpa_contactid' operator='eq' value='" + memberId + @"' />
                      <condition attribute='bpa_planid' operator='eq' value='" + planId + @"' />
                      <condition attribute='statecode' operator='eq' value='0' />
                    </filter>
                  </entity>
                </fetch>
            ";
            tracingService.Trace("ContactHelper.FetchMemberPlan");
            tracingService.Trace(fetchXml);
            EntityCollection memberPlans = service.RetrieveMultiple(new FetchExpression(fetchXml));
            if (memberPlans != null && memberPlans.Entities.Count > 0)
            {
                memberPlan = memberPlans.Entities[0];
            }
            return memberPlan;
        }

        public static Guid CreateMemberPlan(IOrganizationService service, ITracingService tracingService,
            string firstname, string lastname, string fullname, EntityReference contact, EntityReference plan,
            OptionSetValue contactType, EntityReference eligibilityCategory, string sin, EntityReference HomeLocal,string defaultCurrency)
        {
            Entity memberPlan = new Entity(PluginHelper.BpaMemberplan);
            memberPlan["bpa_contactid"] = contact;
            memberPlan["bpa_name"] = firstname + ' ' + lastname;
            if (!string.IsNullOrEmpty(sin))
                memberPlan["bpa_socialinsurancenumber"] = sin;
            memberPlan["bpa_planid"] = plan;
            //memberPlan["bpa_memberplantype"] = contactType;
            memberPlan["bpa_eligibilitycategoryid"] = eligibilityCategory;
            memberPlan["bpa_currenteligibility"] = new OptionSetValue((int)ContactbpaCurrentEligibility.NewMember);

            if (HomeLocal != null)
                memberPlan["bpa_unionhomelocalid"] = HomeLocal;

            DateTime calenderMonth = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddMonths(-1);
            memberPlan["bpa_currentbenefitmonth"] = calenderMonth;
            memberPlan["bpa_currentmontheligibility"] = new OptionSetValue((int)ContactbpaCurrentEligibility.NewMember);

            //Fetch Default Currency
            //string defaultCurrency = ConfigurationHelper.FetchValueId(service, PluginHelper.ConfigDefaultCurrency);
            if (defaultCurrency != Guid.Empty.ToString())
                memberPlan["transactioncurrencyid"] = new EntityReference("transactioncurrency",
                    new Guid(defaultCurrency));

            return service.Create(memberPlan);
        }
        
        public static void UpdateInactiveMonthsCounter(IOrganizationService service, ITracingService tracingService,
            Guid memberId, int counter)
        {
            Entity member = new Entity(PluginHelper.BpaMemberplan);
            member.Id = memberId;
            member["bpa_consecutiveinactivemonths"] = counter;
            service.Update(member);
        }

        #region -------- Methods call from Plugins ----- 
           
        public static EntityReference FetchCurrentEligibilityCategory(IOrganizationService service, ITracingService tracingService, Guid memberId)
        {
            string bpaEligibilitycategory = "bpa_eligibilitycategoryid";
            EntityReference currentEligibilityCategory = null;
            Entity contact = service.Retrieve(PluginHelper.BpaMemberplan, memberId, new ColumnSet(bpaEligibilitycategory));
            if (contact != null)
            {
                if (contact.Attributes.Contains(bpaEligibilitycategory))
                    currentEligibilityCategory = contact.GetAttributeValue<EntityReference>(bpaEligibilitycategory);
            }
            return currentEligibilityCategory;
        }
        
        public static void CreateNewMemberTransactions(IOrganizationService service, ITracingService tracingService,
            Entity entity, Entity imageEntity)
        {
            //OptionSetValue contacttype = null;
            DateTime effectiveDate = DateTime.MinValue;

            if (entity.Attributes.Contains("bpa_retireeeffectivedate") && entity.Attributes["bpa_retireeeffectivedate"] != null)
            {
                //contacttype = ((OptionSetValue)imageEntity.Attributes["bpa_memberplantype"]);
                effectiveDate = entity.GetAttributeValue<DateTime>("bpa_retireeeffectivedate");
                EntityReference eligibilitycategory =
                    imageEntity.GetAttributeValue<EntityReference>("bpa_eligibilitycategoryid");
                EntityReference plan = imageEntity.GetAttributeValue<EntityReference>("bpa_planid");

                if ((plan != null) && (effectiveDate != DateTime.MinValue) && (eligibilitycategory != null))
                {
                    if (PlanHelper.FetchIsRetireePlanType(service, tracingService, plan.Id))
                    {
                        //(contacttype.Value == (int)MemberPlan_bpa_memberplantype.Retiree)
                        int offset = EligibilityCategoryHelper.FetchEligibilityOffSet(service, eligibilitycategory.Id);
                        DateTime workMonth = new DateTime(effectiveDate.Year, effectiveDate.Month, 1).AddMonths(offset*-1);

                        if (!TransactionHelper.IsNewMemberExists(service, entity.Id, workMonth))
                        {
                            Entity transaction = new Entity(PluginHelper.BpaTransaction);
                            transaction["bpa_memberplanid"] = new EntityReference(PluginHelper.BpaMemberplan, entity.Id);
                            transaction["bpa_transactiondate"] = workMonth;
                            transaction["bpa_benefitmonth"] = workMonth.AddMonths(offset);
                            transaction["bpa_currenteligibilitycategory"] = eligibilitycategory;
                            transaction["bpa_transactiontype"] = new OptionSetValue((int)BpaTransactionbpaTransactionType.NewMember);
                            transaction["bpa_transactioncategory"] = new OptionSetValue((int)BpaTransactionbpaTransactionCategory.Eligibility);
                            transaction["bpa_useforcalculation"] = true;
                            service.Create(transaction);
                        }
                    }
                }
            }
        }

        public static EntityReference FetchDefaultFundByMemberId(IOrganizationService service,
            ITracingService tracingService, Guid contactId, int fundType)
        {
            EntityReference fund = null;
            string fetchXml = $@"
                <fetch>
                  <entity name='bpa_trust' >
                    <link-entity name='bpa_sector' from='bpa_trust' to='bpa_trustid' >
                      <link-entity name='bpa_memberplan' from='bpa_planid' to='bpa_sectorid' >
                        <filter>
                          <condition attribute='bpa_memberplanid' operator='eq' value='{contactId}' />
                        </filter>
                      </link-entity>
                    </link-entity>
                    <link-entity name='bpa_fund' from='bpa_trust' to='bpa_trustid' >
                      <attribute name='bpa_fundid' />
                      <attribute name='bpa_name' />
                      <filter>
                        <condition attribute='statecode' operator='eq' value='0' />
                        <condition attribute='bpa_defaultfund' operator='eq' value='1' />
                        <condition attribute='bpa_fundtype' operator='eq' value='{fundType}' />
                      </filter>
                    </link-entity>
                  </entity>
                </fetch>
            ";
            EntityCollection trusts = service.RetrieveMultiple(new FetchExpression(fetchXml));
            if (trusts != null && trusts.Entities.Count > 0)
            {
                Entity e = trusts.Entities[0];

                if (e.Attributes.Contains("bpa_fund3.bpa_fundid"))
                {
                    Guid fundId = (Guid)e.GetAttributeValue<AliasedValue>("bpa_fund3.bpa_fundid").Value;
                    fund = new EntityReference("bpa_fund", fundId);
                }
            }
            return fund;
        }

        public static bool IsMemberPlanRetiree(IOrganizationService service, ITracingService tracingService,
            Guid memeberPlanId)
        {
            bool isRetiree = false;
            string fetchXml = @"
            <fetch>
              <entity name='bpa_sector' >
                <attribute name='bpa_plantype' />
                <link-entity name='bpa_memberplan' from='bpa_planid' to='bpa_sectorid' >
                  <filter>
                    <condition attribute='bpa_memberplanid' operator='eq' value='" + memeberPlanId + @"' />
                  </filter>
                </link-entity>
              </entity>
            </fetch>
            ";
            EntityCollection plans = service.RetrieveMultiple(new FetchExpression(fetchXml));
            if (plans != null && plans.Entities.Count > 0)
            {
                Entity plan = plans.Entities[0];

                if (plan.Attributes.Contains("bpa_plantype"))
                {
                    int plantype = plan.GetAttributeValue<OptionSetValue>("bpa_plantype").Value;
                    if (plantype == (int)PlanPlanType.Retiree)
                        isRetiree = true;
                }
            }

            return isRetiree;
        }

        public static EntityReference FetchDefaultFundByMemberId(IOrganizationService service, Guid contactId)
        {
            EntityReference fund = null;

            string fetchXml = @"
                <fetch>
                  <entity name='bpa_trust' >
                    <link-entity name='bpa_sector' from='bpa_trust' to='bpa_trustid' >
                      <link-entity name='bpa_memberplan' from='bpa_planid' to='bpa_sectorid' >
                        <filter>
                          <condition attribute='bpa_memberplanid' operator='eq' value='" + contactId + @"' />
                        </filter>
                      </link-entity>
                    </link-entity>
                    <link-entity name='bpa_fund' from='bpa_trust' to='bpa_trustid' >
                      <attribute name='bpa_fundid' />
                      <attribute name='bpa_name' />
                      <filter>
                        <condition attribute='statecode' operator='eq' value='0' />
                        <condition attribute='bpa_defaultfund' operator='eq' value='1' />
                        <condition attribute='bpa_fundtype' operator='eq' value='922070001' />
                      </filter>
                    </link-entity>
                  </entity>
                </fetch>
            ";
            EntityCollection trusts = service.RetrieveMultiple(new FetchExpression(fetchXml));
            if (trusts == null || trusts.Entities.Count <= 0) return fund;
            Entity e = trusts.Entities[0];

            if (!e.Attributes.Contains("bpa_fund3.bpa_fundid")) return fund;
            Guid fundId = (Guid)e.GetAttributeValue<AliasedValue>("bpa_fund3.bpa_fundid").Value;
            fund = new EntityReference("bpa_fund", fundId);
            return fund;
        }
       
        public static bool FetchMemberStatus(IOrganizationService service, Guid memberId)
        { 
            bool result = true;
            Entity member = service.Retrieve(PluginHelper.BpaMemberplan, memberId, new ColumnSet("bpa_benefitstatus", "bpa_currenteligibility"));

            if (member == null) return result;
            if (member.Contains("bpa_benefitstatus"))
            {
                int memberstatus = member.GetAttributeValue<OptionSetValue>("bpa_benefitstatus").Value;

                if ((memberstatus == (int)MemberPlanBpaMemberPlanStatus.ReciprocalTransferOut))
                    result = true;
                else
                    result = false;
            }
                        
            return result;
        }
        
        public static void UpdateBothConsecutiveMonths(IOrganizationService service, Guid employeeId, int selfPayCounter, int inactiveMonthCounter, bool isSelfPayCounterUpdate)
        {
            Entity member = new Entity(PluginHelper.BpaMemberplan)
            {
                Id = employeeId,
                ["bpa_consecutiveinactivemonths"] = inactiveMonthCounter
            };

            if (isSelfPayCounterUpdate)
                member["bpa_consecutiveselfpaymonths"] = selfPayCounter;
            service.Update(member);
        }

        public static void SetConsecutiveSelfPayMonths(IOrganizationService service, ITracingService tracingService,
            Guid employeeId, int selfPayCounter)
        {
            Entity member = new Entity(PluginHelper.BpaMemberplan);
            member.Id = employeeId;
            member["bpa_consecutiveselfpaymonths"] = selfPayCounter;
            service.Update(member);
        }

        public static void UpdateLastInsuranceEffectiveDate(IOrganizationService service, ITracingService tracingService, Guid memberPlanId, DateTime benefitWorkMonth)
        {
            Entity member = new Entity(PluginHelper.BpaMemberplan);
            member.Id = memberPlanId;
            member["bpa_insuranceeffectivedate"] = benefitWorkMonth;
            service.Update(member);
        }
 

        public static Entity FetchMemberPlanInformation(IOrganizationService service, ITracingService tracingService, Guid memberPlanId)
        {
            tracingService.Trace("ContactHelper.FetchMemberPlanInformation");

            return service.Retrieve(PluginHelper.BpaMemberplan, memberPlanId, new ColumnSet(true));
        }
        

        public static EntityReference GetMemberPlan(IOrganizationService service, ITracingService tracingService, Guid memberPlanId)
        {
            EntityReference plan = null;
            Entity memberPlan = FetchMemberPlanInformation(service, tracingService, memberPlanId);

            if (memberPlan != null && memberPlan.Contains("bpa_planid"))
                plan = memberPlan.GetAttributeValue<EntityReference>("bpa_planid");
            return plan;
        }

        public static DateTime GetInsuranceEffectiveDate(IOrganizationService service, ITracingService tracingService, Guid memberPlanId)
        {
            tracingService.Trace("ContactHelper.GetInsuranceEffectiveDate");
            DateTime insuranceEffectiveDate = DateTime.MinValue;
            Entity memberPlan = FetchMemberPlanInformation(service, tracingService, memberPlanId);
            tracingService.Trace("AFTER FetchMemberPlanInformation");

            if (memberPlan != null && memberPlan.Contains("bpa_insuranceeffectivedate"))
                insuranceEffectiveDate = memberPlan.GetAttributeValue<DateTime>("bpa_insuranceeffectivedate");
            return insuranceEffectiveDate;
        }

        #endregion



        public static bool HasActiveMemberPlan(IOrganizationService service, ITracingService tracingService, Guid contactId)
        {
            string fetchXml = $@"
                <fetch>
                  <entity name='bpa_memberplan' >
                    <attribute name='bpa_name' />
                    <filter>
                      <condition attribute='bpa_contactid' operator='eq' value='{contactId}' />
                      <condition attribute='statuscode' operator='eq' value='1' />
                    </filter>
                  </entity>
                </fetch>
            ";
            bool isExists = false;
            EntityCollection collection = service.RetrieveMultiple(new FetchExpression(fetchXml));
            if (collection != null && collection.Entities.Count > 0)
                isExists = true;

            return isExists;
        }
    }
}