﻿#region

using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using System;

#endregion

namespace BPA.Benefit.Management.CRM.Plugins.Helper
{
    public class MemberHelper
    {
        public static Entity FetchMemberBySin(IOrganizationService service, string socialInsuranceNo)
        {
            Entity member = null;
            string fetchXml = @"
                <fetch>
                  <entity name='contact' >
                    <attribute name='fullname' />
                    <attribute name='firstname' />
                    <attribute name='lastname' />
                    <attribute name='bpa_socialinsurancenumber' />
                    <filter>
                      <condition attribute='bpa_socialinsurancenumber' operator='eq' value='" + socialInsuranceNo +
                              @"' />
                      <condition attribute='statecode' operator='eq' value='0' />
                    </filter>
                  </entity>
                </fetch>
            ";

            EntityCollection members = service.RetrieveMultiple(new FetchExpression(fetchXml));
            if (members != null && members.Entities.Count > 0)
            {
                member = members.Entities[0];
            }
            return member;
        }


        public static EntityCollection FetchContactByHashSIN(IOrganizationService service, ITracingService tracingService, string hashSIN)
        {
            //Entity contact = null;
            string fetchXml = $@"
                <fetch>
                  <entity name='contact' >
                    <attribute name='fullname' />
                    <attribute name='firstname' />
                    <attribute name='lastname' />
                    <attribute name='bpa_socialinsurancenumber' />
                    <filter>
                      <condition attribute='bpa_hashsin' operator='like' value='%{hashSIN}%' />
                      <condition attribute='statecode' operator='eq' value='0' />
                    </filter>
                  </entity>
                </fetch>
            ";
            tracingService.Trace(fetchXml);
            EntityCollection contacts = service.RetrieveMultiple(new FetchExpression(fetchXml));
            
            return contacts;
        }


        public static EntityCollection FetchContactBySIN(IOrganizationService service, ITracingService tracingService, string sin)
        {
            string fetchXml = $@"
                <fetch>
                  <entity name='contact' >
                    <attribute name='fullname' />
                    <attribute name='firstname' />
                    <attribute name='lastname' />
                    <attribute name='bpa_socialinsurancenumber' />
                    <filter>
                      <condition attribute='bpa_socialinsurancenumber' operator='eq' value='{sin}' />
                      <condition attribute='statecode' operator='eq' value='0' />
                    </filter>
                  </entity>
                </fetch>
            ";
            tracingService.Trace(fetchXml);

            EntityCollection contacts = service.RetrieveMultiple(new FetchExpression(fetchXml));

            return contacts;
        }


        public static Entity FetchContactByMemberPlanId(IOrganizationService service, ITracingService tracingService, Guid memberPlanId)
        {
            string fetchXml = $@"
            <fetch>
                <entity name='contact' >
                    <attribute name='fullname' />
                    <attribute name='bpa_socialinsurancenumber' />
                    <attribute name='ownerid' />
                    <attribute name='bpa_dateofbirth' />

                    <link-entity name='bpa_memberplan' from='bpa_contactid' to='contactid' >
                        <filter>
                            <condition attribute='bpa_memberplanid' operator='eq' value='{memberPlanId}' />
                        </filter>
                    </link-entity>
                </entity>
            </fetch>            ";

            Entity contact = null;
            EntityCollection contacts = service.RetrieveMultiple(new FetchExpression(fetchXml));
            if(contacts != null && contacts.Entities.Count > 0)
            {
                contact = contacts.Entities[0];
            }
            return contact;
        }

        public static bool isDrugCardIdUnique(IOrganizationService service, ITracingService tracingService, string drugCardId)
        {
            string fetchXml = $@"
             <fetch>
                <entity name='bpa_memberplan' >
                    <attribute name='bpa_memberplanid' />
                    <filter>
                        <condition attribute='bpa_drugcardid' operator='eq' value='{drugCardId}' />
                    </filter>
                </entity>
            </fetch>";

            EntityCollection memberPlans = service.RetrieveMultiple(new FetchExpression(fetchXml));

            if (memberPlans != null && memberPlans.Entities.Count > 0)
                return false;
            else
                return true;
        }
    }
}