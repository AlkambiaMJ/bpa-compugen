﻿#region

using System;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;

#endregion

// DONE - TODOJK: Need comments

namespace BPA.Benefit.Management.CRM.Plugins.Helper
{
    public class PlanHelper
    {
        //Get the Plan Type by Plan Id
        public static bool FetchIsRetireePlanType(IOrganizationService service, ITracingService tracingService,
            Guid planId)
        {
            bool isRetiree = false;
            Entity plan = FetchPlanInformation(service, tracingService, planId);
            if (plan != null)
            {
                int plantype = plan.GetAttributeValue<OptionSetValue>("bpa_plantype").Value;
                if (plantype == (int)PlanPlanType.Retiree)
                    isRetiree = true;
            }
            return isRetiree;
        }

        public static int GetPlanEligibilityType(IOrganizationService service, ITracingService tracingService,
            Guid planId)
        {
            int plantype = 0;
            Entity plan = FetchPlanInformation(service, tracingService, planId);
            if (plan != null)
                plantype = plan.GetAttributeValue<OptionSetValue>("bpa_planeligibilitytype").Value;
            return plantype;
        }

        public static Entity FetchPlanInformation(IOrganizationService service, ITracingService tracingService, Guid planId)
        {
            return service.Retrieve(PluginHelper.BpaSector, planId, new ColumnSet(true));
        }

        public static bool IsBenefitPlan(IOrganizationService service, ITracingService tracingService, Guid memberPlanId)
        {
            bool isBenefitPlan = false;

            string fetchXml = $@"
                <fetch>
                  <entity name='bpa_sector' >
                    <attribute name='bpa_benefitplan' />
                    <link-entity name='bpa_memberplan' from='bpa_planid' to='bpa_sectorid' >
                      <filter>
                        <condition attribute='bpa_memberplanid' operator='eq' value='{memberPlanId}' />
                      </filter>
                    </link-entity>
                  </entity>
                </fetch>
                ";
            EntityCollection collection = service.RetrieveMultiple(new FetchExpression(fetchXml));
            if (collection != null && collection.Entities.Count > 0)
            {
                Entity entity = collection.Entities[0];
                isBenefitPlan = entity.GetAttributeValue<bool>("bpa_benefitplan");
            }
            return isBenefitPlan;
        }

        public static Entity FetchPlanAndDefaultFund(IOrganizationService service, ITracingService tracingService, Guid planId, bool isBenefitPlan)
        {
            tracingService.Trace("Inside method of PlanHelper.FetchPlanAndDefaultFund");

            Entity plan = null;
            int fundType = 922070001;
            if (!isBenefitPlan)
                fundType = 922070003;
            string fetchXml = $@"
                <fetch>
                  <entity name='bpa_sector' >
                    <attribute name='bpa_benefitplan' />
                    <attribute name='bpa_name' />
                    <attribute name='bpa_plannumber' />
                    <attribute name='bpa_planeligibilitytype' />
                    <attribute name='bpa_pensionplan' />
                    <attribute name='bpa_plantype' />
                    <filter>
                      <condition attribute='bpa_sectorid' operator='eq' value='{planId}' />
                    </filter>
                    <link-entity name='bpa_trust' from='bpa_trustid' to='bpa_trust' >
                      <link-entity name='bpa_fund' from='bpa_trust' to='bpa_trustid' >
                        <filter>
                          <condition attribute='bpa_defaultfund' operator='eq' value='1' />
                          <condition attribute='bpa_fundtype' operator='in' >
                            <value>922070001</value>
                            <value>922070003</value>
                          </condition>
                        </filter>
                      </link-entity>
                    </link-entity>
                  </entity>
                </fetch> 
            ";
            tracingService.Trace(fetchXml);
            EntityCollection collection = service.RetrieveMultiple(new FetchExpression(fetchXml));
            if (collection != null && collection.Entities.Count > 0)
                plan = collection.Entities[0];
            return plan;
        }

        public static Entity FetchPlanInfoWithPensionBenfitRate(IOrganizationService service, ITracingService tracingService, Guid planId, DateTime workMonth)
        {
            tracingService.Trace("Inside method of PlanHelper.FetchPlanInfoWithPensionBenfitRate");

            Entity plan = null;
            string fetchXml = $@"
                <fetch>
                  <entity name='bpa_sector' >
                    <attribute name='bpa_pensionplantype' />
                    <attribute name='bpa_pensionplan' />
                    <filter>
                      <condition attribute='bpa_sectorid' operator='eq' value='{planId}' />
                    </filter>
                    <link-entity name='bpa_planpensionbenefitrate' from='bpa_planid' to='bpa_sectorid' link-type='outer' >
                      <attribute name='bpa_effectivedate' />
                      <attribute name='bpa_enddate' />
                      <attribute name='bpa_dollaramountrate' />
                      <attribute name='bpa_name' />
                      <attribute name='bpa_calculationtype' />
                      <filter type='and' >
                        <condition attribute='bpa_effectivedate' operator='le' value='{workMonth.ToShortDateString()}' />
                        <condition attribute='statecode' operator='eq' value='0' />
                        <filter type='or' >
                          <condition attribute='bpa_enddate' operator='null' />
                          <condition attribute='bpa_enddate' operator='ge' value='{workMonth.ToShortDateString()}' />
                        </filter>
                      </filter>
                      <order attribute='createdon' descending='true' />
                    </link-entity>
                  </entity>
                </fetch>                
            ";
            tracingService.Trace(fetchXml);
            EntityCollection collection = service.RetrieveMultiple(new FetchExpression(fetchXml));
            if (collection != null && collection.Entities.Count > 0)
                plan = collection.Entities[0];
            return plan;
        }
        public static EntityCollection FetchAllPlanByTrustId(IOrganizationService service, ITracingService tracingService, Guid trustId)
        {
            string fetchXml = $@"
            <fetch>
              <entity name='bpa_sector' >
                <all-attributes/>
                <filter>
                  <condition attribute='bpa_trust' operator='eq' value='{trustId}' />
                  <condition attribute='statecode' operator='eq' value='0' />
                </filter>
              </entity>
            </fetch>
            ";
            return service.RetrieveMultiple(new FetchExpression(fetchXml));
        }

        public static EntityCollection FetchAllPlanVestingRulesByPlanId(IOrganizationService service, ITracingService tracingService, Guid planId)
        {
            string fetchXml = $@"
            <fetch>
              <entity name='bpa_planpensionvesting' >
                <attribute name='bpa_name' />
                <attribute name='ownerid' />
                <attribute name='bpa_startdate' />
                <attribute name='bpa_enddate' />
                <filter>
                  <condition attribute='bpa_planid' operator='eq' value='{planId}' />
                  <condition attribute='statecode' operator='eq' value='0' />
                </filter>
              </entity>
            </fetch>
            ";
            return service.RetrieveMultiple(new FetchExpression(fetchXml));
        }

        public static EntityCollection FetchAllPlanEligibilityRulesByPlanId(IOrganizationService service, ITracingService tracingService, Guid planId)
        {
            string fetchXml = $@"
            <fetch>
              <entity name='bpa_planpensioneligibility' >
                <attribute name='bpa_name' />
                <attribute name='ownerid' />
                <attribute name='bpa_startdate' />
                <attribute name='bpa_enddate' />
                <filter>
                  <condition attribute='bpa_planid' operator='eq' value='{planId}' />
                  <condition attribute='statecode' operator='eq' value='0' />
                </filter>
              </entity>
            </fetch>
            ";
            return service.RetrieveMultiple(new FetchExpression(fetchXml));
        }
        public static EntityCollection FetchAllPlanSolvencyRatesByPlanId(IOrganizationService service, ITracingService tracingService, Guid planId)
        {
            string fetchXml = $@"
            <fetch>
              <entity name='bpa_planpensionsolvency' >
                <attribute name='bpa_name' />
                <attribute name='ownerid' />
                <attribute name='bpa_startdate' />
                <attribute name='bpa_enddate' />
                <filter>
                  <condition attribute='bpa_planid' operator='eq' value='{planId}' />
                  <condition attribute='statecode' operator='eq' value='0' />
                </filter>
              </entity>
            </fetch>
            ";
            return service.RetrieveMultiple(new FetchExpression(fetchXml));
        }
        public static EntityCollection FetchAllPlanBenefitRatesByPlanId(IOrganizationService service, ITracingService tracingService, Guid planId)
        {
            string fetchXml = $@"
            <fetch>
              <entity name='bpa_planpensionbenefitrate' >
                <attribute name='bpa_name' />
                <attribute name='ownerid' />
                <attribute name='bpa_effectivedate' />
                <attribute name='bpa_enddate' />
                <filter>
                  <condition attribute='bpa_planid' operator='eq' value='{planId}' />
                  <condition attribute='statecode' operator='eq' value='0' />
                </filter>
              </entity>
            </fetch>
            ";
            return service.RetrieveMultiple(new FetchExpression(fetchXml));
        }
        public static EntityCollection FetchAllPlanStatementDetailByPlanId(IOrganizationService service, ITracingService tracingService, Guid planId)
        {
            string fetchXml = $@"
            <fetch>
              <entity name='bpa_planstatementdetail' >
                <attribute name='bpa_name' />
                <attribute name='ownerid' />
                <attribute name='bpa_startdate' />
                <attribute name='bpa_enddate' />
                <filter>
                  <condition attribute='bpa_planid' operator='eq' value='{planId}' />
                  <condition attribute='statecode' operator='eq' value='0' />
                </filter>
              </entity>
            </fetch>
            ";
            return service.RetrieveMultiple(new FetchExpression(fetchXml));
        }

    }
}