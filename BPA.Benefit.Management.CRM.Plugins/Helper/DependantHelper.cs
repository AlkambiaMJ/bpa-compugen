﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BPA.Benefit.Management.CRM.Plugins.Helper
{
    public class DependantHelper
    {
        public static EntityCollection FetchAllPlanDependant(IOrganizationService service, ITracingService tracingService, Guid dependantId)
        {
            string fetchXml = $@"
                <fetch>
                  <entity name='bpa_dependantplan' >
                    <attribute name='bpa_name' />
                    <filter>
                      <condition attribute='bpa_dependantid' operator='eq' value='{dependantId}' />
                    </filter>
                  </entity>
                </fetch>
            ";
            return service.RetrieveMultiple(new FetchExpression(fetchXml));

        }
    }
}
