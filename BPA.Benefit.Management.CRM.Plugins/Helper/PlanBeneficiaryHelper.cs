﻿#region

using System;
using System.Collections.Generic;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;

#endregion

namespace BPA.Benefit.Management.CRM.Plugins.Helper
{
    public class PlanBeneficiaryHelper
    {
        public static bool IsBeneficiaryAlredyExists(IOrganizationService service, ITracingService tracingService, Guid beneficiaryId, Guid memberPlanId)
        {
            bool isExists = false;
            string fetchXml = $@"
               <fetch mapping='logical' output-format='xml-platform' version='1.0' >
                  <entity name='bpa_beneficiaryplan' >
                    <attribute name='bpa_name' />
                    <filter>
                      <condition attribute='statecode' operator='eq' value='0' />
                      <condition attribute='bpa_beneficiaryid' operator='eq' value='{beneficiaryId.ToString()}' />
                      <condition attribute='bpa_memberplanid' operator='eq' value='{memberPlanId.ToString()}' />
                    </filter>
                  </entity>
                </fetch>
            ";
            EntityCollection collection = service.RetrieveMultiple(new FetchExpression(fetchXml));

            if (collection != null && collection.Entities.Count > 0)
                isExists = true;

            return isExists;
        }
    }
}
