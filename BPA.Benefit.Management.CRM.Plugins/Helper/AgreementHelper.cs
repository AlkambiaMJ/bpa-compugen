﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BPA.Benefit.Management.CRM.Plugins.Helper
{
    public class AgreementHelper
    {
        public static int FetchAgreementCalculationType(IOrganizationService service, ITracingService tracingService, Guid agreementId)
        {
            int calculationType = 0;
            string[] columns = { "bpa_calculationtype" };
            Entity Agreement = service.Retrieve(PluginHelper.BpaSubsector, agreementId, new ColumnSet(columns));
            if(Agreement != null)
                calculationType = Agreement.Contains("bpa_calculationtype") ? Agreement.GetAttributeValue<OptionSetValue>("bpa_calculationtype").Value : 0;
                       
            return calculationType;
        }
        
        public static EntityCollection FetchAllAgreementGroupByPlanId(IOrganizationService service, ITracingService tracingService, Guid planId)
        {
            string fetchXml = $@"
                <fetch>
                  <entity name='bpa_agreementgroup' >
                    <attribute name='bpa_name' />
                    <attribute name='ownerid' />
                    <filter>
                      <condition attribute='bpa_planid' operator='eq' value='{planId}' />
                      <condition attribute='statecode' operator='eq' value='0' />
                    </filter>
                  </entity>
                </fetch>
            ";
            return service.RetrieveMultiple(new FetchExpression(fetchXml));
        }

        public static EntityCollection FetchAllAgreementsByPlanId(IOrganizationService service, ITracingService tracingService, Guid planId)
        {
            string fetchXml = $@"
                <fetch>
                  <entity name='bpa_subsector' >
                    <attribute name='bpa_name' />
                    <attribute name='ownerid' />
                    <filter>
                      <condition attribute='bpa_sector' operator='eq' value='{planId}' />
                      <condition attribute='statecode' operator='eq' value='0' />
                    </filter>
                  </entity>
                </fetch>
            ";
            return service.RetrieveMultiple(new FetchExpression(fetchXml));
        }

        public static EntityCollection FetchAllDeliquncyRulesByAgreementId(IOrganizationService service, ITracingService tracingService, Guid agreementId)
        {
            string fetchXml = $@"
                <fetch>
                  <entity name='bpa_delinquencyrule' >
                    <attribute name='bpa_name' />
                    <attribute name='ownerid' />
                    <attribute name='bpa_startdate' />
                    <attribute name='bpa_enddate' />
                    <filter>
                      <condition attribute='bpa_agreementid' operator='eq' value='{agreementId}' />
                      <condition attribute='statecode' operator='eq' value='0' />
                    </filter>
                  </entity>
                </fetch>
            ";
            return service.RetrieveMultiple(new FetchExpression(fetchXml));
        }


    }
}
