﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;


namespace BPA.Benefit.Management.CRM.Plugins.Helper
{
    public class BenefitAuditHelper
    {

        public static Guid CreateBenefitAudit(IOrganizationService service, ITracingService tracingService, EntityReference memberPlanId, 
            DateTime benefitMonth, DateTime workMonth, EntityReference oldEligibilityCategory, EntityReference oldEligibilityCategoryDetail
            ,int oldBenefitStatus, EntityReference newEligibilityCategory, EntityReference newEligibilityCategoryDetail
            ,int newBenefitStatus, bool isCategoryChange, bool isBenefitstatuschange, EntityReference ownerid)
        {
            Entity benefitAudit = new Entity(PluginHelper.BpaBenefitAuditHeader);
            benefitAudit["bpa_memberplanid"] = memberPlanId;

            //New Category Detail
            benefitAudit["bpa_neweligibilitycategoryid"] = newEligibilityCategory;
            benefitAudit["bpa_neweligibilitycategorydetailid"] = newEligibilityCategoryDetail;
            benefitAudit["bpa_neweligibility"] = new OptionSetValue(newBenefitStatus);

            //Old Category Information
            if (benefitMonth != DateTime.MinValue)
                benefitAudit["bpa_benefitmonth"] = benefitMonth;
            if (workMonth != DateTime.MinValue)
                benefitAudit["bpa_workmonth"] = workMonth;
            if (oldEligibilityCategory != null)
                benefitAudit["bpa_oldeligibilitycategoryid"] = oldEligibilityCategory;
            if (oldEligibilityCategoryDetail != null)
                benefitAudit["bpa_oldeligibilitycategorydetailid"] = oldEligibilityCategoryDetail;
            if (oldBenefitStatus != 0)
                benefitAudit["bpa_oldeligibility"] = new OptionSetValue(oldBenefitStatus);

            benefitAudit["bpa_categorychange"] = isCategoryChange;
            benefitAudit["bpa_benefitstatuschange"] = isBenefitstatuschange;
            benefitAudit["bpa_name"] = Guid.NewGuid().ToString();
            if(ownerid != null)
                benefitAudit["ownerid"] = ownerid;


            return service.Create(benefitAudit);
        }


        public static Entity IsEligibilityTransactionExists(IOrganizationService service, ITracingService tracingService, Guid memberId, DateTime workMonth)
        {
            Entity ifExists = null;

            // Get All Contributions for related Submission
            QueryExpression queryExpression = new QueryExpression
            {
                EntityName = PluginHelper.BpaBenefitAuditHeader,
                ColumnSet = new ColumnSet(true),
                Criteria =
                {
                    Filters =
                    {
                        new FilterExpression
                        {
                            FilterOperator = LogicalOperator.And,
                            Conditions =
                            {
                                new ConditionExpression("bpa_memberplanid", ConditionOperator.Equal, memberId.ToString()),
                                new ConditionExpression("bpa_workmonth", ConditionOperator.Equal, workMonth)
                            }
                        }
                    }
                }
            };
            queryExpression.AddOrder("createdon", OrderType.Descending);
            EntityCollection transactions = service.RetrieveMultiple(queryExpression);
            if (transactions != null && transactions.Entities.Count > 0)
                ifExists = transactions.Entities[0];
            return ifExists;
        }

        public static Guid CreateBenefitAuditDetail(IOrganizationService service, ITracingService tracingService, Guid benefitAuditHeaderId, EntityReference benefitId, decimal rate, int lives, 
            EntityReference EligibilityCategoryDetail, EntityReference ownerid)
        {
            Entity detail = new Entity(PluginHelper.BpaBenefitAuditDetail);
            detail["bpa_benefitauditheaderid"] = new EntityReference(PluginHelper.BpaBenefitAuditHeader, benefitAuditHeaderId);
            if (benefitId != null)
                detail["bpa_benefitid"] = benefitId;
            if (rate != 0)
                detail["bpa_rate"] = rate;
            detail["bpa_lives"] = lives;
            if (EligibilityCategoryDetail != null)
                detail["bpa_eligibilitycategorydetailid"] = EligibilityCategoryDetail;
            if (ownerid != null)
                detail["ownerid"] = ownerid;

            return service.Create(detail);
        }

        public static EntityCollection FetchAllBenefitsByCategoryDetailId(IOrganizationService service, ITracingService tracingService, Guid eligibilityCategoryDetailId)
        {
            string fetchXml = $@"
                <fetch>
                  <entity name='bpa_benefiteligibilitycategorydetail' >
                    <attribute name='bpa_name' />
                    <attribute name='bpa_premiumrate' />
                    <attribute name='bpa_benefitid' />
                    <filter>
                      <condition attribute='statecode' operator='eq' value='0' />
                      <condition attribute='bpa_eligibilitycategorydetailid' operator='eq' value='{eligibilityCategoryDetailId}' />
                    </filter>
                    <link-entity name='bpa_benefit' from='bpa_benefitid' to='bpa_benefitid' >
                        <attribute name='bpa_benefitstatustype' />
                        <attribute name='bpa_maximumagerestriction' />
                    </link-entity>
                  </entity>
                </fetch>";

            return service.RetrieveMultiple(new FetchExpression(fetchXml));
        }

        public static EntityCollection FetchAllBenefitAuditDetails(IOrganizationService service, ITracingService tracingService, Guid benefitAuditHeaderId, Guid eligibilityCategoryDetailId)
        {
            string fetchXml = $@"
                <fetch>
                  <entity name='bpa_benefitauditdetail' >
                    <attribute name='bpa_lives' />
                    <attribute name='bpa_rate' />
                    <attribute name='bpa_benefitauditheaderid' />
                    <attribute name='bpa_eligibilitycategorydetailidname' />
                    <attribute name='bpa_name' />
                    <attribute name='bpa_benefitid' />
                    <filter>
                      <condition attribute='statuscode' operator='eq' value='1' />
                      <condition attribute='bpa_benefitauditheaderid' operator='eq' value='{benefitAuditHeaderId}' />
                      <condition attribute='bpa_eligibilitycategorydetailid' operator='eq' value='{eligibilityCategoryDetailId}' />
                    </filter>
                  </entity>
                </fetch>
            ";

            return service.RetrieveMultiple(new FetchExpression(fetchXml));

        }
    }
}
