﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Crm.Sdk.Messages;

namespace BPA.Benefit.Management.CRM.Plugins.Helper
{
    class MemberPlanRetroprocessingHelper_Optimized
    {
        public static EntityReference FetchDefaultFundByMemberPlan(IOrganizationService service, ITracingService tracingService, Guid memberPlanId)
        {
            // TODO - Add comments and trace
            EntityReference fund = null;

            string fetchXml = @"
                <fetch>
                  <entity name='bpa_trust' >
                    <link-entity name='bpa_sector' from='bpa_trust' to='bpa_trustid' >
                      <link-entity name='bpa_memberplan' from='bpa_planid' to='bpa_sectorid' >
                        <filter>
                          <condition attribute='bpa_memberplanid' operator='eq' value='" + memberPlanId + @"' />
                        </filter>
                      </link-entity>
                    </link-entity>
                    <link-entity name='bpa_fund' from='bpa_trust' to='bpa_trustid' >
                      <attribute name='bpa_fundid' />
                      <attribute name='bpa_name' />
                      <filter>
                        <condition attribute='statecode' operator='eq' value='0' />
                        <condition attribute='bpa_defaultfund' operator='eq' value='1' />
                        <condition attribute='bpa_fundtype' operator='eq' value='922070001' />
                      </filter>
                    </link-entity>
                  </entity>
                </fetch>
            ";
            EntityCollection trusts = service.RetrieveMultiple(new FetchExpression(fetchXml));
            if (trusts == null || trusts.Entities.Count <= 0) return fund;
            Entity e = trusts.Entities[0];

            if (!e.Attributes.Contains("bpa_fund3.bpa_fundid")) return fund;
            Guid fundId = (Guid)e.GetAttributeValue<AliasedValue>("bpa_fund3.bpa_fundid").Value;
            fund = new EntityReference("bpa_fund", fundId);
            return fund;
        }

        public static EntityCollection FetchAllRelevantTransactions(IOrganizationService service, ITracingService tracingService, Guid memberPlanId)
        {
            // TODO - Add comments and trace

            //<condition attribute='bpa_transactiondate' operator='ge' value='{ startingWorkMonth.ToShortDateString()}' />
            string fetchxml = $@" 
                    <fetch>
                      <entity name='bpa_transaction' >
                        <all-attributes />
                        <filter type='and' >
                          <condition attribute='statecode' operator='eq' value='0' />
                          <condition attribute='statuscode' operator='eq' value='{(int)TransactionStatus.Posted}' />
                          <condition attribute='bpa_memberplanid' operator='eq' value='{memberPlanId.ToString()}' />
                          <filter type='or' >
                            <condition attribute='bpa_useforcalculation' operator='eq' value='1' />
                            <condition attribute='bpa_transactiontype' operator='in' >
                              <value>{(int)BpaTransactionbpaTransactionType.SecondaryDollarBankAdjustment}</value>
                              <value>{(int)BpaTransactionbpaTransactionType.SecondaryDollarBankDraw}</value>
                            </condition>
                          </filter>
                        </filter>
                        <order attribute='createdon' />
                        <link-entity name='bpa_submission' from='bpa_submissionid' to='bpa_submission' link-type='outer'  >
                          <attribute name='bpa_accountagreementid' />
                          <attribute name='bpa_submissionid' />
                          <attribute name='bpa_agreementid' />
                        </link-entity>
                      </entity>
                    </fetch>
                ";

            EntityCollection transactions = service.RetrieveMultiple(new FetchExpression(fetchxml));
            return transactions;
        }

        public static EntityCollection FetchAllTransactionsForWorkMonth(ITracingService tracingService, EntityCollection transactions, Guid memberPlanId, DateTime workMonth)
        {
            // TODO - Add comments and trace
            EntityCollection returnTransactions = new EntityCollection();

            foreach (Entity transaction in transactions.Entities)
            {
                // Loop though each transaction to find the ones we want
                EntityReference memberPlan = transaction.Contains("bpa_memberplanid") ? transaction.GetAttributeValue<EntityReference>("bpa_memberplanid") : null;
                OptionSetValue stateCode = transaction.GetAttributeValue<OptionSetValue>("statecode");
                DateTime transactionDate = transaction.GetAttributeValue<DateTime>("bpa_transactiondate");
                bool useForCalculation = transaction.GetAttributeValue<bool>("bpa_useforcalculation");
                OptionSetValue transactionType = transaction.GetAttributeValue<OptionSetValue>("bpa_transactiontype");

                // Look for records matching
                if (memberPlan.Id == memberPlanId &&
                    stateCode.Value == 0 &&
                    transactionDate >= workMonth &&
                    transactionDate <= workMonth.AddMonths(1).AddDays(-1) &&
                    useForCalculation == true)
                {
                    // Add it to the returned entity collection
                    returnTransactions.Entities.Add(transaction);
                }
            }
            return returnTransactions;
        }

        public static UpdateRequest SetCurrentEligibility(ITracingService tracingService, Guid memberPlanId, int currentEligibilityValue, DateTime BenefitMonth)
        {
            // TODO - Add comments and trace



            tracingService.Trace("Inside SetCurrentEligibility Method");
            Entity memberPlan = new Entity(PluginHelper.BpaMemberplan)
            {
                Id = memberPlanId,
            };
             
            if (currentEligibilityValue != 0)
                memberPlan["bpa_currenteligibility"] = new OptionSetValue(currentEligibilityValue);

            if (currentEligibilityValue == (int)MemberPlanBpaCurrentEligibility.Inactive)
                memberPlan["bpa_benefitstatus"] = new OptionSetValue((int)MemberPlanBpaMemberPlanStatus.Inactive);
            else if (currentEligibilityValue == (int)MemberPlanBpaCurrentEligibility.TransferOut)
                memberPlan["bpa_benefitstatus"] = new OptionSetValue((int)MemberPlanBpaMemberPlanStatus.ReciprocalTransferOut);
            else
                memberPlan["bpa_benefitstatus"] = new OptionSetValue((int)MemberPlanBpaMemberPlanStatus.Active);

            if (BenefitMonth != DateTime.MinValue)
            {
                // Set Current Month Eligibility
                DateTime calenderMonth = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1);
                BenefitMonth = new DateTime(BenefitMonth.Year, BenefitMonth.Month, 1);

                if (BenefitMonth == calenderMonth)
                {
                    memberPlan["bpa_currentbenefitmonth"] = BenefitMonth;
                    if (currentEligibilityValue != 0)
                        memberPlan["bpa_currentmontheligibility"] = new OptionSetValue(currentEligibilityValue);
                }
            }
            // Add the update to the list of requests
            UpdateRequest updateRequest = new UpdateRequest { Target = memberPlan };
            return updateRequest;
        }

        public static UpdateRequest SetCurrentMonthEligibility(ITracingService tracingService, Guid memberPlanId, int currentEligibilityValue, DateTime BenefitMonth)
        {
            tracingService.Trace("Inside SetCurrentMonthEligibility Method");
            Entity memberPlan = new Entity(PluginHelper.BpaMemberplan)
            {
                Id = memberPlanId,
                ["bpa_currentbenefitmonth"] = BenefitMonth,
            };
            if (currentEligibilityValue != 0)
                memberPlan["bpa_currentmontheligibility"] = new OptionSetValue(currentEligibilityValue);


            UpdateRequest updateRequest = new UpdateRequest { Target = memberPlan };
            return updateRequest;
        }
        
        public static DeleteRequest DeleteTransaction(ITracingService tracingService, Guid transactionId)
        {
            // TODO - Add comments and trace
            EntityReference transactionRef = new EntityReference(PluginHelper.BpaTransaction, transactionId);

            // Add the update to the list of requests
            DeleteRequest deleteRequest = new DeleteRequest { Target = transactionRef };
            return deleteRequest;
        }

        public static EntityCollection FetchAllEligibilityDrawTransactions(ITracingService tracingService, EntityCollection transactions, Guid memberPlanId, DateTime workMonth, int transactionTypeDraw)
        {
            // TODO - Add comments and trace

            EntityCollection returnTransactions = new EntityCollection();

            foreach (Entity transaction in transactions.Entities)
            {
                // Loop though each transaction to find the ones we want
                EntityReference memberPlan = transaction.Contains("bpa_memberplanid") ? transaction.GetAttributeValue<EntityReference>("bpa_memberplanid") : null;
                OptionSetValue stateCode = transaction.GetAttributeValue<OptionSetValue>("statecode");
                DateTime transactionDate = transaction.GetAttributeValue<DateTime>("bpa_transactiondate");
                bool useForCalculation = transaction.GetAttributeValue<bool>("bpa_useforcalculation");
                OptionSetValue transactionType = transaction.GetAttributeValue<OptionSetValue>("bpa_transactiontype");

                // Look for records matching
                if (memberPlan.Id == memberPlanId &&
                    stateCode.Value == 0 &&
                    transactionDate >= workMonth &&
                    transactionDate <= workMonth.AddMonths(1).AddDays(-1) &&
                    transactionType.Value == transactionTypeDraw)
                {
                    // Add it to the returned entity collection
                    returnTransactions.Entities.Add(transaction);
                }
            }
            return returnTransactions;
        }

        public static Entity FetchPreviousMonthEligibility(ITracingService tracingService, EntityCollection transactions, Guid memberPlanId, DateTime workMonth)
        {
            // TODO - Add comments and trace


            foreach (Entity transaction in transactions.Entities)
            {
                // Loop though each transaction to find the ones we want
                EntityReference memberPlan = transaction.Contains("bpa_memberplanid") ? transaction.GetAttributeValue<EntityReference>("bpa_memberplanid") : null;
                OptionSetValue stateCode = transaction.GetAttributeValue<OptionSetValue>("statecode");
                DateTime transactionDate = transaction.GetAttributeValue<DateTime>("bpa_transactiondate");
                OptionSetValue transactionCategory = transaction.Contains("bpa_transactioncategory") ? transaction.GetAttributeValue<OptionSetValue>("bpa_transactioncategory") : new OptionSetValue(0);
                OptionSetValue transactionType = transaction.GetAttributeValue<OptionSetValue>("bpa_transactiontype");

                // Transaction types to look for
                var transactionTypeList = new List<int>
                {
                    (int)BpaTransactionbpaTransactionType.InBenefit,
                    (int)BpaTransactionbpaTransactionType.NotInBenefit,
                    (int)BpaTransactionbpaTransactionType.SelfPayInBenefit,
                    (int)BpaTransactionbpaTransactionType.NewMember,
                    (int)BpaTransactionbpaTransactionType.FundAssistedInBenefit,
                    (int)BpaTransactionbpaTransactionType.UnderReview,
                    (int)BpaTransactionbpaTransactionType.TransferOut,
                    (int)BpaTransactionbpaTransactionType.Frozen,
                    (int)BpaTransactionbpaTransactionType.ReInstating
                };

                // Look for records matching
                if (memberPlan.Id == memberPlanId &&
                    stateCode.Value == 0 &&
                    transactionDate >= workMonth &&
                    transactionDate < workMonth.AddMonths(1) &&
                    transactionCategory.Value == (int)BpaTransactionbpaTransactionCategory.Eligibility &&
                    transactionTypeList.Any(item => item == transactionType.Value))
                {
                    // Return the one we found
                    return transaction;
                }
            }
            return null;
        }


        public static EntityReference FetchCurrentEligibilityCategory(ITracingService tracingService, EntityCollection transactions, Entity memberPlan, DateTime workMonth)
        {
            // TODO - Add comments and trace


            EntityReference eligibilityCategory = null;
            workMonth = new DateTime(workMonth.Year, workMonth.Month, 1);
            DateTime endWorkMonth = new DateTime(workMonth.Year, workMonth.Month, 1).AddMonths(1);

            EntityCollection eligibilityTransactions = new EntityCollection();

            // Check does anythng exists for this workmonth
            foreach (Entity transaction in transactions.Entities)
            {
                // Loop though each transaction to find the ones we want
                EntityReference transactionMemberPlanId =  transaction.Contains("bpa_memberplanid") ? transaction.GetAttributeValue<EntityReference>("bpa_memberplanid") : null;
                OptionSetValue stateCode = transaction.GetAttributeValue<OptionSetValue>("statecode");
                DateTime transactionDate = transaction.GetAttributeValue<DateTime>("bpa_transactiondate");
                
                EntityReference bpa_currenteligibilitycategory = transaction.Contains("bpa_currenteligibilitycategory") ? transaction.GetAttributeValue<EntityReference>("bpa_currenteligibilitycategory") : null;

                // Look for records matching
                if (memberPlan.Id == transactionMemberPlanId.Id &&
                    stateCode.Value == 0 &&
                    transactionDate >= workMonth &&
                    transactionDate < endWorkMonth &&
                    bpa_currenteligibilitycategory != null)
                {
                    // Found one, add it to the collection
                    eligibilityTransactions.Entities.Add(transaction);

                }
            }

            if (eligibilityTransactions != null && eligibilityTransactions.Entities.Count > 0)
            {
                Entity entity = eligibilityTransactions.Entities[0];
                eligibilityCategory = entity.Contains("bpa_currenteligibilitycategory") ? entity.GetAttributeValue<EntityReference>("bpa_currenteligibilitycategory") : null;
                return eligibilityCategory;
            }

            // Check is there anything exists in future?
            if (eligibilityCategory == null)
            {
                EntityCollection futureEligibilityTransactions = new EntityCollection();

                foreach (Entity transaction in transactions.Entities)
                {
                    // Loop though each transaction to find the ones we want
                    EntityReference transactionMemberPlanId = transaction.Contains("bpa_memberplanid") ? transaction.GetAttributeValue<EntityReference>("bpa_memberplanid") : null;
                    OptionSetValue stateCode = transaction.GetAttributeValue<OptionSetValue>("statecode");
                    DateTime transactionDate = transaction.GetAttributeValue<DateTime>("bpa_transactiondate");
                    EntityReference bpa_currenteligibilitycategory = transaction.Contains("bpa_currenteligibilitycategory") ? transaction.GetAttributeValue<EntityReference>("bpa_currenteligibilitycategory") : null;

                    // Look for records matching
                    if (memberPlan.Id == transactionMemberPlanId.Id &&
                        stateCode.Value == 0 &&
                        transactionDate >= workMonth &&
                        bpa_currenteligibilitycategory != null)
                    {
                        // Found one, add it to the collection
                        futureEligibilityTransactions.Entities.Add(transaction);

                    }
                }

                if (futureEligibilityTransactions != null && futureEligibilityTransactions.Entities.Count > 0)
                {
                    Entity entity = futureEligibilityTransactions.Entities[0];
                    eligibilityCategory = entity.Contains("bpa_currenteligibilitycategory") ? entity.GetAttributeValue<EntityReference>("bpa_currenteligibilitycategory") : null;
                    return eligibilityCategory;
                }
            }

            // If no eligibility category exists - take the Member Plan's current elibgiblity category
            if (eligibilityCategory == null)
            {
                eligibilityCategory = memberPlan.Contains("bpa_eligibilitycategoryid") ? memberPlan.GetAttributeValue<EntityReference>("bpa_eligibilitycategoryid") : null;
                return eligibilityCategory;
            }
            return null;
        }
        
        public static UpdateRequest UpdateEligbilityCategory(ITracingService tracingService, Guid transactionId, EntityReference usingEligibilityCategory)
        {
            Entity transaction = new Entity(PluginHelper.BpaTransaction);
            transaction["bpa_currenteligibilitycategory"] = usingEligibilityCategory;
            transaction.Id = transactionId;

            // Add the update to the list of requests
            UpdateRequest updateRequest = new UpdateRequest { Target = transaction };
            return updateRequest;
        }


        public static EntityCollection FetchAllEligibilityCategoryByPlan(IOrganizationService service, ITracingService tracingService, Guid planId)
        {
            // TODO - Add comments and trace




            string fetchXml = $@"
                    <fetch mapping='logical' output-format='xml-platform' version='1.0' >
                      <entity name='bpa_eligibilitycategory' >
                        <attribute name='bpa_name' />
                        <attribute name='bpa_eligibilityoffsetmonths' />
                        <attribute name='bpa_maxconsecutivemonthsselfpay' />
                        <attribute name='bpa_planeligibilitytype' />
                        <attribute name='bpa_terminationcycle' />
                        <filter>
                          <condition attribute='statecode' operator='eq' value='0' />
                          <condition attribute='bpa_sector' operator='eq' value='{planId}' />
                        </filter>
                      </entity>
                    </fetch>
                ";

            EntityCollection eligibilityCategories = service.RetrieveMultiple(new FetchExpression(fetchXml));
            return eligibilityCategories;
        }
        

        public static EntityReference FetchLastContributionAgreement(ITracingService tracingService, EntityCollection transactions, Guid memberPlanId, DateTime workMonth)
        {
            // TODO - Add comments and trace




            Entity qualifiedTransaction = null;
            foreach (Entity transaction in transactions.Entities)
            {
                // Loop though each transaction to find the ones we want
                EntityReference memberPlan = transaction.GetAttributeValue<EntityReference>("bpa_memberplanid");
                OptionSetValue stateCode = transaction.GetAttributeValue<OptionSetValue>("statecode");
                DateTime transactionDate = transaction.GetAttributeValue<DateTime>("bpa_transactiondate");
                Boolean useForCalculation = transaction.GetAttributeValue<Boolean>("bpa_useforcalculation");
                OptionSetValue transactionType = transaction.GetAttributeValue<OptionSetValue>("bpa_transactiontype");

                // Look for records matching
                if (memberPlan.Id == memberPlanId &&
                    stateCode.Value == 0 &&
                    transactionDate <= workMonth &&
                    transactionType.Value == (int)BpaTransactionbpaTransactionType.EmployerContribution)
                {
                    // Add it to the returned entity collection
                    if (qualifiedTransaction == null ||
                        transaction.GetAttributeValue<DateTime>("bpa_transactiondate") > qualifiedTransaction.GetAttributeValue<DateTime>("bpa_transactiondate"))
                        qualifiedTransaction = transaction;
                }
            }

            // If one was found, return it
            if (qualifiedTransaction != null && qualifiedTransaction.Contains("bpa_submission1.bpa_agreementid") == true)
                return (EntityReference)qualifiedTransaction.GetAttributeValue<AliasedValue>("bpa_submission1.bpa_agreementid").Value;
            else
                return null;
        }



        public static Dictionary<string, decimal> FetchBankBalances(EntityCollection transactions, ITracingService tracingService, Guid contactId,
            DateTime benefitMonth, Guid defaultFundId)
        {
            Dictionary<string, decimal> endingBalance = new Dictionary<string, decimal>();
                        
            decimal hours = 0;
            hours = transactions.Entities.Where(x => (x.GetAttributeValue<DateTime>("bpa_transactiondate") <= benefitMonth.AddMonths(1).AddDays(-1))
                && (x.Contains("bpa_hours") && x.GetAttributeValue<decimal>("bpa_hours") != 0)
                && ((x.GetAttributeValue<OptionSetValue>("bpa_transactiontype").Value != (int)BpaTransactionbpaTransactionType.SelfPay)
                    || (x.GetAttributeValue<OptionSetValue>("bpa_transactiontype").Value != (int)BpaTransactionbpaTransactionType.SelfPayBankAdjustment)
                    || (x.GetAttributeValue<OptionSetValue>("bpa_transactiontype").Value != (int)BpaTransactionbpaTransactionType.SelfPayDeposit)
                    || (x.GetAttributeValue<OptionSetValue>("bpa_transactiontype").Value != (int)BpaTransactionbpaTransactionType.SelfPayDraw)
                    || (x.GetAttributeValue<OptionSetValue>("bpa_transactiontype").Value != (int)BpaTransactionbpaTransactionType.SelfPayInBenefit)
                    || (x.GetAttributeValue<OptionSetValue>("bpa_transactiontype").Value != (int)BpaTransactionbpaTransactionType.SelfPayMaxConsecutiveReach)
                    || (x.GetAttributeValue<OptionSetValue>("bpa_transactiontype").Value != (int)BpaTransactionbpaTransactionType.SelfPayRefund))).Sum(x => x.GetAttributeValue<decimal>("bpa_hours"));

            decimal dollarhours = 0;
            dollarhours = transactions.Entities.Where(x => ((x.GetAttributeValue<DateTime>("bpa_transactiondate") <= benefitMonth.AddMonths(1).AddDays(-1))
                && (x.Contains("bpa_dollarhour") && x.GetAttributeValue<Money>("bpa_dollarhour") != null)
                && ((x.GetAttributeValue<OptionSetValue>("bpa_transactiontype").Value != (int)BpaTransactionbpaTransactionType.SelfPay)
                    && (x.GetAttributeValue<OptionSetValue>("bpa_transactiontype").Value != (int)BpaTransactionbpaTransactionType.SelfPayBankAdjustment)
                    && (x.GetAttributeValue<OptionSetValue>("bpa_transactiontype").Value != (int)BpaTransactionbpaTransactionType.SelfPayDeposit)
                    && (x.GetAttributeValue<OptionSetValue>("bpa_transactiontype").Value != (int)BpaTransactionbpaTransactionType.SelfPayDraw)
                    && (x.GetAttributeValue<OptionSetValue>("bpa_transactiontype").Value != (int)BpaTransactionbpaTransactionType.SelfPayInBenefit)
                    && (x.GetAttributeValue<OptionSetValue>("bpa_transactiontype").Value != (int)BpaTransactionbpaTransactionType.SelfPayMaxConsecutiveReach)
                    && (x.GetAttributeValue<OptionSetValue>("bpa_transactiontype").Value != (int)BpaTransactionbpaTransactionType.SelfPayRefund)
                    && (x.GetAttributeValue<OptionSetValue>("bpa_transactiontype").Value != (int)BpaTransactionbpaTransactionType.SecondaryDollarBankAdjustment)
                    && (x.GetAttributeValue<OptionSetValue>("bpa_transactiontype").Value != (int)BpaTransactionbpaTransactionType.SecondaryDollarBankDraw) ))).Sum(x => x.GetAttributeValue<Money>("bpa_dollarhour").Value);

            decimal selfpayDollar = 0;
            selfpayDollar = transactions.Entities.Where(x => ((x.GetAttributeValue<DateTime>("bpa_transactiondate") <= benefitMonth.AddMonths(1).AddDays(-1))
                && (x.Contains("bpa_dollarhour") && x.GetAttributeValue<Money>("bpa_dollarhour") != null)
                && ((x.GetAttributeValue<OptionSetValue>("bpa_transactiontype").Value == (int)BpaTransactionbpaTransactionType.SelfPay)
                || (x.GetAttributeValue<OptionSetValue>("bpa_transactiontype").Value == (int)BpaTransactionbpaTransactionType.SelfPayBankAdjustment)
                || (x.GetAttributeValue<OptionSetValue>("bpa_transactiontype").Value == (int)BpaTransactionbpaTransactionType.SelfPayDeposit)
                || (x.GetAttributeValue<OptionSetValue>("bpa_transactiontype").Value == (int)BpaTransactionbpaTransactionType.SelfPayDraw)
                || (x.GetAttributeValue<OptionSetValue>("bpa_transactiontype").Value == (int)BpaTransactionbpaTransactionType.SelfPayInBenefit)
                || (x.GetAttributeValue<OptionSetValue>("bpa_transactiontype").Value == (int)BpaTransactionbpaTransactionType.SelfPayMaxConsecutiveReach)
                || (x.GetAttributeValue<OptionSetValue>("bpa_transactiontype").Value == (int)BpaTransactionbpaTransactionType.SelfPayRefund)))).Sum(x => x.GetAttributeValue<Money>("bpa_dollarhour").Value);

            decimal secondaryDollar = 0;
            secondaryDollar = transactions.Entities.Where(x => ((x.GetAttributeValue<DateTime>("bpa_transactiondate") <= benefitMonth.AddMonths(1).AddDays(-1))
                && (x.Contains("bpa_dollarhour") && x.GetAttributeValue<Money>("bpa_dollarhour") != null)
                && ((x.GetAttributeValue<OptionSetValue>("bpa_transactiontype").Value == (int)BpaTransactionbpaTransactionType.SecondaryDollarBankAdjustment)
                || (x.GetAttributeValue<OptionSetValue>("bpa_transactiontype").Value == (int)BpaTransactionbpaTransactionType.SecondaryDollarBankDraw)))).Sum(x => x.GetAttributeValue<Money>("bpa_dollarhour").Value);

            endingBalance.Add("hours", hours);
            endingBalance.Add("dollarhours", dollarhours);
            endingBalance.Add("selfpaydollar", selfpayDollar);
            endingBalance.Add("secondarydollar", secondaryDollar);

            return endingBalance;

        }
        
        public static Dictionary<string, decimal> UpdateBankBalancesWithTransactions(IOrganizationService service, ITracingService tracingService, EntityCollection transactions,
             Dictionary<string, decimal> bankBalances, Guid memberPlanId, Guid defaultFundId, DateTime startPeriod, DateTime endPeriod, int planEligibilityType)
        {
            // TODO - Add comments and trace



            Dictionary<string, decimal> newBankBalances = bankBalances;

            // Add any values that aren't in bankBalances
            if (newBankBalances.ContainsKey("hours") == false)
                newBankBalances.Add("hours", 0);
            if (newBankBalances.ContainsKey("dollarhours") == false)
                newBankBalances.Add("dollarhours", 0);
            if (newBankBalances.ContainsKey("selfpaydollar") == false)
                newBankBalances.Add("selfpaydollar", 0);
            if (newBankBalances.ContainsKey("secondarydollar") == false)
                newBankBalances.Add("secondarydollar", 0);

            //Filtered Transactions
            // for that month
            List<Entity> transactionList = transactions.Entities.Where(x => ((x.GetAttributeValue<DateTime>("bpa_transactiondate") >= startPeriod)
                   && (x.GetAttributeValue<DateTime>("bpa_transactiondate") <= endPeriod))).ToList();

            foreach (Entity transaction in transactionList)
            {
                // Loop though each transaction to find the ones we want
                EntityReference memberPlan = transaction.GetAttributeValue<EntityReference>("bpa_memberplanid");
                OptionSetValue stateCode = transaction.GetAttributeValue<OptionSetValue>("statecode");
                DateTime transactionDate = transaction.GetAttributeValue<DateTime>("bpa_transactiondate");
                Boolean useForCalculation = transaction.GetAttributeValue<Boolean>("bpa_useforcalculation");
                OptionSetValue transactionType = transaction.GetAttributeValue<OptionSetValue>("bpa_transactiontype");
                EntityReference fund = transaction.GetAttributeValue<EntityReference>("bpa_fund");

                // Transaction types to look for
                var transactionTypeList = new List<int>
                {
                    (int)BpaTransactionbpaTransactionType.EmployerContribution,
                    (int)BpaTransactionbpaTransactionType.HourBankAdjustment,
                    (int)BpaTransactionbpaTransactionType.DollarBankAdjustment,
                    (int)BpaTransactionbpaTransactionType.SecondaryDollarBankAdjustment,
                    (int)BpaTransactionbpaTransactionType.SecondaryDollarBankDraw,
                    (int)BpaTransactionbpaTransactionType.SelfPayBankAdjustment,
                    (int)BpaTransactionbpaTransactionType.SelfPayDeposit,
                    (int)BpaTransactionbpaTransactionType.SelfPayDraw,
                    (int)BpaTransactionbpaTransactionType.SelfPayRefund,
                    (int)BpaTransactionbpaTransactionType.EligibilityDraw,
                    (int)BpaTransactionbpaTransactionType.Reciprocal
                };

                // Look for records matching
                if (memberPlan.Id == memberPlanId &&
                    stateCode.Value == 0 &&
                    //transactionDate >= startPeriod &&
                    //transactionDate <= endPeriod &&
                    defaultFundId == (fund == null ? Guid.Empty : fund.Id) &&
                    transactionTypeList.Any(item => item == transactionType.Value))
                {
                    // Figure out which bank to update and
                    // add the value to the bank balance


                    switch (transactionType.Value)
                    {
                        // SelfPay Bank Update
                        case (int)BpaTransactionbpaTransactionType.SelfPayBankAdjustment:
                        case (int)BpaTransactionbpaTransactionType.SelfPayDeposit:
                        case (int)BpaTransactionbpaTransactionType.SelfPayDraw:
                        case (int)BpaTransactionbpaTransactionType.SelfPayRefund:

                            if (transaction.Contains("bpa_dollarhour") == true && transaction.GetAttributeValue<Money>("bpa_dollarhour").Value != 0)
                                newBankBalances["selfpaydollar"] += transaction.GetAttributeValue<Money>("bpa_dollarhour").Value;
                            break;

                        // Hour & Dollar Bank Update
                        case (int)BpaTransactionbpaTransactionType.EmployerContribution:
                        case (int)BpaTransactionbpaTransactionType.EligibilityDraw:
                        case (int)BpaTransactionbpaTransactionType.Reciprocal: // Transfer 

                            if (transaction.Contains("bpa_hours") == true && transaction.GetAttributeValue<Decimal>("bpa_hours") != 0)
                                newBankBalances["hours"] += transaction.GetAttributeValue<Decimal>("bpa_hours");

                            if (transaction.Contains("bpa_dollarhour") == true && transaction.GetAttributeValue<Money>("bpa_dollarhour").Value != 0)
                            {
                                // Needed to filter secondary bank and regular bank dollars
                                if (useForCalculation == true)
                                    newBankBalances["dollarhours"] += transaction.GetAttributeValue<Money>("bpa_dollarhour").Value;
                                else
                                    newBankBalances["secondarydollar"] += transaction.GetAttributeValue<Money>("bpa_dollarhour").Value;
                            }
                            break;

                        // Hour Bank Update
                        case (int)BpaTransactionbpaTransactionType.HourBankAdjustment:

                            if (transaction.Contains("bpa_hours") == true && transaction.GetAttributeValue<Decimal>("bpa_hours") != 0)
                                newBankBalances["hours"] += transaction.GetAttributeValue<Decimal>("bpa_hours");
                            break;

                        // Dollar Bank Update
                        case (int)BpaTransactionbpaTransactionType.DollarBankAdjustment:

                            if (transaction.Contains("bpa_dollarhour") == true && transaction.GetAttributeValue<Money>("bpa_dollarhour").Value != 0)
                                newBankBalances["dollarhours"] += transaction.GetAttributeValue<Money>("bpa_dollarhour").Value;
                            break;

                        // Secondary Dollar Bank Update
                        case (int)BpaTransactionbpaTransactionType.SecondaryDollarBankAdjustment:

                            if (transaction.Contains("bpa_dollarhour") == true && transaction.GetAttributeValue<Money>("bpa_dollarhour").Value != 0)
                                newBankBalances["secondarydollar"] += transaction.GetAttributeValue<Money>("bpa_dollarhour").Value;
                            break;
                            
                    }
                }
            }
            return newBankBalances;
        }

        public static EntityCollection FetchAllEligibilityDetails(IOrganizationService service, ITracingService tracingService, Guid planId)
        {
            // TODO - Add comments and trace


            string fetchxml = $@"
                <fetch>
                  <entity name='bpa_eligibilitycategorydetail' >
                    <all-attributes />
                    <filter>
                      <condition attribute='statecode' operator='eq' value='0' />
                    </filter>
                    <order attribute='bpa_effectivedate' descending='true' />
                    <link-entity name='bpa_eligibilitycategory' from='bpa_eligibilitycategoryid' to='bpa_eligibilitycategory' >
                      <filter>
                        <condition attribute='bpa_sector' operator='eq' value='{planId}' />
                        <condition attribute='statecode' operator='eq' value='0' />
                      </filter>
                    </link-entity>
                  </entity>
                </fetch>";

            EntityCollection detailCollection = service.RetrieveMultiple(new FetchExpression(fetchxml));
            return detailCollection;
        }

        public static Entity FetchEligibilityDetail(ITracingService tracingService, 
            EntityCollection eligibilityCategoryDetails, Guid eligibilityCategoryId,
            DateTime workMonth)
        {
            // TODO - Add comments and trace




            foreach (Entity eligibilityCategoryDetail in eligibilityCategoryDetails.Entities)
            {
                // Loop though each transaction to find the ones we want
                EntityReference eligibilityCategory = eligibilityCategoryDetail.Contains("bpa_eligibilitycategory") ? eligibilityCategoryDetail.GetAttributeValue<EntityReference>("bpa_eligibilitycategory") : null;
                OptionSetValue stateCode = eligibilityCategoryDetail.GetAttributeValue<OptionSetValue>("statecode");
                DateTime effectiveDate = eligibilityCategoryDetail.GetAttributeValue<DateTime>("bpa_effectivedate");

                // Look for records matching
                if (eligibilityCategory.Id == eligibilityCategoryId &&
                    stateCode.Value == 0 &&
                    effectiveDate <= workMonth)
                {
                    // Return the one we found since it is order by effective date
                    return eligibilityCategoryDetail;
                }
            }
            return null;
        }

        public static DateTime FetchMaxEmployerContributionDate(
            ITracingService tracingService, EntityCollection transactions,
            Guid memberPlanId, DateTime workMonth)
        {
            // TODO - Add comments and trace



            DateTime maxDate = DateTime.MinValue;
            foreach (Entity transaction in transactions.Entities)
            {
                // Loop though each transaction to find the ones we want
                EntityReference memberPlan = transaction.GetAttributeValue<EntityReference>("bpa_memberplanid");
                OptionSetValue stateCode = transaction.GetAttributeValue<OptionSetValue>("statecode");
                DateTime transactionDate = transaction.GetAttributeValue<DateTime>("bpa_transactiondate");
                Boolean useForCalculation = transaction.GetAttributeValue<Boolean>("bpa_useforcalculation");
                OptionSetValue transactionType = transaction.GetAttributeValue<OptionSetValue>("bpa_transactiontype");

                // Transaction types to look for
                var transactionTypeList = new List<int>
                {
                    (int)BpaTransactionbpaTransactionType.EmployerContribution,
                    (int)BpaTransactionbpaTransactionType.HourBankAdjustment,
                    (int)BpaTransactionbpaTransactionType.DollarBankAdjustment
                };

                // Look for records matching
                if (memberPlan.Id == memberPlanId &&
                    stateCode.Value == 0 &&
                    transactionDate <= workMonth &&
                    transactionTypeList.Any(item => item == transactionType.Value))
                {
                    // Return the one we found
                    // Add it to the returned entity collection
                    if (maxDate == DateTime.MinValue ||
                        transaction.GetAttributeValue<DateTime>("bpa_transactiondate") > maxDate)
                        maxDate = transaction.GetAttributeValue<DateTime>("bpa_transactiondate");
                }
            }
            if (maxDate == DateTime.MinValue)
                maxDate = workMonth;
            return maxDate;
        }




        public static int GetHowManySelfPay(ITracingService tracingService,
            EntityCollection transactions, DateTime lastEmployerContributionMonth,
            DateTime workMonth, Guid memberPlanId)
        {
            // TODO - Add comments and trace



            int countSelfPay = 0;
            foreach (Entity transaction in transactions.Entities)
            {
                // Loop though each transaction to find the ones we want
                EntityReference memberPlan = transaction.GetAttributeValue<EntityReference>("bpa_memberplanid");
                OptionSetValue stateCode = transaction.GetAttributeValue<OptionSetValue>("statecode");
                DateTime transactionDate = transaction.GetAttributeValue<DateTime>("bpa_transactiondate");
                Boolean useForCalculation = transaction.GetAttributeValue<Boolean>("bpa_useforcalculation");
                OptionSetValue transactionType = transaction.GetAttributeValue<OptionSetValue>("bpa_transactiontype");

                // Transaction types to look for
                var transactionTypeList = new List<int>
                {
                    (int)BpaTransactionbpaTransactionType.SelfPayInBenefit
                };

                if (memberPlan.Id == memberPlanId &&
                    stateCode.Value == 0 &&
                    transactionDate >= lastEmployerContributionMonth &&
                    transactionDate < workMonth &&
                    transactionTypeList.Any(item => item == transactionType.Value))
                {
                    countSelfPay++;
                }
            }
            return countSelfPay;
        }

        public static bool IsCategoryReinstatement(IOrganizationService service, ITracingService tracingService,
            EntityCollection eligibilityCategories, Guid usingEligibilityCategoryId)
        {
            // TODO - Add comments and trace

            bool isReinstatment = false;
            //Entity eligibilityCategory = eligibilityCategories.Entities.Where(x => x.Id == usingEligibilityCategoryId).ToList().FirstOrDefault();
            Entity eligibilityCategory = service.Retrieve("bpa_eligibilitycategory", usingEligibilityCategoryId, new ColumnSet(new string[] { "bpa_reinstatement" }));
            if (eligibilityCategory != null && eligibilityCategory.Contains("bpa_reinstatement"))
                isReinstatment = eligibilityCategory.GetAttributeValue<bool>("bpa_reinstatement");

            return isReinstatment;
        }

        public static decimal GetReinstatementHours(ITracingService tracingService,
            EntityCollection eligibilityCategories, Guid usingEligibilityCategoryId,
            DateTime effectiveDate)
        {
            decimal reinstatementHours = 0;
            Entity eligibilityCategory = eligibilityCategories.Entities.Where(x => x.Id == usingEligibilityCategoryId).ToList().FirstOrDefault();

            if (eligibilityCategory != null && eligibilityCategory.Contains("bpa_reinstatementhours"))
                reinstatementHours = eligibilityCategory.GetAttributeValue<decimal>("bpa_reinstatementhours");

            return reinstatementHours;
        }

        public static Entity GetReinstatementHours(IOrganizationService service, Guid eligibilityCategoryId,
            DateTime effectiveDate)
        {
            Entity detail = null;
            string fetchxml = $@"
                    <fetch count='1' > 
                      <entity name='bpa_eligibilitycategorydetail' >
                        <attribute name='bpa_eligibilitycategorydetailid' />
                        <attribute name='bpa_eligibilitycategory' />
                        <attribute name='bpa_name' />
                        <attribute name='bpa_effectivedate' />
                        <attribute name='bpa_expirationdate' />
                        <attribute name='bpa_deductionrate' />
                        <attribute name='bpa_minimumeligibility' />
                        <attribute name='bpa_maximummemberbank' />
                        <attribute name='bpa_fundassistanceamount' />
                        <attribute name='bpa_reinstatementdollar' />
                        <attribute name='bpa_selfpayamount' />
                        
                        <attribute name='bpa_hoursdeductionrate' />
                        <attribute name='bpa_hoursminimuminitialeligibility' />
                        <attribute name='bpa_hoursmaxmemberbank' />
                        <attribute name='bpa_hoursfundassistanceamount' />
                        <attribute name='bpa_reinstatementhours' />

                        <filter type='and' >
                          <condition attribute='statecode' operator='eq' value='0' />
                          <condition attribute='bpa_eligibilitycategory' operator='eq' value='{eligibilityCategoryId}' />
                          <condition attribute='bpa_effectivedate' operator='le' value='{effectiveDate.ToShortDateString()}' />
                        </filter>
                        <order attribute='bpa_effectivedate' descending='true' />
                      </entity>
                    </fetch>";
            EntityCollection detailCollection = service.RetrieveMultiple(new FetchExpression(fetchxml));

            if (detailCollection != null && detailCollection.Entities.Count > 0)
            {
                detail = detailCollection.Entities[0];
            }
            return detail;
        }
        public static decimal GetReinstatementDollars(ITracingService tracingService,
            EntityCollection eligibilityCategories, Guid usingEligibilityCategoryId,
            DateTime effectiveDate)
        {
            decimal reinstatementDollars = 0;
            Entity eligibilityCategory = eligibilityCategories.Entities.Where(x => x.Id == usingEligibilityCategoryId).ToList().FirstOrDefault();

            if (eligibilityCategory != null && eligibilityCategory.Contains("bpa_reinstatementdollar"))
                reinstatementDollars = eligibilityCategory.GetAttributeValue<Money>("bpa_reinstatementdollar").Value;

            return reinstatementDollars;
        }


        public static bool IsReserveExists(ITracingService tracingService, EntityCollection transactions,
            Guid memberPlanId, DateTime workMonth)
        {
            // TODO - Add comments and trace



            foreach (Entity transaction in transactions.Entities)
            {
                // Loop though each transaction to find the ones we want
                EntityReference memberPlan = transaction.GetAttributeValue<EntityReference>("bpa_memberplanid");
                OptionSetValue stateCode = transaction.GetAttributeValue<OptionSetValue>("statecode");
                DateTime transactionDate = transaction.GetAttributeValue<DateTime>("bpa_transactiondate");
                OptionSetValue transactionType = transaction.GetAttributeValue<OptionSetValue>("bpa_transactiontype");

                // Transaction types to look for
                var transactionTypeList = new List<int>
                {
                    (int)BpaTransactionbpaTransactionType.ReserveTransfer
                };

                // Look for records matching
                if (memberPlan.Id == memberPlanId &&
                    stateCode.Value == 0 &&
                    transactionDate == workMonth &&
                    transactionTypeList.Any(item => item == transactionType.Value))
                {
                    // Found one
                    return true;
                }
            }
            return false;
        }




        public static Entity FetchLastEligiblityMonthExceptSelfPay(ITracingService tracingService, EntityCollection transactions,
            Guid memberPlanId, DateTime workMonth, DateTime lastEmployerContributionMonth)
        {
            // TODO - Add comments and trace



            Entity returnTransaction = null;
            foreach (Entity transaction in transactions.Entities)
            {
                // Loop though each transaction to find the ones we want
                EntityReference memberPlan = transaction.GetAttributeValue<EntityReference>("bpa_memberplanid");
                OptionSetValue stateCode = transaction.GetAttributeValue<OptionSetValue>("statecode");
                DateTime transactionDate = transaction.GetAttributeValue<DateTime>("bpa_transactiondate");
                Boolean useForCalculation = transaction.GetAttributeValue<Boolean>("bpa_useforcalculation");
                OptionSetValue transactionType = transaction.GetAttributeValue<OptionSetValue>("bpa_transactiontype");

                // Transaction types to look for
                var transactionTypeList = new List<int>
                {
                    (int)BpaTransactionbpaTransactionType.InBenefit,
                    (int)BpaTransactionbpaTransactionType.NotInBenefit,
                    (int)BpaTransactionbpaTransactionType.NewMember,
                    (int)BpaTransactionbpaTransactionType.FundAssistedInBenefit,
                    (int)BpaTransactionbpaTransactionType.UnderReview,
                    (int)BpaTransactionbpaTransactionType.ReInstating
                };

                // Look for records matching
                if (memberPlan.Id == memberPlanId &&
                    stateCode.Value == 0 &&
                    transactionDate > lastEmployerContributionMonth &&
                    transactionDate < workMonth &&
                    transactionTypeList.Any(item => item == transactionType.Value))
                {
                    // Return the one we found
                    // Add it to the returned entity
                    if (returnTransaction == null ||
                        transaction.GetAttributeValue<DateTime>("bpa_transactiondate") > returnTransaction.GetAttributeValue<DateTime>("bpa_transactiondate"))
                        returnTransaction = transaction;
                }
            }
            return returnTransaction;
            //Entity entity = null;
            //string fetchxml = @"
            //        <fetch top='1' >
            //          <entity name='bpa_transaction' >
            //            <attribute name='bpa_transactiondate' />
            //            <attribute name='bpa_transactionid' />
            //            <filter type='and' >
            //              <condition attribute='bpa_memberplanid' operator='eq' value='" + memberPlanId.ToString() + @"' />
            //              <condition attribute='bpa_transactiondate' operator='gt' value='" + lastEmployerContributionMonth.ToShortDateString() + @"' />
            //              <condition attribute='bpa_transactiondate' operator='lt' value='" + workMonth.ToShortDateString() + @"' />
            //              <condition attribute='statecode' operator='eq' value='0' />
            //              <condition attribute='bpa_transactiontype' operator='in' >
            //                <value>" + ((int)BpaTransactionbpaTransactionType.InBenefit).ToString() + @"</value>
            //                <value>" + ((int)BpaTransactionbpaTransactionType.NotInBenefit).ToString() + @"</value>
            //                <value>" + ((int)BpaTransactionbpaTransactionType.NewMember).ToString() + @"</value>
            //                <value>" + ((int)BpaTransactionbpaTransactionType.FundAssistedInBenefit).ToString() + @"</value>
            //                <value>" + ((int)BpaTransactionbpaTransactionType.UnderReview).ToString() + @"</value>
            //                <value>" + ((int)BpaTransactionbpaTransactionType.ReInstating).ToString() + @"</value>
            //              </condition>
            //            </filter>
            //            <order attribute='bpa_transactiondate' descending='true' />
            //          </entity>
            //        </fetch>
            //    ";
            //EntityCollection transactions1 = service.RetrieveMultiple(new FetchExpression(fetchxml));

            //if (transactions1 != null && transactions1.Entities.Count > 0)
            //{
            //    entity = transactions1.Entities[0];
            //}
            //return entity;
        }

        public static int GetMaxConsecutiveMonthsSelfPay(ITracingService tracingService, 
            EntityCollection eligibilityCategories, Guid usingEligibilityCategoryId)
        {
            // TODO - Add comments and trace



            int maxSelfPay = 0;
            Entity eligibilityCategory = eligibilityCategories.Entities.Where(x => x.Id == usingEligibilityCategoryId).ToList().FirstOrDefault();

            if (eligibilityCategory != null && eligibilityCategory.Attributes.Contains("bpa_maxconsecutivemonthsselfpay"))
                maxSelfPay = eligibilityCategory.GetAttributeValue<int>("bpa_maxconsecutivemonthsselfpay");

            return maxSelfPay;
        }

        public static CreateRequest CreateFundAssistedTransactions(ITracingService tracingService, Guid memberId, Dictionary<string, decimal> bankBalances,
            DateTime workMonth, DateTime benefitMonth, Money dollarHours, decimal? hour, int transactionCategory, int transactionType,
            EntityReference fund, EntityReference eligibilityCategory, Guid defaultFundId, EntityReference lastAgreement, EntityReference owningUserTeamRef
            ,int planBaseOn)
        {
            // TODO - Add comments and trace


            // Fund Assisted
            Entity transaction = new Entity(PluginHelper.BpaTransaction);
            transaction["bpa_memberplanid"] = new EntityReference(PluginHelper.BpaMemberplan, memberId);
            transaction["bpa_transactiondate"] = workMonth;
            if (benefitMonth.Day > 1)
                benefitMonth = new DateTime(benefitMonth.Year, benefitMonth.Month, 1);

            transaction["bpa_benefitmonth"] = benefitMonth;

            if (hour != null)
                transaction["bpa_hours"] = hour.Value;

            if (dollarHours != null)
                transaction["bpa_dollarhour"] = dollarHours;
            transaction["bpa_transactioncategory"] = new OptionSetValue(transactionCategory);
            transaction["bpa_transactiontype"] = new OptionSetValue(transactionType);

            if ((fund != null) && (fund.Id != Guid.Empty))
                transaction[PluginHelper.BpaFund] = fund;

            if (eligibilityCategory != null)
            {
                transaction["bpa_currenteligibilitycategory"] = eligibilityCategory;
            }

            if (transactionType == (int)BpaTransactionbpaTransactionType.FundAssistedInBenefit)
            {
                if (planBaseOn == (int)BpaEligibilityCategoryPlanEligiblityType.Hours)
                {
                    if (bankBalances.ContainsKey("hours"))
                        transaction["bpa_endinghourbalance"] = bankBalances["hours"];
                }
                else if (planBaseOn == (int)BpaEligibilityCategoryPlanEligiblityType.Dollar)
                {
                    if (bankBalances.ContainsKey("dollarhours"))
                        transaction["bpa_endingdollarbalance"] = bankBalances["dollarhours"];

                    if (bankBalances.ContainsKey("secondarydollar"))
                        transaction["bpa_secondarydollarbalance"] = new Money(bankBalances["secondarydollar"]);
                }
                if (bankBalances.ContainsKey("selfpaydollar"))
                    transaction["bpa_endingselfpaybalance"] = new Money(bankBalances["selfpaydollar"]);
               
            }
            transaction["bpa_useforcalculation"] = true;
            transaction["bpa_name"] = Guid.NewGuid().ToString();
            transaction["ownerid"] = owningUserTeamRef;
            if (lastAgreement != null)
                transaction["bpa_subsectorid"] = lastAgreement;

            // Return the Create Request
            CreateRequest createRequest = new CreateRequest { Target = transaction};
            return createRequest;

        }

        // Reference 1 of 2
        public static CreateRequest CreateTransaction(ITracingService tracingService, Guid memberPlanId, Dictionary<string, decimal> endingBalances, DateTime workMonth,
            DateTime benefitMonth, EntityReference fund, int transactionCategory, int transactionType, decimal dollarHours, bool isEligibilityTransaction,
            EntityReference eligibilityCategory, Guid defaultFundId, EntityReference lastAgreement, EntityReference owningUserTeamRef, int planBasedOn)
        {
            // TODO - Add comments and trace




            Guid transactionId = Guid.Empty;
            //Dictionary<string, decimal> endingBalances = FetchEndingBalance(service, memberId, workMonth, defaultFundId);

            Entity transaction = new Entity(PluginHelper.BpaTransaction);
            transaction["bpa_memberplanid"] = new EntityReference(PluginHelper.BpaMemberplan, memberPlanId);
            transaction["bpa_transactiondate"] = workMonth;
            if (benefitMonth.Day > 1)
                benefitMonth = new DateTime(benefitMonth.Year, benefitMonth.Month, 1);
            transaction["bpa_benefitmonth"] = benefitMonth;
            if ((fund != null) && (fund.Id != Guid.Empty))
                transaction[PluginHelper.BpaFund] = fund;
            transaction["bpa_transactioncategory"] = new OptionSetValue(transactionCategory);
            transaction["bpa_transactiontype"] = new OptionSetValue(transactionType);
            if (dollarHours != 0)
                transaction["bpa_dollarhour"] = new Money(dollarHours);
            if (isEligibilityTransaction)
            {
                if (planBasedOn == (int)BpaEligibilityCategoryPlanEligiblityType.Hours)
                {
                    if (endingBalances.ContainsKey("hours"))
                        transaction["bpa_endinghourbalance"] = endingBalances["hours"];
                }
                else if (planBasedOn == (int)BpaEligibilityCategoryPlanEligiblityType.Dollar)
                {
                    if (endingBalances.ContainsKey("dollarhours"))
                        transaction["bpa_endingdollarbalance"] = endingBalances["dollarhours"];
                    if (endingBalances.ContainsKey("secondarydollar"))
                        transaction["bpa_secondarydollarbalance"] = new Money(endingBalances["secondarydollar"]);
                }
                if (endingBalances.ContainsKey("selfpaydollar"))
                    transaction["bpa_endingselfpaybalance"] = new Money(endingBalances["selfpaydollar"]);

            }
            if (eligibilityCategory != null)
                transaction["bpa_currenteligibilitycategory"] = eligibilityCategory;
            if ((transactionType == (int)BpaTransactionbpaTransactionType.SecondaryDollarBankAdjustment) ||
                (transactionType == (int)BpaTransactionbpaTransactionType.SecondaryDollarBankDraw))
                transaction["bpa_useforcalculation"] = false;
            else
                transaction["bpa_useforcalculation"] = true;

            transaction["bpa_name"] = Guid.NewGuid().ToString();
            transaction["ownerid"] = owningUserTeamRef;
            if (lastAgreement != null)
                transaction["bpa_subsectorid"] = lastAgreement;

            // Return the Create Request
            CreateRequest createRequest = new CreateRequest { Target = transaction};
            return createRequest;
        }

        public static CreateRequest CreateUnderReviewTransaction(ITracingService tracingService, Guid memberId, Dictionary<string, decimal> endingBalances,
            EntityReference eligibilityCategory, DateTime workMonth, DateTime benefitMonth, int transactionCategory,
            int transactionType, Guid defaultFundId, EntityReference lastAgreement, EntityReference owningUserTeamRef, int planBasedOn)
        {
            // TODO - Add comments and trace



            Entity transaction = new Entity(PluginHelper.BpaTransaction);
            transaction["bpa_memberplanid"] = new EntityReference(PluginHelper.BpaMemberplan, memberId);
            transaction["bpa_transactiondate"] = workMonth;
            if (benefitMonth.Day > 1)
                benefitMonth = new DateTime(benefitMonth.Year, benefitMonth.Month, 1);

            transaction["bpa_benefitmonth"] = benefitMonth;
            transaction["bpa_transactioncategory"] = new OptionSetValue(transactionCategory);
            transaction["bpa_transactiontype"] = new OptionSetValue(transactionType);
            if (planBasedOn == (int)BpaEligibilityCategoryPlanEligiblityType.Hours)
            {
                if (endingBalances.ContainsKey("hours"))
                    transaction["bpa_endinghourbalance"] = endingBalances["hours"];
            }
            else if (planBasedOn == (int)BpaEligibilityCategoryPlanEligiblityType.Dollar)
            {
                if (endingBalances.ContainsKey("dollarhours"))
                    transaction["bpa_endingdollarbalance"] = endingBalances["dollarhours"];
                if (endingBalances.ContainsKey("secondarydollar"))
                    transaction["bpa_secondarydollarbalance"] = new Money(endingBalances["secondarydollar"]);
            }
            if (endingBalances.ContainsKey("selfpaydollar"))
                transaction["bpa_endingselfpaybalance"] = new Money(endingBalances["selfpaydollar"]);

            if (eligibilityCategory != null)
                transaction["bpa_currenteligibilitycategory"] = eligibilityCategory;
            transaction["bpa_useforcalculation"] = true;
            transaction["bpa_name"] = Guid.NewGuid().ToString();
            transaction["ownerid"] = owningUserTeamRef;

            if (lastAgreement != null)
                transaction["bpa_subsectorid"] = lastAgreement;

            // Return the Create Request
            CreateRequest createRequest = new CreateRequest { Target = transaction };
            return createRequest;
        }

        public static CreateRequest CreateDrawTransactionHour(ITracingService tracingService, Guid memberId,
            Dictionary<string, decimal> endingBalances, DateTime workMonth,
            DateTime benefitMonth, int transactionCategory, int transactionType, EntityReference fund, decimal hours,
            EntityReference currentEligibilityCategory, EntityReference lastAgreement, EntityReference owningUserTeamRef)
        {
            // TODO - Add comments and trace




            Entity transaction = new Entity(PluginHelper.BpaTransaction);
            transaction["bpa_memberplanid"] = new EntityReference(PluginHelper.BpaMemberplan, memberId);
            transaction["bpa_transactiondate"] = workMonth;
            if (benefitMonth.Day > 1)
                benefitMonth = new DateTime(benefitMonth.Year, benefitMonth.Month, 1);

            transaction["bpa_benefitmonth"] = benefitMonth;
            transaction["bpa_transactioncategory"] = new OptionSetValue(transactionCategory);
            transaction["bpa_transactiontype"] = new OptionSetValue(transactionType);

            if ((fund != null) && (fund.Id != Guid.Empty))
                transaction["bpa_fund"] = fund;
            if (hours != 0)
                transaction["bpa_hours"] = hours;
            if (currentEligibilityCategory != null)
            {
                transaction["bpa_currenteligibilitycategory"] = currentEligibilityCategory;
            }
            transaction["bpa_useforcalculation"] = true;
            transaction["bpa_name"] = Guid.NewGuid().ToString();
            transaction["ownerid"] = owningUserTeamRef;
            if (lastAgreement != null)
                transaction["bpa_subsectorid"] = lastAgreement;

            // Return the Create Request
            CreateRequest createRequest = new CreateRequest { Target = transaction};
            return createRequest;
        }

        public static Entity FetchLastBenefitMonth(ITracingService tracingService, EntityCollection transactions, DateTime workMonth, Guid memberPlanId)
        {
            // TODO - Add comments and trace




            Entity qualifiedTransaction = null;
            foreach (Entity transaction in transactions.Entities)
            {
                // Loop though each transaction to find the ones we want
                EntityReference memberPlan = transaction.Contains("bpa_memberplanid") ? transaction.GetAttributeValue<EntityReference>("bpa_memberplanid") : null;
                OptionSetValue stateCode = transaction.GetAttributeValue<OptionSetValue>("statecode");
                DateTime transactionDate = transaction.GetAttributeValue<DateTime>("bpa_transactiondate");
                OptionSetValue transactionType = transaction.GetAttributeValue<OptionSetValue>("bpa_transactiontype");

                // Transaction types to look for
                var transactionTypeList = new List<int>
                {
                    (int)BpaTransactionbpaTransactionType.InBenefit,
                    (int)BpaTransactionbpaTransactionType.SelfPayInBenefit,
                    (int)BpaTransactionbpaTransactionType.FundAssistedInBenefit
                };

                // Look for records matching
                if (memberPlan.Id == memberPlanId &&
                    stateCode.Value == 0 &&
                    transactionDate < workMonth &&
                    transactionTypeList.Any(item => item == transactionType.Value))
                {
                    // Return the one we found
                    // Add it to the returned entity collection
                    if (qualifiedTransaction == null ||
                        transaction.GetAttributeValue<DateTime>("bpa_transactiondate") > qualifiedTransaction.GetAttributeValue<DateTime>("bpa_transactiondate"))
                        qualifiedTransaction = transaction;
                }
            }
            return qualifiedTransaction;
        }

        public static Entity FetchLastEligiblityContributionMonth(ITracingService tracingService, EntityCollection transactions,
            DateTime workMonth, Guid memberPlanId)
        {
            // TODO - Add comments and trace




            Entity qualifiedTransaction = null;
            foreach (Entity transaction in transactions.Entities)
            {
                // Loop though each transaction to find the ones we want
                EntityReference memberPlan = transaction.Contains("bpa_memberplanid") ? transaction.GetAttributeValue<EntityReference>("bpa_memberplanid") : null;
                OptionSetValue stateCode = transaction.GetAttributeValue<OptionSetValue>("statecode");
                DateTime transactionDate = transaction.GetAttributeValue<DateTime>("bpa_transactiondate");
                OptionSetValue transactionType = transaction.GetAttributeValue<OptionSetValue>("bpa_transactiontype");

                // Transaction types to look for
                var transactionTypeList = new List<int>
                {
                    (int)BpaTransactionbpaTransactionType.EmployerContribution
                };

                // Look for records matching
                if (memberPlan.Id == memberPlanId &&
                    stateCode.Value == 0 &&
                    transactionDate < workMonth &&
                    transactionTypeList.Any(item => item == transactionType.Value))
                {
                    // Return the one we found
                    // Add it to the returned entity collection
                    if (qualifiedTransaction == null ||
                        transaction.GetAttributeValue<DateTime>("bpa_transactiondate") > qualifiedTransaction.GetAttributeValue<DateTime>("bpa_transactiondate"))
                        qualifiedTransaction = transaction;
                }
            }
            return qualifiedTransaction;
        }

        public static CreateRequest CreateReserveTransferHour(ITracingService tracingService, Guid memberPlanId, DateTime workMonth,
    DateTime benefitMonth, decimal reserveTransferHours, decimal reserveTransferDollars, Guid defaultFundId, EntityReference owningUserTeamRef)
        {
            // TODO - Add comments and trace




            Entity transaction = new Entity(PluginHelper.BpaTransaction);
            transaction["bpa_memberplanid"] = new EntityReference(PluginHelper.BpaMemberplan, memberPlanId);
            transaction["bpa_transactiondate"] = workMonth;
            if (benefitMonth.Day > 1)
                benefitMonth = new DateTime(benefitMonth.Year, benefitMonth.Month, 1);

            transaction["bpa_benefitmonth"] = benefitMonth;

            if(reserveTransferHours != 0)
                transaction["bpa_hours"] = reserveTransferHours;
            if(reserveTransferDollars != 0)
                transaction["bpa_dollarhour"] = new Money(reserveTransferDollars);

            transaction["bpa_transactiontype"] =
                new OptionSetValue((int)BpaTransactionbpaTransactionType.ReserveTransfer);
            if (defaultFundId != Guid.Empty)
                transaction["bpa_fund"] = new EntityReference(PluginHelper.BpaFund, defaultFundId);
            transaction["bpa_useforcalculation"] = true;
            transaction["ownerid"] = owningUserTeamRef;

            // Return the Create Request
            CreateRequest createRequest = new CreateRequest { Target = transaction };
            return createRequest;
        }
        

        public static EntityCollection AlreadyHasInactiveAndReserveTransactions(ITracingService tracingService, EntityCollection transactions,
            Guid memberPlanId, DateTime workMonth)
        {
            // TODO - Add comments and trace

            EntityCollection qualifiedTransactions = new EntityCollection();
            foreach (Entity transaction in transactions.Entities)
            {
                // Loop though each transaction to find the ones we want
                EntityReference memberPlan = transaction.Contains("bpa_memberplanid") ? transaction.GetAttributeValue<EntityReference>("bpa_memberplanid") : null;
                OptionSetValue stateCode = transaction.GetAttributeValue<OptionSetValue>("statecode");
                DateTime transactionDate = transaction.GetAttributeValue<DateTime>("bpa_transactiondate");
                OptionSetValue transactionType = transaction.GetAttributeValue<OptionSetValue>("bpa_transactiontype");

                // Transaction types to look for
                var transactionTypeList = new List<int>
                {
                    (int)BpaTransactionbpaTransactionType.Inactive,
                    (int)BpaTransactionbpaTransactionType.ReserveTransfer
                };

                // Look for records matching
                if (memberPlan.Id == memberPlanId &&
                    stateCode.Value == 0 &&
                    transactionDate == workMonth &&
                    transactionTypeList.Any(item => item == transactionType.Value))
                {
                    // Return the ones found
                    qualifiedTransactions.Entities.Add(transaction);
                }
            }
            return qualifiedTransactions;
        }

        public static DeleteRequest DeleteTransactionNotInBenefit(ITracingService tracingService, EntityCollection transactions,
            Guid memberPlanId, DateTime workMonth)
        {
            // TODO - Add comments and trace

            foreach (Entity transaction in transactions.Entities)
            {
                // Loop though each transaction to find the ones we want
                EntityReference memberPlan = transaction.Contains("bpa_memberplanid") ? transaction.GetAttributeValue<EntityReference>("bpa_memberplanid") : null;
                OptionSetValue stateCode = transaction.GetAttributeValue<OptionSetValue>("statecode");
                DateTime transactionDate = transaction.GetAttributeValue<DateTime>("bpa_transactiondate");
                OptionSetValue transactionType = transaction.GetAttributeValue<OptionSetValue>("bpa_transactiontype");

                // Transaction types to look for
                var transactionTypeList = new List<int>
                {
                    (int)BpaTransactionbpaTransactionType.NotInBenefit
                };

                // Look for records matching
                if (memberPlan.Id == memberPlanId &&
                    stateCode.Value == 0 &&
                    transactionDate == workMonth &&
                    transactionTypeList.Any(item => item == transactionType.Value))
                {
                    // Return the delete request for the record.  There should only be one
                    DeleteRequest deleteRequest = DeleteTransaction(tracingService, transaction.Id);
                    return deleteRequest;
                }
            }
            return null;
        }

        public static CreateRequest CreateReserveTransferTransaction(ITracingService tracingService, Guid memberPlanId, DateTime workMonth,
            DateTime benefitMonth, string fundName, int transactionCategory, int transactionType, decimal dollarHours,
            bool isEligibilityTransaction, EntityReference eligibilityCategory, Dictionary<string, decimal> endingBalances,
            Guid defaultFundId, EntityReference lastAgreement, EntityReference owningUserTeamRef)
        {
            // TODO - Add comments and trace
            
            Entity transaction = new Entity(PluginHelper.BpaTransaction);
            transaction["bpa_memberplanid"] = new EntityReference(PluginHelper.BpaMemberplan, memberPlanId);
            transaction["bpa_transactiondate"] = workMonth;
            if (benefitMonth.Day > 1)
                benefitMonth = new DateTime(benefitMonth.Year, benefitMonth.Month, 1);

            transaction["bpa_benefitmonth"] = benefitMonth;
            transaction["bpa_transactioncategory"] = new OptionSetValue(transactionCategory);
            transaction["bpa_transactiontype"] =
                new OptionSetValue((int)BpaTransactionbpaTransactionType.ReserveTransfer);
            transaction["bpa_useforcalculation"] = true;

            if (defaultFundId != Guid.Empty)
                transaction["bpa_fund"] = new EntityReference(PluginHelper.BpaFund, defaultFundId);

            if (endingBalances.ContainsKey("hours"))
                transaction["bpa_hours"] = endingBalances["hours"] * -1;
            if (endingBalances.ContainsKey("dollarhours"))
                transaction["bpa_dollarhour"] = new Money(endingBalances["dollarhours"] * -1);
            if (endingBalances.ContainsKey("selfpaydollar"))
                transaction["bpa_endingselfpaybalance"] = new Money(endingBalances["selfpaydollar"]);

            if (endingBalances.ContainsKey("secondarydollar"))
                transaction["bpa_secondarydollarbalance"] = new Money(endingBalances["secondarydollar"]);

            if (eligibilityCategory != null)
                transaction["bpa_currenteligibilitycategory"] = eligibilityCategory;
            transaction["bpa_name"] = Guid.NewGuid().ToString();
            transaction["ownerid"] = owningUserTeamRef;

            if (lastAgreement != null)
                transaction["bpa_subsectorid"] = lastAgreement;
            
            // Return the Create Request
            CreateRequest createRequest = new CreateRequest { Target = transaction };
            return createRequest;
        }

        public static UpdateRequest UpdateConsecutiveMonthTotals(ITracingService tracingService, Guid memberPlanId, int selfPayCounter, int inactiveMonthCounter)
        {
            Entity memberPlan = new Entity(PluginHelper.BpaMemberplan)
            {
                Id = memberPlanId,
                ["bpa_consecutiveinactivemonths"] = inactiveMonthCounter,
                ["bpa_consecutiveselfpaymonths"] = selfPayCounter
            };

            // Add the update to the list of requests
            UpdateRequest updateRequest = new UpdateRequest { Target = memberPlan };
            return updateRequest;
        }

        public static bool HasFutureTransactions(ITracingService tracingService, 
            EntityCollection transactions, DateTime workMonth, Guid memberPlanId)
        {

            foreach (Entity transaction in transactions.Entities)
            {
                // Loop though each transaction to find the ones we want
                EntityReference memberPlan = transaction.Contains("bpa_memberplanid") ? transaction.GetAttributeValue<EntityReference>("bpa_memberplanid") : null;
                OptionSetValue stateCode = transaction.GetAttributeValue<OptionSetValue>("statecode");
                DateTime transactionDate = transaction.GetAttributeValue<DateTime>("bpa_transactiondate");
                OptionSetValue transactionCategory = transaction.Contains("bpa_transactioncategory") ? transaction.GetAttributeValue<OptionSetValue>("bpa_transactioncategory") : new OptionSetValue(0);

                OptionSetValue transactionType = transaction.GetAttributeValue<OptionSetValue>("bpa_transactiontype");

                // Transaction types to look for
                var transactionTypeList = new List<int>
                {
                    (int)BpaTransactionbpaTransactionType.EmployerContribution,
                    (int)BpaTransactionbpaTransactionType.EligibilityDraw,
                    (int)BpaTransactionbpaTransactionType.FundAssisted,
                    (int)BpaTransactionbpaTransactionType.FundAssistedInBenefit,
                    (int)BpaTransactionbpaTransactionType.SelfPay,
                    (int)BpaTransactionbpaTransactionType.SelfPayDraw,
                    (int)BpaTransactionbpaTransactionType.SelfPayInBenefit,
                    (int)BpaTransactionbpaTransactionType.TransferIn,
                    (int)BpaTransactionbpaTransactionType.InBenefit,
                    (int)BpaTransactionbpaTransactionType.NotInBenefit,
                    (int)BpaTransactionbpaTransactionType.NewMember,
                    (int)BpaTransactionbpaTransactionType.UnderReview,
                    (int)BpaTransactionbpaTransactionType.SelfPayInBenefit,
                    (int)BpaTransactionbpaTransactionType.ReInstating,
                    (int)BpaTransactionbpaTransactionType.HourBankAdjustment,
                    (int)BpaTransactionbpaTransactionType.DollarBankAdjustment
                };

                // Look for records matching
                    if (memberPlan.Id == memberPlanId &&
                    stateCode.Value == 0 &&
                    transactionDate > workMonth &&
                    transactionTypeList.Any(item => item == transactionType.Value))
                {
                    // Return the one we found
                    return true;
                }
            }
            return false;
        }

        public static DateTime FutureActivityMonth(ITracingService tracingService,
            EntityCollection transactions, DateTime workMonth, Guid memberPlanId)
        {
            // TODO - Add comments and trace

            DateTime futureMonth = DateTime.MinValue;
            foreach (Entity transaction in transactions.Entities)
            {
                // Loop though each transaction to find the ones we want
                EntityReference memberPlan = transaction.Contains("bpa_memberplanid") ? transaction.GetAttributeValue<EntityReference>("bpa_memberplanid") : null;
                OptionSetValue stateCode = transaction.GetAttributeValue<OptionSetValue>("statecode");
                DateTime transactionDate = transaction.GetAttributeValue<DateTime>("bpa_transactiondate");
                OptionSetValue transactionCategory = transaction.Contains("bpa_transactioncategory") ? transaction.GetAttributeValue<OptionSetValue>("bpa_transactioncategory") : new OptionSetValue(0);
                OptionSetValue transactionType = transaction.GetAttributeValue<OptionSetValue>("bpa_transactiontype");

                // Transaction types to look for
                var transactionTypeList = new List<int>
                {
                    (int)BpaTransactionbpaTransactionType.EmployerContribution,
                    (int)BpaTransactionbpaTransactionType.HourBankAdjustment,
                    (int)BpaTransactionbpaTransactionType.DollarBankAdjustment
                };

                // Look for records matching
                if (memberPlan.Id == memberPlanId &&
                stateCode.Value == 0 &&
                transactionDate > workMonth &&
                transactionTypeList.Any(item => item == transactionType.Value))
                {
                    /*
                    // Record the date if it is sooner than the one we already have
                    if (transaction.Contains("bpa_transactiondate") && ((AliasedValue)transaction.Attributes["bpa_transactiondate"]).Value != null)
                        if (futureMonth == DateTime.MinValue || futureMonth > ((DateTime)((AliasedValue)transaction.Attributes["bpa_transactiondate"]).Value))
                            futureMonth = ((DateTime)((AliasedValue)transaction.Attributes["bpa_transactiondate"]).Value);
                     */

                    // Record the date if it is sooner than the one we already have - JK Cleaning above code as futureMonth alwasy true
                    if (transaction.Contains("bpa_transactiondate") && (transaction.GetAttributeValue<DateTime>("bpa_transactiondate") != null))
                        futureMonth = transaction.GetAttributeValue<DateTime>("bpa_transactiondate");
                }
            }
            if (futureMonth != DateTime.MinValue)
                futureMonth = new DateTime(futureMonth.Year, futureMonth.Month, 1).AddMonths(-1);

            return futureMonth;
        }
        
        public static Dictionary<string, decimal> UpdateBanks(ITracingService tracingService, Dictionary<string, decimal> bankBalances,
            decimal? dollarHours, decimal? hour, decimal? selfPayDollar, decimal? secondaryDollar, bool isClearBalance)
        {
            // TODO - Add comments and trace

            // Clear out all values when it is reserve transfer 
            if (isClearBalance)
            {
                bankBalances["hours"] = 0;
                bankBalances["dollarhours"] = 0;
                bankBalances["selfpaydollar"] = 0;
                bankBalances["secondarydollar"] = 0;
            }
            else
            {
                if (bankBalances.ContainsKey("hours") && hour != null)
                    bankBalances["hours"] += (decimal)hour;
                if (bankBalances.ContainsKey("dollarhours") && dollarHours != null)
                    bankBalances["dollarhours"] += (decimal)dollarHours;
                if (bankBalances.ContainsKey("selfpaydollar") && selfPayDollar != null)
                    bankBalances["selfpaydollar"] += (decimal)selfPayDollar;
                if (bankBalances.ContainsKey("secondarydollar") && secondaryDollar != null)
                    bankBalances["secondarydollar"] += (decimal)secondaryDollar;
            }
            return bankBalances;
        }

        public static Entity FetchMemberPlan(IOrganizationService service, ITracingService tracingService, Guid memberPlanId)
        {
            return service.Retrieve("bpa_memberplan", memberPlanId, new ColumnSet(true));
        }

        public static DateTime FetchFirstMonthByMemberPlanByTransationType(IOrganizationService service, ITracingService tracingService, Guid memberPlanId, 
            int transationType)
        {
            DateTime firstMonth = DateTime.MinValue;
            string fetchXml = $@"
                    <fetch aggregate='true' >
                      <entity name='bpa_transaction' >
                        <attribute name='bpa_transactiondate' alias='bpa_transactiondate' aggregate='min' />
                        <filter>
                          <condition attribute='bpa_transactiontype' operator='eq' value='{transationType}' />
                          <condition attribute='bpa_memberplanid' operator='eq' value='{memberPlanId}' />
                        </filter>
                      </entity>
                    </fetch>
            ";
            EntityCollection collection = service.RetrieveMultiple(new FetchExpression(fetchXml));
            if(collection != null && collection.Entities.Count > 0)
            {
                Entity e = collection.Entities[0];
                firstMonth = e.Contains("bpa_transactiondate") ? (DateTime)e.GetAttributeValue<AliasedValue>("bpa_transactiondate").Value : DateTime.MinValue;
            }
            return firstMonth;
        }

    }
}
