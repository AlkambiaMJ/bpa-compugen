﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Metadata;
using Microsoft.Xrm.Sdk.Query;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace BPA.Benefit.Management.CRM.Plugins.Helper
{
    public class InvoiceDetailHelper
    {
        // TODO: Class can be deleted

        //public static Money FetchTax(IOrganizationService service, Guid BusinessUnitId, Entity target)
        //{
        //    Money TaxAmount = new Money(0);
        //    //Get Tax value
        //    QueryExpression queryExpression = new QueryExpression()
        //    {
        //        EntityName = "bpa_tax",
        //        ColumnSet = new ColumnSet(true),
        //        Criteria =
        //        {
        //            Filters =
        //                        {
        //                            new FilterExpression
        //                            {
        //                                FilterOperator = LogicalOperator.And,
        //                                Conditions =
        //                                {
        //                                    new ConditionExpression("bpa_businessunit", ConditionOperator.Equal, BusinessUnitId),
        //                                }
        //                            }
        //                        }
        //        }
        //    };
        //    EntityCollection taxs = service.RetrieveMultiple(queryExpression);
        //    decimal? taxValue = 0;
        //    if (taxs.Entities.Count > 0)
        //    {
        //        Entity tax = taxs.Entities[0];
        //        if (tax.Attributes.Contains("bpa_value"))
        //        {
        //            taxValue = tax.GetAttributeValue<decimal?>("bpa_value").GetValueOrDefault(0);
        //        }
        //    }

        //    if (taxValue != null && taxValue.Value != 0)
        //    {
        //        //Get Invoice - Price List
        //        Guid invoiceid = Guid.Empty;
        //        if (target.Attributes.Contains("invoiceid"))
        //        {
        //            invoiceid = ((EntityReference)target.Attributes["invoiceid"]).Id;
        //            QueryExpression queryInvoice = new QueryExpression()
        //            {
        //                EntityName = "invoice",
        //                ColumnSet = new ColumnSet(true),
        //                Criteria =
        //                {
        //                    Filters =
        //                        {
        //                            new FilterExpression
        //                            {
        //                                FilterOperator = LogicalOperator.And,
        //                                Conditions =
        //                                {
        //                                    new ConditionExpression("invoiceid", ConditionOperator.Equal, invoiceid),
        //                                }
        //                            }
        //                        }
        //                }
        //            };
        //            EntityCollection invoices = service.RetrieveMultiple(queryInvoice);
        //            Guid pricelistId = Guid.Empty;
        //            if (invoices.Entities.Count > 0)
        //            {
        //                Entity invoice = invoices.Entities[0];
        //                if (invoice.Attributes.Contains("pricelevelid"))
        //                    pricelistId = invoice.GetAttributeValue<EntityReference>("pricelevelid").Id;

        //                //GET Price List item
        //                if (pricelistId != Guid.Empty)
        //                {
        //                    Money priceperunit = new Money(0);
        //                    Guid productid = ((EntityReference)target["productid"]).Id;
        //                    QueryExpression queryPriceListItem = new QueryExpression()
        //                    {
        //                        EntityName = "productpricelevel",
        //                        ColumnSet = new ColumnSet(true),
        //                        Criteria =
        //                        {
        //                            Filters =
        //                                        {
        //                                            new FilterExpression
        //                                            {
        //                                                FilterOperator = LogicalOperator.And,
        //                                                Conditions =
        //                                                {
        //                                                    new ConditionExpression("pricelevelid", ConditionOperator.Equal, pricelistId),
        //                                                    new ConditionExpression("productid", ConditionOperator.Equal, productid),
        //                                                }
        //                                            }
        //                                        }
        //                        }
        //                    };

        //                    EntityCollection pricelistItems = service.RetrieveMultiple(queryPriceListItem);
        //                    if (pricelistItems.Entities.Count > 0)
        //                    {
        //                        Entity pricelistItem = pricelistItems.Entities[0];
        //                        if (pricelistItem.Attributes.Contains("amount"))
        //                            priceperunit = pricelistItem.GetAttributeValue<Money>("amount");

        //                        decimal? quantity = 1;
        //                        if (target.Attributes.Contains("quantity"))
        //                            quantity = target.GetAttributeValue<decimal?>("quantity").GetValueOrDefault(1);

        //                        //CALCULATE TAX
        //                        decimal taxAmount = 0;

        //                        if ((priceperunit != null) && (priceperunit.Value != 0))
        //                            taxAmount = (priceperunit.Value * quantity.Value) * taxValue.Value;

        //                        OptionSetValue bpa_invoicetype = null;
        //                        if (invoice.Attributes.Contains("bpa_invoicetype"))
        //                        {
        //                            bpa_invoicetype = invoice.GetAttributeValue<OptionSetValue>("bpa_invoicetype");

        //                            if (bpa_invoicetype.Value == (int)Invoicebpa_InvoiceType.Refund)
        //                                taxAmount = taxAmount * -1;

        //                        }
        //                        TaxAmount = new Money(taxAmount);
        //                    }
        //                }//end of if (pricelistId != Guid.Empty)
        //            }//end of if (invoices.Entities.Count > 0)

        //        }//end of if(target.Attributes.Contains("invoiceid"))
        //    } //end of if (taxValue != null && taxValue.Value != 0)

        //    return TaxAmount;
        //}


        //public static Guid FetchProduct(IOrganizationService service, Guid pricelevelid, DateTime workmonth)
        //{
        //    Guid productId = Guid.Empty;

        //    string fetchXml = @"
        //        <fetch count='50' >
        //            <entity name='productpricelevel' >
        //                <all-attributes/>
        //                <filter type='and' >
        //                    <condition attribute='pricelevelid' operator='eq' value='" + pricelevelid.ToString() + @"' />
        //                </filter>
        //                <link-entity name='product' from='productid' to='productid' link-type='inner'>
        //                    <filter>
        //                        <condition attribute='bpa_benefitmonth' operator='eq' value='" + workmonth.ToShortDateString() + @"'/> 
        //                    </filter>
        //                </link-entity>
        //            </entity>
        //        </fetch>";
        //    EntityCollection products = service.RetrieveMultiple(new FetchExpression(fetchXml));
        //    if (products != null && products.Entities.Count > 0)
        //    {
        //        Entity pricelevelitem = products.Entities[0];
        //        EntityReference product_Ref = pricelevelitem.GetAttributeValue<EntityReference>("productid");
        //        if (product_Ref != null)
        //            productId = product_Ref.Id;
        //    }
        //    return productId;
        //}
    }
}
