﻿#region

using System;
using System.Collections.Generic;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;

#endregion

namespace BPA.Benefit.Management.CRM.Plugins.Helper
{
    public class PlanDependantHelper
    {
        public static bool IsDependantAlredyExists(IOrganizationService service, ITracingService tracingService, Guid dependantId, Guid memberPlanId)
        {
            bool isExists = false;
            string fetchXml = $@"
               <fetch mapping='logical' output-format='xml-platform' version='1.0' >
                  <entity name='bpa_dependantplan' >
                    <attribute name='bpa_name' />
                    <filter>
                      <condition attribute='statecode' operator='eq' value='0' />
                      <condition attribute='bpa_dependantid' operator='eq' value='{dependantId.ToString()}' />
                      <condition attribute='bpa_memberplanid' operator='eq' value='{memberPlanId.ToString()}' />
                    </filter>
                  </entity>
                </fetch>
            ";
            
            EntityCollection planDependants = service.RetrieveMultiple(new FetchExpression(fetchXml));

            if (planDependants != null && planDependants.Entities.Count > 0)
                isExists = true;

            return isExists;
        }
    }
}
