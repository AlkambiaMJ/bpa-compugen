﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xrm.Sdk;

#endregion

// DONE - TODOJK: Clean up needed on commented out code

namespace BPA.Benefit.Management.CRM.Plugins.Helper
{
    public class MemberRetroprocessing
    {
        //public static bool RecalculateTransactionNew (IOrganizationService service, ITracingService tracingService,
        //    EntityReference selectedContact, DateTime startWorkMonth, bool isMemberCategoryChanged)
        //{
        //    // Delete all existing Status and Draw Transactions from startWorkMonth and later - Keep Contribution Transactions

        //    // Get the following values
        //    //Eligibility Category / Category Type = Category Type(CT)
        //    //Eligibility Category Details / Deduction Rate = Dollar Deduction Rate(DDR)
        //    //Eligibility Category Details / Hours Deduction Rate = Hours Deduction Rate(HDR)
        //    //Eligibility Category Details / Self - Pay Amount = Self - Pay Amount(SPA)
        //    //Eligibility Category Details / Fund Assistance Amount = Dollar Fund Assistance Amount(DFAA)
        //    //Eligibility Category Details / Hours Fund Assistance = Hours Fund Assistance(HFA)(Boolean)
        //    //Eligibility Category Details / Minimum Initial Eligibility = Minimum Dollar Initial Eligibility(MDIE)
        //    //Eligibility Category Details / Hours Minimum Initial Eligibility = Minimum Hour Initial Eligibility(MHIE)
        //    //Eligibility Category Details / Reinstatement Dollar = Reinstatement Dollar(RD)
        //    //Eligibility Category Details / Reinstatement Hour = Reinstatement Hour(RH)

        //    // Calculate the last month to calclulate on

        //    // Loop from startWorkMonth to lastWorkMonth


        //        // Get the following values for this month

        //        //Previous Month’s Benefit Status – (In Benefit, In Benefit – Self Pay, In Benefit – Fund Assistance, Not in Benefit, Not in Benefit – New Member,
        //            //Not in Benefit – Reinstating, Not in Benefit – Frozen, Not in Benefit – Under Review)
        //        //Current Month there is a Contribution(Yes / No) = Current Month Contribution(CMC - Boolean)

        //        //Member Plan / Hour Bank(ending balance for previous month) = Hour Bank(HB)
        //        //Member Plan / Dollar Bank(ending balance for previous month) = Dollar Bank(DB)
        //        //Member Plan / Self-Pay Bank(ending balance for previous month) = Self-Pay Bank(SPB)
                
        //    return true;
        //}
        
        //public static string RecalculateTransaction(IOrganizationService service, ITracingService tracingService,
        //    EntityReference selectedContact, DateTime startWorkMonth, bool isMemberCategoryChanged)
        //{
        //    // Retrieve the id
        //    Guid memberId = selectedContact.Id;
            
        //    EntityReference currentEligibilityCategory = ContactHelper.FetchCurrentEligibilityCategory(service, tracingService, selectedContact.Id);
        //    bool isEligibilityCategoryFundAssisted = false;

        //    //Get MAX Work Month from Transaction
        //    DateTime lastWorkMonth = DateTime.Today.AddMonths(-1);
        //    lastWorkMonth = new DateTime(lastWorkMonth.Year, lastWorkMonth.Month, 1);

        //    int memberContactType = ContactHelper.FetchMemberContactType(service, tracingService, selectedContact.Id);
        //    if (memberContactType == (int)PlanPlanType.Retiree)
        //    {
        //        lastWorkMonth = lastWorkMonth.AddMonths(-1);
        //    }
        //    else
        //    {
        //        DateTime maxWorkMonth = TransactionHelper.GetMaxWorkMonth(service, selectedContact.Id, lastWorkMonth);
        //        if (maxWorkMonth > lastWorkMonth)
        //            lastWorkMonth = maxWorkMonth;
        //    }

        //    List<Guid> skipTransactions = null;
        //    bool isMemberBecomeInActive = false;

        //    int selfPayCounter = 0, inactiveMonthCounter = 0, maxInactiveCounter = 0;

        //    //Find Default Welfare Fund for MEmber Plan
        //    EntityReference defaultFund = null;
        //    defaultFund = ContactHelper.FetchDefaultFundByMemberId(service, tracingService, memberId);
        //    if (defaultFund == null)
        //        defaultFund = new EntityReference("bpa_fund", Guid.Empty);

        //    //find Member Plan is retiree ?
        //    bool isRetireePlan = ContactHelper.IsMemberPlanRetiree(service, tracingService, selectedContact.Id);

        //    //Run Reto processing for all the month
        //    while (startWorkMonth <= lastWorkMonth)
        //    {
        //        #region -- Start DATE LOOP ----

        //        skipTransactions = new List<Guid>();
        //        List<Guid> ToBeDeletedTransactions = new List<Guid>();

        //        //Find HOW MANY ELIGIBILITY CONTRIBUTION TRANSACTIONS? IF MORE THAN 1 THAN GET THE LAST AND INGOR EVERY OTHER TRANSACTIONS
        //        EntityCollection EligibilityContributionTransactions =
        //            RecalculationTransactionHelper.FetchAllEligibilityDrawTransactions(service, selectedContact.Id,
        //                startWorkMonth,
        //                (int)BpaTransactionbpaTransactionType.EmployerContribution);
        //        if (EligibilityContributionTransactions != null && EligibilityContributionTransactions.Entities.Count > 1)
        //        {
        //            int iCnt = 0;
        //            foreach (Entity t in EligibilityContributionTransactions.Entities)
        //            {
        //                if (iCnt != 0)
        //                    skipTransactions.Add(t.Id);
        //                iCnt++;
        //            }
        //        }

        //        //Get Current Eligibity from Contact
        //        int currentEligibility = 0; //ContactHelper.FetchCurrentEligibility(selectedContact.Id);
        //        int eligibilityCategoryType = ContactHelper.GetCurrentEligibilityType(service, tracingService, currentEligibilityCategory.Id);
        //        if (eligibilityCategoryType == (int)BpaEligibilitycategorybpaCategoryType.FundAssisted)
        //            isEligibilityCategoryFundAssisted = true;

        //        //Get Previous Month Eligibility Values

        //        #region ----- Get Previous Month Eligibility Values AND SET to Current Eligibility

        //        Entity previousMonthEligibility = RecalculationTransactionHelper.FetchPreviousMonthEligibility(service,
        //            selectedContact.Id, startWorkMonth.AddMonths(-1));
        //        decimal previousMonth_EndingBalance_Hours = 0;
        //        decimal previousMonth_EndingBalance_Dollar = 0;
        //        int previousMonthEligibilityType = 0;
        //        bool IsFirstMonth = false;
        //        if (previousMonthEligibility != null)
        //        {
        //            if (previousMonthEligibility.Attributes.Contains("bpa_endingdollarbalance"))
        //                previousMonth_EndingBalance_Dollar =
        //                    previousMonthEligibility.GetAttributeValue<decimal>("bpa_endingdollarbalance");
        //            if (previousMonthEligibility.Attributes.Contains("bpa_endinghourbalance"))
        //                previousMonth_EndingBalance_Hours =
        //                    previousMonthEligibility.GetAttributeValue<decimal>("bpa_endinghourbalance");
        //            previousMonthEligibilityType =
        //                previousMonthEligibility.GetAttributeValue<OptionSetValue>("bpa_transactiontype").Value;

        //            //Put the Condition for CHECKING
        //            switch (previousMonthEligibilityType)
        //            {
        //                case (int)BpaTransactionbpaTransactionType.InBenefit:
        //                    currentEligibility = (int)MempberPlanBpaCurrentEligibility.InBenefit;
        //                    break;
        //                case (int)BpaTransactionbpaTransactionType.NewMember:
        //                    currentEligibility = (int)MempberPlanBpaCurrentEligibility.NewMember;
        //                    break;
        //                case (int)BpaTransactionbpaTransactionType.NotInBenefit:
        //                    currentEligibility = (int)MempberPlanBpaCurrentEligibility.NotInBenefit;
        //                    break;
        //                case (int)BpaTransactionbpaTransactionType.FundAssistedInBenefit:
        //                    currentEligibility = (int)MempberPlanBpaCurrentEligibility.InBenefit;
        //                    break;
        //                case (int)BpaTransactionbpaTransactionType.SelfPayInBenefit:
        //                    currentEligibility = (int)MempberPlanBpaCurrentEligibility.SelfPayInBenefit;
        //                    break;
        //                case (int)BpaTransactionbpaTransactionType.UnderReview:
        //                    currentEligibility = (int)MempberPlanBpaCurrentEligibility.UnderReview;
        //                    break;
        //                case (int)BpaTransactionbpaTransactionType.TransferOut:
        //                    currentEligibility = (int)MempberPlanBpaCurrentEligibility.TransferOut;
        //                    break;
        //                case (int)BpaTransactionbpaTransactionType.ReInstateing:
        //                    currentEligibility = (int)MempberPlanBpaCurrentEligibility.Reinstatement;
        //                    break;
        //            }
        //            IsFirstMonth = false;
        //        }
        //        else
        //        {
        //            currentEligibility = (int)MempberPlanBpaCurrentEligibility.NewMember;
        //            IsFirstMonth = true;
        //        }

        //        #endregion

        //        if (isEligibilityCategoryFundAssisted &&
        //            ((currentEligibility == (int)MempberPlanBpaCurrentEligibility.NewMember) || (currentEligibility == (int)MempberPlanBpaCurrentEligibility.Frozen) || 
        //            (currentEligibility == (int)MempberPlanBpaCurrentEligibility.UnderReview) ||(currentEligibility == (int)MempberPlanBpaCurrentEligibility.NotInBenefit)))
        //        {
        //            startWorkMonth = startWorkMonth.AddMonths(1);
        //            continue;
        //        }

        //        //Fetch All Transactions for that month
        //        EntityCollection M_transactions = RecalculationTransactionHelper.FetchAllTransactions(service,selectedContact.Id, startWorkMonth);

        //        EntityReference UsingEligibilityCategoty = null;
        //        EntityReference Employer = null;
        //        List<Entity> M_transactionsList = null;
        //        bool HasECED = false;

        //        if (M_transactions != null && M_transactions.Entities.Count > 0)
        //        {
        //            M_transactionsList = M_transactions.Entities.ToList();

        //            List<Entity> doesNotHaveECED = M_transactionsList.Where( x =>
        //                        (x.GetAttributeValue<OptionSetValue>("bpa_transactiontype").Value == (int)BpaTransactionbpaTransactionType.EmployerContribution) ||
        //                        (x.GetAttributeValue<OptionSetValue>("bpa_transactiontype").Value == (int)BpaTransactionbpaTransactionType.SelfPay)).ToList();
        //            if (doesNotHaveECED != null && doesNotHaveECED.Count > 0)
        //            {
        //                HasECED = true;
        //            }

        //            List<Entity> lsFundAssistedAndEmployeeContributions = M_transactionsList.Where(x =>
        //                        (x.GetAttributeValue<OptionSetValue>("bpa_transactiontype").Value == (int)BpaTransactionbpaTransactionType.EmployerContribution) ||
        //                        (x.GetAttributeValue<OptionSetValue>("bpa_transactiontype").Value == (int)BpaTransactionbpaTransactionType.FundAssited)).ToList();
        //            if (lsFundAssistedAndEmployeeContributions != null &&
        //                lsFundAssistedAndEmployeeContributions.Count >= 2)
        //            {
        //                foreach (Entity e in lsFundAssistedAndEmployeeContributions)
        //                {
        //                    int transactionType1 = e.GetAttributeValue<OptionSetValue>("bpa_transactiontype").Value;

        //                    if (transactionType1 == (int)BpaTransactionbpaTransactionType.FundAssited)
        //                    {
        //                        skipTransactions.Add(e.Id);
        //                        break;
        //                    }
        //                }
        //            }
        //        }

        //        bool HasReciprocalTransferOUT = false,
        //            HasReciprocalTransferIN = false,
        //            IsTransferAmountPaid = false,
        //            HasTrasfer = false,
        //            IsReciprocalTransferOutDelete = false;
        //        if (M_transactions != null && M_transactions.Entities.Count > 0)
        //        {
        //            #region ----------------- Member Transfer OUT ? -----------------------------

        //            List<Entity> ListOfReciprocalTransferOut =
        //                M_transactionsList.Where(
        //                    x =>
        //                        x.GetAttributeValue<OptionSetValue>("bpa_transactiontype").Value ==
        //                        (int)BpaTransactionbpaTransactionType.TransferOut).ToList();
        //            if (ListOfReciprocalTransferOut != null && ListOfReciprocalTransferOut.Count > 0)
        //                HasReciprocalTransferOUT = true;
        //            else
        //                HasReciprocalTransferOUT = false;

        //            List<Entity> ListOfTransfer =
        //                M_transactionsList.Where(
        //                    x =>
        //                        x.GetAttributeValue<OptionSetValue>("bpa_transactiontype").Value ==
        //                        (int)BpaTransactionbpaTransactionType.Reciprocal).ToList();
        //            if (ListOfTransfer != null && ListOfTransfer.Count > 0)
        //            {
        //                HasTrasfer = true;
        //                Entity TransactionPaid = ListOfTransfer[0];
        //                if (TransactionPaid.Attributes.Contains("bpa_reciprocaltransfer"))
        //                    IsTransferAmountPaid = TransactionPaid.GetAttributeValue<bool>("bpa_reciprocaltransfer");
        //                else
        //                    IsTransferAmountPaid = false;
        //            }
        //            else
        //            {
        //                IsTransferAmountPaid = false;
        //                HasTrasfer = false;
        //            }

        //            List<Entity> ListOfReciprocalTransferIN =
        //                M_transactionsList.Where(
        //                    x =>
        //                        x.GetAttributeValue<OptionSetValue>("bpa_transactiontype").Value ==
        //                        (int)BpaTransactionbpaTransactionType.TransferIn).ToList();
        //            if (ListOfReciprocalTransferIN != null && ListOfReciprocalTransferIN.Count > 0)
        //                HasReciprocalTransferIN = true;
        //            else
        //                HasReciprocalTransferIN = false;

        //            #endregion

        //            #region ----------------- INACTIVATE ALL ELIGIBILITY TRANSACTIONS -----------------------------

        //            //Make all transactions ELIGIBILITY TRANNSACTION INACTIVE
        //            foreach (Entity transaction in M_transactions.Entities)
        //            {
        //                int transactionType = transaction.GetAttributeValue<OptionSetValue>("bpa_transactiontype").Value;

        //                if ((transactionType == (int)BpaTransactionbpaTransactionType.EligibilityDraw) ||
        //                    (transactionType == (int)BpaTransactionbpaTransactionType.ReserveTransfer) ||
        //                    (transactionType == (int)BpaTransactionbpaTransactionType.InBenefit) ||
        //                    (transactionType == (int)BpaTransactionbpaTransactionType.NotInBenefit) ||
        //                    (transactionType == (int)BpaTransactionbpaTransactionType.FundAssited) ||
        //                    (transactionType == (int)BpaTransactionbpaTransactionType.FundAssistedInBenefit) ||
        //                    (transactionType == (int)BpaTransactionbpaTransactionType.UnderReview) ||
        //                    (transactionType == (int)BpaTransactionbpaTransactionType.NewMember) ||
        //                    (transactionType == (int)BpaTransactionbpaTransactionType.ReInstateing) ||
        //                    (transactionType == (int)BpaTransactionbpaTransactionType.SelfPay) ||
        //                    (transactionType == (int)BpaTransactionbpaTransactionType.SelfPayMaxConsecutiveReach) ||
        //                    (transactionType == (int)BpaTransactionbpaTransactionType.SelfPayInBenefit) ||
        //                    (transactionType == (int)BpaTransactionbpaTransactionType.SelfPayDraw) ||
        //                    (transactionType == (int)BpaTransactionbpaTransactionType.Inactive) ||
        //                    (transactionType == (int)BpaTransactionbpaTransactionType.Frozen) ||
        //                    (transactionType == (int)BpaTransactionbpaTransactionType.TransferOut) ||
        //                    (transactionType == (int)BpaTransactionbpaTransactionType.Reciprocal))
        //                {
        //                    if (transactionType == (int)BpaTransactionbpaTransactionType.Frozen)
        //                    {
        //                        currentEligibility = (int)MempberPlanBpaCurrentEligibility.Frozen;
        //                        ContactHelper.SetCurrentEligibility(service, tracingService, selectedContact.Id, (int)MempberPlanBpaCurrentEligibility.Frozen, DateTime.MinValue);
        //                    }
        //                    else if ((transactionType == (int)BpaTransactionbpaTransactionType.Inactive) ||
        //                                (transactionType == (int)BpaTransactionbpaTransactionType.ReserveTransfer))
        //                    {
        //                        List<Entity> lsEC = M_transactionsList.Where( x =>
        //                                    (x.GetAttributeValue<OptionSetValue>("bpa_transactiontype").Value == (int)BpaTransactionbpaTransactionType.EmployerContribution) &&
        //                                    (x.GetAttributeValue<OptionSetValue>("bpa_transactiontype").Value == (int)BpaTransactionbpaTransactionType.Inactive) &&
        //                                    (x.GetAttributeValue<OptionSetValue>("bpa_transactiontype").Value == (int)BpaTransactionbpaTransactionType.ReserveTransfer)).ToList();

        //                        //Does member received Employer Contribution when he is Inactive and has Reserve Transfer 
        //                        bool IsExist = TransactionHelper.DoesMemberInactiveSameMonth(service, selectedContact.Id, startWorkMonth);

        //                        if (IsExist)
        //                            currentEligibility = (int)MempberPlanBpaCurrentEligibility.NewMember;
        //                        else
        //                        {
        //                            bool reciprocaltransfer = false;
        //                            if (transactionType == (int)BpaTransactionbpaTransactionType.ReserveTransfer)
        //                                reciprocaltransfer = transaction.GetAttributeValue<bool>("bpa_reciprocaltransfer");

        //                            if (!reciprocaltransfer)
        //                            {
        //                                RecalculationTransactionHelper.SetTransactionStatus(service, transaction.Id, false);
        //                                ToBeDeletedTransactions.Add(transaction.Id);
        //                            }
        //                        }
        //                    }
        //                    else if (transactionType == (int)BpaTransactionbpaTransactionType.TransferOut)
        //                    {
        //                        if (HasReciprocalTransferOUT && HasTrasfer && !IsTransferAmountPaid)
        //                        {
        //                            IsReciprocalTransferOutDelete = true;
        //                            RecalculationTransactionHelper.SetTransactionStatus(service, transaction.Id, false);
        //                            ToBeDeletedTransactions.Add(transaction.Id);
        //                        }
        //                        else if (HasReciprocalTransferOUT && !HasTrasfer && !IsTransferAmountPaid)
        //                        {
        //                            IsReciprocalTransferOutDelete = true;
        //                            RecalculationTransactionHelper.SetTransactionStatus(service, transaction.Id, false);
        //                            ToBeDeletedTransactions.Add(transaction.Id);
        //                        }
        //                        else if (HasReciprocalTransferOUT && HasTrasfer && IsTransferAmountPaid)
        //                        {
        //                            RecalculationTransactionHelper.SetTransactionStatus(service, transaction.Id, false);
        //                            ToBeDeletedTransactions.Add(transaction.Id);
        //                            IsReciprocalTransferOutDelete = false;
        //                        }
        //                        else
        //                        {
        //                            IsReciprocalTransferOutDelete = false;
        //                        }
        //                    }
        //                    else if (transactionType == (int)BpaTransactionbpaTransactionType.Reciprocal)
        //                    {
        //                        if (!IsTransferAmountPaid)
        //                        {
        //                            RecalculationTransactionHelper.SetTransactionStatus(service, transaction.Id, false);
        //                            ToBeDeletedTransactions.Add(transaction.Id);
        //                        }
        //                        else
        //                        {
        //                            //Dictionary<string, decimal> endingBalances1 = RecalculationTransactionHelper.FetchEndingBalance(service, selectedContact.Id, startWorkMonth.AddDays(1), defaultFund.Id);
        //                            Dictionary<string, decimal> endingBalances1 = TransactionHelper.FetchEndingBalanceWhenLateContribution(service, selectedContact.Id, startWorkMonth.AddDays(1));

        //                            //Update Transaction
        //                            Entity t = new Entity("bpa_transaction")
        //                            {
        //                                Id = transaction.Id
        //                            };
        //                            if (endingBalances1.ContainsKey("hours"))
        //                                t["bpa_endinghourbalance"] = endingBalances1["hours"];
        //                            if (endingBalances1.ContainsKey("dollarhours"))
        //                                t["bpa_endingdollarbalance"] = endingBalances1["dollarhours"];
        //                            if (endingBalances1.ContainsKey("selfpaydollar"))
        //                                t["bpa_endingselfpaybalance"] = new Money(endingBalances1["selfpaydollar"]);
        //                            service.Update(t);
        //                        }
        //                    }
        //                    else
        //                    {
        //                        RecalculationTransactionHelper.SetTransactionStatus(service, transaction.Id, false);
        //                        ToBeDeletedTransactions.Add(transaction.Id);
        //                    }
        //                }
        //            }
        //            //Trigger Rollup after Retro Processing done
        //            PluginHelper.TriggerRollup(service, tracingService, "bpa_memberplan", selectedContact.Id, "bpa_contributiondollarbank");
        //            PluginHelper.TriggerRollup(service, tracingService, "bpa_memberplan", selectedContact.Id, "bpa_contributionhourbank");
        //            PluginHelper.TriggerRollup(service, tracingService, "bpa_memberplan", selectedContact.Id, "bpa_selfpaybank");

        //            #endregion

        //            #region ----------------- RECALCULATION LOGIC -----------------------

        //            if (M_transactions != null && M_transactions.Entities.Count > 0)
        //            {
        //                M_transactionsList = M_transactions.Entities.ToList();
        //            }

        //            //Check this month has Self-Pay Benefit or not?
        //            bool HasSelfPayInBenefit = false;
        //            List<Entity> ListOfSelfPayAndSelfPayRefund = M_transactionsList.Where(x =>
        //                        x.GetAttributeValue<OptionSetValue>("bpa_transactiontype").Value == (int)BpaTransactionbpaTransactionType.SelfPayInBenefit).ToList();
        //            if (ListOfSelfPayAndSelfPayRefund != null && ListOfSelfPayAndSelfPayRefund.Count > 0)
        //                HasSelfPayInBenefit = true;
        //            else
        //                HasSelfPayInBenefit = false;

        //            //Check this month has FUND ASSISTED or NOT?
        //            bool HasFundAssisted = false;
        //            List<Entity> ListOfFundAssisted = M_transactionsList.Where(x =>
        //                        x.GetAttributeValue<OptionSetValue>("bpa_transactiontype").Value == (int)BpaTransactionbpaTransactionType.FundAssited).ToList();
        //            if (ListOfFundAssisted != null && ListOfFundAssisted.Count > 0)
        //                HasFundAssisted = true;
        //            else
        //                HasFundAssisted = false;

        //            //Find out Memeber Elibiglity Category Calucation Type
        //            //*************************THIS IS ADDED BECAUSE WE NEED TO RECALCUATED BY 'HOURS'****************************************
        //            int EC_CalculationType = 0;
                     
        //            if (isEligibilityCategoryFundAssisted)
        //            {
        //                inactiveMonthCounter = 0;

        //                #region --------------------- FUND ASSISTED ----------------------------

        //                Entity EligibilityCategoryDetail = EligibilityCategoryHelper.FetchEligibilityDetail(service, currentEligibilityCategory.Id, startWorkMonth);
        //                if (EligibilityCategoryDetail != null)
        //                {
        //                    EC_CalculationType = ContactHelper.GetCurrentEligibilityType(service, tracingService, currentEligibilityCategory.Id);
        //                    if (EC_CalculationType == (int)BpaEligibilitycategorybpaCategoryType.FundAssisted)
        //                    {
        //                        HasECED = true;
        //                        Money FA_deductionrate = new Money(0);
        //                        Money FA_fundassistanceamount = new Money(0);
        //                        int FA_eligibilityoffsetmonths = 0;

        //                        if (EligibilityCategoryDetail.Attributes.Contains("bpa_eligibilityoffset"))
        //                            FA_eligibilityoffsetmonths = EligibilityCategoryDetail.GetAttributeValue<int>("bpa_eligibilityoffset");

        //                        if (EligibilityCategoryDetail.Attributes.Contains("bpa_deductionrate"))
        //                            FA_deductionrate = EligibilityCategoryDetail.GetAttributeValue<Money>("bpa_deductionrate");

        //                        if (EligibilityCategoryDetail.Attributes.Contains("bpa_fundassistanceamount"))
        //                            FA_fundassistanceamount = EligibilityCategoryDetail.GetAttributeValue<Money>("bpa_fundassistanceamount");
                                
        //                        RecalculationTransactionHelper.CreateFundAssistedTransactions(service,selectedContact.Id, startWorkMonth,
        //                            startWorkMonth.AddMonths(FA_eligibilityoffsetmonths), FA_fundassistanceamount, null,
        //                            (int)BpaTransactionbpaTransactionCategory.Eligibility, (int)BpaTransactionbpaTransactionType.FundAssited, defaultFund,
        //                            currentEligibilityCategory, defaultFund.Id);

        //                        RecalculationTransactionHelper.CreateFundAssistedTransactions(service,selectedContact.Id, startWorkMonth,
        //                            startWorkMonth.AddMonths(FA_eligibilityoffsetmonths),new Money(FA_deductionrate.Value * -1), null,
        //                            (int)BpaTransactionbpaTransactionCategory.Eligibility,(int)BpaTransactionbpaTransactionType.EligibilityDraw, defaultFund, null,
        //                            defaultFund.Id);

        //                        RecalculationTransactionHelper.CreateFundAssistedTransactions(service,selectedContact.Id, startWorkMonth,
        //                            startWorkMonth.AddMonths(FA_eligibilityoffsetmonths), null, null, (int)BpaTransactionbpaTransactionCategory.Eligibility,
        //                            (int)BpaTransactionbpaTransactionType.FundAssistedInBenefit, null,currentEligibilityCategory, defaultFund.Id);

        //                        //SET Current Eligibility
        //                        ContactHelper.SetCurrentEligibility(service, tracingService, selectedContact.Id,
        //                            (int)MempberPlanBpaCurrentEligibility.FundAssisted, startWorkMonth.AddMonths(FA_eligibilityoffsetmonths));

        //                        //Set SelfPay Counter to 0
        //                        selfPayCounter = 0;
                                
        //                        List<Entity> _lsEC = M_transactionsList.Where(x =>
        //                                    (x.GetAttributeValue<OptionSetValue>("bpa_transactiontype").Value == (int)BpaTransactionbpaTransactionType.EmployerContribution) &&
        //                                    (x.GetAttributeValue<DateTime>("bpa_transactiondate").ToShortDateString() == startWorkMonth.ToShortDateString())).ToList();
        //                        if (_lsEC != null && _lsEC.Count > 0)
        //                        {
        //                            foreach (Entity _e in _lsEC)
        //                            {
        //                                RecalculationTransactionHelper.UpdateElibilityCategory(service, _e.Id, currentEligibilityCategory);
        //                            }
        //                        }
        //                    }
        //                }

        //                #endregion

        //                ContactHelper.UpdateInactiveMonthsCounter(service, tracingService, selectedContact.Id, inactiveMonthCounter);
        //                /****************** ENDED FUND ASSISTED ***********************************/
        //            }
        //            else
        //            {
        //                if (HasReciprocalTransferIN)
        //                {
        //                    if(previousMonthEligibility == null)
        //                        currentEligibility = (int)MempberPlanBpaCurrentEligibility.NewMember;
        //                }
        //                //Make Look for all Transactions
        //                foreach (Entity transaction in M_transactions.Entities)
        //                {
        //                    decimal dollars = 0;
        //                    decimal hours = 0;
        //                    decimal wages = 0;
        //                    bool IsMissMatch = false;
        //                    bool MemberWentToUnderReview = false;
        //                    int SubSector_CalculationType = 0;

        //                    //Get All Transaction Values
        //                    int transactionType = transaction.GetAttributeValue<OptionSetValue>("bpa_transactiontype").Value;
        //                    if (transaction.Attributes.Contains("bpa_submission1.bpa_employer"))
        //                        Employer =
        //                            (EntityReference)
        //                                transaction.GetAttributeValue<AliasedValue>("bpa_submission1.bpa_employer")
        //                                    .Value;

        //                    //SET Work Month Eligibility Category
        //                    if (isMemberCategoryChanged)
        //                        UsingEligibilityCategoty = currentEligibilityCategory;
        //                    else
        //                    {
        //                        if (transaction.Attributes.Contains("bpa_currenteligibilitycategory"))
        //                            UsingEligibilityCategoty = transaction.GetAttributeValue<EntityReference>("bpa_currenteligibilitycategory");
        //                    }

        //                    //Get All Eligibility Values
        //                    int eligibilityOffset = 0;
        //                    if (UsingEligibilityCategoty != null)
        //                        eligibilityOffset = EligibilityCategoryHelper.FetchEligibilityOffSet(service,
        //                            UsingEligibilityCategoty.Id);

        //                    // IGNORE 2ND TRANSACTIONS FOR ELIGIBILITY DRAW *******************************************
        //                    if (skipTransactions.Contains(transaction.Id))
        //                    {
        //                        continue;
        //                    }

        //                    //Find out Memeber Elibiglity Category Calucation Type
        //                    //*************************THIS IS ADDED BECAUSE WE NEED TO RECALCUATED BY 'HOURS'****************************************
        //                    if (UsingEligibilityCategoty != null)
        //                    {
        //                        Entity EligibilityCategoryDetail = EligibilityCategoryHelper.FetchEligibilityCategory(service, UsingEligibilityCategoty.Id);
        //                        if (EligibilityCategoryDetail != null)
        //                        {
        //                            if (EligibilityCategoryDetail.Attributes.Contains("bpa_categorytype"))
        //                                EC_CalculationType =EligibilityCategoryDetail.GetAttributeValue<OptionSetValue>("bpa_categorytype").Value;

        //                            if (EligibilityCategoryDetail.Attributes.Contains("bpa_terminationcycle"))
        //                                maxInactiveCounter =EligibilityCategoryDetail.GetAttributeValue<int>("bpa_terminationcycle");
        //                        }
        //                    }
        //                    bool IsEligiblityHours = false;
        //                    if (EC_CalculationType == (int)BpaEligibilitycategorybpaCategoryType.Hours)
        //                        IsEligiblityHours = true;

        //                    ////*************** END OF ADDING **************************
                            
        //                    if (((transactionType == (int)BpaTransactionbpaTransactionType.FundAssited) && HasECED == false)
        //                        ||
        //                        (transactionType == (int)BpaTransactionbpaTransactionType.EmployerContribution) && (Employer != null) && 
        //                        (HasFundAssisted == false || isEligibilityCategoryFundAssisted == false))
        //                    {


        //                        inactiveMonthCounter = 0;

        //                        #region -------------- TRANSACTION TYPE = EMPLOYER CONTRIBUTION ------------

        //                        //SET Work Month Eligibility Category
        //                        if (isMemberCategoryChanged)
        //                            RecalculationTransactionHelper.UpdateElibilityCategory(service, transaction.Id, UsingEligibilityCategoty);

        //                        decimal HWRate = 0;

        //                        if (Employer != null)
        //                            HWRate = AccountHelper.GetHwRatesByEmployerId(service, Employer.Id, startWorkMonth.AddDays(1));

        //                        //Get Sum of Dollar Hours
        //                        decimal sumdollarhour = 0;
        //                        decimal deductionRate = 0;

        //                        if (IsEligiblityHours)
        //                            sumdollarhour = RecalculationTransactionHelper.GetSumHours(service, selectedContact.Id, startWorkMonth, defaultFund.Id);
        //                        else
        //                            sumdollarhour = RecalculationTransactionHelper.GetSumDollarHour(service, selectedContact.Id, startWorkMonth, defaultFund.Id);

        //                        if (IsEligiblityHours)
        //                            deductionRate = RecalculationTransactionHelper.GetEligibilityDeductionRate_Hours(service, UsingEligibilityCategoty.Id, startWorkMonth.AddDays(1)) * -1;
        //                        else
        //                            deductionRate = EligibilityCategoryHelper.GetEligibilityDeductionRate(service, UsingEligibilityCategoty.Id, startWorkMonth) * -1;

        //                        DateTime benefitmonth = startWorkMonth.AddMonths(eligibilityOffset);
        //                        //Find out Self-Pay draw Amount from Eligibility Category Detail 
        //                        decimal selfPayDeductionRate = EligibilityCategoryHelper.GetEligibilitySelfPayDeductionRate(service, UsingEligibilityCategoty.Id, startWorkMonth.AddDays(1)) * -1;

        //                        //Find out Self-Pay Bank from Member Plan
        //                        //Money SelfPayBank = Recalculation.ContactHelper.GetSelfPayBank(service, selectedContact.Id);
        //                        decimal selpaybank = TransactionHelper.FetchSumOfSelfPayBank(service, tracingService, selectedContact.Id, startWorkMonth);
        //                        Money SelfPayBank = new Money(selpaybank);

        //                        //Get SubSectory Calculation Type

        //                        #region -------- FOR MIS MATCH ( UNDER REVIEW ) ----------------------

        //                        if (Employer != null)
        //                        {
        //                            SubSector_CalculationType = AccountHelper.GetCalculationTypeByAccountId(service, tracingService, Employer.Id);

        //                            if (eligibilityCategoryType != (int)BpaEligibilitycategorybpaCategoryType.FundAssisted)
        //                            {
        //                                if ((SubSector_CalculationType == (int)BpaSubSectorBpaCalculationType.Flat || SubSector_CalculationType == (int)BpaSubSectorBpaCalculationType.Corporate) &&
        //                                    (eligibilityCategoryType != (int)BpaEligibilitycategorybpaCategoryType.Flat) && (eligibilityCategoryType != (int)BpaEligibilitycategorybpaCategoryType.Dollar))
        //                                    IsMissMatch = true;
        //                                else if (SubSector_CalculationType == (int)BpaSubSectorBpaCalculationType.DollarBased && eligibilityCategoryType != (int)BpaEligibilitycategorybpaCategoryType.Dollar)
        //                                    IsMissMatch = true;
        //                                else if (((SubSector_CalculationType == (int)BpaSubSectorBpaCalculationType.HourBased) || (SubSector_CalculationType ==
        //                                           (int)BpaSubSectorBpaCalculationType.Icinonici)) && (eligibilityCategoryType != (int)BpaEligibilitycategorybpaCategoryType.Hours) &&
        //                                         (eligibilityCategoryType != (int)BpaEligibilitycategorybpaCategoryType.Dollar))
        //                                    IsMissMatch = true;
        //                                else if (SubSector_CalculationType == (int)BpaSubSectorBpaCalculationType.Piecework && eligibilityCategoryType != (int)BpaEligibilitycategorybpaCategoryType.Dollar)
        //                                    IsMissMatch = true;
        //                                else
        //                                    IsMissMatch = false;
        //                            }
        //                            else
        //                                IsMissMatch = false;
        //                        }
        //                        else
        //                            IsMissMatch = false;

        //                        #endregion

        //                        bool IsFlatFlatCombination = false; //- FDC
        //                        if ((SubSector_CalculationType == (int)BpaSubSectorBpaCalculationType.Flat) && eligibilityCategoryType == (int)BpaEligibilitycategorybpaCategoryType.Flat)
        //                        {
        //                            IsFlatFlatCombination = true;
        //                            deductionRate = 0;
        //                        } //- FDC

        //                        //If Pervious Month Eligiblity is Under Review than Current Work Month also an 'Under Review '
        //                        if (currentEligibility == (int)BpaTransactionbpaTransactionType.UnderReview)
        //                        {
        //                            RecalculationTransactionHelper.CreateUnderReviewTransaction(service, selectedContact.Id, UsingEligibilityCategoty, startWorkMonth,
        //                                startWorkMonth.AddMonths(eligibilityOffset), (int)BpaTransactionbpaTransactionCategory.Eligibility, (int)BpaTransactionbpaTransactionType.UnderReview, defaultFund.Id);

        //                            ContactHelper.SetCurrentEligibility(service, tracingService, selectedContact.Id,
        //                                (int)MempberPlanBpaCurrentEligibility.UnderReview, startWorkMonth.AddMonths(eligibilityOffset));
        //                        }
        //                        else
        //                        {
        //                            if (transaction.Attributes.Contains("bpa_dollarhour"))
        //                                dollars = transaction.GetAttributeValue<Money>("bpa_dollarhour").Value;
        //                            if (transaction.Attributes.Contains("bpa_hours"))
        //                                hours = transaction.GetAttributeValue<decimal>("bpa_hours");
        //                            if (transaction.Attributes.Contains("bpa_wages"))
        //                                wages = transaction.GetAttributeValue<Money>("bpa_wages").Value;

        //                            if (dollars != 0 || hours > 0 || wages > 0 || SubSector_CalculationType == (int)BpaSubSectorBpaCalculationType.Flat
        //                                || SubSector_CalculationType == (int)BpaSubSectorBpaCalculationType.Corporate)
        //                            {
        //                                //Create Eligibility
        //                                if ((SubSector_CalculationType == (int)BpaSubSectorBpaCalculationType.Flat || (SubSector_CalculationType == (int)BpaSubSectorBpaCalculationType.Corporate))
        //                                    && eligibilityCategoryType == (int)BpaEligibilitycategorybpaCategoryType.Flat)
        //                                {
        //                                    decimal tmpDollarHours = 0;
        //                                    if (SubSector_CalculationType == (int)BpaSubSectorBpaCalculationType.Corporate)
        //                                    {
        //                                        tmpDollarHours = dollars * -1;
        //                                        deductionRate = tmpDollarHours;
        //                                    }
        //                                    else
        //                                    {
        //                                        if (HWRate != 0)
        //                                        {
        //                                            Money Transaction_dollarhours = transaction.GetAttributeValue<Money>("bpa_dollarhour"); //- FDC
        //                                            if (Transaction_dollarhours != null && Transaction_dollarhours.Value != 0 && Transaction_dollarhours.Value != HWRate) //- FDC
        //                                                HWRate = Transaction_dollarhours.Value; //- FDC

        //                                            tmpDollarHours = HWRate * -1;
        //                                        }
        //                                    }

        //                                    RecalculationTransactionHelper.CreateTransaction(service, selectedContact.Id, startWorkMonth,
        //                                        startWorkMonth.AddMonths(eligibilityOffset), defaultFund, (int)BpaTransactionbpaTransactionCategory.Eligibility,
        //                                        (int)BpaTransactionbpaTransactionType.EligibilityDraw, tmpDollarHours, false, null, defaultFund.Id);
        //                                }

        //                                List<Entity> _lsEC = M_transactionsList.Where(x =>
        //                                            (x.GetAttributeValue<OptionSetValue>("bpa_transactiontype").Value == (int)BpaTransactionbpaTransactionType.EmployerContribution) &&
        //                                            (x.GetAttributeValue<DateTime>("bpa_transactiondate").ToShortDateString() == startWorkMonth.ToShortDateString())).ToList();
        //                                if (_lsEC != null && _lsEC.Count > 0)
        //                                {
        //                                    foreach (Entity _e in _lsEC)
        //                                    {
        //                                        RecalculationTransactionHelper.UpdateElibilityCategory(service, _e.Id, UsingEligibilityCategoty);
        //                                    }
        //                                }

        //                                if (currentEligibility == (int)MempberPlanBpaCurrentEligibility.NewMember)
        //                                {
        //                                    #region ------------------- NEW MEMBER -----------------------

        //                                    if (IsMissMatch)
        //                                    {
        //                                        RecalculationTransactionHelper.CreateUnderReviewTransaction(service, selectedContact.Id, UsingEligibilityCategoty, startWorkMonth,
        //                                            startWorkMonth.AddMonths(eligibilityOffset), (int)BpaTransactionbpaTransactionCategory.Eligibility,
        //                                            (int)BpaTransactionbpaTransactionType.UnderReview, defaultFund.Id);

        //                                        ContactHelper.SetCurrentEligibility(service, tracingService, selectedContact.Id,
        //                                            (int)MempberPlanBpaCurrentEligibility.UnderReview, startWorkMonth.AddMonths(eligibilityOffset));

        //                                        MemberWentToUnderReview = true;
        //                                    }
        //                                    else
        //                                    {
        //                                        decimal miniInitEligibility = 0;

        //                                        if (IsEligiblityHours)
        //                                            miniInitEligibility =RecalculationTransactionHelper.GetMiniInitEligibility_Hours(service, UsingEligibilityCategoty.Id,
        //                                                    startWorkMonth.AddMonths(eligibilityOffset).AddDays(1));
        //                                        else if (IsFlatFlatCombination) //- FDC
        //                                            miniInitEligibility = 0; //- FDC
        //                                        else
        //                                            miniInitEligibility = EligibilityCategoryHelper.GetMiniInitEligibility(service, UsingEligibilityCategoty.Id,
        //                                                    startWorkMonth.AddMonths(eligibilityOffset).AddDays(1));

        //                                        if (sumdollarhour >= miniInitEligibility)
        //                                        {
        //                                            // WE ADDED THIS CONDITION BECAUSE OF FLAT - DOLLAR CONDITION [|| ((SubSector_CalculationType == (int)bpa_SubSector_bpa_CalculationType.Flat) && eligibilityCategoryType == (int)bpa_eligibilitycategorybpa_CategoryType.EmployerContribution))]
        //                                            if (((SubSector_CalculationType != (int)BpaSubSectorBpaCalculationType.Flat) && (SubSector_CalculationType != (int)BpaSubSectorBpaCalculationType.Corporate))
        //                                                ||
        //                                                ((SubSector_CalculationType ==(int)BpaSubSectorBpaCalculationType.Flat) && eligibilityCategoryType == (int)BpaEligibilitycategorybpaCategoryType.Dollar))
        //                                            {
        //                                                if (IsEligiblityHours)
        //                                                    RecalculationTransactionHelper.CreateDrawTransaction_Hour( service, selectedContact.Id, startWorkMonth,
        //                                                        startWorkMonth.AddMonths(eligibilityOffset),(int)BpaTransactionbpaTransactionCategory.Eligibility,
        //                                                        (int)BpaTransactionbpaTransactionType.EligibilityDraw, defaultFund, deductionRate, UsingEligibilityCategoty);
        //                                                else
        //                                                    RecalculationTransactionHelper.CreateTransaction(service, selectedContact.Id, startWorkMonth,
        //                                                        startWorkMonth.AddMonths(eligibilityOffset),defaultFund,(int)BpaTransactionbpaTransactionCategory.Eligibility,
        //                                                        (int)BpaTransactionbpaTransactionType.EligibilityDraw,deductionRate, false, null, defaultFund.Id);
        //                                            }

        //                                            ContactHelper.SetCurrentEligibility(service, tracingService, selectedContact.Id,
        //                                                (int)MempberPlanBpaCurrentEligibility.InBenefit, startWorkMonth.AddMonths(eligibilityOffset));

        //                                            RecalculationTransactionHelper.CreateTransaction(service,selectedContact.Id, startWorkMonth,
        //                                                startWorkMonth.AddMonths(eligibilityOffset),null, (int)BpaTransactionbpaTransactionCategory.Eligibility,
        //                                                (int)BpaTransactionbpaTransactionType.InBenefit,0, true, UsingEligibilityCategoty, defaultFund.Id);

        //                                            selfPayCounter = 0;
        //                                        }
        //                                        else
        //                                        {
        //                                            ContactHelper.SetCurrentEligibility(service, tracingService, selectedContact.Id,
        //                                                (int)MempberPlanBpaCurrentEligibility.NewMember, startWorkMonth.AddMonths(eligibilityOffset));

        //                                            RecalculationTransactionHelper.CreateTransaction(service,selectedContact.Id, startWorkMonth,
        //                                                startWorkMonth.AddMonths(eligibilityOffset),null, (int)BpaTransactionbpaTransactionCategory.Eligibility,
        //                                                (int)BpaTransactionbpaTransactionType.NewMember,0, true, UsingEligibilityCategoty, defaultFund.Id);
        //                                        }
        //                                    }

        //                                    #endregion
        //                                } //end of New Number
        //                                else if ((currentEligibility == (int)MempberPlanBpaCurrentEligibility.InBenefit) ||
        //                                         (currentEligibility == (int)MempberPlanBpaCurrentEligibility.SelfPayInBenefit))
        //                                {
        //                                    #region --------------- INBENEFIT & SELFPAY IN BENEIT -----------

        //                                    //Added by jwalin
        //                                    if (IsMissMatch)
        //                                    {
        //                                        #region ------ IS Miss Match ----------
        //                                        ContactHelper.SetCurrentEligibility(service, tracingService,selectedContact.Id,
        //                                            (int)MempberPlanBpaCurrentEligibility.UnderReview, startWorkMonth.AddMonths(eligibilityOffset));

        //                                        RecalculationTransactionHelper.CreateUnderReviewTransaction(
        //                                            service, selectedContact.Id, UsingEligibilityCategoty,
        //                                            startWorkMonth,
        //                                            startWorkMonth.AddMonths(eligibilityOffset),
        //                                            (int)BpaTransactionbpaTransactionCategory.Eligibility,
        //                                            (int)BpaTransactionbpaTransactionType.UnderReview, defaultFund.Id);

        //                                        MemberWentToUnderReview = true;
        //                                        #endregion
        //                                    }
        //                                    else
        //                                    {
        //                                        //Find MaxConsecutiveMonthsSelfPay from Eligibility Category 
        //                                        int MaxConsecutiveMonthsSelfPay = EligibilityCategoryHelper.GetMaxConsecutiveMonthsSelfPay(
        //                                                service, UsingEligibilityCategoty.Id);

        //                                        if (sumdollarhour + deductionRate >= 0)
        //                                        {
        //                                            #region ------------ Dollar Bank and Hour Bank has enough or not? ----------------
        //                                            if (((SubSector_CalculationType != (int)BpaSubSectorBpaCalculationType.Flat)
        //                                                && (SubSector_CalculationType != (int)BpaSubSectorBpaCalculationType.Corporate))
        //                                                //Added by jwalin
        //                                                ||
        //                                                ((SubSector_CalculationType == (int)BpaSubSectorBpaCalculationType.Flat) && eligibilityCategoryType ==
        //                                                    (int)BpaEligibilitycategorybpaCategoryType.Dollar))
        //                                            {
        //                                                if (IsEligiblityHours)
        //                                                    RecalculationTransactionHelper
        //                                                        .CreateDrawTransaction_Hour(service,
        //                                                            selectedContact.Id, startWorkMonth,
        //                                                            startWorkMonth.AddMonths(eligibilityOffset),
        //                                                            (int)
        //                                                                BpaTransactionbpaTransactionCategory
        //                                                                    .Eligibility,
        //                                                            (int)
        //                                                                BpaTransactionbpaTransactionType
        //                                                                    .EligibilityDraw,
        //                                                            defaultFund, deductionRate,
        //                                                            UsingEligibilityCategoty);
        //                                                else
        //                                                    RecalculationTransactionHelper.CreateTransaction(
        //                                                        service, selectedContact.Id, startWorkMonth,
        //                                                        startWorkMonth.AddMonths(eligibilityOffset),
        //                                                        defaultFund,
        //                                                        (int)BpaTransactionbpaTransactionCategory.Eligibility,
        //                                                        (int)BpaTransactionbpaTransactionType.EligibilityDraw,
        //                                                        deductionRate, false, null, defaultFund.Id);
        //                                            }

        //                                            if (previousMonthEligibility == null)
        //                                            {
        //                                                //decimal miniInitEligibility = EligibilityCategoryHelper.GetMiniInitEligibility(UsingEligibilityCategoty.Id,
        //                                                //    startWorkMonth.AddMonths(eligibilityOffset).AddDays(1));
        //                                                decimal miniInitEligibility = 0;

        //                                                if (IsEligiblityHours)
        //                                                    miniInitEligibility =
        //                                                        RecalculationTransactionHelper
        //                                                            .GetMiniInitEligibility_Hours(service,
        //                                                                UsingEligibilityCategoty.Id,
        //                                                                startWorkMonth.AddMonths(eligibilityOffset)
        //                                                                    .AddDays(1));
        //                                                else
        //                                                    miniInitEligibility =
        //                                                        EligibilityCategoryHelper.GetMiniInitEligibility(
        //                                                            service, UsingEligibilityCategoty.Id,
        //                                                            startWorkMonth.AddMonths(eligibilityOffset)
        //                                                                .AddDays(1));

        //                                                if (sumdollarhour >= miniInitEligibility)
        //                                                {
        //                                                    ContactHelper.SetCurrentEligibility(service, tracingService,
        //                                                        selectedContact.Id,
        //                                                        (int)MempberPlanBpaCurrentEligibility.InBenefit, startWorkMonth.AddMonths(eligibilityOffset));

        //                                                    RecalculationTransactionHelper.CreateTransaction(
        //                                                        service, selectedContact.Id, startWorkMonth,
        //                                                        startWorkMonth.AddMonths(eligibilityOffset),
        //                                                        null,
        //                                                        (int)BpaTransactionbpaTransactionCategory.Eligibility,
        //                                                        (int)BpaTransactionbpaTransactionType.InBenefit,
        //                                                        0, true, UsingEligibilityCategoty, defaultFund.Id);

        //                                                    //ContactHelper.SetConsecutiveSelfPayMonths(selectedContact.Id, true);
        //                                                    selfPayCounter = 0;
        //                                                }
        //                                                else
        //                                                {
        //                                                    ContactHelper.SetCurrentEligibility(service, tracingService,
        //                                                        selectedContact.Id,
        //                                                        (int)MempberPlanBpaCurrentEligibility.NewMember, startWorkMonth.AddMonths(eligibilityOffset));

        //                                                    RecalculationTransactionHelper.CreateTransaction(
        //                                                        service, selectedContact.Id, startWorkMonth,
        //                                                        startWorkMonth.AddMonths(eligibilityOffset),
        //                                                        null,
        //                                                        (int)BpaTransactionbpaTransactionCategory.Eligibility,
        //                                                        (int)BpaTransactionbpaTransactionType.NewMember,
        //                                                        0, true, UsingEligibilityCategoty, defaultFund.Id);
        //                                                }
        //                                            }
        //                                            else
        //                                            {
        //                                                //if (currentEligibility == (int)MempberPlan_bpa_CurrentEligibility.SelfPayInBenefit)
        //                                                ContactHelper.SetCurrentEligibility(service, tracingService,
        //                                                    selectedContact.Id,
        //                                                    (int)MempberPlanBpaCurrentEligibility.InBenefit, startWorkMonth.AddMonths(eligibilityOffset));

        //                                                RecalculationTransactionHelper.CreateTransaction(service,
        //                                                    selectedContact.Id, startWorkMonth,
        //                                                    startWorkMonth.AddMonths(eligibilityOffset),
        //                                                    null, (int)BpaTransactionbpaTransactionCategory.Eligibility,
        //                                                    (int)BpaTransactionbpaTransactionType.InBenefit,
        //                                                    0, true, UsingEligibilityCategoty, defaultFund.Id);

        //                                                //ContactHelper.SetConsecutiveSelfPayMonths(selectedContact.Id, true);
        //                                                selfPayCounter = 0;
        //                                            }
        //                                            #endregion
        //                                        }
        //                                        else if ((selfPayDeductionRate != 0) && (SelfPayBank.Value >= (selfPayDeductionRate * -1)))
        //                                        {
        //                                            #region ------------- Self Pay Bank Checking ------------------

        //                                            //Find out MAx Month of Employer Contribution Contribution (if not found return WorkMonth)
        //                                            DateTime ECMaxDate = TransactionHelper.FetchMaxEmployerContributionDate(service, tracingService, selectedContact.Id, startWorkMonth);

        //                                            //Find out How many Previously SELFPAY
        //                                            int cntSP = 0;
        //                                            Entity lastEC = RecalculationTransactionHelper.FetchLastEligiblityMonthExceptSelfPay(service, tracingService,
        //                                                    ECMaxDate, ECMaxDate, selectedContact.Id);
        //                                            if (lastEC != null && lastEC.Attributes.Contains("bpa_transactiondate"))
        //                                            {
        //                                                DateTime end = startWorkMonth.AddMonths(-1);
        //                                                DateTime start =
        //                                                    lastEC.GetAttributeValue<DateTime>("bpa_transactiondate");
        //                                                cntSP = end.Month + end.Year * 12 - (start.Month + start.Year * 12);
        //                                            }
        //                                            if (cntSP == 0)
        //                                            {
        //                                                cntSP = RecalculationTransactionHelper.GetHowManySelfPay(service, tracingService, ECMaxDate, startWorkMonth, selectedContact.Id);
        //                                            }

        //                                            if (cntSP < MaxConsecutiveMonthsSelfPay)
        //                                            {
        //                                                // Create SelfPay draw and Self pay In Benefit Transaction
        //                                                RecalculationTransactionHelper.CreateTransaction(
        //                                                service, selectedContact.Id, startWorkMonth,
        //                                                startWorkMonth.AddMonths(eligibilityOffset),
        //                                                defaultFund,
        //                                                (int)BpaTransactionbpaTransactionCategory.Eligibility,
        //                                                (int)BpaTransactionbpaTransactionType.SelfPayDraw,
        //                                                selfPayDeductionRate, false, UsingEligibilityCategoty, defaultFund.Id);

        //                                                RecalculationTransactionHelper.CreateTransaction(service,
        //                                                   selectedContact.Id, startWorkMonth,
        //                                                   startWorkMonth.AddMonths(eligibilityOffset),
        //                                                   null, (int)BpaTransactionbpaTransactionCategory.Eligibility,
        //                                                   (int)BpaTransactionbpaTransactionType.SelfPayInBenefit,
        //                                                   0, true, UsingEligibilityCategoty, defaultFund.Id);

        //                                                // Update Selfpay counter 
        //                                                selfPayCounter = cntSP + 1;
        //                                                // Trigger Self Pay Bank Rollup
        //                                                PluginHelper.TriggerRollup(service, tracingService, "bpa_memberplan", selectedContact.Id, "bpa_selfpaybank");

        //                                                ContactHelper.SetCurrentEligibility(service, tracingService,selectedContact.Id,
        //                                                    (int)MempberPlanBpaCurrentEligibility.SelfPayInBenefit, startWorkMonth.AddMonths(eligibilityOffset));
        //                                            }
        //                                            else
        //                                            {
        //                                                ContactHelper.SetCurrentEligibility(service, tracingService,selectedContact.Id,
        //                                                    (int)MempberPlanBpaCurrentEligibility.NotInBenefit, startWorkMonth.AddMonths(eligibilityOffset));

        //                                                RecalculationTransactionHelper.CreateTransaction(service,
        //                                                    selectedContact.Id, startWorkMonth,
        //                                                    startWorkMonth.AddMonths(eligibilityOffset),
        //                                                    null, (int)BpaTransactionbpaTransactionCategory.Eligibility,
        //                                                    (int)BpaTransactionbpaTransactionType.NotInBenefit,
        //                                                    0, true, UsingEligibilityCategoty, defaultFund.Id);
        //                                                //inactiveMonthCounter = consecutiveinactivemonths + 1;
        //                                            }
        //                                            #endregion
        //                                        }
        //                                        else
        //                                        {
        //                                            #region ---------- Else Part --------
        //                                            if (HasSelfPayInBenefit)
        //                                            {
        //                                            }
        //                                            else
        //                                            {
        //                                                ContactHelper.SetCurrentEligibility(service, tracingService,
        //                                                    selectedContact.Id,
        //                                                    (int)MempberPlanBpaCurrentEligibility.NotInBenefit, startWorkMonth.AddMonths(eligibilityOffset));

        //                                                RecalculationTransactionHelper.CreateTransaction(service,
        //                                                    selectedContact.Id, startWorkMonth,
        //                                                    startWorkMonth.AddMonths(eligibilityOffset),
        //                                                    null, (int)BpaTransactionbpaTransactionCategory.Eligibility,
        //                                                    (int)BpaTransactionbpaTransactionType.NotInBenefit,
        //                                                    0, true, UsingEligibilityCategoty, defaultFund.Id);
        //                                            }
        //                                            #endregion
        //                                        }
        //                                    }

        //                                    #endregion
        //                                }
        //                                else if ((currentEligibility == (int)MempberPlanBpaCurrentEligibility.NotInBenefit) ||
        //                                         (currentEligibility == (int)MempberPlanBpaCurrentEligibility.Reinstatement))
        //                                {
        //                                    #region  -------------- NOT IN BENEFIT ----------------------------

        //                                    bool IsResinstatement =
        //                                        EligibilityCategoryHelper.IsCategoryReinstatement(service,
        //                                            UsingEligibilityCategoty.Id);
        //                                    decimal reinstatementAmount = 0;

        //                                    if (IsResinstatement)
        //                                    {
        //                                        if (IsEligiblityHours)
        //                                            reinstatementAmount =
        //                                                EligibilityCategoryHelper.GetReinstatementHours(
        //                                                    service, UsingEligibilityCategoty.Id,
        //                                                    startWorkMonth);
        //                                        else
        //                                            reinstatementAmount =
        //                                                EligibilityCategoryHelper.GetReinstatementDollars(
        //                                                    service, UsingEligibilityCategoty.Id,
        //                                                    startWorkMonth);

        //                                        if (reinstatementAmount == 0)
        //                                            IsMissMatch = true;
        //                                    }

        //                                    //Added by jwalin
        //                                    if (IsMissMatch)
        //                                    {
        //                                        ContactHelper.SetCurrentEligibility(service, tracingService,selectedContact.Id,
        //                                            (int)MempberPlanBpaCurrentEligibility.UnderReview, startWorkMonth.AddMonths(eligibilityOffset));

        //                                        RecalculationTransactionHelper.CreateUnderReviewTransaction(
        //                                            service, selectedContact.Id, UsingEligibilityCategoty,
        //                                            startWorkMonth,
        //                                            startWorkMonth.AddMonths(eligibilityOffset),
        //                                            (int)BpaTransactionbpaTransactionCategory.Eligibility,
        //                                            (int)BpaTransactionbpaTransactionType.UnderReview,
        //                                            defaultFund.Id);

        //                                        MemberWentToUnderReview = true;
        //                                    }
        //                                    else
        //                                    {
        //                                        if (IsResinstatement && HasECED)
        //                                        {
        //                                            if (sumdollarhour >= reinstatementAmount)
        //                                            {
        //                                                if (IsEligiblityHours)
        //                                                    RecalculationTransactionHelper
        //                                                        .CreateDrawTransaction_Hour(service,
        //                                                            selectedContact.Id, startWorkMonth,
        //                                                            startWorkMonth.AddMonths(eligibilityOffset),
        //                                                            (int)
        //                                                                BpaTransactionbpaTransactionCategory
        //                                                                    .Eligibility,
        //                                                            (int)
        //                                                                BpaTransactionbpaTransactionType
        //                                                                    .EligibilityDraw,
        //                                                            defaultFund, deductionRate,
        //                                                            UsingEligibilityCategoty);
        //                                                else
        //                                                    RecalculationTransactionHelper.CreateTransaction(
        //                                                        service, selectedContact.Id, startWorkMonth,
        //                                                        startWorkMonth.AddMonths(eligibilityOffset),
        //                                                        defaultFund,
        //                                                        (int)
        //                                                            BpaTransactionbpaTransactionCategory
        //                                                                .Eligibility,
        //                                                        (int)
        //                                                            BpaTransactionbpaTransactionType
        //                                                                .EligibilityDraw, deductionRate, false,
        //                                                        null, defaultFund.Id);

        //                                                ContactHelper.SetCurrentEligibility(service, tracingService,
        //                                                    selectedContact.Id,
        //                                                    (int)MempberPlanBpaCurrentEligibility.InBenefit, startWorkMonth.AddMonths(eligibilityOffset));

        //                                                RecalculationTransactionHelper.CreateTransaction(
        //                                                    service, selectedContact.Id, startWorkMonth,
        //                                                    startWorkMonth.AddMonths(eligibilityOffset),
        //                                                    null,
        //                                                    (int)BpaTransactionbpaTransactionCategory.Eligibility,
        //                                                    (int)BpaTransactionbpaTransactionType.InBenefit,
        //                                                    0, true, UsingEligibilityCategoty, defaultFund.Id);

        //                                                selfPayCounter = 0;
        //                                            }
        //                                            else
        //                                            {
        //                                                RecalculationTransactionHelper.CreateTransaction(
        //                                                    service, selectedContact.Id, startWorkMonth,
        //                                                    startWorkMonth.AddMonths(eligibilityOffset),
        //                                                    null,
        //                                                    (int)BpaTransactionbpaTransactionCategory.Eligibility,
        //                                                    (int)BpaTransactionbpaTransactionType.ReInstateing,
        //                                                    0, true, UsingEligibilityCategoty, defaultFund.Id);

        //                                                ContactHelper.SetCurrentEligibility(service, tracingService,
        //                                                    selectedContact.Id,
        //                                                    (int)MempberPlanBpaCurrentEligibility.Reinstatement, startWorkMonth.AddMonths(eligibilityOffset));
        //                                            }
        //                                        }
        //                                        else
        //                                        {
        //                                            if (sumdollarhour + deductionRate >= 0)
        //                                            {
        //                                                if (((SubSector_CalculationType != (int)BpaSubSectorBpaCalculationType.Flat) &&
        //                                                     (SubSector_CalculationType != (int)BpaSubSectorBpaCalculationType.Corporate))
        //                                                    //Added by jwalin
        //                                                    || ((SubSector_CalculationType == (int)BpaSubSectorBpaCalculationType.Flat) &&
        //                                                     eligibilityCategoryType == (int)BpaEligibilitycategorybpaCategoryType.Dollar))
        //                                                {
        //                                                    if (IsEligiblityHours)
        //                                                        RecalculationTransactionHelper
        //                                                            .CreateDrawTransaction_Hour(service,
        //                                                                selectedContact.Id, startWorkMonth,
        //                                                                startWorkMonth.AddMonths(
        //                                                                    eligibilityOffset),
        //                                                                (int)
        //                                                                    BpaTransactionbpaTransactionCategory
        //                                                                        .Eligibility,
        //                                                                (int)
        //                                                                    BpaTransactionbpaTransactionType
        //                                                                        .EligibilityDraw,
        //                                                                defaultFund, deductionRate,
        //                                                                UsingEligibilityCategoty);
        //                                                    else
        //                                                        RecalculationTransactionHelper.CreateTransaction
        //                                                            (service, selectedContact.Id,
        //                                                                startWorkMonth,
        //                                                                startWorkMonth.AddMonths(
        //                                                                    eligibilityOffset),
        //                                                                defaultFund,
        //                                                                (int)
        //                                                                    BpaTransactionbpaTransactionCategory
        //                                                                        .Eligibility,
        //                                                                (int)
        //                                                                    BpaTransactionbpaTransactionType
        //                                                                        .EligibilityDraw, deductionRate,
        //                                                                false, null, defaultFund.Id);
        //                                                }
        //                                                if (previousMonthEligibility == null)
        //                                                {
        //                                                    ContactHelper.SetCurrentEligibility(service, tracingService,
        //                                                        selectedContact.Id,
        //                                                        (int)MempberPlanBpaCurrentEligibility.NewMember, startWorkMonth.AddMonths(eligibilityOffset));

        //                                                    RecalculationTransactionHelper.CreateTransaction(
        //                                                        service, selectedContact.Id, startWorkMonth,
        //                                                        startWorkMonth.AddMonths(eligibilityOffset),
        //                                                        null,
        //                                                        (int)
        //                                                            BpaTransactionbpaTransactionCategory
        //                                                                .Eligibility,
        //                                                        (int)BpaTransactionbpaTransactionType.NewMember,
        //                                                        0, true, UsingEligibilityCategoty,
        //                                                        defaultFund.Id);
        //                                                }
        //                                                else
        //                                                {
        //                                                    ContactHelper.SetCurrentEligibility(service, tracingService,
        //                                                        selectedContact.Id,
        //                                                        (int)MempberPlanBpaCurrentEligibility.InBenefit, startWorkMonth.AddMonths(eligibilityOffset));

        //                                                    RecalculationTransactionHelper.CreateTransaction(
        //                                                        service, selectedContact.Id, startWorkMonth,
        //                                                        startWorkMonth.AddMonths(eligibilityOffset),
        //                                                        null,
        //                                                        (int)
        //                                                            BpaTransactionbpaTransactionCategory
        //                                                                .Eligibility,
        //                                                        (int)BpaTransactionbpaTransactionType.InBenefit,
        //                                                        0, true, UsingEligibilityCategoty,
        //                                                        defaultFund.Id);

        //                                                    //ContactHelper.SetConsecutiveSelfPayMonths(selectedContact.Id, true);
        //                                                    selfPayCounter = 0;
        //                                                }
        //                                            }
        //                                            /*else if (SelfPayBank.Value >= (selfPayDeductionRate * -1))
        //                                            {
        //                                                #region ------------- Self Pay Bank Checking ------------------
                                                        
        //                                                //Find MaxConsecutiveMonthsSelfPay from Eligibility Category 
        //                                                int MaxConsecutiveMonthsSelfPay = EligibilityCategoryHelper.GetMaxConsecutiveMonthsSelfPay(
        //                                                        service, UsingEligibilityCategoty.Id);

        //                                                //Find out MAx Month of Employer Contribution Contribution (if not found return WorkMonth)
        //                                                DateTime ECMaxDate = TransactionHelper.FetchMaxEmployerContributionDate(service, tracingService, selectedContact.Id, startWorkMonth);

        //                                                //Find out How many Previously SELFPAY
        //                                                int cntSP = 0;
        //                                                Entity lastEC = RecalculationTransactionHelper.FetchLastEligiblityMonthExceptSelfPay(service, tracingService,
        //                                                    ECMaxDate, ECMaxDate, selectedContact.Id);
        //                                                if (lastEC != null && lastEC.Attributes.Contains("bpa_transactiondate"))
        //                                                {
        //                                                    DateTime end = startWorkMonth.AddMonths(-1);
        //                                                    DateTime start =
        //                                                        lastEC.GetAttributeValue<DateTime>("bpa_transactiondate");
        //                                                    cntSP = end.Month + end.Year * 12 - (start.Month + start.Year * 12);
        //                                                }
        //                                                if (cntSP == 0)
        //                                                {
        //                                                    cntSP = RecalculationTransactionHelper.GetHowManySelfPay(service, tracingService, ECMaxDate, startWorkMonth, selectedContact.Id);
        //                                                }

        //                                                if (cntSP < MaxConsecutiveMonthsSelfPay)
        //                                                {
        //                                                    // Create SelfPay draw and Self pay In Benefit Transaction
        //                                                    RecalculationTransactionHelper.CreateTransaction(
        //                                                    service, selectedContact.Id, startWorkMonth,
        //                                                    startWorkMonth.AddMonths(eligibilityOffset),
        //                                                    defaultFund,
        //                                                    (int)BpaTransactionbpaTransactionCategory.Eligibility,
        //                                                    (int)BpaTransactionbpaTransactionType.SelfPayDraw,
        //                                                    selfPayDeductionRate, false, UsingEligibilityCategoty, defaultFund.Id);

        //                                                    RecalculationTransactionHelper.CreateTransaction(service,
        //                                                       selectedContact.Id, startWorkMonth,
        //                                                       startWorkMonth.AddMonths(eligibilityOffset),
        //                                                       null, (int)BpaTransactionbpaTransactionCategory.Eligibility,
        //                                                       (int)BpaTransactionbpaTransactionType.SelfPayInBenefit,
        //                                                       0, true, UsingEligibilityCategoty, defaultFund.Id);

        //                                                    // Update Selfpay counter 
        //                                                    selfPayCounter = cntSP + 1;
        //                                                    // Trigger Self Pay Bank Rollup
        //                                                    PluginHelper.TriggerRollup(service, tracingService, "bpa_memberplan", selectedContact.Id, "bpa_selfpaybank");

        //                                                    if (selfPayCounter == MaxConsecutiveMonthsSelfPay)
        //                                                    {
        //                                                        //Find out Self PAy Bank
        //                                                        decimal endingSelfPayBank = RecalculationTransactionHelper.FetchSelfPayBank(service, selectedContact.Id, startWorkMonth);

        //                                                        //CREATE "SelfPay Max Consecutive Reach" Transaction
        //                                                        RecalculationTransactionHelper.CreateSelfPayMaxReach(service,
        //                                                            startWorkMonth, startWorkMonth.AddMonths(eligibilityOffset),
        //                                                            selectedContact.Id, null);

        //                                                        //if(endingSelfPayBank > 0)
        //                                                        //{
        //                                                        //    //Create Self Pay Refund

        //                                                        //}
        //                                                    }
        //                                                }
        //                                                else
        //                                                {
        //                                                    ContactHelper.SetCurrentEligibility(service, tracingService,selectedContact.Id,
        //                                                        (int)MempberPlanBpaCurrentEligibility.NotInBenefit);

        //                                                    RecalculationTransactionHelper.CreateTransaction(service,
        //                                                        selectedContact.Id, startWorkMonth,
        //                                                        startWorkMonth.AddMonths(eligibilityOffset),
        //                                                        null, (int)BpaTransactionbpaTransactionCategory.Eligibility,
        //                                                        (int)BpaTransactionbpaTransactionType.NotInBenefit,
        //                                                        0, true, UsingEligibilityCategoty, defaultFund.Id);
        //                                                    //inactiveMonthCounter = consecutiveinactivemonths + 1;
        //                                                }
        //                                                #endregion

        //                                            }*/
        //                                            else
        //                                            {
        //                                                ContactHelper.SetCurrentEligibility(service, tracingService,
        //                                                    selectedContact.Id,
        //                                                    (int)MempberPlanBpaCurrentEligibility.NotInBenefit, startWorkMonth.AddMonths(eligibilityOffset));

        //                                                RecalculationTransactionHelper.CreateTransaction(
        //                                                    service, selectedContact.Id, startWorkMonth,
        //                                                    startWorkMonth.AddMonths(eligibilityOffset),
        //                                                    null,
        //                                                    (int)BpaTransactionbpaTransactionCategory.Eligibility,
        //                                                    (int)BpaTransactionbpaTransactionType.NotInBenefit,
        //                                                    0, true, UsingEligibilityCategoty, defaultFund.Id);
        //                                            }
        //                                        }
        //                                    }

        //                                    #endregion
        //                                }
        //                                else if (currentEligibility == (int)MempberPlanBpaCurrentEligibility.UnderReview)
        //                                {
        //                                    #region -------------------- UNDER REVIEW --------------------

        //                                    if (previousMonthEligibility == null)
        //                                    {
        //                                        //decimal miniInitEligibility = EligibilityCategoryHelper.GetMiniInitEligibility(UsingEligibilityCategoty.Id,
        //                                        //    startWorkMonth.AddMonths(eligibilityOffset).AddDays(1));
        //                                        decimal miniInitEligibility = 0;

        //                                        if (IsEligiblityHours)
        //                                            miniInitEligibility =
        //                                                RecalculationTransactionHelper
        //                                                    .GetMiniInitEligibility_Hours(service,
        //                                                        UsingEligibilityCategoty.Id,
        //                                                        startWorkMonth.AddMonths(eligibilityOffset)
        //                                                            .AddDays(1));
        //                                        else
        //                                            miniInitEligibility =
        //                                                EligibilityCategoryHelper.GetMiniInitEligibility(
        //                                                    service, UsingEligibilityCategoty.Id,
        //                                                    startWorkMonth.AddMonths(eligibilityOffset)
        //                                                        .AddDays(1));

        //                                        if (sumdollarhour >= miniInitEligibility)
        //                                        {
        //                                            if (SubSector_CalculationType !=
        //                                                (int)BpaSubSectorBpaCalculationType.Flat)
        //                                            {
        //                                                if (IsEligiblityHours)
        //                                                    RecalculationTransactionHelper
        //                                                        .CreateDrawTransaction_Hour(service,
        //                                                            selectedContact.Id, startWorkMonth,
        //                                                            startWorkMonth.AddMonths(
        //                                                                eligibilityOffset),
        //                                                            (int)
        //                                                                BpaTransactionbpaTransactionCategory
        //                                                                    .Eligibility,
        //                                                            (int)
        //                                                                BpaTransactionbpaTransactionType
        //                                                                    .EligibilityDraw,
        //                                                            defaultFund, deductionRate,
        //                                                            UsingEligibilityCategoty);
        //                                                else
        //                                                    RecalculationTransactionHelper.CreateTransaction
        //                                                        (service, selectedContact.Id,
        //                                                            startWorkMonth,
        //                                                            startWorkMonth.AddMonths(
        //                                                                eligibilityOffset),
        //                                                            defaultFund,
        //                                                            (int)
        //                                                                BpaTransactionbpaTransactionCategory
        //                                                                    .Eligibility,
        //                                                            (int)
        //                                                                BpaTransactionbpaTransactionType
        //                                                                    .EligibilityDraw, deductionRate,
        //                                                            false, null, defaultFund.Id);
        //                                            }

        //                                            ContactHelper.SetCurrentEligibility(service, tracingService,
        //                                                selectedContact.Id,
        //                                                (int)MempberPlanBpaCurrentEligibility.InBenefit, startWorkMonth.AddMonths(eligibilityOffset));

        //                                            RecalculationTransactionHelper.CreateTransaction(
        //                                                service, selectedContact.Id, startWorkMonth,
        //                                                startWorkMonth.AddMonths(eligibilityOffset),
        //                                                null,
        //                                                (int)BpaTransactionbpaTransactionCategory.Eligibility,
        //                                                (int)BpaTransactionbpaTransactionType.InBenefit,
        //                                                0, true, UsingEligibilityCategoty, defaultFund.Id);

        //                                            //ContactHelper.SetConsecutiveSelfPayMonths(selectedContact.Id, true);
        //                                            selfPayCounter = 0;
        //                                        }
        //                                        else
        //                                        {
        //                                            ContactHelper.SetCurrentEligibility(service, tracingService,
        //                                                selectedContact.Id,
        //                                                (int)MempberPlanBpaCurrentEligibility.NewMember, startWorkMonth.AddMonths(eligibilityOffset));

        //                                            RecalculationTransactionHelper.CreateTransaction(
        //                                                service, selectedContact.Id, startWorkMonth,
        //                                                startWorkMonth.AddMonths(eligibilityOffset),
        //                                                null,
        //                                                (int)BpaTransactionbpaTransactionCategory.Eligibility,
        //                                                (int)BpaTransactionbpaTransactionType.NewMember,
        //                                                0, true, UsingEligibilityCategoty, defaultFund.Id);
        //                                        }
        //                                    }
        //                                    else
        //                                    {
        //                                        ContactHelper.SetCurrentEligibility(service, tracingService,
        //                                            selectedContact.Id,
        //                                            (int)MempberPlanBpaCurrentEligibility.UnderReview, startWorkMonth.AddMonths(eligibilityOffset));

        //                                        RecalculationTransactionHelper.CreateUnderReviewTransaction(
        //                                            service, selectedContact.Id, UsingEligibilityCategoty,
        //                                            startWorkMonth,
        //                                            startWorkMonth.AddMonths(eligibilityOffset),
        //                                            (int)BpaTransactionbpaTransactionCategory.Eligibility,
        //                                            (int)BpaTransactionbpaTransactionType.UnderReview,
        //                                            defaultFund.Id);

        //                                        MemberWentToUnderReview = true;
        //                                    }

        //                                    #endregion
        //                                }
        //                            }
        //                        }

        //                        #endregion

        //                        #region -------------- RESERVE TRANSAFER  -----------------------------

        //                        if (UsingEligibilityCategoty != null)
        //                        {
        //                            //VERIFY THIS MONTH ALREDY HAS RESERVER TRANSFER OR NOT??
        //                            bool _IsReserveExists = RecalculationTransactionHelper.IsReserveExists(service,
        //                                selectedContact.Id, startWorkMonth);

        //                            if (_IsReserveExists == false)
        //                            {
        //                                if (IsEligiblityHours)
        //                                {
        //                                    decimal maxMemberBank =
        //                                        RecalculationTransactionHelper.GetMaximumMemberBank_Hour(service,
        //                                            UsingEligibilityCategoty.Id, startWorkMonth.AddDays(1));
        //                                    decimal currentsumdollarhour = RecalculationTransactionHelper.GetSumHours(service, selectedContact.Id,
        //                                            startWorkMonth, defaultFund.Id);
        //                                    decimal totalEmployerContribution = RecalculationTransactionHelper.GetSumOfHours(service,
        //                                            selectedContact.Id, startWorkMonth,
        //                                            (int)BpaTransactionbpaTransactionType.EmployerContribution);
        //                                    decimal totalEligibilityDraw = RecalculationTransactionHelper.GetSumOfHours(service,
        //                                            selectedContact.Id, startWorkMonth, (int)BpaTransactionbpaTransactionType.EligibilityDraw);
        //                                    decimal totalReserveTransfer =
        //                                        RecalculationTransactionHelper.GetSumOfHours(service, selectedContact.Id, startWorkMonth,
        //                                            (int)BpaTransactionbpaTransactionType.ReserveTransfer);

        //                                    //We remove this [(maxMemberBank != 0) &&] condition because of FLAT - DOLLAR Combination 
        //                                    if (currentsumdollarhour >= maxMemberBank)
        //                                    {
        //                                        decimal reserveTransferAmount = maxMemberBank -
        //                                                                        (totalEmployerContribution +
        //                                                                         totalEligibilityDraw +
        //                                                                         totalReserveTransfer);
        //                                        if ((reserveTransferAmount < 0) && (MemberWentToUnderReview == false) &&
        //                                            (currentEligibility != (int)MempberPlanBpaCurrentEligibility.Frozen))

        //                                        //TFS 107, TFS 108, TFS 109
        //                                        {
        //                                            RecalculationTransactionHelper.CreatReserveTransfer_Hour(service,
        //                                                selectedContact.Id, startWorkMonth,
        //                                                startWorkMonth.AddMonths(eligibilityOffset),
        //                                                reserveTransferAmount, defaultFund.Id);

        //                                            //Get the Standing Transaction(like Not In Benefite, In Benefit .. )for that same month 
        //                                            RecalculationTransactionHelper.UpdateEndingBalanace(service,
        //                                                startWorkMonth, selectedContact.Id, defaultFund.Id);
        //                                        }
        //                                    }
        //                                }
        //                                else
        //                                {
        //                                    //int eligibilityOffset1 = EligibilityCategoryHelper.FetchEligibilityOffSet(UsingEligibilityCategoty.Id);
        //                                    decimal maxMemberBank =
        //                                        EligibilityCategoryHelper.GetMaximumMemberBank(service,
        //                                            UsingEligibilityCategoty.Id, startWorkMonth);
        //                                    decimal currentsumdollarhour =
        //                                        RecalculationTransactionHelper.GetSumDollarHour(service,
        //                                            selectedContact.Id, startWorkMonth, defaultFund.Id);
        //                                    decimal totalEmployerContribution =
        //                                        RecalculationTransactionHelper.GetSumAmount(service, selectedContact.Id,
        //                                            startWorkMonth,
        //                                            (int)BpaTransactionbpaTransactionType.EmployerContribution);
        //                                    decimal totalEligibilityDraw =
        //                                        RecalculationTransactionHelper.GetSumAmount(service, selectedContact.Id,
        //                                            startWorkMonth, (int)BpaTransactionbpaTransactionType.EligibilityDraw);
        //                                    decimal totalReserveTransfer =
        //                                        RecalculationTransactionHelper.GetSumAmount(service, selectedContact.Id,
        //                                            startWorkMonth, (int)BpaTransactionbpaTransactionType.ReserveTransfer);

        //                                    //We remove this [(maxMemberBank != 0) &&] condition because of FLAT - DOLLAR Combination 
        //                                    if (currentsumdollarhour >= maxMemberBank)

        //                                    //Added condition by jwalin (maxMemberBank != 0) 
        //                                    {
        //                                        decimal reserveTransferAmount = maxMemberBank -
        //                                                                        (totalEmployerContribution +
        //                                                                         totalEligibilityDraw +
        //                                                                         totalReserveTransfer);
        //                                        if ((reserveTransferAmount < 0) && (MemberWentToUnderReview == false) &&
        //                                            (currentEligibility != (int)MempberPlanBpaCurrentEligibility.Frozen))

        //                                        //TFS 107, TFS 108, TFS 109
        //                                        {
        //                                            RecalculationTransactionHelper.CreatReserveTransfer(service,
        //                                                selectedContact.Id, startWorkMonth,
        //                                                startWorkMonth.AddMonths(eligibilityOffset),
        //                                                reserveTransferAmount, defaultFund.Id);

        //                                            //Get the Standing Transaction(like Not In Benefite, In Benefit .. )for that same month 
        //                                            RecalculationTransactionHelper.UpdateEndingBalanace(service,
        //                                                startWorkMonth, selectedContact.Id, defaultFund.Id);
        //                                        }
        //                                    }
        //                                }
        //                            }
        //                        }

        //                        #endregion
        //                    }
        //                    else
        //                    {
        //                        if (HasECED == false)
        //                        {
        //                            int consecutiveinactivemonths = 0;
                                    
        //                            Entity lastEligibility = RecalculationTransactionHelper.FetchLastBenefitMonth(service, startWorkMonth, memberId);
        //                            if (lastEligibility != null && lastEligibility.Attributes.Contains("bpa_transactiondate"))
        //                            {
        //                                DateTime end = startWorkMonth.AddMonths(-1);
        //                                DateTime start = lastEligibility.GetAttributeValue<DateTime>("bpa_transactiondate");
        //                                consecutiveinactivemonths = end.Month + end.Year * 12 - (start.Month + start.Year * 12);
        //                            }
        //                            else
        //                            {
        //                                Entity lastIA = RecalculationTransactionHelper.FetchLastEligiblityContributionMonth(service, startWorkMonth, selectedContact.Id);
        //                                if (lastIA != null && lastIA.Attributes.Contains("bpa_transactiondate"))
        //                                {
        //                                    DateTime end = startWorkMonth.AddMonths(-1);
        //                                    DateTime start = lastIA.GetAttributeValue<DateTime>("bpa_transactiondate");
        //                                    consecutiveinactivemonths = end.Month + end.Year * 12 - (start.Month + start.Year * 12);
        //                                }
        //                            }
                                   

        //                            if (previousMonthEligibility != null)
        //                            {
        //                                #region -------- Second time run from same month and delete all the records ------

        //                                if (previousMonthEligibility.Attributes.Contains("bpa_currenteligibilitycategory"))
        //                                {
        //                                    if (isMemberCategoryChanged)
        //                                    {
        //                                        UsingEligibilityCategoty = currentEligibilityCategory;
        //                                    }
        //                                    else
        //                                    {
        //                                        if (isEligibilityCategoryFundAssisted == false)
        //                                            UsingEligibilityCategoty =
        //                                                previousMonthEligibility.GetAttributeValue<EntityReference>(
        //                                                    "bpa_currenteligibilitycategory");
        //                                        else
        //                                            UsingEligibilityCategoty = currentEligibilityCategory;
        //                                    }
        //                                    if (UsingEligibilityCategoty != null)
        //                                        eligibilityOffset =
        //                                            EligibilityCategoryHelper.FetchEligibilityOffSet(service,
        //                                                UsingEligibilityCategoty.Id);

        //                                    Entity EligibilityCategory =
        //                                        EligibilityCategoryHelper.FetchEligibilityCategory(service,
        //                                            UsingEligibilityCategoty.Id);
        //                                    Money FA_deductionrate = new Money(0);
        //                                    Money FA_fundassistanceamount = new Money(0);

        //                                    if (EligibilityCategory != null)
        //                                    {
        //                                        if (EligibilityCategory.Attributes.Contains("bpa_categorytype"))
        //                                            EC_CalculationType =
        //                                                EligibilityCategory.GetAttributeValue<OptionSetValue>(
        //                                                    "bpa_categorytype").Value;

        //                                        if (EligibilityCategory.Attributes.Contains("bpa_terminationcycle"))
        //                                            maxInactiveCounter =
        //                                                EligibilityCategory.GetAttributeValue<int>(
        //                                                    "bpa_terminationcycle");

        //                                        Entity EligibilityCategoryDetail =
        //                                            EligibilityCategoryHelper.FetchEligibilityDetail(service,
        //                                                currentEligibilityCategory.Id, startWorkMonth);

        //                                        //if (EligibilityCategory.Attributes.Contains("bpa_eligibilityoffset"))
        //                                        //    FA_eligibilityoffsetmonths = EligibilityCategoryDetail.GetAttributeValue<Int32>("bpa_eligibilityoffset");
        //                                        if (EligibilityCategoryDetail != null)
        //                                        {
        //                                            if (
        //                                                EligibilityCategoryDetail.Attributes.Contains(
        //                                                    "bpa_deductionrate"))
        //                                                FA_deductionrate =
        //                                                    EligibilityCategoryDetail.GetAttributeValue<Money>(
        //                                                        "bpa_deductionrate");

        //                                            if (
        //                                                EligibilityCategoryDetail.Attributes.Contains(
        //                                                    "bpa_fundassistanceamount"))
        //                                                FA_fundassistanceamount =
        //                                                    EligibilityCategoryDetail.GetAttributeValue<Money>(
        //                                                        "bpa_fundassistanceamount");
        //                                        }
        //                                    }

        //                                    if (EC_CalculationType ==
        //                                        (int)BpaEligibilitycategorybpaCategoryType.Hours)
        //                                        IsEligiblityHours = true;

        //                                    //Get Sum of Dollar Hours
        //                                    decimal sumdollarhour = 0;
        //                                    decimal deductionRate = 0;

        //                                    if (IsEligiblityHours)
        //                                        sumdollarhour = RecalculationTransactionHelper.GetSumHours(service,
        //                                            selectedContact.Id, startWorkMonth, defaultFund.Id);
        //                                    else
        //                                        sumdollarhour =
        //                                            RecalculationTransactionHelper.GetSumDollarHour(service,
        //                                                selectedContact.Id, startWorkMonth,
        //                                                defaultFund.Id);

        //                                    //deductionRate = (EligibilityCategoryHelper.GetEligibilityDeductionRate(UsingEligibilityCategoty.Id, startWorkMonth) * -1);

        //                                    if (IsEligiblityHours)
        //                                        deductionRate = RecalculationTransactionHelper.GetEligibilityDeductionRate_Hours (service, UsingEligibilityCategoty.Id,
        //                                                    startWorkMonth.AddDays(1)) * -1;
        //                                    else
        //                                        deductionRate = EligibilityCategoryHelper.GetEligibilityDeductionRate(service, UsingEligibilityCategoty.Id, startWorkMonth) * -1;
        //                                    //Find out Self-Pay draw Amount from Eligibility Category Detail 
        //                                    decimal selfPayDeductionRate = EligibilityCategoryHelper.GetEligibilitySelfPayDeductionRate(service,
        //                                            UsingEligibilityCategoty.Id, startWorkMonth.AddDays(1)) * -1;

        //                                    //Find out Self-Pay Bank from Member Plan
        //                                    //Money SelfPayBank = Recalculation.ContactHelper.GetSelfPayBank(service, selectedContact.Id);
        //                                    decimal selpaybank = TransactionHelper.FetchSumOfSelfPayBank(service, tracingService, selectedContact.Id, startWorkMonth);
        //                                    Money SelfPayBank = new Money(selpaybank);

        //                                    #region ------ FOLLOWING CODE HAS BEEN REMARKED AS WE ARE PASSING WRONG PARAMETER (CONTACTID INSTEAD OF ACCOUNTID)
        //                                    //Get SubSectory Calculation Type
        //                                    //SubSector_CalculationType = AccountHelper.GetCalculationTypeByAccountId(service, tracingService, selectedContact.Id);

        //                                    //#region -------- FOR MIS MATCH ( UNDER REVIEW ) ----------------------

        //                                    ////if ((SubSector_CalculationType == (int)bpa_SubSector_bpa_CalculationType.Flat ||
        //                                    ////        SubSector_CalculationType == (int)bpa_SubSector_bpa_CalculationType.Corporate)
        //                                    ////                && eligibilityCategoryType != (int)bpa_eligibilitycategorybpa_CategoryType.Flat)
        //                                    //if ((SubSector_CalculationType ==
        //                                    //     (int)BpaSubSectorBpaCalculationType.Flat ||
        //                                    //     SubSector_CalculationType ==
        //                                    //     (int)BpaSubSectorBpaCalculationType.Corporate) &&
        //                                    //    (eligibilityCategoryType !=
        //                                    //     (int)BpaEligibilitycategorybpaCategoryType.Flat) &&
        //                                    //    (eligibilityCategoryType !=
        //                                    //     (int)BpaEligibilitycategorybpaCategoryType.Dollar))
        //                                    //    IsMissMatch = true;
        //                                    //else if (SubSector_CalculationType ==
        //                                    //         (int)BpaSubSectorBpaCalculationType.DollarBased &&
        //                                    //         eligibilityCategoryType !=
        //                                    //         (int)BpaEligibilitycategorybpaCategoryType.Dollar)
        //                                    //    IsMissMatch = true;
        //                                    //else if (((SubSector_CalculationType ==
        //                                    //           (int)BpaSubSectorBpaCalculationType.HourBased) ||
        //                                    //          (SubSector_CalculationType ==
        //                                    //           (int)BpaSubSectorBpaCalculationType.Icinonici)) &&
        //                                    //         (eligibilityCategoryType !=
        //                                    //          (int)BpaEligibilitycategorybpaCategoryType.Hours) &&
        //                                    //         (eligibilityCategoryType !=
        //                                    //          (int)
        //                                    //              BpaEligibilitycategorybpaCategoryType
        //                                    //                  .Dollar))
        //                                    //    IsMissMatch = true;
        //                                    //else if (SubSector_CalculationType ==
        //                                    //         (int)BpaSubSectorBpaCalculationType.Piecework &&
        //                                    //         eligibilityCategoryType !=
        //                                    //         (int)
        //                                    //             BpaEligibilitycategorybpaCategoryType
        //                                    //                 .Dollar)
        //                                    //    IsMissMatch = true;
        //                                    //else
        //                                    //    IsMissMatch = false;

        //                                    //#endregion
        //                                    #endregion
        //                                    IsMissMatch = false;

        //                                    if (currentEligibility == (int)MempberPlanBpaCurrentEligibility.NewMember)
        //                                    {
        //                                        #region ------------------- NEW MEMBER -----------------------

        //                                        if (IsMissMatch)
        //                                        {
        //                                            RecalculationTransactionHelper.CreateUnderReviewTransaction( service, selectedContact.Id, UsingEligibilityCategoty,
        //                                                startWorkMonth, startWorkMonth.AddMonths(eligibilityOffset), (int)BpaTransactionbpaTransactionCategory.Eligibility,
        //                                                (int)BpaTransactionbpaTransactionType.UnderReview, defaultFund.Id);

        //                                            ContactHelper.SetCurrentEligibility(service, tracingService,selectedContact.Id,
        //                                                (int)MempberPlanBpaCurrentEligibility.UnderReview, startWorkMonth.AddMonths(eligibilityOffset));
        //                                        }
        //                                        else
        //                                        {
        //                                            decimal miniInitEligibility = 0;

        //                                            if (IsEligiblityHours)
        //                                                miniInitEligibility = RecalculationTransactionHelper.GetMiniInitEligibility_Hours(service,UsingEligibilityCategoty.Id,
        //                                                            startWorkMonth.AddMonths(eligibilityOffset).AddDays(1));
        //                                            else
        //                                                miniInitEligibility = EligibilityCategoryHelper.GetMiniInitEligibility(service, UsingEligibilityCategoty.Id,
        //                                                        startWorkMonth.AddMonths(eligibilityOffset).AddDays(1));

        //                                            if (sumdollarhour >= miniInitEligibility)
        //                                            {
        //                                                if (((SubSector_CalculationType !=(int)BpaSubSectorBpaCalculationType.Flat) && 
        //                                                     (SubSector_CalculationType != (int)BpaSubSectorBpaCalculationType.Corporate))
        //                                                    ||
        //                                                    ((SubSector_CalculationType == (int)BpaSubSectorBpaCalculationType.Flat) && 
        //                                                      eligibilityCategoryType == (int) BpaEligibilitycategorybpaCategoryType .Dollar))
        //                                                {
        //                                                    if (IsEligiblityHours)
        //                                                        RecalculationTransactionHelper.CreateDrawTransaction_Hour(service,selectedContact.Id, startWorkMonth,
        //                                                                startWorkMonth.AddMonths(eligibilityOffset), (int)BpaTransactionbpaTransactionCategory.Eligibility,
        //                                                                (int)BpaTransactionbpaTransactionType.EligibilityDraw,defaultFund, deductionRate,UsingEligibilityCategoty);
        //                                                    else
        //                                                        RecalculationTransactionHelper.CreateTransaction(service, selectedContact.Id, startWorkMonth,
        //                                                            startWorkMonth.AddMonths(eligibilityOffset),defaultFund,(int)BpaTransactionbpaTransactionCategory.Eligibility,
        //                                                            (int)BpaTransactionbpaTransactionType.EligibilityDraw, deductionRate, false,null, defaultFund.Id);
        //                                                }

        //                                                ContactHelper.SetCurrentEligibility(service, tracingService, selectedContact.Id,
        //                                                    (int)MempberPlanBpaCurrentEligibility.InBenefit, startWorkMonth.AddMonths(eligibilityOffset));

        //                                                RecalculationTransactionHelper.CreateTransaction(service,selectedContact.Id, startWorkMonth,
        //                                                    startWorkMonth.AddMonths(eligibilityOffset),null, (int)BpaTransactionbpaTransactionCategory.Eligibility,
        //                                                    (int)BpaTransactionbpaTransactionType.InBenefit,0, true, UsingEligibilityCategoty, defaultFund.Id);

        //                                                selfPayCounter = 0;
        //                                                inactiveMonthCounter = 0;
        //                                            }
        //                                            else
        //                                            {
        //                                                ContactHelper.SetCurrentEligibility(service, tracingService,selectedContact.Id,
        //                                                    (int)MempberPlanBpaCurrentEligibility.NewMember, startWorkMonth.AddMonths(eligibilityOffset));

        //                                                RecalculationTransactionHelper.CreateTransaction(service,selectedContact.Id, startWorkMonth,
        //                                                    startWorkMonth.AddMonths(eligibilityOffset),null, (int)BpaTransactionbpaTransactionCategory.Eligibility,
        //                                                    (int)BpaTransactionbpaTransactionType.NewMember,0, true, UsingEligibilityCategoty, defaultFund.Id);

        //                                                inactiveMonthCounter = consecutiveinactivemonths + 1;
        //                                            }
        //                                        }

        //                                        #endregion
        //                                    } //end of New Number
        //                                    else if ((currentEligibility == (int)MempberPlanBpaCurrentEligibility.InBenefit) ||
        //                                             (currentEligibility == (int)MempberPlanBpaCurrentEligibility.SelfPayInBenefit))
        //                                    {
        //                                        #region --------------- INBENEFIT & SELFPAY IN BENEIT -----------------------

        //                                        //Added by jwalin
        //                                        if (IsMissMatch)
        //                                        {
        //                                            ContactHelper.SetCurrentEligibility(service, tracingService, selectedContact.Id,
        //                                                (int)MempberPlanBpaCurrentEligibility.UnderReview, startWorkMonth.AddMonths(eligibilityOffset));

        //                                            RecalculationTransactionHelper.CreateUnderReviewTransaction(service, selectedContact.Id, UsingEligibilityCategoty,
        //                                                startWorkMonth,startWorkMonth.AddMonths(eligibilityOffset),(int)BpaTransactionbpaTransactionCategory.Eligibility,
        //                                                (int)BpaTransactionbpaTransactionType.UnderReview,defaultFund.Id);
        //                                        }
        //                                        else
        //                                        {
        //                                            if (sumdollarhour + deductionRate >= 0)
        //                                            {
        //                                                //Added by jwalin
        //                                                if (((SubSector_CalculationType !=(int)BpaSubSectorBpaCalculationType.Flat) && (SubSector_CalculationType !=(int)BpaSubSectorBpaCalculationType.Corporate))
        //                                                    ||
        //                                                    ((SubSector_CalculationType == (int)BpaSubSectorBpaCalculationType.Flat) && eligibilityCategoryType == (int)BpaEligibilitycategorybpaCategoryType.Dollar))
        //                                                {
        //                                                    if (IsEligiblityHours)
        //                                                        RecalculationTransactionHelper.CreateDrawTransaction_Hour(service, selectedContact.Id, startWorkMonth, 
        //                                                            startWorkMonth.AddMonths(eligibilityOffset),(int)BpaTransactionbpaTransactionCategory.Eligibility,
        //                                                            (int)BpaTransactionbpaTransactionType.EligibilityDraw,defaultFund, deductionRate,UsingEligibilityCategoty);
        //                                                    else
        //                                                        RecalculationTransactionHelper.CreateTransaction(service, selectedContact.Id, startWorkMonth,
        //                                                                startWorkMonth.AddMonths(eligibilityOffset), defaultFund,(int)BpaTransactionbpaTransactionCategory.Eligibility,
        //                                                                (int)BpaTransactionbpaTransactionType.EligibilityDraw, deductionRate,false, null, defaultFund.Id);
        //                                                }

        //                                                if (previousMonthEligibility == null)
        //                                                {
        //                                                    decimal miniInitEligibility = 0;

        //                                                    if (IsEligiblityHours)
        //                                                        miniInitEligibility = RecalculationTransactionHelper.GetMiniInitEligibility_Hours(service,UsingEligibilityCategoty.Id,
        //                                                                    startWorkMonth.AddMonths(eligibilityOffset).AddDays(1));
        //                                                    else
        //                                                        miniInitEligibility = EligibilityCategoryHelper.GetMiniInitEligibility(service,UsingEligibilityCategoty.Id,
        //                                                                    startWorkMonth.AddMonths(eligibilityOffset).AddDays(1));

        //                                                    if (sumdollarhour >= miniInitEligibility)
        //                                                    {
        //                                                        ContactHelper.SetCurrentEligibility(service, tracingService,selectedContact.Id,
        //                                                            (int)MempberPlanBpaCurrentEligibility.InBenefit, startWorkMonth.AddMonths(eligibilityOffset));

        //                                                        RecalculationTransactionHelper.CreateTransaction(service, selectedContact.Id,startWorkMonth,
        //                                                                startWorkMonth.AddMonths(eligibilityOffset),null,(int)BpaTransactionbpaTransactionCategory.Eligibility,
        //                                                                (int)BpaTransactionbpaTransactionType.InBenefit,0, true, UsingEligibilityCategoty,defaultFund.Id);

        //                                                        selfPayCounter = 0;
        //                                                        inactiveMonthCounter = 0;
        //                                                    }
        //                                                    else
        //                                                    {
        //                                                        ContactHelper.SetCurrentEligibility(service, tracingService,selectedContact.Id, (int)MempberPlanBpaCurrentEligibility.NewMember, startWorkMonth.AddMonths(eligibilityOffset));

        //                                                        RecalculationTransactionHelper.CreateTransaction(service, selectedContact.Id, startWorkMonth,
        //                                                            startWorkMonth.AddMonths(eligibilityOffset), null, (int)BpaTransactionbpaTransactionCategory.Eligibility,
        //                                                                (int)BpaTransactionbpaTransactionType.NewMember, 0, true, UsingEligibilityCategoty, defaultFund.Id);

        //                                                        inactiveMonthCounter = consecutiveinactivemonths + 1;
        //                                                    }
        //                                                }
        //                                                else
        //                                                {
        //                                                    ContactHelper.SetCurrentEligibility(service, tracingService, selectedContact.Id,
        //                                                        (int)MempberPlanBpaCurrentEligibility.InBenefit, startWorkMonth.AddMonths(eligibilityOffset));

        //                                                    RecalculationTransactionHelper.CreateTransaction(service, selectedContact.Id, startWorkMonth,
        //                                                        startWorkMonth.AddMonths(eligibilityOffset),null,(int)BpaTransactionbpaTransactionCategory.Eligibility,
        //                                                        (int)BpaTransactionbpaTransactionType.InBenefit,0, true, UsingEligibilityCategoty,defaultFund.Id);

        //                                                    selfPayCounter = 0;
        //                                                    inactiveMonthCounter = 0;
        //                                                }
        //                                            }
        //                                            else if ((selfPayDeductionRate != 0) && (SelfPayBank.Value >= (selfPayDeductionRate * -1)))
        //                                            {
        //                                                #region ------------- Self Pay Bank Checking ------------------
                                                        
        //                                                //Find MaxConsecutiveMonthsSelfPay from Eligibility Category 
        //                                                int MaxConsecutiveMonthsSelfPay = EligibilityCategoryHelper.GetMaxConsecutiveMonthsSelfPay(
        //                                                        service, UsingEligibilityCategoty.Id);

        //                                                //Find out MAx Month of Employer Contribution Contribution (if not found return WorkMonth)
        //                                                DateTime ECMaxDate = TransactionHelper.FetchMaxEmployerContributionDate(service, tracingService, selectedContact.Id, startWorkMonth);

        //                                                //Find out How many Previously SELFPAY
        //                                                int cntSP = 0;
        //                                                Entity lastEC = RecalculationTransactionHelper.FetchLastEligiblityMonthExceptSelfPay(service, tracingService,
        //                                                        ECMaxDate, ECMaxDate, selectedContact.Id);
        //                                                if (lastEC != null && lastEC.Attributes.Contains("bpa_transactiondate"))
        //                                                {
        //                                                    DateTime end = startWorkMonth.AddMonths(-1);
        //                                                    DateTime start =
        //                                                        lastEC.GetAttributeValue<DateTime>("bpa_transactiondate");
        //                                                    cntSP = end.Month + end.Year * 12 - (start.Month + start.Year * 12);
        //                                                }
        //                                                if (cntSP == 0)
        //                                                {
        //                                                    cntSP = RecalculationTransactionHelper.GetHowManySelfPay(service, tracingService, ECMaxDate, startWorkMonth, selectedContact.Id);
        //                                                }

        //                                                if (cntSP < MaxConsecutiveMonthsSelfPay)
        //                                                {
        //                                                    // Create SelfPay draw and Self pay In Benefit Transaction
        //                                                    RecalculationTransactionHelper.CreateTransaction(service, selectedContact.Id, startWorkMonth,
        //                                                        startWorkMonth.AddMonths(eligibilityOffset),defaultFund, (int)BpaTransactionbpaTransactionCategory.Eligibility,
        //                                                    (int)BpaTransactionbpaTransactionType.SelfPayDraw, selfPayDeductionRate, false, UsingEligibilityCategoty, defaultFund.Id);

        //                                                    RecalculationTransactionHelper.CreateTransaction(service, selectedContact.Id, startWorkMonth,
        //                                                       startWorkMonth.AddMonths(eligibilityOffset), null, (int)BpaTransactionbpaTransactionCategory.Eligibility,
        //                                                       (int)BpaTransactionbpaTransactionType.SelfPayInBenefit, 0, true, UsingEligibilityCategoty, defaultFund.Id);

        //                                                    // Update Selfpay counter 
        //                                                    selfPayCounter = cntSP + 1;
        //                                                    // Trigger Self Pay Bank Rollup
        //                                                    PluginHelper.TriggerRollup(service, tracingService, "bpa_memberplan", selectedContact.Id, "bpa_selfpaybank");

        //                                                    if (selfPayCounter == MaxConsecutiveMonthsSelfPay)
        //                                                    {
        //                                                        //CREATE "SelfPay Max Consecutive Reach" Transaction
        //                                                        RecalculationTransactionHelper.CreateSelfPayMaxReach(service, startWorkMonth, startWorkMonth.AddMonths(eligibilityOffset),
        //                                                            selectedContact.Id, null);
        //                                                    }
        //                                                    ContactHelper.SetCurrentEligibility(service, tracingService,selectedContact.Id,
        //                                                        (int)MempberPlanBpaCurrentEligibility.SelfPayInBenefit, startWorkMonth.AddMonths(eligibilityOffset));
        //                                                }
        //                                                else
        //                                                {
        //                                                    ContactHelper.SetCurrentEligibility(service, tracingService,selectedContact.Id,
        //                                                        (int)MempberPlanBpaCurrentEligibility.NotInBenefit, startWorkMonth.AddMonths(eligibilityOffset));

        //                                                    RecalculationTransactionHelper.CreateTransaction(service, selectedContact.Id, startWorkMonth,
        //                                                        startWorkMonth.AddMonths(eligibilityOffset), null, (int)BpaTransactionbpaTransactionCategory.Eligibility,
        //                                                        (int)BpaTransactionbpaTransactionType.NotInBenefit, 0, true, UsingEligibilityCategoty, defaultFund.Id);

        //                                                    inactiveMonthCounter = consecutiveinactivemonths + 1;
        //                                                }

        //                                                #endregion
        //                                            }
        //                                            else
        //                                            {
        //                                                ContactHelper.SetCurrentEligibility(service, tracingService, selectedContact.Id,
        //                                                    (int)MempberPlanBpaCurrentEligibility.NotInBenefit, startWorkMonth.AddMonths(eligibilityOffset));

        //                                                RecalculationTransactionHelper.CreateTransaction(service, selectedContact.Id, startWorkMonth,
        //                                                    startWorkMonth.AddMonths(eligibilityOffset),null,(int)BpaTransactionbpaTransactionCategory.Eligibility,
        //                                                    (int)BpaTransactionbpaTransactionType.NotInBenefit,0, true, UsingEligibilityCategoty, defaultFund.Id);

        //                                                inactiveMonthCounter = consecutiveinactivemonths + 1;
        //                                            }
        //                                        }

        //                                        #endregion
        //                                    }
        //                                    else if ((currentEligibility == (int)MempberPlanBpaCurrentEligibility.NotInBenefit) ||
        //                                             (currentEligibility == (int)MempberPlanBpaCurrentEligibility.Reinstatement))
        //                                    {
        //                                        #region  -------------- NOT IN BENEFIT ----------------------------

        //                                        bool IsResinstatement = EligibilityCategoryHelper.IsCategoryReinstatement(service, UsingEligibilityCategoty.Id);
        //                                        decimal reinstatementAmount = 0;

        //                                        if (IsResinstatement)
        //                                        {
        //                                            if (IsEligiblityHours)
        //                                                reinstatementAmount =EligibilityCategoryHelper.GetReinstatementHours(service, UsingEligibilityCategoty.Id, startWorkMonth);
        //                                            else
        //                                                reinstatementAmount =EligibilityCategoryHelper.GetReinstatementDollars(service,UsingEligibilityCategoty.Id,startWorkMonth);

        //                                            if (reinstatementAmount == 0)
        //                                                IsMissMatch = true;
        //                                        }

        //                                        //Added by jwalin
        //                                        if (IsMissMatch)
        //                                        {
        //                                            ContactHelper.SetCurrentEligibility(service, tracingService,selectedContact.Id,
        //                                                (int)MempberPlanBpaCurrentEligibility.UnderReview, startWorkMonth.AddMonths(eligibilityOffset));

        //                                            RecalculationTransactionHelper.CreateUnderReviewTransaction(service,selectedContact.Id, UsingEligibilityCategoty,startWorkMonth,
        //                                                    startWorkMonth.AddMonths(eligibilityOffset),(int)BpaTransactionbpaTransactionCategory.Eligibility,
        //                                                    (int)BpaTransactionbpaTransactionType.UnderReview,defaultFund.Id);
        //                                        }
        //                                        else
        //                                        {
        //                                            if (IsResinstatement && (currentEligibility == (int)MempberPlanBpaCurrentEligibility.Reinstatement))
        //                                            {
        //                                                //Find out How many Previously SELFPAY
        //                                                int cntIA = 0;
        //                                                Entity lastIA = RecalculationTransactionHelper.FetchLastEligiblityContributionMonth(service, startWorkMonth, selectedContact.Id);
        //                                                if (lastIA != null && lastIA.Attributes.Contains("bpa_transactiondate"))
        //                                                {
        //                                                    DateTime end = startWorkMonth.AddMonths(-1);
        //                                                    DateTime start = lastIA.GetAttributeValue<DateTime>("bpa_transactiondate");
        //                                                    cntIA = end.Month + end.Year * 12 - (start.Month + start.Year * 12);
        //                                                }
        //                                                if (sumdollarhour >= reinstatementAmount)
        //                                                {
        //                                                    if (IsEligiblityHours)
        //                                                        RecalculationTransactionHelper.CreateDrawTransaction_Hour(service,selectedContact.Id, startWorkMonth,
        //                                                                startWorkMonth.AddMonths(eligibilityOffset),(int)BpaTransactionbpaTransactionCategory.Eligibility,
        //                                                                (int)BpaTransactionbpaTransactionType.EligibilityDraw,defaultFund, deductionRate, UsingEligibilityCategoty);
        //                                                    else
        //                                                        RecalculationTransactionHelper.CreateTransaction(service,selectedContact.Id, startWorkMonth,
        //                                                                startWorkMonth.AddMonths(eligibilityOffset),defaultFund,(int)BpaTransactionbpaTransactionCategory.Eligibility,
        //                                                                (int)BpaTransactionbpaTransactionType.EligibilityDraw,deductionRate, false, null,defaultFund.Id);

        //                                                    ContactHelper.SetCurrentEligibility(service, tracingService,
        //                                                        selectedContact.Id, (int)MempberPlanBpaCurrentEligibility.InBenefit, startWorkMonth.AddMonths(eligibilityOffset));

        //                                                    RecalculationTransactionHelper.CreateTransaction(service, selectedContact.Id,startWorkMonth,
        //                                                            startWorkMonth.AddMonths(eligibilityOffset),null,(int)BpaTransactionbpaTransactionCategory.Eligibility,
        //                                                            (int)BpaTransactionbpaTransactionType.InBenefit,0, true, UsingEligibilityCategoty,defaultFund.Id);

        //                                                    selfPayCounter = 0;
        //                                                }
        //                                                else
        //                                                {
        //                                                    RecalculationTransactionHelper.CreateTransaction(service, selectedContact.Id,startWorkMonth,
        //                                                            startWorkMonth.AddMonths(eligibilityOffset),null,(int)BpaTransactionbpaTransactionCategory.Eligibility,
        //                                                            (int)BpaTransactionbpaTransactionType.ReInstateing,0, true, UsingEligibilityCategoty,defaultFund.Id);

        //                                                    ContactHelper.SetCurrentEligibility(service, tracingService,selectedContact.Id,
        //                                                        (int)MempberPlanBpaCurrentEligibility.Reinstatement, startWorkMonth.AddMonths(eligibilityOffset));

        //                                                    inactiveMonthCounter = cntIA + 1;
        //                                                }
        //                                            }
        //                                            else
        //                                            {
        //                                                //Find out How many Previously SELFPAY
        //                                                int cntIA = 0;
        //                                                Entity lastIA = RecalculationTransactionHelper.FetchLastEligiblityMonthExceptNotInBenefit(service, startWorkMonth, 
        //                                                    selectedContact.Id);
        //                                                if (lastIA != null &&
        //                                                    lastIA.Attributes.Contains("bpa_transactiondate"))
        //                                                {
        //                                                    DateTime end = startWorkMonth.AddMonths(-1);
        //                                                    DateTime start =lastIA.GetAttributeValue<DateTime>("bpa_transactiondate");
        //                                                    cntIA = end.Month + end.Year * 12 - (start.Month + start.Year * 12);
        //                                                }
        //                                                if (sumdollarhour + deductionRate >= 0)
        //                                                {
        //                                                    if (((SubSector_CalculationType != (int)BpaSubSectorBpaCalculationType.Flat) &&
        //                                                         (SubSector_CalculationType != (int)BpaSubSectorBpaCalculationType.Corporate)) //Added by jwalin
        //                                                        ||
        //                                                        ((SubSector_CalculationType ==(int)BpaSubSectorBpaCalculationType.Flat) && eligibilityCategoryType ==(int)BpaEligibilitycategorybpaCategoryType.Dollar))
        //                                                    {
        //                                                        if (IsEligiblityHours)
        //                                                            RecalculationTransactionHelper.CreateDrawTransaction_Hour(service, selectedContact.Id,startWorkMonth,
        //                                                                    startWorkMonth.AddMonths(eligibilityOffset),(int)BpaTransactionbpaTransactionCategory.Eligibility,
        //                                                                    (int)BpaTransactionbpaTransactionType.EligibilityDraw,defaultFund, deductionRate,UsingEligibilityCategoty);
        //                                                        else
        //                                                            RecalculationTransactionHelper.CreateTransaction(service,selectedContact.Id,startWorkMonth,
        //                                                                    startWorkMonth.AddMonths(eligibilityOffset),defaultFund,(int)BpaTransactionbpaTransactionCategory.Eligibility,
        //                                                                    (int)BpaTransactionbpaTransactionType.EligibilityDraw,deductionRate, false, null,defaultFund.Id);
        //                                                    }

        //                                                    if (previousMonthEligibility == null)
        //                                                    {
        //                                                        ContactHelper.SetCurrentEligibility(service, tracingService, selectedContact.Id,
        //                                                            (int)MempberPlanBpaCurrentEligibility.NewMember, startWorkMonth.AddMonths(eligibilityOffset));

        //                                                        RecalculationTransactionHelper.CreateTransaction(service,selectedContact.Id, startWorkMonth,
        //                                                            startWorkMonth.AddMonths(eligibilityOffset),null,(int)BpaTransactionbpaTransactionCategory.Eligibility,
        //                                                                (int)BpaTransactionbpaTransactionType.NewMember,0, true, UsingEligibilityCategoty,defaultFund.Id);

        //                                                        inactiveMonthCounter = cntIA + 1;
        //                                                    }
        //                                                    else
        //                                                    {
        //                                                        ContactHelper.SetCurrentEligibility(service, tracingService, selectedContact.Id,
        //                                                            (int)MempberPlanBpaCurrentEligibility.InBenefit, startWorkMonth.AddMonths(eligibilityOffset));

        //                                                        RecalculationTransactionHelper.CreateTransaction(service,selectedContact.Id, startWorkMonth,
        //                                                            startWorkMonth.AddMonths(eligibilityOffset),null,(int)BpaTransactionbpaTransactionCategory.Eligibility,
        //                                                            (int)BpaTransactionbpaTransactionType.InBenefit,0, true, UsingEligibilityCategoty,defaultFund.Id);
                                                                
        //                                                        selfPayCounter = 0;
        //                                                    }
        //                                                }
                                                        
        //                                                else
        //                                                {
        //                                                    ContactHelper.SetCurrentEligibility(service, tracingService,selectedContact.Id,
        //                                                        (int)MempberPlanBpaCurrentEligibility.NotInBenefit, startWorkMonth.AddMonths(eligibilityOffset));

        //                                                    RecalculationTransactionHelper.CreateTransaction(service, selectedContact.Id,startWorkMonth,
        //                                                            startWorkMonth.AddMonths(eligibilityOffset),null,(int)BpaTransactionbpaTransactionCategory.Eligibility,
        //                                                            (int)BpaTransactionbpaTransactionType.NotInBenefit,0, true, UsingEligibilityCategoty,defaultFund.Id);

        //                                                    inactiveMonthCounter = cntIA + 1;
        //                                                }
        //                                            }
        //                                        }

        //                                        #endregion
        //                                    }
        //                                    else if (currentEligibility ==(int)MempberPlanBpaCurrentEligibility.UnderReview)
        //                                    {
        //                                        #region -------------------- UNDER REVIEW --------------------

        //                                        if (previousMonthEligibility == null)
        //                                        {
        //                                            decimal miniInitEligibility = 0;

        //                                            if (IsEligiblityHours)
        //                                                miniInitEligibility = RecalculationTransactionHelper.GetMiniInitEligibility_Hours(service, UsingEligibilityCategoty.Id,
        //                                                            startWorkMonth.AddMonths(eligibilityOffset).AddDays(1));
        //                                            else
        //                                                miniInitEligibility = EligibilityCategoryHelper.GetMiniInitEligibility(service,UsingEligibilityCategoty.Id,
        //                                                            startWorkMonth.AddMonths(eligibilityOffset).AddDays(1));

        //                                            if (sumdollarhour >= miniInitEligibility)
        //                                            {
        //                                                if (SubSector_CalculationType != (int)BpaSubSectorBpaCalculationType.Flat)
        //                                                {
        //                                                    if (IsEligiblityHours)
        //                                                        RecalculationTransactionHelper.CreateDrawTransaction_Hour(service, selectedContact.Id,startWorkMonth,
        //                                                                startWorkMonth.AddMonths(eligibilityOffset),(int)BpaTransactionbpaTransactionCategory.Eligibility,
        //                                                                (int)BpaTransactionbpaTransactionType.EligibilityDraw,defaultFund, deductionRate,UsingEligibilityCategoty);
        //                                                    else
        //                                                        RecalculationTransactionHelper.CreateTransaction(service,selectedContact.Id, startWorkMonth,
        //                                                                startWorkMonth.AddMonths(eligibilityOffset),defaultFund,(int)BpaTransactionbpaTransactionCategory.Eligibility,
        //                                                                (int)BpaTransactionbpaTransactionType.EligibilityDraw,deductionRate, false, null,defaultFund.Id);
        //                                                }

        //                                                ContactHelper.SetCurrentEligibility(service, tracingService,selectedContact.Id,
        //                                                    (int)MempberPlanBpaCurrentEligibility.InBenefit, startWorkMonth.AddMonths(eligibilityOffset));

        //                                                RecalculationTransactionHelper.CreateTransaction(service, selectedContact.Id,startWorkMonth,
        //                                                        startWorkMonth.AddMonths(eligibilityOffset),null,(int)BpaTransactionbpaTransactionCategory.Eligibility,
        //                                                        (int)BpaTransactionbpaTransactionType.InBenefit,0, true, UsingEligibilityCategoty,defaultFund.Id);
                                                        
        //                                                selfPayCounter = 0;
        //                                            }
        //                                            else
        //                                            {
        //                                                ContactHelper.SetCurrentEligibility(service, tracingService,selectedContact.Id,
        //                                                    (int)MempberPlanBpaCurrentEligibility.NewMember, startWorkMonth.AddMonths(eligibilityOffset));

        //                                                RecalculationTransactionHelper.CreateTransaction(service, selectedContact.Id,startWorkMonth,
        //                                                        startWorkMonth.AddMonths(eligibilityOffset),null,(int)BpaTransactionbpaTransactionCategory.Eligibility,
        //                                                        (int)BpaTransactionbpaTransactionType.NewMember,0, true, UsingEligibilityCategoty,defaultFund.Id);
        //                                            }
        //                                        }
        //                                        else
        //                                        {
        //                                            ContactHelper.SetCurrentEligibility(service, tracingService,selectedContact.Id,
        //                                                (int)MempberPlanBpaCurrentEligibility.UnderReview, startWorkMonth.AddMonths(eligibilityOffset));

        //                                            RecalculationTransactionHelper.CreateUnderReviewTransaction(service,selectedContact.Id, UsingEligibilityCategoty, startWorkMonth,
        //                                                    startWorkMonth.AddMonths(eligibilityOffset),(int)BpaTransactionbpaTransactionCategory.Eligibility,
        //                                                    (int)BpaTransactionbpaTransactionType.UnderReview, defaultFund.Id);
        //                                        }

        //                                        #endregion
        //                                    }
        //                                    else if (currentEligibility == (int)MempberPlanBpaCurrentEligibility.FundAssisted)
        //                                    {
        //                                        #region --- Fund Assisted ----

        //                                        RecalculationTransactionHelper.CreateFundAssistedTransactions(service, selectedContact.Id, startWorkMonth,
        //                                                startWorkMonth.AddMonths(eligibilityOffset), FA_fundassistanceamount, null,
        //                                                (int)BpaTransactionbpaTransactionCategory.Eligibility,(int)BpaTransactionbpaTransactionType.FundAssited, defaultFund,
        //                                                currentEligibilityCategory, defaultFund.Id);

        //                                        RecalculationTransactionHelper.CreateFundAssistedTransactions(service, selectedContact.Id, startWorkMonth,
        //                                                startWorkMonth.AddMonths(eligibilityOffset),new Money(FA_deductionrate.Value * -1), null,
        //                                                (int)BpaTransactionbpaTransactionCategory.Eligibility, (int)BpaTransactionbpaTransactionType.EligibilityDraw, defaultFund, null,
        //                                                defaultFund.Id);

        //                                        RecalculationTransactionHelper.CreateFundAssistedTransactions(service, selectedContact.Id, startWorkMonth,
        //                                            startWorkMonth.AddMonths(eligibilityOffset),null, null, (int)BpaTransactionbpaTransactionCategory.Eligibility,
        //                                                (int)BpaTransactionbpaTransactionType.FundAssistedInBenefit, null, currentEligibilityCategory, defaultFund.Id);

        //                                        //SET Current Eligibility
        //                                        ContactHelper.SetCurrentEligibility(service, tracingService,selectedContact.Id,
        //                                            (int)MempberPlanBpaCurrentEligibility.FundAssisted, startWorkMonth.AddMonths(eligibilityOffset));

        //                                        #endregion
        //                                    }
        //                                }
        //                                HasECED = true;

        //                                #endregion
        //                            }
        //                            else
        //                            {
        //                                #region ---- Else Part ---

        //                                HasECED = true;

        //                                EntityCollection _isAlready =
        //                                    RecalculationTransactionHelper.IsMemberAlreadyInEligibility(service,
        //                                        selectedContact.Id, startWorkMonth);

        //                                if (_isAlready != null && _isAlready.Entities.Count > 0)
        //                                {
        //                                }
        //                                else
        //                                {
        //                                    //Get Sum of Dollar Hours
        //                                    decimal sumdollarhour = 0;
        //                                    decimal deductionRate = 0;

        //                                    if (UsingEligibilityCategoty == null)
        //                                        UsingEligibilityCategoty = ContactHelper.FetchCurrentEligibilityCategory(service, tracingService, selectedContact.Id);

        //                                    if(eligibilityOffset == 0)
        //                                        eligibilityOffset = EligibilityCategoryHelper.FetchEligibilityOffSet(service, UsingEligibilityCategoty.Id);

        //                                    if (IsEligiblityHours)
        //                                        sumdollarhour = RecalculationTransactionHelper.GetSumHours(service,
        //                                            selectedContact.Id, startWorkMonth, defaultFund.Id);
        //                                    else
        //                                        sumdollarhour = RecalculationTransactionHelper.GetSumDollarHour(service,
        //                                                selectedContact.Id, startWorkMonth,
        //                                                defaultFund.Id);

        //                                    if (IsEligiblityHours)
        //                                        deductionRate = RecalculationTransactionHelper.GetEligibilityDeductionRate_Hours (service, UsingEligibilityCategoty.Id,
        //                                                    startWorkMonth.AddDays(1)) * -1;
        //                                    else
        //                                        deductionRate =
        //                                            EligibilityCategoryHelper.GetEligibilityDeductionRate(service, UsingEligibilityCategoty.Id, startWorkMonth) * -1;

        //                                    DateTime benefitmonth = startWorkMonth.AddMonths(eligibilityOffset);
        //                                    if (currentEligibility == (int)MempberPlanBpaCurrentEligibility.NewMember)
        //                                    {
        //                                        #region ------------------- NEW MEMBER -----------------------

        //                                        if (IsMissMatch)
        //                                        {
        //                                            RecalculationTransactionHelper.CreateUnderReviewTransaction(
        //                                                service, selectedContact.Id, UsingEligibilityCategoty,
        //                                                startWorkMonth,
        //                                                startWorkMonth.AddMonths(eligibilityOffset),
        //                                                (int)BpaTransactionbpaTransactionCategory.Eligibility,
        //                                                (int)BpaTransactionbpaTransactionType.UnderReview,
        //                                                defaultFund.Id);

        //                                            ContactHelper.SetCurrentEligibility(service, tracingService,selectedContact.Id,
        //                                                (int)MempberPlanBpaCurrentEligibility.UnderReview, startWorkMonth.AddMonths(eligibilityOffset));
        //                                            MemberWentToUnderReview = true;
        //                                        }
        //                                        else
        //                                        {
        //                                            decimal miniInitEligibility = 0;

        //                                            if (IsEligiblityHours)
        //                                                miniInitEligibility =
        //                                                    RecalculationTransactionHelper
        //                                                        .GetMiniInitEligibility_Hours(service,
        //                                                            UsingEligibilityCategoty.Id,
        //                                                            startWorkMonth.AddMonths(eligibilityOffset)
        //                                                                .AddDays(1));
        //                                            else
        //                                                miniInitEligibility =
        //                                                    EligibilityCategoryHelper.GetMiniInitEligibility(
        //                                                        service, UsingEligibilityCategoty.Id,
        //                                                        startWorkMonth.AddMonths(eligibilityOffset)
        //                                                            .AddDays(1));

        //                                            if ((sumdollarhour >= miniInitEligibility) &&
        //                                                (sumdollarhour < deductionRate * -1))
        //                                            {
        //                                                ContactHelper.SetCurrentEligibility(service, tracingService,
        //                                                    selectedContact.Id,
        //                                                    (int)MempberPlanBpaCurrentEligibility.NotInBenefit, startWorkMonth.AddMonths(eligibilityOffset));

        //                                                RecalculationTransactionHelper.CreateTransaction(service,
        //                                                    selectedContact.Id, startWorkMonth,
        //                                                    startWorkMonth.AddMonths(eligibilityOffset),
        //                                                    null, (int)BpaTransactionbpaTransactionCategory.Eligibility,
        //                                                    (int)BpaTransactionbpaTransactionType.NotInBenefit,
        //                                                    0, true, UsingEligibilityCategoty, defaultFund.Id);
        //                                            }
        //                                            else if (sumdollarhour >= miniInitEligibility)
        //                                            {
        //                                                if (((SubSector_CalculationType !=
        //                                                      (int)BpaSubSectorBpaCalculationType.Flat) &&
        //                                                     (SubSector_CalculationType !=
        //                                                      (int)BpaSubSectorBpaCalculationType.Corporate))
        //                                                    ||
        //                                                    ((SubSector_CalculationType ==
        //                                                      (int)BpaSubSectorBpaCalculationType.Flat) &&
        //                                                     eligibilityCategoryType ==
        //                                                     (int)
        //                                                         BpaEligibilitycategorybpaCategoryType
        //                                                             .Dollar))
        //                                                {
        //                                                    if (IsEligiblityHours)
        //                                                        RecalculationTransactionHelper
        //                                                            .CreateDrawTransaction_Hour(service,
        //                                                                selectedContact.Id, startWorkMonth,
        //                                                                startWorkMonth.AddMonths(eligibilityOffset),
        //                                                                (int)
        //                                                                    BpaTransactionbpaTransactionCategory
        //                                                                        .Eligibility,
        //                                                                (int)
        //                                                                    BpaTransactionbpaTransactionType
        //                                                                        .EligibilityDraw,
        //                                                                defaultFund, deductionRate,
        //                                                                UsingEligibilityCategoty);
        //                                                    else
        //                                                        RecalculationTransactionHelper.CreateTransaction(
        //                                                            service, selectedContact.Id, startWorkMonth,
        //                                                            startWorkMonth.AddMonths(eligibilityOffset),
        //                                                            defaultFund,
        //                                                            (int)
        //                                                                BpaTransactionbpaTransactionCategory
        //                                                                    .Eligibility,
        //                                                            (int)
        //                                                                BpaTransactionbpaTransactionType
        //                                                                    .EligibilityDraw, deductionRate, false,
        //                                                            null, defaultFund.Id);
        //                                                }

        //                                                ContactHelper.SetCurrentEligibility(service, tracingService,
        //                                                    selectedContact.Id,
        //                                                    (int)MempberPlanBpaCurrentEligibility.InBenefit, startWorkMonth.AddMonths(eligibilityOffset));

        //                                                RecalculationTransactionHelper.CreateTransaction(service,
        //                                                    selectedContact.Id, startWorkMonth,
        //                                                    startWorkMonth.AddMonths(eligibilityOffset),
        //                                                    null, (int)BpaTransactionbpaTransactionCategory.Eligibility,
        //                                                    (int)BpaTransactionbpaTransactionType.InBenefit,
        //                                                    0, true, UsingEligibilityCategoty, defaultFund.Id);

        //                                                selfPayCounter = 0;
        //                                            }
        //                                            else
        //                                            {
        //                                                ContactHelper.SetCurrentEligibility(service, tracingService,
        //                                                    selectedContact.Id,
        //                                                    (int)MempberPlanBpaCurrentEligibility.NewMember, startWorkMonth.AddMonths(eligibilityOffset));

        //                                                RecalculationTransactionHelper.CreateTransaction(service,
        //                                                    selectedContact.Id, startWorkMonth,
        //                                                    startWorkMonth.AddMonths(eligibilityOffset),
        //                                                    null, (int)BpaTransactionbpaTransactionCategory.Eligibility,
        //                                                    (int)BpaTransactionbpaTransactionType.NewMember,
        //                                                    0, true, UsingEligibilityCategoty, defaultFund.Id);
        //                                            }
        //                                        }

        //                                        #endregion
        //                                    } //end of New Number
        //                                }

        //                                #endregion
        //                            }

        //                            #region ------ Update Inactive --------

        //                            if (inactiveMonthCounter >= 0)
        //                            {
        //                                bool IsOverMax = false;
        //                                if (inactiveMonthCounter > maxInactiveCounter)
        //                                {
        //                                    inactiveMonthCounter = maxInactiveCounter;
        //                                    IsOverMax = true;
        //                                }
        //                                if (inactiveMonthCounter <= maxInactiveCounter)
        //                                {
        //                                    //if (inactiveMonthCounter > maxInactiveCounter)
        //                                    //    inactiveMonthCounter = maxInactiveCounter;

        //                                    ContactHelper.UpdateInactiveMonthsCounter(service, tracingService, selectedContact.Id, inactiveMonthCounter);
        //                                    if (inactiveMonthCounter > 0)
        //                                    {
        //                                        if (inactiveMonthCounter == maxInactiveCounter)
        //                                        {
        //                                            EntityCollection _InactiveNReserveTransactions =
        //                                                RecalculationTransactionHelper
        //                                                    .AlreadyHasInactiveAndReserveTransactions(service,
        //                                                        selectedContact.Id, startWorkMonth.AddMonths(1));

        //                                            if (_InactiveNReserveTransactions != null &&
        //                                                _InactiveNReserveTransactions.Entities.Count > 0)
        //                                            {
        //                                                foreach (Entity _t in _InactiveNReserveTransactions.Entities
        //                                                    )
        //                                                {
        //                                                    TransactionHelper.DeleteTransactions(service, _t.Id);
        //                                                }
        //                                            }
        //                                            ////CREATE 'INACTIVE' ELIGIBILITY TRANSACTION
        //                                            ////CREATE 'RESERVE TRANSFER ' TRANSACTION
        //                                            //RecalculationTransactionHelper.CreateTransaction(service, selectedContact.Id, startWorkMonth.AddMonths(1), startWorkMonth.AddMonths(eligibilityOffset + 1), string.Empty,
        //                                            //    (int)Transaction_bpa_TransactionCategory.Eligibility, (int)Transaction_bpa_TransactionType.Inactive,
        //                                            //    0, true, UsingEligibilityCategoty, true);

        //                                            if (IsOverMax)
        //                                            {
        //                                                RecalculationTransactionHelper.CreateTransaction(service,
        //                                                    selectedContact.Id, startWorkMonth,
        //                                                    startWorkMonth.AddMonths(eligibilityOffset),
        //                                                    string.Empty,
        //                                                    (int)BpaTransactionbpaTransactionCategory.Eligibility,
        //                                                    (int)BpaTransactionbpaTransactionType.Inactive,
        //                                                    0, true, UsingEligibilityCategoty, true, defaultFund.Id);

        //                                                //Delete Transaction for that month
        //                                                RecalculationTransactionHelper.DeleteTransactionNotInBenefit
        //                                                    (service, selectedContact.Id, startWorkMonth);
        //                                            }
        //                                            else
        //                                            {
        //                                                RecalculationTransactionHelper.CreateTransaction(service,
        //                                                    selectedContact.Id, startWorkMonth.AddMonths(1),
        //                                                    startWorkMonth.AddMonths(eligibilityOffset + 1),
        //                                                    string.Empty,
        //                                                    (int)BpaTransactionbpaTransactionCategory.Eligibility,
        //                                                    (int)BpaTransactionbpaTransactionType.Inactive,
        //                                                    0, true, UsingEligibilityCategoty, true, defaultFund.Id);
        //                                            }

        //                                            //UPDATE CONTACT 'CURRENT ELIGIBILITY' TO 'INACTIVE'
        //                                            ContactHelper.SetCurrentEligibility(service, tracingService,selectedContact.Id,
        //                                                (int)MempberPlanBpaCurrentEligibility.Inactive, startWorkMonth.AddMonths(eligibilityOffset));

        //                                            isMemberBecomeInActive = true;
        //                                        }
        //                                    }
        //                                }
        //                            }

        //                            #endregion
        //                        }
        //                    }

        //                    if (transactionType == (int)BpaTransactionbpaTransactionType.EmployerContribution)
        //                        ContactHelper.UpdateInactiveMonthsCounter(service, tracingService, selectedContact.Id, 0);
        //                } //end of foreach
        //            }

        //            #endregion
        //        } //end of if (M_transactions != null && M_transactions.Entities.Count > 0)

        //        #endregion

        //        #region -------- ELSE  THERE IS NO RECORDS -----

        //        //if(HasECED == false) //BUG # 939
        //        else
        //        {
        //            int consecutiveinactivemonths = 0;

        //            // ContactHelper.GetConsecutiveInactiveMonth(service, selectedContact.Id);

        //            //int cntIB = 0;
        //            Entity lastEligibility = RecalculationTransactionHelper.FetchLastBenefitMonth(service, startWorkMonth, memberId);
        //            if (lastEligibility != null && lastEligibility.Attributes.Contains("bpa_transactiondate"))
        //            {
        //                DateTime end = startWorkMonth.AddMonths(-1);
        //                DateTime start = lastEligibility.GetAttributeValue<DateTime>("bpa_transactiondate");
        //                consecutiveinactivemonths = end.Month + end.Year * 12 - (start.Month + start.Year * 12);
        //            }
        //            else
        //            {
        //                Entity lastIA = RecalculationTransactionHelper.FetchLastEligiblityContributionMonth(service, startWorkMonth, selectedContact.Id);
        //                if (lastIA != null && lastIA.Attributes.Contains("bpa_transactiondate"))
        //                {
        //                    DateTime end = startWorkMonth.AddMonths(-1);
        //                    DateTime start = lastIA.GetAttributeValue<DateTime>("bpa_transactiondate");
        //                    consecutiveinactivemonths = end.Month + end.Year * 12 - (start.Month + start.Year * 12);
        //                }
        //            }

        //            if (previousMonthEligibility != null)
        //            {
        //                if (previousMonthEligibility.Attributes.Contains("bpa_currenteligibilitycategory"))
        //                {
        //                    if (isEligibilityCategoryFundAssisted == false)
        //                        UsingEligibilityCategoty = previousMonthEligibility.GetAttributeValue<EntityReference>("bpa_currenteligibilitycategory");
        //                    else
        //                        UsingEligibilityCategoty = currentEligibilityCategory;
        //                    int EC_CalculationType = 0;
        //                    Entity EligibilityCategory = EligibilityCategoryHelper.FetchEligibilityCategory(service, UsingEligibilityCategoty.Id);
        //                    Money FA_deductionrate = new Money(0);
        //                    Money FA_fundassistanceamount = new Money(0);

        //                    if (EligibilityCategory != null)
        //                    {
        //                        if (EligibilityCategory.Attributes.Contains("bpa_categorytype"))
        //                            EC_CalculationType = EligibilityCategory.GetAttributeValue<OptionSetValue>("bpa_categorytype").Value;

        //                        if (EligibilityCategory.Attributes.Contains("bpa_terminationcycle"))
        //                            maxInactiveCounter = EligibilityCategory.GetAttributeValue<int>("bpa_terminationcycle");

        //                        Entity EligibilityCategoryDetail = EligibilityCategoryHelper.FetchEligibilityDetail(service, currentEligibilityCategory.Id, startWorkMonth);
                                
        //                        if (EligibilityCategoryDetail != null)
        //                        {
        //                            if (EligibilityCategoryDetail.Attributes.Contains("bpa_deductionrate"))
        //                                FA_deductionrate = EligibilityCategoryDetail.GetAttributeValue<Money>("bpa_deductionrate");

        //                            if (EligibilityCategoryDetail.Attributes.Contains("bpa_fundassistanceamount"))
        //                                FA_fundassistanceamount = EligibilityCategoryDetail.GetAttributeValue<Money>("bpa_fundassistanceamount");
        //                        }
        //                    }

        //                    bool IsEligiblityHours = false;
        //                    if (EC_CalculationType == (int)BpaEligibilitycategorybpaCategoryType.Hours)
        //                        IsEligiblityHours = true;

        //                    int eligibilityOffset = EligibilityCategoryHelper.FetchEligibilityOffSet(service, UsingEligibilityCategoty.Id);

        //                    //Get Sum of Dollar Hours
        //                    decimal sumdollarhour = 0;
        //                    decimal deductionRate = 0;
        //                    bool IsMissMatch = false;

        //                    if (IsEligiblityHours)
        //                        sumdollarhour = RecalculationTransactionHelper.GetSumHours(service, selectedContact.Id,
        //                            startWorkMonth, defaultFund.Id);
        //                    else
        //                        sumdollarhour = RecalculationTransactionHelper.GetSumDollarHour(service, selectedContact.Id, startWorkMonth, defaultFund.Id);

        //                    if (IsEligiblityHours)
        //                        deductionRate = RecalculationTransactionHelper.GetEligibilityDeductionRate_Hours(service, UsingEligibilityCategoty.Id, startWorkMonth.AddDays(1)) * -1;
        //                    else
        //                        deductionRate = EligibilityCategoryHelper.GetEligibilityDeductionRate(service, UsingEligibilityCategoty.Id, startWorkMonth) * -1;
        //                    //Find out Self-Pay draw Amount from Eligibility Category Detail 
        //                    decimal selfPayDeductionRate = EligibilityCategoryHelper.GetEligibilitySelfPayDeductionRate(service, UsingEligibilityCategoty.Id, startWorkMonth.AddDays(1)) * -1;

        //                    //Find out Self-Pay Bank from Member Plan
        //                    //Money SelfPayBank = Recalculation.ContactHelper.GetSelfPayBank(service, selectedContact.Id);
        //                    decimal selpaybank = TransactionHelper.FetchSumOfSelfPayBank(service, tracingService, selectedContact.Id, startWorkMonth);
        //                    Money SelfPayBank = new Money(selpaybank);

        //                    #region ------ FOLLOWING CODE HAS BEEN REMARKED AS WE ARE PASSING WRONG PARAMETER (CONTACTID INSTEAD OF ACCOUNTID)
        //                    //Get SubSectory Calculation Type
        //                    //int SubSector_CalculationType = AccountHelper.GetCalculationTypeByAccountId(service, tracingService, selectedContact.Id);

        //                    //#region -------- FOR MIS MATCH ( UNDER REVIEW ) ----------------------

        //                    ////if ((SubSector_CalculationType == (int)bpa_SubSector_bpa_CalculationType.Flat ||
        //                    ////        SubSector_CalculationType == (int)bpa_SubSector_bpa_CalculationType.Corporate)
        //                    ////                && eligibilityCategoryType != (int)bpa_eligibilitycategorybpa_CategoryType.Flat)
        //                    //if ((SubSector_CalculationType == (int)BpaSubSectorBpaCalculationType.Flat ||
        //                    //     SubSector_CalculationType == (int)BpaSubSectorBpaCalculationType.Corporate) &&
        //                    //    (eligibilityCategoryType != (int)BpaEligibilitycategorybpaCategoryType.Flat) &&
        //                    //    (eligibilityCategoryType !=
        //                    //     (int)BpaEligibilitycategorybpaCategoryType.Dollar))
        //                    //    IsMissMatch = true;
        //                    //else if (SubSector_CalculationType == (int)BpaSubSectorBpaCalculationType.DollarBased &&
        //                    //         eligibilityCategoryType !=
        //                    //         (int)BpaEligibilitycategorybpaCategoryType.Dollar)
        //                    //    IsMissMatch = true;
        //                    //else if (((SubSector_CalculationType == (int)BpaSubSectorBpaCalculationType.HourBased) ||
        //                    //          (SubSector_CalculationType == (int)BpaSubSectorBpaCalculationType.Icinonici)) &&
        //                    //         (eligibilityCategoryType != (int)BpaEligibilitycategorybpaCategoryType.Hours) &&
        //                    //         (eligibilityCategoryType !=
        //                    //          (int)BpaEligibilitycategorybpaCategoryType.Dollar))
        //                    //    IsMissMatch = true;
        //                    //else if (SubSector_CalculationType == (int)BpaSubSectorBpaCalculationType.Piecework &&
        //                    //         eligibilityCategoryType !=
        //                    //         (int)BpaEligibilitycategorybpaCategoryType.Dollar)
        //                    //    IsMissMatch = true;
        //                    //else
        //                    //    IsMissMatch = false;

        //                    //#endregion
        //                    #endregion

        //                    if (currentEligibility == (int)MempberPlanBpaCurrentEligibility.Inactive)
        //                    {
        //                        //We are not doing anything just skip IN FUTURE IF WE WAMT TO CREATE EVERY MONTH INACTIVE TRANSACTION
        //                    }
        //                    else if (currentEligibility == (int)MempberPlanBpaCurrentEligibility.Frozen)
        //                    {
        //                        //We are not doing anything just skip IN FUTURE IF WE WAMT TO CREATE EVERY MONTH FROZEN TRANSACTION
        //                    }
        //                    else if (currentEligibility == (int)MempberPlanBpaCurrentEligibility.TransferOut)
        //                    {
        //                        #region ------- TRANSFER OUT --------
        //                        RecalculationTransactionHelper.CreateTransactionTransferOut(service, selectedContact.Id,
        //                            startWorkMonth, startWorkMonth.AddMonths(eligibilityOffset),
        //                            (int)BpaTransactionbpaTransactionType.TransferOut,
        //                            Guid.Empty, UsingEligibilityCategoty, defaultFund.Id);
        //                        ContactHelper.SetCurrentEligibility(service, tracingService,selectedContact.Id,
        //                            (int)MempberPlanBpaCurrentEligibility.TransferOut, startWorkMonth.AddMonths(eligibilityOffset));
        //                        #endregion
        //                    }
        //                    else if (currentEligibility == (int)MempberPlanBpaCurrentEligibility.NewMember)
        //                    {
        //                         #region ------------------- NEW MEMBER -----------------------

        //                        if (IsMissMatch)
        //                        {
        //                            RecalculationTransactionHelper.CreateUnderReviewTransaction(service,
        //                                selectedContact.Id, UsingEligibilityCategoty, startWorkMonth,
        //                                startWorkMonth.AddMonths(eligibilityOffset),
        //                                (int)BpaTransactionbpaTransactionCategory.Eligibility,
        //                                (int)BpaTransactionbpaTransactionType.UnderReview, defaultFund.Id);

        //                            ContactHelper.SetCurrentEligibility(service, tracingService,selectedContact.Id,
        //                                (int)MempberPlanBpaCurrentEligibility.UnderReview, startWorkMonth.AddMonths(eligibilityOffset));
        //                        }
        //                        else
        //                        {
        //                            //decimal miniInitEligibility = EligibilityCategoryHelper.GetMiniInitEligibility(UsingEligibilityCategoty.Id,
        //                            //    startWorkMonth.AddMonths(eligibilityOffset).AddDays(1));
        //                            decimal miniInitEligibility = 0;

        //                            if (IsEligiblityHours)
        //                                miniInitEligibility =
        //                                    RecalculationTransactionHelper.GetMiniInitEligibility_Hours(service,
        //                                        UsingEligibilityCategoty.Id,
        //                                        startWorkMonth.AddMonths(eligibilityOffset).AddDays(1));
        //                            else
        //                                miniInitEligibility = EligibilityCategoryHelper.GetMiniInitEligibility(service,
        //                                    UsingEligibilityCategoty.Id,
        //                                    startWorkMonth.AddMonths(eligibilityOffset).AddDays(1));

        //                            if (sumdollarhour >= miniInitEligibility)
        //                            {
        //                                //if (((SubSector_CalculationType != (int)BpaSubSectorBpaCalculationType.Flat) &&
        //                                //     (SubSector_CalculationType != (int)BpaSubSectorBpaCalculationType.Corporate))
        //                                //    ||
        //                                //    ((SubSector_CalculationType == (int)BpaSubSectorBpaCalculationType.Flat) &&
        //                                //     eligibilityCategoryType ==
        //                                //     (int)BpaEligibilitycategorybpaCategoryType.Dollar))
        //                                //{
        //                                //    if (IsEligiblityHours)
        //                                //        RecalculationTransactionHelper.CreateDrawTransaction_Hour(service,
        //                                //            selectedContact.Id, startWorkMonth,
        //                                //            startWorkMonth.AddMonths(eligibilityOffset),
        //                                //            (int)BpaTransactionbpaTransactionCategory.Eligibility,
        //                                //            (int)BpaTransactionbpaTransactionType.EligibilityDraw,
        //                                //            defaultFund, deductionRate, UsingEligibilityCategoty);
        //                                //    else
        //                                //        RecalculationTransactionHelper.CreateTransaction(service,
        //                                //            selectedContact.Id, startWorkMonth,
        //                                //            startWorkMonth.AddMonths(eligibilityOffset),
        //                                //            defaultFund, (int)BpaTransactionbpaTransactionCategory.Eligibility,
        //                                //            (int)BpaTransactionbpaTransactionType.EligibilityDraw, deductionRate,
        //                                //            false, null, defaultFund.Id);
        //                                //}

        //                                ContactHelper.SetCurrentEligibility(service, tracingService,selectedContact.Id,
        //                                    (int)MempberPlanBpaCurrentEligibility.InBenefit, startWorkMonth.AddMonths(eligibilityOffset));

        //                                RecalculationTransactionHelper.CreateTransaction(service, selectedContact.Id,
        //                                    startWorkMonth, startWorkMonth.AddMonths(eligibilityOffset),
        //                                    null, (int)BpaTransactionbpaTransactionCategory.Eligibility,
        //                                    (int)BpaTransactionbpaTransactionType.InBenefit,
        //                                    0, true, UsingEligibilityCategoty, defaultFund.Id);

        //                                selfPayCounter = 0;
        //                                inactiveMonthCounter = 0;
        //                            }
        //                            else
        //                            {
        //                                ContactHelper.SetCurrentEligibility(service, tracingService,selectedContact.Id,
        //                                    (int)MempberPlanBpaCurrentEligibility.NewMember, startWorkMonth.AddMonths(eligibilityOffset));

        //                                RecalculationTransactionHelper.CreateTransaction(service, selectedContact.Id,
        //                                    startWorkMonth, startWorkMonth.AddMonths(eligibilityOffset),
        //                                    null, (int)BpaTransactionbpaTransactionCategory.Eligibility,
        //                                    (int)BpaTransactionbpaTransactionType.NewMember,
        //                                    0, true, UsingEligibilityCategoty, defaultFund.Id);

        //                                inactiveMonthCounter = consecutiveinactivemonths + 1;
        //                            }
        //                        }

        //                        #endregion
        //                    } //end of New Number
        //                    else if ((currentEligibility == (int)MempberPlanBpaCurrentEligibility.InBenefit) ||
        //                             (currentEligibility == (int)MempberPlanBpaCurrentEligibility.SelfPayInBenefit))
        //                    {
        //                        #region --------------- INBENEFIT & SELFPAY IN BENEIT -----------------------

        //                        //Added by jwalin
        //                        if (IsMissMatch)
        //                        {
        //                            ContactHelper.SetCurrentEligibility(service, tracingService,selectedContact.Id,
        //                                (int)MempberPlanBpaCurrentEligibility.UnderReview, startWorkMonth.AddMonths(eligibilityOffset));

        //                            RecalculationTransactionHelper.CreateUnderReviewTransaction(service,
        //                                selectedContact.Id, UsingEligibilityCategoty, startWorkMonth,
        //                                startWorkMonth.AddMonths(eligibilityOffset),
        //                                (int)BpaTransactionbpaTransactionCategory.Eligibility,
        //                                (int)BpaTransactionbpaTransactionType.UnderReview, defaultFund.Id);
        //                        }
        //                        else
        //                        {
        //                            if (sumdollarhour + deductionRate >= 0)
        //                            {
        //                                //if (((SubSector_CalculationType != (int)BpaSubSectorBpaCalculationType.Flat) &&
        //                                //     (SubSector_CalculationType !=
        //                                //      (int)BpaSubSectorBpaCalculationType.Corporate)) //Added by jwalin
        //                                //    ||
        //                                //    ((SubSector_CalculationType == (int)BpaSubSectorBpaCalculationType.Flat) &&
        //                                //     eligibilityCategoryType ==
        //                                //     (int)BpaEligibilitycategorybpaCategoryType.Dollar))
        //                                //{
        //                                //    if (IsEligiblityHours)
        //                                //        RecalculationTransactionHelper.CreateDrawTransaction_Hour(service,
        //                                //            selectedContact.Id, startWorkMonth,
        //                                //            startWorkMonth.AddMonths(eligibilityOffset),
        //                                //            (int)BpaTransactionbpaTransactionCategory.Eligibility,
        //                                //            (int)BpaTransactionbpaTransactionType.EligibilityDraw,
        //                                //            defaultFund, deductionRate, UsingEligibilityCategoty);
        //                                //    else
        //                                //        RecalculationTransactionHelper.CreateTransaction(service,
        //                                //            selectedContact.Id, startWorkMonth,
        //                                //            startWorkMonth.AddMonths(eligibilityOffset),
        //                                //            defaultFund, (int)BpaTransactionbpaTransactionCategory.Eligibility,
        //                                //            (int)BpaTransactionbpaTransactionType.EligibilityDraw,
        //                                //            deductionRate, false, null, defaultFund.Id);
        //                                //}

        //                                if (previousMonthEligibility == null)
        //                                {
        //                                    //decimal miniInitEligibility = EligibilityCategoryHelper.GetMiniInitEligibility(UsingEligibilityCategoty.Id,
        //                                    //    startWorkMonth.AddMonths(eligibilityOffset).AddDays(1));
        //                                    decimal miniInitEligibility = 0;

        //                                    if (IsEligiblityHours)
        //                                        miniInitEligibility =
        //                                            RecalculationTransactionHelper.GetMiniInitEligibility_Hours(
        //                                                service, UsingEligibilityCategoty.Id,
        //                                                startWorkMonth.AddMonths(eligibilityOffset).AddDays(1));
        //                                    else
        //                                        miniInitEligibility =
        //                                            EligibilityCategoryHelper.GetMiniInitEligibility(service,
        //                                                UsingEligibilityCategoty.Id,
        //                                                startWorkMonth.AddMonths(eligibilityOffset).AddDays(1));

        //                                    if (sumdollarhour >= miniInitEligibility)
        //                                    {
        //                                        ContactHelper.SetCurrentEligibility(service, tracingService,selectedContact.Id,
        //                                            (int)MempberPlanBpaCurrentEligibility.InBenefit, startWorkMonth.AddMonths(eligibilityOffset));

        //                                        RecalculationTransactionHelper.CreateTransaction(service,
        //                                            selectedContact.Id, startWorkMonth,
        //                                            startWorkMonth.AddMonths(eligibilityOffset),
        //                                            null, (int)BpaTransactionbpaTransactionCategory.Eligibility,
        //                                            (int)BpaTransactionbpaTransactionType.InBenefit,
        //                                            0, true, UsingEligibilityCategoty, defaultFund.Id);

        //                                        //ContactHelper.SetConsecutiveSelfPayMonths(selectedContact.Id, true);
        //                                        selfPayCounter = 0;
        //                                        inactiveMonthCounter = 0;
        //                                    }
        //                                    else
        //                                    {
        //                                        ContactHelper.SetCurrentEligibility(service, tracingService,selectedContact.Id,
        //                                            (int)MempberPlanBpaCurrentEligibility.NewMember, startWorkMonth.AddMonths(eligibilityOffset));

        //                                        RecalculationTransactionHelper.CreateTransaction(service,
        //                                            selectedContact.Id, startWorkMonth,
        //                                            startWorkMonth.AddMonths(eligibilityOffset),
        //                                            null, (int)BpaTransactionbpaTransactionCategory.Eligibility,
        //                                            (int)BpaTransactionbpaTransactionType.NewMember,
        //                                            0, true, UsingEligibilityCategoty, defaultFund.Id);
        //                                        inactiveMonthCounter = consecutiveinactivemonths + 1;
        //                                    }
        //                                }
        //                                else
        //                                {
        //                                    //if (currentEligibility == (int)MempberPlan_bpa_CurrentEligibility.SelfPayInBenefit)
        //                                    ContactHelper.SetCurrentEligibility(service, tracingService,selectedContact.Id,
        //                                        (int)MempberPlanBpaCurrentEligibility.InBenefit, startWorkMonth.AddMonths(eligibilityOffset));

        //                                    RecalculationTransactionHelper.CreateTransaction(service,
        //                                        selectedContact.Id, startWorkMonth,
        //                                        startWorkMonth.AddMonths(eligibilityOffset),
        //                                        null, (int)BpaTransactionbpaTransactionCategory.Eligibility,
        //                                        (int)BpaTransactionbpaTransactionType.InBenefit,
        //                                        0, true, UsingEligibilityCategoty, defaultFund.Id);

        //                                    //ContactHelper.SetConsecutiveSelfPayMonths(selectedContact.Id, true);
        //                                    selfPayCounter = 0;
        //                                    inactiveMonthCounter = 0;
        //                                }
        //                            }
        //                            else if ((selfPayDeductionRate != 0) && (SelfPayBank.Value >= (selfPayDeductionRate * -1)))
        //                            {
        //                                #region ------------- Self Pay Bank Checking ------------------
        //                                //if (!Recalculation.TransactionHelper.DoesWorkMonthHasSelfPayTransaction(service, tracingService, startWorkMonth, selectedContact.Id))
        //                                //{

        //                                //    // Create SelfPay draw and Self pay In Benefit Transaction
        //                                //    RecalculationTransactionHelper.CreateTransaction(
        //                                //        service, selectedContact.Id, startWorkMonth,
        //                                //        startWorkMonth.AddMonths(eligibilityOffset),
        //                                //        defaultFund,
        //                                //        (int)BpaTransactionbpaTransactionCategory.Eligibility,
        //                                //        (int)BpaTransactionbpaTransactionType.SelfPay,
        //                                //        (selfPayDeductionRate * -1), false, UsingEligibilityCategoty, defaultFund.Id);

        //                                //}


        //                                //Find MaxConsecutiveMonthsSelfPay from Eligibility Category 
        //                                int MaxConsecutiveMonthsSelfPay = EligibilityCategoryHelper.GetMaxConsecutiveMonthsSelfPay(
        //                                        service, UsingEligibilityCategoty.Id);

        //                                //Find out MAx Month of Employer Contribution Contribution (if not found return WorkMonth)
        //                                DateTime ECMaxDate = TransactionHelper.FetchMaxEmployerContributionDate(service, tracingService, selectedContact.Id, startWorkMonth);

        //                                //Find out How many Previously SELFPAY
        //                                int cntSP = 0;
        //                                Entity lastEC = RecalculationTransactionHelper.FetchLastEligiblityMonthExceptSelfPay(service, tracingService,
        //                                    ECMaxDate, ECMaxDate, selectedContact.Id);
        //                                if (lastEC != null && lastEC.Attributes.Contains("bpa_transactiondate"))
        //                                {
        //                                    DateTime end = startWorkMonth.AddMonths(-1);
        //                                    DateTime start =
        //                                        lastEC.GetAttributeValue<DateTime>("bpa_transactiondate");
        //                                    cntSP = end.Month + end.Year * 12 - (start.Month + start.Year * 12);
        //                                }
        //                                if (cntSP == 0)
        //                                {
        //                                    cntSP = RecalculationTransactionHelper.GetHowManySelfPay(service, tracingService, ECMaxDate, startWorkMonth, selectedContact.Id);
        //                                }

        //                                if (cntSP < MaxConsecutiveMonthsSelfPay)
        //                                {
        //                                    // Create SelfPay draw and Self pay In Benefit Transaction
        //                                    RecalculationTransactionHelper.CreateTransaction(
        //                                        service, selectedContact.Id, startWorkMonth, startWorkMonth.AddMonths(eligibilityOffset),
        //                                            defaultFund,(int)BpaTransactionbpaTransactionCategory.Eligibility,
        //                                            (int)BpaTransactionbpaTransactionType.SelfPayDraw, selfPayDeductionRate, false, 
        //                                            UsingEligibilityCategoty, defaultFund.Id);

        //                                    RecalculationTransactionHelper.CreateTransaction(service,
        //                                       selectedContact.Id, startWorkMonth, startWorkMonth.AddMonths(eligibilityOffset),
        //                                       null, (int)BpaTransactionbpaTransactionCategory.Eligibility, (int)BpaTransactionbpaTransactionType.SelfPayInBenefit,
        //                                       0, true, UsingEligibilityCategoty, defaultFund.Id);

        //                                    // Update Selfpay counter 
        //                                    selfPayCounter = cntSP + 1;
        //                                    // Trigger Self Pay Bank Rollup
        //                                    PluginHelper.TriggerRollup(service, tracingService, "bpa_memberplan", selectedContact.Id, "bpa_selfpaybank");

        //                                    if (selfPayCounter == MaxConsecutiveMonthsSelfPay)
        //                                    {
        //                                        //CREATE "SelfPay Max Consecutive Reach" Transaction
        //                                        RecalculationTransactionHelper.CreateSelfPayMaxReach(service, startWorkMonth, startWorkMonth.AddMonths(eligibilityOffset), selectedContact.Id, null);
        //                                    }
        //                                }
        //                                else
        //                                {
        //                                    ContactHelper.SetCurrentEligibility(service, tracingService,selectedContact.Id,
        //                                        (int)MempberPlanBpaCurrentEligibility.NotInBenefit, startWorkMonth.AddMonths(eligibilityOffset));

        //                                    RecalculationTransactionHelper.CreateTransaction(service,
        //                                        selectedContact.Id, startWorkMonth,
        //                                        startWorkMonth.AddMonths(eligibilityOffset),
        //                                        null, (int)BpaTransactionbpaTransactionCategory.Eligibility,
        //                                        (int)BpaTransactionbpaTransactionType.NotInBenefit,
        //                                        0, true, UsingEligibilityCategoty, defaultFund.Id);
        //                                    inactiveMonthCounter = consecutiveinactivemonths + 1;
        //                                }
        //                                #endregion
        //                            }
        //                            else
        //                            {
        //                                ContactHelper.SetCurrentEligibility(service, tracingService,selectedContact.Id,
        //                                    (int)MempberPlanBpaCurrentEligibility.NotInBenefit, startWorkMonth.AddMonths(eligibilityOffset));

        //                                RecalculationTransactionHelper.CreateTransaction(service,
        //                                    selectedContact.Id, startWorkMonth,
        //                                    startWorkMonth.AddMonths(eligibilityOffset),
        //                                    null, (int)BpaTransactionbpaTransactionCategory.Eligibility,
        //                                    (int)BpaTransactionbpaTransactionType.NotInBenefit,
        //                                    0, true, UsingEligibilityCategoty, defaultFund.Id);
        //                                inactiveMonthCounter = consecutiveinactivemonths + 1;
        //                            }
        //                        }

        //                        #endregion
        //                    }
        //                    else if ((currentEligibility == (int)MempberPlanBpaCurrentEligibility.NotInBenefit) ||
        //                             (currentEligibility == (int)MempberPlanBpaCurrentEligibility.Reinstatement))
        //                    {
        //                        #region  -------------- NOT IN BENEFIT ----------------------------

        //                        bool IsResinstatement =
        //                            EligibilityCategoryHelper.IsCategoryReinstatement(service,
        //                                UsingEligibilityCategoty.Id);
        //                        decimal reinstatementAmount = 0;

        //                        if (IsResinstatement)
        //                        {
        //                            if (IsEligiblityHours)
        //                                reinstatementAmount =
        //                                    EligibilityCategoryHelper.GetReinstatementHours(service,
        //                                        UsingEligibilityCategoty.Id, startWorkMonth);
        //                            else
        //                                reinstatementAmount =
        //                                    EligibilityCategoryHelper.GetReinstatementDollars(service,
        //                                        UsingEligibilityCategoty.Id, startWorkMonth);

        //                            if (reinstatementAmount == 0)
        //                                IsMissMatch = true;
        //                        }

        //                        //Added by jwalin
        //                        if (IsMissMatch)
        //                        {
        //                            ContactHelper.SetCurrentEligibility(service, tracingService,selectedContact.Id,
        //                                (int)MempberPlanBpaCurrentEligibility.UnderReview, startWorkMonth.AddMonths(eligibilityOffset));

        //                            RecalculationTransactionHelper.CreateUnderReviewTransaction(service,
        //                                selectedContact.Id, UsingEligibilityCategoty, startWorkMonth,
        //                                startWorkMonth.AddMonths(eligibilityOffset),
        //                                (int)BpaTransactionbpaTransactionCategory.Eligibility,
        //                                (int)BpaTransactionbpaTransactionType.UnderReview, defaultFund.Id);
        //                        }
        //                        else
        //                        {
        //                            if (IsResinstatement &&
        //                                (currentEligibility ==
        //                                 (int)MempberPlanBpaCurrentEligibility.Reinstatement))
        //                            {
        //                                //Find out How many Previously SELFPAY
        //                                int cntIA = 0;
        //                                Entity lastIA =
        //                                    RecalculationTransactionHelper.FetchLastEligiblityContributionMonth(
        //                                        service, startWorkMonth, selectedContact.Id);
        //                                if (lastIA != null && lastIA.Attributes.Contains("bpa_transactiondate"))
        //                                {
        //                                    DateTime end = startWorkMonth.AddMonths(-1);
        //                                    DateTime start =
        //                                        lastIA.GetAttributeValue<DateTime>("bpa_transactiondate");
        //                                    cntIA = end.Month + end.Year * 12 - (start.Month + start.Year * 12);
        //                                }
        //                                if (sumdollarhour >= reinstatementAmount)
        //                                {
        //                                    if (IsEligiblityHours)
        //                                        RecalculationTransactionHelper.CreateDrawTransaction_Hour(
        //                                            service, selectedContact.Id, startWorkMonth,
        //                                            startWorkMonth.AddMonths(eligibilityOffset),
        //                                            (int)BpaTransactionbpaTransactionCategory.Eligibility,
        //                                            (int)BpaTransactionbpaTransactionType.EligibilityDraw,
        //                                            defaultFund, deductionRate, UsingEligibilityCategoty);
        //                                    else
        //                                        RecalculationTransactionHelper.CreateTransaction(service,
        //                                            selectedContact.Id, startWorkMonth,
        //                                            startWorkMonth.AddMonths(eligibilityOffset),
        //                                            defaultFund,
        //                                            (int)BpaTransactionbpaTransactionCategory.Eligibility,
        //                                            (int)BpaTransactionbpaTransactionType.EligibilityDraw,
        //                                            deductionRate, false, null, defaultFund.Id);

        //                                    ContactHelper.SetCurrentEligibility(service, tracingService,selectedContact.Id,
        //                                        (int)MempberPlanBpaCurrentEligibility.InBenefit, startWorkMonth.AddMonths(eligibilityOffset));

        //                                    RecalculationTransactionHelper.CreateTransaction(service,
        //                                        selectedContact.Id, startWorkMonth,
        //                                        startWorkMonth.AddMonths(eligibilityOffset),
        //                                        null, (int)BpaTransactionbpaTransactionCategory.Eligibility,
        //                                        (int)BpaTransactionbpaTransactionType.InBenefit,
        //                                        0, true, UsingEligibilityCategoty, defaultFund.Id);

        //                                    selfPayCounter = 0;
        //                                }
        //                                else
        //                                {
        //                                    RecalculationTransactionHelper.CreateTransaction(service,
        //                                        selectedContact.Id, startWorkMonth,
        //                                        startWorkMonth.AddMonths(eligibilityOffset),
        //                                        null, (int)BpaTransactionbpaTransactionCategory.Eligibility,
        //                                        (int)BpaTransactionbpaTransactionType.ReInstateing,
        //                                        0, true, UsingEligibilityCategoty, defaultFund.Id);

        //                                    ContactHelper.SetCurrentEligibility(service, tracingService,selectedContact.Id,
        //                                        (int)MempberPlanBpaCurrentEligibility.Reinstatement, startWorkMonth.AddMonths(eligibilityOffset));
        //                                    inactiveMonthCounter = cntIA + 1;
        //                                }
        //                            }
        //                            else
        //                            {
        //                                if (sumdollarhour + deductionRate >= 0)
        //                                {
        //                                    //if (((SubSector_CalculationType !=
        //                                    //      (int)BpaSubSectorBpaCalculationType.Flat) &&
        //                                    //     (SubSector_CalculationType !=
        //                                    //      (int)BpaSubSectorBpaCalculationType.Corporate))

        //                                    //    //Added by jwalin
        //                                    //    ||
        //                                    //    ((SubSector_CalculationType ==
        //                                    //      (int)BpaSubSectorBpaCalculationType.Flat) &&
        //                                    //     eligibilityCategoryType ==
        //                                    //     (int)BpaEligibilitycategorybpaCategoryType.Dollar))
        //                                    //{
        //                                    //    if (IsEligiblityHours)
        //                                    //        RecalculationTransactionHelper.CreateDrawTransaction_Hour(
        //                                    //            service, selectedContact.Id, startWorkMonth,
        //                                    //            startWorkMonth.AddMonths(eligibilityOffset),
        //                                    //            (int)BpaTransactionbpaTransactionCategory.Eligibility,
        //                                    //            (int)BpaTransactionbpaTransactionType.EligibilityDraw,
        //                                    //            defaultFund, deductionRate, UsingEligibilityCategoty);
        //                                    //    else
        //                                    //        RecalculationTransactionHelper.CreateTransaction(service,
        //                                    //            selectedContact.Id, startWorkMonth,
        //                                    //            startWorkMonth.AddMonths(eligibilityOffset),
        //                                    //            defaultFund,
        //                                    //            (int)BpaTransactionbpaTransactionCategory.Eligibility,
        //                                    //            (int)BpaTransactionbpaTransactionType.EligibilityDraw,
        //                                    //            deductionRate, false, null, defaultFund.Id);
        //                                    //}
        //                                    if (previousMonthEligibility == null)
        //                                    {
        //                                        ContactHelper.SetCurrentEligibility(service, tracingService,selectedContact.Id,
        //                                            (int)MempberPlanBpaCurrentEligibility.NewMember, startWorkMonth.AddMonths(eligibilityOffset));

        //                                        RecalculationTransactionHelper.CreateTransaction(service,
        //                                            selectedContact.Id, startWorkMonth,
        //                                            startWorkMonth.AddMonths(eligibilityOffset),
        //                                            null, (int)BpaTransactionbpaTransactionCategory.Eligibility,
        //                                            (int)BpaTransactionbpaTransactionType.NewMember,
        //                                            0, true, UsingEligibilityCategoty, defaultFund.Id);
        //                                        inactiveMonthCounter = consecutiveinactivemonths + 1;
        //                                    }
        //                                    else
        //                                    {
        //                                        ContactHelper.SetCurrentEligibility(service, tracingService,selectedContact.Id,
        //                                            (int)MempberPlanBpaCurrentEligibility.InBenefit, startWorkMonth.AddMonths(eligibilityOffset));

        //                                        RecalculationTransactionHelper.CreateTransaction(service,
        //                                            selectedContact.Id, startWorkMonth,
        //                                            startWorkMonth.AddMonths(eligibilityOffset),
        //                                            null, (int)BpaTransactionbpaTransactionCategory.Eligibility,
        //                                            (int)BpaTransactionbpaTransactionType.InBenefit,
        //                                            0, true, UsingEligibilityCategoty, defaultFund.Id);

        //                                        //ContactHelper.SetConsecutiveSelfPayMonths(selectedContact.Id, true);
        //                                        selfPayCounter = 0;
        //                                    }
        //                                }
        //                                /*else if (SelfPayBank.Value >= (selfPayDeductionRate * -1))
        //                                {
        //                                    #region ------------- Self Pay Bank Checking ------------------
        //                                    //if (!Recalculation.TransactionHelper.DoesWorkMonthHasSelfPayTransaction(service, tracingService, startWorkMonth, selectedContact.Id))
        //                                    //{

        //                                    //    // Create SelfPay draw and Self pay In Benefit Transaction
        //                                    //    RecalculationTransactionHelper.CreateTransaction(
        //                                    //        service, selectedContact.Id, startWorkMonth,
        //                                    //        startWorkMonth.AddMonths(eligibilityOffset),
        //                                    //        defaultFund,
        //                                    //        (int)BpaTransactionbpaTransactionCategory.Eligibility,
        //                                    //        (int)BpaTransactionbpaTransactionType.SelfPay,
        //                                    //        (selfPayDeductionRate * -1), false, UsingEligibilityCategoty, defaultFund.Id);

        //                                    //}


        //                                    //Find MaxConsecutiveMonthsSelfPay from Eligibility Category 
        //                                    int MaxConsecutiveMonthsSelfPay = EligibilityCategoryHelper.GetMaxConsecutiveMonthsSelfPay(
        //                                            service, UsingEligibilityCategoty.Id);

        //                                    //Find out MAx Month of Employer Contribution Contribution (if not found return WorkMonth)
        //                                    DateTime ECMaxDate = TransactionHelper.FetchMaxEmployerContributionDate(service, tracingService, selectedContact.Id, startWorkMonth);

        //                                    //Find out How many Previously SELFPAY
        //                                    int cntSP = 0;
        //                                    Entity lastEC = RecalculationTransactionHelper.FetchLastEligiblityMonthExceptSelfPay(service, tracingService,
        //                                            ECMaxDate, ECMaxDate, selectedContact.Id);
        //                                    if (lastEC != null && lastEC.Attributes.Contains("bpa_transactiondate"))
        //                                    {
        //                                        DateTime end = startWorkMonth.AddMonths(-1);
        //                                        DateTime start =
        //                                            lastEC.GetAttributeValue<DateTime>("bpa_transactiondate");
        //                                        cntSP = end.Month + end.Year * 12 - (start.Month + start.Year * 12);
        //                                    }
        //                                    if (cntSP == 0)
        //                                    {
        //                                        cntSP = RecalculationTransactionHelper.GetHowManySelfPay(service, tracingService, ECMaxDate, startWorkMonth, selectedContact.Id);
        //                                    }

        //                                    if (cntSP < MaxConsecutiveMonthsSelfPay)
        //                                    {
        //                                        // Create SelfPay draw and Self pay In Benefit Transaction
        //                                        RecalculationTransactionHelper.CreateTransaction(
        //                                        service, selectedContact.Id, startWorkMonth,
        //                                        startWorkMonth.AddMonths(eligibilityOffset),
        //                                        defaultFund,
        //                                        (int)BpaTransactionbpaTransactionCategory.Eligibility,
        //                                        (int)BpaTransactionbpaTransactionType.SelfPayDraw,
        //                                        selfPayDeductionRate, false, UsingEligibilityCategoty, defaultFund.Id);

        //                                        RecalculationTransactionHelper.CreateTransaction(service,
        //                                           selectedContact.Id, startWorkMonth,
        //                                           startWorkMonth.AddMonths(eligibilityOffset),
        //                                           null, (int)BpaTransactionbpaTransactionCategory.Eligibility,
        //                                           (int)BpaTransactionbpaTransactionType.SelfPayInBenefit,
        //                                           0, true, UsingEligibilityCategoty, defaultFund.Id);

        //                                        // Update Selfpay counter 
        //                                        selfPayCounter = cntSP + 1;
        //                                        // Trigger Self Pay Bank Rollup
        //                                        PluginHelper.TriggerRollup(service, tracingService, "bpa_memberplan", selectedContact.Id, "bpa_selfpaybank");

        //                                        if (selfPayCounter == MaxConsecutiveMonthsSelfPay)
        //                                        {
        //                                            //CREATE "SelfPay Max Consecutive Reach" Transaction
        //                                            RecalculationTransactionHelper.CreateSelfPayMaxReach(service,
        //                                                startWorkMonth, startWorkMonth.AddMonths(eligibilityOffset),
        //                                                selectedContact.Id, null);
        //                                        }
        //                                    }
        //                                    else
        //                                    {
        //                                        ContactHelper.SetCurrentEligibility(service, tracingService,selectedContact.Id,
        //                                            (int)MempberPlanBpaCurrentEligibility.NotInBenefit);

        //                                        RecalculationTransactionHelper.CreateTransaction(service,
        //                                            selectedContact.Id, startWorkMonth,
        //                                            startWorkMonth.AddMonths(eligibilityOffset),
        //                                            null, (int)BpaTransactionbpaTransactionCategory.Eligibility,
        //                                            (int)BpaTransactionbpaTransactionType.NotInBenefit,
        //                                            0, true, UsingEligibilityCategoty, defaultFund.Id);
        //                                        inactiveMonthCounter = consecutiveinactivemonths + 1;
        //                                    }
        //                                    #endregion

        //                                }*/
        //                                else
        //                                {
        //                                    ContactHelper.SetCurrentEligibility(service, tracingService,selectedContact.Id,
        //                                        (int)MempberPlanBpaCurrentEligibility.NotInBenefit, startWorkMonth.AddMonths(eligibilityOffset));

        //                                    RecalculationTransactionHelper.CreateTransaction(service,
        //                                        selectedContact.Id, startWorkMonth,
        //                                        startWorkMonth.AddMonths(eligibilityOffset),
        //                                        null, (int)BpaTransactionbpaTransactionCategory.Eligibility,
        //                                        (int)BpaTransactionbpaTransactionType.NotInBenefit,
        //                                        0, true, UsingEligibilityCategoty, defaultFund.Id);
        //                                    inactiveMonthCounter = consecutiveinactivemonths + 1;
        //                                }
        //                            }
        //                        }

        //                        #endregion
        //                    }
        //                    else if (currentEligibility == (int)MempberPlanBpaCurrentEligibility.UnderReview)
        //                    {
        //                        #region -------------------- UNDER REVIEW --------------------

        //                        if (previousMonthEligibility == null)
        //                        {
        //                            //decimal miniInitEligibility = EligibilityCategoryHelper.GetMiniInitEligibility(UsingEligibilityCategoty.Id,
        //                            //    startWorkMonth.AddMonths(eligibilityOffset).AddDays(1));
        //                            decimal miniInitEligibility = 0;

        //                            if (IsEligiblityHours)
        //                                miniInitEligibility =
        //                                    RecalculationTransactionHelper.GetMiniInitEligibility_Hours(
        //                                        service, UsingEligibilityCategoty.Id,
        //                                        startWorkMonth.AddMonths(eligibilityOffset).AddDays(1));
        //                            else
        //                                miniInitEligibility =
        //                                    EligibilityCategoryHelper.GetMiniInitEligibility(service,
        //                                        UsingEligibilityCategoty.Id,
        //                                        startWorkMonth.AddMonths(eligibilityOffset).AddDays(1));

        //                            if (sumdollarhour >= miniInitEligibility)
        //                            {
        //                                //if (SubSector_CalculationType !=
        //                                //    (int)BpaSubSectorBpaCalculationType.Flat)
        //                                //{
        //                                //    if (IsEligiblityHours)
        //                                //        RecalculationTransactionHelper.CreateDrawTransaction_Hour(
        //                                //            service, selectedContact.Id, startWorkMonth,
        //                                //            startWorkMonth.AddMonths(eligibilityOffset),
        //                                //            (int)BpaTransactionbpaTransactionCategory.Eligibility,
        //                                //            (int)BpaTransactionbpaTransactionType.EligibilityDraw,
        //                                //            defaultFund, deductionRate, UsingEligibilityCategoty);
        //                                //    else
        //                                //        RecalculationTransactionHelper.CreateTransaction(service,
        //                                //            selectedContact.Id, startWorkMonth,
        //                                //            startWorkMonth.AddMonths(eligibilityOffset),
        //                                //            defaultFund,
        //                                //            (int)BpaTransactionbpaTransactionCategory.Eligibility,
        //                                //            (int)BpaTransactionbpaTransactionType.EligibilityDraw,
        //                                //            deductionRate, false, null, defaultFund.Id);
        //                                //}

        //                                ContactHelper.SetCurrentEligibility(service, tracingService,selectedContact.Id,
        //                                    (int)MempberPlanBpaCurrentEligibility.InBenefit, startWorkMonth.AddMonths(eligibilityOffset));

        //                                RecalculationTransactionHelper.CreateTransaction(service,
        //                                    selectedContact.Id, startWorkMonth,
        //                                    startWorkMonth.AddMonths(eligibilityOffset),
        //                                    null, (int)BpaTransactionbpaTransactionCategory.Eligibility,
        //                                    (int)BpaTransactionbpaTransactionType.InBenefit,
        //                                    0, true, UsingEligibilityCategoty, defaultFund.Id);

        //                                //ContactHelper.SetConsecutiveSelfPayMonths(selectedContact.Id, true);
        //                                selfPayCounter = 0;
        //                            }
        //                            else
        //                            {
        //                                ContactHelper.SetCurrentEligibility(service, tracingService,selectedContact.Id,
        //                                    (int)MempberPlanBpaCurrentEligibility.NewMember, startWorkMonth.AddMonths(eligibilityOffset));

        //                                RecalculationTransactionHelper.CreateTransaction(service,
        //                                    selectedContact.Id, startWorkMonth,
        //                                    startWorkMonth.AddMonths(eligibilityOffset),
        //                                    null, (int)BpaTransactionbpaTransactionCategory.Eligibility,
        //                                    (int)BpaTransactionbpaTransactionType.NewMember,
        //                                    0, true, UsingEligibilityCategoty, defaultFund.Id);
        //                            }
        //                        }
        //                        else
        //                        {
        //                            ContactHelper.SetCurrentEligibility(service, tracingService,selectedContact.Id,
        //                                (int)MempberPlanBpaCurrentEligibility.UnderReview, startWorkMonth.AddMonths(eligibilityOffset));

        //                            RecalculationTransactionHelper.CreateUnderReviewTransaction(service,
        //                                selectedContact.Id, UsingEligibilityCategoty, startWorkMonth,
        //                                startWorkMonth.AddMonths(eligibilityOffset),
        //                                (int)BpaTransactionbpaTransactionCategory.Eligibility,
        //                                (int)BpaTransactionbpaTransactionType.UnderReview, defaultFund.Id);
        //                        }

        //                        #endregion
        //                    }
        //                    else if (currentEligibility == (int)MempberPlanBpaCurrentEligibility.FundAssisted)
        //                    {
        //                        #region ------------------------ Fund Assisted ------------------------------

        //                        RecalculationTransactionHelper.CreateFundAssistedTransactions(service,
        //                            selectedContact.Id, startWorkMonth,
        //                            startWorkMonth.AddMonths(eligibilityOffset), FA_fundassistanceamount, null,
        //                            (int)BpaTransactionbpaTransactionCategory.Eligibility,
        //                            (int)BpaTransactionbpaTransactionType.FundAssited, defaultFund,
        //                            currentEligibilityCategory, defaultFund.Id);

        //                        RecalculationTransactionHelper.CreateFundAssistedTransactions(service,
        //                            selectedContact.Id, startWorkMonth,
        //                            startWorkMonth.AddMonths(eligibilityOffset),
        //                            new Money(FA_deductionrate.Value * -1), null,
        //                            (int)BpaTransactionbpaTransactionCategory.Eligibility,
        //                            (int)BpaTransactionbpaTransactionType.EligibilityDraw, defaultFund, null,
        //                            defaultFund.Id);

        //                        RecalculationTransactionHelper.CreateFundAssistedTransactions(service,
        //                            selectedContact.Id, startWorkMonth,
        //                            startWorkMonth.AddMonths(eligibilityOffset), null, null,
        //                            (int)BpaTransactionbpaTransactionCategory.Eligibility,
        //                            (int)BpaTransactionbpaTransactionType.FundAssistedInBenefit, null,
        //                            currentEligibilityCategory, defaultFund.Id);

        //                        //SET Current Eligibility
        //                        ContactHelper.SetCurrentEligibility(service, tracingService,selectedContact.Id,
        //                            (int)MempberPlanBpaCurrentEligibility.FundAssisted, startWorkMonth.AddMonths(eligibilityOffset));

        //                        #endregion
        //                    }
                            

        //                    #region -------------- INACTIVE MONTH COUNTER -----------------
        //                    if (inactiveMonthCounter >= 0)
        //                    {
        //                        if (inactiveMonthCounter > maxInactiveCounter)
        //                            inactiveMonthCounter = maxInactiveCounter;

        //                        if (inactiveMonthCounter <= maxInactiveCounter)
        //                        {
        //                            ContactHelper.UpdateInactiveMonthsCounter(service, tracingService, selectedContact.Id,
        //                                inactiveMonthCounter);
        //                            if (inactiveMonthCounter > 0)
        //                            {
        //                                if (inactiveMonthCounter == maxInactiveCounter)
        //                                {
        //                                    EntityCollection _InactiveNReserveTransactions =
        //                                        RecalculationTransactionHelper.AlreadyHasInactiveAndReserveTransactions(
        //                                            service, selectedContact.Id, startWorkMonth.AddMonths(1));

        //                                    if (_InactiveNReserveTransactions != null &&
        //                                        _InactiveNReserveTransactions.Entities.Count > 0)
        //                                    {
        //                                        foreach (Entity _t in _InactiveNReserveTransactions.Entities)
        //                                            TransactionHelper.DeleteTransactions(service, _t.Id);
        //                                    }

        //                                    //CREATE 'INACTIVE' ELIGIBILITY TRANSACTION
        //                                    //CREATE 'RESERVE TRANSFER ' TRANSACTION
        //                                    RecalculationTransactionHelper.CreateTransaction(service,
        //                                        selectedContact.Id, startWorkMonth.AddMonths(1),
        //                                        startWorkMonth.AddMonths(eligibilityOffset + 1), string.Empty,
        //                                        (int)BpaTransactionbpaTransactionCategory.Eligibility,
        //                                        (int)BpaTransactionbpaTransactionType.Inactive,
        //                                        0, true, UsingEligibilityCategoty, true, defaultFund.Id);

        //                                    //UPDATE CONTACT 'CURRENT ELIGIBILITY' TO 'INACTIVE'
        //                                    ContactHelper.SetCurrentEligibility(service, tracingService,selectedContact.Id,
        //                                        (int)MempberPlanBpaCurrentEligibility.Inactive, startWorkMonth.AddMonths(eligibilityOffset));

        //                                    isMemberBecomeInActive = true;
        //                                }
        //                            }
        //                        }
        //                    }
        //                    #endregion

        //                }
        //            }
        //        } //end of ELSE

        //        #endregion

        //        #region ------------ Delete all inactive transactions --------------
        //        if (selfPayCounter >= 0)
        //        {
        //            //Update CONTACT SelfPay Count
        //            ContactHelper.SetConsecutiveSelfPayMonths(service, tracingService, selectedContact.Id, selfPayCounter);
        //        }

        //        if (isMemberBecomeInActive)
        //        {
        //            //HAS Future Records
        //            if (!TransactionHelper.HasFutureTransactions(service, startWorkMonth, selectedContact.Id))
        //                break;
        //            isMemberBecomeInActive = false;
        //        }

        //        //delete all inactive transactions - as user does not have rights so we update the flag and workflow will delete the transaction
        //        if (ToBeDeletedTransactions.Count > 0)
        //        {
        //            foreach (Guid transid in ToBeDeletedTransactions)
        //            {
        //                Entity trans = new Entity("bpa_transaction");
        //                trans.Id = transid;
        //                trans["bpa_deleteme"] = true;
        //                service.Update(trans);
        //            }
        //        }
        //        #endregion

        //        startWorkMonth = startWorkMonth.AddMonths(1);
        //        UsingEligibilityCategoty = null;
        //        Employer = null;
        //    } //end of while

        //    //Trigger Rollup after Retro Processing done
        //    PluginHelper.TriggerRollup(service, tracingService, PluginHelper.BpaMemberplan, selectedContact.Id, "bpa_contributiondollarbank");
        //    PluginHelper.TriggerRollup(service, tracingService, PluginHelper.BpaMemberplan, selectedContact.Id, "bpa_contributionhourbank");
        //    PluginHelper.TriggerRollup(service, tracingService, PluginHelper.BpaMemberplan, selectedContact.Id, "bpa_selfpaybank");

        //    return string.Empty;
        //}



        //static void RetroProcessingHourBase(IOrganizationService service, ITracingService tracingService, EntityReference selectedContact, 
        //    EntityReference UsingEligibilityCategoty, int currentEligibility, Entity previousMonthEligibility, DateTime startWorkMonth, 
        //    EntityReference defaultFund, bool IsMissMatch, int SubSector_CalculationType, bool HasSelfPayInBenefit, bool HasECED,
        //    out bool memberWentToUnderReview, out int selfPayCounter)
        //{
        //    //GET both hour Bank and Self Pay Bank
        //    decimal sumdollarhour = RecalculationTransactionHelper.GetSumHours(service, selectedContact.Id, startWorkMonth, defaultFund.Id);
        //    decimal sumSelpPay = TransactionHelper.FetchSumOfSelfPayBank(service, tracingService, selectedContact.Id, startWorkMonth);
        //    Money SelfPayBank = new Money(sumSelpPay);
        //    bool MemberWentToUnderReview = false;
        //    int SPCounter = 0;
        //    //Get All information for Elibiglity Category and Elibility CAtegory Detail
        //    Entity EligibilityCategory = EligibilityCategoryHelper.FetchEligibilityCategory(service, UsingEligibilityCategoty.Id);
        //    Entity EligibilityCategoryDetail = EligibilityCategoryHelper.FetchEligibilityDetail(service, EligibilityCategory.Id, startWorkMonth);
        //    int eligibilityOffset = EligibilityCategory.Contains("bpa_eligibilityoffsetmonths") ? EligibilityCategory.GetAttributeValue<int>("bpa_eligibilityoffsetmonths") : 0;
        //    decimal deductionRate = EligibilityCategoryDetail.Contains("bpa_hoursdeductionrate") ? EligibilityCategoryDetail.GetAttributeValue<decimal>("bpa_hoursdeductionrate") : 0;
        //    decimal selfPayDeductionRate = EligibilityCategoryDetail.Contains("bpa_selfpayamount") ? EligibilityCategoryDetail.GetAttributeValue<decimal>("bpa_selfpayamount") : 0;
        //    decimal miniInitEligibility = EligibilityCategoryDetail.Contains("bpa_minimumeligibility") ? EligibilityCategoryDetail.GetAttributeValue<decimal>("bpa_minimumeligibility") : 0;

        //    if (currentEligibility == (int)MempberPlanBpaCurrentEligibility.NewMember)
        //    {
        //        #region ------------------- NEW MEMBER -----------------------

        //        if (IsMissMatch)
        //        {
        //            RecalculationTransactionHelper.CreateUnderReviewTransaction(service, selectedContact.Id, UsingEligibilityCategoty, startWorkMonth,
        //                startWorkMonth.AddMonths(eligibilityOffset), (int)BpaTransactionbpaTransactionCategory.Eligibility,
        //                (int)BpaTransactionbpaTransactionType.UnderReview, defaultFund.Id);

        //            ContactHelper.SetCurrentEligibility(service, tracingService, selectedContact.Id,
        //                (int)MempberPlanBpaCurrentEligibility.UnderReview, startWorkMonth.AddMonths(eligibilityOffset));

        //            MemberWentToUnderReview = true;

        //        }
        //        else
        //        {
        //            if (sumdollarhour >= miniInitEligibility)
        //            {
        //                if ((SubSector_CalculationType != (int)BpaSubSectorBpaCalculationType.Flat) && (SubSector_CalculationType != (int)BpaSubSectorBpaCalculationType.Corporate))
        //                {

        //                        RecalculationTransactionHelper.CreateDrawTransaction_Hour(service, selectedContact.Id, startWorkMonth,
        //                            startWorkMonth.AddMonths(eligibilityOffset), (int)BpaTransactionbpaTransactionCategory.Eligibility,
        //                            (int)BpaTransactionbpaTransactionType.EligibilityDraw, defaultFund, deductionRate, UsingEligibilityCategoty);
        //                }

        //                ContactHelper.SetCurrentEligibility(service, tracingService, selectedContact.Id,
        //                    (int)MempberPlanBpaCurrentEligibility.InBenefit, startWorkMonth.AddMonths(eligibilityOffset));

        //                RecalculationTransactionHelper.CreateTransaction(service, selectedContact.Id, startWorkMonth,
        //                    startWorkMonth.AddMonths(eligibilityOffset), null, (int)BpaTransactionbpaTransactionCategory.Eligibility,
        //                    (int)BpaTransactionbpaTransactionType.InBenefit, 0, true, UsingEligibilityCategoty, defaultFund.Id);

        //                SPCounter = 0;

        //            }
        //            else
        //            {
        //                ContactHelper.SetCurrentEligibility(service, tracingService, selectedContact.Id,
        //                    (int)MempberPlanBpaCurrentEligibility.NewMember, startWorkMonth.AddMonths(eligibilityOffset));

        //                RecalculationTransactionHelper.CreateTransaction(service, selectedContact.Id, startWorkMonth,
        //                    startWorkMonth.AddMonths(eligibilityOffset), null, (int)BpaTransactionbpaTransactionCategory.Eligibility,
        //                    (int)BpaTransactionbpaTransactionType.NewMember, 0, true, UsingEligibilityCategoty, defaultFund.Id);
        //            }
        //            MemberWentToUnderReview = false;

        //        }

        //        #endregion
        //    } //end of New Number
        //    else if ((currentEligibility == (int)MempberPlanBpaCurrentEligibility.InBenefit) || (currentEligibility == (int)MempberPlanBpaCurrentEligibility.SelfPayInBenefit))
        //    {
        //        #region --------------- INBENEFIT & SELFPAY IN BENEIT -----------

        //        //Added by jwalin
        //        if (IsMissMatch)
        //        {
        //            #region ------ IS Miss Match ----------
        //            ContactHelper.SetCurrentEligibility(service, tracingService, selectedContact.Id,
        //                (int)MempberPlanBpaCurrentEligibility.UnderReview, startWorkMonth.AddMonths(eligibilityOffset));

        //            RecalculationTransactionHelper.CreateUnderReviewTransaction(
        //                service, selectedContact.Id, UsingEligibilityCategoty,
        //                startWorkMonth,
        //                startWorkMonth.AddMonths(eligibilityOffset),
        //                (int)BpaTransactionbpaTransactionCategory.Eligibility,
        //                (int)BpaTransactionbpaTransactionType.UnderReview, defaultFund.Id);

        //            MemberWentToUnderReview = true;

        //            #endregion
        //        }
        //        else
        //        {
        //            //Find MaxConsecutiveMonthsSelfPay from Eligibility Category 
        //            int MaxConsecutiveMonthsSelfPay = EligibilityCategoryHelper.GetMaxConsecutiveMonthsSelfPay(service, UsingEligibilityCategoty.Id);

        //            if (sumdollarhour + deductionRate >= 0)
        //            {
        //                #region ------------ Dollar Bank and Hour Bank has enough or not? ----------------
        //                if ((SubSector_CalculationType != (int)BpaSubSectorBpaCalculationType.Flat) && (SubSector_CalculationType != (int)BpaSubSectorBpaCalculationType.Corporate))
        //                {
        //                        RecalculationTransactionHelper.CreateDrawTransaction_Hour(service,selectedContact.Id, startWorkMonth,startWorkMonth.AddMonths(eligibilityOffset),
        //                            (int)BpaTransactionbpaTransactionCategory.Eligibility,(int)BpaTransactionbpaTransactionType.EligibilityDraw,defaultFund, deductionRate,
        //                            UsingEligibilityCategoty);
        //                }

        //                if (previousMonthEligibility == null)
        //                {
        //                    if (sumdollarhour >= miniInitEligibility)
        //                    {
        //                        ContactHelper.SetCurrentEligibility(service, tracingService, selectedContact.Id,
        //                            (int)MempberPlanBpaCurrentEligibility.InBenefit, startWorkMonth.AddMonths(eligibilityOffset));

        //                        RecalculationTransactionHelper.CreateTransaction(service, selectedContact.Id, startWorkMonth, startWorkMonth.AddMonths(eligibilityOffset),
        //                            null,(int)BpaTransactionbpaTransactionCategory.Eligibility,(int)BpaTransactionbpaTransactionType.InBenefit,
        //                            0, true, UsingEligibilityCategoty, defaultFund.Id);
        //                        SPCounter = 0;

        //                    }
        //                    else
        //                    {
        //                        ContactHelper.SetCurrentEligibility(service, tracingService, selectedContact.Id,
        //                            (int)MempberPlanBpaCurrentEligibility.NewMember, startWorkMonth.AddMonths(eligibilityOffset));

        //                        RecalculationTransactionHelper.CreateTransaction(service, selectedContact.Id, startWorkMonth,startWorkMonth.AddMonths(eligibilityOffset),
        //                            null,(int)BpaTransactionbpaTransactionCategory.Eligibility,(int)BpaTransactionbpaTransactionType.NewMember,
        //                            0, true, UsingEligibilityCategoty, defaultFund.Id);
        //                    }
        //                }
        //                else
        //                {
        //                    ContactHelper.SetCurrentEligibility(service, tracingService, selectedContact.Id,
        //                        (int)MempberPlanBpaCurrentEligibility.InBenefit, startWorkMonth.AddMonths(eligibilityOffset));

        //                    RecalculationTransactionHelper.CreateTransaction(service, selectedContact.Id, startWorkMonth,startWorkMonth.AddMonths(eligibilityOffset),
        //                        null, (int)BpaTransactionbpaTransactionCategory.Eligibility, (int)BpaTransactionbpaTransactionType.InBenefit,
        //                        0, true, UsingEligibilityCategoty, defaultFund.Id);

        //                    SPCounter = 0;
        //                }
        //                #endregion
        //            }
        //            else if ((selfPayDeductionRate != 0) && (SelfPayBank.Value >= (selfPayDeductionRate * -1)))
        //            {
        //                #region ------------- Self Pay Bank Checking ------------------

        //                //Find out MAx Month of Employer Contribution Contribution (if not found return WorkMonth)
        //                DateTime ECMaxDate = TransactionHelper.FetchMaxEmployerContributionDate(service, tracingService, selectedContact.Id, startWorkMonth);

        //                //Find out How many Previously SELFPAY
        //                int cntSP = 0;
        //                Entity lastEC = RecalculationTransactionHelper.FetchLastEligiblityMonthExceptSelfPay(service, tracingService,
        //                        ECMaxDate, ECMaxDate, selectedContact.Id);
        //                if (lastEC != null && lastEC.Attributes.Contains("bpa_transactiondate"))
        //                {
        //                    DateTime end = startWorkMonth.AddMonths(-1);
        //                    DateTime start =
        //                        lastEC.GetAttributeValue<DateTime>("bpa_transactiondate");
        //                    cntSP = end.Month + end.Year * 12 - (start.Month + start.Year * 12);
        //                }
        //                if (cntSP == 0)
        //                {
        //                    cntSP = RecalculationTransactionHelper.GetHowManySelfPay(service, tracingService, ECMaxDate, startWorkMonth, selectedContact.Id);
        //                }

        //                if (cntSP < MaxConsecutiveMonthsSelfPay)
        //                {
        //                    // Create SelfPay draw and Self pay In Benefit Transaction
        //                    RecalculationTransactionHelper.CreateTransaction(
        //                    service, selectedContact.Id, startWorkMonth,
        //                    startWorkMonth.AddMonths(eligibilityOffset),
        //                    defaultFund,
        //                    (int)BpaTransactionbpaTransactionCategory.Eligibility,
        //                    (int)BpaTransactionbpaTransactionType.SelfPayDraw,
        //                    selfPayDeductionRate, false, UsingEligibilityCategoty, defaultFund.Id);

        //                    RecalculationTransactionHelper.CreateTransaction(service,
        //                       selectedContact.Id, startWorkMonth,
        //                       startWorkMonth.AddMonths(eligibilityOffset),
        //                       null, (int)BpaTransactionbpaTransactionCategory.Eligibility,
        //                       (int)BpaTransactionbpaTransactionType.SelfPayInBenefit,
        //                       0, true, UsingEligibilityCategoty, defaultFund.Id);

        //                    selfPayCounter = cntSP + 1;

        //                    // Trigger Self Pay Bank Rollup
        //                    PluginHelper.TriggerRollup(service, tracingService, "bpa_memberplan", selectedContact.Id, "bpa_selfpaybank");

        //                    ContactHelper.SetCurrentEligibility(service, tracingService, selectedContact.Id,
        //                        (int)MempberPlanBpaCurrentEligibility.SelfPayInBenefit, startWorkMonth.AddMonths(eligibilityOffset));
        //                }
        //                else
        //                {
        //                    ContactHelper.SetCurrentEligibility(service, tracingService, selectedContact.Id,
        //                        (int)MempberPlanBpaCurrentEligibility.NotInBenefit, startWorkMonth.AddMonths(eligibilityOffset));

        //                    RecalculationTransactionHelper.CreateTransaction(service,
        //                        selectedContact.Id, startWorkMonth,
        //                        startWorkMonth.AddMonths(eligibilityOffset),
        //                        null, (int)BpaTransactionbpaTransactionCategory.Eligibility,
        //                        (int)BpaTransactionbpaTransactionType.NotInBenefit,
        //                        0, true, UsingEligibilityCategoty, defaultFund.Id);
        //                }
        //                #endregion
        //            }
        //            else
        //            {
        //                #region ---------- Else Part --------
        //                if (HasSelfPayInBenefit)
        //                {
        //                }
        //                else
        //                {
        //                    ContactHelper.SetCurrentEligibility(service, tracingService,
        //                        selectedContact.Id,
        //                        (int)MempberPlanBpaCurrentEligibility.NotInBenefit, startWorkMonth.AddMonths(eligibilityOffset));

        //                    RecalculationTransactionHelper.CreateTransaction(service,
        //                        selectedContact.Id, startWorkMonth,
        //                        startWorkMonth.AddMonths(eligibilityOffset),
        //                        null, (int)BpaTransactionbpaTransactionCategory.Eligibility,
        //                        (int)BpaTransactionbpaTransactionType.NotInBenefit,
        //                        0, true, UsingEligibilityCategoty, defaultFund.Id);
        //                }
        //                #endregion
        //            }
        //            MemberWentToUnderReview = false;

        //        }

        //        #endregion
        //    }
        //    else if ((currentEligibility == (int)MempberPlanBpaCurrentEligibility.NotInBenefit) || (currentEligibility == (int)MempberPlanBpaCurrentEligibility.Reinstatement))
        //    {
        //        #region  -------------- NOT IN BENEFIT ----------------------------

        //        bool IsResinstatement = EligibilityCategoryHelper.IsCategoryReinstatement(service, UsingEligibilityCategoty.Id);
        //        decimal reinstatementAmount = 0;

        //        if (IsResinstatement)
        //        {
        //            reinstatementAmount = EligibilityCategoryHelper.GetReinstatementHours(service, UsingEligibilityCategoty.Id, startWorkMonth);

        //            if (reinstatementAmount == 0)
        //                IsMissMatch = true;
        //        }

        //        //Added by jwalin
        //        if (IsMissMatch)
        //        {
        //            ContactHelper.SetCurrentEligibility(service, tracingService, selectedContact.Id,
        //                (int)MempberPlanBpaCurrentEligibility.UnderReview, startWorkMonth.AddMonths(eligibilityOffset));

        //            RecalculationTransactionHelper.CreateUnderReviewTransaction(service, selectedContact.Id, UsingEligibilityCategoty,
        //                startWorkMonth,startWorkMonth.AddMonths(eligibilityOffset),(int)BpaTransactionbpaTransactionCategory.Eligibility,
        //                (int)BpaTransactionbpaTransactionType.UnderReview, defaultFund.Id);
        //            MemberWentToUnderReview = false;

        //        }
        //        else
        //        {
        //            if (IsResinstatement && HasECED)
        //            {
        //                if (sumdollarhour >= reinstatementAmount)
        //                {
        //                    RecalculationTransactionHelper.CreateDrawTransaction_Hour(service,selectedContact.Id, startWorkMonth,
        //                                startWorkMonth.AddMonths(eligibilityOffset),(int)BpaTransactionbpaTransactionCategory.Eligibility,
        //                                (int)BpaTransactionbpaTransactionType.EligibilityDraw,defaultFund, deductionRate, UsingEligibilityCategoty);

        //                    ContactHelper.SetCurrentEligibility(service, tracingService,selectedContact.Id,
        //                        (int)MempberPlanBpaCurrentEligibility.InBenefit, startWorkMonth.AddMonths(eligibilityOffset));

        //                    RecalculationTransactionHelper.CreateTransaction(service, selectedContact.Id, startWorkMonth,startWorkMonth.AddMonths(eligibilityOffset),
        //                        null,(int)BpaTransactionbpaTransactionCategory.Eligibility,(int)BpaTransactionbpaTransactionType.InBenefit,
        //                        0, true, UsingEligibilityCategoty, defaultFund.Id);
        //                }
        //                else
        //                {
        //                    RecalculationTransactionHelper.CreateTransaction(service, selectedContact.Id, startWorkMonth,startWorkMonth.AddMonths(eligibilityOffset),
        //                        null,(int)BpaTransactionbpaTransactionCategory.Eligibility,(int)BpaTransactionbpaTransactionType.ReInstateing,
        //                        0, true, UsingEligibilityCategoty, defaultFund.Id);

        //                    ContactHelper.SetCurrentEligibility(service, tracingService, selectedContact.Id,
        //                        (int)MempberPlanBpaCurrentEligibility.Reinstatement, startWorkMonth.AddMonths(eligibilityOffset));
        //                }
        //            }
        //            else
        //            {
        //                if (sumdollarhour + deductionRate >= 0)
        //                {
        //                    if ((SubSector_CalculationType != (int)BpaSubSectorBpaCalculationType.Flat) && (SubSector_CalculationType != (int)BpaSubSectorBpaCalculationType.Corporate))
        //                    {
        //                        RecalculationTransactionHelper.CreateDrawTransaction_Hour(service,selectedContact.Id, startWorkMonth,
        //                            startWorkMonth.AddMonths(eligibilityOffset),(int)BpaTransactionbpaTransactionCategory.Eligibility,
        //                            (int)BpaTransactionbpaTransactionType.EligibilityDraw,defaultFund, deductionRate,UsingEligibilityCategoty);
        //                    }
        //                    if (previousMonthEligibility == null)
        //                    {
        //                        ContactHelper.SetCurrentEligibility(service, tracingService,selectedContact.Id,
        //                            (int)MempberPlanBpaCurrentEligibility.NewMember, startWorkMonth.AddMonths(eligibilityOffset));

        //                        RecalculationTransactionHelper.CreateTransaction(service, selectedContact.Id, startWorkMonth,startWorkMonth.AddMonths(eligibilityOffset),
        //                            null,(int)BpaTransactionbpaTransactionCategory.Eligibility,(int)BpaTransactionbpaTransactionType.NewMember,
        //                            0, true, UsingEligibilityCategoty,defaultFund.Id);
        //                    }
        //                    else
        //                    {
        //                        ContactHelper.SetCurrentEligibility(service, tracingService, selectedContact.Id,
        //                            (int)MempberPlanBpaCurrentEligibility.InBenefit, startWorkMonth.AddMonths(eligibilityOffset));

        //                        RecalculationTransactionHelper.CreateTransaction(service, selectedContact.Id, startWorkMonth, startWorkMonth.AddMonths(eligibilityOffset),
        //                            null,(int)BpaTransactionbpaTransactionCategory.Eligibility,(int)BpaTransactionbpaTransactionType.InBenefit,
        //                            0, true, UsingEligibilityCategoty,defaultFund.Id);
        //                    }
        //                }
        //                else
        //                {
        //                    ContactHelper.SetCurrentEligibility(service, tracingService, selectedContact.Id,
        //                        (int)MempberPlanBpaCurrentEligibility.NotInBenefit, startWorkMonth.AddMonths(eligibilityOffset));

        //                    RecalculationTransactionHelper.CreateTransaction(service, selectedContact.Id, startWorkMonth,startWorkMonth.AddMonths(eligibilityOffset),
        //                        null,(int)BpaTransactionbpaTransactionCategory.Eligibility,(int)BpaTransactionbpaTransactionType.NotInBenefit,
        //                        0, true, UsingEligibilityCategoty, defaultFund.Id);
        //                }
        //            }
        //            MemberWentToUnderReview = false;

        //        }

        //        #endregion
        //    }
        //    else if (currentEligibility == (int)MempberPlanBpaCurrentEligibility.UnderReview)
        //    {
        //        #region -------------------- UNDER REVIEW --------------------

        //        if (previousMonthEligibility == null)
        //        {
        //            if (sumdollarhour >= miniInitEligibility)
        //            {
        //                if (SubSector_CalculationType != (int)BpaSubSectorBpaCalculationType.Flat)
        //                {
        //                    RecalculationTransactionHelper.CreateDrawTransaction_Hour(service,selectedContact.Id, startWorkMonth,
        //                        startWorkMonth.AddMonths(eligibilityOffset),(int)BpaTransactionbpaTransactionCategory.Eligibility,
        //                        (int)BpaTransactionbpaTransactionType.EligibilityDraw,defaultFund, deductionRate,UsingEligibilityCategoty);
        //                }

        //                ContactHelper.SetCurrentEligibility(service, tracingService,selectedContact.Id,
        //                    (int)MempberPlanBpaCurrentEligibility.InBenefit, startWorkMonth.AddMonths(eligibilityOffset));

        //                RecalculationTransactionHelper.CreateTransaction(service, selectedContact.Id, startWorkMonth,
        //                    startWorkMonth.AddMonths(eligibilityOffset),null,(int)BpaTransactionbpaTransactionCategory.Eligibility,
        //                    (int)BpaTransactionbpaTransactionType.InBenefit, 0, true, UsingEligibilityCategoty, defaultFund.Id);
        //            }
        //            else
        //            {
        //                ContactHelper.SetCurrentEligibility(service, tracingService,selectedContact.Id,
        //                    (int)MempberPlanBpaCurrentEligibility.NewMember, startWorkMonth.AddMonths(eligibilityOffset));

        //                RecalculationTransactionHelper.CreateTransaction(service, selectedContact.Id, startWorkMonth,
        //                    startWorkMonth.AddMonths(eligibilityOffset),null,(int)BpaTransactionbpaTransactionCategory.Eligibility,
        //                    (int)BpaTransactionbpaTransactionType.NewMember,0, true, UsingEligibilityCategoty, defaultFund.Id);
        //            }
        //            MemberWentToUnderReview = false;

        //        }
        //        else
        //        {
        //            ContactHelper.SetCurrentEligibility(service, tracingService,selectedContact.Id,
        //                (int)MempberPlanBpaCurrentEligibility.UnderReview, startWorkMonth.AddMonths(eligibilityOffset));

        //            RecalculationTransactionHelper.CreateUnderReviewTransaction(service, selectedContact.Id, UsingEligibilityCategoty,
        //                startWorkMonth,startWorkMonth.AddMonths(eligibilityOffset),(int)BpaTransactionbpaTransactionCategory.Eligibility,
        //                (int)BpaTransactionbpaTransactionType.UnderReview,defaultFund.Id);

        //            MemberWentToUnderReview = true;

        //        }

        //        #endregion
        //    }


        //    memberWentToUnderReview = MemberWentToUnderReview;
        //    selfPayCounter = SPCounter;
        //}
    }
}