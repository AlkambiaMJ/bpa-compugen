﻿#region

using System;
using System.Activities;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xrm.Sdk;

#endregion

namespace BPA.Benefit.Management.CRM.Plugins.Helper
{
    public class MemberRetroprocessing_Old
    {
        public static string RecalculateTransaction(IOrganizationService service, ITracingService tracingService, EntityReference selectedContact,
            DateTime Start_WorkMonth, bool IsMemberCategoryChanged, bool IsRunFromMonthlyTrigger, EntityReference CurrentEligibilityCategory)
        {
            DateTime startTime = DateTime.Now;

            // Retrieve the id
            Guid memberId = selectedContact.Id;

            //Get Current Eligibility from Member Plan
            //EntityReference CurrentEligibilityCategory = ContactHelper.FetchCurrentEligibilityCategory(service, tracingService, selectedContact.Id);

            if(CurrentEligibilityCategory == null)
                CurrentEligibilityCategory = ContactHelper.FetchCurrentEligibilityCategory(service, tracingService, selectedContact.Id);
            

            //Get MAX Work Month from Transaction
            DateTime Last_WorkMonth = DateTime.Today.AddMonths(-1);
            Last_WorkMonth = new DateTime(Last_WorkMonth.Year, Last_WorkMonth.Month, 1);
            
            //Declared all variables
            List<Guid> SkipTransactions = null;
            bool IsMemberBecomeInActive = false;

            int SelfPayCounter = 0, InactiveMonthCounter = 0, MaxInactiveCounter = 0;

            //Find Default Welfare Fund for MEmber Plan
            EntityReference DefaultFund = null;
            DefaultFund = ContactHelper.FetchDefaultFundByMemberId(service, memberId);
            if (DefaultFund == null)
                DefaultFund = new EntityReference("bpa_fund", Guid.Empty);

            //find Member Plan is retiree ?
            bool IsRetireePlan = ContactHelper.IsMemberPlanRetiree(service, tracingService, selectedContact.Id);

            //Select all Future Tansactions
            EntityCollection M_transactions = RecalculationTransactionHelper.FetchAllFutureTransactions(service, selectedContact.Id, Start_WorkMonth);

            #region ----------------- INACTIVATE ALL ELIGIBILITY TRANSACTIONS -----------------------------

            if (M_transactions != null && M_transactions.Entities.Count > 0)
            {
                List<Entity> M_transactionsList = M_transactions.Entities.ToList();

                //Make all transactions ELIGIBILITY TRANNSACTION INACTIVE
                foreach (Entity transaction in M_transactions.Entities)
                {
                    int transactionType = transaction.GetAttributeValue<OptionSetValue>("bpa_transactiontype").Value;

                    if ((transactionType == (int)BpaTransactionbpaTransactionType.EmployerContribution) ||
                        (transactionType == (int)BpaTransactionbpaTransactionType.DollarBankAdjustment) ||
                        (transactionType == (int)BpaTransactionbpaTransactionType.HourBankAdjustment) ||
                        (transactionType == (int)BpaTransactionbpaTransactionType.SecondaryDollarBankAdjustment))
                    {
                        //Do not do any thing move next trrnasaction
                    }
                    else if ((transactionType == (int)BpaTransactionbpaTransactionType.EligibilityDraw) ||
                        (transactionType == (int)BpaTransactionbpaTransactionType.ReserveTransfer) ||
                        (transactionType == (int)BpaTransactionbpaTransactionType.InBenefit) ||
                        (transactionType == (int)BpaTransactionbpaTransactionType.NotInBenefit) ||
                        (transactionType == (int)BpaTransactionbpaTransactionType.FundAssisted) ||
                        (transactionType == (int)BpaTransactionbpaTransactionType.FundAssistedInBenefit) ||
                        (transactionType == (int)BpaTransactionbpaTransactionType.UnderReview) ||
                        (transactionType == (int)BpaTransactionbpaTransactionType.NewMember) ||
                        (transactionType == (int)BpaTransactionbpaTransactionType.ReInstating) ||
                        (transactionType == (int)BpaTransactionbpaTransactionType.SelfPay) ||
                        (transactionType == (int)BpaTransactionbpaTransactionType.SelfPayMaxConsecutiveReach) ||
                        (transactionType == (int)BpaTransactionbpaTransactionType.SelfPayInBenefit) ||
                        (transactionType == (int)BpaTransactionbpaTransactionType.SelfPayDraw) ||
                        (transactionType == (int)BpaTransactionbpaTransactionType.Inactive) ||
                        (transactionType == (int)BpaTransactionbpaTransactionType.Frozen) || 
                        (transactionType == (int)BpaTransactionbpaTransactionType.SecondaryDollarBankDraw))
                    {
                        if (transactionType == (int)BpaTransactionbpaTransactionType.Frozen)
                        {
                            ContactHelper.SetCurrentEligibility(service, tracingService, selectedContact.Id, (int)MemberPlanBpaCurrentEligibility.Frozen, DateTime.MinValue);
                        }
                        else if ((transactionType == (int)BpaTransactionbpaTransactionType.Inactive) || (transactionType == (int)BpaTransactionbpaTransactionType.ReserveTransfer))
                        {
                            List<Entity> lsEC = M_transactionsList.Where(x =>
                                       (x.GetAttributeValue<OptionSetValue>("bpa_transactiontype").Value == (int)BpaTransactionbpaTransactionType.EmployerContribution) &&
                                       (x.GetAttributeValue<OptionSetValue>("bpa_transactiontype").Value == (int)BpaTransactionbpaTransactionType.Inactive) &&
                                       (x.GetAttributeValue<OptionSetValue>("bpa_transactiontype").Value == (int)BpaTransactionbpaTransactionType.ReserveTransfer)).ToList();

                            bool reciprocaltransfer = false;
                            if (transactionType == (int)BpaTransactionbpaTransactionType.ReserveTransfer)
                                reciprocaltransfer = transaction.GetAttributeValue<bool>("bpa_reciprocaltransfer");

                            if (!reciprocaltransfer)
                            {
                                RecalculationTransactionHelper.SetTransactionStatus(service, transaction.Id, false);
                                RecalculationTransactionHelper.DeleteTransaction(service, transaction.Id);

                            }
                        }
                        else
                        {
                            RecalculationTransactionHelper.SetTransactionStatus(service, transaction.Id, false);
                            RecalculationTransactionHelper.DeleteTransaction(service, transaction.Id);

                        }
                    }

                }

            }
            #endregion

            //Loop throght start date to end date
            while (Start_WorkMonth <= Last_WorkMonth)
            {
                #region -- Start DATE LOOP ----

                SkipTransactions = new List<Guid>();
                List<Guid> ToBeDeletedTransactions = new List<Guid>();

                //Find HOW MANY ELIGIBILITY CONTRIBUTION TRANSACTIONS? IF MORE THAN 1 THAN GET THE LAST AND INGOR EVERY OTHER TRANSACTIONS
                EntityCollection EligibilityContributionTransactions =
                    RecalculationTransactionHelper.FetchAllEligibilityDrawTransactions(service, selectedContact.Id,
                        Start_WorkMonth, (int)BpaTransactionbpaTransactionType.EmployerContribution);
                if (EligibilityContributionTransactions != null && EligibilityContributionTransactions.Entities.Count > 1)
                {
                    int iCnt = 0;
                    foreach (Entity t in EligibilityContributionTransactions.Entities)
                    {
                        if (iCnt != 0)
                            SkipTransactions.Add(t.Id);
                        iCnt++;
                    }
                }

                //Get Current Eligibity from Contact
                int currentEligibility = 0;
                //int eligibilityCategoryType = ContactHelper.GetCurrentEligibilityType(service, tracingService, CurrentEligibilityCategory.Id);
                
                //Get Previous Month Eligibility Values
                #region ----- Get Previous Month Eligibility Values AND SET to Current Eligibility

                Entity previousMonthEligibility = RecalculationTransactionHelper.FetchPreviousMonthEligibility(service, selectedContact.Id, Start_WorkMonth.AddMonths(-1));
                int previousMonthEligibilityType = 0;

                if (previousMonthEligibility != null)
                {
                    previousMonthEligibilityType = previousMonthEligibility.GetAttributeValue<OptionSetValue>("bpa_transactiontype").Value;

                    //Put the Condition for CHECKING
                    switch (previousMonthEligibilityType)
                    {
                        case (int)BpaTransactionbpaTransactionType.InBenefit:
                            currentEligibility = (int)MemberPlanBpaCurrentEligibility.InBenefit;
                            break;
                        case (int)BpaTransactionbpaTransactionType.NewMember:
                            currentEligibility = (int)MemberPlanBpaCurrentEligibility.NewMember;
                            break;
                        case (int)BpaTransactionbpaTransactionType.NotInBenefit:
                            currentEligibility = (int)MemberPlanBpaCurrentEligibility.NotInBenefit;
                            break;
                        case (int)BpaTransactionbpaTransactionType.FundAssistedInBenefit:
                            currentEligibility = (int)MemberPlanBpaCurrentEligibility.InBenefit;
                            break;
                        case (int)BpaTransactionbpaTransactionType.SelfPayInBenefit:
                            currentEligibility = (int)MemberPlanBpaCurrentEligibility.SelfPayInBenefit;
                            break;
                        case (int)BpaTransactionbpaTransactionType.UnderReview:
                            currentEligibility = (int)MemberPlanBpaCurrentEligibility.UnderReview;
                            break;
                        case (int)BpaTransactionbpaTransactionType.TransferOut:
                            currentEligibility = (int)MemberPlanBpaCurrentEligibility.TransferOut;
                            break;
                        case (int)BpaTransactionbpaTransactionType.ReInstating:
                            currentEligibility = (int)MemberPlanBpaCurrentEligibility.Reinstatement;
                            break;
                    }
                }
                else
                {
                    currentEligibility = (int)MemberPlanBpaCurrentEligibility.NewMember;
                }

                #endregion

                //Fetch All Transactions for that month
                 M_transactions = RecalculationTransactionHelper.FetchAllTransactions(service, selectedContact.Id, Start_WorkMonth);

                //find Member Eligibility Category
                EntityReference elibilityCategory = null;
                if (IsMemberCategoryChanged)
                    elibilityCategory = CurrentEligibilityCategory;
                else
                    elibilityCategory = TransactionHelper.FetchCurrentEligibilityCategory(service, tracingService, selectedContact.Id, Start_WorkMonth);
                EntityReference UsingEligibilityCategoty = elibilityCategory;

                List<Entity> M_transactionsList = null;
                bool HasECED = false,
                    HasReciprocalTransferOUT = false,
                    HasReciprocalTransferIN = false,
                    IsTransferAmountPaid = false,
                    HasTrasfer = false,
                    IsReciprocalTransferOutDelete = false,
                    HasSelfPayInBenefit = false,
                    MemberWentToUnderReview = false;
                int EC_CalculationType = 0;


                //Find Last Agreement of Employer Contribution OR Self Pay Deposit Received
                EntityReference lastAgreement = TransactionHelper.FetchLastContributionAgreement(service, tracingService, selectedContact.Id, Start_WorkMonth);

                //Check if Transcations Count is More than 0
                if (M_transactions != null && M_transactions.Entities.Count > 0)
                {
                    //Convert Entities Collection to List
                    M_transactionsList = M_transactions.Entities.ToList();

                    #region ----- Setting all Boolean variables -------
                    //Does this list has Employer Contribution
                    List<Entity> doesNotHaveECED = M_transactionsList.Where(x =>
                                (x.GetAttributeValue<OptionSetValue>("bpa_transactiontype").Value == (int)BpaTransactionbpaTransactionType.EmployerContribution) ||
                                (x.GetAttributeValue<OptionSetValue>("bpa_transactiontype").Value == (int)BpaTransactionbpaTransactionType.HourBankAdjustment) ||
                                (x.GetAttributeValue<OptionSetValue>("bpa_transactiontype").Value == (int)BpaTransactionbpaTransactionType.DollarBankAdjustment)).ToList();
                    if (doesNotHaveECED != null && doesNotHaveECED.Count > 0)
                        HasECED = true;

                    //Not sure it execute this condition or not?
                    List<Entity> lsFundAssistedAndEmployeeContributions = M_transactionsList.Where(x =>
                                (x.GetAttributeValue<OptionSetValue>("bpa_transactiontype").Value == (int)BpaTransactionbpaTransactionType.EmployerContribution) ||
                                (x.GetAttributeValue<OptionSetValue>("bpa_transactiontype").Value == (int)BpaTransactionbpaTransactionType.FundAssisted)).ToList();
                    if (lsFundAssistedAndEmployeeContributions != null && lsFundAssistedAndEmployeeContributions.Count >= 2)
                    {
                        foreach (Entity e in lsFundAssistedAndEmployeeContributions)
                        {
                            int transactionType1 = e.GetAttributeValue<OptionSetValue>("bpa_transactiontype").Value;

                            if (transactionType1 == (int)BpaTransactionbpaTransactionType.FundAssisted)
                            {
                                SkipTransactions.Add(e.Id);
                                break;
                            }
                        }
                    }


                    //Check this month has Self-Pay Benefit or not?
                    List<Entity> ListOfSelfPayAndSelfPayRefund = M_transactionsList.Where(x =>
                                x.GetAttributeValue<OptionSetValue>("bpa_transactiontype").Value == (int)BpaTransactionbpaTransactionType.SelfPayInBenefit).ToList();
                    if (ListOfSelfPayAndSelfPayRefund != null && ListOfSelfPayAndSelfPayRefund.Count > 0)
                        HasSelfPayInBenefit = true;
                    else
                        HasSelfPayInBenefit = false;

                    //Check this month has FUND ASSISTED or NOT?
                    bool HasFundAssisted = false;
                    List<Entity> ListOfFundAssisted = M_transactionsList.Where(x =>
                                x.GetAttributeValue<OptionSetValue>("bpa_transactiontype").Value == (int)BpaTransactionbpaTransactionType.FundAssisted).ToList();
                    if (ListOfFundAssisted != null && ListOfFundAssisted.Count > 0)
                        HasFundAssisted = true;
                    else
                        HasFundAssisted = false;

                    #endregion

                    #region ----------------- Member Transfer OUT/ IN ? -----------------------------
                    //Does this list has Transfer Out Transaction
                    List<Entity> ListOfReciprocalTransferOut = M_transactionsList.Where(x =>
                                x.GetAttributeValue<OptionSetValue>("bpa_transactiontype").Value == (int)BpaTransactionbpaTransactionType.TransferOut).ToList();
                    if (ListOfReciprocalTransferOut != null && ListOfReciprocalTransferOut.Count > 0)
                        HasReciprocalTransferOUT = true;
                    else
                        HasReciprocalTransferOUT = false;

                    //Does this list has Transfer Transaction
                    List<Entity> ListOfTransfer = M_transactionsList.Where(x =>
                                x.GetAttributeValue<OptionSetValue>("bpa_transactiontype").Value == (int)BpaTransactionbpaTransactionType.Reciprocal).ToList();
                    if (ListOfTransfer != null && ListOfTransfer.Count > 0)
                    {
                        HasTrasfer = true;
                        Entity TransactionPaid = ListOfTransfer[0];
                        if (TransactionPaid.Attributes.Contains("bpa_reciprocaltransfer"))
                            IsTransferAmountPaid = TransactionPaid.GetAttributeValue<bool>("bpa_reciprocaltransfer");
                        else
                            IsTransferAmountPaid = false;
                    }
                    else
                    {
                        IsTransferAmountPaid = false;
                        HasTrasfer = false;
                    }

                    //Does this list has Transfer In Transactions
                    List<Entity> ListOfReciprocalTransferIN = M_transactionsList.Where(x =>
                                x.GetAttributeValue<OptionSetValue>("bpa_transactiontype").Value == (int)BpaTransactionbpaTransactionType.TransferIn).ToList();
                    if (ListOfReciprocalTransferIN != null && ListOfReciprocalTransferIN.Count > 0)
                        HasReciprocalTransferIN = true;
                    else
                        HasReciprocalTransferIN = false;

                    #endregion
                    
                    #region ----------------- INACTIVATE ALL ELIGIBILITY TRANSACTIONS -----------------------------

                    //Make all transactions ELIGIBILITY TRANNSACTION INACTIVE
                    foreach (Entity transaction in M_transactions.Entities)
                    {
                        int transactionType = transaction.GetAttributeValue<OptionSetValue>("bpa_transactiontype").Value;

                        if ((transactionType == (int)BpaTransactionbpaTransactionType.EligibilityDraw) ||
                            (transactionType == (int)BpaTransactionbpaTransactionType.ReserveTransfer) ||
                            (transactionType == (int)BpaTransactionbpaTransactionType.InBenefit) ||
                            (transactionType == (int)BpaTransactionbpaTransactionType.NotInBenefit) ||
                            (transactionType == (int)BpaTransactionbpaTransactionType.FundAssisted) ||
                            (transactionType == (int)BpaTransactionbpaTransactionType.FundAssistedInBenefit) ||
                            (transactionType == (int)BpaTransactionbpaTransactionType.UnderReview) ||
                            (transactionType == (int)BpaTransactionbpaTransactionType.NewMember) ||
                            (transactionType == (int)BpaTransactionbpaTransactionType.ReInstating) ||
                            (transactionType == (int)BpaTransactionbpaTransactionType.SelfPay) ||
                            (transactionType == (int)BpaTransactionbpaTransactionType.SelfPayMaxConsecutiveReach) ||
                            (transactionType == (int)BpaTransactionbpaTransactionType.SelfPayInBenefit) ||
                            (transactionType == (int)BpaTransactionbpaTransactionType.SelfPayDraw) ||
                            (transactionType == (int)BpaTransactionbpaTransactionType.Inactive) ||
                            (transactionType == (int)BpaTransactionbpaTransactionType.Frozen) ||
                            (transactionType == (int)BpaTransactionbpaTransactionType.TransferOut) ||
                            (transactionType == (int)BpaTransactionbpaTransactionType.Reciprocal) ||
                            (transactionType == (int)BpaTransactionbpaTransactionType.SecondaryDollarBankDraw))
                        {
                           if (transactionType == (int)BpaTransactionbpaTransactionType.TransferOut)
                            {
                                if (HasReciprocalTransferOUT && HasTrasfer && !IsTransferAmountPaid)
                                {
                                    IsReciprocalTransferOutDelete = true;
                                    RecalculationTransactionHelper.SetTransactionStatus(service, transaction.Id, false);
                                    ToBeDeletedTransactions.Add(transaction.Id);
                                }
                                else if (HasReciprocalTransferOUT && !HasTrasfer && !IsTransferAmountPaid)
                                {
                                    IsReciprocalTransferOutDelete = true;
                                    RecalculationTransactionHelper.SetTransactionStatus(service, transaction.Id, false);
                                    ToBeDeletedTransactions.Add(transaction.Id);
                                }
                                else if (HasReciprocalTransferOUT && HasTrasfer && IsTransferAmountPaid)
                                {
                                    RecalculationTransactionHelper.SetTransactionStatus(service, transaction.Id, false);
                                    ToBeDeletedTransactions.Add(transaction.Id);
                                    IsReciprocalTransferOutDelete = false;
                                }
                                else
                                {
                                    IsReciprocalTransferOutDelete = false;
                                }
                            }
                            else if (transactionType == (int)BpaTransactionbpaTransactionType.Reciprocal)
                            {
                                if (!IsTransferAmountPaid)
                                {
                                    RecalculationTransactionHelper.SetTransactionStatus(service, transaction.Id, false);
                                    ToBeDeletedTransactions.Add(transaction.Id);
                                }
                                else
                                {
                                    Dictionary<string, decimal> endingBalances1 = RecalculationTransactionHelper.FetchEndingBalance(service, selectedContact.Id, Start_WorkMonth, DefaultFund.Id);

                                    //Update Transaction
                                    Entity t = new Entity("bpa_transaction")
                                    {
                                        Id = transaction.Id
                                    };
                                    if (endingBalances1.ContainsKey("hours"))
                                        t["bpa_endinghourbalance"] = endingBalances1["hours"];
                                    if (endingBalances1.ContainsKey("dollarhours"))
                                        t["bpa_endingdollarbalance"] = endingBalances1["dollarhours"];
                                    if (endingBalances1.ContainsKey("selfpaydollar"))
                                        t["bpa_endingselfpaybalance"] = new Money(endingBalances1["selfpaydollar"]);
                                    service.Update(t);
                                }
                            }
                            else
                            {
                                RecalculationTransactionHelper.SetTransactionStatus(service, transaction.Id, false);
                                ToBeDeletedTransactions.Add(transaction.Id);
                            }
                        }
                    }
                    //Trigger Rollup after Retro Processing done
                    PluginHelper.TriggerRollup(service, tracingService, "bpa_memberplan", selectedContact.Id, "bpa_contributiondollarbank");
                    PluginHelper.TriggerRollup(service, tracingService, "bpa_memberplan", selectedContact.Id, "bpa_contributionhourbank");
                    PluginHelper.TriggerRollup(service, tracingService, "bpa_memberplan", selectedContact.Id, "bpa_selfpaybank");

                    #endregion

                    #region ----------------- RECALCULATION LOGIC -----------------------
                    
                    //Check Transfer IN and previous month eligiblity null means it's NEw Member
                    if (HasReciprocalTransferIN && (previousMonthEligibility == null))
                        currentEligibility = (int)MemberPlanBpaCurrentEligibility.NewMember;


                    //Get All Eligibility CATEGORY  Values
                    int eligibilityOffset = 0;
                    bool IsEligiblityHours = false;
                    if (UsingEligibilityCategoty != null)
                    {
                        Entity EligibilityCategoryInformation = EligibilityCategoryHelper.FetchEligibilityCategory(service, UsingEligibilityCategoty.Id);
                        if (EligibilityCategoryInformation.Contains("bpa_planeligibilitytype"))
                            EC_CalculationType = EligibilityCategoryInformation.GetAttributeValue<OptionSetValue>("bpa_planeligibilitytype").Value;

                        if (EligibilityCategoryInformation.Contains("bpa_terminationcycle"))
                            MaxInactiveCounter = EligibilityCategoryInformation.GetAttributeValue<int>("bpa_terminationcycle");

                        if (EligibilityCategoryInformation.Contains("bpa_eligibilityoffsetmonths"))
                            eligibilityOffset = EligibilityCategoryInformation.GetAttributeValue<int>("bpa_eligibilityoffsetmonths");
                    }

                    //Is Eligibility Category Hours or Dollar??
                    if (EC_CalculationType == (int)BpaEligibilityCategoryPlanEligiblityType.Hours)
                        IsEligiblityHours = true;

                    #region -------------- TRANSACTION TYPE = EMPLOYER CONTRIBUTION UPDATE ELIGIBILITY CATEGORY------------

                    if (IsMemberCategoryChanged)
                    {
                        List<Entity> _lsEC = M_transactionsList.Where(x =>
                                    ((x.GetAttributeValue<OptionSetValue>("bpa_transactiontype").Value == (int)BpaTransactionbpaTransactionType.EmployerContribution) ||
                                     (x.GetAttributeValue<OptionSetValue>("bpa_transactiontype").Value == (int)BpaTransactionbpaTransactionType.HourBankAdjustment) ||
                                     (x.GetAttributeValue<OptionSetValue>("bpa_transactiontype").Value == (int)BpaTransactionbpaTransactionType.DollarBankAdjustment)) &&
                                     (x.GetAttributeValue<DateTime>("bpa_transactiondate").ToShortDateString() == Start_WorkMonth.ToShortDateString())).ToList();
                        if (_lsEC != null && _lsEC.Count > 0)
                        {
                            foreach (Entity _e in _lsEC)
                                RecalculationTransactionHelper.UpdateElibilityCategory(service, _e.Id, UsingEligibilityCategoty);
                        }
                    }

                    #endregion

                    if (IsEligiblityHours)
                    {
                        RetroProcessingHourBase(service, tracingService, selectedContact, UsingEligibilityCategoty, currentEligibility, previousMonthEligibility, Start_WorkMonth, DefaultFund, false, 0,
                            HasSelfPayInBenefit, HasECED, lastAgreement, out MemberWentToUnderReview, out SelfPayCounter, out InactiveMonthCounter, out IsMemberBecomeInActive);
                    }
                    else
                    {
                        RetroProcessingDollarBase(service, tracingService, selectedContact, UsingEligibilityCategoty, currentEligibility, previousMonthEligibility, Start_WorkMonth, DefaultFund, false, 0,
                            HasSelfPayInBenefit, HasECED, lastAgreement, out MemberWentToUnderReview, out SelfPayCounter, out InactiveMonthCounter, out IsMemberBecomeInActive);
                    }

                    

                    #endregion
                } //end of if (M_transactions != null && M_transactions.Entities.Count > 0)

                #endregion

                #region -------- ELSE  THERE IS NO RECORDS -----
                else
                {
                    //Get All Eligibility CATEGORY  Values
                    int eligibilityOffset = 0;
                    bool IsEligiblityHours = false;
                    if (UsingEligibilityCategoty != null)
                    {
                        Entity EligibilityCategoryInformation = EligibilityCategoryHelper.FetchEligibilityCategory(service, UsingEligibilityCategoty.Id);
                        if (EligibilityCategoryInformation.Contains("bpa_planeligibilitytype"))
                            EC_CalculationType = EligibilityCategoryInformation.GetAttributeValue<OptionSetValue>("bpa_planeligibilitytype").Value;
                    }


                    //Is Eligibility Category Hours or Dollar??
                    if (EC_CalculationType == (int)BpaEligibilityCategoryPlanEligiblityType.Hours)
                        IsEligiblityHours = true;

                    if (IsEligiblityHours)
                    {
                        RetroProcessingHourBase(service, tracingService, selectedContact, UsingEligibilityCategoty, currentEligibility, previousMonthEligibility, Start_WorkMonth, DefaultFund, false, 0,
                            HasSelfPayInBenefit, HasECED, lastAgreement, out MemberWentToUnderReview, out SelfPayCounter, out InactiveMonthCounter, out IsMemberBecomeInActive);
                    }
                    else
                    {
                        RetroProcessingDollarBase(service, tracingService, selectedContact, UsingEligibilityCategoty, currentEligibility, previousMonthEligibility, Start_WorkMonth, DefaultFund, false, 0,
                            HasSelfPayInBenefit, HasECED, lastAgreement, out MemberWentToUnderReview, out SelfPayCounter, out InactiveMonthCounter, out IsMemberBecomeInActive);
                    }
                    
                } //end of ELSE

                #endregion

                #region ------------ UPDATE SELEPAY COUNTER AND DELETE INACTIVE TRANSACTIONS ------------------

                /*if (SelfPayCounter >= 0)
                {
                    //Update CONTACT SelfPay Count
                    ContactHelper.SetConsecutiveSelfPayMonths(service, selectedContact.Id, SelfPayCounter);
                }*/


                if (IsMemberBecomeInActive)
                {
                    //HAS Future Records
                    if (!RecalculationTransactionHelper.HasFutureTransactions(service, Start_WorkMonth, selectedContact.Id))
                        break;
                    else
                    {
                        //Find next Workmonth where Employer Contribution or Any Adjustment 
                        Start_WorkMonth = RecalculationTransactionHelper.FutureAtivityMonth(service, Start_WorkMonth, selectedContact.Id);
                    }
                    //IsMemberBecomeInActive = false;
                }

                //delete all inactive transactions
                if (ToBeDeletedTransactions.Count > 0)
                {
                    foreach (Guid _transid in ToBeDeletedTransactions)
                    {
                        service.Delete("bpa_transaction", _transid);
                        //Entity _trans = new Entity("bpa_transaction");
                        //_trans.Id = _transid;
                        //_trans["bpa_deleteme"] = true;
                        //service.Update(_trans);
                    }
                }

                #endregion
                
                Start_WorkMonth = Start_WorkMonth.AddMonths(1);
                UsingEligibilityCategoty = null;

                //Trigger Rollup after Retro Processing done
                PluginHelper.TriggerRollup(service, tracingService, PluginHelper.BpaMemberplan, selectedContact.Id, "bpa_contributiondollarbank");
                PluginHelper.TriggerRollup(service, tracingService, PluginHelper.BpaMemberplan, selectedContact.Id, "bpa_contributionhourbank");
                PluginHelper.TriggerRollup(service, tracingService, PluginHelper.BpaMemberplan, selectedContact.Id, "bpa_selfpaybank");
                PluginHelper.TriggerRollup(service, tracingService, PluginHelper.BpaMemberplan, selectedContact.Id, "bpa_secondarydollarbank");

            } //end of while

            DateTime endTime = DateTime.Now;
            double totalmins = endTime.Subtract(startTime).Minutes;
            double totalmins1 = endTime.Subtract(startTime).Seconds;

            return string.Empty;
            //return "MINTUS: " + totalmins.ToString() + " SECOND : " + totalmins1.ToString();

        }

        #region ---- Hour Base ----

        static void RetroProcessingHourBase(IOrganizationService service, ITracingService tracingService, EntityReference selectedContact,
            EntityReference UsingEligibilityCategoty, int currentEligibility, Entity previousMonthEligibility, DateTime startWorkMonth,
            EntityReference defaultFund, bool IsMissMatch, int SubSector_CalculationType, bool HasSelfPayInBenefit, bool HasECED, EntityReference lastAgreement,
            out bool memberWentToUnderReview, out int selfPayCounter, out int InactiveMonthCounter, out bool IsMemberBecomeInActive)
        {
            //GET both hour Bank and Self Pay Bank
            decimal sumdollarhour = RecalculationTransactionHelper.GetSumHours(service, selectedContact.Id, startWorkMonth, defaultFund.Id);
            decimal sumSelpPay = TransactionHelper.FetchSumOfSelfPayBank(service, tracingService, selectedContact.Id, startWorkMonth);
            Money SelfPayBank = new Money(sumSelpPay);
            bool MemberWentToUnderReview = false;
            int SPCounter = 0;
            bool isSelfPayCounterUpdate = false;
            int inactiveMonthCounter = 0;
            bool isMemberBecomeInActive = false;

            //Get All information for Elibiglity Category and Elibility CAtegory Detail
            Entity EligibilityCategory = EligibilityCategoryHelper.FetchEligibilityCategory(service, UsingEligibilityCategoty.Id);
            Entity EligibilityCategoryDetail = EligibilityCategoryHelper.FetchEligibilityDetail(service, EligibilityCategory.Id, startWorkMonth);
            int eligibilityOffset = EligibilityCategory.Contains("bpa_eligibilityoffsetmonths") ? EligibilityCategory.GetAttributeValue<int>("bpa_eligibilityoffsetmonths") : 0;
            int MaxInactiveCounter = EligibilityCategory.Contains("bpa_terminationcycle") ? EligibilityCategory.GetAttributeValue<int>("bpa_terminationcycle") : 0;

            decimal deductionRate = EligibilityCategoryDetail.Contains("bpa_hoursdeductionrate") ? (EligibilityCategoryDetail.GetAttributeValue<decimal>("bpa_hoursdeductionrate") * -1) : 0;
            decimal selfPayDeductionRate = EligibilityCategoryDetail.Contains("bpa_selfpayamount") ? (EligibilityCategoryDetail.GetAttributeValue<Money>("bpa_selfpayamount").Value * -1) : 0;
            decimal miniInitEligibility = EligibilityCategoryDetail.Contains("bpa_hoursminimuminitialeligibility") ? EligibilityCategoryDetail.GetAttributeValue<decimal>("bpa_hoursminimuminitialeligibility") : 0;
            decimal fundAssistanceAmount = EligibilityCategoryDetail.Contains("bpa_hoursfundassistanceamount") ? (EligibilityCategoryDetail.GetAttributeValue<decimal>("bpa_hoursfundassistanceamount") * -1) : 0;

            //Check If Deductuion Rate(Regular Draw) AND Fund Assistance Amount are same then member is Fully Fund Assistance
            if (fundAssistanceAmount == deductionRate)
            {
                #region -------- FUND ASSISTANCE ---------------------
                // rules is prevous month eligibility must be 'IN BENEFIT'
                if ((currentEligibility == (int)MemberPlanBpaCurrentEligibility.InBenefit) || (currentEligibility == (int)MemberPlanBpaCurrentEligibility.SelfPayInBenefit)
                    || (currentEligibility == (int)MemberPlanBpaCurrentEligibility.FundAssisted))
                {
                    
                    RecalculationTransactionHelper.CreateFundAssistedTransactions(service, selectedContact.Id, startWorkMonth, startWorkMonth.AddMonths(eligibilityOffset), null, 
                        (fundAssistanceAmount * -1),  (int)BpaTransactionbpaTransactionCategory.Eligibility, (int)BpaTransactionbpaTransactionType.FundAssisted, defaultFund, 
                        UsingEligibilityCategoty, defaultFund.Id, lastAgreement);

                    RecalculationTransactionHelper.CreateFundAssistedTransactions(service, selectedContact.Id, startWorkMonth, startWorkMonth.AddMonths(eligibilityOffset), null, 
                        fundAssistanceAmount, (int)BpaTransactionbpaTransactionCategory.Eligibility, (int)BpaTransactionbpaTransactionType.EligibilityDraw, defaultFund, 
                        UsingEligibilityCategoty, defaultFund.Id, lastAgreement);

                    RecalculationTransactionHelper.CreateFundAssistedTransactions(service, selectedContact.Id, startWorkMonth, startWorkMonth.AddMonths(eligibilityOffset), null,
                        null, (int)BpaTransactionbpaTransactionCategory.Eligibility, (int)BpaTransactionbpaTransactionType.FundAssistedInBenefit, null, 
                        UsingEligibilityCategoty, defaultFund.Id, lastAgreement);

                    //SET Current Eligibility
                    ContactHelper.SetCurrentEligibility(service, tracingService, selectedContact.Id, (int)MemberPlanBpaCurrentEligibility.FundAssisted, startWorkMonth.AddMonths(eligibilityOffset));

                    //Set SelfPay Counter to 0
                    SPCounter = 0;
                    inactiveMonthCounter = 0;
                    isSelfPayCounterUpdate = true;
                }
                else
                {
                    //Member goes to not in benefit
                    ContactHelper.SetCurrentEligibility(service, tracingService, selectedContact.Id, (int)MemberPlanBpaCurrentEligibility.NotInBenefit, startWorkMonth.AddMonths(eligibilityOffset));

                    RecalculationTransactionHelper.CreateTransaction(service, selectedContact.Id, startWorkMonth,startWorkMonth.AddMonths(eligibilityOffset), null, 
                        (int)BpaTransactionbpaTransactionCategory.Eligibility, (int)BpaTransactionbpaTransactionType.NotInBenefit, 0, true, UsingEligibilityCategoty, 
                        defaultFund.Id, lastAgreement);

                    inactiveMonthCounter = 1;
                }
                #endregion
            }
            else
            {
                if (currentEligibility == (int)MemberPlanBpaCurrentEligibility.NewMember)
                {
                    #region ------------------- NEW MEMBER -----------------------

                    if (IsMissMatch)
                    {
                        RecalculationTransactionHelper.CreateUnderReviewTransaction(service, selectedContact.Id, UsingEligibilityCategoty, startWorkMonth,
                            startWorkMonth.AddMonths(eligibilityOffset), (int)BpaTransactionbpaTransactionCategory.Eligibility,
                            (int)BpaTransactionbpaTransactionType.UnderReview, defaultFund.Id, lastAgreement);

                        ContactHelper.SetCurrentEligibility(service, tracingService, selectedContact.Id,
                            (int)MemberPlanBpaCurrentEligibility.UnderReview, startWorkMonth.AddMonths(eligibilityOffset));

                        MemberWentToUnderReview = true;
                    }
                    else
                    {
                        if (sumdollarhour >= miniInitEligibility)
                        {
                            //if ((SubSector_CalculationType != (int)BpaSubSectorBpaCalculationType.Flat) && (SubSector_CalculationType != (int)BpaSubSectorBpaCalculationType.Corporate))
                            //{
                            RecalculationTransactionHelper.CreateDrawTransaction_Hour(service, selectedContact.Id, startWorkMonth,
                                startWorkMonth.AddMonths(eligibilityOffset), (int)BpaTransactionbpaTransactionCategory.Eligibility,
                                (int)BpaTransactionbpaTransactionType.EligibilityDraw, defaultFund, deductionRate, UsingEligibilityCategoty, lastAgreement);
                            //}

                            ContactHelper.SetCurrentEligibility(service, tracingService, selectedContact.Id,
                                (int)MemberPlanBpaCurrentEligibility.InBenefit, startWorkMonth.AddMonths(eligibilityOffset));

                            RecalculationTransactionHelper.CreateTransaction(service, selectedContact.Id, startWorkMonth,
                                startWorkMonth.AddMonths(eligibilityOffset), null, (int)BpaTransactionbpaTransactionCategory.Eligibility,
                                (int)BpaTransactionbpaTransactionType.InBenefit, 0, true, UsingEligibilityCategoty, defaultFund.Id, lastAgreement);
                            isSelfPayCounterUpdate = true;
                        }
                        else
                        {
                            ContactHelper.SetCurrentEligibility(service, tracingService, selectedContact.Id,
                                (int)MemberPlanBpaCurrentEligibility.NewMember, startWorkMonth.AddMonths(eligibilityOffset));

                            RecalculationTransactionHelper.CreateTransaction(service, selectedContact.Id, startWorkMonth,
                                startWorkMonth.AddMonths(eligibilityOffset), null, (int)BpaTransactionbpaTransactionCategory.Eligibility,
                                (int)BpaTransactionbpaTransactionType.NewMember, 0, true, UsingEligibilityCategoty, defaultFund.Id, lastAgreement);
                        }

                        SPCounter = 0;
                        inactiveMonthCounter = 1;
                        MemberWentToUnderReview = false;
                    }

                    #endregion
                } //end of New Number
                else if ((currentEligibility == (int)MemberPlanBpaCurrentEligibility.InBenefit) || (currentEligibility == (int)MemberPlanBpaCurrentEligibility.SelfPayInBenefit))
                {
                    #region --------------- INBENEFIT & SELFPAY IN BENEIT -----------

                    //Added by jwalin
                    if (IsMissMatch)
                    {
                        #region ------ IS Miss Match ----------
                        ContactHelper.SetCurrentEligibility(service, tracingService, selectedContact.Id,
                            (int)MemberPlanBpaCurrentEligibility.UnderReview, startWorkMonth.AddMonths(eligibilityOffset));

                        RecalculationTransactionHelper.CreateUnderReviewTransaction( service, selectedContact.Id, UsingEligibilityCategoty,startWorkMonth, 
                            startWorkMonth.AddMonths(eligibilityOffset), (int)BpaTransactionbpaTransactionCategory.Eligibility, (int)BpaTransactionbpaTransactionType.UnderReview, 
                            defaultFund.Id, lastAgreement);

                        MemberWentToUnderReview = true;

                        #endregion
                    }
                    else
                    {
                        //Find MaxConsecutiveMonthsSelfPay from Eligibility Category 
                        int MaxConsecutiveMonthsSelfPay = EligibilityCategoryHelper.GetMaxConsecutiveMonthsSelfPay(service, UsingEligibilityCategoty.Id);

                        if (sumdollarhour + deductionRate >= 0)
                        {
                            #region ------------ Dollar Bank and Hour Bank has enough or not? ----------------
                            //if ((SubSector_CalculationType != (int)BpaSubSectorBpaCalculationType.Flat) && (SubSector_CalculationType != (int)BpaSubSectorBpaCalculationType.Corporate))
                            //{
                            RecalculationTransactionHelper.CreateDrawTransaction_Hour(service, selectedContact.Id, startWorkMonth, startWorkMonth.AddMonths(eligibilityOffset),
                                (int)BpaTransactionbpaTransactionCategory.Eligibility, (int)BpaTransactionbpaTransactionType.EligibilityDraw, defaultFund, deductionRate,
                                UsingEligibilityCategoty, lastAgreement);
                            //}

                            if (previousMonthEligibility == null)
                            {
                                if (sumdollarhour >= miniInitEligibility)
                                {
                                    ContactHelper.SetCurrentEligibility(service, tracingService, selectedContact.Id,
                                        (int)MemberPlanBpaCurrentEligibility.InBenefit, startWorkMonth.AddMonths(eligibilityOffset));

                                    RecalculationTransactionHelper.CreateTransaction(service, selectedContact.Id, startWorkMonth, startWorkMonth.AddMonths(eligibilityOffset),
                                        null, (int)BpaTransactionbpaTransactionCategory.Eligibility, (int)BpaTransactionbpaTransactionType.InBenefit,
                                        0, true, UsingEligibilityCategoty, defaultFund.Id, lastAgreement);

                                    isSelfPayCounterUpdate = true;
                                }
                                else
                                {
                                    ContactHelper.SetCurrentEligibility(service, tracingService, selectedContact.Id,
                                        (int)MemberPlanBpaCurrentEligibility.NewMember, startWorkMonth.AddMonths(eligibilityOffset));

                                    RecalculationTransactionHelper.CreateTransaction(service, selectedContact.Id, startWorkMonth, startWorkMonth.AddMonths(eligibilityOffset),
                                        null, (int)BpaTransactionbpaTransactionCategory.Eligibility, (int)BpaTransactionbpaTransactionType.NewMember,
                                        0, true, UsingEligibilityCategoty, defaultFund.Id, lastAgreement);
                                }
                            }
                            else
                            {
                                ContactHelper.SetCurrentEligibility(service, tracingService, selectedContact.Id,
                                    (int)MemberPlanBpaCurrentEligibility.InBenefit, startWorkMonth.AddMonths(eligibilityOffset));

                                RecalculationTransactionHelper.CreateTransaction(service, selectedContact.Id, startWorkMonth, startWorkMonth.AddMonths(eligibilityOffset),
                                    null, (int)BpaTransactionbpaTransactionCategory.Eligibility, (int)BpaTransactionbpaTransactionType.InBenefit,
                                    0, true, UsingEligibilityCategoty, defaultFund.Id, lastAgreement);
                                isSelfPayCounterUpdate = true;
                            }

                            SPCounter = 0;
                            inactiveMonthCounter = 0;
                            isSelfPayCounterUpdate = true;
                            #endregion
                        }
                        else if (((selfPayDeductionRate != 0) && (SelfPayBank.Value >= (selfPayDeductionRate * -1))) && (sumdollarhour >= 0))
                        {
                            #region ------------- Self Pay Bank Checking ------------------

                            //Find out MAx Month of Employer Contribution Contribution (if not found return WorkMonth)
                            DateTime ECMaxDate = TransactionHelper.FetchMaxEmployerContributionDate(service, tracingService, selectedContact.Id, startWorkMonth);

                            //Find out How many Previously SELFPAY
                            int cntSP = 0;
                            Entity lastEC = RecalculationTransactionHelper.FetchLastEligiblityMonthExceptSelfPay(service, tracingService,
                                    ECMaxDate, ECMaxDate, selectedContact.Id);
                            if (lastEC != null && lastEC.Attributes.Contains("bpa_transactiondate"))
                            {
                                DateTime end = startWorkMonth.AddMonths(-1);
                                DateTime start =
                                    lastEC.GetAttributeValue<DateTime>("bpa_transactiondate");
                                cntSP = end.Month + end.Year * 12 - (start.Month + start.Year * 12);
                            }
                            if (cntSP == 0)
                            {
                                cntSP = RecalculationTransactionHelper.GetHowManySelfPay(service, tracingService, ECMaxDate, startWorkMonth, selectedContact.Id);
                            }

                            if (cntSP < MaxConsecutiveMonthsSelfPay)
                            {
                                // Create SelfPay draw and Self pay In Benefit Transaction
                                RecalculationTransactionHelper.CreateTransaction(service, selectedContact.Id, startWorkMonth,startWorkMonth.AddMonths(eligibilityOffset),
                                defaultFund, (int)BpaTransactionbpaTransactionCategory.Eligibility,(int)BpaTransactionbpaTransactionType.SelfPayDraw, selfPayDeductionRate, 
                                false, UsingEligibilityCategoty, defaultFund.Id, lastAgreement);

                                RecalculationTransactionHelper.CreateTransaction(service, selectedContact.Id, startWorkMonth, startWorkMonth.AddMonths(eligibilityOffset),
                                   null, (int)BpaTransactionbpaTransactionCategory.Eligibility,(int)BpaTransactionbpaTransactionType.SelfPayInBenefit, 0, true, 
                                   UsingEligibilityCategoty, defaultFund.Id, lastAgreement);

                                //selfPayCounter = cntSP + 1;
                                SPCounter = cntSP + 1;
                                isSelfPayCounterUpdate = true;
                                // Trigger Self Pay Bank Rollup
                                PluginHelper.TriggerRollup(service, tracingService, "bpa_memberplan", selectedContact.Id, "bpa_selfpaybank");

                                ContactHelper.SetCurrentEligibility(service, tracingService, selectedContact.Id,
                                    (int)MemberPlanBpaCurrentEligibility.SelfPayInBenefit, startWorkMonth.AddMonths(eligibilityOffset));
                                
                            }
                            else
                            {
                                ContactHelper.SetCurrentEligibility(service, tracingService, selectedContact.Id,
                                    (int)MemberPlanBpaCurrentEligibility.NotInBenefit, startWorkMonth.AddMonths(eligibilityOffset));

                                RecalculationTransactionHelper.CreateTransaction(service, selectedContact.Id, startWorkMonth, startWorkMonth.AddMonths(eligibilityOffset),
                                    null, (int)BpaTransactionbpaTransactionCategory.Eligibility, (int)BpaTransactionbpaTransactionType.NotInBenefit,
                                    0, true, UsingEligibilityCategoty, defaultFund.Id, lastAgreement);
                                inactiveMonthCounter = 1;
                            }
                            #endregion
                        }
                        else
                        {
                            #region ---------- Else Part --------
                            if (HasSelfPayInBenefit)
                            {
                            }
                            else
                            {
                                ContactHelper.SetCurrentEligibility(service, tracingService, selectedContact.Id,
                                    (int)MemberPlanBpaCurrentEligibility.NotInBenefit, startWorkMonth.AddMonths(eligibilityOffset));

                                RecalculationTransactionHelper.CreateTransaction(service, selectedContact.Id, startWorkMonth, startWorkMonth.AddMonths(eligibilityOffset),
                                    null, (int)BpaTransactionbpaTransactionCategory.Eligibility, (int)BpaTransactionbpaTransactionType.NotInBenefit, 
                                    0, true, UsingEligibilityCategoty, defaultFund.Id, lastAgreement);

                                inactiveMonthCounter = 1;
                            }
                            #endregion
                        }
                        MemberWentToUnderReview = false;

                    }

                    #endregion
                }
                else if ((currentEligibility == (int)MemberPlanBpaCurrentEligibility.NotInBenefit) || (currentEligibility == (int)MemberPlanBpaCurrentEligibility.Reinstatement))
                {
                    #region  -------------- NOT IN BENEFIT ----------------------------

                    bool IsResinstatement = EligibilityCategoryHelper.IsCategoryReinstatement(service, UsingEligibilityCategoty.Id);
                    decimal reinstatementAmount = 0;

                    if (IsResinstatement)
                    {
                        reinstatementAmount = EligibilityCategoryHelper.GetReinstatementHours(service, UsingEligibilityCategoty.Id, startWorkMonth);

                        if (reinstatementAmount == 0)
                            IsMissMatch = true;
                    }

                    //Added by jwalin
                    if (IsMissMatch)
                    {
                        ContactHelper.SetCurrentEligibility(service, tracingService, selectedContact.Id,
                            (int)MemberPlanBpaCurrentEligibility.UnderReview, startWorkMonth.AddMonths(eligibilityOffset));

                        RecalculationTransactionHelper.CreateUnderReviewTransaction(service, selectedContact.Id, UsingEligibilityCategoty,
                            startWorkMonth, startWorkMonth.AddMonths(eligibilityOffset), (int)BpaTransactionbpaTransactionCategory.Eligibility,
                            (int)BpaTransactionbpaTransactionType.UnderReview, defaultFund.Id, lastAgreement);
                        MemberWentToUnderReview = false;

                    }
                    else
                    {
                        if (IsResinstatement && ((currentEligibility == (int)MemberPlanBpaCurrentEligibility.Reinstatement) || HasECED))
                        {
                            if (sumdollarhour >= reinstatementAmount)
                            {
                                RecalculationTransactionHelper.CreateDrawTransaction_Hour(service, selectedContact.Id, startWorkMonth,
                                    startWorkMonth.AddMonths(eligibilityOffset), (int)BpaTransactionbpaTransactionCategory.Eligibility, 
                                    (int)BpaTransactionbpaTransactionType.EligibilityDraw, defaultFund, deductionRate, UsingEligibilityCategoty, lastAgreement);

                                ContactHelper.SetCurrentEligibility(service, tracingService, selectedContact.Id,
                                    (int)MemberPlanBpaCurrentEligibility.InBenefit, startWorkMonth.AddMonths(eligibilityOffset));

                                RecalculationTransactionHelper.CreateTransaction(service, selectedContact.Id, startWorkMonth, startWorkMonth.AddMonths(eligibilityOffset),
                                    null, (int)BpaTransactionbpaTransactionCategory.Eligibility, (int)BpaTransactionbpaTransactionType.InBenefit,
                                    0, true, UsingEligibilityCategoty, defaultFund.Id, lastAgreement);
                                isSelfPayCounterUpdate = true;
                            }
                            else
                            {
                                RecalculationTransactionHelper.CreateTransaction(service, selectedContact.Id, startWorkMonth, startWorkMonth.AddMonths(eligibilityOffset),
                                    null, (int)BpaTransactionbpaTransactionCategory.Eligibility, (int)BpaTransactionbpaTransactionType.ReInstating,
                                    0, true, UsingEligibilityCategoty, defaultFund.Id, lastAgreement);

                                ContactHelper.SetCurrentEligibility(service, tracingService, selectedContact.Id,
                                    (int)MemberPlanBpaCurrentEligibility.Reinstatement, startWorkMonth.AddMonths(eligibilityOffset));
                            }

                            inactiveMonthCounter = 0;
                        }
                        else
                        {
                            if (sumdollarhour + deductionRate >= 0)
                            {
                                //if ((SubSector_CalculationType != (int)BpaSubSectorBpaCalculationType.Flat) && (SubSector_CalculationType != (int)BpaSubSectorBpaCalculationType.Corporate))
                                //{
                                RecalculationTransactionHelper.CreateDrawTransaction_Hour(service, selectedContact.Id, startWorkMonth,
                                    startWorkMonth.AddMonths(eligibilityOffset), (int)BpaTransactionbpaTransactionCategory.Eligibility,
                                    (int)BpaTransactionbpaTransactionType.EligibilityDraw, defaultFund, deductionRate, UsingEligibilityCategoty, lastAgreement);
                                //}
                                if (previousMonthEligibility == null)
                                {
                                    ContactHelper.SetCurrentEligibility(service, tracingService, selectedContact.Id,
                                        (int)MemberPlanBpaCurrentEligibility.NewMember, startWorkMonth.AddMonths(eligibilityOffset));

                                    RecalculationTransactionHelper.CreateTransaction(service, selectedContact.Id, startWorkMonth, startWorkMonth.AddMonths(eligibilityOffset),
                                        null, (int)BpaTransactionbpaTransactionCategory.Eligibility, (int)BpaTransactionbpaTransactionType.NewMember,
                                        0, true, UsingEligibilityCategoty, defaultFund.Id, lastAgreement);
                                }
                                else
                                {
                                    ContactHelper.SetCurrentEligibility(service, tracingService, selectedContact.Id,
                                        (int)MemberPlanBpaCurrentEligibility.InBenefit, startWorkMonth.AddMonths(eligibilityOffset));

                                    RecalculationTransactionHelper.CreateTransaction(service, selectedContact.Id, startWorkMonth, startWorkMonth.AddMonths(eligibilityOffset),
                                        null, (int)BpaTransactionbpaTransactionCategory.Eligibility, (int)BpaTransactionbpaTransactionType.InBenefit,
                                        0, true, UsingEligibilityCategoty, defaultFund.Id, lastAgreement);

                                    isSelfPayCounterUpdate = true;
                                }
                                inactiveMonthCounter = 0;
                            }
                            else
                            {
                                ContactHelper.SetCurrentEligibility(service, tracingService, selectedContact.Id,
                                    (int)MemberPlanBpaCurrentEligibility.NotInBenefit, startWorkMonth.AddMonths(eligibilityOffset));

                                RecalculationTransactionHelper.CreateTransaction(service, selectedContact.Id, startWorkMonth, startWorkMonth.AddMonths(eligibilityOffset),
                                    null, (int)BpaTransactionbpaTransactionCategory.Eligibility, (int)BpaTransactionbpaTransactionType.NotInBenefit,
                                    0, true, UsingEligibilityCategoty, defaultFund.Id, lastAgreement);
                                inactiveMonthCounter = 1;
                            }
                        }
                        MemberWentToUnderReview = false;

                    }

                    #endregion
                }
                else if (currentEligibility == (int)MemberPlanBpaCurrentEligibility.UnderReview)
                {
                    #region -------------------- UNDER REVIEW --------------------

                    if (previousMonthEligibility == null)
                    {
                        if (sumdollarhour >= miniInitEligibility)
                        {
                            //if (SubSector_CalculationType != (int)BpaSubSectorBpaCalculationType.Flat)
                            //{
                            RecalculationTransactionHelper.CreateDrawTransaction_Hour(service, selectedContact.Id, startWorkMonth,
                                startWorkMonth.AddMonths(eligibilityOffset), (int)BpaTransactionbpaTransactionCategory.Eligibility,
                                (int)BpaTransactionbpaTransactionType.EligibilityDraw, defaultFund, deductionRate, UsingEligibilityCategoty, lastAgreement);
                            //}

                            ContactHelper.SetCurrentEligibility(service, tracingService, selectedContact.Id,
                                (int)MemberPlanBpaCurrentEligibility.InBenefit, startWorkMonth.AddMonths(eligibilityOffset));

                            RecalculationTransactionHelper.CreateTransaction(service, selectedContact.Id, startWorkMonth,
                                startWorkMonth.AddMonths(eligibilityOffset), null, (int)BpaTransactionbpaTransactionCategory.Eligibility,
                                (int)BpaTransactionbpaTransactionType.InBenefit, 0, true, UsingEligibilityCategoty, defaultFund.Id, lastAgreement);
                            isSelfPayCounterUpdate = true;
                        }
                        else
                        {
                            ContactHelper.SetCurrentEligibility(service, tracingService, selectedContact.Id,
                                (int)MemberPlanBpaCurrentEligibility.NewMember, startWorkMonth.AddMonths(eligibilityOffset));

                            RecalculationTransactionHelper.CreateTransaction(service, selectedContact.Id, startWorkMonth,
                                startWorkMonth.AddMonths(eligibilityOffset), null, (int)BpaTransactionbpaTransactionCategory.Eligibility,
                                (int)BpaTransactionbpaTransactionType.NewMember, 0, true, UsingEligibilityCategoty, defaultFund.Id, lastAgreement);
                        }
                        MemberWentToUnderReview = false;
                    }
                    else
                    {
                        ContactHelper.SetCurrentEligibility(service, tracingService, selectedContact.Id,
                            (int)MemberPlanBpaCurrentEligibility.UnderReview, startWorkMonth.AddMonths(eligibilityOffset));

                        RecalculationTransactionHelper.CreateUnderReviewTransaction(service, selectedContact.Id, UsingEligibilityCategoty,
                            startWorkMonth, startWorkMonth.AddMonths(eligibilityOffset), (int)BpaTransactionbpaTransactionCategory.Eligibility,
                            (int)BpaTransactionbpaTransactionType.UnderReview, defaultFund.Id, lastAgreement);

                        MemberWentToUnderReview = true;

                    }
                    inactiveMonthCounter = 0;

                    #endregion
                }
            }
            #region --------------- INACTIVE MONTH COUNTER LOGIC ------------------
            int consecutiveinactivemonths = 0;
            
            if (HasECED)
                inactiveMonthCounter = 0;
            else
            {
                DateTime end = startWorkMonth.AddMonths(-1);
                DateTime lastEligibilityDate = DateTime.MinValue;
                Entity lastEligibility = RecalculationTransactionHelper.FetchLastBenefitMonth(service, startWorkMonth, selectedContact.Id);
                if (lastEligibility != null && lastEligibility.Attributes.Contains("bpa_transactiondate"))
                {
                    lastEligibilityDate = lastEligibility.GetAttributeValue<DateTime>("bpa_transactiondate");
                }

                DateTime lastActivityDate = DateTime.MinValue;
                Entity lastIA = RecalculationTransactionHelper.FetchLastEligiblityContributionMonth(service, startWorkMonth, selectedContact.Id);
                if (lastIA != null && lastIA.Attributes.Contains("bpa_transactiondate"))
                {
                    lastActivityDate = lastIA.GetAttributeValue<DateTime>("bpa_transactiondate");
                }


                if (lastActivityDate == DateTime.MinValue && lastEligibilityDate == DateTime.MinValue) {
                    //Retrive directly from Member plan
                    Entity MemberPlan = service.Retrieve("bpa_memberplan", selectedContact.Id, new Microsoft.Xrm.Sdk.Query.ColumnSet(new string[] { "bpa_consecutiveinactivemonths" }));
                    consecutiveinactivemonths = MemberPlan.Contains("bpa_consecutiveinactivemonths") ? MemberPlan.GetAttributeValue<int>("bpa_consecutiveinactivemonths") : 0;

                }
                else
                {
                    if (lastActivityDate > lastEligibilityDate)
                        consecutiveinactivemonths = end.Month + end.Year * 12 - (lastActivityDate.Month + lastActivityDate.Year * 12);
                    else
                        consecutiveinactivemonths = end.Month + end.Year * 12 - (lastEligibilityDate.Month + lastEligibilityDate.Year * 12);
                }
            }
            #endregion


            #region -------------- RESERVE TRANSAFER  -----------------------------

            if (UsingEligibilityCategoty != null)
            {
                //VERIFY THIS MONTH ALREDY HAS RESERVER TRANSFER OR NOT??
                bool _IsReserveExists = RecalculationTransactionHelper.IsReserveExists(service, selectedContact.Id, startWorkMonth);

                if (_IsReserveExists == false)
                {
                    decimal maxMemberBank = EligibilityCategoryHelper.GetMaximumMemberBank_Hour(service, UsingEligibilityCategoty.Id, startWorkMonth.AddDays(1));
                    decimal currentsumdollarhour = RecalculationTransactionHelper.GetSumHours(service, selectedContact.Id, startWorkMonth, defaultFund.Id);
                    decimal totalEmployerContribution = RecalculationTransactionHelper.GetSumOfHours(service, selectedContact.Id, startWorkMonth,
                            (int)BpaTransactionbpaTransactionType.EmployerContribution);
                    decimal totalEligibilityDraw = RecalculationTransactionHelper.GetSumOfHours(service, selectedContact.Id, startWorkMonth, 
                        (int)BpaTransactionbpaTransactionType.EligibilityDraw);
                    decimal totalReserveTransfer = RecalculationTransactionHelper.GetSumOfHours(service, selectedContact.Id, startWorkMonth,
                            (int)BpaTransactionbpaTransactionType.ReserveTransfer);

                    //We remove this [(maxMemberBank != 0) &&] condition because of FLAT - DOLLAR Combination 
                    if (currentsumdollarhour >= maxMemberBank)
                    {
                        decimal reserveTransferAmount = maxMemberBank - (totalEmployerContribution + totalEligibilityDraw + totalReserveTransfer);
                        //TFS 107, TFS 108, TFS 109
                        if ((reserveTransferAmount < 0) && (MemberWentToUnderReview == false) && (currentEligibility != (int)MemberPlanBpaCurrentEligibility.Frozen))
                        {
                            RecalculationTransactionHelper.CreatReserveTransfer_Hour(service, selectedContact.Id, startWorkMonth, startWorkMonth.AddMonths(eligibilityOffset),
                                reserveTransferAmount, defaultFund.Id);

                            //Get the Standing Transaction(like Not In Benefite, In Benefit .. )for that same month 
                            RecalculationTransactionHelper.UpdateEndingBalanace(service, startWorkMonth, selectedContact.Id, defaultFund.Id);
                        }
                    }
                    
                    
                }
            }

            #endregion
            //Set Out VARIABLES
            memberWentToUnderReview = MemberWentToUnderReview;
            selfPayCounter = SPCounter;
            InactiveMonthCounter = inactiveMonthCounter + consecutiveinactivemonths;

            #region ------ CRATE Inactive and Reserve Transfer Transaction --------

            ////CREATE 'INACTIVE' ELIGIBILITY TRANSACTION
            ////CREATE 'RESERVE TRANSFER ' TRANSACTION
            //// ALSO CHECK IS THERE ANY EXTRA TRANSACTION IN FUTURE DELETE IT.
            if (InactiveMonthCounter >= 0)
            {
                bool IsOverMax = false;
                if (InactiveMonthCounter > MaxInactiveCounter)
                {
                    InactiveMonthCounter = MaxInactiveCounter;
                    IsOverMax = true;
                }
                if (InactiveMonthCounter <= MaxInactiveCounter)
                {
                    ContactHelper.UpdateInactiveMonthsCounter(service, tracingService, selectedContact.Id, InactiveMonthCounter);
                    if (InactiveMonthCounter > 0)
                    {
                        if (InactiveMonthCounter == MaxInactiveCounter)
                        {
                            EntityCollection _InactiveNReserveTransactions = RecalculationTransactionHelper.AlreadyHasInactiveAndReserveTransactions(service, selectedContact.Id, startWorkMonth.AddMonths(1));

                            if (_InactiveNReserveTransactions != null && _InactiveNReserveTransactions.Entities.Count > 0)
                            {
                                foreach (Entity _t in _InactiveNReserveTransactions.Entities)
                                {
                                    RecalculationTransactionHelper.DeleteTransaction(service, _t.Id);
                                }
                            }
                            ////CREATE 'INACTIVE' ELIGIBILITY TRANSACTION
                            ////CREATE 'RESERVE TRANSFER ' TRANSACTION
                            if (IsOverMax)
                            {
                                RecalculationTransactionHelper.CreateTransaction(service, selectedContact.Id, startWorkMonth,
                                    startWorkMonth.AddMonths(eligibilityOffset), string.Empty, (int)BpaTransactionbpaTransactionCategory.Eligibility,
                                    (int)BpaTransactionbpaTransactionType.Inactive, 0, true, UsingEligibilityCategoty, true, defaultFund.Id, lastAgreement);

                                //Delete Transaction for that month
                                RecalculationTransactionHelper.DeleteTransactionNotInBenefit (service, selectedContact.Id, startWorkMonth);
                            }
                            else
                            {
                                RecalculationTransactionHelper.CreateTransaction(service, selectedContact.Id, startWorkMonth.AddMonths(1),
                                    startWorkMonth.AddMonths(eligibilityOffset + 1), string.Empty, (int)BpaTransactionbpaTransactionCategory.Eligibility,
                                    (int)BpaTransactionbpaTransactionType.Inactive, 0, true, UsingEligibilityCategoty, true, defaultFund.Id, lastAgreement);
                            }

                            //UPDATE CONTACT 'CURRENT ELIGIBILITY' TO 'INACTIVE'
                            ContactHelper.SetCurrentEligibility(service, tracingService, selectedContact.Id,
                                (int)MemberPlanBpaCurrentEligibility.Inactive, startWorkMonth.AddMonths(eligibilityOffset));

                            isMemberBecomeInActive = true;
                        }
                    }
                }
            }

            #endregion

            
            //if(isSelfPayCounterUpdate)
            //    ContactHelper.SetConsecutiveSelfPayMonths(service, tracingService, selectedContact.Id, SPCounter);

            //UPDAET CONSECUTIVE BOTH MONTHS
            ContactHelper.UpdateBothConsecutiveMonths(service, selectedContact.Id, SPCounter, InactiveMonthCounter, isSelfPayCounterUpdate);

            //Set Out VARIABLE
            IsMemberBecomeInActive = isMemberBecomeInActive;
        }


        #endregion


        #region ---- DOLLAR BASE -----


        static void RetroProcessingDollarBase(IOrganizationService service, ITracingService tracingService, EntityReference selectedContact,
            EntityReference UsingEligibilityCategoty, int currentEligibility, Entity previousMonthEligibility, DateTime startWorkMonth,
            EntityReference defaultFund, bool IsMissMatch, int SubSector_CalculationType, bool HasSelfPayInBenefit, bool HasECED, EntityReference lastAgreement,
            out bool memberWentToUnderReview, out int selfPayCounter, out int InactiveMonthCounter, out bool IsMemberBecomeInActive)
        {
            //GET both Dollar Bank and Self Pay Bank
            decimal sumdollarhour = RecalculationTransactionHelper.GetSumDollarHour(service, selectedContact.Id, startWorkMonth, defaultFund.Id);
            decimal sumSelpPay = TransactionHelper.FetchSumOfSelfPayBank(service, tracingService, selectedContact.Id, startWorkMonth);
            Money SelfPayBank = new Money(sumSelpPay);

            decimal sumSecondaryBank = TransactionHelper.GetSumSecondaryBank(service, tracingService, selectedContact.Id, startWorkMonth);


            //Declare variable
            bool MemberWentToUnderReview = false;
            int SPCounter = 0;
            bool isSelfPayCounterUpdate = false;
            int inactiveMonthCounter = 0;
            bool isMemberBecomeInActive = false;

            //Get All information for Elibiglity Category and Elibility CAtegory Detail
            Entity EligibilityCategory = EligibilityCategoryHelper.FetchEligibilityCategory(service, UsingEligibilityCategoty.Id);
            Entity EligibilityCategoryDetail = EligibilityCategoryHelper.FetchEligibilityDetail(service, EligibilityCategory.Id, startWorkMonth);

            int eligibilityOffset = EligibilityCategory.Contains("bpa_eligibilityoffsetmonths") ? EligibilityCategory.GetAttributeValue<int>("bpa_eligibilityoffsetmonths") : 0;
            int MaxInactiveCounter = EligibilityCategory.Contains("bpa_terminationcycle") ? EligibilityCategory.GetAttributeValue<int>("bpa_terminationcycle") : 0;
            decimal deductionRate = EligibilityCategoryDetail.Contains("bpa_deductionrate") ? (EligibilityCategoryDetail.GetAttributeValue<Money>("bpa_deductionrate").Value * -1) : 0;
            decimal selfPayDeductionRate = EligibilityCategoryDetail.Contains("bpa_selfpayamount") ? (EligibilityCategoryDetail.GetAttributeValue<Money>("bpa_selfpayamount").Value * -1) : 0;
            decimal miniInitEligibility = EligibilityCategoryDetail.Contains("bpa_minimumeligibility") ? EligibilityCategoryDetail.GetAttributeValue<Money>("bpa_minimumeligibility").Value : 0;
            decimal fundAssistanceAmount = EligibilityCategoryDetail.Contains("bpa_fundassistanceamount") ? (EligibilityCategoryDetail.GetAttributeValue<Money>("bpa_fundassistanceamount").Value) : 0;

            //Check If Deductuion Rate(Regular Draw) AND Fund Assistance Amount are same then member is Fully Fund Assistance
            if ((fundAssistanceAmount * -1) == deductionRate)
            {
                #region -------- FULLY FUND ASSISTANCE ---------------------
                // rules is prevous month eligibility must be 'IN BENEFIT'
                if ((currentEligibility == (int)MemberPlanBpaCurrentEligibility.InBenefit) || (currentEligibility == (int)MemberPlanBpaCurrentEligibility.SelfPayInBenefit)
                    || (currentEligibility == (int)MemberPlanBpaCurrentEligibility.FundAssisted))
                {

                    RecalculationTransactionHelper.CreateFundAssistedTransactions(service, selectedContact.Id, startWorkMonth, startWorkMonth.AddMonths(eligibilityOffset), 
                        new Money(fundAssistanceAmount * -1), null, (int)BpaTransactionbpaTransactionCategory.Eligibility, (int)BpaTransactionbpaTransactionType.FundAssisted, 
                        defaultFund, UsingEligibilityCategoty, defaultFund.Id, lastAgreement);

                    RecalculationTransactionHelper.CreateFundAssistedTransactions(service, selectedContact.Id, startWorkMonth, startWorkMonth.AddMonths(eligibilityOffset), 
                        new Money(fundAssistanceAmount), null, (int)BpaTransactionbpaTransactionCategory.Eligibility, (int)BpaTransactionbpaTransactionType.EligibilityDraw, 
                        defaultFund, UsingEligibilityCategoty, defaultFund.Id, lastAgreement);

                    RecalculationTransactionHelper.CreateFundAssistedTransactions(service, selectedContact.Id, startWorkMonth, startWorkMonth.AddMonths(eligibilityOffset), 
                        null, null, (int)BpaTransactionbpaTransactionCategory.Eligibility, (int)BpaTransactionbpaTransactionType.FundAssistedInBenefit, 
                        null, UsingEligibilityCategoty, defaultFund.Id, lastAgreement);

                    //SET Current Eligibility
                    ContactHelper.SetCurrentEligibility(service, tracingService, selectedContact.Id, (int)MemberPlanBpaCurrentEligibility.FundAssisted, startWorkMonth.AddMonths(eligibilityOffset));

                    //Set SelfPay Counter to 0
                    SPCounter = 0;
                    inactiveMonthCounter = 0;
                }
                else
                {
                    ContactHelper.SetCurrentEligibility(service, tracingService, selectedContact.Id, (int)MemberPlanBpaCurrentEligibility.NotInBenefit, startWorkMonth.AddMonths(eligibilityOffset));

                    RecalculationTransactionHelper.CreateTransaction(service, selectedContact.Id, startWorkMonth, startWorkMonth.AddMonths(eligibilityOffset), null,
                        (int)BpaTransactionbpaTransactionCategory.Eligibility, (int)BpaTransactionbpaTransactionType.NotInBenefit, 0, true, UsingEligibilityCategoty,
                        defaultFund.Id, lastAgreement);
                    inactiveMonthCounter = 1;
                }
                #endregion
            }
            else if (fundAssistanceAmount == 0)
            {
                #region ----- REGULAR DOLLAR LOGIC -------
                if (currentEligibility == (int)MemberPlanBpaCurrentEligibility.NewMember)
                {
                    #region ------------------- NEW MEMBER -----------------------

                    if (IsMissMatch)
                    {
                        RecalculationTransactionHelper.CreateUnderReviewTransaction(service, selectedContact.Id, UsingEligibilityCategoty, startWorkMonth,
                            startWorkMonth.AddMonths(eligibilityOffset), (int)BpaTransactionbpaTransactionCategory.Eligibility,
                            (int)BpaTransactionbpaTransactionType.UnderReview, defaultFund.Id, lastAgreement);

                        ContactHelper.SetCurrentEligibility(service, tracingService, selectedContact.Id,
                            (int)MemberPlanBpaCurrentEligibility.UnderReview, startWorkMonth.AddMonths(eligibilityOffset));

                        MemberWentToUnderReview = true;
                    }
                    else
                    {
                        if (sumdollarhour >= miniInitEligibility)
                        {
                            RecalculationTransactionHelper.CreateTransaction(service, selectedContact.Id, startWorkMonth, startWorkMonth.AddMonths(eligibilityOffset), defaultFund,
                                (int)BpaTransactionbpaTransactionCategory.Eligibility, (int)BpaTransactionbpaTransactionType.EligibilityDraw, deductionRate,
                                false, UsingEligibilityCategoty, defaultFund.Id, lastAgreement);

                            ContactHelper.SetCurrentEligibility(service, tracingService, selectedContact.Id,
                                (int)MemberPlanBpaCurrentEligibility.InBenefit, startWorkMonth.AddMonths(eligibilityOffset));

                            RecalculationTransactionHelper.CreateTransaction(service, selectedContact.Id, startWorkMonth,
                                startWorkMonth.AddMonths(eligibilityOffset), null, (int)BpaTransactionbpaTransactionCategory.Eligibility,
                                (int)BpaTransactionbpaTransactionType.InBenefit, 0, true, UsingEligibilityCategoty, defaultFund.Id, lastAgreement);

                            isSelfPayCounterUpdate = true;
                        }
                        else if (sumSecondaryBank >= miniInitEligibility)
                        {
                            RecalculationTransactionHelper.CreateTransaction(service, selectedContact.Id, startWorkMonth, startWorkMonth.AddMonths(eligibilityOffset), 
                                defaultFund, (int)BpaTransactionbpaTransactionCategory.Eligibility, (int)BpaTransactionbpaTransactionType.SecondaryDollarBankDraw, deductionRate,
                               false, UsingEligibilityCategoty, defaultFund.Id, lastAgreement);

                            ContactHelper.SetCurrentEligibility(service, tracingService, selectedContact.Id,
                                (int)MemberPlanBpaCurrentEligibility.InBenefit, startWorkMonth.AddMonths(eligibilityOffset));

                            RecalculationTransactionHelper.CreateTransaction(service, selectedContact.Id, startWorkMonth,
                                startWorkMonth.AddMonths(eligibilityOffset), null, (int)BpaTransactionbpaTransactionCategory.Eligibility,
                                (int)BpaTransactionbpaTransactionType.InBenefit, 0, true, UsingEligibilityCategoty, defaultFund.Id, lastAgreement);

                            isSelfPayCounterUpdate = true;
                        }
                        else
                        {
                            ContactHelper.SetCurrentEligibility(service, tracingService, selectedContact.Id,
                                (int)MemberPlanBpaCurrentEligibility.NewMember, startWorkMonth.AddMonths(eligibilityOffset));

                            RecalculationTransactionHelper.CreateTransaction(service, selectedContact.Id, startWorkMonth,
                                startWorkMonth.AddMonths(eligibilityOffset), null, (int)BpaTransactionbpaTransactionCategory.Eligibility,
                                (int)BpaTransactionbpaTransactionType.NewMember, 0, true, UsingEligibilityCategoty, defaultFund.Id, lastAgreement);
                        }

                        SPCounter = 0;
                        inactiveMonthCounter = 1;
                        MemberWentToUnderReview = false;
                    }

                    #endregion
                } //end of New Number
                else if ((currentEligibility == (int)MemberPlanBpaCurrentEligibility.InBenefit) || (currentEligibility == (int)MemberPlanBpaCurrentEligibility.SelfPayInBenefit))
                {
                    #region --------------- INBENEFIT & SELFPAY IN BENEIT -----------

                    //Added by jwalin
                    if (IsMissMatch)
                    {
                        #region ------ IS Miss Match ----------
                        ContactHelper.SetCurrentEligibility(service, tracingService, selectedContact.Id,
                            (int)MemberPlanBpaCurrentEligibility.UnderReview, startWorkMonth.AddMonths(eligibilityOffset));

                        RecalculationTransactionHelper.CreateUnderReviewTransaction(service, selectedContact.Id, UsingEligibilityCategoty, startWorkMonth, 
                            startWorkMonth.AddMonths(eligibilityOffset), (int)BpaTransactionbpaTransactionCategory.Eligibility, 
                            (int)BpaTransactionbpaTransactionType.UnderReview, defaultFund.Id, lastAgreement);

                        MemberWentToUnderReview = true;

                        #endregion
                    }
                    else
                    {
                        //Find MaxConsecutiveMonthsSelfPay from Eligibility Category 
                        int MaxConsecutiveMonthsSelfPay = EligibilityCategoryHelper.GetMaxConsecutiveMonthsSelfPay(service, UsingEligibilityCategoty.Id);

                        if (sumdollarhour + deductionRate >= 0)
                        {
                            #region ------------ Dollar Bank and Hour Bank has enough or not? ----------------
                            RecalculationTransactionHelper.CreateTransaction(service, selectedContact.Id, startWorkMonth, startWorkMonth.AddMonths(eligibilityOffset), defaultFund, 
                                (int)BpaTransactionbpaTransactionCategory.Eligibility, (int)BpaTransactionbpaTransactionType.EligibilityDraw, deductionRate, 
                                false, UsingEligibilityCategoty, defaultFund.Id, lastAgreement);

                            if (previousMonthEligibility == null)
                            {
                                if (sumdollarhour >= miniInitEligibility)
                                {
                                    ContactHelper.SetCurrentEligibility(service, tracingService, selectedContact.Id,
                                        (int)MemberPlanBpaCurrentEligibility.InBenefit, startWorkMonth.AddMonths(eligibilityOffset));

                                    RecalculationTransactionHelper.CreateTransaction(service, selectedContact.Id, startWorkMonth, startWorkMonth.AddMonths(eligibilityOffset),
                                        null, (int)BpaTransactionbpaTransactionCategory.Eligibility, (int)BpaTransactionbpaTransactionType.InBenefit,
                                        0, true, UsingEligibilityCategoty, defaultFund.Id, lastAgreement);
                                    isSelfPayCounterUpdate = true;
                                }
                                else
                                {
                                    ContactHelper.SetCurrentEligibility(service, tracingService, selectedContact.Id,
                                        (int)MemberPlanBpaCurrentEligibility.NewMember, startWorkMonth.AddMonths(eligibilityOffset));

                                    RecalculationTransactionHelper.CreateTransaction(service, selectedContact.Id, startWorkMonth, startWorkMonth.AddMonths(eligibilityOffset),
                                        null, (int)BpaTransactionbpaTransactionCategory.Eligibility, (int)BpaTransactionbpaTransactionType.NewMember,
                                        0, true, UsingEligibilityCategoty, defaultFund.Id, lastAgreement);
                                }
                            }
                            else
                            {
                                ContactHelper.SetCurrentEligibility(service, tracingService, selectedContact.Id,
                                    (int)MemberPlanBpaCurrentEligibility.InBenefit, startWorkMonth.AddMonths(eligibilityOffset));

                                RecalculationTransactionHelper.CreateTransaction(service, selectedContact.Id, startWorkMonth, startWorkMonth.AddMonths(eligibilityOffset),
                                    null, (int)BpaTransactionbpaTransactionCategory.Eligibility, (int)BpaTransactionbpaTransactionType.InBenefit,
                                    0, true, UsingEligibilityCategoty, defaultFund.Id, lastAgreement);
                                isSelfPayCounterUpdate = true;
                            }

                            SPCounter = 0;
                            inactiveMonthCounter = 0;
                            isSelfPayCounterUpdate = true;
                            #endregion
                        }
                        else if(sumSecondaryBank + deductionRate >= 0)
                        {
                            #region ------------- SECONDARY DOLLAR BANK DRAW ---------------
                            RecalculationTransactionHelper.CreateTransaction(service, selectedContact.Id, startWorkMonth, startWorkMonth.AddMonths(eligibilityOffset), 
                                defaultFund, (int)BpaTransactionbpaTransactionCategory.Eligibility, (int)BpaTransactionbpaTransactionType.SecondaryDollarBankDraw,
                                deductionRate, false, UsingEligibilityCategoty, defaultFund.Id, lastAgreement);

                            if (previousMonthEligibility == null)
                            {
                                if (sumSecondaryBank >= miniInitEligibility)
                                {
                                    ContactHelper.SetCurrentEligibility(service, tracingService, selectedContact.Id,
                                        (int)MemberPlanBpaCurrentEligibility.InBenefit, startWorkMonth.AddMonths(eligibilityOffset));

                                    RecalculationTransactionHelper.CreateTransaction(service, selectedContact.Id, startWorkMonth, startWorkMonth.AddMonths(eligibilityOffset),
                                        null, (int)BpaTransactionbpaTransactionCategory.Eligibility, (int)BpaTransactionbpaTransactionType.InBenefit,
                                        0, true, UsingEligibilityCategoty, defaultFund.Id, lastAgreement);
                                    isSelfPayCounterUpdate = true;
                                }
                                else
                                {
                                    ContactHelper.SetCurrentEligibility(service, tracingService, selectedContact.Id,
                                        (int)MemberPlanBpaCurrentEligibility.NewMember, startWorkMonth.AddMonths(eligibilityOffset));

                                    RecalculationTransactionHelper.CreateTransaction(service, selectedContact.Id, startWorkMonth, startWorkMonth.AddMonths(eligibilityOffset),
                                        null, (int)BpaTransactionbpaTransactionCategory.Eligibility, (int)BpaTransactionbpaTransactionType.NewMember,
                                        0, true, UsingEligibilityCategoty, defaultFund.Id, lastAgreement);
                                }
                            }
                            else
                            {
                                ContactHelper.SetCurrentEligibility(service, tracingService, selectedContact.Id,
                                    (int)MemberPlanBpaCurrentEligibility.InBenefit, startWorkMonth.AddMonths(eligibilityOffset));

                                RecalculationTransactionHelper.CreateTransaction(service, selectedContact.Id, startWorkMonth, startWorkMonth.AddMonths(eligibilityOffset),
                                    null, (int)BpaTransactionbpaTransactionCategory.Eligibility, (int)BpaTransactionbpaTransactionType.InBenefit,
                                    0, true, UsingEligibilityCategoty, defaultFund.Id, lastAgreement);
                                isSelfPayCounterUpdate = true;
                            }

                            SPCounter = 0;
                            inactiveMonthCounter = 0;
                            isSelfPayCounterUpdate = true;

                            #endregion
                        }
                        else if (((selfPayDeductionRate != 0) && (SelfPayBank.Value >= (selfPayDeductionRate * -1))) && (sumdollarhour >= 0))
                        {
                            #region ------------- Self Pay Bank Checking ------------------

                            //Find out MAx Month of Employer Contribution Contribution (if not found return WorkMonth)
                            DateTime ECMaxDate = TransactionHelper.FetchMaxEmployerContributionDate(service, tracingService, selectedContact.Id, startWorkMonth);

                            //Find out How many Previously SELFPAY
                            int cntSP = 0;
                            Entity lastEC = RecalculationTransactionHelper.FetchLastEligiblityMonthExceptSelfPay(service, tracingService,
                                    ECMaxDate, ECMaxDate, selectedContact.Id);
                            if (lastEC != null && lastEC.Attributes.Contains("bpa_transactiondate"))
                            {
                                DateTime end = startWorkMonth.AddMonths(-1);
                                DateTime start =
                                    lastEC.GetAttributeValue<DateTime>("bpa_transactiondate");
                                cntSP = end.Month + end.Year * 12 - (start.Month + start.Year * 12);
                            }
                            if (cntSP == 0)
                            {
                                cntSP = RecalculationTransactionHelper.GetHowManySelfPay(service, tracingService, ECMaxDate, startWorkMonth, selectedContact.Id);
                            }

                            if (cntSP < MaxConsecutiveMonthsSelfPay)
                            {
                                // Create SelfPay draw and Self pay In Benefit Transaction
                                RecalculationTransactionHelper.CreateTransaction(service, selectedContact.Id, startWorkMonth, startWorkMonth.AddMonths(eligibilityOffset),
                                    defaultFund, (int)BpaTransactionbpaTransactionCategory.Eligibility, (int)BpaTransactionbpaTransactionType.SelfPayDraw, selfPayDeductionRate,
                                    false, UsingEligibilityCategoty, defaultFund.Id, lastAgreement);

                                RecalculationTransactionHelper.CreateTransaction(service, selectedContact.Id, startWorkMonth, startWorkMonth.AddMonths(eligibilityOffset),
                                   null, (int)BpaTransactionbpaTransactionCategory.Eligibility, (int)BpaTransactionbpaTransactionType.SelfPayInBenefit, 0, true, 
                                   UsingEligibilityCategoty, defaultFund.Id, lastAgreement);

                                //selfPayCounter = cntSP + 1;
                                SPCounter = cntSP + 1;

                                // Trigger Self Pay Bank Rollup
                                PluginHelper.TriggerRollup(service, tracingService, "bpa_memberplan", selectedContact.Id, "bpa_selfpaybank");

                                ContactHelper.SetCurrentEligibility(service, tracingService, selectedContact.Id,
                                    (int)MemberPlanBpaCurrentEligibility.SelfPayInBenefit, startWorkMonth.AddMonths(eligibilityOffset));

                                isSelfPayCounterUpdate = true;
                            }
                            else
                            {
                                ContactHelper.SetCurrentEligibility(service, tracingService, selectedContact.Id, (int)MemberPlanBpaCurrentEligibility.NotInBenefit, startWorkMonth.AddMonths(eligibilityOffset));

                                RecalculationTransactionHelper.CreateTransaction(service, selectedContact.Id, startWorkMonth, startWorkMonth.AddMonths(eligibilityOffset), null,
                                    (int)BpaTransactionbpaTransactionCategory.Eligibility, (int)BpaTransactionbpaTransactionType.NotInBenefit, 0, true, UsingEligibilityCategoty, 
                                    defaultFund.Id, lastAgreement);
                                inactiveMonthCounter = 1;
                            }
                            #endregion
                        }
                        else
                        {
                            #region ---------- Else Part --------
                            if (HasSelfPayInBenefit)
                            { }
                            else
                            {
                                ContactHelper.SetCurrentEligibility(service, tracingService, selectedContact.Id,
                                    (int)MemberPlanBpaCurrentEligibility.NotInBenefit, startWorkMonth.AddMonths(eligibilityOffset));

                                RecalculationTransactionHelper.CreateTransaction(service, selectedContact.Id, startWorkMonth, startWorkMonth.AddMonths(eligibilityOffset),
                                    null, (int)BpaTransactionbpaTransactionCategory.Eligibility, (int)BpaTransactionbpaTransactionType.NotInBenefit, 0, true, 
                                    UsingEligibilityCategoty, defaultFund.Id, lastAgreement);

                                inactiveMonthCounter = 1;
                            }
                            #endregion
                        }
                        MemberWentToUnderReview = false;

                    }

                    #endregion
                }
                else if ((currentEligibility == (int)MemberPlanBpaCurrentEligibility.NotInBenefit) || (currentEligibility == (int)MemberPlanBpaCurrentEligibility.Reinstatement))
                {
                    #region  -------------- NOT IN BENEFIT ----------------------------

                    bool IsResinstatement = EligibilityCategoryHelper.IsCategoryReinstatement(service, UsingEligibilityCategoty.Id);
                    decimal reinstatementAmount = 0;

                    if (IsResinstatement)
                    {
                        reinstatementAmount = EligibilityCategoryHelper.GetReinstatementDollars(service, UsingEligibilityCategoty.Id, startWorkMonth);

                        if (reinstatementAmount == 0)
                            IsMissMatch = true;
                    }

                    //Added by jwalin
                    if (IsMissMatch)
                    {
                        ContactHelper.SetCurrentEligibility(service, tracingService, selectedContact.Id, (int)MemberPlanBpaCurrentEligibility.UnderReview, startWorkMonth.AddMonths(eligibilityOffset));

                        RecalculationTransactionHelper.CreateUnderReviewTransaction(service, selectedContact.Id, UsingEligibilityCategoty, startWorkMonth, 
                            startWorkMonth.AddMonths(eligibilityOffset), (int)BpaTransactionbpaTransactionCategory.Eligibility, (int)BpaTransactionbpaTransactionType.UnderReview, 
                            defaultFund.Id, lastAgreement);

                        MemberWentToUnderReview = false;
                    }
                    else
                    {
                        if (IsResinstatement && ((currentEligibility == (int)MemberPlanBpaCurrentEligibility.Reinstatement) || HasECED))
                        {
                            if (sumdollarhour >= reinstatementAmount)
                            {
                                RecalculationTransactionHelper.CreateTransaction(service, selectedContact.Id, startWorkMonth, startWorkMonth.AddMonths(eligibilityOffset), defaultFund, 
                                    (int)BpaTransactionbpaTransactionCategory.Eligibility, (int)BpaTransactionbpaTransactionType.EligibilityDraw, deductionRate, false, 
                                    UsingEligibilityCategoty, defaultFund.Id, lastAgreement);
                                
                                ContactHelper.SetCurrentEligibility(service, tracingService, selectedContact.Id,(int)MemberPlanBpaCurrentEligibility.InBenefit, startWorkMonth.AddMonths(eligibilityOffset));

                                RecalculationTransactionHelper.CreateTransaction(service, selectedContact.Id, startWorkMonth, startWorkMonth.AddMonths(eligibilityOffset),
                                    null, (int)BpaTransactionbpaTransactionCategory.Eligibility, (int)BpaTransactionbpaTransactionType.InBenefit, 0, true, UsingEligibilityCategoty, 
                                    defaultFund.Id, lastAgreement);
                                isSelfPayCounterUpdate = true;
                            }
                            else
                            {
                                RecalculationTransactionHelper.CreateTransaction(service, selectedContact.Id, startWorkMonth, startWorkMonth.AddMonths(eligibilityOffset), null, 
                                    (int)BpaTransactionbpaTransactionCategory.Eligibility, (int)BpaTransactionbpaTransactionType.ReInstating, 0, true, UsingEligibilityCategoty,
                                    defaultFund.Id, lastAgreement);

                                ContactHelper.SetCurrentEligibility(service, tracingService, selectedContact.Id, (int)MemberPlanBpaCurrentEligibility.Reinstatement, startWorkMonth.AddMonths(eligibilityOffset));
                            }

                            inactiveMonthCounter = 0;
                        }
                        else
                        {
                            if (sumdollarhour + deductionRate >= 0)
                            {
                                RecalculationTransactionHelper.CreateTransaction(service, selectedContact.Id, startWorkMonth, startWorkMonth.AddMonths(eligibilityOffset), defaultFund, 
                                    (int)BpaTransactionbpaTransactionCategory.Eligibility, (int)BpaTransactionbpaTransactionType.EligibilityDraw, deductionRate, false, UsingEligibilityCategoty, 
                                    defaultFund.Id, lastAgreement);

                                if (previousMonthEligibility == null)
                                {
                                    ContactHelper.SetCurrentEligibility(service, tracingService, selectedContact.Id,(int)MemberPlanBpaCurrentEligibility.NewMember, startWorkMonth.AddMonths(eligibilityOffset));

                                    RecalculationTransactionHelper.CreateTransaction(service, selectedContact.Id, startWorkMonth, startWorkMonth.AddMonths(eligibilityOffset),null, 
                                        (int)BpaTransactionbpaTransactionCategory.Eligibility, (int)BpaTransactionbpaTransactionType.NewMember,0, true, UsingEligibilityCategoty, 
                                        defaultFund.Id, lastAgreement);
                                }
                                else
                                {
                                    ContactHelper.SetCurrentEligibility(service, tracingService, selectedContact.Id,(int)MemberPlanBpaCurrentEligibility.InBenefit, startWorkMonth.AddMonths(eligibilityOffset));

                                    RecalculationTransactionHelper.CreateTransaction(service, selectedContact.Id, startWorkMonth, startWorkMonth.AddMonths(eligibilityOffset),null, 
                                        (int)BpaTransactionbpaTransactionCategory.Eligibility, (int)BpaTransactionbpaTransactionType.InBenefit,0, true, UsingEligibilityCategoty,
                                        defaultFund.Id, lastAgreement);
                                    isSelfPayCounterUpdate = true;
                                }
                                inactiveMonthCounter = 0;
                            }
                            else if (sumSecondaryBank + deductionRate >= 0)
                            {
                                #region ------------- SECONDARY DOLLAR BANK DRAW ---------------
                                RecalculationTransactionHelper.CreateTransaction(service, selectedContact.Id, startWorkMonth, startWorkMonth.AddMonths(eligibilityOffset),
                                    defaultFund, (int)BpaTransactionbpaTransactionCategory.Eligibility, (int)BpaTransactionbpaTransactionType.SecondaryDollarBankDraw,
                                    deductionRate, false, UsingEligibilityCategoty, defaultFund.Id, lastAgreement);

                                if (previousMonthEligibility == null)
                                {
                                    if (sumSecondaryBank >= miniInitEligibility)
                                    {
                                        ContactHelper.SetCurrentEligibility(service, tracingService, selectedContact.Id, (int)MemberPlanBpaCurrentEligibility.InBenefit, startWorkMonth.AddMonths(eligibilityOffset));

                                        RecalculationTransactionHelper.CreateTransaction(service, selectedContact.Id, startWorkMonth, startWorkMonth.AddMonths(eligibilityOffset),
                                            null, (int)BpaTransactionbpaTransactionCategory.Eligibility, (int)BpaTransactionbpaTransactionType.InBenefit,
                                            0, true, UsingEligibilityCategoty, defaultFund.Id, lastAgreement);
                                        
                                    }
                                    else
                                    {
                                        ContactHelper.SetCurrentEligibility(service, tracingService, selectedContact.Id,
                                            (int)MemberPlanBpaCurrentEligibility.NewMember, startWorkMonth.AddMonths(eligibilityOffset));

                                        RecalculationTransactionHelper.CreateTransaction(service, selectedContact.Id, startWorkMonth, startWorkMonth.AddMonths(eligibilityOffset),
                                            null, (int)BpaTransactionbpaTransactionCategory.Eligibility, (int)BpaTransactionbpaTransactionType.NewMember,
                                            0, true, UsingEligibilityCategoty, defaultFund.Id, lastAgreement);
                                    }
                                }
                                else
                                {
                                    ContactHelper.SetCurrentEligibility(service, tracingService, selectedContact.Id,
                                        (int)MemberPlanBpaCurrentEligibility.InBenefit, startWorkMonth.AddMonths(eligibilityOffset));

                                    RecalculationTransactionHelper.CreateTransaction(service, selectedContact.Id, startWorkMonth, startWorkMonth.AddMonths(eligibilityOffset),
                                        null, (int)BpaTransactionbpaTransactionCategory.Eligibility, (int)BpaTransactionbpaTransactionType.InBenefit,
                                        0, true, UsingEligibilityCategoty, defaultFund.Id, lastAgreement);
                                    isSelfPayCounterUpdate = true;
                                }
                                inactiveMonthCounter = 0;
                                #endregion
                            }
                            else
                            {
                                ContactHelper.SetCurrentEligibility(service, tracingService, selectedContact.Id,(int)MemberPlanBpaCurrentEligibility.NotInBenefit, startWorkMonth.AddMonths(eligibilityOffset));

                                RecalculationTransactionHelper.CreateTransaction(service, selectedContact.Id, startWorkMonth, startWorkMonth.AddMonths(eligibilityOffset), null, 
                                    (int)BpaTransactionbpaTransactionCategory.Eligibility, (int)BpaTransactionbpaTransactionType.NotInBenefit, 0, true, UsingEligibilityCategoty, 
                                    defaultFund.Id, lastAgreement);
                                inactiveMonthCounter = 1;
                            }
                        }
                        MemberWentToUnderReview = false;

                    }

                    #endregion
                }
                else if (currentEligibility == (int)MemberPlanBpaCurrentEligibility.UnderReview)
                {
                    #region -------------------- UNDER REVIEW --------------------

                    if (previousMonthEligibility == null)
                    {
                        if (sumdollarhour >= miniInitEligibility)
                        {
                            RecalculationTransactionHelper.CreateTransaction(service, selectedContact.Id, startWorkMonth, startWorkMonth.AddMonths(eligibilityOffset), defaultFund,
                                (int)BpaTransactionbpaTransactionCategory.Eligibility, (int)BpaTransactionbpaTransactionType.EligibilityDraw, deductionRate, false, 
                                UsingEligibilityCategoty, defaultFund.Id, lastAgreement);

                            ContactHelper.SetCurrentEligibility(service, tracingService, selectedContact.Id,
                                (int)MemberPlanBpaCurrentEligibility.InBenefit, startWorkMonth.AddMonths(eligibilityOffset));

                            RecalculationTransactionHelper.CreateTransaction(service, selectedContact.Id, startWorkMonth, startWorkMonth.AddMonths(eligibilityOffset), null, 
                                (int)BpaTransactionbpaTransactionCategory.Eligibility, (int)BpaTransactionbpaTransactionType.InBenefit, 0, true, UsingEligibilityCategoty, 
                                defaultFund.Id, lastAgreement);
                            isSelfPayCounterUpdate = true;
                        }
                        else if (sumSecondaryBank >= miniInitEligibility)
                        {
                            ContactHelper.SetCurrentEligibility(service, tracingService, selectedContact.Id, (int)MemberPlanBpaCurrentEligibility.InBenefit,
                                startWorkMonth.AddMonths(eligibilityOffset));

                            RecalculationTransactionHelper.CreateTransaction(service, selectedContact.Id, startWorkMonth, startWorkMonth.AddMonths(eligibilityOffset),
                                null, (int)BpaTransactionbpaTransactionCategory.Eligibility, (int)BpaTransactionbpaTransactionType.InBenefit,
                                0, true, UsingEligibilityCategoty, defaultFund.Id, lastAgreement);

                        }
                        else
                        {
                            ContactHelper.SetCurrentEligibility(service, tracingService, selectedContact.Id,
                                (int)MemberPlanBpaCurrentEligibility.NewMember, startWorkMonth.AddMonths(eligibilityOffset));

                            RecalculationTransactionHelper.CreateTransaction(service, selectedContact.Id, startWorkMonth, startWorkMonth.AddMonths(eligibilityOffset), null, 
                                (int)BpaTransactionbpaTransactionCategory.Eligibility, (int)BpaTransactionbpaTransactionType.NewMember, 0, true, UsingEligibilityCategoty, 
                                defaultFund.Id, lastAgreement);
                        }
                        MemberWentToUnderReview = false;
                    }
                    else
                    {
                        ContactHelper.SetCurrentEligibility(service, tracingService, selectedContact.Id,
                            (int)MemberPlanBpaCurrentEligibility.UnderReview, startWorkMonth.AddMonths(eligibilityOffset));

                        RecalculationTransactionHelper.CreateUnderReviewTransaction(service, selectedContact.Id, UsingEligibilityCategoty,
                            startWorkMonth, startWorkMonth.AddMonths(eligibilityOffset), (int)BpaTransactionbpaTransactionCategory.Eligibility,
                            (int)BpaTransactionbpaTransactionType.UnderReview, defaultFund.Id, lastAgreement);

                        MemberWentToUnderReview = true;

                    }
                    inactiveMonthCounter = 0;

                    #endregion
                }

                #endregion  
            }
            else
            {
                #region ---------- PARTICAL SELF PAY ----------


                if (currentEligibility == (int)MemberPlanBpaCurrentEligibility.NewMember)
                {
                    #region ------------------- NEW MEMBER -----------------------

                    if (IsMissMatch)
                    {
                        RecalculationTransactionHelper.CreateUnderReviewTransaction(service, selectedContact.Id, UsingEligibilityCategoty, startWorkMonth, 
                            startWorkMonth.AddMonths(eligibilityOffset), (int)BpaTransactionbpaTransactionCategory.Eligibility, (int)BpaTransactionbpaTransactionType.UnderReview, 
                            defaultFund.Id, lastAgreement);

                        ContactHelper.SetCurrentEligibility(service, tracingService, selectedContact.Id, (int)MemberPlanBpaCurrentEligibility.UnderReview, startWorkMonth.AddMonths(eligibilityOffset));

                        MemberWentToUnderReview = true;
                    }
                    else
                    {
                        if (sumdollarhour >= miniInitEligibility)
                        {
                            RecalculationTransactionHelper.CreateTransaction(service, selectedContact.Id, startWorkMonth, startWorkMonth.AddMonths(eligibilityOffset), defaultFund,
                                (int)BpaTransactionbpaTransactionCategory.Eligibility, (int)BpaTransactionbpaTransactionType.EligibilityDraw, deductionRate, false, 
                                UsingEligibilityCategoty, defaultFund.Id, lastAgreement);

                            ContactHelper.SetCurrentEligibility(service, tracingService, selectedContact.Id, (int)MemberPlanBpaCurrentEligibility.InBenefit, startWorkMonth.AddMonths(eligibilityOffset));

                            RecalculationTransactionHelper.CreateTransaction(service, selectedContact.Id, startWorkMonth, startWorkMonth.AddMonths(eligibilityOffset), null,
                                (int)BpaTransactionbpaTransactionCategory.Eligibility, (int)BpaTransactionbpaTransactionType.InBenefit, 0, true, UsingEligibilityCategoty, 
                                defaultFund.Id, lastAgreement);
                            isSelfPayCounterUpdate = true;
                        }
                        else if (sumSecondaryBank >= miniInitEligibility)
                        {
                            RecalculationTransactionHelper.CreateTransaction(service, selectedContact.Id, startWorkMonth, startWorkMonth.AddMonths(eligibilityOffset),
                                defaultFund, (int)BpaTransactionbpaTransactionCategory.Eligibility, (int)BpaTransactionbpaTransactionType.SecondaryDollarBankDraw, deductionRate,
                               false, UsingEligibilityCategoty, defaultFund.Id, lastAgreement);

                            ContactHelper.SetCurrentEligibility(service, tracingService, selectedContact.Id,
                                (int)MemberPlanBpaCurrentEligibility.InBenefit, startWorkMonth.AddMonths(eligibilityOffset));

                            RecalculationTransactionHelper.CreateTransaction(service, selectedContact.Id, startWorkMonth,
                                startWorkMonth.AddMonths(eligibilityOffset), null, (int)BpaTransactionbpaTransactionCategory.Eligibility,
                                (int)BpaTransactionbpaTransactionType.InBenefit, 0, true, UsingEligibilityCategoty, defaultFund.Id, lastAgreement);

                            isSelfPayCounterUpdate = true;
                        }
                        else
                        {
                            ContactHelper.SetCurrentEligibility(service, tracingService, selectedContact.Id, (int)MemberPlanBpaCurrentEligibility.NewMember, startWorkMonth.AddMonths(eligibilityOffset));

                            RecalculationTransactionHelper.CreateTransaction(service, selectedContact.Id, startWorkMonth, startWorkMonth.AddMonths(eligibilityOffset), null,
                                (int)BpaTransactionbpaTransactionCategory.Eligibility, (int)BpaTransactionbpaTransactionType.NewMember, 0, true, UsingEligibilityCategoty, 
                                defaultFund.Id, lastAgreement);
                        }

                        SPCounter = 0;
                        inactiveMonthCounter = 1;
                        MemberWentToUnderReview = false;
                    }

                    #endregion
                } //end of New Number
                else if ((currentEligibility == (int)MemberPlanBpaCurrentEligibility.NotInBenefit) || (currentEligibility == (int)MemberPlanBpaCurrentEligibility.Reinstatement))
                {
                    #region  -------------- NOT IN BENEFIT ----------------------------

                    bool IsResinstatement = EligibilityCategoryHelper.IsCategoryReinstatement(service, UsingEligibilityCategoty.Id);
                    decimal reinstatementAmount = 0;

                    if (IsResinstatement)
                    {
                        reinstatementAmount = EligibilityCategoryHelper.GetReinstatementDollars(service, UsingEligibilityCategoty.Id, startWorkMonth);

                        if (reinstatementAmount == 0)
                            IsMissMatch = true;
                    }

                    //Added by jwalin
                    if (IsMissMatch)
                    {
                        ContactHelper.SetCurrentEligibility(service, tracingService, selectedContact.Id, (int)MemberPlanBpaCurrentEligibility.UnderReview, startWorkMonth.AddMonths(eligibilityOffset));

                        RecalculationTransactionHelper.CreateUnderReviewTransaction(service, selectedContact.Id, UsingEligibilityCategoty, startWorkMonth, 
                            startWorkMonth.AddMonths(eligibilityOffset), (int)BpaTransactionbpaTransactionCategory.Eligibility, (int)BpaTransactionbpaTransactionType.UnderReview, 
                            defaultFund.Id, lastAgreement);

                        MemberWentToUnderReview = false;
                    }
                    else
                    {
                        if (IsResinstatement && ((currentEligibility == (int)MemberPlanBpaCurrentEligibility.Reinstatement) || HasECED))
                        {
                            if (sumdollarhour >= reinstatementAmount)
                            {
                                RecalculationTransactionHelper.CreateTransaction(service, selectedContact.Id, startWorkMonth, startWorkMonth.AddMonths(eligibilityOffset), defaultFund,
                                    (int)BpaTransactionbpaTransactionCategory.Eligibility, (int)BpaTransactionbpaTransactionType.EligibilityDraw, deductionRate, false, 
                                    UsingEligibilityCategoty, defaultFund.Id, lastAgreement);

                                ContactHelper.SetCurrentEligibility(service, tracingService, selectedContact.Id, (int)MemberPlanBpaCurrentEligibility.InBenefit, startWorkMonth.AddMonths(eligibilityOffset));

                                RecalculationTransactionHelper.CreateTransaction(service, selectedContact.Id, startWorkMonth, startWorkMonth.AddMonths(eligibilityOffset), null,
                                    (int)BpaTransactionbpaTransactionCategory.Eligibility, (int)BpaTransactionbpaTransactionType.InBenefit, 0, true, UsingEligibilityCategoty, 
                                    defaultFund.Id, lastAgreement);
                                isSelfPayCounterUpdate = true;
                            }
                            else
                            {
                                RecalculationTransactionHelper.CreateTransaction(service, selectedContact.Id, startWorkMonth, startWorkMonth.AddMonths(eligibilityOffset), null,
                                    (int)BpaTransactionbpaTransactionCategory.Eligibility, (int)BpaTransactionbpaTransactionType.ReInstating, 0, true, UsingEligibilityCategoty, 
                                    defaultFund.Id, lastAgreement);

                                ContactHelper.SetCurrentEligibility(service, tracingService, selectedContact.Id, (int)MemberPlanBpaCurrentEligibility.Reinstatement, startWorkMonth.AddMonths(eligibilityOffset));
                            }

                            inactiveMonthCounter = 0;
                        }
                        else
                        {
                            if (sumdollarhour + deductionRate >= 0)
                            {
                                RecalculationTransactionHelper.CreateTransaction(service, selectedContact.Id, startWorkMonth, startWorkMonth.AddMonths(eligibilityOffset), defaultFund,
                                    (int)BpaTransactionbpaTransactionCategory.Eligibility, (int)BpaTransactionbpaTransactionType.EligibilityDraw, deductionRate, false, 
                                    UsingEligibilityCategoty, defaultFund.Id, lastAgreement);

                                if (previousMonthEligibility == null)
                                {
                                    ContactHelper.SetCurrentEligibility(service, tracingService, selectedContact.Id,
                                        (int)MemberPlanBpaCurrentEligibility.NewMember, startWorkMonth.AddMonths(eligibilityOffset));

                                    RecalculationTransactionHelper.CreateTransaction(service, selectedContact.Id, startWorkMonth, startWorkMonth.AddMonths(eligibilityOffset),
                                        null, (int)BpaTransactionbpaTransactionCategory.Eligibility, (int)BpaTransactionbpaTransactionType.NewMember,
                                        0, true, UsingEligibilityCategoty, defaultFund.Id, lastAgreement);
                                }
                                else
                                {
                                    ContactHelper.SetCurrentEligibility(service, tracingService, selectedContact.Id,
                                        (int)MemberPlanBpaCurrentEligibility.InBenefit, startWorkMonth.AddMonths(eligibilityOffset));

                                    RecalculationTransactionHelper.CreateTransaction(service, selectedContact.Id, startWorkMonth, startWorkMonth.AddMonths(eligibilityOffset),
                                        null, (int)BpaTransactionbpaTransactionCategory.Eligibility, (int)BpaTransactionbpaTransactionType.InBenefit,
                                        0, true, UsingEligibilityCategoty, defaultFund.Id, lastAgreement);
                                    isSelfPayCounterUpdate = true;
                                }
                                inactiveMonthCounter = 0;
                            }
                            else if (sumSecondaryBank + deductionRate >= 0)
                            {
                                #region ------------- SECONDARY DOLLAR BANK DRAW ---------------
                                RecalculationTransactionHelper.CreateTransaction(service, selectedContact.Id, startWorkMonth, startWorkMonth.AddMonths(eligibilityOffset),
                                    defaultFund, (int)BpaTransactionbpaTransactionCategory.Eligibility, (int)BpaTransactionbpaTransactionType.SecondaryDollarBankDraw,
                                    deductionRate, false, UsingEligibilityCategoty, defaultFund.Id, lastAgreement);

                                if (previousMonthEligibility == null)
                                {
                                    if (sumSecondaryBank >= miniInitEligibility)
                                    {
                                        ContactHelper.SetCurrentEligibility(service, tracingService, selectedContact.Id, (int)MemberPlanBpaCurrentEligibility.InBenefit, startWorkMonth.AddMonths(eligibilityOffset));

                                        RecalculationTransactionHelper.CreateTransaction(service, selectedContact.Id, startWorkMonth, startWorkMonth.AddMonths(eligibilityOffset),
                                            null, (int)BpaTransactionbpaTransactionCategory.Eligibility, (int)BpaTransactionbpaTransactionType.InBenefit,
                                            0, true, UsingEligibilityCategoty, defaultFund.Id, lastAgreement);

                                    }
                                    else
                                    {
                                        ContactHelper.SetCurrentEligibility(service, tracingService, selectedContact.Id,
                                            (int)MemberPlanBpaCurrentEligibility.NewMember, startWorkMonth.AddMonths(eligibilityOffset));

                                        RecalculationTransactionHelper.CreateTransaction(service, selectedContact.Id, startWorkMonth, startWorkMonth.AddMonths(eligibilityOffset),
                                            null, (int)BpaTransactionbpaTransactionCategory.Eligibility, (int)BpaTransactionbpaTransactionType.NewMember,
                                            0, true, UsingEligibilityCategoty, defaultFund.Id, lastAgreement);
                                    }
                                }
                                else
                                {
                                    ContactHelper.SetCurrentEligibility(service, tracingService, selectedContact.Id,
                                        (int)MemberPlanBpaCurrentEligibility.InBenefit, startWorkMonth.AddMonths(eligibilityOffset));

                                    RecalculationTransactionHelper.CreateTransaction(service, selectedContact.Id, startWorkMonth, startWorkMonth.AddMonths(eligibilityOffset),
                                        null, (int)BpaTransactionbpaTransactionCategory.Eligibility, (int)BpaTransactionbpaTransactionType.InBenefit,
                                        0, true, UsingEligibilityCategoty, defaultFund.Id, lastAgreement);
                                    isSelfPayCounterUpdate = true;
                                }
                                inactiveMonthCounter = 0;
                                #endregion
                            }
                            else
                            {
                                ContactHelper.SetCurrentEligibility(service, tracingService, selectedContact.Id,
                                    (int)MemberPlanBpaCurrentEligibility.NotInBenefit, startWorkMonth.AddMonths(eligibilityOffset));

                                RecalculationTransactionHelper.CreateTransaction(service, selectedContact.Id, startWorkMonth, startWorkMonth.AddMonths(eligibilityOffset),
                                    null, (int)BpaTransactionbpaTransactionCategory.Eligibility, (int)BpaTransactionbpaTransactionType.NotInBenefit,
                                    0, true, UsingEligibilityCategoty, defaultFund.Id, lastAgreement);
                                inactiveMonthCounter = 1;
                            }
                        }
                        MemberWentToUnderReview = false;

                    }

                    #endregion
                }
                else if ((currentEligibility == (int)MemberPlanBpaCurrentEligibility.InBenefit) || (currentEligibility == (int)MemberPlanBpaCurrentEligibility.SelfPayInBenefit))
                {
                    #region --------------- INBENEFIT & SELFPAY IN BENEIT -----------

                    //Added by jwalin
                    if (IsMissMatch)
                    {
                        #region ------ IS Miss Match ----------
                        ContactHelper.SetCurrentEligibility(service, tracingService, selectedContact.Id, (int)MemberPlanBpaCurrentEligibility.UnderReview, startWorkMonth.AddMonths(eligibilityOffset));

                        RecalculationTransactionHelper.CreateUnderReviewTransaction(service, selectedContact.Id, UsingEligibilityCategoty, startWorkMonth, startWorkMonth.AddMonths(eligibilityOffset),
                            (int)BpaTransactionbpaTransactionCategory.Eligibility, (int)BpaTransactionbpaTransactionType.UnderReview, defaultFund.Id, lastAgreement);

                        MemberWentToUnderReview = true;

                        #endregion
                    }
                    else
                    {
                        //Find MaxConsecutiveMonthsSelfPay from Eligibility Category 
                        int MaxConsecutiveMonthsSelfPay = EligibilityCategoryHelper.GetMaxConsecutiveMonthsSelfPay(service, UsingEligibilityCategoty.Id);

                        //Dollar Bank is enough for eligibility draw
                        if (sumdollarhour + deductionRate >= 0)
                        {
                            #region ------------ Dollar Bank and Hour Bank has enough or not? ----------------

                            RecalculationTransactionHelper.CreateTransaction(service, selectedContact.Id, startWorkMonth, startWorkMonth.AddMonths(eligibilityOffset), defaultFund,
                                (int)BpaTransactionbpaTransactionCategory.Eligibility, (int)BpaTransactionbpaTransactionType.EligibilityDraw, deductionRate, false, 
                                UsingEligibilityCategoty, defaultFund.Id, lastAgreement);

                            if (previousMonthEligibility == null)
                            {
                                if (sumdollarhour >= miniInitEligibility)
                                {
                                    ContactHelper.SetCurrentEligibility(service, tracingService, selectedContact.Id, (int)MemberPlanBpaCurrentEligibility.InBenefit, startWorkMonth.AddMonths(eligibilityOffset));

                                    RecalculationTransactionHelper.CreateTransaction(service, selectedContact.Id, startWorkMonth, startWorkMonth.AddMonths(eligibilityOffset), null,
                                        (int)BpaTransactionbpaTransactionCategory.Eligibility, (int)BpaTransactionbpaTransactionType.InBenefit, 0, true, UsingEligibilityCategoty, 
                                        defaultFund.Id, lastAgreement);
                                    isSelfPayCounterUpdate = true;
                                }
                                else
                                {
                                    ContactHelper.SetCurrentEligibility(service, tracingService, selectedContact.Id, (int)MemberPlanBpaCurrentEligibility.NewMember, startWorkMonth.AddMonths(eligibilityOffset));

                                    RecalculationTransactionHelper.CreateTransaction(service, selectedContact.Id, startWorkMonth, startWorkMonth.AddMonths(eligibilityOffset), null,
                                        (int)BpaTransactionbpaTransactionCategory.Eligibility, (int)BpaTransactionbpaTransactionType.NewMember, 0, true, UsingEligibilityCategoty, 
                                        defaultFund.Id, lastAgreement);
                                }
                            }
                            else
                            {
                                ContactHelper.SetCurrentEligibility(service, tracingService, selectedContact.Id, (int)MemberPlanBpaCurrentEligibility.InBenefit, startWorkMonth.AddMonths(eligibilityOffset));

                                RecalculationTransactionHelper.CreateTransaction(service, selectedContact.Id, startWorkMonth, startWorkMonth.AddMonths(eligibilityOffset), null,
                                    (int)BpaTransactionbpaTransactionCategory.Eligibility, (int)BpaTransactionbpaTransactionType.InBenefit, 0, true, UsingEligibilityCategoty, 
                                    defaultFund.Id, lastAgreement);

                                isSelfPayCounterUpdate = true;
                            }

                            SPCounter = 0;
                            inactiveMonthCounter = 0;
                            #endregion
                        }
                        else if (sumSecondaryBank + deductionRate >= 0)
                        {
                            #region ------------- SECONDARY DOLLAR BANK DRAW ---------------
                            RecalculationTransactionHelper.CreateTransaction(service, selectedContact.Id, startWorkMonth, startWorkMonth.AddMonths(eligibilityOffset),
                                defaultFund, (int)BpaTransactionbpaTransactionCategory.Eligibility, (int)BpaTransactionbpaTransactionType.SecondaryDollarBankDraw,
                                deductionRate, false, UsingEligibilityCategoty, defaultFund.Id, lastAgreement);

                            if (previousMonthEligibility == null)
                            {
                                if (sumSecondaryBank >= miniInitEligibility)
                                {
                                    ContactHelper.SetCurrentEligibility(service, tracingService, selectedContact.Id,
                                        (int)MemberPlanBpaCurrentEligibility.InBenefit, startWorkMonth.AddMonths(eligibilityOffset));

                                    RecalculationTransactionHelper.CreateTransaction(service, selectedContact.Id, startWorkMonth, startWorkMonth.AddMonths(eligibilityOffset),
                                        null, (int)BpaTransactionbpaTransactionCategory.Eligibility, (int)BpaTransactionbpaTransactionType.InBenefit,
                                        0, true, UsingEligibilityCategoty, defaultFund.Id, lastAgreement);
                                    isSelfPayCounterUpdate = true;
                                }
                                else
                                {
                                    ContactHelper.SetCurrentEligibility(service, tracingService, selectedContact.Id,
                                        (int)MemberPlanBpaCurrentEligibility.NewMember, startWorkMonth.AddMonths(eligibilityOffset));

                                    RecalculationTransactionHelper.CreateTransaction(service, selectedContact.Id, startWorkMonth, startWorkMonth.AddMonths(eligibilityOffset),
                                        null, (int)BpaTransactionbpaTransactionCategory.Eligibility, (int)BpaTransactionbpaTransactionType.NewMember,
                                        0, true, UsingEligibilityCategoty, defaultFund.Id, lastAgreement);
                                }
                            }
                            else
                            {
                                ContactHelper.SetCurrentEligibility(service, tracingService, selectedContact.Id,
                                    (int)MemberPlanBpaCurrentEligibility.InBenefit, startWorkMonth.AddMonths(eligibilityOffset));

                                RecalculationTransactionHelper.CreateTransaction(service, selectedContact.Id, startWorkMonth, startWorkMonth.AddMonths(eligibilityOffset),
                                    null, (int)BpaTransactionbpaTransactionCategory.Eligibility, (int)BpaTransactionbpaTransactionType.InBenefit,
                                    0, true, UsingEligibilityCategoty, defaultFund.Id, lastAgreement);
                                isSelfPayCounterUpdate = true;
                            }

                            SPCounter = 0;
                            inactiveMonthCounter = 0;
                            isSelfPayCounterUpdate = true;

                            #endregion
                        }
                        //Dollar Bank is enough for maximum Fund Assistance
                        else if (sumdollarhour >= fundAssistanceAmount)
                        {
                            #region --------- Dollar Bank is enough for Fund Assistance Amount ------

                            decimal maximumSelfPayAmount = (deductionRate * -1) - sumdollarhour;
                            if (SelfPayBank.Value >= maximumSelfPayAmount)
                            {
                                #region --------- Selfpay bank is enough -------
                                //1. draw of 'maximumAssistance' which is self pay
                                //2. draw full amount of dollar bank


                                //Find out MAx Month of Employer Contribution Contribution (if not found return WorkMonth)
                                DateTime ECMaxDate = TransactionHelper.FetchMaxEmployerContributionDate(service, tracingService, selectedContact.Id, startWorkMonth);

                                //Find out How many Previously SELFPAY
                                int cntSP = 0;
                                Entity lastEC = RecalculationTransactionHelper.FetchLastEligiblityMonthExceptSelfPay(service, tracingService,
                                        ECMaxDate, ECMaxDate, selectedContact.Id);
                                if (lastEC != null && lastEC.Attributes.Contains("bpa_transactiondate"))
                                {
                                    DateTime end = startWorkMonth.AddMonths(-1);
                                    DateTime start =
                                        lastEC.GetAttributeValue<DateTime>("bpa_transactiondate");
                                    cntSP = end.Month + end.Year * 12 - (start.Month + start.Year * 12);
                                }
                                if (cntSP == 0)
                                {
                                    cntSP = RecalculationTransactionHelper.GetHowManySelfPay(service, tracingService, ECMaxDate, startWorkMonth, selectedContact.Id);
                                }

                                if (cntSP < MaxConsecutiveMonthsSelfPay)
                                {
                                    RecalculationTransactionHelper.CreateTransaction(service, selectedContact.Id, startWorkMonth, startWorkMonth.AddMonths(eligibilityOffset), defaultFund,
                                        (int)BpaTransactionbpaTransactionCategory.Eligibility, (int)BpaTransactionbpaTransactionType.EligibilityDraw, (sumdollarhour * -1), false, 
                                        UsingEligibilityCategoty, defaultFund.Id, lastAgreement);

                                    RecalculationTransactionHelper.CreateTransaction(service, selectedContact.Id, startWorkMonth, startWorkMonth.AddMonths(eligibilityOffset), defaultFund,
                                        (int)BpaTransactionbpaTransactionCategory.Eligibility, (int)BpaTransactionbpaTransactionType.SelfPayDraw, (Math.Abs(maximumSelfPayAmount) * -1), 
                                        false, null, defaultFund.Id, lastAgreement);

                                    RecalculationTransactionHelper.CreateTransaction(service, selectedContact.Id, startWorkMonth, startWorkMonth.AddMonths(eligibilityOffset),
                                       null, (int)BpaTransactionbpaTransactionCategory.Eligibility, (int)BpaTransactionbpaTransactionType.SelfPayInBenefit, 0, true, 
                                       UsingEligibilityCategoty, defaultFund.Id, lastAgreement);

                                    //selfPayCounter = cntSP + 1;
                                    SPCounter = cntSP + 1;
                                    isSelfPayCounterUpdate = true;

                                    // Trigger Self Pay Bank Rollup
                                    PluginHelper.TriggerRollup(service, tracingService, "bpa_memberplan", selectedContact.Id, "bpa_selfpaybank");

                                    ContactHelper.SetCurrentEligibility(service, tracingService, selectedContact.Id, (int)MemberPlanBpaCurrentEligibility.SelfPayInBenefit, startWorkMonth.AddMonths(eligibilityOffset));
                                }
                                else
                                {
                                    ContactHelper.SetCurrentEligibility(service, tracingService, selectedContact.Id, (int)MemberPlanBpaCurrentEligibility.NotInBenefit, startWorkMonth.AddMonths(eligibilityOffset));

                                    RecalculationTransactionHelper.CreateTransaction(service, selectedContact.Id, startWorkMonth, startWorkMonth.AddMonths(eligibilityOffset), null,
                                        (int)BpaTransactionbpaTransactionCategory.Eligibility, (int)BpaTransactionbpaTransactionType.NotInBenefit, 0, true, UsingEligibilityCategoty, 
                                        defaultFund.Id, lastAgreement);
                                    inactiveMonthCounter = 1;
                                }



                                #endregion
                            }
                            else
                            {
                                ContactHelper.SetCurrentEligibility(service, tracingService, selectedContact.Id, (int)MemberPlanBpaCurrentEligibility.NotInBenefit, startWorkMonth.AddMonths(eligibilityOffset));

                                RecalculationTransactionHelper.CreateTransaction(service, selectedContact.Id, startWorkMonth, startWorkMonth.AddMonths(eligibilityOffset), null,
                                    (int)BpaTransactionbpaTransactionCategory.Eligibility, (int)BpaTransactionbpaTransactionType.NotInBenefit, 0, true, UsingEligibilityCategoty, 
                                    defaultFund.Id, lastAgreement);
                                inactiveMonthCounter = 1;
                            }
                            #endregion
                        }
                        else
                        {
                            #region --------- Dollar Bank is NOT enough for Fund Assistance Amount ------

                            //Selef Pay Bank is enough for SElf pay draw
                            if ((selfPayDeductionRate != 0) && (SelfPayBank.Value >= (selfPayDeductionRate * -1)) && (sumdollarhour >=0 ))
                            //if ((selfPayDeductionRate != 0) && (SelfPayBank.Value >= (selfPayDeductionRate * -1)))
                            {
                                #region ------------- Self Pay Bank Checking ------------------

                                //Find out MAx Month of Employer Contribution Contribution (if not found return WorkMonth)
                                DateTime ECMaxDate = TransactionHelper.FetchMaxEmployerContributionDate(service, tracingService, selectedContact.Id, startWorkMonth);

                                //Find out How many Previously SELFPAY
                                int cntSP = 0;
                                Entity lastEC = RecalculationTransactionHelper.FetchLastEligiblityMonthExceptSelfPay(service, tracingService,
                                        ECMaxDate, ECMaxDate, selectedContact.Id);
                                if (lastEC != null && lastEC.Attributes.Contains("bpa_transactiondate"))
                                {
                                    DateTime end = startWorkMonth.AddMonths(-1);
                                    DateTime start =
                                        lastEC.GetAttributeValue<DateTime>("bpa_transactiondate");
                                    cntSP = end.Month + end.Year * 12 - (start.Month + start.Year * 12);
                                }
                                if (cntSP == 0)
                                {
                                    cntSP = RecalculationTransactionHelper.GetHowManySelfPay(service, tracingService, ECMaxDate, startWorkMonth, selectedContact.Id);
                                }

                                if (cntSP < MaxConsecutiveMonthsSelfPay)
                                {
                                    //Give Maximum Fund Assistance Amount

                                    decimal fundAssistanceEiligibilityAmount = Math.Abs(deductionRate) - Math.Abs(sumdollarhour) - Math.Abs(selfPayDeductionRate);
                                    RecalculationTransactionHelper.CreateFundAssistedTransactions(service, selectedContact.Id, startWorkMonth, startWorkMonth.AddMonths(eligibilityOffset),
                                        new Money(fundAssistanceEiligibilityAmount), null, (int)BpaTransactionbpaTransactionCategory.Eligibility, 
                                        (int)BpaTransactionbpaTransactionType.FundAssisted, defaultFund,UsingEligibilityCategoty, defaultFund.Id, lastAgreement);

                                    //Make a Eligibility Draw of Fund Assistance Amount
                                    RecalculationTransactionHelper.CreateFundAssistedTransactions(service, selectedContact.Id, startWorkMonth, startWorkMonth.AddMonths(eligibilityOffset),
                                        new Money(fundAssistanceAmount * -1), null, (int)BpaTransactionbpaTransactionCategory.Eligibility, (int)BpaTransactionbpaTransactionType.EligibilityDraw, 
                                        defaultFund, null,defaultFund.Id, lastAgreement);
                                    
                                    // Create SelfPay draw and Self pay In Benefit Transaction
                                    RecalculationTransactionHelper.CreateTransaction(service, selectedContact.Id, startWorkMonth, startWorkMonth.AddMonths(eligibilityOffset),
                                        defaultFund, (int)BpaTransactionbpaTransactionCategory.Eligibility, (int)BpaTransactionbpaTransactionType.SelfPayDraw, selfPayDeductionRate,
                                        false, UsingEligibilityCategoty, defaultFund.Id, lastAgreement);

                                    RecalculationTransactionHelper.CreateTransaction(service, selectedContact.Id, startWorkMonth, startWorkMonth.AddMonths(eligibilityOffset),
                                        null, (int)BpaTransactionbpaTransactionCategory.Eligibility, (int)BpaTransactionbpaTransactionType.SelfPayInBenefit, 0, true, 
                                        UsingEligibilityCategoty, defaultFund.Id, lastAgreement);

                                    //selfPayCounter = cntSP + 1;
                                    SPCounter = cntSP + 1;
                                    isSelfPayCounterUpdate = true;

                                    // Trigger Self Pay Bank Rollup
                                    PluginHelper.TriggerRollup(service, tracingService, "bpa_memberplan", selectedContact.Id, "bpa_selfpaybank");

                                    ContactHelper.SetCurrentEligibility(service, tracingService, selectedContact.Id, (int)MemberPlanBpaCurrentEligibility.SelfPayInBenefit, startWorkMonth.AddMonths(eligibilityOffset));
                                }
                                else
                                {
                                    ContactHelper.SetCurrentEligibility(service, tracingService, selectedContact.Id, (int)MemberPlanBpaCurrentEligibility.NotInBenefit, startWorkMonth.AddMonths(eligibilityOffset));

                                    RecalculationTransactionHelper.CreateTransaction(service, selectedContact.Id, startWorkMonth, startWorkMonth.AddMonths(eligibilityOffset), null,
                                        (int)BpaTransactionbpaTransactionCategory.Eligibility, (int)BpaTransactionbpaTransactionType.NotInBenefit, 0, true, 
                                        UsingEligibilityCategoty, defaultFund.Id, lastAgreement);
                                    inactiveMonthCounter = 1;
                                }
                                #endregion
                            }
                            else
                            {
                                ContactHelper.SetCurrentEligibility(service, tracingService, selectedContact.Id, (int)MemberPlanBpaCurrentEligibility.NotInBenefit, startWorkMonth.AddMonths(eligibilityOffset));

                                RecalculationTransactionHelper.CreateTransaction(service, selectedContact.Id, startWorkMonth, startWorkMonth.AddMonths(eligibilityOffset), null,
                                    (int)BpaTransactionbpaTransactionCategory.Eligibility, (int)BpaTransactionbpaTransactionType.NotInBenefit, 0, true, 
                                    UsingEligibilityCategoty, defaultFund.Id, lastAgreement);
                                inactiveMonthCounter = 1;
                            }

                            #endregion
                        }
                        MemberWentToUnderReview = false;
                        
                    }


                    #endregion
                }
                else if (currentEligibility == (int)MemberPlanBpaCurrentEligibility.UnderReview)
                {
                    #region -------------------- UNDER REVIEW --------------------

                    if (previousMonthEligibility == null)
                    {
                        if (sumdollarhour >= miniInitEligibility)
                        {
                            RecalculationTransactionHelper.CreateTransaction(service, selectedContact.Id, startWorkMonth, startWorkMonth.AddMonths(eligibilityOffset), defaultFund,
                                (int)BpaTransactionbpaTransactionCategory.Eligibility, (int)BpaTransactionbpaTransactionType.EligibilityDraw, deductionRate, false, 
                                UsingEligibilityCategoty, defaultFund.Id, lastAgreement);
                            
                            ContactHelper.SetCurrentEligibility(service, tracingService, selectedContact.Id,
                                (int)MemberPlanBpaCurrentEligibility.InBenefit, startWorkMonth.AddMonths(eligibilityOffset));

                            RecalculationTransactionHelper.CreateTransaction(service, selectedContact.Id, startWorkMonth,
                                startWorkMonth.AddMonths(eligibilityOffset), null, (int)BpaTransactionbpaTransactionCategory.Eligibility,
                                (int)BpaTransactionbpaTransactionType.InBenefit, 0, true, UsingEligibilityCategoty, defaultFund.Id, lastAgreement);
                            isSelfPayCounterUpdate = true;
                        }
                        else if (sumSecondaryBank >= miniInitEligibility)
                        {
                            RecalculationTransactionHelper.CreateTransaction(service, selectedContact.Id, startWorkMonth, startWorkMonth.AddMonths(eligibilityOffset),
                                defaultFund, (int)BpaTransactionbpaTransactionCategory.Eligibility, (int)BpaTransactionbpaTransactionType.SecondaryDollarBankDraw, deductionRate,
                               false, UsingEligibilityCategoty, defaultFund.Id, lastAgreement);

                            ContactHelper.SetCurrentEligibility(service, tracingService, selectedContact.Id,
                                (int)MemberPlanBpaCurrentEligibility.InBenefit, startWorkMonth.AddMonths(eligibilityOffset));

                            RecalculationTransactionHelper.CreateTransaction(service, selectedContact.Id, startWorkMonth,
                                startWorkMonth.AddMonths(eligibilityOffset), null, (int)BpaTransactionbpaTransactionCategory.Eligibility,
                                (int)BpaTransactionbpaTransactionType.InBenefit, 0, true, UsingEligibilityCategoty, defaultFund.Id, lastAgreement);

                            isSelfPayCounterUpdate = true;
                        }

                        else
                        {
                            ContactHelper.SetCurrentEligibility(service, tracingService, selectedContact.Id,
                                (int)MemberPlanBpaCurrentEligibility.NewMember, startWorkMonth.AddMonths(eligibilityOffset));

                            RecalculationTransactionHelper.CreateTransaction(service, selectedContact.Id, startWorkMonth,
                                startWorkMonth.AddMonths(eligibilityOffset), null, (int)BpaTransactionbpaTransactionCategory.Eligibility,
                                (int)BpaTransactionbpaTransactionType.NewMember, 0, true, UsingEligibilityCategoty, defaultFund.Id, lastAgreement);
                        }
                        MemberWentToUnderReview = false;
                    }
                    else
                    {
                        ContactHelper.SetCurrentEligibility(service, tracingService, selectedContact.Id,
                            (int)MemberPlanBpaCurrentEligibility.UnderReview, startWorkMonth.AddMonths(eligibilityOffset));

                        RecalculationTransactionHelper.CreateUnderReviewTransaction(service, selectedContact.Id, UsingEligibilityCategoty,
                            startWorkMonth, startWorkMonth.AddMonths(eligibilityOffset), (int)BpaTransactionbpaTransactionCategory.Eligibility,
                            (int)BpaTransactionbpaTransactionType.UnderReview, defaultFund.Id, lastAgreement);

                        MemberWentToUnderReview = true;

                    }
                    inactiveMonthCounter = 0;

                    #endregion
                }


                #endregion
            }


            #region -------------- RESERVE TRANSAFER  -----------------------------

            if (UsingEligibilityCategoty != null)
            {
                //VERIFY THIS MONTH ALREDY HAS RESERVER TRANSFER OR NOT??
                bool _IsReserveExists = RecalculationTransactionHelper.IsReserveExists(service,selectedContact.Id, startWorkMonth);

                if (_IsReserveExists == false)
                {
                    decimal maxMemberBank = EligibilityCategoryHelper.GetMaximumMemberBank(service, UsingEligibilityCategoty.Id, startWorkMonth);
                    decimal currentsumdollarhour = RecalculationTransactionHelper.GetSumDollarHour(service, selectedContact.Id, startWorkMonth, defaultFund.Id);
                    decimal totalEmployerContribution = RecalculationTransactionHelper.GetSumAmount(service, selectedContact.Id, startWorkMonth,
                            (int)BpaTransactionbpaTransactionType.EmployerContribution);
                    decimal totalEligibilityDraw = RecalculationTransactionHelper.GetSumAmount(service, selectedContact.Id, 
                        startWorkMonth, (int)BpaTransactionbpaTransactionType.EligibilityDraw);
                    decimal totalReserveTransfer = RecalculationTransactionHelper.GetSumAmount(service, selectedContact.Id, 
                        startWorkMonth, (int)BpaTransactionbpaTransactionType.ReserveTransfer);

                    //We remove this [(maxMemberBank != 0) &&] condition because of FLAT - DOLLAR Combination 
                    //Added condition by jwalin (maxMemberBank != 0) 
                    if (currentsumdollarhour >= maxMemberBank)
                    {
                        decimal reserveTransferAmount = maxMemberBank - (totalEmployerContribution + totalEligibilityDraw + totalReserveTransfer);
                        //TFS 107, TFS 108, TFS 109
                        if ((reserveTransferAmount < 0) && (MemberWentToUnderReview == false) && (currentEligibility != (int)MemberPlanBpaCurrentEligibility.Frozen))
                        {
                            RecalculationTransactionHelper.CreatReserveTransfer(service, selectedContact.Id, startWorkMonth,startWorkMonth.AddMonths(eligibilityOffset),
                                reserveTransferAmount, defaultFund.Id);

                            //Get the Standing Transaction(like Not In Benefite, In Benefit .. )for that same month 
                            RecalculationTransactionHelper.UpdateEndingBalanace(service, startWorkMonth, selectedContact.Id, defaultFund.Id);
                        }
                    }
                    
                }
            }

            #endregion

            #region --------------- INACTIVE MONTH COUNTER LOGIC ------------------
            int consecutiveinactivemonths = 0;

            if (HasECED)
                inactiveMonthCounter = 0;
            else
            {
                DateTime end = startWorkMonth.AddMonths(-1);
                DateTime lastEligibilityDate = DateTime.MinValue;
                Entity lastEligibility = RecalculationTransactionHelper.FetchLastBenefitMonth(service, startWorkMonth, selectedContact.Id);
                if (lastEligibility != null && lastEligibility.Attributes.Contains("bpa_transactiondate"))
                {
                    lastEligibilityDate = lastEligibility.GetAttributeValue<DateTime>("bpa_transactiondate");
                }

                DateTime lastActivityDate = DateTime.MinValue;
                Entity lastIA = RecalculationTransactionHelper.FetchLastEligiblityContributionMonth(service, startWorkMonth, selectedContact.Id);
                if (lastIA != null && lastIA.Attributes.Contains("bpa_transactiondate"))
                {
                    lastActivityDate = lastIA.GetAttributeValue<DateTime>("bpa_transactiondate");
                }


                if (lastActivityDate == DateTime.MinValue && lastEligibilityDate == DateTime.MinValue)
                {
                    //Retrive directly from Member plan
                    Entity MemberPlan = service.Retrieve("bpa_memberplan", selectedContact.Id, new Microsoft.Xrm.Sdk.Query.ColumnSet(new string[] { "bpa_consecutiveinactivemonths" }));
                    consecutiveinactivemonths = MemberPlan.Contains("bpa_consecutiveinactivemonths") ? MemberPlan.GetAttributeValue<int>("bpa_consecutiveinactivemonths") : 0;

                }
                else
                {
                    if (lastActivityDate > lastEligibilityDate)
                        consecutiveinactivemonths = end.Month + end.Year * 12 - (lastActivityDate.Month + lastActivityDate.Year * 12);
                    else
                        consecutiveinactivemonths = end.Month + end.Year * 12 - (lastEligibilityDate.Month + lastEligibilityDate.Year * 12);
                }
            }
            #endregion

            //Set Out VARIABLES
            memberWentToUnderReview = MemberWentToUnderReview;
            selfPayCounter = SPCounter;
            InactiveMonthCounter = inactiveMonthCounter + consecutiveinactivemonths;

            #region ------ CRATE Inactive and Reserve Transfer Transaction --------

            ////CREATE 'INACTIVE' ELIGIBILITY TRANSACTION
            ////CREATE 'RESERVE TRANSFER ' TRANSACTION
            //// ALSO CHECK IS THERE ANY EXTRA TRANSACTION IN FUTURE DELETE IT.
            if (InactiveMonthCounter >= 0)
            {
                bool IsOverMax = false;
                if (InactiveMonthCounter > MaxInactiveCounter)
                {
                    InactiveMonthCounter = MaxInactiveCounter;
                    IsOverMax = true;
                }
                if (InactiveMonthCounter <= MaxInactiveCounter)
                {
                    ContactHelper.UpdateInactiveMonthsCounter(service, tracingService, selectedContact.Id, InactiveMonthCounter);
                    if (InactiveMonthCounter > 0)
                    {
                        if (InactiveMonthCounter == MaxInactiveCounter)
                        {
                            EntityCollection _InactiveNReserveTransactions = RecalculationTransactionHelper.AlreadyHasInactiveAndReserveTransactions(service, selectedContact.Id, startWorkMonth.AddMonths(1));

                            if (_InactiveNReserveTransactions != null && _InactiveNReserveTransactions.Entities.Count > 0)
                            {
                                foreach (Entity _t in _InactiveNReserveTransactions.Entities)
                                {
                                    RecalculationTransactionHelper.DeleteTransaction(service, _t.Id);
                                }
                            }
                            ////CREATE 'INACTIVE' ELIGIBILITY TRANSACTION
                            ////CREATE 'RESERVE TRANSFER ' TRANSACTION
                            if (IsOverMax)
                            {
                                RecalculationTransactionHelper.CreateTransaction(service, selectedContact.Id, startWorkMonth,
                                    startWorkMonth.AddMonths(eligibilityOffset), string.Empty, (int)BpaTransactionbpaTransactionCategory.Eligibility,
                                    (int)BpaTransactionbpaTransactionType.Inactive, 0, true, UsingEligibilityCategoty, true, defaultFund.Id, lastAgreement);

                                //Delete Transaction for that month
                                RecalculationTransactionHelper.DeleteTransactionNotInBenefit(service, selectedContact.Id, startWorkMonth);
                            }
                            else
                            {
                                RecalculationTransactionHelper.CreateTransaction(service, selectedContact.Id, startWorkMonth.AddMonths(1),
                                    startWorkMonth.AddMonths(eligibilityOffset + 1), string.Empty, (int)BpaTransactionbpaTransactionCategory.Eligibility,
                                    (int)BpaTransactionbpaTransactionType.Inactive, 0, true, UsingEligibilityCategoty, true, defaultFund.Id, lastAgreement);
                            }

                            //UPDATE CONTACT 'CURRENT ELIGIBILITY' TO 'INACTIVE'
                            ContactHelper.SetCurrentEligibility(service, tracingService, selectedContact.Id, 
                                (int)MemberPlanBpaCurrentEligibility.Inactive, startWorkMonth.AddMonths(eligibilityOffset));

                            isMemberBecomeInActive = true;
                        }
                    }
                }
            }

            #endregion

            //UPDAET CONSECUTIVE BOTH MONTHS
            ContactHelper.UpdateBothConsecutiveMonths(service, selectedContact.Id, SPCounter, InactiveMonthCounter, isSelfPayCounterUpdate);

            //Set Out VARIABLE
            IsMemberBecomeInActive = isMemberBecomeInActive;
        }
        #endregion

    }

}