﻿#region

using System;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;

#endregion

namespace BPA.Benefit.Management.CRM.Plugins.Helper
{
    public class EventHelper
    {
        //public const string Event = "bpa_event";
        //public const string EventReport = "bpa_eventreport";

        public static bool IsExistsOpenEventActivity(IOrganizationService service, Guid memberId, int eventType, DateTime benefitMonth)
        {
            bool isexists = false;
            string fetchXml = $@"
                <fetch mapping='logical' output-format='xml-platform' version='1.0' >
                  <entity name='bpa_event'>
                    <filter>
                      <condition attribute='statecode' operator='eq' value='0' />
                      <condition attribute='statuscode' operator='eq' value='1' />
                      <condition attribute='bpa_memberplanid' operator='eq' value='{memberId}' />
                      <condition attribute='bpa_eventtype' operator='eq' value='{eventType}' />
                    </filter>
                  </entity>
                </fetch>
            ";
            //<condition attribute='bpa_benefitmonth' operator='eq' value='{benefitMonth.ToShortDateString()}' />
            EntityCollection collection = service.RetrieveMultiple(new FetchExpression(fetchXml));

            if (collection != null && collection.Entities.Count > 0)
                isexists = true;

            return isexists;
        }
    }
}