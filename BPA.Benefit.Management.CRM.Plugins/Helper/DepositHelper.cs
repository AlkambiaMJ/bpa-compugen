﻿#region

using System;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;

#endregion

namespace BPA.Benefit.Management.CRM.Plugins.Helper
{
    public static class DepositHelper
    {
        public static Guid IsDepositExists(IOrganizationService service, Guid trustId, Guid userId, int submissionPaymentMethod)
        {
            Guid depositId = Guid.Empty;
            int type = 0;

            if (submissionPaymentMethod == (int)SubmissionPaymentType.Cheque)
                type = (int)DepositType.Cheque;
            else if ((submissionPaymentMethod == (int)SubmissionPaymentType.ETFOrDirectDeposit))
                type = (int)DepositType.ETFDirectDeposit;
            else if ((submissionPaymentMethod == (int)SubmissionPaymentType.Debit))
                type = (int)DepositType.Debit;
            else if (submissionPaymentMethod == (int)SubmissionPaymentType.PreAuth)
                type = (int)DepositType.PreAuth;
            if (submissionPaymentMethod == (int)SubmissionPaymentType.NoPayment)
                type = (int)DepositType.NoPayment;

            string fetchxml = $@"
                <fetch>
                    <entity name='bpa_deposit'>
                    <filter>
                        <condition attribute='statecode' operator='eq' value='0' />
                        <condition attribute='bpa_depositstatus' operator='eq' value='{(int)DepositDepositStatus.Open}' />
                        <condition attribute='createdby' operator='eq' value='{userId }' />
                        <condition attribute='bpa_trust' operator='eq' value='{trustId}' />
                        <condition attribute='bpa_type' operator='eq' value='{type.ToString()}' />
                    </filter>
                    </entity>
                </fetch>
                ";

            EntityCollection deposits = service.RetrieveMultiple(new FetchExpression(fetchxml));
            if (deposits != null && deposits.Entities.Count > 0)
            {
                Entity deposit = deposits.Entities[0];
                depositId = deposit.Id;
            }

            return depositId;
        }

        public static Guid CreateDeposit(IOrganizationService service, Guid trustId, int submissionPaymentMethod,string defaultCurrency)
        {
            Guid depositId = Guid.Empty;
            Entity deposit = new Entity(PluginHelper.BpaDeposit);
            deposit["bpa_trust"] = new EntityReference(PluginHelper.BpaTrust, trustId);
            deposit["bpa_depositstatus"] = new OptionSetValue((int)DepositDepositStatus.Open);
            int type = 0;
            if (submissionPaymentMethod == (int)SubmissionPaymentType.Cheque)
                type = (int)DepositType.Cheque;
            else if (submissionPaymentMethod == (int)SubmissionPaymentType.Debit)
                type = (int)DepositType.Debit;
            else if (submissionPaymentMethod == (int)SubmissionPaymentType.ETFOrDirectDeposit)
                type = (int)DepositType.ETFDirectDeposit;
            else if (submissionPaymentMethod == (int)SubmissionPaymentType.PreAuth)
                type = (int)DepositType.PreAuth;
            else if (submissionPaymentMethod == (int)SubmissionPaymentType.NoPayment)
                type = (int)DepositType.NoPayment;

            if (type != 0)
                deposit["bpa_type"] = new OptionSetValue(type);
            //string defaultCurrency = ConfigurationHelper.FetchValueId(service, PluginHelper.ConfigDefaultCurrency);
            if (defaultCurrency != Guid.Empty.ToString())
                deposit["transactioncurrencyid"] = new EntityReference(PluginHelper.Transactioncurrency,
                    new Guid(defaultCurrency));
            deposit["bpa_ismanuallycreated"] = false;
            depositId = service.Create(deposit);
            return depositId;
        }

        public static Money GetSumOfSubmissionsByDepositId(IOrganizationService service, ITracingService tracingService, Guid depositId)
        {
            Money totalAmount = null;
            string fetchXml = $@"
			<fetch aggregate='true' >
			  <entity name='bpa_submission' >
				<attribute name='bpa_submissionpaid' alias='bpa_submissionpaid' aggregate='sum' />
				<filter>
				  <condition attribute='bpa_depositid' operator='eq' value='{depositId}' />
				</filter>
			  </entity>
			</fetch>";

            EntityCollection collection = service.RetrieveMultiple(new FetchExpression(fetchXml));
            if(collection != null && collection.Entities.Count > 0)
            {
                Entity entity = collection.Entities[0];

                if (entity.Contains("bpa_submissionpaid") && entity.GetAttributeValue<AliasedValue>("bpa_submissionpaid") != null)
                    totalAmount = (Money)entity.GetAttributeValue<AliasedValue>("bpa_submissionpaid").Value;
            }
            return totalAmount;
        }
    }
}