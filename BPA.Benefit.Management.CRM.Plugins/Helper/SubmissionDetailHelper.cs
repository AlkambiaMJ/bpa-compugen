﻿#region

using System;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;

#endregion

namespace BPA.Benefit.Management.CRM.Plugins.Helper
{
    public class SubmissionDetailHelper
    {
        public static bool IsSubmissionDetailExists(IOrganizationService service, ITracingService tracing,
            Guid submissionId, Guid employmentId)
        {
            bool isExists = false;
            string fetchXml = @"
                    <fetch>
                      <entity name='bpa_submissiondetail' >
                        <attribute name='bpa_name' />
                        <filter>
                          <condition attribute='statecode' operator='eq' value='0' />
                          <condition attribute='bpa_employmentid' operator='eq' value='" + employmentId + @"' />
                          <condition attribute='bpa_submissionid' operator='eq' value='" + submissionId + @"' />
                        </filter>
                      </entity>
                    </fetch>";
            EntityCollection collection = service.RetrieveMultiple(new FetchExpression(fetchXml));
            if (collection != null && collection.Entities.Count > 0)
            {
                isExists = true;
            }
            return isExists;
        }

        public static EntityCollection FetchAllSubmissionDetails(IOrganizationService service, ITracingService tracing,
           Guid employmentId)
        {
            string fetchXml = @"
                    <fetch>
                      <entity name='bpa_submissiondetail' >
                        <attribute name='bpa_name' />
                        <filter>
                          <condition attribute='statecode' operator='eq' value='0' />
                          <condition attribute='bpa_employmentid' operator='eq' value='" + employmentId + @"' />
                        </filter>
                      </entity>
                    </fetch>";
            return service.RetrieveMultiple(new FetchExpression(fetchXml));
            
        }
    }
}