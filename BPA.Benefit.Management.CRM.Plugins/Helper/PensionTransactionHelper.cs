﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Query;

#endregion

namespace BPA.Benefit.Management.CRM.Plugins.Helper
{
    public static class PensionTransactionHelper
    {
        public static Guid Create(IOrganizationService service, ITracingService tracingService, int pensionTransactionCategory, int transactionType, 
            EntityReference memberPlan, Guid periodOfService, Guid fund, string defaultCurrency, EntityReference submission, EntityReference memberAdjustment,
            DateTime workMonth, decimal actualHours, decimal rate, Money dollars, Money grossWages, decimal accrualMultiplier, decimal pensionableHours,
            decimal pensionableContribution )
        {
            tracingService.Trace("PensionTransactionHelper.Create");
            Entity transaction = new Entity(PluginHelper.BpaPensionTransaction)
            {
                ["bpa_name"] = Guid.NewGuid().ToString(),
                ["bpa_memberplanid"] = memberPlan
            };
            if (pensionTransactionCategory == 0)
                pensionTransactionCategory = (int)PensionTransactionCategory.Other;
            transaction["bpa_pensiontransactioncategory"] = new OptionSetValue(pensionTransactionCategory);

            if (transactionType != 0)
                transaction["bpa_pensiontransactiontype"] = new OptionSetValue(transactionType);
           
            if (periodOfService != Guid.Empty)
                transaction["bpa_periodofserviceid"] = new EntityReference(PluginHelper.BpaPeriodOfService, periodOfService);

            if (fund != Guid.Empty)
                transaction["bpa_fundid"] = new EntityReference(PluginHelper.BpaFund, fund);

            if (defaultCurrency != Guid.Empty.ToString())
                transaction["transactioncurrencyid"] = new EntityReference(PluginHelper.Transactioncurrency, new Guid(defaultCurrency));
            if (submission != null)
                transaction["bpa_submissionid"] = submission;
            if (memberAdjustment != null)
                transaction["bpa_memberadjustmentid"] = memberAdjustment;
            
            if (workMonth != DateTime.MinValue)
                transaction["bpa_workmonth"] = workMonth;

            transaction["bpa_actualhours"] = actualHours;
            transaction["bpa_rate"] = rate;
            transaction["bpa_contributionamount"] = dollars;
            if (grossWages != null)
                transaction["bpa_grosswage"] = grossWages;

            if (accrualMultiplier != 0)
                transaction["bpa_accrualmultiplier"] = accrualMultiplier;
            if (pensionableHours != 0)
                transaction["bpa_pensionablehours"] = pensionableHours;
            if (pensionableContribution != 0)
                transaction["bpa_pensionablecontribution"] = new Money(pensionableContribution);

            return service.Create(transaction);
        }

        public static EntityCollection FetchAllTransactionsBySubmissionId(IOrganizationService service, ITracingService tracingService, Guid submissionId)
        {
            EntityCollection transactions = new EntityCollection();

            string fetchXml = $@"
                <fetch version='1.0'>
                  <entity name='bpa_pensiontransaction' >
                    <attribute name='bpa_name' />
                    <attribute name='bpa_memberplanid' />

                    <filter>
                      <condition attribute='statecode' operator='eq' value='0' />
                      <condition attribute='bpa_submissionid' operator='eq' value='{submissionId}' />
                    </filter>
                  </entity>
                </fetch>
            ";
            tracingService.Trace(fetchXml);
            var conversionRequest = new FetchXmlToQueryExpressionRequest
            {
                FetchXml = fetchXml
            };

            var conversionResponse = (FetchXmlToQueryExpressionResponse)service.Execute(conversionRequest);

            // Use the newly converted query expression to make a retrieve multiple
            // request to Microsoft Dynamics CRM.
            QueryExpression queryServicios = conversionResponse.Query;
            int pageNumber = 1;
            RetrieveMultipleRequest multiRequest;
            RetrieveMultipleResponse multiResponse = new RetrieveMultipleResponse();

            do
            {
                queryServicios.PageInfo.Count = 1500;
                queryServicios.PageInfo.PagingCookie = pageNumber == 1
                    ? null
                    : multiResponse.EntityCollection.PagingCookie;
                queryServicios.PageInfo.PageNumber = pageNumber++;

                multiRequest = new RetrieveMultipleRequest();
                multiRequest.Query = queryServicios;
                multiResponse = (RetrieveMultipleResponse)service.Execute(multiRequest);

                transactions.Entities.AddRange(multiResponse.EntityCollection.Entities);
            } while (multiResponse.EntityCollection.MoreRecords);

            return transactions;
        }
    }
}
