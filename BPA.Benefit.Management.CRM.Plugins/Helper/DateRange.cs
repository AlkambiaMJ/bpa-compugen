﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BPA.Benefit.Management.CRM.Plugins.Helper
{
    public class DateRange
    {
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }

        public DateRange() { }
        public DateRange(DateTime Start, DateTime End)
        {
            StartDate = Start;
            EndDate = End;
        }
        
        public static bool CheckGap(List<DateRange> source)
        {
            bool gapFound = false;
            List<DateRange> sortedSource = source.OrderBy(d => d.StartDate).ToList();

            foreach (DateRange dr in sortedSource)
            {
                // Get Current Item Index
                int currentIndex = sortedSource.IndexOf(dr);

                // If Current Item Index is same as last item 
                if (currentIndex == sortedSource.Count - 1)
                    break;
                // Get next item index and next ITem
                int nextIndex = currentIndex + 1;
                DateRange nextItem = sortedSource.ElementAt(nextIndex);

                // Check both start date and end date are not same
                if (nextItem.StartDate != dr.StartDate && nextItem.EndDate != dr.EndDate)
                {
                    // We can also used following 
                    // int diffDay = (nextItem.StartDate - dr.EndDate).Days;

                    // End date +1 day is equal to the next start date
                    if (dr.EndDate.AddDays(1) == nextItem.StartDate )
                        gapFound = false;
                    else
                        gapFound = true;
                }

                if (gapFound)
                    break;
            }

            return gapFound;
        }

    }
}
