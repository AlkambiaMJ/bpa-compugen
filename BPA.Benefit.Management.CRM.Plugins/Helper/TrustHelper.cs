﻿#region

using System;
using System.Collections.Generic;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;

#endregion

// DONE - TODOJK: CLean up commented out code

namespace BPA.Benefit.Management.CRM.Plugins.Helper
{
    //This is the main class for Benefit Trust which contains all method related to 'bpa_trust' entity
    public class TrustHelper
    {

        public static Entity FechAllInformation(IOrganizationService service,ITracingService tracingService, Guid trustId)
        {
            return service.Retrieve(PluginHelper.BpaTrust, trustId, new ColumnSet(true));
        }

        public static List<Entity> FetchAllDefaultFunds(IOrganizationService service, ITracingService tracingService, Guid trustId)
        {
            List<Entity> defaultfunds = new List<Entity>();
            string fetchXml = @"
               <fetch mapping='logical' output-format='xml-platform' version='1.0' >
                  <entity name='bpa_fund' >
                    <attribute name='bpa_defaultfund' />
                    <attribute name='bpa_fundid' />
                    <attribute name='bpa_fundtype' />
                    <filter>
                      <condition attribute='statecode' operator='eq' value='0' />
                      <condition attribute='bpa_defaultfund' operator='eq' value='1' />
                    </filter>
                    <link-entity name='bpa_trust' from='bpa_trustid' to='bpa_trust' >
                      <filter>
                        <condition attribute='bpa_trustid' operator='eq' value='" + trustId + @"' />
                      </filter>
                    </link-entity>
                  </entity>
                </fetch>
            ";

            //<condition attribute='bpa_fundtype' operator='eq' value='922070001' />
            EntityCollection funds = service.RetrieveMultiple(new FetchExpression(fetchXml));
            if (funds == null || funds.Entities.Count <= 0) return defaultfunds;
            tracingService.Trace("RetriveMultiple = " + funds.Entities.Count);
            foreach (Entity e in funds.Entities)
            {
                defaultfunds.Add(e);

                //tracingService.Trace("Fund Name = " + e.GetAttributeValue<EntityReference>("bpa_fundid").Id + " = " + e.GetAttributeValue<EntityReference>("bpa_fundid").Name + " = " + e.GetAttributeValue<OptionSetValue>("bpa_fundtype").Value);
            }
            return defaultfunds;
        }

        public static Guid CreatTrustValidation(IOrganizationService service, ITracingService tracingService, EntityReference trust, int validationType)
        {
            Entity trustValidation = new Entity(PluginHelper.BpaTrustValidation)
            {
                ["bpa_trustid"] = trust,
                ["bpa_validationtype"] = new OptionSetValue(validationType), // Trust Configruation
                ["bpa_startdate"] = DateTime.Now
            };
            return service.Create(trustValidation);
        }

        public static void UpdateTrustValidation(IOrganizationService service, ITracingService tracingService, Guid trustValidationId, string result )
        {
            Entity trustValidation = new Entity(PluginHelper.BpaTrustValidation)
            {
                Id = trustValidationId,
                ["bpa_enddate"] = DateTime.Now,
                ["bpa_trustvalidationresult"] = result
            };
            service.Update(trustValidation);
        }


        public static Guid CreatTrustValidationDetail(IOrganizationService service, ITracingService tracingService, Guid trustValidationId, string primaryEntityType,
            string primaryEntityName, string secondaryEntityType, string secondaryEntityName, string validationType, string validationResult)
        {
            Entity trustValidationDetail = new Entity(PluginHelper.BpaTrustValidationDetail)
            {
                ["bpa_trustvalidationid"] = new EntityReference(PluginHelper.BpaTrustValidation, trustValidationId)
            };

            if (!string.IsNullOrEmpty(primaryEntityType))
                trustValidationDetail["bpa_primaryentitytype"] = primaryEntityType;
            if (!string.IsNullOrEmpty(primaryEntityName))
                trustValidationDetail["bpa_primaryentityname"] = primaryEntityName;

            if (!string.IsNullOrEmpty(secondaryEntityType))
                trustValidationDetail["bpa_secondaryentitytype"] = secondaryEntityType;
            if (!string.IsNullOrEmpty(secondaryEntityName))
                trustValidationDetail["bpa_secondaryentityname"] = secondaryEntityName;

            if (!string.IsNullOrEmpty(validationType))
                trustValidationDetail["bpa_validationtype"] = validationType;
            if (!string.IsNullOrEmpty(validationResult))
                trustValidationDetail["bpa_validationresult"] = validationResult;

            return service.Create(trustValidationDetail);
        }

        public static EntityCollection FetchAllTrustValidationDetail(IOrganizationService service, ITracingService tracingService, Guid trustValidationId, string validationResult)
        {
            string fetchXml = $@"
                <fetch>
                  <entity name='bpa_trustvalidationdetail' >
                    <attribute name='bpa_primaryentityname' />
                    <filter>
                      <condition attribute='bpa_trustvalidationid' operator='eq' value='{trustValidationId}' />
                      <condition attribute='bpa_validationresult' operator='eq' value='{validationResult}' />
                    </filter>
                  </entity>
                </fetch>
            ";

            return service.RetrieveMultiple(new FetchExpression(fetchXml));
        }


        public static EntityCollection FetchAllBenefitByTrustId(IOrganizationService service, ITracingService tracingService, Guid trustId)
        {
            string fetchXml = $@"
                <fetch>
                  <entity name='bpa_benefit' >
                    <attribute name='bpa_name' />
                    <attribute name='ownerid' />
                    <filter>
                      <condition attribute='bpa_trustid' operator='eq' value='{trustId}' />
                      <condition attribute='statecode' operator='eq' value='0' />
                    </filter>
                  </entity>
                </fetch>
            ";

            return service.RetrieveMultiple(new FetchExpression(fetchXml));
        }
    }
}