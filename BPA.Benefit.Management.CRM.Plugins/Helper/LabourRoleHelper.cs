﻿#region

using System;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;

#endregion

namespace BPA.Benefit.Management.CRM.Plugins.Helper
{
    public class LabourRoleHelper
    {
        public static Guid Create(IOrganizationService service, ITracingService tracingService, Guid agreementId, string defaultLabourRole)
        {
            Guid recordId = Guid.Empty;

            // Fetch Default Job Classification from Configuration
            //string defaultLabourRole = ConfigurationHelper.FetchValue(service, tracingService, "Default Job Classification");
            tracingService.Trace($"defaultLabourRole '{defaultLabourRole}'");
            if (!string.IsNullOrEmpty(defaultLabourRole))
            {

                Entity labourrole = new Entity(PluginHelper.BpaLabourrole)
                {
                    ["bpa_agreementid"] = new EntityReference(PluginHelper.BpaSubsector, agreementId),
                    ["bpa_name"] = defaultLabourRole,
                    ["bpa_isdefault"] = true
                };
                recordId =  service.Create(labourrole);
            }
            return recordId;
        }

        public static bool IsDefaultLabourRoleExists(IOrganizationService service, Guid agreementId, bool isFromUpdate,
            Guid labourRoleId)
        {
            bool isExists = false;
            string fetchXml = @"
                <fetch mapping='logical' output-format='xml-platform' version='1.0' >
                  <entity name='bpa_labourrole' >
                    <filter>
                      <condition attribute='bpa_agreementid' operator='eq' value='" + agreementId + @"' />
                      <condition attribute='statecode' operator='eq' value='0' />
                      <condition attribute='bpa_isdefault' operator='eq' value='1' />";

            if (isFromUpdate)
            {
                fetchXml += @" <condition attribute='bpa_labourroleid' operator='neq' value='" + labourRoleId + @"' />";
            }

            fetchXml += @"        </filter>
                  </entity>
                </fetch>
            ";

            EntityCollection funds = service.RetrieveMultiple(new FetchExpression(fetchXml));
            if (funds != null && funds.Entities.Count > 0)
            {
                isExists = true;
            }
            return isExists;
        }

        public static Entity FetchDefaultLabourRole(IOrganizationService service, Guid agreementId)
        {
            Entity defaultRole = null;
            string fetchXml = $@"
                <fetch mapping='logical' output-format='xml-platform' version='1.0' >
                  <entity name='bpa_labourrole' >
                    <filter>
                      <condition attribute='bpa_agreementid' operator='eq' value='{agreementId}' />
                      <condition attribute='statecode' operator='eq' value='0' />
                      <condition attribute='bpa_isdefault' operator='eq' value='1' />
                    </filter>
                  </entity>
                </fetch>
            ";

            EntityCollection roles = service.RetrieveMultiple(new FetchExpression(fetchXml));
            if (roles != null && roles.Entities.Count > 0)
            {
                defaultRole = roles.Entities[0];
            }

            return defaultRole;
        }

        public static EntityCollection FetchAllJobClassificationByAgreementId(IOrganizationService service, ITracingService tracingService, Guid agreementId)
        {
            string fetchXml = $@"
                <fetch mapping='logical' output-format='xml-platform' version='1.0' >
                  <entity name='bpa_labourrole' >
                    <attribute name='bpa_name' />
                    <attribute name='bpa_isdefault' />
                    <attribute name='ownerid' />
                    <filter>
                      <condition attribute='bpa_agreementid' operator='eq' value='{agreementId}' />
                      <condition attribute='statecode' operator='eq' value='0' />
                    </filter>
                  </entity>
                </fetch>
            ";

            return service.RetrieveMultiple(new FetchExpression(fetchXml));
        }

        public static EntityCollection FetchAllRateJobClassificationByJobClassificationId(IOrganizationService service, ITracingService tracingService, Guid jobClassificationId)
        {
            tracingService.Trace("LabourRoleHelper.FetchAllRateJobClassificationByJobClassificationId");
            string fetchXml = $@"
                <fetch version='1.0' >
                  <entity name='bpa_ratelabourrole' >
                    <attribute name='bpa_name' />
                    <filter>
                      <condition attribute='statuscode' operator='eq' value='1' />
                      <condition attribute='bpa_labourroleid' operator='eq' value='{jobClassificationId}' />
                    </filter>
                  </entity>
                </fetch>
            ";
            tracingService.Trace(fetchXml);
            return service.RetrieveMultiple(new FetchExpression(fetchXml));
        }
    }
}