﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Query;

#endregion

namespace BPA.Benefit.Management.CRM.Plugins.Helper
{
    public static class TransactionHelper
    {
        //private static readonly string EntityName = "bpa_transaction";

        public static bool IsMemberPaidLastMonth(IOrganizationService service, Guid contactId, DateTime benefitMonth,
            bool checkNotInBenefit)
        {
            bool allPaid = false;

            QueryExpression queryExpression = new QueryExpression
            {
                EntityName = "bpa_transaction",
                ColumnSet = new ColumnSet(true),
                Criteria =
                {
                    Filters =
                    {
                        new FilterExpression
                        {
                            FilterOperator = LogicalOperator.And,
                            Conditions =
                            {
                                new ConditionExpression("statecode", ConditionOperator.Equal, 0),
                                new ConditionExpression("statuscode", ConditionOperator.Equal, (int)TransactionStatus.Posted),
                                new ConditionExpression("bpa_memberplanid", ConditionOperator.Equal,
                                    contactId.ToString()),
                                new ConditionExpression("bpa_benefitmonth", ConditionOperator.Equal,
                                    benefitMonth.ToShortDateString())
                            }
                        }
                    }
                }
            };

            if (checkNotInBenefit)
                queryExpression.Criteria.AddCondition
                    (
                        new ConditionExpression("bpa_transactiontype",
                            ConditionOperator.In
                            ,
                            new[]
                            {
                                (int)BpaTransactionbpaTransactionType.InBenefit,
                                (int)BpaTransactionbpaTransactionType.SelfPay,
                                (int)BpaTransactionbpaTransactionType.SelfPayInBenefit,
                                (int)BpaTransactionbpaTransactionType.FundAssisted
                            }
                            ));

            EntityCollection transactions = service.RetrieveMultiple(queryExpression);
            if ((transactions != null) && (transactions.Entities.Count > 0))
                allPaid = true;
            return allPaid;
        }

        #region Unpaid Transactions

        public static Guid CreateTransaction(IOrganizationService service, ITracingService trace,
            EntityReference memberPlan, DateTime workMonth, int transactionCategory,
            int transactionType, Guid memberPlanAdjustmentId, decimal hours, Money dollars, EntityReference eligibilityCategory, EntityReference lastAgreement, 
            string defaultCurrency, EntityReference owner)
        {
            Guid recordId = Guid.Empty;
            Entity transaction = new Entity(PluginHelper.BpaTransaction);
            if (transactionCategory != 0)
                transaction["bpa_transactioncategory"] = new OptionSetValue(transactionCategory);
            transaction["bpa_transactiontype"] = new OptionSetValue(transactionType);
            transaction["bpa_memberplanid"] = memberPlan;
            transaction["bpa_memberplanadjustmentid"] = new EntityReference(PluginHelper.BpaMemberPlanadjustment,
                memberPlanAdjustmentId);
            transaction["bpa_transactiondate"] = new DateTime(workMonth.Year, workMonth.Month, 1);
            //string defaultCurrency = ConfigurationHelper.FetchValueId(service, PluginHelper.ConfigDefaultCurrency);
            if (defaultCurrency != Guid.Empty.ToString())
                transaction["transactioncurrencyid"] = new EntityReference("transactioncurrency",
                    new Guid(defaultCurrency));

            if (transactionType != (int)BpaTransactionbpaTransactionType.SecondaryDollarBankAdjustment)
                transaction["bpa_useforcalculation"] = true;

            //Find Default Welfare Fund for MEmber Plan
            EntityReference defaultFund = null;
            int fundType = 0;
            if (transactionType == (int)BpaTransactionbpaTransactionType.VacationPayAdjustment)
                fundType = (int)BpaFundbpaFundType.VacationPay;
            else
                fundType = (int)BpaFundbpaFundType.Welfare;
            defaultFund = ContactHelper.FetchDefaultFundByMemberId(service, trace, memberPlan.Id, fundType);
            if (defaultFund != null)
                transaction["bpa_fund"] = defaultFund;

            if (transactionType == (int)BpaTransactionbpaTransactionType.HourBankAdjustment)
                transaction["bpa_hours"] = hours;
            else
                transaction["bpa_dollarhour"] = dollars;

            //
            if (eligibilityCategory != null)
                transaction["bpa_currenteligibilitycategory"] = eligibilityCategory;

            transaction["bpa_name"] = Guid.NewGuid().ToString();
            if (lastAgreement != null)
                transaction["bpa_subsectorid"] = lastAgreement;

            if(owner != null)
                transaction["ownerid"] = owner;
            recordId = service.Create(transaction);
            return recordId;
        }

        public static List<KeyValuePair<string, DateTime>> FetchAllBenefitTransactions(IOrganizationService service,
            Guid memberId)
        {
            QueryExpression queryExpression = new QueryExpression
            {
                EntityName = PluginHelper.BpaTransaction,
                ColumnSet = new ColumnSet(true),
                Criteria =
                {
                    Filters =
                    {
                        new FilterExpression
                        {
                            FilterOperator = LogicalOperator.And,
                            Conditions =
                            {
                                new ConditionExpression("statecode", ConditionOperator.Equal, 0),
                                new ConditionExpression("statuscode", ConditionOperator.Equal, (int)TransactionStatus.Posted),
                                new ConditionExpression("bpa_memberplanid", ConditionOperator.Equal, memberId.ToString()),
                                new ConditionExpression("bpa_benefitmonth", ConditionOperator.NotNull),
                                new ConditionExpression("bpa_transactiontype", ConditionOperator.In, new[]
                                {
                                    (int)BpaTransactionbpaTransactionType.InBenefit,
                                    (int)BpaTransactionbpaTransactionType.SelfPayInBenefit,
                                    (int)BpaTransactionbpaTransactionType.FundAssisted,
                                    (int)BpaTransactionbpaTransactionType.UnderReview
                                })
                            }
                        }
                    }
                }
            };
            queryExpression.AddOrder("bpa_transactiontype", OrderType.Descending);
            EntityCollection trasactions = service.RetrieveMultiple(queryExpression);

            List<KeyValuePair<string, DateTime>> lsBenefitMonth = new List<KeyValuePair<string, DateTime>>();

            //ICollection<KeyValuePair<String, DateTime>> openWith = new Dictionary<String, DateTime>();

            if (trasactions != null && trasactions.Entities.Count > 0)
            {
                foreach (Entity trans in trasactions.Entities)
                {
                    if (trans.Attributes.Contains("bpa_benefitmonth"))
                    {
                        DateTime benefitMonth = trans.GetAttributeValue<DateTime>("bpa_benefitmonth");

                        lsBenefitMonth.Add(new KeyValuePair<string, DateTime>(benefitMonth.ToShortDateString(),
                            new DateTime(benefitMonth.Year, benefitMonth.Month, benefitMonth.Day)));
                    }
                }
            }

            return lsBenefitMonth;
        }

        #endregion

        #region SELF PAY

        public static bool IsNewMemberExists(IOrganizationService service, Guid memberId, DateTime workMonth)
        {
            bool isexists = false;

            //Get All Contributions for related Submission
            QueryExpression queryExpression = new QueryExpression
            {
                EntityName = PluginHelper.BpaTransaction,
                ColumnSet = new ColumnSet(true),
                Criteria =
                {
                    Filters =
                    {
                        new FilterExpression
                        {
                            FilterOperator = LogicalOperator.And,
                            Conditions =
                            {
                                new ConditionExpression("statecode", ConditionOperator.Equal, 0),
                                new ConditionExpression("statuscode", ConditionOperator.Equal, (int)TransactionStatus.Posted),
                                new ConditionExpression("bpa_transactiontype", ConditionOperator.In,
                                    new[] {(int)BpaTransactionbpaTransactionType.NewMember}),
                                new ConditionExpression("bpa_memberplanid", ConditionOperator.Equal, memberId.ToString()),
                                new ConditionExpression("bpa_transactiondate", ConditionOperator.Equal,
                                    workMonth.ToShortDateString())
                            }
                        }
                    }
                }
            };
            EntityCollection transactions = service.RetrieveMultiple(queryExpression);
            if (transactions != null && transactions.Entities.Count > 0)
                isexists = true;
            else
                isexists = false;

            return isexists;
        }
        
        public static EntityCollection FetchNotInBenefitTransaction(IOrganizationService service, DateTime workMonth,
            DateTime benefitMonth, Guid memberId)
        {
            //Get All Contributions for related Submission
            QueryExpression queryExpression = new QueryExpression
            {
                EntityName = PluginHelper.BpaTransaction,
                ColumnSet = new ColumnSet(true),
                Criteria =
                {
                    Filters =
                    {
                        new FilterExpression
                        {
                            FilterOperator = LogicalOperator.And,
                            Conditions =
                            {
                                new ConditionExpression("statecode", ConditionOperator.Equal, 0),
                                new ConditionExpression("statuscode", ConditionOperator.Equal, (int)TransactionStatus.Posted),
                                new ConditionExpression("bpa_transactiontype", ConditionOperator.In,
                                    new[] {(int)BpaTransactionbpaTransactionType.NotInBenefit}),
                                new ConditionExpression("bpa_memberplanid", ConditionOperator.Equal, memberId.ToString())
                            }
                        }
                    }
                }
            };
            if (benefitMonth != DateTime.MinValue)
                queryExpression.Criteria.AddCondition(new ConditionExpression("bpa_benefitmonth",
                    ConditionOperator.Equal, benefitMonth.ToShortDateString()));
            if (workMonth != DateTime.MinValue)
                queryExpression.Criteria.AddCondition(new ConditionExpression("bpa_transactiondate",
                    ConditionOperator.Equal, workMonth.ToShortDateString()));

            EntityCollection transactions = service.RetrieveMultiple(queryExpression);

            //if ((transactions != null) && (transactions.Entities.Count > 0))
            //    IsInSelfPay = true;
            //return IsInSelfPay;
            return transactions;
        }
        
        public static bool IsMemberNotInBenefitForPreviousMonth(IOrganizationService service, Guid memberId,
            DateTime workMonth)
        {
            bool isNotInBenefit = false;
            string fetchXml = $@"
                    <fetch>
                      <entity name='bpa_transaction' >
                        <attribute name='bpa_transactiontype' />
                        <filter>
                          <condition attribute='statecode' operator='eq' value='0' />
                          <condition attribute='statuscode' operator='eq' value='{(int)TransactionStatus.Posted}' />
                          <condition attribute='bpa_memberplanid' operator='eq' value='{memberId}' />
                          <condition attribute='bpa_transactiondate' operator='eq' value='{workMonth.ToShortDateString()}' />
                          <condition attribute='bpa_transactioncategory' operator='eq' value='{(int)BpaTransactionbpaTransactionCategory.Eligibility}' />
                        </filter>
                      </entity>
                    </fetch>
                ";

            EntityCollection entities = service.RetrieveMultiple(new FetchExpression(fetchXml));

            if ((entities == null) || (entities.Entities.Count == 0))
                isNotInBenefit = true;
            else if (entities != null && entities.Entities.Count > 0)
            {
                Entity e = entities.Entities[0];
                int transactionType = 0;
                if (e.Attributes.Contains("bpa_transactiontype"))
                    transactionType = e.GetAttributeValue<OptionSetValue>("bpa_transactiontype").Value;

                if ((transactionType == (int)BpaTransactionbpaTransactionType.Inactive) ||
                    (transactionType == (int)BpaTransactionbpaTransactionType.ReInstating) ||
                    (transactionType == (int)BpaTransactionbpaTransactionType.NewMember))
                    isNotInBenefit = true;
            }

            return isNotInBenefit;
        }

        public static Guid CreateTransactionWithPayment(IOrganizationService service, ITracingService tracingService, EntityReference member, DateTime workMonth, 
            int transactionCategory, int transactionType, Money amount, EntityReference payment, DateTime paymentDate, Guid fundId)
        {
            Guid recordid = Guid.Empty;
            workMonth = new DateTime(workMonth.Year, workMonth.Month, 1);
            Entity transaction = new Entity(PluginHelper.BpaTransaction);
            transaction["bpa_memberplanid"] = member;
            transaction["bpa_transactiondate"] = workMonth;
            if (transactionCategory != 0)
                transaction["bpa_transactioncategory"] = new OptionSetValue(transactionCategory);
            transaction["bpa_transactiontype"] = new OptionSetValue(transactionType);

            if (transactionType == (int)BpaTransactionbpaTransactionType.SelfPayRefund)
                transaction["bpa_useforcalculation"] = true;

            //if (Amount.Value != 0)
            transaction["bpa_dollarhour"] = amount;
            if (payment != null)
                transaction["bpa_paymentid"] = payment;
            //if (paymentDate != DateTime.MinValue)
            //    transaction["bpa_paymentdate"] = paymentDate;


            transaction["bpa_name"] = Guid.NewGuid().ToString();

            if (fundId != Guid.Empty)
                transaction["bpa_fund"] = new EntityReference(PluginHelper.BpaFund, fundId);

            EntityReference lastAgreement = FetchLastContributionAgreement(service, tracingService, member.Id, workMonth);
            if(lastAgreement != null)
                transaction["bpa_subsectorid"] = lastAgreement;
            recordid = service.Create(transaction);

            return recordid;
        }

        public static Entity FetchLastEligiblityMonthExceptSelfPay(IOrganizationService service, DateTime workMonth,
            Guid memberId)
        {
            Entity entity = null;
            string fetchxml = @"
                    <fetch top='1' >
                      <entity name='bpa_transaction' >
                        <attribute name='bpa_transactiondate' />
                        <attribute name='bpa_transactionid' />
                        <filter type='and' >
                          <condition attribute='bpa_memberplanid' operator='eq' value='" + memberId + @"' />
	                      <condition attribute='bpa_transactiondate' operator='lt' value='" + workMonth.ToShortDateString() +
                              @"' />
                          <condition attribute='statecode' operator='eq' value='0' />
                          <condition attribute='statuscode' operator='eq' value='" + ((int)TransactionStatus.Posted).ToString() + @"' />
                          <condition attribute='bpa_transactiontype' operator='in' >
                            <value>" + (int)BpaTransactionbpaTransactionType.InBenefit + @"</value>
                            <value>" + (int)BpaTransactionbpaTransactionType.NotInBenefit + @"</value>
                            <value>" + (int)BpaTransactionbpaTransactionType.NewMember + @"</value>
                            <value>" + (int)BpaTransactionbpaTransactionType.FundAssistedInBenefit + @"</value>
                            <value>" + (int)BpaTransactionbpaTransactionType.UnderReview + @"</value>
                            <value>" + (int)BpaTransactionbpaTransactionType.ReInstating + @"</value>
                          </condition>
                        </filter>
                        <order attribute='bpa_transactiondate' descending='true' />
                      </entity>
                    </fetch>
                ";
            EntityCollection transactions = service.RetrieveMultiple(new FetchExpression(fetchxml));

            if (transactions != null && transactions.Entities.Count > 0)
            {
                entity = transactions.Entities[0];
            }
            return entity;
        }

        public static bool FetchIsInBenefitTransExists(IOrganizationService service, Guid memberId,
            DateTime benefitMonth)
        {
            bool isExists = false;

            //Get All Contributions for related Submission
            QueryExpression queryExpression = new QueryExpression
            {
                EntityName = "bpa_transaction",
                ColumnSet = new ColumnSet(true),
                Criteria =
                {
                    Filters =
                    {
                        new FilterExpression
                        {
                            FilterOperator = LogicalOperator.And,
                            Conditions =
                            {
                                new ConditionExpression("statecode", ConditionOperator.Equal, 0),
                                new ConditionExpression("statuscode", ConditionOperator.Equal, (int)TransactionStatus.Posted),
                                new ConditionExpression("bpa_memberplanid", ConditionOperator.Equal, memberId.ToString()),
                                new ConditionExpression("bpa_benefitmonth", ConditionOperator.GreaterThan,
                                    benefitMonth.ToShortDateString()),
                                new ConditionExpression("bpa_transactiontype", ConditionOperator.Equal,
                                    (int)BpaTransactionbpaTransactionType.InBenefit)
                            }
                        }
                    }
                }
            };

            EntityCollection transactions = service.RetrieveMultiple(queryExpression);

            if ((transactions != null) && (transactions.Entities.Count > 0))
                isExists = true;
            return isExists;
        }

        #endregion
        
        #region ------- plugin methods --------
        
        public static Entity IsMemberTranferIn(IOrganizationService service, Guid memberId, DateTime workMonth,
            int transactionType)
        {
            Entity isTranferIn = null;
            string fetchxml = @"
                <fetch>
                    <entity name='bpa_transaction' >
                    <attribute name='bpa_transactiondate' />
                    <filter>
                        <condition attribute='statecode' operator='eq' value='0' />
                        <condition attribute='statuscode' operator='eq' value='" + ((int)TransactionStatus.Posted).ToString() + @"' />
                        <condition attribute='bpa_memberplanid' operator='eq' value='" + memberId + @"' />
                        <condition attribute='bpa_transactiondate' operator='ge' value='" +
                              workMonth.ToShortDateString() + @"' />
                        <condition attribute='bpa_transactiontype' operator='eq' value='922070028' />
                    </filter>
                    </entity>
                </fetch>
            ";

            EntityCollection transactions = service.RetrieveMultiple(new FetchExpression(fetchxml));

            if ((transactions != null) && (transactions.Entities.Count > 0))
            {
                isTranferIn = transactions.Entities[0];
            }
            return isTranferIn;
        }

        public static Guid FetchEligibilityTransactionForWorkMonth(IOrganizationService service, DateTime workMonth,
            Guid memberId)
        {
            Guid transactionid = Guid.Empty;
            string fetchxml = @"<fetch >
                <entity name='bpa_transaction' >
                <attribute name='bpa_transactionid' />
                <filter type='and' >
                    <condition attribute='statecode' operator='eq' value='0' />
                    <condition attribute='statuscode' operator='eq' value='" + ((int)TransactionStatus.Posted).ToString() + @"' />
                    <condition attribute='bpa_memberplanid' operator='eq' value='" + memberId + @"' />
                    <condition attribute='bpa_transactiondate' operator='eq' value=' " + workMonth.ToShortDateString() +
                              @"' />
                    <condition attribute='bpa_transactiontype' operator='in' >
	                    <value>" + (int)BpaTransactionbpaTransactionType.InBenefit + @"</value>
	                    <value>" + (int)BpaTransactionbpaTransactionType.NotInBenefit + @"</value>
	                    <value>" + (int)BpaTransactionbpaTransactionType.SelfPayInBenefit + @"</value>
                        <value>" + (int)BpaTransactionbpaTransactionType.NewMember + @"</value>
	                    <value>" + (int)BpaTransactionbpaTransactionType.FundAssistedInBenefit + @"</value>
	                    <value>" + (int)BpaTransactionbpaTransactionType.UnderReview + @"</value>
                        <value>" + (int)BpaTransactionbpaTransactionType.ReInstating + @"</value>
                        <value>" + (int)BpaTransactionbpaTransactionType.Inactive + @"</value>
                        <value>" + (int)BpaTransactionbpaTransactionType.Frozen + @"</value>
                    </condition>
                </filter>
                <order attribute='createdon' descending='true'  />
                </entity>
            </fetch>";
            EntityCollection transactions = service.RetrieveMultiple(new FetchExpression(fetchxml));

            if (transactions != null && transactions.Entities.Count > 0)
            {
                transactionid = transactions.Entities[0].Id;
            }
            return transactionid;
        }

        public static EntityReference FetchEligiblityCategorty(IOrganizationService service, Guid transactionId)
        {
            EntityReference elibiglityCategory = null;
            Entity trans = service.Retrieve(PluginHelper.BpaTransaction, transactionId, new ColumnSet("bpa_currenteligibilitycategory"));

            if (trans != null && trans.Attributes.Contains("bpa_currenteligibilitycategory"))
                elibiglityCategory = trans.GetAttributeValue<EntityReference>("bpa_currenteligibilitycategory");
            return elibiglityCategory;
        }

        public static int FetchPreviousMonthEligiblity(IOrganizationService service, Guid memberId, DateTime workMonth)
        {
            int eligibility = 0;

            string fetchXml = @"
                <fetch>
                    <entity name='bpa_transaction' >
                    <attribute name='bpa_transactiontype' />
                    <filter>
                        <condition attribute='statecode' operator='eq' value='0' />
                        <condition attribute='statuscode' operator='eq' value='" + ((int)TransactionStatus.Posted).ToString() + @"' />
                        <condition attribute='bpa_memberplanid' operator='eq' value='" + memberId + @"' />
                        <condition attribute='bpa_transactiondate' operator='eq' value='" +
                              workMonth.ToShortDateString() + @"' />
                        <condition attribute='bpa_transactiontype' operator='in' >
                            <value>" + ((int)BpaTransactionbpaTransactionType.NotInBenefit).ToString() + @"</value>
                            <value>" + ((int)BpaTransactionbpaTransactionType.ReInstating).ToString() + @"</value>
                        </condition>
                    </filter>
                    </entity>
                </fetch>
            ";

            EntityCollection entityCollection = service.RetrieveMultiple(new FetchExpression(fetchXml));
            if (entityCollection != null && entityCollection.Entities.Count > 0)
            {
                Entity entity = entityCollection.Entities[0];
                if (entity != null)
                {
                    if (entity.Attributes.Contains("bpa_transactiontype"))
                        eligibility = entity.GetAttributeValue<OptionSetValue>("bpa_transactiontype").Value;
                }
            }

            return eligibility;
        }

        #endregion

        #region ------- CREATE ---------

        public static Guid CreateContributionTransaction(IOrganizationService service, ITracingService tracingService, Guid submissionId, Guid memberId,
            DateTime workMonth, DateTime benefitMonth, decimal hours, Money grossWages, Guid labourRoleId, Money dollarHour, Guid fundId, decimal rate, 
            int transactionCategory, int transactionType, EntityReference currentEligibilityCategory, decimal iciHours, decimal noniciHours, 
            decimal vacationAmount, decimal? endingHours, decimal? endingDollars, bool useForCalculation, EntityReference agreement, 
            Guid periodOfServiceId, int transactionStatus, EntityReference owner, Guid submissionDetailId, decimal hoursWorked, decimal hoursEarned)
        {
            Guid transId = Guid.Empty;

            Entity transaction = new Entity(PluginHelper.BpaTransaction);
            transaction["bpa_memberplanid"] = new EntityReference(PluginHelper.BpaMemberplan, memberId);
            if(owner != null)
                transaction["ownerid"] = owner;

            //if (labourRoleId != null && labourRoleId != Guid.Empty)
            //    transaction["bpa_laborrole"] = new EntityReference("bpa_labourrole", labourRoleId);

            if (currentEligibilityCategory != null)
            {
                //transaction["bpa_eligibilitycategory"] = CurrentEligibilityCategory.Name;
                transaction["bpa_currenteligibilitycategory"] = currentEligibilityCategory;
            }
            if (submissionId != null && submissionId != Guid.Empty)
                transaction["bpa_submission"] = new EntityReference(PluginHelper.BpaSubmission, submissionId);

            //WorkMonth = new DateTime(WorkMonth.Year, WorkMonth.Month, 1);
            transaction["bpa_transactiondate"] = workMonth;
            if (benefitMonth != DateTime.MinValue)
            {
                if (benefitMonth.Day > 1)
                    benefitMonth = new DateTime(benefitMonth.Year, benefitMonth.Month, 1);
                //BenefitMonth = new DateTime(BenefitMonth.Year, BenefitMonth.Month, 1);
                transaction["bpa_benefitmonth"] = benefitMonth;
            }
            if (transactionCategory != 0)
                transaction["bpa_transactioncategory"] = new OptionSetValue(transactionCategory);
            if (transactionType != 0)
                transaction["bpa_transactiontype"] = new OptionSetValue(transactionType);

            if (fundId != null && fundId != Guid.Empty)
                transaction["bpa_fund"] = new EntityReference(PluginHelper.BpaFund, fundId);

            if (rate != 0)
                transaction["bpa_rate"] = rate;

            if(endingHours != null)
                transaction["bpa_endinghourbalance"] = endingHours;

            if (endingDollars != null)
                transaction["bpa_endingdollarbalance"] = endingDollars;
            transaction["bpa_useforcalculation"] = useForCalculation;

            if ((int)BpaTransactionbpaTransactionType.VacationContribution == transactionType)
            {
                //transaction["bpa_vacationpayamount"] = new Money(vacationAmount);

                if (grossWages != null && grossWages.Value != 0)
                    transaction["bpa_wages"] = grossWages;

                transaction["bpa_dollarhour"] = new Money(vacationAmount);
            }
            else if ((int)BpaTransactionbpaTransactionType.PrePaidLegal == transactionType)
            {
                transaction["bpa_prepaidlegalamount"] = new Money(vacationAmount);
            }

            //|| ((int)bpa_transactionbpa_TransactionType.SelfPayInBenefit == TransactionType)
            else if (((int)BpaTransactionbpaTransactionType.SelfPay == transactionType) ||
                     ((int)BpaTransactionbpaTransactionType.SelfPayDraw == transactionType))
            {
                //transaction["bpa_transactionpaid"] = true;
                transaction["bpa_dollarhour"] = dollarHour;
            }
            else
            {
                if (hours != 0)
                    transaction["bpa_hours"] = hours;

                if (dollarHour != null && dollarHour.Value != 0)
                    transaction["bpa_dollarhour"] = dollarHour;

                if (grossWages != null && grossWages.Value != 0)
                    transaction["bpa_wages"] = grossWages;

                if (iciHours != 0)
                    transaction["bpa_icihours"] = iciHours;

                if (noniciHours != 0)
                    transaction["bpa_nonicihours"] = noniciHours;
                
                if (hoursWorked != 0)
                    transaction["bpa_hoursworked"] = hoursWorked;

                if (hoursEarned != 0)
                    transaction["bpa_hoursearned"] = hoursEarned;
            }
            transaction["bpa_name"] = Guid.NewGuid().ToString();

            // TFS# 1125 - Populate last agreement from Employer Contribution
            if (transactionType == (int)BpaTransactionbpaTransactionType.SelfPayDeposit)
                agreement = FetchLastContributionAgreement(service, tracingService, memberId, workMonth);
            else if ((transactionType == (int)BpaTransactionbpaTransactionType.PensionContribution) && (periodOfServiceId != Guid.Empty))
                transaction["bpa_periodofserviceid"] = new EntityReference(PluginHelper.BpaPeriodOfService, periodOfServiceId);

            if (agreement != null)
                transaction["bpa_subsectorid"] = agreement;

            transaction["statuscode"] = new OptionSetValue(transactionStatus);

            if(submissionDetailId != Guid.Empty)
                transaction["bpa_submissiondetailid"] = new EntityReference(PluginHelper.BpaSubmissiondetail, submissionDetailId);

            transId = service.Create(transaction);
            return transId;
        }

        //                      <condition attribute='statuscode' operator='eq' value='{(int)TransactionStatus.Posted}' />
        public static EntityCollection FetchAllTransactionsBySubmissionId(IOrganizationService service, ITracingService tracingService, Guid submissionId)
        {
            EntityCollection transactions = new EntityCollection();

            string fetchXml = $@"
                <fetch version='1.0'>
                  <entity name='bpa_transaction' >
                    <attribute name='bpa_name' />
                    <filter>
                      <condition attribute='statecode' operator='eq' value='0' />
                      <condition attribute='bpa_submission' operator='eq' value='{submissionId}' />
                    </filter>
                  </entity>
                </fetch>
            ";

            tracingService.Trace(fetchXml);

            var conversionRequest = new FetchXmlToQueryExpressionRequest
            {
                FetchXml = fetchXml
            };

            var conversionResponse = (FetchXmlToQueryExpressionResponse)service.Execute(conversionRequest);

            // Use the newly converted query expression to make a retrieve multiple
            // request to Microsoft Dynamics CRM.
            QueryExpression queryServicios = conversionResponse.Query;
            int pageNumber = 1;
            RetrieveMultipleRequest multiRequest;
            RetrieveMultipleResponse multiResponse = new RetrieveMultipleResponse();

            do
            {
                queryServicios.PageInfo.Count = 1500;
                queryServicios.PageInfo.PagingCookie = pageNumber == 1
                    ? null
                    : multiResponse.EntityCollection.PagingCookie;
                queryServicios.PageInfo.PageNumber = pageNumber++;

                multiRequest = new RetrieveMultipleRequest();
                multiRequest.Query = queryServicios;
                multiResponse = (RetrieveMultipleResponse)service.Execute(multiRequest);

                transactions.Entities.AddRange(multiResponse.EntityCollection.Entities);
            } while (multiResponse.EntityCollection.MoreRecords);

            return transactions;
        }
        
        public static DateTime GetLastWorkMonth(IOrganizationService service, ITracingService tracingService, Guid employeeId)
        {
            DateTime lastWorkMonth = DateTime.Now;

            string fetchxml = $@"
                <fetch>
                    <entity name='bpa_transaction' >
                    <attribute name='bpa_transactiondate' />
                    <filter type='and' >
                        <condition attribute='statecode' operator='eq' value='0' />
                        <condition attribute='statuscode' operator='eq' value='{(int)TransactionStatus.Posted}' />
                        <condition attribute='bpa_memberplanid' operator='eq' value='" + employeeId + @"' />
                    </filter>
                    <order attribute='bpa_transactiondate' descending='true' />
                    </entity>
                </fetch>
            ";
            tracingService.Trace(fetchxml);
            EntityCollection lasttransactions = service.RetrieveMultiple(new FetchExpression(fetchxml));
            if (lasttransactions == null || lasttransactions.Entities.Count <= 0) return lastWorkMonth;

            //Entity lasttransaction = lasttransactions.Entities.ToList().FirstOrDefault();
            Entity lasttransaction = lasttransactions.Entities[0];
            if (lasttransaction.Attributes.Contains("bpa_transactiondate"))
                lastWorkMonth = lasttransaction.GetAttributeValue<DateTime>("bpa_transactiondate");

            return lastWorkMonth;
        }

        public static EntityCollection FetchAllTransactionsForReciprocal(IOrganizationService service, ITracingService tracingService, Guid contactId, DateTime workMonth)
        {
            string fetchxml = @"
                    <fetch>
                      <entity name='bpa_transaction' >
                        <attribute name='bpa_transactiondate' />
                        <attribute name='bpa_transactionid' />
                        <attribute name='bpa_currenteligibilitycategory' />
                        <attribute name='bpa_reciprocaltransfer' />
                        <attribute name='bpa_transactiontype' />
                        <attribute name='bpa_memberplanadjustmentid' />
                        <filter type='and' >
                          <condition attribute='bpa_memberplanid' operator='eq' value='" + contactId + @"' />
	                      <condition attribute='bpa_transactiondate' operator='eq' value='" + workMonth.ToShortDateString() +
                              @"' />
                          <condition attribute='statecode' operator='eq' value='0' />
                          <condition attribute='statuscode' operator='eq' value='" + (int)TransactionStatus.Posted + @"' />
                          <condition attribute='bpa_transactiontype' operator='in' >
                            <value>" + (int)BpaTransactionbpaTransactionType.EligibilityDraw + @"</value>
                            <value>" + (int)BpaTransactionbpaTransactionType.InBenefit + @"</value>
                            <value>" + (int)BpaTransactionbpaTransactionType.NotInBenefit + @"</value>
                            <value>" + (int)BpaTransactionbpaTransactionType.ReserveTransfer + @"</value>
                            <value>" + (int)BpaTransactionbpaTransactionType.FundAssisted + @"</value>
                            <value>" + (int)BpaTransactionbpaTransactionType.NewMember + @"</value>
                            <value>" + (int)BpaTransactionbpaTransactionType.FundAssistedInBenefit + @"</value>
                            <value>" + (int)BpaTransactionbpaTransactionType.UnderReview + @"</value>
                            <value>" + (int)BpaTransactionbpaTransactionType.Frozen + @"</value>
                            <value>" + (int)BpaTransactionbpaTransactionType.TransferIn + @"</value>
                            <value>" + (int)BpaTransactionbpaTransactionType.TransferOut + @"</value>
                            <value>" + (int)BpaTransactionbpaTransactionType.ReInstating + @"</value>
                            <value>" + (int)BpaTransactionbpaTransactionType.Reciprocal + @"</value>
                            <value>" + (int)BpaTransactionbpaTransactionType.SelfPayInBenefit + @"</value>
                            <value>" + (int)BpaTransactionbpaTransactionType.SelfPayDraw + @"</value>
                            <value>" + (int)BpaTransactionbpaTransactionType.SecondaryDollarBankDraw + @"</value>
                          </condition>
                        </filter>
                        <order attribute='bpa_transactiondate' descending='false' />
                      </entity>
                    </fetch>
                ";
            EntityCollection entityCollection = service.RetrieveMultiple(new FetchExpression(fetchxml));

            return entityCollection;
        }
        
        public static Dictionary<string, decimal> FetchEndingBalanceWhenLateContribution(IOrganizationService service, Guid contactId,
            DateTime benefitMonth)
        {
            Dictionary<string, decimal> endingbalance = new Dictionary<string, decimal>();
            
            decimal hours = 0, dollarhours = 0;
            string fetchXml = $@"
                 <fetch aggregate='true' >
                          <entity name='bpa_transaction' >
                            <attribute name='bpa_hours' alias='bpa_hours' aggregate='sum' />
	                        <attribute name='bpa_dollarhour' alias='bpa_dollarhour' aggregate='sum' />
                            <filter type='and' >
                              <condition attribute='bpa_memberplanid' operator='eq' value='{contactId}' />
                              <condition attribute='statecode' operator='eq' value='0' />
                              <condition attribute='statuscode' operator='eq' value='{(int)TransactionStatus.Posted}' />
                              <condition attribute='bpa_useforcalculation' operator='eq' value='1' />
                              <condition attribute='bpa_transactiondate' operator='le' value='{benefitMonth.AddMonths(1).AddDays(-1).ToShortDateString()}' />
                              <condition attribute='bpa_transactiontype' operator='not-in' >
                                <value>{((int)BpaTransactionbpaTransactionType.SelfPay).ToString()}</value>
                                <value>{((int)BpaTransactionbpaTransactionType.SelfPayBankAdjustment).ToString()}</value>
                                <value>{((int)BpaTransactionbpaTransactionType.SelfPayDeposit).ToString()}</value>
                                <value>{((int)BpaTransactionbpaTransactionType.SelfPayDraw).ToString()}</value>
                                <value>{((int)BpaTransactionbpaTransactionType.SelfPayInBenefit).ToString()}</value>
                                <value>{((int)BpaTransactionbpaTransactionType.SelfPayMaxConsecutiveReach).ToString()}</value>
                                <value>{((int)BpaTransactionbpaTransactionType.SelfPayRefund).ToString()}</value>
                              </condition>
                            </filter>
                          </entity>
                        </fetch>            ";

            EntityCollection sums = service.RetrieveMultiple(new FetchExpression(fetchXml));

            if (sums != null && sums.Entities.Count > 0)
            {
                Entity sum = sums.Entities[0];

                if (sum.Attributes.Contains("bpa_hours") && ((AliasedValue)sum.Attributes["bpa_hours"]).Value != null)
                    hours = (decimal)((AliasedValue)sum.Attributes["bpa_hours"]).Value;

                if (sum.Attributes.Contains("bpa_dollarhour") &&
                    ((AliasedValue)sum.Attributes["bpa_dollarhour"]).Value != null)
                    dollarhours = ((Money)((AliasedValue)sum.Attributes["bpa_dollarhour"]).Value).Value;
            }

            endingbalance.Add("hours", (hours));
            endingbalance.Add("dollarhours", (dollarhours));


            //Self Pay
            fetchXml = $@"
                <fetch aggregate='true' >
                    <entity name='bpa_transaction' >
	                <attribute name='bpa_dollarhour' alias='bpa_dollarhour' aggregate='sum' />
                    <filter type='and' >
                        <condition attribute='bpa_memberplanid' operator='eq' value='{contactId}' />
                        <condition attribute='statecode' operator='eq' value='0' />
                        <condition attribute='statuscode' operator='eq' value='{(int)TransactionStatus.Posted}' />
                        <condition attribute='bpa_transactiondate' operator='le' value='{benefitMonth.AddMonths(1).AddDays(-1).ToShortDateString()}' />
                        <condition attribute='bpa_useforcalculation' operator='eq' value='1' />
                        <condition attribute='bpa_transactiontype' operator='in' >
                        <value>{ ((int)BpaTransactionbpaTransactionType.SelfPay).ToString()}</value>
                        <value>{((int)BpaTransactionbpaTransactionType.SelfPayBankAdjustment).ToString()}</value>
                        <value>{((int)BpaTransactionbpaTransactionType.SelfPayDeposit).ToString()}</value>
                        <value>{((int)BpaTransactionbpaTransactionType.SelfPayDraw).ToString()}</value>
                        <value>{((int)BpaTransactionbpaTransactionType.SelfPayInBenefit).ToString()}</value>
                        <value>{((int)BpaTransactionbpaTransactionType.SelfPayMaxConsecutiveReach).ToString()}</value>
                        <value>{((int)BpaTransactionbpaTransactionType.SelfPayRefund).ToString()}</value>
                        </condition>
                    </filter>
                    </entity>
                </fetch>            ";
            sums = service.RetrieveMultiple(new FetchExpression(fetchXml));

            if (sums != null && sums.Entities.Count > 0)
            {
                Entity sum = sums.Entities[0];
                dollarhours = 0;
                
                if (sum.Attributes.Contains("bpa_dollarhour") && ((AliasedValue)sum.Attributes["bpa_dollarhour"]).Value != null)
                    dollarhours = ((Money)((AliasedValue)sum.Attributes["bpa_dollarhour"]).Value).Value;
                endingbalance.Add("selfpaydollar", dollarhours);
            }

            // secondary dollar bank
            fetchXml = $@"
                <fetch aggregate='true' >
                    <entity name='bpa_transaction' >
	                <attribute name='bpa_dollarhour' alias='bpa_dollarhour' aggregate='sum' />
                    <filter type='and' >
                        <condition attribute='bpa_memberplanid' operator='eq' value='{contactId}' />
                        <condition attribute='statecode' operator='eq' value='0' />
                        <condition attribute='statuscode' operator='eq' value='{(int)TransactionStatus.Posted}' />
                        <condition attribute='bpa_transactiondate' operator='le' value='{benefitMonth.AddMonths(1).AddDays(-1).ToShortDateString()}' />
                        <condition attribute='bpa_transactiontype' operator='in' >
                        <value>{((int)BpaTransactionbpaTransactionType.SecondaryDollarBankAdjustment).ToString()}</value>
                        <value>{((int)BpaTransactionbpaTransactionType.SecondaryDollarBankDraw).ToString()}</value>
                        </condition>
                    </filter>
                    </entity>
                </fetch>            ";
            sums = service.RetrieveMultiple(new FetchExpression(fetchXml));

            if (sums != null && sums.Entities.Count > 0)
            {
                Entity sum = sums.Entities[0];
                dollarhours = 0;

                //if (sum.Attributes.Contains("bpa_dollarhour"))
                if (sum.Attributes.Contains("bpa_dollarhour") &&
                    ((AliasedValue)sum.Attributes["bpa_dollarhour"]).Value != null)
                    dollarhours = ((Money)((AliasedValue)sum.Attributes["bpa_dollarhour"]).Value).Value;
                endingbalance.Add("secondarydollar", dollarhours);
            }
            return endingbalance;
        }

        public static Guid CreateReserveTransaction(IOrganizationService service, ITracingService tracingService, Guid memberId, DateTime workMonth,
            int transactionType, Guid accountId, int transactionCategory, Guid adjustmentId, bool isAmountPaid, DateTime benefitMonth, decimal hours, 
            Money dollars, EntityReference agreement)
        {
            tracingService.Trace("Inside .CreateReserveTransaction Method");
            Guid transactionId = Guid.Empty;
            tracingService.Trace("Aftet FetchEndingBalance Method Called");
            
            Entity rTransaction = new Entity(PluginHelper.BpaTransaction)
            {
                ["bpa_memberplanid"] = new EntityReference(PluginHelper.BpaMemberplan, memberId),
                ["bpa_transactiondate"] = workMonth,
                ["bpa_transactiontype"] = new OptionSetValue(transactionType),
                ["bpa_transactioncategory"] = new OptionSetValue(transactionCategory),
                ["bpa_memberplanadjustmentid"] = new EntityReference(PluginHelper.BpaMemberPlanadjustment, adjustmentId)
            };
            rTransaction["bpa_hours"] = hours * -1;
            rTransaction["bpa_dollarhour"] = new Money(dollars.Value * -1);
            if (accountId != Guid.Empty)
                rTransaction["bpa_unionaccount"] = new EntityReference(PluginHelper.Account, accountId);
            rTransaction["bpa_useforcalculation"] = true;
            if(benefitMonth != DateTime.MinValue)
                rTransaction["bpa_benefitmonth"] = new DateTime(benefitMonth.Year, benefitMonth.Month, 1);
            // Find Default Welfare Fund for MEmber Plan
            EntityReference defaultFund = null;
            defaultFund = ContactHelper.FetchDefaultFundByMemberId(service, memberId) ?? new EntityReference("bpa_fund", Guid.Empty);

            tracingService.Trace($"After defalut fund {defaultFund.ToString()} ");

            if (defaultFund.Id != Guid.Empty)
                rTransaction["bpa_fund"] = defaultFund;

            //Amount Paid?
            if(isAmountPaid)
            {
                tracingService.Trace($"Inside IsAmountPaid ");

                rTransaction["bpa_reciprocaltransfer"] = true;
                rTransaction["bpa_reciprocaltransferdate"] = new DateTime(DateTime.Today.Year, DateTime.Today.Month, DateTime.Today.Day);
            }
            rTransaction["bpa_name"] = Guid.NewGuid().ToString();
            if(agreement != null)
                rTransaction["bpa_subsectorid"] = agreement;
            transactionId = service.Create(rTransaction);

            tracingService.Trace($" Transfer Transaction Created: {transactionId.ToString()} ");

            return transactionId;
        }
        
        public static Guid CreateTransactionReciprocal(IOrganizationService service, ITracingService tracingService, Guid memberId, DateTime workMonth,
            int transactionType, Guid accountId, int reciprocalType, EntityReference currentEligibility, Guid adjustmentId, DateTime benefitMonth,
            Dictionary<string, decimal> endingBalances, EntityReference agreement, Guid submissionId, bool hasEndingBalance, int status)
        {
            Guid transactionId = Guid.Empty;

            if (agreement == null)
                agreement = FetchLastContributionAgreement(service, tracingService, memberId, workMonth);

            if (reciprocalType == (int)MemberPlanBenefitAdjustmentType.TransferReciprocalIn)
            {
                #region ------ Transfer IN ---------------
                Entity transaction = new Entity(PluginHelper.BpaTransaction)
                {
                    ["bpa_memberplanid"] = new EntityReference(PluginHelper.BpaMemberplan, memberId),
                    ["bpa_transactiondate"] = workMonth,
                    ["bpa_transactiontype"] = new OptionSetValue(transactionType)
                };
                if (adjustmentId != Guid.Empty)
                    transaction["bpa_memberplanadjustmentid"] = new EntityReference(PluginHelper.BpaMemberPlanadjustment, adjustmentId);
                if (accountId != Guid.Empty)
                    transaction["bpa_unionaccount"] = new EntityReference(PluginHelper.Account, accountId);
                if (currentEligibility != null)
                    transaction["bpa_currenteligibilitycategory"] = currentEligibility;
                transaction["bpa_useforcalculation"] = true;
                if (benefitMonth != DateTime.MinValue)
                    transaction["bpa_benefitmonth"] = new DateTime(benefitMonth.Year, benefitMonth.Month, 1);
                transaction["bpa_name"] = Guid.NewGuid().ToString();

                if (agreement != null)
                    transaction["bpa_subsectorid"] = agreement;

                if (submissionId != Guid.Empty)
                    transaction["bpa_submission"] = new EntityReference(PluginHelper.BpaSubmission, submissionId);
                transactionId = service.Create(transaction);

                #endregion
            }
            else
            {
                Entity transaction = new Entity(PluginHelper.BpaTransaction)
                {
                    ["bpa_memberplanid"] = new EntityReference(PluginHelper.BpaMemberplan, memberId),
                    ["bpa_transactiondate"] = workMonth,
                    ["bpa_transactiontype"] = new OptionSetValue(transactionType),
                    ["bpa_unionaccount"] = new EntityReference(PluginHelper.Account, accountId)
                };

                if (adjustmentId != Guid.Empty)
                    transaction["bpa_memberplanadjustmentid"] = new EntityReference(PluginHelper.BpaMemberPlanadjustment, adjustmentId);
                if(hasEndingBalance)
                { 
                    if (endingBalances.ContainsKey("hours"))
                        transaction["bpa_endinghourbalance"] = endingBalances["hours"];
                    if (endingBalances.ContainsKey("dollarhours"))
                        transaction["bpa_endingdollarbalance"] = endingBalances["dollarhours"];
                    if (endingBalances.ContainsKey("selfpaydollar"))
                        transaction["bpa_endingselfpaybalance"] = new Money(endingBalances["selfpaydollar"]);
                    if (endingBalances.ContainsKey("selfpaydollar"))
                        transaction["bpa_secondarydollarbalance"] = new Money(endingBalances["secondarydollar"]);
                }
                if (currentEligibility != null)
                    transaction["bpa_currenteligibilitycategory"] = currentEligibility;
                transaction["bpa_useforcalculation"] = true;
                if (benefitMonth != DateTime.MinValue)
                    transaction["bpa_benefitmonth"] = new DateTime(benefitMonth.Year, benefitMonth.Month, 1);
                transaction["bpa_name"] = Guid.NewGuid().ToString();

                if(agreement != null)
                    transaction["bpa_subsectorid"] = agreement;

                if (submissionId != Guid.Empty)
                    transaction["bpa_submission"] = new EntityReference(PluginHelper.BpaSubmission, submissionId);

                if (status == 0)
                    status = (int)TransactionStatus.Posted;

                transaction["statuscode"] = new OptionSetValue(status);

                transactionId = service.Create(transaction);
                
            }
            return transactionId;
        }
        
        public static EntityCollection FetchAllTransactionsByMonth(IOrganizationService service, ITracingService tracingService, Guid contactId, DateTime workMonth)
        {
            string fetchxml = $@"
                    <fetch>
                      <entity name='bpa_transaction' >
                        <attribute name='bpa_transactiondate' />
                        <attribute name='bpa_transactionid' />
                        <attribute name='bpa_transactiontype' />
                        <filter type='and' >
                          <condition attribute='bpa_memberplanid' operator='eq' value='{contactId}' />
	                      <condition attribute='bpa_transactiondate' operator='eq' value='{workMonth.ToShortDateString()}' />
                          <condition attribute='statecode' operator='eq' value='0' />
                          <condition attribute='statuscode' operator='eq' value='{(int)TransactionStatus.Posted}' />
                          <condition attribute='bpa_transactiontype' operator='in' >
                            <value>" + (int)BpaTransactionbpaTransactionType.InBenefit + @"</value>
                            <value>" + (int)BpaTransactionbpaTransactionType.NotInBenefit + @"</value>
                            <value>" + (int)BpaTransactionbpaTransactionType.ReserveTransfer + @"</value>
                            <value>" + (int)BpaTransactionbpaTransactionType.FundAssisted + @"</value>
                            <value>" + (int)BpaTransactionbpaTransactionType.NewMember + @"</value>
                            <value>" + (int)BpaTransactionbpaTransactionType.FundAssistedInBenefit + @"</value>
                            <value>" + (int)BpaTransactionbpaTransactionType.UnderReview + @"</value>
                            <value>" + (int)BpaTransactionbpaTransactionType.Frozen + @"</value>
                            <value>" + (int)BpaTransactionbpaTransactionType.TransferIn + @"</value>
                            <value>" + (int)BpaTransactionbpaTransactionType.TransferOut + @"</value>
                            <value>" + (int)BpaTransactionbpaTransactionType.ReInstating + @"</value>
                            <value>" + (int)BpaTransactionbpaTransactionType.Reciprocal + @"</value>
                            <value>" + (int)BpaTransactionbpaTransactionType.SelfPayInBenefit + @"</value>
                          </condition>
                        </filter>
                      </entity>
                    </fetch>
                ";
            return  service.RetrieveMultiple(new FetchExpression(fetchxml));

        }

        public static Guid CreateTransactionNewMember(IOrganizationService service, ITracingService tracingService, Guid memberId, DateTime workMonth,
            DateTime benefitMonth, int transactionType, Guid accountId, EntityReference currentEligibility)
        {
            if (benefitMonth.Day > 1)
                benefitMonth = new DateTime(benefitMonth.Year, benefitMonth.Month, 1);

            Entity transaction = new Entity(PluginHelper.BpaTransaction)
            {
                ["bpa_memberplanid"] = new EntityReference(PluginHelper.BpaMemberplan, memberId),
                ["bpa_transactiondate"] = workMonth,
                ["bpa_benefitmonth"] = benefitMonth,
                ["bpa_transactiontype"] = new OptionSetValue(transactionType),
                ["bpa_unionaccount"] = new EntityReference(PluginHelper.Account, accountId)
            };
            if (currentEligibility != null)
                transaction["bpa_currenteligibilitycategory"] = currentEligibility;
            transaction["bpa_useforcalculation"] = true;
            transaction["bpa_name"] = Guid.NewGuid().ToString();

            EntityReference lastAgreement = FetchLastContributionAgreement(service, tracingService, memberId, workMonth);
            if (lastAgreement != null)
                transaction["bpa_subsectorid"] = lastAgreement;

            Guid transactionId = service.Create(transaction);

            return transactionId;
        }

        #endregion

        #region --------------- Recalculation Methods --------------

        public static DateTime FetchInactiveWorkMonth(IOrganizationService service, Guid memberId, int transactionType,
            DateTime workMonth)
        {
            DateTime inactiveWorkMonth = DateTime.MinValue;
            string fetchxml = $@"
                <fetch>
                    <entity name='bpa_transaction' >
                    <attribute name='bpa_transactiondate'/>
                    <filter>
                        <condition attribute='statecode' operator='eq' value='0' />
                        <condition attribute='statuscode' operator='eq' value='{(int)TransactionStatus.Posted}' />
                        <condition attribute='bpa_memberplanid' operator='eq' value='" + memberId + @"' />
                        <condition attribute='bpa_transactiontype' operator='eq' value='" + transactionType + @"' />
                        <condition attribute='bpa_transactiondate' operator='ge' value='" +
                              workMonth.ToShortDateString() + @"' />
                    </filter>
                    <order attribute='bpa_transactiondate' descending='true'  />
                    </entity>
                </fetch>
            ";

            EntityCollection entityCollection = service.RetrieveMultiple(new FetchExpression(fetchxml));
            if (entityCollection != null && entityCollection.Entities.Count > 0)
            {
                Entity inactive = entityCollection.Entities[0];
                if (inactive != null)
                {
                    if (inactive.Attributes.Contains("bpa_transactiondate"))
                        inactiveWorkMonth = inactive.GetAttributeValue<DateTime>("bpa_transactiondate");
                }
            }
            return inactiveWorkMonth;
        }

        public static EntityCollection FetchInactiveTransactions(IOrganizationService service, Guid memberId,
            DateTime workMonth)
        {
            string fetchxml = $@"
                <fetch>
                    <entity name='bpa_transaction' >
                    <attribute name='bpa_transactiondate'/>
                    <filter>
                        <condition attribute='statecode' operator='eq' value='0' />
                        <condition attribute='statuscode' operator='eq' value='{(int)TransactionStatus.Posted}' />
                        <condition attribute='bpa_memberplanid' operator='eq' value='" + memberId + @"' />
                        <condition attribute='bpa_transactiontype' operator='in'>
                            <value>" + (int)BpaTransactionbpaTransactionType.Inactive + @"</value>
                            <value>" + (int)BpaTransactionbpaTransactionType.ReserveTransfer + @"</value>
                        </condition>
                        <condition attribute='bpa_transactiondate' operator='ge' value='" +
                              workMonth.ToShortDateString() + @"' />
                    </filter>
                    <order attribute='bpa_transactiondate' descending='true'  />
                    </entity>
                </fetch>
            ";

            return service.RetrieveMultiple(new FetchExpression(fetchxml));
        }
        
        public static bool IsFlatRateTransactionExists(IOrganizationService service, ITracingService tracingService,
            Guid memberPlanId, DateTime workMonth, Guid fundId, Guid employerId)
        {
            tracingService.Trace($"Inside TransactionHelper.IsFlatRateTransactionExists");

            DateTime firstOfMonth = new DateTime(workMonth.Year, workMonth.Month, 1);
            DateTime firstOfNextMonth = new DateTime(workMonth.Year, workMonth.Month, 1).AddMonths(1);
            ;
            DateTime lastOfMonth = firstOfNextMonth.AddDays(-1);
            bool isExist = false;
            string fetchXml =
                $@"
                    <fetch>
                      <entity name='bpa_transaction' >
                        <attribute name='bpa_fund' />
                        <attribute name='bpa_name' />
                        <filter>
                          <condition attribute='statecode' operator='eq' value='0' />
                        <condition attribute='statuscode' operator='eq' value='{(int)TransactionStatus.Posted}' />
                          <condition attribute='bpa_transactiondate' operator='ge' value='{firstOfMonth
                    .ToShortDateString()}' />
                          <condition attribute='bpa_transactiondate' operator='le' value='{lastOfMonth
                        .ToShortDateString()}' />
                          <condition attribute='bpa_memberplanid' operator='eq' value='{memberPlanId}' />
                          <condition attribute='bpa_fund' operator='eq' value='{fundId}' />
                        </filter>
                        <link-entity name='bpa_submission' from='bpa_submissionid' to='bpa_submission' >
                          <filter>
                            <condition attribute='bpa_accountagreementid' operator='eq' value='{employerId}' />
                          </filter>
                        </link-entity>
                      </entity>
                    </fetch>
				";
            tracingService.Trace($"fetch xml \n{fetchXml}");

            EntityCollection transactions = service.RetrieveMultiple(new FetchExpression(fetchXml));

            if (transactions != null && transactions.Entities.Count > 0)
                isExist = true;
            return isExist;
        }
        
        public static decimal FetchSumOfSelfPayBank(IOrganizationService service, ITracingService tracingService, Guid memberId, DateTime WorkMonth)
        {
            decimal sum = 0;
            string fetchXml = $@"
                    <fetch aggregate='true' >
                      <entity name='bpa_transaction' >
	                    <attribute name='bpa_dollarhour' alias='bpa_dollarhour' aggregate='sum' />
                        <filter type='and' >
                          <condition attribute='bpa_memberplanid' operator='eq' value='{memberId}' />
                          <condition attribute='statecode' operator='eq' value='0' />
                          <condition attribute='statuscode' operator='eq' value='{(int)TransactionStatus.Posted}' />
                          <condition attribute='bpa_useforcalculation' operator='eq' value='1' />
                          <condition attribute='bpa_transactiondate' operator='le' value='{WorkMonth.AddMonths(1).AddDays(-1).ToShortDateString()}' />
                          <condition attribute='bpa_transactiontype' operator='in'>
                            <value>{((int)BpaTransactionbpaTransactionType.SelfPayDraw).ToString()}</value>
                            <value>{((int)BpaTransactionbpaTransactionType.SelfPayRefund).ToString()}</value>
                            <value>{((int)BpaTransactionbpaTransactionType.SelfPayMaxConsecutiveReach).ToString()}</value>
                            <value>{((int)BpaTransactionbpaTransactionType.SelfPayDeposit).ToString()}</value>
                            <value>{((int)BpaTransactionbpaTransactionType.SelfPayBankAdjustment).ToString()}</value>
                          </condition>
                        </filter>
                      </entity>
                    </fetch>            ";
            EntityCollection collections = service.RetrieveMultiple(new FetchExpression(fetchXml));

            if (collections != null && collections.Entities.Count > 0)
            {
                Entity entity = collections.Entities[0];

                if (entity.Contains("bpa_dollarhour") && ((AliasedValue)entity.Attributes["bpa_dollarhour"]).Value != null)
                    sum = ((Money)((AliasedValue)entity.Attributes["bpa_dollarhour"]).Value).Value;
            }
            return sum;
        }


        public static decimal GetSumSecondaryBank(IOrganizationService service, ITracingService tracingService, Guid memberId, DateTime WorkMonth)
        {
            decimal sum = 0;
            string fetchXml = $@"
                    <fetch aggregate='true' >
                      <entity name='bpa_transaction' >
	                    <attribute name='bpa_dollarhour' alias='bpa_dollarhour' aggregate='sum' />
                        <filter type='and' >
                          <condition attribute='bpa_memberplanid' operator='eq' value='{memberId}' />
                          <condition attribute='statecode' operator='eq' value='0' />
                          <condition attribute='statuscode' operator='eq' value='{(int)TransactionStatus.Posted}' />
                          <condition attribute='bpa_transactiondate' operator='le' value='{WorkMonth.AddMonths(1).AddDays(-1).ToShortDateString()}' />
                          <condition attribute='bpa_transactiontype' operator='in'>
                            <value>{((int)BpaTransactionbpaTransactionType.SecondaryDollarBankAdjustment).ToString()}</value>
                            <value>{((int)BpaTransactionbpaTransactionType.SecondaryDollarBankDraw).ToString()}</value>
                          </condition>
                        </filter>
                      </entity>
                    </fetch>            ";
            EntityCollection collections = service.RetrieveMultiple(new FetchExpression(fetchXml));

            if (collections != null && collections.Entities.Count > 0)
            {
                Entity entity = collections.Entities[0];

                if (entity.Contains("bpa_dollarhour") && ((AliasedValue)entity.Attributes["bpa_dollarhour"]).Value != null)
                    sum = ((Money)((AliasedValue)entity.Attributes["bpa_dollarhour"]).Value).Value;
            }
            return sum;
        }

        public static DateTime FetchMaxEmployerContributionDate(IOrganizationService service, ITracingService tracingService, Guid MemberId, DateTime WorkMonth)
        {
            DateTime MaxDate = DateTime.MinValue;

            string fetchXml = $@"
                    <fetch aggregate='true' >
                      <entity name='bpa_transaction' >
	                    <attribute name='bpa_transactiondate' alias='bpa_transactiondate' aggregate='Max' />
                        <filter type='and' >
                          <condition attribute='bpa_memberplanid' operator='eq' value='{MemberId}' />
                          <condition attribute='statecode' operator='eq' value='0' />
                          <condition attribute='statuscode' operator='eq' value='{(int)TransactionStatus.Posted}' />
                          <condition attribute='bpa_transactiondate' operator='le' value='{WorkMonth.ToShortDateString()}' />
                          <condition attribute='bpa_transactiontype' operator='in'>
                            <value>{((int)BpaTransactionbpaTransactionType.EmployerContribution).ToString()}</value>
                            <value>{((int)BpaTransactionbpaTransactionType.HourBankAdjustment).ToString()}</value>
                            <value>{((int)BpaTransactionbpaTransactionType.DollarBankAdjustment).ToString()}</value>
                          </condition>
                        </filter>
                      </entity>
                    </fetch>            ";
            EntityCollection collections = service.RetrieveMultiple(new FetchExpression(fetchXml));

            if (collections != null && collections.Entities.Count > 0)
            {
                Entity entity = collections.Entities[0];

                if (entity.Contains("bpa_transactiondate") && ((AliasedValue)entity.Attributes["bpa_transactiondate"]).Value != null)
                    MaxDate = ((DateTime)((AliasedValue)entity.Attributes["bpa_transactiondate"]).Value);
            }

            if (MaxDate == DateTime.MinValue)
                MaxDate = WorkMonth;
            return MaxDate;
        }

        public static void DeleteTransactions(IOrganizationService service, Guid transactionId)
        {
            //Entity trans = new Entity(PluginHelper.BpaTransaction)
            //{
            //    Id = transactionId,
            //    ["bpa_deleteme"] = true
            //};
            //service.Update(trans);
            service.Delete(PluginHelper.BpaTransaction, transactionId);
        }
        public static Entity FetchPreviousMonthEligibility(IOrganizationService service, Guid MemberId, DateTime WorkMonth)
        {
            Entity transaction = null;

            string fetchxml = $@"
                    <fetch>
                      <entity name='bpa_transaction' >
                        <all-attributes/>
                        <filter type='and' >
                          <condition attribute='statecode' operator='eq' value='0' />
                          <condition attribute='statuscode' operator='eq' value='{(int)TransactionStatus.Posted}' />
                          <condition attribute='bpa_memberplanid' operator='eq' value='" + MemberId.ToString() + @"' />
                          <condition attribute='bpa_transactiondate' operator='ge' value='" + WorkMonth.ToShortDateString() + @"' />
                          <condition attribute='bpa_transactiondate' operator='le' value='" + WorkMonth.AddDays(1).ToShortDateString() + @"' />
                          <condition attribute='bpa_transactioncategory' operator='eq' value='" + ((int)BpaTransactionbpaTransactionCategory.Eligibility).ToString() + @"' />
                          <condition attribute='bpa_transactiontype' operator='in' >
                            <value>" + ((int)BpaTransactionbpaTransactionType.InBenefit).ToString() + @"</value>
                            <value>" + ((int)BpaTransactionbpaTransactionType.NotInBenefit).ToString() + @"</value>
                            <value>" + ((int)BpaTransactionbpaTransactionType.SelfPayInBenefit).ToString() + @"</value>
                            <value>" + ((int)BpaTransactionbpaTransactionType.NewMember).ToString() + @"</value>
                            <value>" + ((int)BpaTransactionbpaTransactionType.FundAssistedInBenefit).ToString() + @"</value>
                            <value>" + ((int)BpaTransactionbpaTransactionType.UnderReview).ToString() + @"</value>
                            <value>" + ((int)BpaTransactionbpaTransactionType.TransferOut).ToString() + @"</value>
                            <value>" + ((int)BpaTransactionbpaTransactionType.Frozen).ToString() + @"</value>
                            <value>" + ((int)BpaTransactionbpaTransactionType.ReInstating).ToString() + @"</value>
                          </condition>
                        </filter>
                      </entity>
                    </fetch>
                ";
            EntityCollection transactions = service.RetrieveMultiple(new FetchExpression(fetchxml));
            if (transactions != null && transactions.Entities.Count > 0)
            {
                transaction = transactions.Entities.ToList().FirstOrDefault();
            }
            return transaction;
        }
        
        public static Dictionary<string, decimal> FetchEndingBalanceForReciprocal(IOrganizationService service, Guid contactId, DateTime benefitMonth)
        {
            Dictionary<string, decimal> endingbalance = new Dictionary<string, decimal>();
            //<condition attribute='bpa_reciprocaltransfer' operator='eq' value='1' />
            //<condition attribute='bpa_transactiondate' operator='ge' value='{benefitMonth.ToShortDateString()}' />

            //Total Bank 
            decimal bankHours = 0, bankDollars = 0;
            string fetchXml =$@"
                <fetch aggregate='true' >
                      <entity name='bpa_transaction' >
                        <attribute name='bpa_hours' alias='bpa_hours' aggregate='sum' />
	                    <attribute name='bpa_dollarhour' alias='bpa_dollarhour' aggregate='sum' />
                        <filter type='and' >
                          <condition attribute='bpa_memberplanid' operator='eq' value='{contactId.ToString()}' />
                          <condition attribute='statecode' operator='eq' value='0' />
                          <condition attribute='statuscode' operator='eq' value='{(int)TransactionStatus.Posted}' />
                          <condition attribute='bpa_useforcalculation' operator='eq' value='1' />
                          <condition attribute='bpa_transactiondate' operator='le' value='{benefitMonth.AddMonths(1).AddDays(-1).ToShortDateString()}' />
                          <condition attribute='bpa_transactiontype' operator='not-in' >
                            <value>{((int)BpaTransactionbpaTransactionType.SelfPay).ToString()}</value>
                            <value>{((int)BpaTransactionbpaTransactionType.SelfPayBankAdjustment).ToString()}</value>
                            <value>{((int)BpaTransactionbpaTransactionType.SelfPayDeposit).ToString()}</value>
                            <value>{((int)BpaTransactionbpaTransactionType.SelfPayDraw).ToString()}</value>
                            <value>{((int)BpaTransactionbpaTransactionType.SelfPayInBenefit).ToString()}</value>
                            <value>{((int)BpaTransactionbpaTransactionType.SelfPayMaxConsecutiveReach).ToString()}</value>
                            <value>{((int)BpaTransactionbpaTransactionType.SelfPayRefund).ToString()}</value>
                          </condition>
                        </filter>
                  </entity>
                </fetch>            ";
            EntityCollection collections = service.RetrieveMultiple(new FetchExpression(fetchXml));

            if (collections != null && collections.Entities.Count > 0)
            {
                Entity sum = collections.Entities[0];
                if (sum.Attributes.Contains("bpa_hours") && ((AliasedValue)sum.Attributes["bpa_hours"]).Value != null)
                    bankHours = (decimal)((AliasedValue)sum.Attributes["bpa_hours"]).Value;
                if (sum.Attributes.Contains("bpa_dollarhour") &&
                    ((AliasedValue)sum.Attributes["bpa_dollarhour"]).Value != null)
                    bankDollars = ((Money)((AliasedValue)sum.Attributes["bpa_dollarhour"]).Value).Value;
            }
            
            endingbalance.Add("hours", (bankHours));
            endingbalance.Add("dollarhours", (bankDollars));
            
            // secondary dollar bank
            fetchXml = $@"
                <fetch aggregate='true' >
                    <entity name='bpa_transaction' >
	                <attribute name='bpa_dollarhour' alias='bpa_dollarhour' aggregate='sum' />
                    <filter type='and' >
                        <condition attribute='bpa_memberplanid' operator='eq' value='{contactId}' />
                        <condition attribute='statecode' operator='eq' value='0' />
                        <condition attribute='statuscode' operator='eq' value='{(int)TransactionStatus.Posted}' />
                        <condition attribute='bpa_transactiondate' operator='le' value='{benefitMonth.AddMonths(1).AddDays(-1).ToShortDateString()}' />
                        <condition attribute='bpa_transactiontype' operator='in' >
                        <value>{((int)BpaTransactionbpaTransactionType.SecondaryDollarBankAdjustment).ToString()}</value>
                        <value>{((int)BpaTransactionbpaTransactionType.SecondaryDollarBankDraw).ToString()}</value>
                        </condition>
                    </filter>
                    </entity>
                </fetch>            ";
            EntityCollection sums = service.RetrieveMultiple(new FetchExpression(fetchXml));

            if (sums != null && sums.Entities.Count > 0)
            {
                Entity sum = sums.Entities[0];
                decimal dollarhours = 0;

                //if (sum.Attributes.Contains("bpa_dollarhour"))
                if (sum.Attributes.Contains("bpa_dollarhour") &&
                    ((AliasedValue)sum.Attributes["bpa_dollarhour"]).Value != null)
                    dollarhours = ((Money)((AliasedValue)sum.Attributes["bpa_dollarhour"]).Value).Value;
                endingbalance.Add("secondarydollar", dollarhours);
            }
            return endingbalance;
        }

        public static EntityCollection FetchAllTransactionByAdjustmentId(IOrganizationService service, ITracingService tracingService, Guid ajustmentId, DateTime WorkMonth)
        {
            string fetchxml = $@"
                    <fetch>
                      <entity name='bpa_transaction' >
                        <attribute name='bpa_name' />
                        <filter type='and' >
                          <condition attribute='statecode' operator='eq' value='0' />
                          <condition attribute='statuscode' operator='eq' value='{(int)TransactionStatus.Posted}' />
                          <condition attribute='bpa_memberplanadjustmentid' operator='eq' value='{ajustmentId.ToString()}' />
                          <condition attribute='bpa_transactiondate' operator='ge' value='{WorkMonth.ToShortDateString()}' />
                        </filter>
                      </entity>
                    </fetch>
                ";
            return service.RetrieveMultiple(new FetchExpression(fetchxml));
        }
 
        public static EntityReference FetchCurrentEligibilityCategory(IOrganizationService service, ITracingService tracingService, Guid memberPlanId, DateTime workMonth)
        {
            EntityReference eligibilityCategory = null;
            workMonth = new DateTime(workMonth.Year, workMonth.Month, 1);
            DateTime endWorkMonth = new DateTime(workMonth.Year, workMonth.Month, 1).AddMonths(1);

            //Check does anythng exists for that workmonth
            string fetchXml = $@"
                <fetch>
                  <entity name='bpa_transaction' >
                    <attribute name='bpa_currenteligibilitycategory' />
                    <attribute name='createdon' />
                    <filter type='and' >
                      <condition attribute='bpa_memberplanid' operator='eq' value='{memberPlanId}' />
                      <condition attribute='statecode' operator='eq' value='0' />
                      <condition attribute='statuscode' operator='eq' value='{(int)TransactionStatus.Posted}' />
                      <condition attribute='bpa_transactiondate' operator='ge' value='{workMonth.ToShortDateString()}' />
                      <condition attribute='bpa_transactiondate' operator='lt' value='{endWorkMonth.ToShortDateString()}' />
                      <condition attribute='bpa_currenteligibilitycategory' operator='not-null' />
                    </filter>
                    <order attribute='createdon' />
                  </entity>
                </fetch>           ";
            EntityCollection collections = service.RetrieveMultiple(new FetchExpression(fetchXml));

            if (collections != null && collections.Entities.Count > 0)
            {
                Entity entity = collections.Entities[0];
                eligibilityCategory = entity.Contains("bpa_currenteligibilitycategory") ? entity.GetAttributeValue<EntityReference>("bpa_currenteligibilitycategory") : null;
            }

            //Check is there anything exists in future?
            if (eligibilityCategory == null)
            {
                fetchXml = $@"
                <fetch>
                  <entity name='bpa_transaction' >
                    <attribute name='bpa_currenteligibilitycategory' />
                    <attribute name='createdon' />
                    <filter type='and' >
                      <condition attribute='bpa_memberplanid' operator='eq' value='{memberPlanId}' />
                      <condition attribute='statecode' operator='eq' value='0' />
                      <condition attribute='statuscode' operator='eq' value='{(int)TransactionStatus.Posted}' />
                      <condition attribute='bpa_transactiondate' operator='ge' value='{workMonth.ToShortDateString()}' />
                      <condition attribute='bpa_currenteligibilitycategory' operator='not-null' />
                    </filter>
                    <order attribute='createdon' />
                  </entity>
                </fetch>           ";
                collections = service.RetrieveMultiple(new FetchExpression(fetchXml));

                if (collections != null && collections.Entities.Count > 0)
                {
                    Entity entity = collections.Entities[0];
                    eligibilityCategory = entity.Contains("bpa_currenteligibilitycategory") ? entity.GetAttributeValue<EntityReference>("bpa_currenteligibilitycategory") : null;
                }
            }
            //NOTHING EXISTS - TAKE MEMER CURRENT eligiblity Category
            if (eligibilityCategory == null)
            {
                Entity memberplan = service.Retrieve(PluginHelper.BpaMemberplan, memberPlanId, new ColumnSet(new string[] { "bpa_eligibilitycategoryid" }));

                eligibilityCategory = memberplan.Contains("bpa_eligibilitycategoryid") ? memberplan.GetAttributeValue<EntityReference>("bpa_eligibilitycategoryid") : null;
            }


            return eligibilityCategory;
        }
        #endregion

        public static EntityCollection FetchAllMemberVacationPayByTrustId(IOrganizationService service, ITracingService tracingService, DateTime endWorkMonth, Guid trustId)
        {

            // Define the fetch attributes.
            // Set the number of records per page to retrieve.
            int fetchCount = 4999;
            // Initialize the page number.
            int pageNumber = 1;
            // Specify the current paging cookie. For retrieving the first page, 
            // pagingCookie should be null.
            string pagingCookie = null;

            EntityCollection collection = new EntityCollection();
            string fetchXml = $@"
                <fetch aggregate='true' mapping='logical' output-format='xml-platform' version='1.0'>
                  <entity name='bpa_transaction' >
                    <attribute name='bpa_dollarhour' alias='bpa_dollarhour' aggregate='sum' />
                    <attribute name='bpa_memberplanid' alias='bpa_memberplanid' groupby='true' />
                    <filter>
                      <condition attribute='statecode' operator='eq' value='0' />
                      <condition attribute='statuscode' operator='eq' value='{(int)TransactionStatus.Posted}' />
                      <condition attribute='bpa_transactiondate' operator='lt' value='{endWorkMonth.ToShortDateString()}' />
                      <condition attribute='bpa_transactiontype' operator='in' >
                        <value>{(int)BpaTransactionbpaTransactionType.VacationContribution}</value>
                        <value>{(int)BpaTransactionbpaTransactionType.VacationDraw}</value>
                        <value>{(int)BpaTransactionbpaTransactionType.VacationPayAdjustment}</value>
                        <value>{(int)BpaTransactionbpaTransactionType.AnnualVacationDraw}</value>
                      </condition>
                      <condition attribute='bpa_paymentid' operator='null' />
                    </filter>
                    <link-entity name='bpa_memberplan' from='bpa_memberplanid' to='bpa_memberplanid' >
                      <attribute name='bpa_planid' alias='bpa_planid' groupby='true' />
                      <link-entity name='bpa_sector' from='bpa_sectorid' to='bpa_planid' >
                        <filter>
                          <condition attribute='bpa_trust' operator='eq' value='{trustId.ToString()}' />
                        </filter>
                      </link-entity>
                    </link-entity>
                  </entity>
                </fetch>            ";
            tracingService.Trace($"fetch Query:\n {fetchXml}");


            while (true)
            {
                // Build fetchXml string with the placeholders.
                string xml = PluginHelper.CreateXml(fetchXml, pagingCookie, pageNumber, fetchCount);

                // Excute the fetch query and get the xml result.
                RetrieveMultipleRequest fetchRequest1 = new RetrieveMultipleRequest
                {
                    Query = new FetchExpression(xml)
                };

                EntityCollection returnCollection = ((RetrieveMultipleResponse)service.Execute(fetchRequest1)).EntityCollection;
                tracingService.Trace($"Adding into EntityCollection");
                collection.Entities.AddRange(returnCollection.Entities);
                tracingService.Trace($"Added into EntityCollection");

                // Check for morerecords, if it returns 1.
                if (returnCollection.MoreRecords)
                {
                    // Increment the page number to retrieve the next page.
                    pageNumber++;

                    // Set the paging cookie to the paging cookie returned from current results.                            
                    pagingCookie = returnCollection.PagingCookie;
                }
                else
                {
                    // If no more records in the result nodes, exit the loop.
                    break;
                }
            }
            
            return collection;

        }
        public static EntityCollection FetchAllVacationTransactionByMemberPlanId(IOrganizationService service, ITracingService tracingService, DateTime endWorkMonth, Guid memberPlanId)
        {
            EntityCollection collection = new EntityCollection();
            string fetchXml = $@"
                <fetch>
                  <entity name='bpa_transaction' >
                    <attribute name='bpa_name' />
                    <filter>
                      <condition attribute='statecode' operator='eq' value='0' />
                      <condition attribute='statuscode' operator='eq' value='{(int)TransactionStatus.Posted}' />
                      <condition attribute='bpa_transactiondate' operator='lt' value='{endWorkMonth.ToShortDateString()}' />
                      <condition attribute='bpa_memberplanid' operator='eq' value='{memberPlanId.ToString()}' />
                      <condition attribute='bpa_transactiontype' operator='in' >
                        <value>{(int)BpaTransactionbpaTransactionType.VacationContribution}</value>
                        <value>{(int)BpaTransactionbpaTransactionType.VacationDraw}</value>
                        <value>{(int)BpaTransactionbpaTransactionType.VacationPayAdjustment}</value>
                        <value>{(int)BpaTransactionbpaTransactionType.AnnualVacationDraw}</value>
                      </condition>
                      <condition attribute='bpa_paymentid' operator='null' />
                    </filter>
                  </entity>
                </fetch>
            ";
            tracingService.Trace($"fetch Query:\n {fetchXml}");

            FetchXmlToQueryExpressionRequest conversionRequest = new FetchXmlToQueryExpressionRequest
            {
                FetchXml = fetchXml
            };

            FetchXmlToQueryExpressionResponse conversionResponse = (FetchXmlToQueryExpressionResponse)service.Execute(conversionRequest);

            // Use the newly converted query expression to make a retrieve multiple
            // request to Microsoft Dynamics CRM.
            QueryExpression queryServicios = conversionResponse.Query;
            int pageNumber = 1;
            RetrieveMultipleResponse multiResponse = new RetrieveMultipleResponse();
            tracingService.Trace($"Declared RetrieveMultipleResponse");


            //Makeing loop if record cound is more than 5000 and add into collection
            do
            {
                queryServicios.PageInfo.Count = 5000;
                queryServicios.PageInfo.PagingCookie = (pageNumber == 1) ? null : multiResponse.EntityCollection.PagingCookie;
                queryServicios.PageInfo.PageNumber = pageNumber++;

                RetrieveMultipleRequest multiRequest = new RetrieveMultipleRequest { Query = queryServicios };
                tracingService.Trace($"Make Request");
                multiResponse = (RetrieveMultipleResponse)service.Execute(multiRequest);

                tracingService.Trace($"Adding into EntityCollection");
                collection.Entities.AddRange(multiResponse.EntityCollection.Entities);
                tracingService.Trace($"Added into EntityCollection");
            } while (multiResponse.EntityCollection.MoreRecords);

            return collection;

        }


        public static void UpdatePayment(IOrganizationService service, ITracingService trackingService, Guid transactionId, Guid paymentId)
        {
            Entity transaction = new Entity(PluginHelper.BpaTransaction)
            {
                Id = transactionId,
            };

            if (paymentId != Guid.Empty)
                transaction["bpa_paymentid"] = new EntityReference(PluginHelper.BpaPayment, paymentId);
            else
                transaction["bpa_paymentid"] = null;

            service.Update(transaction);
        }

        public static EntityCollection FetchAllVacationTransactionsByPaymentId(IOrganizationService service, ITracingService tracingService, Guid paymentId)
        {
            EntityCollection collection = new EntityCollection();
            string fetchXml = $@"
                <fetch>
                  <entity name='bpa_transaction' >
                    <attribute name='bpa_name' />
                    <filter>
                      <condition attribute='statuscode' operator='eq' value='{(int)TransactionStatus.Posted}' />
                      <condition attribute='statecode' operator='eq' value='0' />
                      <condition attribute='bpa_paymentid' operator='eq' value='{paymentId.ToString()}' />
                    </filter>
                  </entity>
                </fetch>
            ";
            tracingService.Trace($"fetch Query:\n {fetchXml}");

            FetchXmlToQueryExpressionRequest conversionRequest = new FetchXmlToQueryExpressionRequest
            {
                FetchXml = fetchXml
            };

            FetchXmlToQueryExpressionResponse conversionResponse = (FetchXmlToQueryExpressionResponse)service.Execute(conversionRequest);

            // Use the newly converted query expression to make a retrieve multiple
            // request to Microsoft Dynamics CRM.
            QueryExpression queryServicios = conversionResponse.Query;
            int pageNumber = 1;
            RetrieveMultipleResponse multiResponse = new RetrieveMultipleResponse();
            tracingService.Trace($"Declared RetrieveMultipleResponse");


            //Makeing loop if record cound is more than 5000 and add into collection
            do
            {
                queryServicios.PageInfo.Count = 5000;
                queryServicios.PageInfo.PagingCookie = (pageNumber == 1) ? null : multiResponse.EntityCollection.PagingCookie;
                queryServicios.PageInfo.PageNumber = pageNumber++;

                RetrieveMultipleRequest multiRequest = new RetrieveMultipleRequest { Query = queryServicios };
                tracingService.Trace($"Make Request");
                multiResponse = (RetrieveMultipleResponse)service.Execute(multiRequest);

                tracingService.Trace($"Adding into EntityCollection");
                collection.Entities.AddRange(multiResponse.EntityCollection.Entities);
                tracingService.Trace($"Added into EntityCollection");
            } while (multiResponse.EntityCollection.MoreRecords);

            return collection;

        }


        public static EntityReference FetchLastContributionAgreement(IOrganizationService service, ITracingService tracingService, Guid memberPlanId, DateTime workMonth)
        {
            EntityReference lastAgreement = null;
            string fetchXml = $@"
                <fetch version='1.0' >
                  <entity name='bpa_transaction' >
                    <attribute name='bpa_submission' />
                    <filter>
                      <condition attribute='statecode' operator='eq' value='0' />
                      <condition attribute='statuscode' operator='eq' value='{(int)TransactionStatus.Posted}' />
                      <condition attribute='bpa_transactiontype' operator='in' >
                        <value>{(int)BpaTransactionbpaTransactionType.EmployerContribution}</value>
                      </condition>
                      <condition attribute='bpa_submission' operator='not-null' />
                      <condition attribute='bpa_memberplanid' operator='eq' value='{memberPlanId}' />
                      <condition attribute='bpa_transactiondate' operator='le' value='{workMonth.ToShortDateString()}' />
                    </filter>
                    <order attribute='bpa_transactiondate' descending='true' />
                    <order attribute='bpa_posteddate' descending='true' />
                    <link-entity name='bpa_submission' from='bpa_submissionid' to='bpa_submission' >
                      <attribute name='bpa_agreementid' />
                    </link-entity>
                  </entity>
                </fetch>
            ";
            /*
                <value>{(int)BpaTransactionbpaTransactionType.SelfPayDeposit}</value>
                <value>{(int)BpaTransactionbpaTransactionType.SelfPay}</value>
             */
            EntityCollection collection = service.RetrieveMultiple(new FetchExpression(fetchXml));

            if(collection != null && collection.Entities.Count > 0)
            {
                Entity entity = collection.Entities[0];
                if (entity.Contains("bpa_submission1.bpa_agreementid"))
                    lastAgreement = (EntityReference)entity.GetAttributeValue<AliasedValue>("bpa_submission1.bpa_agreementid").Value;
            }

            return lastAgreement;
        }

        public static bool IsMemberNotInBenefitPreviousMonth(IOrganizationService service, Guid MemberId, DateTime WorkMonth)
        {
            bool IsNotInBenefit = false;
            DateTime startDate = new DateTime(WorkMonth.Year, WorkMonth.Month, 1).AddMonths(-1);
            DateTime endDate = new DateTime(WorkMonth.Year, WorkMonth.Month, 1);
            string fetchxml =$@"
                    <fetch top='10'>
                      <entity name='bpa_transaction' >
                        <attribute name='bpa_transactiontype' />
                        <filter type='and' >
                          <condition attribute='statecode' operator='eq' value='0' />
                          <condition attribute='statuscode' operator='eq' value='{(int)TransactionStatus.Posted}' />
                          <condition attribute='bpa_memberplanid' operator='eq' value='" + MemberId.ToString() + @"' />
                          <condition attribute='bpa_transactiondate' operator='ge' value='" + startDate.ToShortDateString() + @"' />
                          <condition attribute='bpa_transactiondate' operator='lt' value='" + endDate.ToShortDateString() + @"' />
                          <condition attribute='bpa_transactioncategory' operator='eq' value='" + ((int)BpaTransactionbpaTransactionCategory.Eligibility).ToString() + @"' />
                          <condition attribute='bpa_transactiontype' operator='in' >
                            <value>" + ((int)BpaTransactionbpaTransactionType.InBenefit).ToString() + @"</value>
                            <value>" + ((int)BpaTransactionbpaTransactionType.SelfPayInBenefit).ToString() + @"</value>
                            <value>" + ((int)BpaTransactionbpaTransactionType.FundAssistedInBenefit).ToString() + @"</value>
                          </condition>
                        </filter>
                        <order attribute='createdon' descending='true' />
                      </entity>
                    </fetch>
                ";

            /*
                <value>" + ((int)BpaTransactionbpaTransactionType.NewMember).ToString() + @"</value>
                <value>" + ((int)BpaTransactionbpaTransactionType.NotInBenefit).ToString() + @"</value>
                <value>" + ((int)BpaTransactionbpaTransactionType.FundAssistedInBenefit).ToString() + @"</value>
                <value>" + ((int)BpaTransactionbpaTransactionType.UnderReview).ToString() + @"</value>
                <value>" + ((int)BpaTransactionbpaTransactionType.TransferOut).ToString() + @"</value>
                <value>" + ((int)BpaTransactionbpaTransactionType.Frozen).ToString() + @"</value>
                <value>" + ((int)BpaTransactionbpaTransactionType.ReInstating).ToString() + @"</value>
                <value>" + ((int)BpaTransactionbpaTransactionType.Inactive).ToString() + @"</value> 
             */
            EntityCollection transactions = service.RetrieveMultiple(new FetchExpression(fetchxml));

            if (transactions == null)
                IsNotInBenefit = true;
            else if (transactions != null && transactions.Entities.Count > 0)
                IsNotInBenefit = false;
            else if (transactions != null && transactions.Entities.Count == 0)
                IsNotInBenefit = true;

            return IsNotInBenefit;
        }

        public static EntityCollection FetchAllTransactionsByPaymentId(IOrganizationService service, ITracingService tracingService, Guid paymentId)
        {
            string fetchxml = $@"
                    <fetch>
                      <entity name='bpa_transaction' >
                        <attribute name='bpa_name' />
                        <filter type='and' >
                            <condition attribute='statecode' operator='eq' value='0' />
                            <condition attribute='statuscode' operator='eq' value='{(int)TransactionStatus.Posted}' />
                            <condition attribute='bpa_paymentid' operator='eq' value='{paymentId.ToString()}' />
                        </filter>
                        <order attribute='createdon' descending='true' />
                      </entity>
                    </fetch>
                ";

            return service.RetrieveMultiple(new FetchExpression(fetchxml));
        }


        public static EntityCollection FetchReserverTransaction(IOrganizationService service, ITracingService tracingService, Guid memberPlanId, DateTime workMonth)
        {

            string fetchXml = $@"
                <fetch>
                    <entity name='bpa_transaction' >
                    <attribute name='bpa_name'/>
                    <filter type='and' >
                        <condition attribute='bpa_memberplanid' operator='eq' value='{memberPlanId}' />
                        <condition attribute='statecode' operator='eq' value='0' />
                        <condition attribute='bpa_transactiondate' operator='ge' value='{workMonth.ToShortDateString() }' />
                        <condition attribute='bpa_transactiondate' operator='lt' value='{workMonth.AddMonths(1).ToShortDateString() }' />
                        <condition attribute='statuscode' operator='eq' value='{(int)TransactionStatus.Posted}' />
                        <condition attribute='bpa_transactiontype' operator='in' >
                            <value>{((int)BpaTransactionbpaTransactionType.Inactive).ToString()}</value>
                            <value>{((int)BpaTransactionbpaTransactionType.InBenefit).ToString()}</value>
                            <value>{((int)BpaTransactionbpaTransactionType.NotInBenefit).ToString()}</value>
                            <value>{((int)BpaTransactionbpaTransactionType.FundAssistedInBenefit).ToString()}</value>
                            <value>{((int)BpaTransactionbpaTransactionType.SelfPayInBenefit).ToString()}</value>
                            <value>{((int)BpaTransactionbpaTransactionType.UnderReview).ToString()}</value>
                            <value>{((int)BpaTransactionbpaTransactionType.Frozen).ToString()}</value>
                            <value>{((int)BpaTransactionbpaTransactionType.NewMember).ToString()}</value>
                            <value>{((int)BpaTransactionbpaTransactionType.ReInstating).ToString()}</value>
                            <value>{((int)BpaTransactionbpaTransactionType.TransferIn).ToString()}</value>
                            <value>{((int)BpaTransactionbpaTransactionType.TransferOut).ToString()}</value>
                            <value>{((int)BpaTransactionbpaTransactionType.Reciprocal).ToString()}</value>
                        </condition>
                    </filter>
                    </entity>
                </fetch>            ";
            tracingService.Trace(fetchXml);
            return service.RetrieveMultiple(new FetchExpression(fetchXml));
        }

        public static Dictionary<string, decimal> FetchEndingBalance(IOrganizationService service, ITracingService tracingService, Guid contactId, DateTime benefitMonth)
        {
            Dictionary<string, decimal> endingbalance = new Dictionary<string, decimal>();

            string fetchXml = $@"
                <fetch aggregate='true' >
                    <entity name='bpa_transaction' >
                    <attribute name='bpa_hours' alias='bpa_hours' aggregate='sum' />
	                <attribute name='bpa_dollarhour' alias='bpa_dollarhour' aggregate='sum' />
                    <filter type='and' >
                        <condition attribute='bpa_memberplanid' operator='eq' value='{contactId}' />
                        <condition attribute='bpa_transactiondate' operator='le' value='{benefitMonth.AddMonths(1).AddDays(-1).ToShortDateString() }' />
                        <condition attribute='bpa_useforcalculation' operator='eq' value='1' />
                        <condition attribute='statuscode' operator='eq' value='{(int)TransactionStatus.Posted}' />
                        <condition attribute='bpa_transactiontype' operator='not-in' >
                            <value>{((int)BpaTransactionbpaTransactionType.SelfPay).ToString()}</value>
                            <value>{((int)BpaTransactionbpaTransactionType.SelfPayBankAdjustment).ToString()}</value>
                            <value>{((int)BpaTransactionbpaTransactionType.SelfPayDeposit).ToString()}</value>
                            <value>{((int)BpaTransactionbpaTransactionType.SelfPayDraw).ToString()}</value>
                            <value>{((int)BpaTransactionbpaTransactionType.SelfPayInBenefit).ToString()}</value>
                            <value>{((int)BpaTransactionbpaTransactionType.SelfPayMaxConsecutiveReach).ToString()}</value>
                            <value>{((int)BpaTransactionbpaTransactionType.SelfPayRefund).ToString()}</value>
                        </condition>
                    </filter>
                    </entity>
                </fetch>            ";
            tracingService.Trace(fetchXml);
            EntityCollection sums = service.RetrieveMultiple(new FetchExpression(fetchXml));

            if (sums != null && sums.Entities.Count > 0)
            {
                Entity sum = sums.Entities[0];
                decimal hours = 0, dollarhours = 0;

                //if (sum.Attributes.Contains("bpa_hours"))
                if (sum.Attributes.Contains("bpa_hours") && ((AliasedValue)sum.Attributes["bpa_hours"]).Value != null)
                    hours = (decimal)((AliasedValue)sum.Attributes["bpa_hours"]).Value;
                endingbalance.Add("hours", hours);

                //if (sum.Attributes.Contains("bpa_dollarhour"))
                if (sum.Attributes.Contains("bpa_dollarhour") &&
                    ((AliasedValue)sum.Attributes["bpa_dollarhour"]).Value != null)
                    dollarhours = ((Money)((AliasedValue)sum.Attributes["bpa_dollarhour"]).Value).Value;
                endingbalance.Add("dollarhours", dollarhours);
            }

            //"selfpay")
            fetchXml = $@"
                <fetch aggregate='true' >
                    <entity name='bpa_transaction' >
	                <attribute name='bpa_dollarhour' alias='bpa_dollarhour' aggregate='sum' />
                    <filter type='and' >
                        <condition attribute='bpa_memberplanid' operator='eq' value='{contactId}' />
                        <condition attribute='statecode' operator='eq' value='0' />
                        <condition attribute='bpa_transactiondate' operator='le' value='{benefitMonth.AddMonths(1).AddDays(-1).ToShortDateString()}' />
                        <condition attribute='bpa_useforcalculation' operator='eq' value='1' />
                        <condition attribute='statuscode' operator='eq' value='{(int)TransactionStatus.Posted}' />
                        <condition attribute='bpa_transactiontype' operator='in' >
                        <value>{((int)BpaTransactionbpaTransactionType.SelfPay).ToString()}</value>
                        <value>{((int)BpaTransactionbpaTransactionType.SelfPayBankAdjustment).ToString()}</value>
                        <value>{((int)BpaTransactionbpaTransactionType.SelfPayDeposit).ToString()}</value>
                        <value>{((int)BpaTransactionbpaTransactionType.SelfPayDraw).ToString()}</value>
                        <value>{((int)BpaTransactionbpaTransactionType.SelfPayInBenefit).ToString()}</value>
                        <value>{((int)BpaTransactionbpaTransactionType.SelfPayMaxConsecutiveReach).ToString()}</value>
                        <value>{((int)BpaTransactionbpaTransactionType.SelfPayRefund).ToString()}</value>
                        </condition>
                    </filter>
                    </entity>
                </fetch>            ";
            sums = service.RetrieveMultiple(new FetchExpression(fetchXml));

            if (sums != null && sums.Entities.Count > 0)
            {
                Entity sum = sums.Entities[0];
                decimal dollarhours = 0;

                //if (sum.Attributes.Contains("bpa_dollarhour"))
                if (sum.Attributes.Contains("bpa_dollarhour") &&
                    ((AliasedValue)sum.Attributes["bpa_dollarhour"]).Value != null)
                    dollarhours = ((Money)((AliasedValue)sum.Attributes["bpa_dollarhour"]).Value).Value;
                endingbalance.Add("selfpaydollar", dollarhours);
            }

            // secondary dollar bank
            fetchXml = $@"
                <fetch aggregate='true' >
                    <entity name='bpa_transaction' >
	                <attribute name='bpa_dollarhour' alias='bpa_dollarhour' aggregate='sum' />
                    <filter type='and' >
                        <condition attribute='bpa_memberplanid' operator='eq' value='{contactId}' />
                        <condition attribute='statecode' operator='eq' value='0' />
                        <condition attribute='bpa_transactiondate' operator='le' value='{benefitMonth.AddMonths(1).AddDays(-1).ToShortDateString()}' />
                        <condition attribute='statuscode' operator='eq' value='{(int)TransactionStatus.Posted}' />
                        <condition attribute='bpa_transactiontype' operator='in' >
                        <value>{((int)BpaTransactionbpaTransactionType.SecondaryDollarBankAdjustment).ToString()}</value>
                        <value>{((int)BpaTransactionbpaTransactionType.SecondaryDollarBankDraw).ToString()}</value>
                        </condition>
                    </filter>
                    </entity>
                </fetch>            ";
            sums = service.RetrieveMultiple(new FetchExpression(fetchXml));

            if (sums != null && sums.Entities.Count > 0)
            {
                Entity sum = sums.Entities[0];
                decimal dollarhours = 0;

                //if (sum.Attributes.Contains("bpa_dollarhour"))
                if (sum.Attributes.Contains("bpa_dollarhour") &&
                    ((AliasedValue)sum.Attributes["bpa_dollarhour"]).Value != null)
                    dollarhours = ((Money)((AliasedValue)sum.Attributes["bpa_dollarhour"]).Value).Value;
                endingbalance.Add("secondarydollar", dollarhours);
            }
            return endingbalance;
        }

        public static EntityCollection FetchAllFrozenTransaction(IOrganizationService service, ITracingService tracingService, Guid memberPlanId, DateTime workMonth)
        {
            if(workMonth == DateTime.MinValue)
                workMonth = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1);

            // secondary dollar bank
            string fetchXml = $@"
                 <fetch>
                    <entity name='bpa_transaction' >
	                <attribute name='bpa_name'/>
	                <attribute name='bpa_transactiondate'/>
                    <filter type='and' >
                        <condition attribute='bpa_memberplanid' operator='eq' value='{memberPlanId}' />
                        <condition attribute='statecode' operator='eq' value='0' />
                        <condition attribute='statuscode' operator='eq' value='{(int)TransactionStatus.Posted}' />
                        <condition attribute='bpa_transactiondate' operator='ge' value='{workMonth.ToShortDateString()}' />
                        <condition attribute='bpa_transactiontype' operator='in' >
                            <value>{((int)BpaTransactionbpaTransactionType.InBenefit).ToString()}</value>
                            <value>{((int)BpaTransactionbpaTransactionType.NotInBenefit).ToString()}</value>
                            <value>{((int)BpaTransactionbpaTransactionType.SelfPayInBenefit).ToString()}</value>
                            <value>{((int)BpaTransactionbpaTransactionType.FundAssistedInBenefit).ToString()}</value>
                            <value>{((int)BpaTransactionbpaTransactionType.UnderReview).ToString()}</value>
                            <value>{((int)BpaTransactionbpaTransactionType.NewMember).ToString()}</value>
                            <value>{((int)BpaTransactionbpaTransactionType.ReInstating).ToString()}</value>
                            <value>{((int)BpaTransactionbpaTransactionType.Frozen).ToString()}</value>
                            <value>{((int)BpaTransactionbpaTransactionType.Reciprocal).ToString()}</value>
                            <value>{((int)BpaTransactionbpaTransactionType.TransferOut).ToString()}</value>

                        </condition>
                    </filter>
                    </entity>
                </fetch>            ";
            return service.RetrieveMultiple(new FetchExpression(fetchXml));

        }

        public static EntityCollection FetchAllEligibilityStatusTransaction(IOrganizationService service, ITracingService tracingService, Guid memberPlanId, DateTime workMonth)
        {
            if (workMonth == DateTime.MinValue)
                workMonth = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1);

            // secondary dollar bank
            string fetchXml = $@"
                 <fetch>
                    <entity name='bpa_transaction' >
	                <attribute name='bpa_name'/>
                    <attribute name='bpa_transactiondate'/>
                    <attribute name='bpa_submission'/>
                    <filter type='and' >
                        <condition attribute='bpa_memberplanid' operator='eq' value='{memberPlanId}' />
                        <condition attribute='statecode' operator='eq' value='0' />
                        <condition attribute='statuscode' operator='eq' value='{(int)TransactionStatus.Posted}' />
                        <condition attribute='bpa_transactiondate' operator='ge' value='{workMonth.ToShortDateString()}' />
                        <condition attribute='bpa_transactiontype' operator='in' >
                            <value>{((int)BpaTransactionbpaTransactionType.InBenefit).ToString()}</value>
                            <value>{((int)BpaTransactionbpaTransactionType.NotInBenefit).ToString()}</value>
                            <value>{((int)BpaTransactionbpaTransactionType.SelfPayInBenefit).ToString()}</value>
                            <value>{((int)BpaTransactionbpaTransactionType.FundAssistedInBenefit).ToString()}</value>
                            <value>{((int)BpaTransactionbpaTransactionType.UnderReview).ToString()}</value>
                            <value>{((int)BpaTransactionbpaTransactionType.NewMember).ToString()}</value>
                            <value>{((int)BpaTransactionbpaTransactionType.ReInstating).ToString()}</value>
                            <value>{((int)BpaTransactionbpaTransactionType.Frozen).ToString()}</value>
                            <value>{((int)BpaTransactionbpaTransactionType.Reciprocal).ToString()}</value>
                            <value>{((int)BpaTransactionbpaTransactionType.TransferOut).ToString()}</value>
                        </condition>
                    </filter>
                    </entity>
                </fetch>            ";
            return service.RetrieveMultiple(new FetchExpression(fetchXml));

        }


        public static EntityCollection IsTransactionExists(IOrganizationService service, ITracingService traceing, Guid memberId, DateTime workMonth,
            int transactionType, Guid submissionId)
        {
            traceing.Trace("Inside TransactionHelper.IsTransactionExists Method");
            workMonth = new DateTime(workMonth.Year, workMonth.Month, 1);
            //Entity entity = null;
            string fetchxml = $@"
                <fetch>
                    <entity name='bpa_transaction' >
                    <attribute name='bpa_transactiondate' />
                    <filter>
                        <condition attribute='statecode' operator='eq' value='0' />
                        <condition attribute='statuscode' operator='eq' value='{((int)TransactionStatus.Posted).ToString()}' />
                        <condition attribute='bpa_memberplanid' operator='eq' value='{memberId}' />
                        <condition attribute='bpa_submission' operator='neq' value='{submissionId}' />
                        <condition attribute='bpa_transactiondate' operator='ge' value='{workMonth.ToShortDateString()}' />
                        <condition attribute='bpa_transactiondate' operator='le' value='{workMonth.AddMonths(1).AddDays(-1).ToShortDateString()}' />
                        <condition attribute='bpa_transactiontype' operator='eq' value='{(int)BpaTransactionbpaTransactionType.TransferOut}' />
                    </filter>
                    </entity>
                </fetch>
            ";
            traceing.Trace(fetchxml);
            EntityCollection transactions = service.RetrieveMultiple(new FetchExpression(fetchxml));
            //if ((transactions != null) && (transactions.Entities.Count > 0))
            //{
            //    entity = transactions.Entities[0];
            //    traceing.Trace("Found Record");

            //}
            return transactions;
        }

        public static decimal GetTotalVacationTransactions(IOrganizationService service, ITracingService tracingService, Guid memberPlanId)
        {
            tracingService.Trace("Getting Total Vacation Transactions.");

            decimal totalDollarAmount = decimal.Zero;

            EntityCollection collection = new EntityCollection();
            string fetchXml = $@"
                <fetch aggregate='true' >
                  <entity name='bpa_transaction' >
                    <attribute name='bpa_dollarhour' alias='TransactionTotal' aggregate='sum' />
                    <filter>
                      <condition attribute='bpa_transactiondate' operator='le' value='1 april 2017' />
                      <condition attribute='bpa_transactiontype' operator='eq' value='922070006' />
                      <condition attribute='bpa_paymentid' operator='null' />
                      <condition attribute='bpa_memberplanid' operator='eq' value='{memberPlanId}' />
                    </filter>
                  </entity>
                </fetch>";

            EntityCollection aggregateResult = service.RetrieveMultiple(new FetchExpression(fetchXml));

            if(aggregateResult.Entities.Count > 0 && aggregateResult.Entities[0].Attributes.Contains("TransactionTotal") && aggregateResult.Entities[0].GetAttributeValue<AliasedValue>("TransactionTotal").Value != null)
            {
                totalDollarAmount = ((Money)aggregateResult.Entities[0].GetAttributeValue<AliasedValue>("TransactionTotal").Value).Value;
            }
            
            tracingService.Trace($"Total Vacation Transactions is {totalDollarAmount.ToString()}");

            return totalDollarAmount;
        }

        public static DateTime GetMaxBenefitMonth(IOrganizationService service, ITracingService tracingService, Guid memberPlanId, DateTime workMonth)
        {
            tracingService.Trace("INSIDE TransactionHelper.GetMaxBenefitMonth");
            DateTime maxBenefitMonth = DateTime.MinValue;
            string fetchXml = $@"
               <fetch aggregate='true' >
                  <entity name='bpa_transaction' >
                    <attribute name='bpa_benefitmonth' alias='bpa_benefitmonth' aggregate='max' />
                    <filter>
                      <condition attribute='bpa_memberplanid' operator='eq' value='{memberPlanId}' />
                      <condition attribute='statecode' operator='eq' value='0' />
                      <condition attribute='statuscode' operator='eq' value='{((int)TransactionStatus.Posted).ToString()}' />
                      <condition attribute='bpa_transactioncategory' operator='eq' value='922070002' />
                      <condition attribute='bpa_transactiondate' operator='lt' value='{workMonth.ToShortDateString()}' />
                      <condition attribute='bpa_transactiontype' operator='in' >
                        <value>922070007</value>
                        <value>922070009</value>
                        <value>922070020</value>
                        <value>922070024</value>
                        <value>922070025</value>
                        <value>922070026</value>
                        <value>922070027</value>
                        <value>922070029</value>
                      </condition>
                    </filter>
                  </entity>
                </fetch>";
            tracingService.Trace(fetchXml);

            EntityCollection collection = service.RetrieveMultiple(new FetchExpression(fetchXml));

            if (collection != null && collection.Entities.Count > 0 )
            {
                Entity notInBenefit = collection.Entities[0];
                if(notInBenefit.Contains("bpa_benefitmonth"))
                    maxBenefitMonth = ((DateTime)notInBenefit.GetAttributeValue<AliasedValue>("bpa_benefitmonth").Value).AddMonths(1);
            } 
            tracingService.Trace($@"maxBenefitMonth {maxBenefitMonth.ToString()}");

            if (maxBenefitMonth == DateTime.MinValue)
            {
                maxBenefitMonth = GetFirstBenefitMonth(service, tracingService, memberPlanId, workMonth);
            }

            return maxBenefitMonth;
        }
        public static DateTime GetFirstBenefitMonth(IOrganizationService service, ITracingService tracingService, Guid MemberId, DateTime WorkMonth)
        {
            tracingService.Trace("INSIDE TransactionHelper.GetFirstBenefitMonth");

            DateTime maxBenefitMonth = DateTime.MinValue;
            string fetchxml = $@"
                    <fetch aggregate='true' >
                      <entity name='bpa_transaction' >
                        <attribute name='bpa_benefitmonth' alias='bpa_benefitmonth' aggregate='min' />
                        <filter type='and' >
                          <condition attribute='statecode' operator='eq' value='0' />
                          <condition attribute='statuscode' operator='eq' value='{(int)TransactionStatus.Posted}' />
                          <condition attribute='bpa_memberplanid' operator='eq' value='{MemberId.ToString()}' />
                          <condition attribute='bpa_transactiondate' operator='lt' value='{WorkMonth.ToShortDateString()}' />
                          <condition attribute='bpa_transactioncategory' operator='eq' value='{((int)BpaTransactionbpaTransactionCategory.Eligibility).ToString()}' />
                          <condition attribute='bpa_transactiontype' operator='in' >
                            <value>{((int)BpaTransactionbpaTransactionType.InBenefit).ToString()}</value>
                            <value>{((int)BpaTransactionbpaTransactionType.SelfPayInBenefit).ToString()}</value>
                            <value>{((int)BpaTransactionbpaTransactionType.FundAssistedInBenefit).ToString()}</value>
                          </condition>
                        </filter>
                      </entity>
                    </fetch>
                ";
            tracingService.Trace(fetchxml);


            EntityCollection transactions = service.RetrieveMultiple(new FetchExpression(fetchxml));
            if (transactions != null && transactions.Entities.Count > 0)
            {
                Entity trans = transactions.Entities[0];
                if (trans.Contains("bpa_benefitmonth"))
                    maxBenefitMonth = ((DateTime)trans.GetAttributeValue<AliasedValue>("bpa_benefitmonth").Value);

            }
            return maxBenefitMonth;
        }

    }
}
