﻿#region

using System;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;

#endregion

namespace BPA.Benefit.Management.CRM.Plugins.Helper
{
    public class SubmissionAdjustmentHelper
    {
        //Get Sum of Submission Adjustment
        public static decimal GetSumOfSubmissionAdjustment(IOrganizationService service, ITracingService tracingService,
            Guid employerId, DateTime createdon)
        {
            decimal bpaAmount = 0;
            string fetchxml = $@"
                <fetch aggregate='true' >
                  <entity name='bpa_submissionadjustment' >
                    <attribute name='bpa_amount' alias='bpa_amount' aggregate='sum' />
                    <filter>
                      <condition attribute='statuscode' operator='eq' value='{((int)AdjustmentStatusCode.Complete).ToString()}' />
                      <condition attribute='bpa_accountagreementid' operator='eq' value='{employerId.ToString()}' />
                      <condition attribute='createdon' operator='lt' value='{createdon}' />
                      <condition attribute='bpa_type' operator='not-in' >
                        <value>{(int)AdjustmentbpaType.InterestCharge}</value>
                        <value>{(int)AdjustmentbpaType.InterestPaid}</value>
                        <value>{(int)AdjustmentbpaType.InterestWaived}</value>
                      </condition>
                    </filter>
                  </entity>
                </fetch>";
            //                        <value>{(int)AdjustmentbpaType.InterestRedirection}</value>
            EntityCollection sums = service.RetrieveMultiple(new FetchExpression(fetchxml));
            if (sums != null && sums.Entities.Count > 0)
            {
                Entity sum = sums.Entities[0];
                if (sum.Attributes.Contains("bpa_amount") &&
                    (sum.GetAttributeValue<AliasedValue>("bpa_amount").Value != null))
                    bpaAmount = ((Money)sum.GetAttributeValue<AliasedValue>("bpa_amount").Value).Value;
            }
            return bpaAmount;
        }


        public static decimal FetchPreviousInteresetBalance(IOrganizationService service, ITracingService tracingService, Guid employerId, DateTime createdon, Guid submisisonId)
        {
            decimal bpaAmount = 0;
            string fetchxml = $@"
                <fetch aggregate='true' >
                  <entity name='bpa_submissionadjustment' >
                    <attribute name='bpa_amount' alias='bpa_amount' aggregate='sum' />
                    <filter>
                      <condition attribute='statuscode' operator='eq' value='{((int)AdjustmentStatusCode.Complete).ToString()}' />
                      <condition attribute='bpa_accountagreementid' operator='eq' value='{employerId.ToString()} ' />
                      <condition attribute='createdon' operator='lt' value='{createdon}' />
                      <condition attribute='bpa_type' operator='in' >
                        <value>{(int)AdjustmentbpaType.InterestCharge}</value>
                        <value>{(int)AdjustmentbpaType.InterestPaid}</value>
                        <value>{(int)AdjustmentbpaType.InterestWaived}</value>
                      </condition>
                      <condition attribute='bpa_submission' operator='neq' value='{submisisonId.ToString()}' />
                    </filter>
                  </entity>
                </fetch>";
            //                        <value>{(int)AdjustmentbpaType.InterestRedirection}</value>
            EntityCollection sums = service.RetrieveMultiple(new FetchExpression(fetchxml));
            if (sums != null && sums.Entities.Count > 0)
            {
                Entity sum = sums.Entities[0];
                if (sum.Attributes.Contains("bpa_amount") &&
                    (sum.GetAttributeValue<AliasedValue>("bpa_amount").Value != null))
                    bpaAmount = ((Money)sum.GetAttributeValue<AliasedValue>("bpa_amount").Value).Value;
            }
            return bpaAmount;
        }

        public static void UpdateAccountVariance(IOrganizationService service, ITracingService tracingService,
            Entity entity, Entity postImageEntity)
        {
            if (entity.Attributes.Contains("bpa_amount") && entity.GetAttributeValue<Money>("bpa_amount") != null)
            {
                // Populate the amount, create on date and employer
                tracingService.Trace("Populate the amount, create on date and employer");
                Money amount = entity.GetAttributeValue<Money>("bpa_amount");
                DateTime createdOn = DateTime.MinValue;
                EntityReference employerId = null;
                if (postImageEntity == null)
                {
                    createdOn = entity.Attributes.Contains("createdon")
                        ? entity.GetAttributeValue<DateTime>("createdon")
                        : DateTime.MinValue;
                    employerId = entity.Attributes.Contains("bpa_accountagreementid")
                        ? entity.GetAttributeValue<EntityReference>("bpa_accountagreementid")
                        : null;
                }
                else
                {
                    tracingService.Trace("Populate the amount, create on date and employer");
                    createdOn = postImageEntity.Attributes.Contains("createdon")
                        ? postImageEntity.GetAttributeValue<DateTime>("createdon")
                        : DateTime.MinValue;
                    employerId = postImageEntity.Attributes.Contains("bpa_accountagreementid")
                        ? postImageEntity.GetAttributeValue<EntityReference>("bpa_accountagreementid")
                        : null;
                }

                // If there was a non-zero amount added to the adjustment
                tracingService.Trace("If there was a non-zero amount added to the adjustment");
                if ((amount.Value != 0) && (employerId != null))
                {
                    // Update the Account (Employer) variance
                    tracingService.Trace("Update the Account (Employer) variance");
                    decimal openingBalance = 0;
                    openingBalance = SubmissionHelper.GetOpeningVariance(service, tracingService, employerId.Id,
                        createdOn);
                    tracingService.Trace("2 Opening Balance - " + openingBalance);
                    AccountAgreementHelper.UpdateAccountVariance(service, tracingService, employerId.Id, openingBalance);
                    tracingService.Trace("Account Update Successfully Completed.");
                }
            }
        }

        public static Guid CreateAdjustment(IOrganizationService service, ITracingService tracingService, DateTime workMonth, EntityReference employer, int adjustmentType, decimal amount, string description, EntityReference submission)
        {
            Entity entity = new Entity(PluginHelper.BpaSubmissionadjustment)
            {
                ["bpa_workmonth"] = workMonth,
                ["bpa_accountagreementid"] = employer,
                ["bpa_type"] = new OptionSetValue(adjustmentType),
                ["bpa_amount"] = new Money(amount),
                ["bpa_description"] = description,
                ["bpa_submission"] = submission
            };

            return service.Create(entity);
        }
    }
}