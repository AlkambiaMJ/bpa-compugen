﻿namespace BPA.Benefit.Management.CRM.Plugins.Helper
{
    public enum AccountStatus
    {
        Open = 922070000,
        Closed = 922070001
    }

    public enum AccountType
    {
        Employer = 922070000,
        Union = 922070001,
        InternalOffice = 922070002,
        Insurer = 922070003,
        Association = 922070004,
        SelfPay = 922070005
    }
    public enum MemberPlanBpaMemberplantype
    {
        Retiree = 922070000,
        Member = 922070003,
        Employee = 922070002,
        Corporate = 922070001
    }

    public enum ContactbpaCurrentEligibility
    {
        NewMember = 922070000,
        InBenefit = 922070001,
        SelfPayInBenefit = 922070002,
        FundAssisted = 922070003,
        NotInBenefit = 922070004,
        Frozen = 922070005,
        Inactive = 922070006,
        UnderReview = 922070007,
        Reinstatement = 922070008,
        TransferOut = 922070009
    }

    public enum AdjustmentbpaType
    {
        MemberCorrection = 922070003,
        Miscellaneous = 922070001,
        Nsf = 922070000,
        Redirection = 922070002,
        InterestCharge = 922070004,
        InterestPaid = 922070005,
        InterestWaived = 922070006,
        InterestRedirection = 922070007,
    }

    public enum AdjustmentStatusCode
    {
        Active = 1,
        Complete = 2,
        Cancelled = 922070001
    }

    public enum SubmissionType
    {
        Nsf = 922070004,
        Reciprocal = 922070003,
        Historical = 922070002,
        Employer = 100000000,
        Adjustment = 100000001,
        Late = 922070000,
        NoActivity = 922070001,
        EmployerMoneyOnly = 100000002
    }

    public enum SubmissionSubmissionType
    {
        EmployerRegular = 922070000,
        EmployerNoActivity = 922070001,
        EmployerMoneyOnly = 922070002,
        EmployerAdjustment = 922070003,
        Member = 922070004,
        MemberSelfPayRefund = 922070005,
        RetireeMonthlySubmission = 922070006
    }

    public enum SubmissionStatus
    {
        Pending = 922070000,
        Verification = 922070001,
        PendingDeposit = 922070002,
        Completed = 922070003,
        Nsf = 922070004,
        Cancelled = 922070005
    }

    public enum SubmissionStatusCode
    {
        Active = 1,
        Processing = 922070003,
        Completed = 2,
        Cancelled = 922070002
    }

    public enum SubmissionPaymentType
    {
        Cheque = 922070000,
        ETFOrDirectDeposit = 922070001,
        NoPayment = 922070002,
        Debit = 922070003,
        PreAuth = 922070004
    }

    public enum ContactContactType
    {
        Other = 922070001,
        Member = 922070000
    }

    public enum BpaTransactionbpaTransactionType
    {
        EmployerContribution = 922070000,
        SelfPay = 922070001,
        Adjustment = 922070002,
        EligibilityDraw = 922070003,
        InBenefit = 922070004,
        Payout = 922070005,
        VacationContribution = 922070006,
        Reciprocal = 922070007,
        SelfPayDraw = 922070008,
        NotInBenefit = 922070009,
        ReserveTransfer = 922070010,
        VacationDraw = 922070011,
        FundAssisted = 922070012,
        Historical = 922070013,
        OpeningBalance = 922070014,
        AnnualVacationDraw = 922070015,
        VacationPayAdjustment = 922070016,
        PrePaidLegal = 922070017,
        UnionandWorkingDues = 922070018,
        SelfPayInBenefit = 922070019,
        NewMember = 922070020,
        SelfPayMaxConsecutiveReach = 922070021,
        SelfPayRefund = 922070022,
        FundAssistedInBenefit = 922070023,
        UnderReview = 922070024,
        ReInstating = 922070025,
        Frozen = 922070026,
        Inactive = 922070027,
        TransferIn = 922070028,
        TransferOut = 922070029,
        LateTransferIn = 922070030,
        NonWelfareCorrectional = 922070031,
        DollarBankAdjustment = 922070032,
        HourBankAdjustment = 922070033,
        SelfPayBankAdjustment = 922070034,
        EmployerRefund = 922070035,
        SelfPayDeposit = 922070036,
        SecondaryDollarBankDraw = 922070037,
        SecondaryDollarBankAdjustment = 922070038,
        PensionContribution = 922070039
    }

    public enum BpaTransactionbpaTransactionCategory
    {
        Contributions = 922070001,
        Eligibility = 922070002,
        PensionAdjustment = 922070003,
        MemberPayment = 922070005,
        Historic = 922070006,
        BenefitAdjustment = 922070007,
        Fees = 922070000,
        Benefit = 922070004,
    }

    public enum DepositDepositStatus
    {
        Open = 922070000,
        Deposited = 922070001
    }

    public enum EmployerWorkStatus
    {
        Employed = 922070000,
        NotEmployed = 922070001
    }

    public enum BpaFundbpaFundType
    {
        Union = 922070002,
        VacationPay = 922070000,
        Welfare = 922070001,
        Pension = 922070003,
        NonWelfare = 922070005,
        Nsf = 922070004,
        DelinquencyInterest = 922070006
    }
    
    public enum BpaEligibilityCategoryPlanEligiblityType
    {
        Hours = 922070001,
        Dollar = 922070000
    }

    public enum BpaSubSectorBpaCalculationType
    {
        HourBased = 922070000,
        Flat = 922070001,
        DollarBased = 922070002,
        Piecework = 922070003,
        Corporate = 922070004,
        Icinonici = 922070005
    }

    public enum RateCalculationType
    {
        Hourly = 922070000,
        Dollar = 922070001,
        FlatRate = 922070002,
        PercentageGrossWages = 922070003,
        Tax = 922070004,
        PensionMemberVoluntaryDollar = 922070005,
        PensionMemberRequiredDollar = 922070006,
        HourEarned = 922070007

    }

    public enum BpaContributionbpaContributionType
    {
        Overpayment = 922070004,
        Interest = 922070003,
        Adjustment = 922070002,
        Regular = 922070000,
        Outstanding = 922070001,
        Nsf = 922070005,
        Redirection = 922070006,
        Disbursement = 922070007
    }

    public enum BpaSubmissionDetailBpaMemberType
    {
        MemberEmployed = 922070000,
        MemberNotEmployed = 922070001,
        NewMember = 922070002
    }

    public enum BpaSubmissionDetailBpaFlatRateRule
    {
        Charge = 922070000,
        Ignore = 922070001,
        Remove = 922070002
    }

    public enum PlanPlanType
    {
        Active = 922070000,
        Retiree = 922070001
    }


    public enum ContactStatusCode
    {
        Active = 1,
        Inactive = 2
    }

    public enum MemberPlanBpaCurrentEligibility  // Spelled Wrong
    {
        NewMember = 922070000,
        InBenefit = 922070001,
        SelfPayInBenefit = 922070002,
        FundAssisted = 922070003,
        NotInBenefit = 922070004,
        Frozen = 922070005,
        Inactive = 922070006,
        UnderReview = 922070007,
        Reinstatement = 922070008,
        TransferOut = 922070009,
        TransferIn = 922070010
    }

    public enum SubmissionSubmissionTypeNew
    {
        EmployerRegular = 922070000,
        EmployerNoActivity = 922070001,
        EmployerMoneyOnly = 922070002,
        EmployerAdjustment = 922070003,
        Member = 922070004,
        MemberSelfPayRefund = 922070005
    }

    public enum SubmissionDetailFlatRateRule
    {
        Charge = 922070000,
        Ignore = 922070001,
        Remove = 922070002
    }

    public enum PaymentBatchPaymentType
    {
        InterimVacation = 922070000,
        ParentalLeave = 922070001,
        JuryDuty = 922070002,
        BereavmentLeave = 922070003,
        AnnualVacation = 922070004,
        PrePaidLegal = 922070005,
        SelfPayRefund = 922070006,
        EmployerRefund = 922070007, //Not used
        DentalClinic = 922070008 // Not used
    }

    public enum PaymentBatchStatus
    {
        Active = 1,
        Processing = 922070000,
        Cancelled = 2,
        Completed = 922070001
    }

    public enum MemberPlanAdjustmentType
    {
        TransferReciprocalIn = 922070000,
        TransferReciprocalOut = 922070001,
        Other = 922070002

    }

    public enum MemberPlanBenefitAdjustmentType
    {
        OtherBenefit = 922070000,
        TransferReciprocalIn = 922070001,
        TransferReciprocalOut = 922070002
    }

    public enum MemberPlanAdjustmentCategory
    {
        BenefitAdjustment = 922070000,
        PensionAdjustment = 922070001
    }
    
    public enum MemberPlanBpaMemberPlanStatus
    {
        Active = 922070000,
        Inactive = 922070001,
        ReciprocalTransferOut = 922070002,
        Suspended = 922070003,
        NA = 922070004
    }

    public enum DepositType
    {
        Cheque = 922070000,
        ETFDirectDeposit = 922070001,
        Debit = 922070002,
        PreAuth = 922070004,
        NoPayment = 922070003
    }

    public enum MemberAdjustmentStatusReason
    {
        Cancelled = 922070000,
        Completed = 2,
        Active = 1
    }

    public enum BpaBenefiStatusType
    {
        Both = 922070000,
        Active = 922070001,
        SelfPay = 922070002
    }

    public enum BpaPaymentPaymentType
    {
        AnnualVacation = 922070004,
        BereavmentLeave = 922070003,
        EmployerRefund = 922070006,
        InterimVacation = 922070000,
        JuryDuty = 922070002,
        ParentalLeave = 922070001,
        PrePaidLegal = 922070005,
        SelfPayRefund = 922070007
    }

    public enum BpaPaymentStatusCode
    {
        Completed = 2,
        Cancelled = 922070001,
        Processing = 922070000,
        Active = 1
    }

    public enum BpaAnnualVacationRequestStatusCode
    {
        Pending = 1,
        Processing = 922070000,
        Cancelled = 922070001,
        Completed = 2 
    }

    public enum BpaPeriodOfServiceStatusCode
    {
        Qualifying = 1,
        Active = 922070000,
        Deferred = 922070001,
        Retiree = 922070002,
        Inactive = 2
    }

    public enum BpaPensionVestingRequirement
    {
        Immediate = 922070000,
        NumberOfYearsInPlan = 922070001,
        Age = 922070002,
        AgeAndNumberOfYearsInPlan = 922070003
    }

    public enum BpaPensionEventType
    {
        MemberJoinedPlan = 922070000,
        MemberQualifiedforPlan = 922070001,
        MemberVestedinPlan = 922070002,
        MemberReturnedtoActive = 922070003,
        MemberisDisabled = 922070004,
        MemberRetires = 922070005,
        MemberTerminatesfromPlan = 922070006,
        MemberMarriageBreakdown = 922070007,
        MemberDeath = 922070008,
        MemberTransfersOutofPlan = 922070009,
        MemberTransfersintoPlan = 922070010
    }

    public enum BpaPensionEventStatus
    {
        Pending = 1,
        Completed = 2,
        Cancelled = 922070000
    }

    public enum TransactionStatus
    {
        Posted = 1,
        Pending = 922070000,
        Completed = 2
    }


    public enum PensionTransactionType
    {
        PensionContribution = 922070000,
        DCContribution = 922070001,
        DBContribution = 922070002,
        RequiredContribution = 922070003,
        VoluntaryContribution = 922070004,
        BuybackContribution = 922070005,
        TransferredContribution = 922070006,
        Deemed = 922070007,
        Forfeited = 922070008,
        Adjustment = 922070009
    }


    public enum PensionTransactionCategory
    {
        Employer = 922070000,
        Employee = 922070001,
        Other = 922070002
    }

    public enum PensionPlanType
    {
        DefinedBenefit = 922070000,
        DefinedContribution = 922070001
    }


    public enum PensionBenefitCalculationType
    {
        Hour = 922070000,
        Dollar = 922070001
    }

    public enum PaymentMethod
    {
        Cheque = 922070001,
        EFT_ToMember = 922070002,
        EFT_FromEmployer = 922070003
    }

}