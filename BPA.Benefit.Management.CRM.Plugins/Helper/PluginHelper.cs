﻿#region

using System;
using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using System.IO;
using System.Xml;
using System.Text;
using System.Collections.Generic;

#endregion

namespace BPA.Benefit.Management.CRM.Plugins.Helper
{
    public static class PluginHelper
    {
        //Plugin 
        public const string PostImageAlias = "PostImage";
        public const string PreImageAlias = "PreImage";
        public const string Target = "Target";
        public const string EntityMoniker = "EntityMoniker";
        public const int PreStage = 20;
        public const int PostStage = 40;

        //Plugin Messages
        public const string Create = "Create";
        public const string Update = "Update";
        public const string Delete = "Delete";
        public const string Associate = "Associate";
        public const string SetState = "SetState";
        public const string SetStateDynamicEntity = "SetStateDynamicentity";

        public const string HealthNWelfareFund = "health & welfare";
        public const string NsfFeeNameConfig = "NSF Fee Name";


        public const string ConfigCrmAdmin = "CRM Admin";
        public const string SINNumber = "SIN Number";
        public const string ConfigDefaultCurrency = "Default Currency";
        public const string ConfigSubmissionCompletedStage = "Submission - Business Process Flow Stage";
        public const string ConfigSubmissionBusinessProcessFlow = "Submission - Business Process Flow"; 
        public const string DefaultJobClassification = "Default Job Classification";
        public const string ConfigExecuteMultipleRequest = "ExecuteMultipleRequest";

        public static Guid SubmissionStageIdPending = new Guid("CBAAE3DA-139F-491B-915F-0FA44833A0B7");
        public static Guid SubmissionStageIdVerify = new Guid("20867DA5-89E2-4A76-E237-34A00C85D4A3");
        public static Guid SubmissionStageIdDepositPending = new Guid("98659877-7BD7-B38A-B13B-000CD9A48260");
        public static Guid SubmissionStageIdComplete = new Guid("E3978A8B-8183-2CE0-0A05-48EAB8A50442");

        // Deactivate a record
        public static void DeactivateRecord(string entityName, Guid recordId, IOrganizationService organizationService)
        {
            var cols = new ColumnSet("statecode", "statuscode");

            //Check if it is Active or not
            var entity = organizationService.Retrieve(entityName, recordId, cols);

            if (entity != null && entity.GetAttributeValue<OptionSetValue>("statecode").Value == 0)
            {
                //StateCode = 1 and StatusCode = 2 for deactivating Account or Contact
                SetStateRequest setStateRequest = new SetStateRequest();
                setStateRequest.EntityMoniker = new EntityReference(entityName, recordId);

                // Set the Request Object’s Properties
                setStateRequest.State = new OptionSetValue(1); // Inactive state
                setStateRequest.Status = new OptionSetValue(2); // Complete status

                // Execute the Request
                organizationService.Execute(setStateRequest);
            }
        }
        public static void DeactivateRecord(IOrganizationService organizationService, string entityName, Guid recordId, int reasonCode )
        {
            var cols = new ColumnSet("statecode", "statuscode");

            //Check if it is Active or not
            //var entity = organizationService.Retrieve(entityName, recordId, cols);

                //StateCode = 1 and StatusCode = 2 for deactivating Account or Contact
                SetStateRequest setStateRequest = new SetStateRequest();
                setStateRequest.EntityMoniker = new EntityReference(entityName, recordId);

                // Set the Request Object’s Properties
                setStateRequest.State = new OptionSetValue(1); // Inactive state
                setStateRequest.Status = new OptionSetValue(reasonCode); // Complete status

                // Execute the Request
                organizationService.Execute(setStateRequest);

        }
        public static void ActiveInactivateRecord(IOrganizationService organizationService, string entityName, Guid recordId, int statecode, int statuscode)
        {
            SetStateRequest setStateRequest = new SetStateRequest();
            setStateRequest.EntityMoniker = new EntityReference(entityName, recordId);

            // Set the Request Object’s Properties
            setStateRequest.State = new OptionSetValue(statecode); // state
            setStateRequest.Status = new OptionSetValue(statuscode); // status

            // Execute the Request
            organizationService.Execute(setStateRequest);
        }
        public static void TriggerRollup(IOrganizationService service, ITracingService trace, string entityName,
            Guid objectGuid, string nameOfField)
        {
            trace.Trace($@"Inside triggerRollup - {nameOfField}");
            CalculateRollupFieldRequest crfr = new CalculateRollupFieldRequest
            {
                Target = new EntityReference(entityName, objectGuid),
                FieldName = nameOfField
            };
            CalculateRollupFieldResponse response = (CalculateRollupFieldResponse)service.Execute(crfr);
            service.Update(response.Entity);
            trace.Trace($@"Inside triggerRollup - {nameOfField} - Response");
        }

        public static void AssignRecord(IOrganizationService service, ITracingService tracingService, EntityReference owner, string entityName , Guid recordId)
        {
            tracingService.Trace("Inside AssignRecord");
            AssignRequest assign = new AssignRequest
            {
                Assignee = owner,
                Target = new EntityReference(entityName, recordId)
            };
            // Execute the Request
            service.Execute(assign);
            tracingService.Trace($@"Update Entity - {entityName} ,Owner - {owner.Name}");
        }


        public static void UpdateField(IOrganizationService service, ITracingService tracingService, Guid recordId, string entityName, string fieldName, 
            string fieldValue)
        {
            Entity entity = new Entity(entityName)
            {
                Id = recordId,
                [fieldName] = fieldValue
            };

            service.Update(entity);
        }

        public static int CalculateAge(ITracingService tracingService, DateTime benefitWorkMonth, DateTime dateOfBirth)
        {
            tracingService.Trace("Inside PluginHelper.CalculateAge");
            tracingService.Trace($"DateTime.Now.DayOfYear: {benefitWorkMonth.DayOfYear}");
            tracingService.Trace($" dateOfBirth.DayOfYear: { dateOfBirth.DayOfYear}");

            int age = 0;
            age = benefitWorkMonth.Year - dateOfBirth.Year;
            if (benefitWorkMonth.DayOfYear < dateOfBirth.DayOfYear)
                age = age - 1;
            tracingService.Trace($"Calculate Age: {age}");

            return age;
        }

        /**********************
         * CASE 1
         *      existingStartDate -------------------- existingEndDate
         *                  newStartDate ---------------------- newEndDate
         * CASE 2
         *             existingStartDate -------------------- existingEndDate
         *      newStartDate ---------------------- newEndDate
         * CASE 3
         *        existingStartDate ---------- existingEndDate
         *      newStartDate ---------------------- newEndDate
         * CASE 4
         *      existingStartDate -------------------- existingEndDate
         *                 newStartDate ----------- newEndDate
         * CASE 5
         *      existingStartDate -------------------- existingEndDate                                      cStart -------------------- cEnd
         *                                                              newStartDate ----------- newEndDate
         *************************/
        public static bool DoesDateOverlap(DateTime existingStartDate, DateTime existingEndDate, DateTime newStartDate, DateTime newEndDate, ITracingService tracingService)
        {
            tracingService.Trace("Inside PluginHelper.DoesDateOverlap");
            bool isOverlap = false;

            if (newEndDate == DateTime.MinValue)
            {
                if ((existingEndDate >= newStartDate))
                {
                    tracingService.Trace("Case 6 Overlap ");
                    isOverlap = true; // Case 6
                }
            }
            else
            {
                isOverlap = newStartDate <= existingEndDate && existingStartDate <= newEndDate;
                //if (existingStartDate <= newStartDate)
                //{
                //    tracingService.Trace("Inside  if (existingStartDate <= newStartDate) ");

                //    if (existingEndDate >= newStartDate && existingEndDate <= newEndDate)
                //    {
                //        tracingService.Trace("Case 1 Overlap ");
                //        isOverlap = true; // Case 1
                //    }

                //    if (existingEndDate >= newEndDate)
                //    {
                //        tracingService.Trace("Case 3 Overlap ");
                //        isOverlap = true; // Case 3
                //    }
                //}
                //else
                //{
                //    tracingService.Trace("Inside  else ");

                //    if (newEndDate >= existingStartDate && newEndDate <= existingEndDate)
                //    {
                //        tracingService.Trace("Case 2 Overlap ");
                //        isOverlap = true; // Case 2
                //    }
                //    if (newEndDate >= existingEndDate)
                //    {
                //        tracingService.Trace("Case 4 Overlap ");
                //        isOverlap = true; // Case 4
                //    }
                //}
            }

            return isOverlap;
        }

        #region ---- Entity Names ---

        public const string Contact = "contact";
        public const string BpaMember = "bpa_member";
        public const string Product = "product";
        public const string Account = "account";
        public const string AccountAgreement = "bpa_accountagreement";
        public const string Invoice = "invoice";
        public const string Invoicedetail = "invoicedetail";
        public const string Productpricelevel = "productpricelevel";
        public const string BpaInvoicetransaction = "bpa_invoicetransaction";

        public const string BpaTransaction = "bpa_transaction";
        public const string BpaPortalsubmission = "bpa_portalsubmission";
        public const string BpaSubmission = "bpa_submission";
        public const string BpaSubmissiondetail = "bpa_submissiondetail";
        public const string BpaContribution = "bpa_contribution";
        public const string BpaTax = "bpa_tax";
        public const string BpaConfiguration = "bpa_configuration";
        public const string BpaEligibilitycategory = "bpa_eligibilitycategory";
        public const string BpaEligibilitycategorydetail = "bpa_eligibilitycategorydetail";
        public const string BpaSubmissionadjustment = "bpa_submissionadjustment";
        public const string BpaBulkselfpay = "bpa_bulkselfpay";
        public const string BpaFund = "bpa_fund";
        public const string BpaDependant = "bpa_dependant";
        public const string BpaSectorlabourrole = "bpa_sectorlabourrole";
        public const string BpaBenefitaudit = "bpa_benefitaudit"; // will be remove
        public const string BpaRate = "bpa_rate";
        public const string BpaLabourrole = "bpa_labourrole";
        public const string BpaRateJobClassification = "bpa_ratelabourrole";

        public const string BpaEventreport = "bpa_eventreport";
        public const string BpaEvent = "bpa_event";
        public const string BpaDeposit = "bpa_deposit";
        public const string BpaTrust = "bpa_trust";
        public const string BpaEmployment = "bpa_employment";
        public const string BpaSector = "bpa_sector";
        public const string BpaSubsector = "bpa_subsector";
        public const string Transactioncurrency = "transactioncurrency";
        public const string BpaMemberplan = "bpa_memberplan";
        public const string BpaMemberPlanadjustment = "bpa_memberplanadjustment";
        public const string BpaDelinquency = "bpa_delinquency";
        public const string BpaDelinquencyrule = "bpa_delinquencyrule";
        public const string BpaPayment = "bpa_payment";
        public const string BpaCheque = "bpa_cheque";
        public const string BpaBenefitAuditHeader = "bpa_benefitauditheader";
        public const string BpaBenefitAuditDetail = "bpa_benefitauditdetail";
        public const string BpaBenefit = "bpa_benefit";
        public const string BpaPaymentBatch = "bpa_paymentbatch";
        public const string BpaAnnualCacationRequest = "bpa_annualvacationrequest";
        public const string BpaBeneficiary = "bpa_beneficiary";
        public const string BpaPlanBeneficiary = "bpa_beneficiaryplan";
        public const string BpaPlanDependant = "bpa_dependantplan";
        public const string BpaPeriodOfService = "bpa_periodofservice";
        public const string BpaPensionEvent = "bpa_pensionevent";
        public const string BpaPensionTransaction = "bpa_pensiontransaction";
        public const string BpaPrePrintRate = "bpa_preprintrate";
        public const string BpaTrustValidation = "bpa_trustvalidation";
        public const string BpaTrustValidationDetail = "bpa_trustvalidationdetail";


        public const string BpaSubmssionBusinessProcessFlow = "bpa_bpf_5a5100a29c7d400bbe9421c6ae3ebde7";
        


        #endregion



        #region ------- More than 5000 REcords FetchXml -------

        public static string CreateXml(string xml, string cookie, int page, int count)
        {
            StringReader stringReader = new StringReader(xml);
            XmlTextReader reader = new XmlTextReader(stringReader);

            // Load document
            XmlDocument doc = new XmlDocument();
            doc.Load(reader);

            return CreateXml(doc, cookie, page, count);
        }

        public static  string CreateXml(XmlDocument doc, string cookie, int page, int count)
        {
            XmlAttributeCollection attrs = doc.DocumentElement.Attributes;

            if (cookie != null)
            {
                XmlAttribute pagingAttr = doc.CreateAttribute("paging-cookie");
                pagingAttr.Value = cookie;
                attrs.Append(pagingAttr);
            }

            XmlAttribute pageAttr = doc.CreateAttribute("page");
            pageAttr.Value = System.Convert.ToString(page);
            attrs.Append(pageAttr);

            XmlAttribute countAttr = doc.CreateAttribute("count");
            countAttr.Value = System.Convert.ToString(count);
            attrs.Append(countAttr);

            StringBuilder sb = new StringBuilder(1024);
            StringWriter stringWriter = new StringWriter(sb);

            XmlTextWriter writer = new XmlTextWriter(stringWriter);
            doc.WriteTo(writer);
            writer.Close();

            return sb.ToString();
        }
        #endregion

    }
}