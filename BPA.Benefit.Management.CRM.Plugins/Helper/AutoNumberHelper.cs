﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BPA.Benefit.Management.CRM.Plugins.Helper
{
    public static class AutoNumberHelper
    {
        static readonly object lockObject = new object();
        static readonly object lock4Number = new object();


        #region Contacts
        /// <summary>
        /// Fetches the next Contact Number with prefix from the Configuration entity
        /// </summary>
        /// <param name="service">CRM Org Service</param>
        /// <returns>Contact Number in format PTL0</returns>
        public static string GetNextContactNumber(IOrganizationService service)
        {
            int contactValue = 0;
            string prefix = string.Empty;
            string contactNumber = string.Empty;

            lock (lock4Number)
            {
                #region Build Query
                QueryExpression query = new QueryExpression("bpa_configuration");
                query.Criteria.AddCondition("statuscode", ConditionOperator.Equal, 1);
                query.Criteria.AddCondition("bpa_name", ConditionOperator.Equal, "SIN Number");
                query.ColumnSet = new ColumnSet(new string[] { "bpa_value", "bpa_prefix" });
                #endregion

                #region Retrieve configuration
                EntityCollection ec = service.RetrieveMultiple(query);
                if (ec.Entities.Count != 0)
                {
                    // Take the first result
                    Entity config = ec.Entities[0];
                    if (config.Contains("bpa_prefix") && config.Contains("bpa_value"))
                    {

                        // Get Contact Prefix & Number
                        prefix = (string)config["bpa_prefix"];
                        string Value = (string)config["bpa_value"];

                        Int32.TryParse(Value, out contactValue);
                        string GeneratedSIN = contactValue.ToString().PadLeft(5, '0');
                        GeneratedSIN = GeneratedSIN.Substring(0, 2) + "-" + GeneratedSIN.Substring(2, GeneratedSIN.Length - 2);

                        // Concatenate prefix and number together
                        contactNumber = prefix + "" + GeneratedSIN; // contactValue.ToString().PadLeft(3, '0');

                        // Set the Next number to update the configuration
                        int nextNumber = contactValue + 1;

                        // Update the Configuration
                        config["bpa_value"] = nextNumber.ToString();
                        service.Update(config);
                    }
                    else
                    {
                        throw new InvalidPluginExecutionException("Configuration Entity is missing Next Contact Number");
                    }
                }
                else
                {
                    throw new InvalidPluginExecutionException("Configuration Entity is missing.  Please add a Configuration record and a Next Contact Number.");
                }
                #endregion
            }

            return contactNumber;
        }
        #endregion
        
    }
    
}
