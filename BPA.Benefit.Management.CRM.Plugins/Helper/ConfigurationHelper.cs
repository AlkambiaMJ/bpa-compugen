﻿#region

using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;

#endregion

namespace BPA.Benefit.Management.CRM.Plugins.Helper
{
    public static class ConfigurationHelper
    {
        public static string FetchValue(IOrganizationService service, ITracingService tracingservice,
            string configKeyName)
        {
            string value = string.Empty;

            QueryExpression queryExpression = new QueryExpression
            {
                EntityName = "bpa_configuration",
                ColumnSet = new ColumnSet(true),
                Criteria =
                {
                    Filters =
                    {
                        new FilterExpression
                        {
                            FilterOperator = LogicalOperator.And,
                            Conditions =
                            {
                                new ConditionExpression("bpa_name", ConditionOperator.Equal, configKeyName)
                            }
                        }
                    }
                }
            };
            EntityCollection configurations = service.RetrieveMultiple(queryExpression);

            if (configurations.Entities.Count <= 0) return value;
            Entity config = configurations.Entities[0];
            if (config.Attributes.Contains("bpa_value"))
                value = config.GetAttributeValue<string>("bpa_value");
            return value;
        }

        public static string FetchValueId(IOrganizationService service, string keyName)
        {
            string valueId = string.Empty;

            // Get All Contributions for related Submission
            QueryExpression queryExpression = new QueryExpression
            {
                EntityName = "bpa_configuration",
                ColumnSet = new ColumnSet(true),
                Criteria =
                {
                    Filters =
                    {
                        new FilterExpression
                        {
                            FilterOperator = LogicalOperator.And,
                            Conditions =
                            {
                                new ConditionExpression("bpa_name", ConditionOperator.Equal, keyName)
                            }
                        }
                    }
                }
            };

            EntityCollection configurations = service.RetrieveMultiple(queryExpression);

            if (configurations.Entities.Count <= 0) return valueId;
            Entity configuration = configurations.Entities[0];
            if (configuration.Attributes.Contains("bpa_valueid"))
                valueId = configuration.GetAttributeValue<string>("bpa_valueid");
            return valueId;
        }


        public static void UpdateNextSINNumber(IOrganizationService service, string SIN)
        {
            
            QueryExpression query = new QueryExpression(PluginHelper.BpaConfiguration);
            query.Criteria.AddCondition("statuscode", ConditionOperator.Equal, 1);
            query.Criteria.AddCondition("bpa_name", ConditionOperator.Equal, PluginHelper.SINNumber);
            query.ColumnSet = new ColumnSet("bpa_value", "bpa_prefix");

            EntityCollection ec = service.RetrieveMultiple(query);
            if (ec.Entities.Count != 0)
            {
                // Take the first result
                Entity config = ec.Entities[0];

                // Update the Configuration
                config["bpa_value"] = SIN;
                service.Update(config);
            }
            else
                throw new InvalidPluginExecutionException(
                    "Configuration Entity is missing.  Please add a Configuration record and a Next Contact Number.");
        }

    }
}