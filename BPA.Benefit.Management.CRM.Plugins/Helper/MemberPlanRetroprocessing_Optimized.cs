﻿/*******************************************
 * IMPORTANT NOTES: 
 * 
 * WE ARE NOT USING 'SELFPAY MAX REACH TRANSACION'  ANY MORE. IT WILL NOT CREATE A THIS TYPE OF TRANSCATION. 
 * IF MAX LMIT REACH IT WILL CREATE 'NOT IN BENEFIT' AND LEAVE THE MONEY INTO THE BANK
 ******************************************* */

#region

using System;
using System.Activities;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Sdk.Messages;
using System.ServiceModel;

#endregion

namespace BPA.Benefit.Management.CRM.Plugins.Helper
{
    public class MemberPlanRetroprocessing
    {
        public static string RecalculateTransactions(IPluginExecutionContext context, IServiceProvider serviceProvider, IOrganizationService service, ITracingService tracingService,
            string crmAdminUserId, bool executeMultiple, EntityReference memberPlanRef, DateTime startWorkMonth, bool isMemberCategoryChanged, bool isRunFromMonthlyTrigger, EntityReference eligibilityCategory)
        {
            tracingService.Trace("Inside MemberPlanRetroprocessing.RecalculateTransactions");

            if (startWorkMonth.Day > 1)
                startWorkMonth = new DateTime(startWorkMonth.Year, startWorkMonth.Month, 1);


            // TESTING - Record the processing start time
            DateTime retroprocessingStartTime = DateTime.Now;

            // Create the Transaction Request object to hold all of our changes in
            ExecuteTransactionRequest requestRetroprocessing = null;
            requestRetroprocessing = new ExecuteTransactionRequest()
            {
                // Create an empty organization request collection.
                Requests = new OrganizationRequestCollection(),
                ReturnResponses = true
            };

            // UpsertRequest

            // Get the MemberPlan record
            Entity memberPlan = service.Retrieve(PluginHelper.BpaMemberplan, memberPlanRef.Id, new ColumnSet(true));
            if (memberPlan == null)
            {
                // Raise an error that MemberPlan record is null
                throw new InvalidPluginExecutionException(OperationStatus.Failed,
                    @"Cannot retrieve MemberPlan record.");
            }

            // Get Current Eligibility from Member Plan
            EntityReference currentEligibilityCategoryRef = memberPlan.GetAttributeValue<EntityReference>("bpa_eligibilitycategoryid");

            // Get the plan for the Member Plan
            EntityReference planRef = memberPlan.GetAttributeValue<EntityReference>("bpa_planid");
            Entity plan = service.Retrieve(PluginHelper.BpaSector, planRef.Id, new ColumnSet(true));

            if (plan == null)
            {
                // Raise an error that Plan record is null
                throw new InvalidPluginExecutionException(OperationStatus.Failed,
                    @"Cannot retrieve Plan record.");
            }

            // Validate plan is Benefit Plan 
            bool isBenefitPlan = plan.GetAttributeValue<bool>("bpa_benefitplan");
            if (!isBenefitPlan)
            {
                tracingService.Trace("Member's Plan is not Benefit Plan");
                return string.Empty;
            }
            // Get the Plan owner and use that user/team as the owner of the records created
            EntityReference owningUserTeamRef = plan.GetAttributeValue<EntityReference>("ownerid");

            // Get all Eligibility Categories for this Member's Plan
            EntityCollection eligibilityCategories = MemberPlanRetroprocessingHelper_Optimized.FetchAllEligibilityCategoryByPlan(service, tracingService, planRef.Id);
            if (eligibilityCategories.Entities.Count == 0)
            {
                // Raise an error that Eligibility Categories record is null
                throw new InvalidPluginExecutionException(OperationStatus.Failed,
                    @"Cannot retrieve Eligibility Category records.");
            }

            // Get all Eligibility Category Details for this Member's Plan
            EntityCollection eligibilityCategoryDetails = MemberPlanRetroprocessingHelper_Optimized.FetchAllEligibilityDetails(service, tracingService, planRef.Id);
            if (eligibilityCategoryDetails.Entities.Count == 0)
            {
                // Raise an error that Eligibility Categories record is null
                throw new InvalidPluginExecutionException(OperationStatus.Failed,
                    @"Cannot retrieve Eligibility Category Detail records.");
            }

            // Get Maximum Work Month form Transaction
            DateTime lastWorkMonth = DateTime.Today.AddMonths(-1);
            lastWorkMonth = new DateTime(lastWorkMonth.Year, lastWorkMonth.Month, 1);
            
            // Declared all variables
            List<Guid> skipTransactions = null;
            bool isMemberBecomeInActive = false;
            int selfPayCounter = 0, inactiveMonthCounter = 0, maxInactiveCounter = 0;

            // Variables to hold the current member status shown on the Member Plan record
            int memberPlanCurrentEligibilityValue = 0;
            DateTime memberPlanBenefitMonth = DateTime.MinValue;

            // Find Default Welfare Fund for Member Plan
            EntityReference defaultFundRef = null;
            defaultFundRef = MemberPlanRetroprocessingHelper_Optimized.FetchDefaultFundByMemberPlan(service, tracingService, memberPlanRef.Id);
            if (defaultFundRef == null)
                defaultFundRef = new EntityReference(PluginHelper.BpaFund, Guid.Empty);

            // Select all relevant transactions
            EntityCollection transactions = MemberPlanRetroprocessingHelper_Optimized.FetchAllRelevantTransactions(service, tracingService, memberPlanRef.Id);
            EntityCollection M_transactions = MemberPlanRetroprocessingHelper_Optimized.FetchAllRelevantTransactions(service, tracingService, memberPlanRef.Id); 

            //Get the first transaction
            Entity firstTransaction = null;
            if (transactions != null && transactions.Entities.Count > 0)
            {
                firstTransaction = transactions.Entities.OrderBy(x => x.Attributes["bpa_transactiondate"]).FirstOrDefault();
                DateTime firstWorkMonth = firstTransaction.Contains("bpa_transactiondate") ? firstTransaction.GetAttributeValue<DateTime>("bpa_transactiondate") : DateTime.MinValue;

                if (startWorkMonth < firstWorkMonth)
                    startWorkMonth = firstWorkMonth;

                if (startWorkMonth.Day > 1)
                    startWorkMonth = new DateTime(startWorkMonth.Year, startWorkMonth.Month, 1);
            }

            #region ---- DELETE ALL ELIGIBILITY TRANSACTIONS STARTING AT START WORK MONTH ----

            // If there are any transactions
            if (transactions != null && transactions.Entities.Count > 0)
            {
                List<Entity> transactionsList = transactions.Entities.ToList();

                // Delete all eligibility transactions
                foreach (Entity transaction in transactionsList)
                {
                    // Filter out all transactions below start work month
                    DateTime transactionWorkMonth = transaction.GetAttributeValue<DateTime>("bpa_transactiondate");
                    if (transactionWorkMonth >= startWorkMonth)
                    {
                        // Get the transaction type
                        int transactionType = transaction.GetAttributeValue<OptionSetValue>("bpa_transactiontype").Value;

                        // See if this transaction type is one that should be deleted
                        if ((transactionType == (int)BpaTransactionbpaTransactionType.EmployerContribution) ||
                            (transactionType == (int)BpaTransactionbpaTransactionType.DollarBankAdjustment) ||
                            (transactionType == (int)BpaTransactionbpaTransactionType.HourBankAdjustment) ||
                            (transactionType == (int)BpaTransactionbpaTransactionType.SecondaryDollarBankAdjustment))
                        {
                            // Do not do anything.  Skip to the next transaction
                        }
                        else if ((transactionType == (int)BpaTransactionbpaTransactionType.EligibilityDraw) ||
                            (transactionType == (int)BpaTransactionbpaTransactionType.ReserveTransfer) ||
                            (transactionType == (int)BpaTransactionbpaTransactionType.InBenefit) ||
                            (transactionType == (int)BpaTransactionbpaTransactionType.NotInBenefit) ||
                            (transactionType == (int)BpaTransactionbpaTransactionType.FundAssisted) ||
                            (transactionType == (int)BpaTransactionbpaTransactionType.FundAssistedInBenefit) ||
                            (transactionType == (int)BpaTransactionbpaTransactionType.UnderReview) ||
                            (transactionType == (int)BpaTransactionbpaTransactionType.NewMember) ||
                            (transactionType == (int)BpaTransactionbpaTransactionType.ReInstating) ||
                            (transactionType == (int)BpaTransactionbpaTransactionType.SelfPay) ||
                            (transactionType == (int)BpaTransactionbpaTransactionType.SelfPayMaxConsecutiveReach) ||
                            (transactionType == (int)BpaTransactionbpaTransactionType.SelfPayInBenefit) ||
                            (transactionType == (int)BpaTransactionbpaTransactionType.SelfPayDraw) ||
                            (transactionType == (int)BpaTransactionbpaTransactionType.Inactive) ||
                            (transactionType == (int)BpaTransactionbpaTransactionType.Frozen) ||
                            (transactionType == (int)BpaTransactionbpaTransactionType.SecondaryDollarBankDraw))
                        {
                            if (transactionType == (int)BpaTransactionbpaTransactionType.Frozen)
                            {
                                // Set the Current Eligibility values
                                //memberPlanCurrentEligibilityValue = (int)MemberPlanBpaCurrentEligibility.Frozen;
                                memberPlanBenefitMonth = DateTime.MinValue;
                            }
                            else if ((transactionType == (int)BpaTransactionbpaTransactionType.Inactive) || (transactionType == (int)BpaTransactionbpaTransactionType.ReserveTransfer))
                            {
                                List<Entity> lsEC = transactionsList.Where(x =>
                                           (x.GetAttributeValue<OptionSetValue>("bpa_transactiontype").Value == (int)BpaTransactionbpaTransactionType.EmployerContribution) &&
                                           (x.GetAttributeValue<OptionSetValue>("bpa_transactiontype").Value == (int)BpaTransactionbpaTransactionType.Inactive) &&
                                           (x.GetAttributeValue<OptionSetValue>("bpa_transactiontype").Value == (int)BpaTransactionbpaTransactionType.ReserveTransfer)).ToList();

                                bool reciprocaltransfer = false;
                                if (transactionType == (int)BpaTransactionbpaTransactionType.ReserveTransfer)
                                    reciprocaltransfer = transaction.GetAttributeValue<bool>("bpa_reciprocaltransfer");

                                if (!reciprocaltransfer)
                                {
                                    // Delete the transaction
                                    transactions.Entities.Remove(transaction);
                                    requestRetroprocessing.Requests.Add(MemberPlanRetroprocessingHelper_Optimized.DeleteTransaction(tracingService, transaction.Id));
                                }
                            }
                            else
                            {
                                // Delete the transaction
                                transactions.Entities.Remove(transaction);
                                requestRetroprocessing.Requests.Add(MemberPlanRetroprocessingHelper_Optimized.DeleteTransaction(tracingService, transaction.Id));
                            }
                        }
                    }
                }
            }
            #endregion

            // Get the Bank Totals from the start of retroprocessing
            Dictionary<string, decimal> bankBalances = MemberPlanRetroprocessingHelper_Optimized.FetchBankBalances(transactions, tracingService, memberPlanRef.Id, startWorkMonth, defaultFundRef.Id);

            // Find last Eligibility Date
            DateTime lastEligibilityDate = DateTime.MinValue;
            Entity lastEligibility = MemberPlanRetroprocessingHelper_Optimized.FetchLastBenefitMonth(tracingService, transactions, startWorkMonth, memberPlan.Id);
            if (lastEligibility != null && lastEligibility.Attributes.Contains("bpa_transactiondate"))
            {
                lastEligibilityDate = lastEligibility.GetAttributeValue<DateTime>("bpa_transactiondate");
            }


            // Get Previous Month Eligibility Values
            int currentEligibility = 0;
            Entity previousMonthEligibility = MemberPlanRetroprocessingHelper_Optimized.FetchPreviousMonthEligibility(tracingService, transactions, memberPlanRef.Id, startWorkMonth.AddMonths(-1));
            int previousMonthEligibilityType = 0;
            
            //Get Current Eligibility from Member Plan
            Entity MemberPlanInfo = MemberPlanRetroprocessingHelper_Optimized.FetchMemberPlan(service, tracingService, memberPlanRef.Id);
            OptionSetValue currentEligibilityValue = new OptionSetValue(0);
            if (MemberPlanInfo.Contains("bpa_currenteligibility"))
                currentEligibilityValue = MemberPlanInfo.GetAttributeValue<OptionSetValue>("bpa_currenteligibility");
            DateTime firstMonthOfFrozen = MemberPlanRetroprocessingHelper_Optimized.FetchFirstMonthByMemberPlanByTransationType(service, tracingService, memberPlanRef.Id,
                (int)BpaTransactionbpaTransactionType.Frozen);

            #region ---- SELF PAY COUNTER ----
            // Find out Max Month of Employer Contribution (if not found return WorkMonth)
            //DateTime employerContributionMaxDate = MemberPlanRetroprocessingHelper_Optimized.FetchMaxEmployerContributionDate(tracingService, transactions, memberPlan.Id, startWorkMonth);
            DateTime ECMaxDate = TransactionHelper.FetchMaxEmployerContributionDate(service, tracingService, memberPlan.Id, startWorkMonth);

            // Find out how many previous SelfPay there are
            //Entity lastEC = MemberPlanRetroprocessingHelper_Optimized.FetchLastEligiblityMonthExceptSelfPay(tracingService, transactions,
            //    memberPlan.Id, startWorkMonth, employerContributionMaxDate);
            //Entity lastEC = RecalculationTransactionHelper.FetchLastEligiblityMonthExceptSelfPay1(service, tracingService,
            //            ECMaxDate, memberPlan.Id);

            //if (lastEC != null && lastEC.Attributes.Contains("bpa_transactiondate"))
            //{
            //    DateTime end = startWorkMonth.AddMonths(-1);
            //    DateTime start = lastEC.GetAttributeValue<DateTime>("bpa_transactiondate");
            //    selfPayCounter = end.Month + end.Year * 12 - (start.Month + start.Year * 12);
            //}
            selfPayCounter = 0;
            if (selfPayCounter == 0)
            {
                //selfPayCounter = MemberPlanRetroprocessingHelper_Optimized.GetHowManySelfPay(tracingService, transactions, employerContributionMaxDate, startWorkMonth, memberPlan.Id);
                selfPayCounter = RecalculationTransactionHelper.GetHowManySelfPay(service, tracingService, ECMaxDate, startWorkMonth, memberPlan.Id);
            }
            #endregion

            #region ---- START WORK MONTH LOOP ----
            List<DateTime> finalReservedWorkMonth = new List<DateTime>();

            // Loop through from start date to end date
            while (startWorkMonth <= lastWorkMonth)
            {
                skipTransactions = new List<Guid>();

                // Find how many eligibility contribution transactions there are.  If more than 1, then get the last and ignore the others
                EntityCollection eligibilityContributionTransactions =
                    MemberPlanRetroprocessingHelper_Optimized.FetchAllEligibilityDrawTransactions(tracingService, transactions, memberPlanRef.Id,
                        startWorkMonth, (int)BpaTransactionbpaTransactionType.EmployerContribution);

                if (eligibilityContributionTransactions != null && eligibilityContributionTransactions.Entities.Count > 1)
                {
                    int iCnt = 0;
                    foreach (Entity t in eligibilityContributionTransactions.Entities)
                    {
                        if (iCnt != 0)
                            skipTransactions.Add(t.Id);
                        iCnt++;
                    }
                }

                #region ---- GET PREVIOUS MONTH ELIGIBILITY VALUES AND SET TO CURRENT ELIGIBILITY ----

                if (currentEligibilityValue.Value == (int)MemberPlanBpaCurrentEligibility.Frozen)
                {
                    if(startWorkMonth >= firstMonthOfFrozen)
                        memberPlanCurrentEligibilityValue = (int)MemberPlanBpaCurrentEligibility.Frozen;
                }
                
                if (memberPlanCurrentEligibilityValue == 0)
                {
                    if (previousMonthEligibility != null)
                    {
                        previousMonthEligibilityType = previousMonthEligibility.GetAttributeValue<OptionSetValue>("bpa_transactiontype").Value;

                        // Put the eligibility in for checking
                        switch (previousMonthEligibilityType)
                        {
                            case (int)BpaTransactionbpaTransactionType.InBenefit:
                                currentEligibility = (int)MemberPlanBpaCurrentEligibility.InBenefit;
                                break;
                            case (int)BpaTransactionbpaTransactionType.NewMember:
                                currentEligibility = (int)MemberPlanBpaCurrentEligibility.NewMember;
                                break;
                            case (int)BpaTransactionbpaTransactionType.NotInBenefit:
                                currentEligibility = (int)MemberPlanBpaCurrentEligibility.NotInBenefit;
                                break;
                            case (int)BpaTransactionbpaTransactionType.FundAssistedInBenefit:
                                currentEligibility = (int)MemberPlanBpaCurrentEligibility.InBenefit;
                                break;
                            case (int)BpaTransactionbpaTransactionType.SelfPayInBenefit:
                                currentEligibility = (int)MemberPlanBpaCurrentEligibility.SelfPayInBenefit;
                                break;
                            case (int)BpaTransactionbpaTransactionType.UnderReview:
                                currentEligibility = (int)MemberPlanBpaCurrentEligibility.UnderReview;
                                break;
                            case (int)BpaTransactionbpaTransactionType.TransferOut:
                                currentEligibility = (int)MemberPlanBpaCurrentEligibility.TransferOut;
                                break;
                            case (int)BpaTransactionbpaTransactionType.ReInstating:
                                currentEligibility = (int)MemberPlanBpaCurrentEligibility.Reinstatement;
                                break;
                            case (int)BpaTransactionbpaTransactionType.Frozen:
                                currentEligibility = (int)MemberPlanBpaCurrentEligibility.Frozen;
                                break;
                        }
                    }
                    else
                    {
                        currentEligibility = (int)MemberPlanBpaCurrentEligibility.NewMember;
                    }
                }
                else
                {
                    currentEligibility = memberPlanCurrentEligibilityValue;
                }
                
                #endregion

                // Fetch All Transactions for that month
                EntityCollection transactionsCurrentWorkMonth = MemberPlanRetroprocessingHelper_Optimized.FetchAllTransactionsForWorkMonth(tracingService, transactions, memberPlanRef.Id, startWorkMonth);

                // Find Member Eligibility Category
                EntityReference EligibilityCategory = null;
                if (isMemberCategoryChanged)
                    EligibilityCategory = eligibilityCategory;
                else
                    EligibilityCategory = MemberPlanRetroprocessingHelper_Optimized.FetchCurrentEligibilityCategory(tracingService, M_transactions, memberPlan, startWorkMonth);
                EntityReference usingEligibilityCategory = EligibilityCategory;


                #region ---- TRANSACTION TYPE = EMPLOYER CONTRIBUTION UPDATE ELIGIBILITY CATEGORY ----

                if (isMemberCategoryChanged)
                {
                    List<Entity> lsEC = transactions.Entities.Where(x =>
                                ((x.GetAttributeValue<OptionSetValue>("bpa_transactiontype").Value == (int)BpaTransactionbpaTransactionType.EmployerContribution) ||
                                 (x.GetAttributeValue<OptionSetValue>("bpa_transactiontype").Value == (int)BpaTransactionbpaTransactionType.HourBankAdjustment) ||
                                 (x.GetAttributeValue<OptionSetValue>("bpa_transactiontype").Value == (int)BpaTransactionbpaTransactionType.DollarBankAdjustment) ||
                                 (x.GetAttributeValue<OptionSetValue>("bpa_transactiontype").Value == (int)BpaTransactionbpaTransactionType.TransferIn) ||
                                 (x.GetAttributeValue<OptionSetValue>("bpa_transactiontype").Value == (int)BpaTransactionbpaTransactionType.Reciprocal) ||
                                 (x.GetAttributeValue<OptionSetValue>("bpa_transactiontype").Value == (int)BpaTransactionbpaTransactionType.ReserveTransfer) ||
                                 (x.GetAttributeValue<OptionSetValue>("bpa_transactiontype").Value == (int)BpaTransactionbpaTransactionType.SelfPayDeposit) ||
                                 (x.GetAttributeValue<OptionSetValue>("bpa_transactiontype").Value == (int)BpaTransactionbpaTransactionType.ReInstating) ) &&
                                 (x.GetAttributeValue<DateTime>("bpa_transactiondate").ToShortDateString() == startWorkMonth.ToShortDateString())).ToList();
                    if (lsEC != null && lsEC.Count > 0)
                    {
                        foreach (Entity e in lsEC)
                            requestRetroprocessing.Requests.Add(MemberPlanRetroprocessingHelper_Optimized.UpdateEligbilityCategory(tracingService, e.Id, usingEligibilityCategory));
                    }
                }

                #endregion

                List<Entity> transactionsList = null;
                bool hasECED = false,
                    hasReciprocalTransferOut = false,
                    hasReciprocalTransferIn = false,
                    isTransferAmountPaid = false,
                    hasTransfer = false,
                    isReciprocalTransferOutDelete = false,
                    hasSelfPayInBenefit = false,
                    memberWentToUnderReview = false;
                int eligibilityCategoryCalculationType = 0;

                // Find Last Agreement of Employer Contribution OR Self Pay Deposit Received
                EntityReference lastAgreement = MemberPlanRetroprocessingHelper_Optimized.FetchLastContributionAgreement(tracingService, transactions, memberPlan.Id, startWorkMonth);

                #region ---- IF THERE ARE ANY TRANSACTIONS ----

                // Check if Transactions Count is More than 0
                if (transactionsCurrentWorkMonth != null && transactionsCurrentWorkMonth.Entities.Count > 0)
                {
                    // Convert Entities Collection to List
                    transactionsList = transactionsCurrentWorkMonth.Entities.ToList();

                    #region ---- SETTING ALL BOOLEAN VARIABLES ----

                    // Does this list has Employer Contribution
                    List<Entity> doesNotHaveECED = transactionsList.Where(x =>
                                (x.GetAttributeValue<OptionSetValue>("bpa_transactiontype").Value == (int)BpaTransactionbpaTransactionType.EmployerContribution) ||
                                (x.GetAttributeValue<OptionSetValue>("bpa_transactiontype").Value == (int)BpaTransactionbpaTransactionType.HourBankAdjustment) ||
                                (x.GetAttributeValue<OptionSetValue>("bpa_transactiontype").Value == (int)BpaTransactionbpaTransactionType.DollarBankAdjustment)).ToList();
                    if (doesNotHaveECED != null && doesNotHaveECED.Count > 0)
                        hasECED = true;

                    // Not sure it execute this condition or not?
                    List<Entity> lsFundAssistedAndEmployeeContributions = transactionsList.Where(x =>
                                (x.GetAttributeValue<OptionSetValue>("bpa_transactiontype").Value == (int)BpaTransactionbpaTransactionType.EmployerContribution) ||
                                (x.GetAttributeValue<OptionSetValue>("bpa_transactiontype").Value == (int)BpaTransactionbpaTransactionType.FundAssisted)).ToList();
                    if (lsFundAssistedAndEmployeeContributions != null && lsFundAssistedAndEmployeeContributions.Count >= 2)
                    {
                        foreach (Entity e in lsFundAssistedAndEmployeeContributions)
                        {
                            int transactionType1 = e.GetAttributeValue<OptionSetValue>("bpa_transactiontype").Value;

                            if (transactionType1 == (int)BpaTransactionbpaTransactionType.FundAssisted)
                            {
                                skipTransactions.Add(e.Id);
                                break;
                            }
                        }
                    }

                    // Check this month has Self-Pay Benefit or not?
                    List<Entity> ListOfSelfPayAndSelfPayRefund = transactionsList.Where(x =>
                        x.GetAttributeValue<OptionSetValue>("bpa_transactiontype").Value == (int)BpaTransactionbpaTransactionType.SelfPayInBenefit).ToList();
                    if (ListOfSelfPayAndSelfPayRefund != null && ListOfSelfPayAndSelfPayRefund.Count > 0)
                        hasSelfPayInBenefit = true;
                    else
                        hasSelfPayInBenefit = false;

                    // Check this month has FUND ASSISTED or NOT?
                    bool hasFundAssisted = false;
                    List<Entity> listOfFundAssisted = transactionsList.Where(x =>
                        x.GetAttributeValue<OptionSetValue>("bpa_transactiontype").Value == (int)BpaTransactionbpaTransactionType.FundAssisted).ToList();
                    if (listOfFundAssisted != null && listOfFundAssisted.Count > 0)
                        hasFundAssisted = true;
                    else
                        hasFundAssisted = false;

                    #endregion

                    #region ---- MEMBER TRANSFER IN OR OUT? ----
                    
                    // Does this list has Transfer Out Transaction
                    List<Entity> listOfReciprocalTransferOut = transactionsList.Where(x =>
                                x.GetAttributeValue<OptionSetValue>("bpa_transactiontype").Value == (int)BpaTransactionbpaTransactionType.TransferOut).ToList();
                    if (listOfReciprocalTransferOut != null && listOfReciprocalTransferOut.Count > 0)
                        hasReciprocalTransferOut = true;
                    else
                        hasReciprocalTransferOut = false;

                    // Does this list has Transfer Transaction
                    List<Entity> listOfTransfer = transactionsList.Where(x =>
                                x.GetAttributeValue<OptionSetValue>("bpa_transactiontype").Value == (int)BpaTransactionbpaTransactionType.Reciprocal).ToList();
                    if (listOfTransfer != null && listOfTransfer.Count > 0)
                    {
                        hasTransfer = true;
                        Entity transactionPaid = listOfTransfer[0];
                        isTransferAmountPaid = transactionPaid.GetAttributeValue<Boolean>("bpa_reciprocaltransfer");
                    }
                    else
                    {
                        isTransferAmountPaid = false;
                        hasTransfer = false;
                    }

                    // Does this list has Transfer In Transactions
                    List<Entity> listOfReciprocalTransferIn = transactionsList.Where(x =>
                                x.GetAttributeValue<OptionSetValue>("bpa_transactiontype").Value == (int)BpaTransactionbpaTransactionType.TransferIn).ToList();
                    if (listOfReciprocalTransferIn != null && listOfReciprocalTransferIn.Count > 0)
                        hasReciprocalTransferIn = true;
                    else
                        hasReciprocalTransferIn = false;

                    #endregion

                    #region ---- CLEANUP TRANSFER OUT/IN TRANSACTIONS ----

                    // Cleanup the transfer out and in transactions that are not covered in the initial cleanup
                    foreach (Entity transaction in transactionsCurrentWorkMonth.Entities)
                    {
                        int transactionType = transaction.GetAttributeValue<OptionSetValue>("bpa_transactiontype").Value;

                        if ((transactionType == (int)BpaTransactionbpaTransactionType.EligibilityDraw) ||
                            (transactionType == (int)BpaTransactionbpaTransactionType.ReserveTransfer) ||
                            (transactionType == (int)BpaTransactionbpaTransactionType.InBenefit) ||
                            (transactionType == (int)BpaTransactionbpaTransactionType.NotInBenefit) ||
                            (transactionType == (int)BpaTransactionbpaTransactionType.FundAssisted) ||
                            (transactionType == (int)BpaTransactionbpaTransactionType.FundAssistedInBenefit) ||
                            (transactionType == (int)BpaTransactionbpaTransactionType.UnderReview) ||
                            (transactionType == (int)BpaTransactionbpaTransactionType.NewMember) ||
                            (transactionType == (int)BpaTransactionbpaTransactionType.ReInstating) ||
                            (transactionType == (int)BpaTransactionbpaTransactionType.SelfPay) ||
                            (transactionType == (int)BpaTransactionbpaTransactionType.SelfPayMaxConsecutiveReach) ||
                            (transactionType == (int)BpaTransactionbpaTransactionType.SelfPayInBenefit) ||
                            (transactionType == (int)BpaTransactionbpaTransactionType.SelfPayDraw) ||
                            (transactionType == (int)BpaTransactionbpaTransactionType.Inactive) ||
                            (transactionType == (int)BpaTransactionbpaTransactionType.Frozen) ||
                            (transactionType == (int)BpaTransactionbpaTransactionType.TransferOut) ||
                            (transactionType == (int)BpaTransactionbpaTransactionType.Reciprocal) ||
                            (transactionType == (int)BpaTransactionbpaTransactionType.SecondaryDollarBankDraw))
                        {
                           if (transactionType == (int)BpaTransactionbpaTransactionType.TransferOut)
                            {
                                if (hasReciprocalTransferOut && hasTransfer && !isTransferAmountPaid)
                                {
                                    isReciprocalTransferOutDelete = true;
                                    transactions.Entities.Remove(transaction);
                                    requestRetroprocessing.Requests.Add(MemberPlanRetroprocessingHelper_Optimized.DeleteTransaction(tracingService, transaction.Id));
                                }
                                else if (hasReciprocalTransferOut && !hasTransfer && !isTransferAmountPaid)
                                {
                                    isReciprocalTransferOutDelete = true;
                                    transactions.Entities.Remove(transaction);
                                    requestRetroprocessing.Requests.Add(MemberPlanRetroprocessingHelper_Optimized.DeleteTransaction(tracingService, transaction.Id));
                                }
                                else if (hasReciprocalTransferOut && hasTransfer && isTransferAmountPaid)
                                {
                                    isReciprocalTransferOutDelete = false;
                                    transactions.Entities.Remove(transaction);
                                    requestRetroprocessing.Requests.Add(MemberPlanRetroprocessingHelper_Optimized.DeleteTransaction(tracingService, transaction.Id));
                                }
                                else
                                {
                                    isReciprocalTransferOutDelete = false;
                                }
                            }
                            else if (transactionType == (int)BpaTransactionbpaTransactionType.Reciprocal)
                            {
                                if (!isTransferAmountPaid)
                                {
                                    transactions.Entities.Remove(transaction);
                                    requestRetroprocessing.Requests.Add(MemberPlanRetroprocessingHelper_Optimized.DeleteTransaction(tracingService, transaction.Id));
                                }
                                else
                                {
                                    // Using Bank Balances now that are kept track of in variables instead of querying the database
                                    // Update Transaction
                                    Entity t = new Entity("bpa_transaction")
                                    {
                                        Id = transaction.Id
                                    };
                                    if (bankBalances.ContainsKey("hours"))
                                        t["bpa_endinghourbalance"] = bankBalances["hours"];
                                    if (bankBalances.ContainsKey("dollarhours"))
                                        t["bpa_endingdollarbalance"] = bankBalances["dollarhours"];
                                    if (bankBalances.ContainsKey("selfpaydollar"))
                                        t["bpa_endingselfpaybalance"] = new Money(bankBalances["selfpaydollar"]);
                                    if (bankBalances.ContainsKey("secondarydollar"))
                                        t["bpa_secondarydollarbalance"] = new Money(bankBalances["secondarydollar"]);

                                    // Add the update to the list of requests to process
                                    UpdateRequest updateBankBalances = new UpdateRequest { Target = t };
                                    requestRetroprocessing.Requests.Add(updateBankBalances);
                                }
                            }
                            else if(transactionType == (int)BpaTransactionbpaTransactionType.Frozen)
                            {
                                memberPlanCurrentEligibilityValue = (int)MemberPlanBpaCurrentEligibility.Frozen;
                                // Using Bank Balances now that are kept track of in variables instead of querying the database
                                // Update Transaction
                                Entity t = new Entity("bpa_transaction")
                                {
                                    Id = transaction.Id
                                };
                                if (bankBalances.ContainsKey("hours"))
                                    t["bpa_endinghourbalance"] = bankBalances["hours"];
                                if (bankBalances.ContainsKey("dollarhours"))
                                    t["bpa_endingdollarbalance"] = bankBalances["dollarhours"];
                                if (bankBalances.ContainsKey("selfpaydollar"))
                                    t["bpa_endingselfpaybalance"] = new Money(bankBalances["selfpaydollar"]);
                                if (bankBalances.ContainsKey("secondarydollar"))
                                    t["bpa_secondarydollarbalance"] = new Money(bankBalances["secondarydollar"]);

                                // Add the update to the list of requests to process
                                UpdateRequest updateBankBalances = new UpdateRequest { Target = t };
                                requestRetroprocessing.Requests.Add(updateBankBalances);
                            }
                            else
                            {
                                transactions.Entities.Remove(transaction);
                                requestRetroprocessing.Requests.Add(MemberPlanRetroprocessingHelper_Optimized.DeleteTransaction(tracingService, transaction.Id));
                            }
                        }
                    }

                    #endregion

                    #region ---- RECALCULATION LOGIC ----
                    // Get all eligibility category values
                    int eligibilityOffset = 0;
                    bool isEligiblityHours = false;
                    if (usingEligibilityCategory != null)
                    {
                        Entity eligibilityCategoryInformation = eligibilityCategories.Entities.Where(x => x.Id == usingEligibilityCategory.Id).ToList().FirstOrDefault();
                        if (eligibilityCategoryInformation.Contains("bpa_planeligibilitytype"))
                            eligibilityCategoryCalculationType = eligibilityCategoryInformation.GetAttributeValue<OptionSetValue>("bpa_planeligibilitytype").Value;

                        if (eligibilityCategoryInformation.Contains("bpa_terminationcycle"))
                            maxInactiveCounter = eligibilityCategoryInformation.GetAttributeValue<int>("bpa_terminationcycle");

                        if (eligibilityCategoryInformation.Contains("bpa_eligibilityoffsetmonths"))
                            eligibilityOffset = eligibilityCategoryInformation.GetAttributeValue<int>("bpa_eligibilityoffsetmonths");
                    }

                    // Is Eligibility Category Hours or Dollar?
                    if (eligibilityCategoryCalculationType == (int)BpaEligibilityCategoryPlanEligiblityType.Hours)
                        isEligiblityHours = true;

                    Dictionary<string, decimal> updatedBankBalances = null;
                    List<DateTime> reservedWorkMonth = finalReservedWorkMonth;

                    if (isEligiblityHours)
                    {
                        RetroProcessingHourBase(serviceProvider, service, tracingService, requestRetroprocessing, transactions, eligibilityCategories, 
                            eligibilityCategoryDetails, bankBalances,
                            memberPlan, usingEligibilityCategory, currentEligibility, previousMonthEligibility, startWorkMonth, defaultFundRef, false, 0,
                           hasSelfPayInBenefit, hasECED, lastAgreement, owningUserTeamRef, out memberWentToUnderReview, ref selfPayCounter, 
                           out inactiveMonthCounter, out isMemberBecomeInActive, out memberPlanCurrentEligibilityValue, out memberPlanBenefitMonth,
                           crmAdminUserId, out updatedBankBalances, ref lastEligibilityDate, out reservedWorkMonth);
                    }
                    else
                    {
                        RetroProcessingDollarBase(serviceProvider, service, tracingService, requestRetroprocessing, transactions, eligibilityCategories,
                            eligibilityCategoryDetails, bankBalances,
                            memberPlan, usingEligibilityCategory, currentEligibility, previousMonthEligibility, startWorkMonth, defaultFundRef, false, 0,
                            hasSelfPayInBenefit, hasECED, lastAgreement, owningUserTeamRef, out memberWentToUnderReview, ref selfPayCounter,
                            out inactiveMonthCounter, out isMemberBecomeInActive, out memberPlanCurrentEligibilityValue, out memberPlanBenefitMonth,
                            crmAdminUserId, out updatedBankBalances, ref lastEligibilityDate, out reservedWorkMonth);
                    }
                    bankBalances = updatedBankBalances;
                    finalReservedWorkMonth.AddRange(reservedWorkMonth);

                    #endregion
                } 

                #endregion

                #region ---- ELSE THERE IS NO RECORDS ----
                else
                {
                    // Get all eligibility category values
                    int eligibilityOffset = 0;
                    bool isEligiblityHours = false;
                    if (usingEligibilityCategory != null)
                    {
                        Entity eligibilityCategoryInformation = eligibilityCategories.Entities.Where(x => x.Id == usingEligibilityCategory.Id).ToList().FirstOrDefault();
                        if (eligibilityCategoryInformation.Contains("bpa_planeligibilitytype"))
                            eligibilityCategoryCalculationType = eligibilityCategoryInformation.GetAttributeValue<OptionSetValue>("bpa_planeligibilitytype").Value;
                    }

                    // Is Eligibility Category Hours or Dollar??
                    if (eligibilityCategoryCalculationType == (int)BpaEligibilityCategoryPlanEligiblityType.Hours)
                        isEligiblityHours = true;

                    Dictionary<string, decimal> updatedBankBalances = null;
                    List<DateTime> reservedWorkMonth = finalReservedWorkMonth;

                    if (isEligiblityHours)
                    {
                        RetroProcessingHourBase(serviceProvider, service, tracingService, requestRetroprocessing, transactions, eligibilityCategories,
                            eligibilityCategoryDetails, bankBalances,
                            memberPlan, usingEligibilityCategory, currentEligibility, previousMonthEligibility, startWorkMonth, defaultFundRef, false, 0,
                            hasSelfPayInBenefit, hasECED, lastAgreement, owningUserTeamRef, out memberWentToUnderReview, ref selfPayCounter, 
                            out inactiveMonthCounter, out isMemberBecomeInActive, out memberPlanCurrentEligibilityValue, out memberPlanBenefitMonth,
                            crmAdminUserId, out updatedBankBalances, ref lastEligibilityDate, out reservedWorkMonth);
                    }
                    else
                    {
                        RetroProcessingDollarBase(serviceProvider, service, tracingService, requestRetroprocessing, transactions, eligibilityCategories,
                            eligibilityCategoryDetails, bankBalances,
                            memberPlan, usingEligibilityCategory, currentEligibility, previousMonthEligibility, startWorkMonth, defaultFundRef, false, 0,
                            hasSelfPayInBenefit, hasECED, lastAgreement, owningUserTeamRef, out memberWentToUnderReview, ref selfPayCounter,
                            out inactiveMonthCounter, out isMemberBecomeInActive, out memberPlanCurrentEligibilityValue, out memberPlanBenefitMonth,
                            crmAdminUserId, out updatedBankBalances, ref lastEligibilityDate, out reservedWorkMonth);
                    }
                    bankBalances = updatedBankBalances;
                }
                #endregion

                // Skip to the next month if there are future transactions
                if (isMemberBecomeInActive)
                {
                    // Has Future Records
                    if (!MemberPlanRetroprocessingHelper_Optimized.HasFutureTransactions(tracingService, transactions, startWorkMonth, memberPlanRef.Id))
                        break;
                    else
                    {
                        // Find next Workmonth where Employer Contribution or Any Adjustment 
                        startWorkMonth = MemberPlanRetroprocessingHelper_Optimized.FutureActivityMonth(tracingService, transactions, startWorkMonth, memberPlanRef.Id);
                        startWorkMonth = startWorkMonth.AddMonths(-1);
                    }
                }


                //Update Current Benefit Month and Current Month Eligibility
                DateTime calenderMonth = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1);
                memberPlanBenefitMonth = new DateTime(memberPlanBenefitMonth.Year, memberPlanBenefitMonth.Month, 1);
                if (memberPlanBenefitMonth == calenderMonth)
                    requestRetroprocessing.Requests.Add(MemberPlanRetroprocessingHelper_Optimized.SetCurrentMonthEligibility(tracingService, memberPlanRef.Id, memberPlanCurrentEligibilityValue, memberPlanBenefitMonth));

                // Increment to the next work month                
                startWorkMonth = startWorkMonth.AddMonths(1);
                usingEligibilityCategory = null;

                // Get the Bank Totals from the start of retroprocessing
                bankBalances = MemberPlanRetroprocessingHelper_Optimized.UpdateBankBalancesWithTransactions(service, tracingService, transactions,
                    bankBalances, memberPlanRef.Id, defaultFundRef.Id, startWorkMonth, startWorkMonth.AddMonths(1).AddDays(-1), plan.GetAttributeValue<OptionSetValue>("bpa_planeligibilitytype").Value);
                
            } // End of start date to end date loop

            #endregion

            #region ---- UPDATING TOTALS AND STATUSES ON THE MEMBERPLAN RECORD ----
            // Updating totals and statuses on the MemberPlan record

            // Update the Inactive Month counter, Self Pay Counter on the Member Plan
            requestRetroprocessing.Requests.Add(MemberPlanRetroprocessingHelper_Optimized.UpdateConsecutiveMonthTotals(tracingService,
                memberPlan.Id, selfPayCounter, inactiveMonthCounter));

            // Update the current eligibility on the Member Plan
            requestRetroprocessing.Requests.Add(MemberPlanRetroprocessingHelper_Optimized.SetCurrentEligibility(tracingService, memberPlanRef.Id, memberPlanCurrentEligibilityValue, memberPlanBenefitMonth));

            // TESTING - Record the processing database start time
            DateTime retroprocessingDatabaseStartTime = DateTime.Now;

            // Flush out all of the pending transactions at once
            // Switch to CRMAdmin User
            tracingService.Trace("Starting to flush out all pending transactions.");
            IOrganizationServiceFactory serviceFactory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
            IOrganizationService serviceAdmin = serviceFactory.CreateOrganizationService(new Guid(crmAdminUserId));

            // TESTING - record the number of requests
            int requestCount = requestRetroprocessing.Requests.Count;
            tracingService.Trace($"TOTAL REQUEST IN requestRetroprocessing {requestCount}");

            ExecuteTransactionResponse responseRetroprocessing = null;
            if (executeMultiple)
            {
                //responseRetroprocessing = (ExecuteTransactionResponse)serviceAdmin.Execute(requestRetroprocessing);
                try
                {
                    int pageSize = 1000;
                    // TESTING - record the number of requests
                    if (requestCount > pageSize)
                    {
                        tracingService.Trace($"INSIDE if (requestCount > pageSize)");

                        decimal maxPage = Math.Ceiling((decimal)requestCount / pageSize);
                        ExecuteTransactionRequest tmp = new ExecuteTransactionRequest()
                        {
                            // Create an empty organization request collection.
                            Requests = new OrganizationRequestCollection(),
                            ReturnResponses = true
                        };

                        for (int pageNumber = 1; pageNumber <= maxPage; pageNumber++)
                        {
                            tracingService.Trace($"INSIDE LOOP {pageNumber} / {maxPage}");

                            List<OrganizationRequest> listRequest = requestRetroprocessing.Requests.Skip((pageNumber - 1) * pageSize)
                                                                                            .Take(pageSize).ToList();
                            OrganizationRequestCollection orc = new OrganizationRequestCollection();
                            //listRequest.ForEach(orc.Add);
                            // remove all null Request
                            foreach (OrganizationRequest or in listRequest)
                            {
                                if (or != null)
                                    tmp.Requests = orc;
                            }
                            //listRequest.ForEach(orc.Add);

                            //tmp.Requests = orc;
                            ExecuteTransactionResponse responsePostingTransaction = (ExecuteTransactionResponse)serviceAdmin.Execute(tmp);
                        }
                    }
                    else
                    {
                        tracingService.Trace($"INSIDE ELSE OF if (requestCount >  pageSize)");
                        ExecuteTransactionRequest tmp = new ExecuteTransactionRequest()
                        {
                            // Create an empty organization request collection.
                            Requests = new OrganizationRequestCollection(),
                            ReturnResponses = true
                        };
                        List<OrganizationRequest> listRequest = requestRetroprocessing.Requests.ToList();
                        OrganizationRequestCollection orc = new OrganizationRequestCollection();
                        // remove all null Request
                        foreach(OrganizationRequest or in listRequest)
                        {
                            if(or != null)
                                tmp.Requests = orc;
                        }
                        //listRequest.ForEach(orc.Add);
                        
                        ExecuteTransactionResponse responsePostingTransaction = (ExecuteTransactionResponse)serviceAdmin.Execute(requestRetroprocessing);
                    }
                }
                catch (FaultException<OrganizationServiceFault> ex)
                {
                    throw new InvalidPluginExecutionException($@"Update/Delete request failed for the transaction {((ExecuteTransactionFault)(ex.Detail)).FaultedRequestIndex + 1} and the reason being: {ex.Detail.Message}");
                }
            }
            else
            {
                List<OrganizationRequest> listRequest = requestRetroprocessing.Requests.ToList();
                OrganizationRequestCollection orc = new OrganizationRequestCollection();
                //listRequest.ForEach(orc.Add);
                // remove all null Request
                foreach (OrganizationRequest or in listRequest)
                {
                    if (or != null)
                        serviceAdmin.Execute(or);
                }
            }


            // TODO - Loop though all of the transactions to make sure they are complete
            tracingService.Trace("BEFORE Trigger Rollup- MemberPlanRetroprocessing.RecalculateTransactions - ");

            // Trigger Rollup recalculations after Retroprocessing is complete
            PluginHelper.TriggerRollup(serviceAdmin, tracingService, PluginHelper.BpaMemberplan, memberPlanRef.Id, "bpa_contributiondollarbank");
            PluginHelper.TriggerRollup(serviceAdmin, tracingService, PluginHelper.BpaMemberplan, memberPlanRef.Id, "bpa_contributionhourbank");
            PluginHelper.TriggerRollup(serviceAdmin, tracingService, PluginHelper.BpaMemberplan, memberPlanRef.Id, "bpa_selfpaybank");
            PluginHelper.TriggerRollup(serviceAdmin, tracingService, PluginHelper.BpaMemberplan, memberPlanRef.Id, "bpa_secondarydollarbank");

            tracingService.Trace("AFTER Trigger Rollup- MemberPlanRetroprocessing.RecalculateTransactions - ");

            #endregion

            #region ------- Checking is Reserve Transfer Transction Created ------------

            // checking is there any reserve Transfer record Created ?
            // YEs get for that month elibiligyt Trancation and Update Ending Balance

            if (finalReservedWorkMonth != null && finalReservedWorkMonth.Count > 0)
            {
                tracingService.Trace("==================================================");
                tracingService.Trace("Inside Reserve Work Month");
                // Get Eligibility Status Transaction
                foreach (DateTime reserveWorkMonth in finalReservedWorkMonth)
                {
                    EntityCollection tranCollection = TransactionHelper.FetchReserverTransaction(serviceAdmin, tracingService, memberPlan.Id, reserveWorkMonth);
                    if (tranCollection != null && tranCollection.Entities.Count > 0)
                    {
                        tracingService.Trace($@"Total Transaction Collection {tranCollection.Entities.Count}" );

                        foreach (Entity e in tranCollection.Entities)
                        {
                            tracingService.Trace($@"Transaction NAme {e.GetAttributeValue<string>("bpa_name")}");

                            // Get total Balance upto that month 
                            Dictionary<string, decimal> endingBalances = TransactionHelper.FetchEndingBalance(service, tracingService, memberPlan.Id, reserveWorkMonth);
                            Entity update = new Entity(PluginHelper.BpaTransaction)
                            { Id = e.Id };
                            if (endingBalances.ContainsKey("hours"))
                            {
                                tracingService.Trace($@"Hours: {endingBalances["hours"]}");
                                update["bpa_endinghourbalance"] = endingBalances["hours"];
                            }
                            if (endingBalances.ContainsKey("dollarhours"))
                            {
                                tracingService.Trace($@"Dollarhours: {endingBalances["dollarhours"]}");
                                update["bpa_endingdollarbalance"] = endingBalances["dollarhours"];
                            }
                            if (endingBalances.ContainsKey("selfpaydollar"))
                            {
                                tracingService.Trace($@"Selfpay Dollar: {endingBalances["selfpaydollar"]}");
                                update["bpa_endingselfpaybalance"] = new Money(endingBalances["selfpaydollar"]);
                            }
                            if (endingBalances.ContainsKey("secondarydollar"))
                            {
                                tracingService.Trace($@"Secondary Dollar: {endingBalances["secondarydollar"]}");
                                update["bpa_secondarydollarbalance"] = new Money(endingBalances["secondarydollar"]);
                            }

                            serviceAdmin.Update(update);
                        }
                    }
                } 
                tracingService.Trace("==================================================");
                tracingService.Trace("COMPLETED Reserve Work Month");
            }

            #endregion

            #region ---- TIMING TEST REPORTING ----

            // TESTING - Calculate time difference from start processing to end processing time
            DateTime endTime = DateTime.Now;
            double retroprocessingTotalMinutes = endTime.Subtract(retroprocessingStartTime).Minutes;
            double retroprocessingTotalSeconds = endTime.Subtract(retroprocessingStartTime).Seconds;
            double retroprocessingTotalMilliSeconds = endTime.Subtract(retroprocessingStartTime).Milliseconds;

            double retroprocessingDatabaseTotalMinutes = endTime.Subtract(retroprocessingDatabaseStartTime).Minutes;
            double retroprocessingDatabaseTotalSeconds = endTime.Subtract(retroprocessingDatabaseStartTime).Seconds;
            double retroprocessingDatabaseTotalMilliSeconds = endTime.Subtract(retroprocessingDatabaseStartTime).Milliseconds;

            //throw new InvalidPluginExecutionException(OperationStatus.Failed, "Retroprocessing Minutes: " + retroprocessingTotalMinutes.ToString() +
            //    " Seconds: " + retroprocessingTotalSeconds.ToString() +
            //    " Milliseconds: " + retroprocessingTotalMilliSeconds.ToString() + Environment.NewLine +
            //    "Database Minutes: " + retroprocessingDatabaseTotalMinutes.ToString() +
            //    " Seconds: " + retroprocessingDatabaseTotalSeconds.ToString() +
            //    " Milliseconds: " + retroprocessingDatabaseTotalMilliSeconds.ToString() + Environment.NewLine +
            //    "Request Count: " + requestCount + Environment.NewLine);

            string retroTimeing = "Retroprocessing Minutes: " + retroprocessingTotalMinutes.ToString() +
                " Seconds: " + retroprocessingTotalSeconds.ToString() +
                " Milliseconds: " + retroprocessingTotalMilliSeconds.ToString() + Environment.NewLine +
                "Database Minutes: " + retroprocessingDatabaseTotalMinutes.ToString() +
                " Seconds: " + retroprocessingDatabaseTotalSeconds.ToString() +
                " Milliseconds: " + retroprocessingDatabaseTotalMilliSeconds.ToString() + Environment.NewLine +
                "Request Count: " + requestCount + Environment.NewLine;
            tracingService.Trace(retroTimeing);
            return retroTimeing;

            #endregion
        }

        #region ---- HOUR BASE CALCULATIONS ----

        static void RetroProcessingHourBase(IServiceProvider serviceProvider, IOrganizationService service, ITracingService tracingService, 
            ExecuteTransactionRequest requestRetroprocessing, EntityCollection transactions, EntityCollection eligibilityCategories, 
            EntityCollection eligibilityCategoryDetails, Dictionary<string, decimal> bankBalances, Entity memberPlan, 
            EntityReference usingEligibilityCategory, int currentEligibility, Entity previousMonthEligibility, DateTime startWorkMonth,
            EntityReference defaultFundRef, bool isMisMatch, int subSectorCalculationType, bool hasSelfPayInBenefit, bool hasECED, 
            EntityReference lastAgreement, EntityReference owningUserTeamRef, 
            out bool memberWentToUnderReview, ref int selfPayCounter, out int inactiveMonthCounter, out bool isMemberBecomeInActive,
            out int memberPlanCurrentEligibilityValue, out DateTime memberPlanBenefitMonth, string crmAdminUserId,
            out Dictionary<string, decimal> updatedBankBalances, ref DateTime lastEligibilityDate, out List<DateTime> reserveTransactionWorkMonth)
        {
            int planIsHoursBased = (int)BpaEligibilityCategoryPlanEligiblityType.Hours;
            reserveTransactionWorkMonth = new List<DateTime>();
            // Get both Hour Bank and Self Pay Bank
            decimal sumDollarHour = 0;
            if (bankBalances.ContainsKey("hours"))
                sumDollarHour = bankBalances["hours"];

            decimal sumSelfPay = 0;
            if (bankBalances.ContainsKey("selfpaydollar"))
                sumSelfPay = bankBalances["selfpaydollar"];

            Money selfPayBank = new Money(sumSelfPay);
            //selfPayCounter ;
            bool isSelfPayCounterUpdate = false;
            inactiveMonthCounter = 0;
            isMemberBecomeInActive = false;
            memberWentToUnderReview = false;
            memberPlanCurrentEligibilityValue = 0;
            memberPlanBenefitMonth = DateTime.MinValue;
            int countSelfPay = 0;
            bool isReinstating = false;

            // Get all information for Eligibility Category and Eligibility Category Detail
            Entity eligibilityCategory = eligibilityCategories.Entities.Where(x => x.Id == usingEligibilityCategory.Id).ToList().FirstOrDefault();
            Entity eligibilityCategoryDetail = MemberPlanRetroprocessingHelper_Optimized.FetchEligibilityDetail(
                tracingService, eligibilityCategoryDetails, eligibilityCategory.Id, startWorkMonth);

            if (eligibilityCategoryDetail == null)
                throw new InvalidPluginExecutionException(OperationStatus.Failed, @"System could not find Eligibility Category Detail Information for specific work month. Please contact BPA Administrator.");

            int eligibilityOffset = eligibilityCategory.Contains("bpa_eligibilityoffsetmonths") ? eligibilityCategory.GetAttributeValue<int>("bpa_eligibilityoffsetmonths") : 0;
            int maxInactiveCounter = eligibilityCategory.Contains("bpa_terminationcycle") ? eligibilityCategory.GetAttributeValue<int>("bpa_terminationcycle") : 0;

            decimal deductionRate = eligibilityCategoryDetail.Contains("bpa_hoursdeductionrate") ? (eligibilityCategoryDetail.GetAttributeValue<decimal>("bpa_hoursdeductionrate") * -1) : 0;
            decimal selfPayDeductionRate = eligibilityCategoryDetail.Contains("bpa_selfpayamount") ? (eligibilityCategoryDetail.GetAttributeValue<Money>("bpa_selfpayamount").Value * -1) : 0;
            decimal miniInitEligibility = eligibilityCategoryDetail.Contains("bpa_hoursminimuminitialeligibility") ? eligibilityCategoryDetail.GetAttributeValue<decimal>("bpa_hoursminimuminitialeligibility") : 0;
            decimal fundAssistanceAmount = eligibilityCategoryDetail.Contains("bpa_hoursfundassistanceamount") ? (eligibilityCategoryDetail.GetAttributeValue<decimal>("bpa_hoursfundassistanceamount") * -1) : 0;
            int maxMemberBank = eligibilityCategoryDetail.Contains("bpa_hoursmaxmemberbank") ? eligibilityCategoryDetail.GetAttributeValue<int>("bpa_hoursmaxmemberbank") : 0;
            
            // bpa_maximummemberbank - Dollar (Money)
            // bpa_hoursmaxmemberbank - Hour (int)

            // Check if Deduction Rate (Regular Draw) AND Fund Assistance Amount are same then member is Fully Fund Assisted
            if (fundAssistanceAmount == deductionRate)
            {
                #region ---- FUND ASSISTANCE ----

                // Rules is previous month eligibility must be 'IN BENEFIT'
                if ((currentEligibility == (int)MemberPlanBpaCurrentEligibility.InBenefit) || (currentEligibility == (int)MemberPlanBpaCurrentEligibility.SelfPayInBenefit)
                    || (currentEligibility == (int)MemberPlanBpaCurrentEligibility.FundAssisted))
                {
                    // NOTE - Because the fund assistance is not changing a MemberPlan's
                    // bank balance, there is no need to update the banks
                    requestRetroprocessing.Requests.Add(MemberPlanRetroprocessingHelper_Optimized.CreateFundAssistedTransactions(
                        tracingService, memberPlan.Id, bankBalances, startWorkMonth, startWorkMonth.AddMonths(eligibilityOffset), null, 
                        (fundAssistanceAmount * -1),  (int)BpaTransactionbpaTransactionCategory.Eligibility, (int)BpaTransactionbpaTransactionType.FundAssisted, defaultFundRef, 
                        usingEligibilityCategory, defaultFundRef.Id, lastAgreement, owningUserTeamRef, planIsHoursBased));

                    requestRetroprocessing.Requests.Add(MemberPlanRetroprocessingHelper_Optimized.CreateFundAssistedTransactions(
                        tracingService, memberPlan.Id, bankBalances, startWorkMonth, startWorkMonth.AddMonths(eligibilityOffset), null, 
                        fundAssistanceAmount, (int)BpaTransactionbpaTransactionCategory.Eligibility, (int)BpaTransactionbpaTransactionType.EligibilityDraw, defaultFundRef, 
                        usingEligibilityCategory, defaultFundRef.Id, lastAgreement, owningUserTeamRef, planIsHoursBased));

                    requestRetroprocessing.Requests.Add(MemberPlanRetroprocessingHelper_Optimized.CreateFundAssistedTransactions(
                        tracingService, memberPlan.Id, bankBalances, startWorkMonth, startWorkMonth.AddMonths(eligibilityOffset), null,
                        null, (int)BpaTransactionbpaTransactionCategory.Eligibility, (int)BpaTransactionbpaTransactionType.FundAssistedInBenefit, null, 
                        usingEligibilityCategory, defaultFundRef.Id, lastAgreement, owningUserTeamRef, planIsHoursBased));

                    // Set the Current Eligibility values
                    memberPlanCurrentEligibilityValue = (int)MemberPlanBpaCurrentEligibility.FundAssisted;
                    memberPlanBenefitMonth = startWorkMonth.AddMonths(eligibilityOffset);

                    // Set SelfPay Counter to 0
                    selfPayCounter = 0;
                    inactiveMonthCounter = 0;
                    isSelfPayCounterUpdate = true;
                    lastEligibilityDate = startWorkMonth;

                }
                else
                {
                    // Member goes to not in benefit
                    requestRetroprocessing.Requests.Add(MemberPlanRetroprocessingHelper_Optimized.CreateTransaction(tracingService, memberPlan.Id, bankBalances, startWorkMonth, startWorkMonth.AddMonths(eligibilityOffset), null,
                        (int)BpaTransactionbpaTransactionCategory.Eligibility, (int)BpaTransactionbpaTransactionType.NotInBenefit, 0, true, usingEligibilityCategory,
                        defaultFundRef.Id, lastAgreement, owningUserTeamRef, planIsHoursBased));

                    // Set the Current Eligibility values
                    memberPlanCurrentEligibilityValue = (int)MemberPlanBpaCurrentEligibility.NotInBenefit;
                    memberPlanBenefitMonth = startWorkMonth.AddMonths(eligibilityOffset);

                    inactiveMonthCounter = 1;
                }
                #endregion
            }
            else
            {
                if (currentEligibility == (int)MemberPlanBpaCurrentEligibility.NewMember)
                {
                    #region ------------------- NEW MEMBER -----------------------

                    if (isMisMatch)
                    {
                        requestRetroprocessing.Requests.Add(MemberPlanRetroprocessingHelper_Optimized.CreateUnderReviewTransaction(tracingService, memberPlan.Id,
                            bankBalances, usingEligibilityCategory, startWorkMonth,
                            startWorkMonth.AddMonths(eligibilityOffset), (int)BpaTransactionbpaTransactionCategory.Eligibility,
                            (int)BpaTransactionbpaTransactionType.UnderReview, defaultFundRef.Id, lastAgreement, owningUserTeamRef, planIsHoursBased));

                        // Set the Current Eligibility values
                        memberPlanCurrentEligibilityValue = (int)MemberPlanBpaCurrentEligibility.UnderReview;
                        memberPlanBenefitMonth = startWorkMonth.AddMonths(eligibilityOffset);

                        memberWentToUnderReview = true;
                    }
                    else
                    {
                        if (sumDollarHour >= miniInitEligibility)
                        {
                            requestRetroprocessing.Requests.Add(MemberPlanRetroprocessingHelper_Optimized.CreateDrawTransactionHour(
                                tracingService, memberPlan.Id, bankBalances, startWorkMonth,
                                startWorkMonth.AddMonths(eligibilityOffset), (int)BpaTransactionbpaTransactionCategory.Eligibility,
                                (int)BpaTransactionbpaTransactionType.EligibilityDraw, defaultFundRef, deductionRate, usingEligibilityCategory,
                                lastAgreement, owningUserTeamRef));

                            // Update Banks - Drawing from hours bank
                            bankBalances = MemberPlanRetroprocessingHelper_Optimized.UpdateBanks(tracingService, bankBalances, 0, deductionRate, 0, 0, false);

                            // Set the Current Eligibility values
                            memberPlanCurrentEligibilityValue = (int)MemberPlanBpaCurrentEligibility.InBenefit;
                            memberPlanBenefitMonth = startWorkMonth.AddMonths(eligibilityOffset);

                            requestRetroprocessing.Requests.Add(MemberPlanRetroprocessingHelper_Optimized.CreateTransaction(tracingService, memberPlan.Id,
                                bankBalances, startWorkMonth,
                                startWorkMonth.AddMonths(eligibilityOffset), null, (int)BpaTransactionbpaTransactionCategory.Eligibility,
                                (int)BpaTransactionbpaTransactionType.InBenefit, 0, true, usingEligibilityCategory,
                                defaultFundRef.Id, lastAgreement, owningUserTeamRef, planIsHoursBased));

                            isSelfPayCounterUpdate = true;
                            lastEligibilityDate = startWorkMonth;
                        }
                        else
                        {
                            // Set the Current Eligibility values
                            memberPlanCurrentEligibilityValue = (int)MemberPlanBpaCurrentEligibility.NewMember;
                            memberPlanBenefitMonth = startWorkMonth.AddMonths(eligibilityOffset);

                            requestRetroprocessing.Requests.Add(MemberPlanRetroprocessingHelper_Optimized.CreateTransaction(tracingService, memberPlan.Id,
                                bankBalances, startWorkMonth,
                                startWorkMonth.AddMonths(eligibilityOffset), null, (int)BpaTransactionbpaTransactionCategory.Eligibility,
                                (int)BpaTransactionbpaTransactionType.NewMember, 0, true, usingEligibilityCategory,
                                defaultFundRef.Id, lastAgreement, owningUserTeamRef, planIsHoursBased));
                        }

                        selfPayCounter = 0;
                        inactiveMonthCounter = 1;
                        memberWentToUnderReview = false;
                    }

                    #endregion
                }
                else if ((currentEligibility == (int)MemberPlanBpaCurrentEligibility.InBenefit) || (currentEligibility == (int)MemberPlanBpaCurrentEligibility.SelfPayInBenefit))
                {
                    #region ---- INBENEFIT & SELFPAY IN BENEFIT ----
                    if (isMisMatch)
                    {
                        // Set the Current Eligibility values
                        memberPlanCurrentEligibilityValue = (int)MemberPlanBpaCurrentEligibility.UnderReview;
                        memberPlanBenefitMonth = startWorkMonth.AddMonths(eligibilityOffset);

                        requestRetroprocessing.Requests.Add(MemberPlanRetroprocessingHelper_Optimized.CreateUnderReviewTransaction(tracingService, memberPlan.Id,
                            bankBalances, usingEligibilityCategory, startWorkMonth,
                            startWorkMonth.AddMonths(eligibilityOffset), (int)BpaTransactionbpaTransactionCategory.Eligibility,
                            (int)BpaTransactionbpaTransactionType.UnderReview, defaultFundRef.Id, lastAgreement, owningUserTeamRef, planIsHoursBased));

                        memberWentToUnderReview = true;
                    }
                    else
                    {
                        // Find MaxConsecutiveMonthsSelfPay from Eligibility Category 
                        int maxConsecutiveMonthsSelfPay = MemberPlanRetroprocessingHelper_Optimized.GetMaxConsecutiveMonthsSelfPay(
                            tracingService, eligibilityCategories, usingEligibilityCategory.Id);
                        if (sumDollarHour + deductionRate >= 0)
                        {
                            #region ------------ Hours are enough --------
                            // Dollar Bank and Hour Bank has enough or not?
                            requestRetroprocessing.Requests.Add(MemberPlanRetroprocessingHelper_Optimized.CreateDrawTransactionHour(
                                tracingService, memberPlan.Id, bankBalances, startWorkMonth,
                                startWorkMonth.AddMonths(eligibilityOffset), (int)BpaTransactionbpaTransactionCategory.Eligibility,
                                (int)BpaTransactionbpaTransactionType.EligibilityDraw, defaultFundRef, deductionRate, usingEligibilityCategory,
                                lastAgreement, owningUserTeamRef));

                            // Update Banks - Drawing from hours bank
                            bankBalances = MemberPlanRetroprocessingHelper_Optimized.UpdateBanks(tracingService, bankBalances, 0, deductionRate, 0, 0, false);

                            // Set the Current Eligibility values
                            memberPlanCurrentEligibilityValue = (int)MemberPlanBpaCurrentEligibility.InBenefit;
                            memberPlanBenefitMonth = startWorkMonth.AddMonths(eligibilityOffset);

                            requestRetroprocessing.Requests.Add(MemberPlanRetroprocessingHelper_Optimized.CreateTransaction(tracingService, memberPlan.Id,
                                bankBalances, startWorkMonth,
                                startWorkMonth.AddMonths(eligibilityOffset), null, (int)BpaTransactionbpaTransactionCategory.Eligibility,
                                (int)BpaTransactionbpaTransactionType.InBenefit, 0, true, usingEligibilityCategory,
                                defaultFundRef.Id, lastAgreement, owningUserTeamRef, planIsHoursBased));

                            isSelfPayCounterUpdate = true;
                            lastEligibilityDate = startWorkMonth;

                            selfPayCounter = 0;
                            inactiveMonthCounter = 0;
                            isSelfPayCounterUpdate = true;
                            #endregion
                        }
                        else if (((selfPayDeductionRate != 0) && (selfPayBank.Value >= (selfPayDeductionRate * -1))) && (sumDollarHour >= 0))
                        {
                            #region ---- SELFPAY BANK CHECKING ----
                            if (selfPayCounter < maxConsecutiveMonthsSelfPay)
                            {
                                // Create SelfPay draw and Self pay In Benefit Transaction
                                requestRetroprocessing.Requests.Add(MemberPlanRetroprocessingHelper_Optimized.CreateTransaction(tracingService, memberPlan.Id,
                                    bankBalances, startWorkMonth,
                                    startWorkMonth.AddMonths(eligibilityOffset), defaultFundRef, (int)BpaTransactionbpaTransactionCategory.Eligibility,
                                    (int)BpaTransactionbpaTransactionType.SelfPayDraw, selfPayDeductionRate, false, usingEligibilityCategory,
                                    defaultFundRef.Id, lastAgreement, owningUserTeamRef, planIsHoursBased));

                                // Update Banks - Drawing from selfpay bank
                                bankBalances = MemberPlanRetroprocessingHelper_Optimized.UpdateBanks(tracingService, bankBalances, 0, 0, selfPayDeductionRate, 0, false);

                                requestRetroprocessing.Requests.Add(MemberPlanRetroprocessingHelper_Optimized.CreateTransaction(tracingService, memberPlan.Id,
                                    bankBalances, startWorkMonth,
                                    startWorkMonth.AddMonths(eligibilityOffset), null, (int)BpaTransactionbpaTransactionCategory.Eligibility,
                                    (int)BpaTransactionbpaTransactionType.SelfPayInBenefit, 0, true, usingEligibilityCategory,
                                    defaultFundRef.Id, lastAgreement, owningUserTeamRef, planIsHoursBased));

                                countSelfPay = 1;
                                isSelfPayCounterUpdate = true;

                                // Set the Current Eligibility values
                                memberPlanCurrentEligibilityValue = (int)MemberPlanBpaCurrentEligibility.SelfPayInBenefit;
                                memberPlanBenefitMonth = startWorkMonth.AddMonths(eligibilityOffset);
                                lastEligibilityDate = startWorkMonth;
                            }
                            else
                            {
                                // Set the Current Eligibility values
                                memberPlanCurrentEligibilityValue = (int)MemberPlanBpaCurrentEligibility.NotInBenefit;
                                memberPlanBenefitMonth = startWorkMonth.AddMonths(eligibilityOffset);

                                requestRetroprocessing.Requests.Add(MemberPlanRetroprocessingHelper_Optimized.CreateTransaction(tracingService, memberPlan.Id,
                                    bankBalances, startWorkMonth,
                                    startWorkMonth.AddMonths(eligibilityOffset), null, (int)BpaTransactionbpaTransactionCategory.Eligibility,
                                    (int)BpaTransactionbpaTransactionType.NotInBenefit, 0, true, usingEligibilityCategory,
                                    defaultFundRef.Id, lastAgreement, owningUserTeamRef, planIsHoursBased));

                                inactiveMonthCounter = 1;
                            }
                            #endregion
                        }
                        else
                        {
                            #region ---- ELSE PART ----
                            if (!hasSelfPayInBenefit)
                            {
                                // Set the Current Eligibility values
                                memberPlanCurrentEligibilityValue = (int)MemberPlanBpaCurrentEligibility.NotInBenefit;
                                memberPlanBenefitMonth = startWorkMonth.AddMonths(eligibilityOffset);

                                requestRetroprocessing.Requests.Add(MemberPlanRetroprocessingHelper_Optimized.CreateTransaction(tracingService, memberPlan.Id,
                                    bankBalances, startWorkMonth,
                                    startWorkMonth.AddMonths(eligibilityOffset), null, (int)BpaTransactionbpaTransactionCategory.Eligibility,
                                    (int)BpaTransactionbpaTransactionType.NotInBenefit, 0, true, usingEligibilityCategory,
                                    defaultFundRef.Id, lastAgreement, owningUserTeamRef, planIsHoursBased));

                                inactiveMonthCounter = 1;
                            }
                            #endregion
                        }
                        memberWentToUnderReview = false;
                    }
                    #endregion
                }
                else if ((currentEligibility == (int)MemberPlanBpaCurrentEligibility.NotInBenefit) || (currentEligibility == (int)MemberPlanBpaCurrentEligibility.Reinstatement))
                {
                    #region  ---- NOT IN BENEFIT ----
                    Entity eligibilityCategoryUsed = service.Retrieve("bpa_eligibilitycategory", usingEligibilityCategory.Id, new ColumnSet(true));

                    //bool isReinstatement = MemberPlanRetroprocessingHelper_Optimized.IsCategoryReinstatement(service, tracingService,
                    //    eligibilityCategories, usingEligibilityCategory.Id);
                    bool isReinstatement = false;
                    if (eligibilityCategoryUsed != null && eligibilityCategoryUsed.Contains("bpa_reinstatement"))
                        isReinstatement = eligibilityCategoryUsed.GetAttributeValue<bool>("bpa_reinstatement");

                    decimal reinstatementAmount = 0;

                    if (isReinstatement)
                    {
                        //reinstatementAmount = MemberPlanRetroprocessingHelper_Optimized.GetReinstatementHours(tracingService,
                        //    eligibilityCategories, usingEligibilityCategory.Id, startWorkMonth);
                        reinstatementAmount = EligibilityCategoryHelper.GetReinstatementHours(service, eligibilityCategoryUsed.Id, startWorkMonth);

                        //if (eligibilityCategoryUsed != null && eligibilityCategoryUsed.Contains("bpa_reinstatementhours"))
                        //    reinstatementAmount = eligibilityCategory.GetAttributeValue<decimal>("bpa_reinstatementhours");
                        if (reinstatementAmount == 0)
                            isMisMatch = true;
                    }
                    if (isMisMatch)
                    {
                        // Set the Current Eligibility values
                        memberPlanCurrentEligibilityValue = (int)MemberPlanBpaCurrentEligibility.UnderReview;
                        memberPlanBenefitMonth = startWorkMonth.AddMonths(eligibilityOffset);

                        requestRetroprocessing.Requests.Add(MemberPlanRetroprocessingHelper_Optimized.CreateUnderReviewTransaction(tracingService, memberPlan.Id,
                            bankBalances, usingEligibilityCategory, startWorkMonth,
                            startWorkMonth.AddMonths(eligibilityOffset), (int)BpaTransactionbpaTransactionCategory.Eligibility,
                            (int)BpaTransactionbpaTransactionType.UnderReview, defaultFundRef.Id, lastAgreement, owningUserTeamRef, planIsHoursBased));

                        memberWentToUnderReview = true;
                    }
                    else
                    {
                        if (isReinstatement && ((currentEligibility == (int)MemberPlanBpaCurrentEligibility.Reinstatement) || hasECED))
                        {
                            if (sumDollarHour >= reinstatementAmount)
                            {
                                requestRetroprocessing.Requests.Add(MemberPlanRetroprocessingHelper_Optimized.CreateDrawTransactionHour(
                                    tracingService, memberPlan.Id, bankBalances, startWorkMonth,
                                    startWorkMonth.AddMonths(eligibilityOffset), (int)BpaTransactionbpaTransactionCategory.Eligibility,
                                    (int)BpaTransactionbpaTransactionType.EligibilityDraw, defaultFundRef, deductionRate, usingEligibilityCategory,
                                    lastAgreement, owningUserTeamRef));

                                // Update Banks - Drawing from hours bank
                                bankBalances = MemberPlanRetroprocessingHelper_Optimized.UpdateBanks(tracingService, bankBalances, 0, deductionRate, 0, 0, false);

                                // Set the Current Eligibility values
                                memberPlanCurrentEligibilityValue = (int)MemberPlanBpaCurrentEligibility.InBenefit;
                                memberPlanBenefitMonth = startWorkMonth.AddMonths(eligibilityOffset);

                                requestRetroprocessing.Requests.Add(MemberPlanRetroprocessingHelper_Optimized.CreateTransaction(tracingService, memberPlan.Id,
                                    bankBalances, startWorkMonth,
                                    startWorkMonth.AddMonths(eligibilityOffset), null, (int)BpaTransactionbpaTransactionCategory.Eligibility,
                                    (int)BpaTransactionbpaTransactionType.InBenefit, 0, true, usingEligibilityCategory,
                                    defaultFundRef.Id, lastAgreement, owningUserTeamRef, planIsHoursBased));

                                isSelfPayCounterUpdate = true;
                                lastEligibilityDate = startWorkMonth;
                                inactiveMonthCounter = 0;

                            }
                            else
                            {
                                // Set the Current Eligibility values
                                memberPlanCurrentEligibilityValue = (int)MemberPlanBpaCurrentEligibility.Reinstatement;
                                memberPlanBenefitMonth = startWorkMonth.AddMonths(eligibilityOffset);

                                requestRetroprocessing.Requests.Add(MemberPlanRetroprocessingHelper_Optimized.CreateTransaction(tracingService, memberPlan.Id,
                                    bankBalances, startWorkMonth,
                                    startWorkMonth.AddMonths(eligibilityOffset), null, (int)BpaTransactionbpaTransactionCategory.Eligibility,
                                    (int)BpaTransactionbpaTransactionType.ReInstating, 0, true, usingEligibilityCategory,
                                    defaultFundRef.Id, lastAgreement, owningUserTeamRef, planIsHoursBased));
                                isReinstating = true;
                                inactiveMonthCounter = 1;

                            }
                            if(isSelfPayCounterUpdate)
                                selfPayCounter = 0;
                        }
                        else
                        {
                            if (sumDollarHour + deductionRate >= 0)
                            {
                                requestRetroprocessing.Requests.Add(MemberPlanRetroprocessingHelper_Optimized.CreateDrawTransactionHour(
                                    tracingService, memberPlan.Id, bankBalances, startWorkMonth,
                                    startWorkMonth.AddMonths(eligibilityOffset), (int)BpaTransactionbpaTransactionCategory.Eligibility,
                                    (int)BpaTransactionbpaTransactionType.EligibilityDraw, defaultFundRef, deductionRate, usingEligibilityCategory,
                                    lastAgreement, owningUserTeamRef));

                                // Update Banks - Drawing from hours bank
                                bankBalances = MemberPlanRetroprocessingHelper_Optimized.UpdateBanks(tracingService, bankBalances, 0, deductionRate, 0, 0, false);

                                // Set the Current Eligibility values
                                memberPlanCurrentEligibilityValue = (int)MemberPlanBpaCurrentEligibility.InBenefit;
                                memberPlanBenefitMonth = startWorkMonth.AddMonths(eligibilityOffset);

                                requestRetroprocessing.Requests.Add(MemberPlanRetroprocessingHelper_Optimized.CreateTransaction(tracingService, memberPlan.Id,
                                    bankBalances, startWorkMonth,
                                    startWorkMonth.AddMonths(eligibilityOffset), null, (int)BpaTransactionbpaTransactionCategory.Eligibility,
                                    (int)BpaTransactionbpaTransactionType.InBenefit, 0, true, usingEligibilityCategory,
                                    defaultFundRef.Id, lastAgreement, owningUserTeamRef, planIsHoursBased));
                                isSelfPayCounterUpdate = true;
                                inactiveMonthCounter = 0;
                                lastEligibilityDate = startWorkMonth;
                            }
                            else
                            {
                                // Set the Current Eligibility values
                                memberPlanCurrentEligibilityValue = (int)MemberPlanBpaCurrentEligibility.NotInBenefit;
                                memberPlanBenefitMonth = startWorkMonth.AddMonths(eligibilityOffset);

                                requestRetroprocessing.Requests.Add(MemberPlanRetroprocessingHelper_Optimized.CreateTransaction(tracingService, memberPlan.Id,
                                    bankBalances, startWorkMonth,
                                    startWorkMonth.AddMonths(eligibilityOffset), null, (int)BpaTransactionbpaTransactionCategory.Eligibility,
                                    (int)BpaTransactionbpaTransactionType.NotInBenefit, 0, true, usingEligibilityCategory,
                                    defaultFundRef.Id, lastAgreement, owningUserTeamRef, planIsHoursBased));
                                inactiveMonthCounter = 1;
                            }
                            if (isSelfPayCounterUpdate)
                                selfPayCounter = 0;
                        }
                        memberWentToUnderReview = false;
                    }
                    #endregion
                }
                else if (currentEligibility == (int)MemberPlanBpaCurrentEligibility.UnderReview)
                {
                    #region ---- UNDER REVIEW ----
                    if (previousMonthEligibility == null)
                    {
                        if (sumDollarHour >= miniInitEligibility)
                        {
                            requestRetroprocessing.Requests.Add(MemberPlanRetroprocessingHelper_Optimized.CreateDrawTransactionHour(
                                tracingService, memberPlan.Id, bankBalances, startWorkMonth,
                                startWorkMonth.AddMonths(eligibilityOffset), (int)BpaTransactionbpaTransactionCategory.Eligibility,
                                (int)BpaTransactionbpaTransactionType.EligibilityDraw, defaultFundRef, deductionRate, usingEligibilityCategory,
                                lastAgreement, owningUserTeamRef));

                            // Update Banks - Drawing from hours bank
                            bankBalances = MemberPlanRetroprocessingHelper_Optimized.UpdateBanks(tracingService, bankBalances, 0, deductionRate, 0, 0, false);

                            // Set the Current Eligibility values
                            memberPlanCurrentEligibilityValue = (int)MemberPlanBpaCurrentEligibility.InBenefit;
                            memberPlanBenefitMonth = startWorkMonth.AddMonths(eligibilityOffset);

                            requestRetroprocessing.Requests.Add(MemberPlanRetroprocessingHelper_Optimized.CreateTransaction(tracingService, memberPlan.Id,
                                bankBalances, startWorkMonth,
                                startWorkMonth.AddMonths(eligibilityOffset), null, (int)BpaTransactionbpaTransactionCategory.Eligibility,
                                (int)BpaTransactionbpaTransactionType.InBenefit, 0, true, usingEligibilityCategory,
                                defaultFundRef.Id, lastAgreement, owningUserTeamRef, planIsHoursBased));

                            isSelfPayCounterUpdate = true;
                            lastEligibilityDate = startWorkMonth;
                        }
                        else
                        {
                            // Set the Current Eligibility values
                            memberPlanCurrentEligibilityValue = (int)MemberPlanBpaCurrentEligibility.NewMember;
                            memberPlanBenefitMonth = startWorkMonth.AddMonths(eligibilityOffset);

                            requestRetroprocessing.Requests.Add(MemberPlanRetroprocessingHelper_Optimized.CreateTransaction(tracingService, memberPlan.Id,
                                bankBalances, startWorkMonth,
                                startWorkMonth.AddMonths(eligibilityOffset), null, (int)BpaTransactionbpaTransactionCategory.Eligibility,
                                (int)BpaTransactionbpaTransactionType.NewMember, 0, true, usingEligibilityCategory,
                                defaultFundRef.Id, lastAgreement, owningUserTeamRef, planIsHoursBased));
                        }
                        memberWentToUnderReview = false;
                    }
                    else
                    {
                        // Set the Current Eligibility values
                        memberPlanCurrentEligibilityValue = (int)MemberPlanBpaCurrentEligibility.UnderReview;
                        memberPlanBenefitMonth = startWorkMonth.AddMonths(eligibilityOffset);

                        requestRetroprocessing.Requests.Add(MemberPlanRetroprocessingHelper_Optimized.CreateUnderReviewTransaction(tracingService, memberPlan.Id,
                            bankBalances, usingEligibilityCategory, startWorkMonth,
                            startWorkMonth.AddMonths(eligibilityOffset), (int)BpaTransactionbpaTransactionCategory.Eligibility,
                            (int)BpaTransactionbpaTransactionType.UnderReview, defaultFundRef.Id, lastAgreement, owningUserTeamRef, planIsHoursBased));

                        memberWentToUnderReview = true;
                    }
                    inactiveMonthCounter = 0;
                    selfPayCounter = 0;
                    #endregion
                }
                else if (currentEligibility == (int)MemberPlanBpaCurrentEligibility.Frozen)
                {

                    memberPlanCurrentEligibilityValue = (int)MemberPlanBpaCurrentEligibility.Frozen;
                    
                    //requestRetroprocessing.Requests.Add(MemberPlanRetroprocessingHelper_Optimized.CreateTransaction(tracingService, memberPlan.Id,
                    //    bankBalances, startWorkMonth,
                    //    startWorkMonth.AddMonths(eligibilityOffset), null, (int)BpaTransactionbpaTransactionCategory.Eligibility,
                    //    (int)BpaTransactionbpaTransactionType.Frozen, 0, true, usingEligibilityCategory,
                    //    defaultFundRef.Id, lastAgreement, owningUserTeamRef)); memberPlanCurrentEligibilityValue = (int)MemberPlanBpaCurrentEligibility.Frozen;
                }

            }

            #region ---- RESERVE TRANSFER  ----

            if (usingEligibilityCategory != null)
            {
                // Verify this month already has a reserve transfer or not
                // This probably needs to be verified
                bool isReserveExists = MemberPlanRetroprocessingHelper_Optimized.IsReserveExists(
                    tracingService, transactions, memberPlan.Id, startWorkMonth);
                if (isReserveExists == false)
                {
                    // Get all of the totals of the banks from CRM
                    decimal currentSumDollarHour = 0;
                    if (bankBalances.ContainsKey("hours") == true)
                        currentSumDollarHour = bankBalances["hours"];

                    if (currentSumDollarHour > maxMemberBank)
                    {
                        decimal reserveTransferAmount = currentSumDollarHour - maxMemberBank;
                        if ((reserveTransferAmount > 0) && (memberWentToUnderReview == false) && (currentEligibility != (int)MemberPlanBpaCurrentEligibility.Frozen))
                        {
                            requestRetroprocessing.Requests.Add(MemberPlanRetroprocessingHelper_Optimized.CreateReserveTransferHour(tracingService,
                                memberPlan.Id, startWorkMonth, startWorkMonth.AddMonths(eligibilityOffset),
                                (reserveTransferAmount * -1), 0, defaultFundRef.Id, owningUserTeamRef));

                            // Update Banks - Drawing from hours bank
                            bankBalances = MemberPlanRetroprocessingHelper_Optimized.UpdateBanks(tracingService, bankBalances, 0, (reserveTransferAmount * -1), 0, 0, false);

                            tracingService.Trace($@"insert month {startWorkMonth}");

                            // Find current month target from requestRetroprocessing
                            reserveTransactionWorkMonth.Add(startWorkMonth);
                        }
                    }
                }
            }

            #endregion

            #region ---- INACTIVE MONTH COUNTER LOGIC ----
            int consecutiveInactiveMonths = 0;

            if (hasECED)
            {
                inactiveMonthCounter = 0;
                lastEligibilityDate = startWorkMonth;
            
            }
            else
            {
                if (inactiveMonthCounter == 1 || isReinstating)
                {
                    DateTime end = startWorkMonth.AddMonths(-1);
                    DateTime lastActivityDate = DateTime.MinValue;
                    Entity lastIA = MemberPlanRetroprocessingHelper_Optimized.FetchLastEligiblityContributionMonth(tracingService, transactions, startWorkMonth, memberPlan.Id);
                    if (lastIA != null && lastIA.Attributes.Contains("bpa_transactiondate"))
                    {
                        lastActivityDate = lastIA.GetAttributeValue<DateTime>("bpa_transactiondate");
                    }

                    if (lastActivityDate == DateTime.MinValue && lastEligibilityDate == DateTime.MinValue)
                    {

                        // Retrive directly from Member plan
                        consecutiveInactiveMonths = memberPlan.Contains("bpa_consecutiveinactivemonths") ? memberPlan.GetAttributeValue<int>("bpa_consecutiveinactivemonths") : 0;
                    }
                    else
                    {
                        if (lastActivityDate > lastEligibilityDate)
                            consecutiveInactiveMonths = end.Month + end.Year * 12 - (lastActivityDate.Month + lastActivityDate.Year * 12);
                        else
                            consecutiveInactiveMonths = end.Month + end.Year * 12 - (lastEligibilityDate.Month + lastEligibilityDate.Year * 12);
                    }
                }
            }
            #endregion
            
            // Set variables initial values
            inactiveMonthCounter = inactiveMonthCounter + consecutiveInactiveMonths;
            selfPayCounter = selfPayCounter + countSelfPay;

            #region ---- CREATE Inactive and Reserve Transfer Transaction ----

            // Create 'Inactive' eligibility transactions
            // Create 'Reserve Transfer' eligibility transactions
            // also check if there are any extra transations in the future to delete
            if (inactiveMonthCounter >= 0)
            {
                bool isOverMax = false;
                if (inactiveMonthCounter > maxInactiveCounter)
                {
                    inactiveMonthCounter = maxInactiveCounter;
                    isOverMax = true;
                }
                if (inactiveMonthCounter <= maxInactiveCounter)
                {
                    // NOTE:  Updating totals on MemberPlan is now moved to the end of the loop
                    if (inactiveMonthCounter > 0)
                    {
                        if (inactiveMonthCounter == maxInactiveCounter)
                        {
                            EntityCollection inactiveNReserveTransactions = MemberPlanRetroprocessingHelper_Optimized.AlreadyHasInactiveAndReserveTransactions(
                                tracingService, transactions, memberPlan.Id, startWorkMonth.AddMonths(1));

                            if (inactiveNReserveTransactions != null && inactiveNReserveTransactions.Entities.Count > 0)
                            {
                                foreach (Entity t in inactiveNReserveTransactions.Entities)
                                {
                                    requestRetroprocessing.Requests.Add(MemberPlanRetroprocessingHelper_Optimized.DeleteTransaction(tracingService, t.Id));
                                }
                            }

                            if (isOverMax)
                            {
                                // Create 'Inactive' eligibility transactions
                                // Create 'Reserve Transfer' eligibility transactions

                                // Create the Reserve Transfer Transaction and then the regular inactive transaction
                                requestRetroprocessing.Requests.Add(MemberPlanRetroprocessingHelper_Optimized.CreateReserveTransferTransaction(tracingService,
                                    memberPlan.Id, startWorkMonth,
                                    startWorkMonth.AddMonths(eligibilityOffset), string.Empty, (int)BpaTransactionbpaTransactionCategory.Eligibility,
                                    (int)BpaTransactionbpaTransactionType.Inactive, 0, true, usingEligibilityCategory, bankBalances, defaultFundRef.Id,
                                    lastAgreement, owningUserTeamRef));

                                // Update Banks - Drawing from hours bank
                                bankBalances = MemberPlanRetroprocessingHelper_Optimized.UpdateBanks(tracingService, bankBalances, 0, 0, 0, 0, true);

                                requestRetroprocessing.Requests.Add(MemberPlanRetroprocessingHelper_Optimized.CreateTransaction(tracingService, memberPlan.Id,
                                    bankBalances, startWorkMonth,
                                    startWorkMonth.AddMonths(eligibilityOffset), null, (int)BpaTransactionbpaTransactionCategory.Eligibility,
                                    (int)BpaTransactionbpaTransactionType.Inactive, 0, true, usingEligibilityCategory,
                                    defaultFundRef.Id, lastAgreement, owningUserTeamRef, planIsHoursBased));

                                // Delete Transaction for that month
                                requestRetroprocessing.Requests.Add(MemberPlanRetroprocessingHelper_Optimized.DeleteTransactionNotInBenefit(tracingService, transactions, memberPlan.Id, startWorkMonth));
                            }
                            else
                            {
                                // Create the Reserve Transfer Transaction and then the regular inactive transaction
                                requestRetroprocessing.Requests.Add(MemberPlanRetroprocessingHelper_Optimized.CreateReserveTransferTransaction(tracingService,
                                    memberPlan.Id, startWorkMonth.AddMonths(1),
                                    startWorkMonth.AddMonths(eligibilityOffset + 1), string.Empty, (int)BpaTransactionbpaTransactionCategory.Eligibility,
                                    (int)BpaTransactionbpaTransactionType.Inactive, 0, true, usingEligibilityCategory, bankBalances, defaultFundRef.Id,
                                    lastAgreement, owningUserTeamRef));

                                // Update Banks - Drawing from hours bank
                                bankBalances = MemberPlanRetroprocessingHelper_Optimized.UpdateBanks(tracingService, bankBalances, 0, 0, 0, 0, true);

                                requestRetroprocessing.Requests.Add(MemberPlanRetroprocessingHelper_Optimized.CreateTransaction(tracingService, memberPlan.Id,
                                    bankBalances, startWorkMonth.AddMonths(1),
                                    startWorkMonth.AddMonths(eligibilityOffset + 1), null, (int)BpaTransactionbpaTransactionCategory.Eligibility,
                                    (int)BpaTransactionbpaTransactionType.Inactive, 0, true, usingEligibilityCategory,
                                    defaultFundRef.Id, lastAgreement, owningUserTeamRef, planIsHoursBased));
                            }

                            // Update MemberPlan current eligibility to Inactive
                            // Set the Current Eligibility values
                            memberPlanCurrentEligibilityValue = (int)MemberPlanBpaCurrentEligibility.Inactive;
                            memberPlanBenefitMonth = startWorkMonth.AddMonths(eligibilityOffset);

                            isMemberBecomeInActive = true;
                        }
                    }
                }
            }

            #endregion
            // NOTE:  Updating totals on MemberPlan is now moved to the end of the loop
            updatedBankBalances = bankBalances;
        }
        #endregion

        #region ---- DOLLAR BASE CALCULATIONS ----
        static void RetroProcessingDollarBase(IServiceProvider serviceProvider, IOrganizationService service, ITracingService tracingService, 
            ExecuteTransactionRequest requestRetroprocessing, EntityCollection transactions, EntityCollection eligibilityCategories, 
            EntityCollection eligibilityCategoryDetails, Dictionary<string, decimal> bankBalances, Entity memberPlan,
            EntityReference usingEligibilityCategory, int currentEligibility, Entity previousMonthEligibility, DateTime startWorkMonth,
            EntityReference defaultFundRef, bool isMisMatch, int subSectorCalculationType, bool hasSelfPayInBenefit, bool hasECED, EntityReference lastAgreement,
            EntityReference owningUserTeamRef, out bool memberWentToUnderReview, ref int selfPayCounter, out int inactiveMonthCounter,
            out bool isMemberBecomeInActive, out int memberPlanCurrentEligibilityValue, out DateTime memberPlanBenefitMonth,
            string crmAdminUserId, out Dictionary<string, decimal> updatedBankBalances, ref DateTime lastEligibilityDate, out List<DateTime> reserveTransactionWorkMonth)
        {
            int planIsDollarBased = (int)BpaEligibilityCategoryPlanEligiblityType.Dollar;

            reserveTransactionWorkMonth = new List<DateTime>();
            // Get both Dollar Bank and Self Pay Bank
            decimal sumDollarHour = 0;
            if (bankBalances.ContainsKey("dollarhours"))
                sumDollarHour = bankBalances["dollarhours"];

            decimal sumSelfPay = 0;
            if (bankBalances.ContainsKey("selfpaydollar"))
                sumSelfPay = bankBalances["selfpaydollar"];
            Money selfPayBank = new Money(sumSelfPay);

            decimal sumSecondaryBank = 0;
            if (bankBalances.ContainsKey("secondarydollar"))
                sumSecondaryBank = bankBalances["secondarydollar"];

            // Declare variables
            bool isSelfPayCounterUpdate = false;
            inactiveMonthCounter = 0;
            //selfPayCounter = 0;
            isMemberBecomeInActive = false;
            memberPlanCurrentEligibilityValue = 0;
            memberPlanBenefitMonth = DateTime.MinValue;
            memberWentToUnderReview = false;
            int countSelfPay = 0;
            bool isReinstating = false;

            // Get all information for Eligibility Category and Eligibility Category Detail
            Entity eligibilityCategory = eligibilityCategories.Entities.Where(x => x.Id == usingEligibilityCategory.Id).ToList().FirstOrDefault();
            Entity eligibilityCategoryDetail = MemberPlanRetroprocessingHelper_Optimized.FetchEligibilityDetail(
                tracingService, eligibilityCategoryDetails, eligibilityCategory.Id, startWorkMonth);

            if (eligibilityCategoryDetail == null)
                throw new InvalidPluginExecutionException(OperationStatus.Failed, @"System could not find Eligibility Category Detail Information for specific work month. Please contact BPA Administrator.");

            int eligibilityOffset = eligibilityCategory.Contains("bpa_eligibilityoffsetmonths") ? eligibilityCategory.GetAttributeValue<int>("bpa_eligibilityoffsetmonths") : 0;
            int maxInactiveCounter = eligibilityCategory.Contains("bpa_terminationcycle") ? eligibilityCategory.GetAttributeValue<int>("bpa_terminationcycle") : 0;
            decimal deductionRate = eligibilityCategoryDetail.Contains("bpa_deductionrate") ? (eligibilityCategoryDetail.GetAttributeValue<Money>("bpa_deductionrate").Value * -1) : 0;
            decimal selfPayDeductionRate = eligibilityCategoryDetail.Contains("bpa_selfpayamount") ? (eligibilityCategoryDetail.GetAttributeValue<Money>("bpa_selfpayamount").Value * -1) : 0;
            decimal miniInitEligibility = eligibilityCategoryDetail.Contains("bpa_minimumeligibility") ? eligibilityCategoryDetail.GetAttributeValue<Money>("bpa_minimumeligibility").Value : 0;
            decimal fundAssistanceAmount = eligibilityCategoryDetail.Contains("bpa_fundassistanceamount") ? (eligibilityCategoryDetail.GetAttributeValue<Money>("bpa_fundassistanceamount").Value) : 0;
            decimal maxMemberBank = eligibilityCategoryDetail.Contains("bpa_maximummemberbank") ? eligibilityCategoryDetail.GetAttributeValue<Money>("bpa_maximummemberbank").Value : 0;

            // - Dollar (Mopney)
            // Check if Deduction Rate (Regular Draw) AND Fund Assistance Amount are same then member is Fully Fund Assistance
            if ((fundAssistanceAmount * -1) == deductionRate)
            {
                #region ---- FULLY FUND ASSISTED ----

                // Rule is previous month eligibility must be 'IN BENEFIT'
                if ((currentEligibility == (int)MemberPlanBpaCurrentEligibility.InBenefit) || (currentEligibility == (int)MemberPlanBpaCurrentEligibility.SelfPayInBenefit)
                    || (currentEligibility == (int)MemberPlanBpaCurrentEligibility.FundAssisted))
                {
                    requestRetroprocessing.Requests.Add(MemberPlanRetroprocessingHelper_Optimized.CreateFundAssistedTransactions(
                        tracingService, memberPlan.Id, bankBalances, startWorkMonth, startWorkMonth.AddMonths(eligibilityOffset), new Money(fundAssistanceAmount * -1), null,
                        (int)BpaTransactionbpaTransactionCategory.Eligibility, (int)BpaTransactionbpaTransactionType.FundAssisted, defaultFundRef,
                        usingEligibilityCategory, defaultFundRef.Id, lastAgreement, owningUserTeamRef, planIsDollarBased));

                    requestRetroprocessing.Requests.Add(MemberPlanRetroprocessingHelper_Optimized.CreateFundAssistedTransactions(
                        tracingService, memberPlan.Id, bankBalances, startWorkMonth, startWorkMonth.AddMonths(eligibilityOffset),
                        new Money(fundAssistanceAmount), null, (int)BpaTransactionbpaTransactionCategory.Eligibility, (int)BpaTransactionbpaTransactionType.EligibilityDraw,
                        defaultFundRef, usingEligibilityCategory, defaultFundRef.Id, lastAgreement, owningUserTeamRef, planIsDollarBased));

                    requestRetroprocessing.Requests.Add(MemberPlanRetroprocessingHelper_Optimized.CreateFundAssistedTransactions(
                        tracingService, memberPlan.Id, bankBalances, startWorkMonth, startWorkMonth.AddMonths(eligibilityOffset),
                        null, null, (int)BpaTransactionbpaTransactionCategory.Eligibility, (int)BpaTransactionbpaTransactionType.FundAssistedInBenefit,
                        null, usingEligibilityCategory, defaultFundRef.Id, lastAgreement, owningUserTeamRef, planIsDollarBased));

                    // Set the Current Eligibility values
                    memberPlanCurrentEligibilityValue = (int)MemberPlanBpaCurrentEligibility.FundAssisted;
                    memberPlanBenefitMonth = startWorkMonth.AddMonths(eligibilityOffset);

                    isSelfPayCounterUpdate = true;
                    lastEligibilityDate = startWorkMonth;
                    inactiveMonthCounter = 0;
                }
                else
                {
                    // Set the Current Eligibility values
                    memberPlanCurrentEligibilityValue = (int)MemberPlanBpaCurrentEligibility.NotInBenefit;
                    memberPlanBenefitMonth = startWorkMonth.AddMonths(eligibilityOffset);

                    // Member goes to not in benefit
                    requestRetroprocessing.Requests.Add(MemberPlanRetroprocessingHelper_Optimized.CreateTransaction(tracingService, memberPlan.Id, bankBalances,
                        startWorkMonth, startWorkMonth.AddMonths(eligibilityOffset), null,
                        (int)BpaTransactionbpaTransactionCategory.Eligibility, (int)BpaTransactionbpaTransactionType.NotInBenefit, 0, true, usingEligibilityCategory,
                        defaultFundRef.Id, lastAgreement, owningUserTeamRef, planIsDollarBased));

                    inactiveMonthCounter = 1;
                }
                if(isSelfPayCounterUpdate)
                    selfPayCounter = 0;
                #endregion
            }
            else if (fundAssistanceAmount == 0)
            {
                #region ---- REGULAR DOLLAR LOGIC ----
                if (currentEligibility == (int)MemberPlanBpaCurrentEligibility.NewMember)
                {
                    // New Member Processing
                    #region ---- NEW MEMBER ----
                    if (isMisMatch)
                    {
                        requestRetroprocessing.Requests.Add(MemberPlanRetroprocessingHelper_Optimized.CreateUnderReviewTransaction(tracingService, memberPlan.Id,
                            bankBalances, usingEligibilityCategory, startWorkMonth,
                            startWorkMonth.AddMonths(eligibilityOffset), (int)BpaTransactionbpaTransactionCategory.Eligibility,
                            (int)BpaTransactionbpaTransactionType.UnderReview, defaultFundRef.Id, lastAgreement, owningUserTeamRef, planIsDollarBased));

                        // Set the Current Eligibility values
                        memberPlanCurrentEligibilityValue = (int)MemberPlanBpaCurrentEligibility.UnderReview;
                        memberPlanBenefitMonth = startWorkMonth.AddMonths(eligibilityOffset);

                        memberWentToUnderReview = true;
                    }
                    else
                    {
                        if (sumDollarHour >= miniInitEligibility)
                        {
                            requestRetroprocessing.Requests.Add(MemberPlanRetroprocessingHelper_Optimized.CreateTransaction(tracingService, memberPlan.Id, bankBalances,
                                startWorkMonth, startWorkMonth.AddMonths(eligibilityOffset), defaultFundRef,
                                (int)BpaTransactionbpaTransactionCategory.Eligibility, (int)BpaTransactionbpaTransactionType.EligibilityDraw, deductionRate, false, usingEligibilityCategory,
                                defaultFundRef.Id, lastAgreement, owningUserTeamRef, planIsDollarBased));

                            // Update Banks - Drawing from dollar bank
                            bankBalances = MemberPlanRetroprocessingHelper_Optimized.UpdateBanks(tracingService, bankBalances, deductionRate, 0, 0, 0, false);

                            // Set the Current Eligibility values
                            memberPlanCurrentEligibilityValue = (int)MemberPlanBpaCurrentEligibility.InBenefit;
                            memberPlanBenefitMonth = startWorkMonth.AddMonths(eligibilityOffset);

                            requestRetroprocessing.Requests.Add(MemberPlanRetroprocessingHelper_Optimized.CreateTransaction(tracingService, memberPlan.Id, bankBalances,
                                startWorkMonth, startWorkMonth.AddMonths(eligibilityOffset), null,
                                (int)BpaTransactionbpaTransactionCategory.Eligibility, (int)BpaTransactionbpaTransactionType.InBenefit, 0, true, usingEligibilityCategory,
                                defaultFundRef.Id, lastAgreement, owningUserTeamRef, planIsDollarBased));

                            inactiveMonthCounter = 0;
                            lastEligibilityDate = startWorkMonth;
                            isSelfPayCounterUpdate = true;
                        }
                        else if (sumSecondaryBank >= miniInitEligibility)
                        {
                            requestRetroprocessing.Requests.Add(MemberPlanRetroprocessingHelper_Optimized.CreateTransaction(tracingService, memberPlan.Id, bankBalances,
                                startWorkMonth, startWorkMonth.AddMonths(eligibilityOffset), defaultFundRef,
                                (int)BpaTransactionbpaTransactionCategory.Eligibility, (int)BpaTransactionbpaTransactionType.SecondaryDollarBankDraw, deductionRate,
                                false, usingEligibilityCategory, defaultFundRef.Id, lastAgreement, owningUserTeamRef, planIsDollarBased));

                            // Update Banks - Drawing from secondary dollar bank
                            bankBalances = MemberPlanRetroprocessingHelper_Optimized.UpdateBanks(tracingService, bankBalances, 0, 0, 0, deductionRate, false);

                            // Set the Current Eligibility values
                            memberPlanCurrentEligibilityValue = (int)MemberPlanBpaCurrentEligibility.InBenefit;
                            memberPlanBenefitMonth = startWorkMonth.AddMonths(eligibilityOffset);

                            requestRetroprocessing.Requests.Add(MemberPlanRetroprocessingHelper_Optimized.CreateTransaction(tracingService, memberPlan.Id, bankBalances,
                                startWorkMonth, startWorkMonth.AddMonths(eligibilityOffset), null,
                                (int)BpaTransactionbpaTransactionCategory.Eligibility, (int)BpaTransactionbpaTransactionType.InBenefit, 0, true, usingEligibilityCategory,
                                defaultFundRef.Id, lastAgreement, owningUserTeamRef, planIsDollarBased));

                            lastEligibilityDate = startWorkMonth;
                            inactiveMonthCounter = 0;
                            isSelfPayCounterUpdate = true;
                        }
                        else
                        {
                            // Set the Current Eligibility values
                            memberPlanCurrentEligibilityValue = (int)MemberPlanBpaCurrentEligibility.NewMember;
                            memberPlanBenefitMonth = startWorkMonth.AddMonths(eligibilityOffset);

                            requestRetroprocessing.Requests.Add(MemberPlanRetroprocessingHelper_Optimized.CreateTransaction(tracingService, memberPlan.Id, bankBalances,
                                startWorkMonth, startWorkMonth.AddMonths(eligibilityOffset), null,
                                (int)BpaTransactionbpaTransactionCategory.Eligibility, (int)BpaTransactionbpaTransactionType.NewMember, 0, true, usingEligibilityCategory,
                                defaultFundRef.Id, lastAgreement, owningUserTeamRef, planIsDollarBased));
                            inactiveMonthCounter = 1;
                        }
                        if(isSelfPayCounterUpdate)
                            selfPayCounter = 0;
                        memberWentToUnderReview = false;
                    }
                    #endregion
                } // End of New Number
                else if ((currentEligibility == (int)MemberPlanBpaCurrentEligibility.InBenefit) || (currentEligibility == (int)MemberPlanBpaCurrentEligibility.SelfPayInBenefit))
                {
                    #region ---- IN BENEFIT & SELFPAY IN BENEFIT ----
                    if (isMisMatch)
                    {
                        requestRetroprocessing.Requests.Add(MemberPlanRetroprocessingHelper_Optimized.CreateUnderReviewTransaction(tracingService, memberPlan.Id,
                            bankBalances, usingEligibilityCategory, startWorkMonth,
                            startWorkMonth.AddMonths(eligibilityOffset), (int)BpaTransactionbpaTransactionCategory.Eligibility,
                            (int)BpaTransactionbpaTransactionType.UnderReview, defaultFundRef.Id, lastAgreement, owningUserTeamRef, planIsDollarBased));

                        // Set the Current Eligibility values
                        memberPlanCurrentEligibilityValue = (int)MemberPlanBpaCurrentEligibility.UnderReview;
                        memberPlanBenefitMonth = startWorkMonth.AddMonths(eligibilityOffset);

                        memberWentToUnderReview = true;
                    }
                    else
                    {
                        // Find MaxConsecutiveMonthsSelfPay from Eligibility Category 
                        int maxConsecutiveMonthsSelfPay = MemberPlanRetroprocessingHelper_Optimized.GetMaxConsecutiveMonthsSelfPay(
                            tracingService, eligibilityCategories, usingEligibilityCategory.Id);

                        if (sumDollarHour + deductionRate >= 0)
                        {
                            #region ---- DOLLAR BANK is enough ----

                            // Dollar Bank and Hour Bank has enough or not? 
                            requestRetroprocessing.Requests.Add(MemberPlanRetroprocessingHelper_Optimized.CreateTransaction(tracingService, memberPlan.Id, bankBalances,
                                startWorkMonth, startWorkMonth.AddMonths(eligibilityOffset), null,
                                (int)BpaTransactionbpaTransactionCategory.Eligibility, (int)BpaTransactionbpaTransactionType.EligibilityDraw, deductionRate, false, usingEligibilityCategory,
                                defaultFundRef.Id, lastAgreement, owningUserTeamRef, planIsDollarBased));

                            // Update Banks - Drawing from dollar bank
                            bankBalances = MemberPlanRetroprocessingHelper_Optimized.UpdateBanks(tracingService, bankBalances, deductionRate, 0, 0, 0, false);

                            // Set the Current Eligibility values
                            memberPlanCurrentEligibilityValue = (int)MemberPlanBpaCurrentEligibility.InBenefit;
                            memberPlanBenefitMonth = startWorkMonth.AddMonths(eligibilityOffset);

                            requestRetroprocessing.Requests.Add(MemberPlanRetroprocessingHelper_Optimized.CreateTransaction(tracingService, memberPlan.Id, bankBalances,
                                startWorkMonth, startWorkMonth.AddMonths(eligibilityOffset), null,
                                (int)BpaTransactionbpaTransactionCategory.Eligibility, (int)BpaTransactionbpaTransactionType.InBenefit, 0, true, usingEligibilityCategory,
                                defaultFundRef.Id, lastAgreement, owningUserTeamRef, planIsDollarBased));

                            isSelfPayCounterUpdate = true;
                            lastEligibilityDate = startWorkMonth;

                            inactiveMonthCounter = 0;
                            #endregion
                        }
                        else if (sumSecondaryBank + deductionRate >= 0)
                        {
                            #region ---- SECONDARY DOLLAR BANK CHECKING ----

                            // Secondary Dollar Bank draw
                            requestRetroprocessing.Requests.Add(MemberPlanRetroprocessingHelper_Optimized.CreateTransaction(tracingService, memberPlan.Id, bankBalances,
                                startWorkMonth, startWorkMonth.AddMonths(eligibilityOffset), null,
                                (int)BpaTransactionbpaTransactionCategory.Eligibility, (int)BpaTransactionbpaTransactionType.SecondaryDollarBankDraw, deductionRate,
                                false, usingEligibilityCategory,
                                defaultFundRef.Id, lastAgreement, owningUserTeamRef, planIsDollarBased));

                            // Update Banks - Drawing from secondary dollar bank
                            bankBalances = MemberPlanRetroprocessingHelper_Optimized.UpdateBanks(tracingService, bankBalances, 0, 0, 0, deductionRate, false);

                            // Set the Current Eligibility values
                            memberPlanCurrentEligibilityValue = (int)MemberPlanBpaCurrentEligibility.InBenefit;
                            memberPlanBenefitMonth = startWorkMonth.AddMonths(eligibilityOffset);

                            requestRetroprocessing.Requests.Add(MemberPlanRetroprocessingHelper_Optimized.CreateTransaction(tracingService, memberPlan.Id, bankBalances,
                                startWorkMonth, startWorkMonth.AddMonths(eligibilityOffset), null,
                                (int)BpaTransactionbpaTransactionCategory.Eligibility, (int)BpaTransactionbpaTransactionType.InBenefit, 0, true, usingEligibilityCategory,
                                defaultFundRef.Id, lastAgreement, owningUserTeamRef, planIsDollarBased));
                            lastEligibilityDate = startWorkMonth;
                            
                            inactiveMonthCounter = 0;
                            isSelfPayCounterUpdate = true;

                            #endregion
                        }
                        else if (((selfPayDeductionRate != 0) && (selfPayBank.Value >= (selfPayDeductionRate * -1))) && (sumDollarHour >= 0))
                        {
                            #region ---- SELFPAY BANK CHECKING ----

                            // Self Pay Bank Checking 
                            if (selfPayCounter < maxConsecutiveMonthsSelfPay)
                            {
                                // Create SelfPay draw and SelfPay In Benefit transaction
                                requestRetroprocessing.Requests.Add(MemberPlanRetroprocessingHelper_Optimized.CreateTransaction(tracingService, memberPlan.Id, bankBalances,
                                    startWorkMonth, startWorkMonth.AddMonths(eligibilityOffset), null,
                                    (int)BpaTransactionbpaTransactionCategory.Eligibility, (int)BpaTransactionbpaTransactionType.SelfPayDraw, selfPayDeductionRate,
                                    false, usingEligibilityCategory,
                                    defaultFundRef.Id, lastAgreement, owningUserTeamRef, planIsDollarBased));

                                // Update Banks - Drawing from selfpay bank
                                bankBalances = MemberPlanRetroprocessingHelper_Optimized.UpdateBanks(tracingService, bankBalances, 0, 0, selfPayDeductionRate, 0, false);

                                requestRetroprocessing.Requests.Add(MemberPlanRetroprocessingHelper_Optimized.CreateTransaction(tracingService, memberPlan.Id, bankBalances,
                                    startWorkMonth, startWorkMonth.AddMonths(eligibilityOffset), null,
                                    (int)BpaTransactionbpaTransactionCategory.Eligibility, (int)BpaTransactionbpaTransactionType.SelfPayInBenefit, 0,
                                    true, usingEligibilityCategory,
                                    defaultFundRef.Id, lastAgreement, owningUserTeamRef, planIsDollarBased));

                                // Increment the self pay counter
                                //selfPayCounter = countSelfPay + 1;
                                countSelfPay = 1;

                                // Set the Current Eligibility values
                                memberPlanCurrentEligibilityValue = (int)MemberPlanBpaCurrentEligibility.SelfPayInBenefit;
                                memberPlanBenefitMonth = startWorkMonth.AddMonths(eligibilityOffset);
                                lastEligibilityDate = startWorkMonth;

                                //isSelfPayCounterUpdate = true;
                            }
                            else
                            {
                                // Set the Current Eligibility values
                                memberPlanCurrentEligibilityValue = (int)MemberPlanBpaCurrentEligibility.NotInBenefit;
                                memberPlanBenefitMonth = startWorkMonth.AddMonths(eligibilityOffset);

                                requestRetroprocessing.Requests.Add(MemberPlanRetroprocessingHelper_Optimized.CreateTransaction(tracingService, memberPlan.Id, bankBalances,
                                    startWorkMonth, startWorkMonth.AddMonths(eligibilityOffset), null,
                                    (int)BpaTransactionbpaTransactionCategory.Eligibility, (int)BpaTransactionbpaTransactionType.NotInBenefit, 0, true, usingEligibilityCategory,
                                    defaultFundRef.Id, lastAgreement, owningUserTeamRef, planIsDollarBased));

                                inactiveMonthCounter = 1;
                            }

                            #endregion
                        }
                        else
                        {
                            #region ---- ELSE PART ----

                            if (!hasSelfPayInBenefit)
                            {
                                // Set the Current Eligibility values
                                memberPlanCurrentEligibilityValue = (int)MemberPlanBpaCurrentEligibility.NotInBenefit;
                                memberPlanBenefitMonth = startWorkMonth.AddMonths(eligibilityOffset);

                                requestRetroprocessing.Requests.Add(MemberPlanRetroprocessingHelper_Optimized.CreateTransaction(tracingService, memberPlan.Id, bankBalances,
                                    startWorkMonth, startWorkMonth.AddMonths(eligibilityOffset), null,
                                    (int)BpaTransactionbpaTransactionCategory.Eligibility, (int)BpaTransactionbpaTransactionType.NotInBenefit, 0, true, usingEligibilityCategory,
                                    defaultFundRef.Id, lastAgreement, owningUserTeamRef, planIsDollarBased));

                                inactiveMonthCounter = 1;
                            }
                            #endregion
                        }

                        if (isSelfPayCounterUpdate)
                            selfPayCounter = 0;

                        memberWentToUnderReview = false;
                    }
                    #endregion
                }
                else if ((currentEligibility == (int)MemberPlanBpaCurrentEligibility.NotInBenefit) || (currentEligibility == (int)MemberPlanBpaCurrentEligibility.Reinstatement))
                {
                    #region  ---- NOT IN BENEFIT ----
                    //bool isResinstatement = MemberPlanRetroprocessingHelper_Optimized.IsCategoryReinstatement(service, tracingService,
                    //    eligibilityCategories, usingEligibilityCategory.Id);
                    Entity eligibilityCategoryUsed = service.Retrieve("bpa_eligibilitycategory", usingEligibilityCategory.Id, new ColumnSet(true));
                    bool isResinstatement = false;
                    if (eligibilityCategoryUsed != null && eligibilityCategoryUsed.Contains("bpa_reinstatement"))
                        isResinstatement = eligibilityCategoryUsed.GetAttributeValue<bool>("bpa_reinstatement");

                    decimal reinstatementAmount = 0;

                    if (isResinstatement)
                    {
                        //reinstatementAmount = MemberPlanRetroprocessingHelper_Optimized.GetReinstatementDollars(tracingService,
                        //    eligibilityCategories, usingEligibilityCategory.Id, startWorkMonth);
                        reinstatementAmount = EligibilityCategoryHelper.GetReinstatementDollars(service, eligibilityCategoryUsed.Id, startWorkMonth);

                        if (reinstatementAmount == 0)
                            isMisMatch = true;
                    }
                    if (isMisMatch)
                    {
                        requestRetroprocessing.Requests.Add(MemberPlanRetroprocessingHelper_Optimized.CreateUnderReviewTransaction(tracingService, memberPlan.Id,
                            bankBalances, usingEligibilityCategory, startWorkMonth,
                            startWorkMonth.AddMonths(eligibilityOffset), (int)BpaTransactionbpaTransactionCategory.Eligibility,
                            (int)BpaTransactionbpaTransactionType.UnderReview, defaultFundRef.Id, lastAgreement, owningUserTeamRef, planIsDollarBased));

                        // Set the Current Eligibility values
                        memberPlanCurrentEligibilityValue = (int)MemberPlanBpaCurrentEligibility.UnderReview;
                        memberPlanBenefitMonth = startWorkMonth.AddMonths(eligibilityOffset);

                        memberWentToUnderReview = false;
                    }
                    else
                    {
                        if (isResinstatement && ((currentEligibility == (int)MemberPlanBpaCurrentEligibility.Reinstatement) || hasECED))
                        {
                            if (sumDollarHour >= reinstatementAmount)
                            {
                                requestRetroprocessing.Requests.Add(MemberPlanRetroprocessingHelper_Optimized.CreateTransaction(tracingService, memberPlan.Id, bankBalances,
                                    startWorkMonth, startWorkMonth.AddMonths(eligibilityOffset), null,
                                    (int)BpaTransactionbpaTransactionCategory.Eligibility, (int)BpaTransactionbpaTransactionType.EligibilityDraw, deductionRate, false,
                                    usingEligibilityCategory, defaultFundRef.Id, lastAgreement, owningUserTeamRef, planIsDollarBased));

                                // Update Banks - Drawing from dollar bank
                                bankBalances = MemberPlanRetroprocessingHelper_Optimized.UpdateBanks(tracingService, bankBalances, deductionRate, 0, 0, 0, false);

                                // Set the Current Eligibility values
                                memberPlanCurrentEligibilityValue = (int)MemberPlanBpaCurrentEligibility.InBenefit;
                                memberPlanBenefitMonth = startWorkMonth.AddMonths(eligibilityOffset);

                                requestRetroprocessing.Requests.Add(MemberPlanRetroprocessingHelper_Optimized.CreateTransaction(tracingService, memberPlan.Id, bankBalances,
                                    startWorkMonth, startWorkMonth.AddMonths(eligibilityOffset), null,
                                    (int)BpaTransactionbpaTransactionCategory.Eligibility, (int)BpaTransactionbpaTransactionType.InBenefit, 0, true, usingEligibilityCategory,
                                    defaultFundRef.Id, lastAgreement, owningUserTeamRef, planIsDollarBased));
                                lastEligibilityDate = startWorkMonth;

                                isSelfPayCounterUpdate = true;
                                inactiveMonthCounter = 0;

                            }
                            else
                            {
                                requestRetroprocessing.Requests.Add(MemberPlanRetroprocessingHelper_Optimized.CreateTransaction(tracingService, memberPlan.Id, bankBalances,
                                    startWorkMonth, startWorkMonth.AddMonths(eligibilityOffset), null,
                                    (int)BpaTransactionbpaTransactionCategory.Eligibility, (int)BpaTransactionbpaTransactionType.ReInstating, 0, true, usingEligibilityCategory,
                                    defaultFundRef.Id, lastAgreement, owningUserTeamRef, planIsDollarBased));

                                // Set the Current Eligibility values
                                memberPlanCurrentEligibilityValue = (int)MemberPlanBpaCurrentEligibility.Reinstatement;
                                memberPlanBenefitMonth = startWorkMonth.AddMonths(eligibilityOffset);
                                isReinstating = true;
                                inactiveMonthCounter = 1;

                            }
                        }
                        else
                        {
                            if (sumDollarHour + deductionRate >= 0)
                            {
                                requestRetroprocessing.Requests.Add(MemberPlanRetroprocessingHelper_Optimized.CreateTransaction(tracingService, memberPlan.Id, bankBalances,
                                    startWorkMonth, startWorkMonth.AddMonths(eligibilityOffset), null,
                                    (int)BpaTransactionbpaTransactionCategory.Eligibility, (int)BpaTransactionbpaTransactionType.EligibilityDraw, deductionRate,
                                    false, usingEligibilityCategory,
                                    defaultFundRef.Id, lastAgreement, owningUserTeamRef, planIsDollarBased));

                                // Update Banks - Drawing from dollar bank
                                bankBalances = MemberPlanRetroprocessingHelper_Optimized.UpdateBanks(tracingService, bankBalances, deductionRate, 0, 0, 0, false);

                                // Set the Current Eligibility values
                                memberPlanCurrentEligibilityValue = (int)MemberPlanBpaCurrentEligibility.InBenefit;
                                memberPlanBenefitMonth = startWorkMonth.AddMonths(eligibilityOffset);

                                requestRetroprocessing.Requests.Add(MemberPlanRetroprocessingHelper_Optimized.CreateTransaction(tracingService, memberPlan.Id, bankBalances,
                                    startWorkMonth, startWorkMonth.AddMonths(eligibilityOffset), null,
                                    (int)BpaTransactionbpaTransactionCategory.Eligibility, (int)BpaTransactionbpaTransactionType.InBenefit, 0, true, usingEligibilityCategory,
                                    defaultFundRef.Id, lastAgreement, owningUserTeamRef, planIsDollarBased));

                                isSelfPayCounterUpdate = true;
                                lastEligibilityDate = startWorkMonth;
                                inactiveMonthCounter = 0;
                            }
                            else if (sumSecondaryBank + deductionRate >= 0)
                            {
                                // Secondary Dollar Bank draw
                                requestRetroprocessing.Requests.Add(MemberPlanRetroprocessingHelper_Optimized.CreateTransaction(tracingService, memberPlan.Id, bankBalances,
                                    startWorkMonth, startWorkMonth.AddMonths(eligibilityOffset), null,
                                    (int)BpaTransactionbpaTransactionCategory.Eligibility, (int)BpaTransactionbpaTransactionType.SecondaryDollarBankDraw, deductionRate,
                                    false, usingEligibilityCategory,
                                    defaultFundRef.Id, lastAgreement, owningUserTeamRef, planIsDollarBased));

                                // Update Banks - Drawing from secondary dollar bank
                                bankBalances = MemberPlanRetroprocessingHelper_Optimized.UpdateBanks(tracingService, bankBalances, 0, 0, 0, deductionRate, false);

                                // Set the Current Eligibility values
                                memberPlanCurrentEligibilityValue = (int)MemberPlanBpaCurrentEligibility.InBenefit;
                                memberPlanBenefitMonth = startWorkMonth.AddMonths(eligibilityOffset);

                                requestRetroprocessing.Requests.Add(MemberPlanRetroprocessingHelper_Optimized.CreateTransaction(tracingService, memberPlan.Id, bankBalances,
                                    startWorkMonth, startWorkMonth.AddMonths(eligibilityOffset), null,
                                    (int)BpaTransactionbpaTransactionCategory.Eligibility, (int)BpaTransactionbpaTransactionType.InBenefit, 0, true, usingEligibilityCategory,
                                    defaultFundRef.Id, lastAgreement, owningUserTeamRef, planIsDollarBased));

                                isSelfPayCounterUpdate = true;
                                lastEligibilityDate = startWorkMonth;
                                inactiveMonthCounter = 0;
                            }
                            else
                            {
                                // Set the Current Eligibility values
                                memberPlanCurrentEligibilityValue = (int)MemberPlanBpaCurrentEligibility.NotInBenefit;
                                memberPlanBenefitMonth = startWorkMonth.AddMonths(eligibilityOffset);

                                requestRetroprocessing.Requests.Add(MemberPlanRetroprocessingHelper_Optimized.CreateTransaction(tracingService, memberPlan.Id, bankBalances,
                                    startWorkMonth, startWorkMonth.AddMonths(eligibilityOffset), null,
                                    (int)BpaTransactionbpaTransactionCategory.Eligibility, (int)BpaTransactionbpaTransactionType.NotInBenefit, 0, true, usingEligibilityCategory,
                                    defaultFundRef.Id, lastAgreement, owningUserTeamRef, planIsDollarBased));

                                inactiveMonthCounter = 1;
                            }
                        }
                        if (isSelfPayCounterUpdate)
                            selfPayCounter = 0;
                        memberWentToUnderReview = false;
                    }
                    #endregion
                }
                else if (currentEligibility == (int)MemberPlanBpaCurrentEligibility.UnderReview)
                {
                    #region ---- UNDER REVIEW ----
                    // Under Review

                    requestRetroprocessing.Requests.Add(MemberPlanRetroprocessingHelper_Optimized.CreateUnderReviewTransaction(tracingService, memberPlan.Id,
                        bankBalances, usingEligibilityCategory, startWorkMonth,
                        startWorkMonth.AddMonths(eligibilityOffset), (int)BpaTransactionbpaTransactionCategory.Eligibility,
                        (int)BpaTransactionbpaTransactionType.UnderReview, defaultFundRef.Id, lastAgreement, owningUserTeamRef, planIsDollarBased));

                    // Set the Current Eligibility values
                    memberPlanCurrentEligibilityValue = (int)MemberPlanBpaCurrentEligibility.UnderReview;
                    memberPlanBenefitMonth = startWorkMonth.AddMonths(eligibilityOffset);

                    memberWentToUnderReview = true;
                    inactiveMonthCounter = 0;
                    #endregion
                }
                else if (currentEligibility == (int)MemberPlanBpaCurrentEligibility.Frozen)
                {
                    memberPlanCurrentEligibilityValue = (int)MemberPlanBpaCurrentEligibility.Frozen;
                }
                #endregion
            }
            else
            {
                #region ---- PARTIAL SELF PAY ----
                if (currentEligibility == (int)MemberPlanBpaCurrentEligibility.NewMember)
                {
                    #region ---- NEW MEMBER ----
                    if (isMisMatch)
                    {
                        requestRetroprocessing.Requests.Add(MemberPlanRetroprocessingHelper_Optimized.CreateUnderReviewTransaction(tracingService, memberPlan.Id,
                            bankBalances, usingEligibilityCategory, startWorkMonth,
                            startWorkMonth.AddMonths(eligibilityOffset), (int)BpaTransactionbpaTransactionCategory.Eligibility,
                            (int)BpaTransactionbpaTransactionType.UnderReview, defaultFundRef.Id, lastAgreement, owningUserTeamRef, planIsDollarBased));

                        // Set the Current Eligibility values
                        memberPlanCurrentEligibilityValue = (int)MemberPlanBpaCurrentEligibility.UnderReview;
                        memberPlanBenefitMonth = startWorkMonth.AddMonths(eligibilityOffset);

                        memberWentToUnderReview = true;
                    }
                    else
                    {
                        if (sumDollarHour >= miniInitEligibility)
                        {
                            requestRetroprocessing.Requests.Add(MemberPlanRetroprocessingHelper_Optimized.CreateTransaction(tracingService, memberPlan.Id, bankBalances,
                                startWorkMonth, startWorkMonth.AddMonths(eligibilityOffset), defaultFundRef,
                                (int)BpaTransactionbpaTransactionCategory.Eligibility, (int)BpaTransactionbpaTransactionType.EligibilityDraw, deductionRate, false, usingEligibilityCategory,
                                defaultFundRef.Id, lastAgreement, owningUserTeamRef, planIsDollarBased));

                            // Update Banks - Drawing from dollar bank
                            bankBalances = MemberPlanRetroprocessingHelper_Optimized.UpdateBanks(tracingService, bankBalances, deductionRate, 0, 0, 0, false);

                            // Set the Current Eligibility values
                            memberPlanCurrentEligibilityValue = (int)MemberPlanBpaCurrentEligibility.InBenefit;
                            memberPlanBenefitMonth = startWorkMonth.AddMonths(eligibilityOffset);

                            requestRetroprocessing.Requests.Add(MemberPlanRetroprocessingHelper_Optimized.CreateTransaction(tracingService, memberPlan.Id, bankBalances,
                                startWorkMonth, startWorkMonth.AddMonths(eligibilityOffset), null,
                                (int)BpaTransactionbpaTransactionCategory.Eligibility, (int)BpaTransactionbpaTransactionType.InBenefit, 0, true, usingEligibilityCategory,
                                defaultFundRef.Id, lastAgreement, owningUserTeamRef, planIsDollarBased));

                            inactiveMonthCounter = 0;
                            lastEligibilityDate = startWorkMonth;
                            isSelfPayCounterUpdate = true;
                        }
                        else if (sumSecondaryBank >= miniInitEligibility)
                        {
                            requestRetroprocessing.Requests.Add(MemberPlanRetroprocessingHelper_Optimized.CreateTransaction(tracingService, memberPlan.Id, bankBalances,
                                startWorkMonth, startWorkMonth.AddMonths(eligibilityOffset), defaultFundRef,
                                (int)BpaTransactionbpaTransactionCategory.Eligibility, (int)BpaTransactionbpaTransactionType.SecondaryDollarBankDraw, deductionRate, false, usingEligibilityCategory,
                                defaultFundRef.Id, lastAgreement, owningUserTeamRef, planIsDollarBased));

                            // Update Banks - Drawing from secondary dollar bank
                            bankBalances = MemberPlanRetroprocessingHelper_Optimized.UpdateBanks(tracingService, bankBalances, 0, 0, 0, deductionRate, false);

                            // Set the Current Eligibility values
                            memberPlanCurrentEligibilityValue = (int)MemberPlanBpaCurrentEligibility.InBenefit;
                            memberPlanBenefitMonth = startWorkMonth.AddMonths(eligibilityOffset);

                            requestRetroprocessing.Requests.Add(MemberPlanRetroprocessingHelper_Optimized.CreateTransaction(tracingService, memberPlan.Id, bankBalances,
                                startWorkMonth, startWorkMonth.AddMonths(eligibilityOffset), null,
                                (int)BpaTransactionbpaTransactionCategory.Eligibility, (int)BpaTransactionbpaTransactionType.InBenefit, 0, true, usingEligibilityCategory,
                                defaultFundRef.Id, lastAgreement, owningUserTeamRef, planIsDollarBased));

                            isSelfPayCounterUpdate = true;
                            inactiveMonthCounter = 0;
                            lastEligibilityDate = startWorkMonth;

                        }
                        else
                        {
                            // Set the Current Eligibility values
                            memberPlanCurrentEligibilityValue = (int)MemberPlanBpaCurrentEligibility.NewMember;
                            memberPlanBenefitMonth = startWorkMonth.AddMonths(eligibilityOffset);

                            requestRetroprocessing.Requests.Add(MemberPlanRetroprocessingHelper_Optimized.CreateTransaction(tracingService, memberPlan.Id, bankBalances,
                                startWorkMonth, startWorkMonth.AddMonths(eligibilityOffset), null,
                                (int)BpaTransactionbpaTransactionCategory.Eligibility, (int)BpaTransactionbpaTransactionType.NewMember, 0, true, usingEligibilityCategory,
                                defaultFundRef.Id, lastAgreement, owningUserTeamRef, planIsDollarBased));
                            inactiveMonthCounter = 1;

                        }

                        if (isSelfPayCounterUpdate)
                            selfPayCounter = 0;
                        memberWentToUnderReview = false;
                    }
                    #endregion
                } //end of New Number
                else if ((currentEligibility == (int)MemberPlanBpaCurrentEligibility.NotInBenefit) || (currentEligibility == (int)MemberPlanBpaCurrentEligibility.Reinstatement))
                {
                    #region  ---- NOT IN BENEFIT ----
                    //bool isResinstatement = MemberPlanRetroprocessingHelper_Optimized.IsCategoryReinstatement(service, tracingService,
                    //    eligibilityCategories, usingEligibilityCategory.Id);
                    //decimal reinstatementAmount = 0;

                    //if (isResinstatement)
                    //{
                    //    reinstatementAmount = MemberPlanRetroprocessingHelper_Optimized.GetReinstatementDollars(tracingService,
                    //        eligibilityCategories, usingEligibilityCategory.Id, startWorkMonth);

                    //    if (reinstatementAmount == 0)
                    //        isMisMatch = true;
                    //}
                    Entity eligibilityCategoryUsed = service.Retrieve("bpa_eligibilitycategory", usingEligibilityCategory.Id, new ColumnSet(true));
                    
                    bool isReinstatement = false;
                    if (eligibilityCategoryUsed != null && eligibilityCategoryUsed.Contains("bpa_reinstatement"))
                        isReinstatement = eligibilityCategoryUsed.GetAttributeValue<bool>("bpa_reinstatement");

                    decimal reinstatementAmount = 0;

                    if (isReinstatement)
                    {
                        reinstatementAmount = EligibilityCategoryHelper.GetReinstatementDollars(service, eligibilityCategoryUsed.Id, startWorkMonth);
                        if (reinstatementAmount == 0)
                            isMisMatch = true;
                    }
                    if (isMisMatch)
                    {
                        requestRetroprocessing.Requests.Add(MemberPlanRetroprocessingHelper_Optimized.CreateUnderReviewTransaction(tracingService, memberPlan.Id,
                            bankBalances, usingEligibilityCategory, startWorkMonth,
                            startWorkMonth.AddMonths(eligibilityOffset), (int)BpaTransactionbpaTransactionCategory.Eligibility,
                            (int)BpaTransactionbpaTransactionType.UnderReview, defaultFundRef.Id, lastAgreement, owningUserTeamRef, planIsDollarBased));

                        // Set the Current Eligibility values
                        memberPlanCurrentEligibilityValue = (int)MemberPlanBpaCurrentEligibility.UnderReview;
                        memberPlanBenefitMonth = startWorkMonth.AddMonths(eligibilityOffset);

                        memberWentToUnderReview = true;
                    }
                    else
                    {
                        if (isReinstatement && ((currentEligibility == (int)MemberPlanBpaCurrentEligibility.Reinstatement) || hasECED))
                        {
                            if (sumDollarHour >= reinstatementAmount)
                            {
                                requestRetroprocessing.Requests.Add(MemberPlanRetroprocessingHelper_Optimized.CreateTransaction(tracingService, memberPlan.Id, bankBalances,
                                    startWorkMonth, startWorkMonth.AddMonths(eligibilityOffset), defaultFundRef,
                                    (int)BpaTransactionbpaTransactionCategory.Eligibility, (int)BpaTransactionbpaTransactionType.EligibilityDraw, deductionRate, false, usingEligibilityCategory,
                                    defaultFundRef.Id, lastAgreement, owningUserTeamRef, planIsDollarBased));

                                // Update Banks - Drawing from dollar bank
                                bankBalances = MemberPlanRetroprocessingHelper_Optimized.UpdateBanks(tracingService, bankBalances, deductionRate, 0, 0, 0, false);

                                // Set the Current Eligibility values
                                memberPlanCurrentEligibilityValue = (int)MemberPlanBpaCurrentEligibility.InBenefit;
                                memberPlanBenefitMonth = startWorkMonth.AddMonths(eligibilityOffset);

                                requestRetroprocessing.Requests.Add(MemberPlanRetroprocessingHelper_Optimized.CreateTransaction(tracingService, memberPlan.Id, bankBalances,
                                    startWorkMonth, startWorkMonth.AddMonths(eligibilityOffset), null,
                                    (int)BpaTransactionbpaTransactionCategory.Eligibility, (int)BpaTransactionbpaTransactionType.InBenefit, 0, true, usingEligibilityCategory,
                                    defaultFundRef.Id, lastAgreement, owningUserTeamRef, planIsDollarBased));

                                isSelfPayCounterUpdate = true;
                                lastEligibilityDate = startWorkMonth;
                                inactiveMonthCounter = 0;

                            }
                            else
                            {
                                // Set the Current Eligibility values
                                memberPlanCurrentEligibilityValue = (int)MemberPlanBpaCurrentEligibility.Reinstatement;
                                memberPlanBenefitMonth = startWorkMonth.AddMonths(eligibilityOffset);

                                requestRetroprocessing.Requests.Add(MemberPlanRetroprocessingHelper_Optimized.CreateTransaction(tracingService, memberPlan.Id, bankBalances,
                                    startWorkMonth, startWorkMonth.AddMonths(eligibilityOffset), null,
                                    (int)BpaTransactionbpaTransactionCategory.Eligibility, (int)BpaTransactionbpaTransactionType.ReInstating, 0, true, usingEligibilityCategory,
                                    defaultFundRef.Id, lastAgreement, owningUserTeamRef, planIsDollarBased));
                                isReinstating = true;
                                inactiveMonthCounter = 1;

                            }
                        }
                        else
                        {
                            if (sumDollarHour + deductionRate >= 0)
                            {
                                requestRetroprocessing.Requests.Add(MemberPlanRetroprocessingHelper_Optimized.CreateTransaction(tracingService, memberPlan.Id, bankBalances,
                                    startWorkMonth, startWorkMonth.AddMonths(eligibilityOffset), defaultFundRef,
                                    (int)BpaTransactionbpaTransactionCategory.Eligibility, (int)BpaTransactionbpaTransactionType.EligibilityDraw, deductionRate, false, usingEligibilityCategory,
                                    defaultFundRef.Id, lastAgreement, owningUserTeamRef, planIsDollarBased));

                                // Update Banks - Drawing from dollar bank
                                bankBalances = MemberPlanRetroprocessingHelper_Optimized.UpdateBanks(tracingService, bankBalances, deductionRate, 0, 0, 0, false);

                                // Set the Current Eligibility values
                                memberPlanCurrentEligibilityValue = (int)MemberPlanBpaCurrentEligibility.InBenefit;
                                memberPlanBenefitMonth = startWorkMonth.AddMonths(eligibilityOffset);

                                requestRetroprocessing.Requests.Add(MemberPlanRetroprocessingHelper_Optimized.CreateTransaction(tracingService, memberPlan.Id, bankBalances,
                                    startWorkMonth, startWorkMonth.AddMonths(eligibilityOffset), null,
                                    (int)BpaTransactionbpaTransactionCategory.Eligibility, (int)BpaTransactionbpaTransactionType.InBenefit, 0, true, usingEligibilityCategory,
                                    defaultFundRef.Id, lastAgreement, owningUserTeamRef, planIsDollarBased));

                                isSelfPayCounterUpdate = true;
                                lastEligibilityDate = startWorkMonth;
                                inactiveMonthCounter = 0;
                            }
                            else if (sumSecondaryBank + deductionRate >= 0)
                            {
                                #region ---- SECONDARY DOLLAR BANK DRAW ----

                                // Secondary Dollar Bank draw
                                requestRetroprocessing.Requests.Add(MemberPlanRetroprocessingHelper_Optimized.CreateTransaction(tracingService, memberPlan.Id, bankBalances,
                                    startWorkMonth, startWorkMonth.AddMonths(eligibilityOffset), defaultFundRef,
                                    (int)BpaTransactionbpaTransactionCategory.Eligibility, (int)BpaTransactionbpaTransactionType.SecondaryDollarBankDraw, deductionRate, false, usingEligibilityCategory,
                                    defaultFundRef.Id, lastAgreement, owningUserTeamRef, planIsDollarBased));

                                // Update Banks - Drawing from secondary dollar bank
                                bankBalances = MemberPlanRetroprocessingHelper_Optimized.UpdateBanks(tracingService, bankBalances, 0, 0, 0, deductionRate, false);


                                // Set the Current Eligibility values
                                memberPlanCurrentEligibilityValue = (int)MemberPlanBpaCurrentEligibility.InBenefit;
                                memberPlanBenefitMonth = startWorkMonth.AddMonths(eligibilityOffset);

                                requestRetroprocessing.Requests.Add(MemberPlanRetroprocessingHelper_Optimized.CreateTransaction(tracingService, memberPlan.Id, bankBalances,
                                    startWorkMonth, startWorkMonth.AddMonths(eligibilityOffset), null,
                                    (int)BpaTransactionbpaTransactionCategory.Eligibility, (int)BpaTransactionbpaTransactionType.InBenefit, 0, true, usingEligibilityCategory,
                                    defaultFundRef.Id, lastAgreement, owningUserTeamRef, planIsDollarBased));

                                isSelfPayCounterUpdate = true;
                                lastEligibilityDate = startWorkMonth;
                                inactiveMonthCounter = 0;
                                #endregion
                            }
                            else
                            {
                                // Set the Current Eligibility values
                                memberPlanCurrentEligibilityValue = (int)MemberPlanBpaCurrentEligibility.NotInBenefit;
                                memberPlanBenefitMonth = startWorkMonth.AddMonths(eligibilityOffset);

                                requestRetroprocessing.Requests.Add(MemberPlanRetroprocessingHelper_Optimized.CreateTransaction(tracingService, memberPlan.Id, bankBalances,
                                    startWorkMonth, startWorkMonth.AddMonths(eligibilityOffset), null,
                                    (int)BpaTransactionbpaTransactionCategory.Eligibility, (int)BpaTransactionbpaTransactionType.NotInBenefit, 0, true, usingEligibilityCategory,
                                    defaultFundRef.Id, lastAgreement, owningUserTeamRef, planIsDollarBased));

                                inactiveMonthCounter = 1;
                            }
                        }
                        if (isSelfPayCounterUpdate)
                            selfPayCounter = 0;
                        memberWentToUnderReview = false;
                    }
                    #endregion
                }
                else if ((currentEligibility == (int)MemberPlanBpaCurrentEligibility.InBenefit) || (currentEligibility == (int)MemberPlanBpaCurrentEligibility.SelfPayInBenefit))
                {
                    #region ---- IN BENEFIT & SELFPAY IN BENEFIT ----
                    if (isMisMatch)
                    {
                        requestRetroprocessing.Requests.Add(MemberPlanRetroprocessingHelper_Optimized.CreateUnderReviewTransaction(tracingService, memberPlan.Id,
                            bankBalances, usingEligibilityCategory, startWorkMonth,
                            startWorkMonth.AddMonths(eligibilityOffset), (int)BpaTransactionbpaTransactionCategory.Eligibility,
                            (int)BpaTransactionbpaTransactionType.UnderReview, defaultFundRef.Id, lastAgreement, owningUserTeamRef, planIsDollarBased));

                        // Set the Current Eligibility values
                        memberPlanCurrentEligibilityValue = (int)MemberPlanBpaCurrentEligibility.UnderReview;
                        memberPlanBenefitMonth = startWorkMonth.AddMonths(eligibilityOffset);

                        memberWentToUnderReview = true;
                    }
                    else
                    {
                        // Find MaxConsecutiveMonthsSelfPay from Eligibility Category 
                        int maxConsecutiveMonthsSelfPay = MemberPlanRetroprocessingHelper_Optimized.GetMaxConsecutiveMonthsSelfPay(
                            tracingService, eligibilityCategories, usingEligibilityCategory.Id);

                        // Dollar Bank is enough for eligibility draw
                        if (sumDollarHour + deductionRate >= 0)
                        {
                            // Dollar Bank and Hour Bank has enough or not? 
                            requestRetroprocessing.Requests.Add(MemberPlanRetroprocessingHelper_Optimized.CreateTransaction(tracingService, memberPlan.Id, bankBalances,
                                startWorkMonth, startWorkMonth.AddMonths(eligibilityOffset), defaultFundRef,
                                (int)BpaTransactionbpaTransactionCategory.Eligibility, (int)BpaTransactionbpaTransactionType.EligibilityDraw, deductionRate, false, usingEligibilityCategory,
                                defaultFundRef.Id, lastAgreement, owningUserTeamRef, planIsDollarBased));

                            // Update Banks - Drawing from dollar bank
                            bankBalances = MemberPlanRetroprocessingHelper_Optimized.UpdateBanks(tracingService, bankBalances, deductionRate, 0, 0, 0, false);

                            // Set the Current Eligibility values
                            memberPlanCurrentEligibilityValue = (int)MemberPlanBpaCurrentEligibility.InBenefit;
                            memberPlanBenefitMonth = startWorkMonth.AddMonths(eligibilityOffset);

                            requestRetroprocessing.Requests.Add(MemberPlanRetroprocessingHelper_Optimized.CreateTransaction(tracingService, memberPlan.Id, bankBalances,
                                startWorkMonth, startWorkMonth.AddMonths(eligibilityOffset), null,
                                (int)BpaTransactionbpaTransactionCategory.Eligibility, (int)BpaTransactionbpaTransactionType.InBenefit, 0, true, usingEligibilityCategory,
                                defaultFundRef.Id, lastAgreement, owningUserTeamRef, planIsDollarBased));

                            lastEligibilityDate = startWorkMonth;
                            isSelfPayCounterUpdate = true;
                            inactiveMonthCounter = 0;
                        }
                        else if (sumSecondaryBank + deductionRate >= 0)
                        {
                            // Secondary Dollar Bank draw
                            requestRetroprocessing.Requests.Add(MemberPlanRetroprocessingHelper_Optimized.CreateTransaction(tracingService, memberPlan.Id, bankBalances,
                                startWorkMonth, startWorkMonth.AddMonths(eligibilityOffset), defaultFundRef,
                                (int)BpaTransactionbpaTransactionCategory.Eligibility, (int)BpaTransactionbpaTransactionType.SecondaryDollarBankDraw, deductionRate, false, usingEligibilityCategory,
                                defaultFundRef.Id, lastAgreement, owningUserTeamRef, planIsDollarBased));

                            // Update Banks - Drawing from secondary dollar bank
                            bankBalances = MemberPlanRetroprocessingHelper_Optimized.UpdateBanks(tracingService, bankBalances, 0, 0, 0, deductionRate, false);

                            // Set the Current Eligibility values
                            memberPlanCurrentEligibilityValue = (int)MemberPlanBpaCurrentEligibility.InBenefit;
                            memberPlanBenefitMonth = startWorkMonth.AddMonths(eligibilityOffset);

                            requestRetroprocessing.Requests.Add(MemberPlanRetroprocessingHelper_Optimized.CreateTransaction(tracingService, memberPlan.Id, bankBalances,
                                startWorkMonth, startWorkMonth.AddMonths(eligibilityOffset), null,
                                (int)BpaTransactionbpaTransactionCategory.Eligibility, (int)BpaTransactionbpaTransactionType.InBenefit, 0, true, usingEligibilityCategory,
                                defaultFundRef.Id, lastAgreement, owningUserTeamRef, planIsDollarBased));

                            inactiveMonthCounter = 0;
                            lastEligibilityDate = startWorkMonth;
                            isSelfPayCounterUpdate = true;
                        }

                        // Dollar Bank is enough for maximum Fund Assistance
                        else if (sumDollarHour >= fundAssistanceAmount)
                        {
                            #region ---- DOLLAR BANK IS ENOUGH FOR FUND ASSISTANCE AMOUNT ----
                            decimal maximumSelfPayAmount = (deductionRate * -1) - sumDollarHour;
                            if (selfPayBank.Value >= maximumSelfPayAmount)
                            {
                                // SelfPay bank is enough
                                // 1. Draw of 'maximumAssistance' which is self pay
                                // 2. Draw full amount of dollar bank
                                if (countSelfPay < maxConsecutiveMonthsSelfPay)
                                {
                                    requestRetroprocessing.Requests.Add(MemberPlanRetroprocessingHelper_Optimized.CreateTransaction(tracingService, memberPlan.Id, bankBalances,
                                        startWorkMonth, startWorkMonth.AddMonths(eligibilityOffset), defaultFundRef,
                                        (int)BpaTransactionbpaTransactionCategory.Eligibility, (int)BpaTransactionbpaTransactionType.EligibilityDraw, (sumDollarHour * -1),
                                        false, usingEligibilityCategory,
                                        defaultFundRef.Id, lastAgreement, owningUserTeamRef, planIsDollarBased));

                                    requestRetroprocessing.Requests.Add(MemberPlanRetroprocessingHelper_Optimized.CreateTransaction(tracingService, memberPlan.Id, bankBalances,
                                        startWorkMonth, startWorkMonth.AddMonths(eligibilityOffset), defaultFundRef,
                                        (int)BpaTransactionbpaTransactionCategory.Eligibility, (int)BpaTransactionbpaTransactionType.SelfPayDraw, (Math.Abs(maximumSelfPayAmount) * -1),
                                        false, usingEligibilityCategory,
                                        defaultFundRef.Id, lastAgreement, owningUserTeamRef, planIsDollarBased));

                                    // Update Banks - Drawing from dollar bank and selfpay bank
                                    bankBalances = MemberPlanRetroprocessingHelper_Optimized.UpdateBanks(tracingService, bankBalances, (Math.Abs(sumDollarHour) * -1), 0, (Math.Abs(maximumSelfPayAmount) * -1), 0, false);

                                    requestRetroprocessing.Requests.Add(MemberPlanRetroprocessingHelper_Optimized.CreateTransaction(tracingService, memberPlan.Id, bankBalances,
                                        startWorkMonth, startWorkMonth.AddMonths(eligibilityOffset), null,
                                        (int)BpaTransactionbpaTransactionCategory.Eligibility, (int)BpaTransactionbpaTransactionType.SelfPayInBenefit, 0, true, usingEligibilityCategory,
                                        defaultFundRef.Id, lastAgreement, owningUserTeamRef, planIsDollarBased));

                                    // Set the Current Eligibility values
                                    memberPlanCurrentEligibilityValue = (int)MemberPlanBpaCurrentEligibility.SelfPayInBenefit;
                                    memberPlanBenefitMonth = startWorkMonth.AddMonths(eligibilityOffset);

                                    // Increment the Self Pay counter
                                    //selfPayCounter = countSelfPay + 1;
                                    countSelfPay = 1;
                                    
                                    lastEligibilityDate = startWorkMonth;
                                    inactiveMonthCounter = 0;

                                }
                                else
                                {
                                    // Set the Current Eligibility values
                                    memberPlanCurrentEligibilityValue = (int)MemberPlanBpaCurrentEligibility.NotInBenefit;
                                    memberPlanBenefitMonth = startWorkMonth.AddMonths(eligibilityOffset);

                                    requestRetroprocessing.Requests.Add(MemberPlanRetroprocessingHelper_Optimized.CreateTransaction(tracingService, memberPlan.Id, bankBalances,
                                        startWorkMonth, startWorkMonth.AddMonths(eligibilityOffset), null,
                                        (int)BpaTransactionbpaTransactionCategory.Eligibility, (int)BpaTransactionbpaTransactionType.NotInBenefit, 0, true, usingEligibilityCategory,
                                        defaultFundRef.Id, lastAgreement, owningUserTeamRef, planIsDollarBased));

                                    inactiveMonthCounter = 1;
                                }
                            }
                            else
                            {
                                // Set the Current Eligibility values
                                memberPlanCurrentEligibilityValue = (int)MemberPlanBpaCurrentEligibility.NotInBenefit;
                                memberPlanBenefitMonth = startWorkMonth.AddMonths(eligibilityOffset);

                                requestRetroprocessing.Requests.Add(MemberPlanRetroprocessingHelper_Optimized.CreateTransaction(tracingService, memberPlan.Id, bankBalances,
                                    startWorkMonth, startWorkMonth.AddMonths(eligibilityOffset), null,
                                    (int)BpaTransactionbpaTransactionCategory.Eligibility, (int)BpaTransactionbpaTransactionType.NotInBenefit, 0, true, usingEligibilityCategory,
                                    defaultFundRef.Id, lastAgreement, owningUserTeamRef, planIsDollarBased));

                                inactiveMonthCounter = 1;
                            }
                            if (isSelfPayCounterUpdate)
                                selfPayCounter = 0;
                            #endregion
                        }
                        else
                        {
                            #region ---- DOLLAR BANK IS NOT ENOUGH FOR FUND ASSISTANCE AMOUNT ----

                            // Self Pay bank is enough for Self Pay draw
                            if ((selfPayDeductionRate != 0) && (selfPayBank.Value >= (selfPayDeductionRate * -1)) && (sumDollarHour >= 0))
                            {
                                if (countSelfPay < maxConsecutiveMonthsSelfPay)
                                {
                                    // Give Maximum Fund Assistance Amount
                                    decimal fundAssistanceEligibilityAmount = Math.Abs(deductionRate) - Math.Abs(sumDollarHour) - Math.Abs(selfPayDeductionRate);

                                    requestRetroprocessing.Requests.Add(MemberPlanRetroprocessingHelper_Optimized.CreateFundAssistedTransactions(
                                        tracingService, memberPlan.Id, bankBalances, startWorkMonth, startWorkMonth.AddMonths(eligibilityOffset), new Money(fundAssistanceEligibilityAmount),
                                        null, (int)BpaTransactionbpaTransactionCategory.Eligibility, (int)BpaTransactionbpaTransactionType.FundAssisted, defaultFundRef,
                                        usingEligibilityCategory, defaultFundRef.Id, lastAgreement, owningUserTeamRef, planIsDollarBased));

                                    // Make a Eligibility Draw of Fund Assistance Amount
                                    requestRetroprocessing.Requests.Add(MemberPlanRetroprocessingHelper_Optimized.CreateFundAssistedTransactions(
                                        tracingService, memberPlan.Id, bankBalances, startWorkMonth, startWorkMonth.AddMonths(eligibilityOffset), new Money(fundAssistanceAmount * -1),
                                        null, (int)BpaTransactionbpaTransactionCategory.Eligibility, (int)BpaTransactionbpaTransactionType.EligibilityDraw, defaultFundRef,
                                        null, defaultFundRef.Id, lastAgreement, owningUserTeamRef, planIsDollarBased));

                                    // Create SelfPay draw and Self pay In Benefit Transaction
                                    requestRetroprocessing.Requests.Add(MemberPlanRetroprocessingHelper_Optimized.CreateTransaction(tracingService, memberPlan.Id, bankBalances,
                                        startWorkMonth, startWorkMonth.AddMonths(eligibilityOffset), defaultFundRef,
                                        (int)BpaTransactionbpaTransactionCategory.Eligibility, (int)BpaTransactionbpaTransactionType.SelfPayDraw, selfPayDeductionRate,
                                        false, usingEligibilityCategory,
                                        defaultFundRef.Id, lastAgreement, owningUserTeamRef, planIsDollarBased));

                                    // Update Banks - Drawing from selfpay bank
                                    decimal tmpDollarHours = 0;
                                    if (bankBalances.ContainsKey("dollarhours"))
                                        tmpDollarHours = (bankBalances["dollarhours"] * -1 );// (decimal)dollarHours;

                                    //if (fundAssistanceEligibilityAmount != 0)
                                    //    fundAssistanceEligibilityAmount = (fundAssistanceEligibilityAmount * -1);
                                    bankBalances = MemberPlanRetroprocessingHelper_Optimized.UpdateBanks(tracingService, bankBalances, (sumDollarHour * -1), 0, selfPayDeductionRate, 0, false);

                                    requestRetroprocessing.Requests.Add(MemberPlanRetroprocessingHelper_Optimized.CreateTransaction(tracingService, memberPlan.Id, bankBalances,
                                        startWorkMonth, startWorkMonth.AddMonths(eligibilityOffset), null,
                                        (int)BpaTransactionbpaTransactionCategory.Eligibility, (int)BpaTransactionbpaTransactionType.SelfPayInBenefit, 0,
                                        true, usingEligibilityCategory,
                                        defaultFundRef.Id, lastAgreement, owningUserTeamRef, planIsDollarBased));

                                    // Set the Current Eligibility values
                                    memberPlanCurrentEligibilityValue = (int)MemberPlanBpaCurrentEligibility.SelfPayInBenefit;
                                    memberPlanBenefitMonth = startWorkMonth.AddMonths(eligibilityOffset);

                                    // Increment Self Pay counter
                                    //selfPayCounter = countSelfPay + 1;
                                    countSelfPay = 1;
                                    //isSelfPayCounterUpdate = true;
                                    lastEligibilityDate = startWorkMonth;
                                    inactiveMonthCounter = 0;

                                }
                                else
                                {
                                    // Set the Current Eligibility values
                                    memberPlanCurrentEligibilityValue = (int)MemberPlanBpaCurrentEligibility.NotInBenefit;
                                    memberPlanBenefitMonth = startWorkMonth.AddMonths(eligibilityOffset);

                                    requestRetroprocessing.Requests.Add(MemberPlanRetroprocessingHelper_Optimized.CreateTransaction(tracingService, memberPlan.Id, bankBalances,
                                        startWorkMonth, startWorkMonth.AddMonths(eligibilityOffset), null,
                                        (int)BpaTransactionbpaTransactionCategory.Eligibility, (int)BpaTransactionbpaTransactionType.NotInBenefit, 0,
                                        true, usingEligibilityCategory,
                                        defaultFundRef.Id, lastAgreement, owningUserTeamRef, planIsDollarBased));

                                    inactiveMonthCounter = 1;
                                }
                            }
                            else
                            {
                                // Set the Current Eligibility values
                                memberPlanCurrentEligibilityValue = (int)MemberPlanBpaCurrentEligibility.NotInBenefit;
                                memberPlanBenefitMonth = startWorkMonth.AddMonths(eligibilityOffset);

                                requestRetroprocessing.Requests.Add(MemberPlanRetroprocessingHelper_Optimized.CreateTransaction(tracingService, memberPlan.Id, bankBalances,
                                    startWorkMonth, startWorkMonth.AddMonths(eligibilityOffset), null,
                                    (int)BpaTransactionbpaTransactionCategory.Eligibility, (int)BpaTransactionbpaTransactionType.NotInBenefit, 0,
                                    true, usingEligibilityCategory,
                                    defaultFundRef.Id, lastAgreement, owningUserTeamRef, planIsDollarBased));

                                inactiveMonthCounter = 1;
                            }
                        }
                        if (isSelfPayCounterUpdate)
                            selfPayCounter = 0;
                        memberWentToUnderReview = false;
                        #endregion
                    }
                    #endregion
                }
                else if (currentEligibility == (int)MemberPlanBpaCurrentEligibility.UnderReview)
                {
                    #region ---- UNDER REVIEW ----
                    if (previousMonthEligibility == null)
                    {
                        if (sumDollarHour >= miniInitEligibility)
                        {
                            requestRetroprocessing.Requests.Add(MemberPlanRetroprocessingHelper_Optimized.CreateTransaction(tracingService, memberPlan.Id, bankBalances,
                                startWorkMonth, startWorkMonth.AddMonths(eligibilityOffset), defaultFundRef,
                                (int)BpaTransactionbpaTransactionCategory.Eligibility, (int)BpaTransactionbpaTransactionType.EligibilityDraw, deductionRate, false, usingEligibilityCategory,
                                defaultFundRef.Id, lastAgreement, owningUserTeamRef, planIsDollarBased));

                            // Update Banks - Drawing from dollar bank
                            bankBalances = MemberPlanRetroprocessingHelper_Optimized.UpdateBanks(tracingService, bankBalances, deductionRate, 0, 0, 0, false);

                            // Set the Current Eligibility values
                            memberPlanCurrentEligibilityValue = (int)MemberPlanBpaCurrentEligibility.InBenefit;
                            memberPlanBenefitMonth = startWorkMonth.AddMonths(eligibilityOffset);

                            requestRetroprocessing.Requests.Add(MemberPlanRetroprocessingHelper_Optimized.CreateTransaction(tracingService, memberPlan.Id, bankBalances,
                                startWorkMonth, startWorkMonth.AddMonths(eligibilityOffset), null,
                                (int)BpaTransactionbpaTransactionCategory.Eligibility, (int)BpaTransactionbpaTransactionType.InBenefit, 0, true, usingEligibilityCategory,
                                defaultFundRef.Id, lastAgreement, owningUserTeamRef, planIsDollarBased));

                            lastEligibilityDate = startWorkMonth;
                            isSelfPayCounterUpdate = true;
                        }
                        else if (sumSecondaryBank >= miniInitEligibility)
                        {
                            requestRetroprocessing.Requests.Add(MemberPlanRetroprocessingHelper_Optimized.CreateTransaction(tracingService, memberPlan.Id, bankBalances,
                                startWorkMonth, startWorkMonth.AddMonths(eligibilityOffset), defaultFundRef,
                                (int)BpaTransactionbpaTransactionCategory.Eligibility, (int)BpaTransactionbpaTransactionType.SecondaryDollarBankDraw, deductionRate,
                                false, usingEligibilityCategory,
                                defaultFundRef.Id, lastAgreement, owningUserTeamRef, planIsDollarBased));

                            // Update Banks - Drawing from secondary dollar bank
                            bankBalances = MemberPlanRetroprocessingHelper_Optimized.UpdateBanks(tracingService, bankBalances, 0, 0, 0, deductionRate, false);

                            // Set the Current Eligibility values
                            memberPlanCurrentEligibilityValue = (int)MemberPlanBpaCurrentEligibility.InBenefit;
                            memberPlanBenefitMonth = startWorkMonth.AddMonths(eligibilityOffset);

                            requestRetroprocessing.Requests.Add(MemberPlanRetroprocessingHelper_Optimized.CreateTransaction(tracingService, memberPlan.Id, bankBalances,
                                startWorkMonth, startWorkMonth.AddMonths(eligibilityOffset), null,
                                (int)BpaTransactionbpaTransactionCategory.Eligibility, (int)BpaTransactionbpaTransactionType.InBenefit, 0, true, usingEligibilityCategory,
                                defaultFundRef.Id, lastAgreement, owningUserTeamRef, planIsDollarBased));

                            lastEligibilityDate = startWorkMonth;
                            isSelfPayCounterUpdate = true;
                        }

                        else
                        {
                            // Set the Current Eligibility values
                            memberPlanCurrentEligibilityValue = (int)MemberPlanBpaCurrentEligibility.NewMember;
                            memberPlanBenefitMonth = startWorkMonth.AddMonths(eligibilityOffset);

                            requestRetroprocessing.Requests.Add(MemberPlanRetroprocessingHelper_Optimized.CreateTransaction(tracingService, memberPlan.Id, bankBalances,
                                startWorkMonth, startWorkMonth.AddMonths(eligibilityOffset), null,
                                (int)BpaTransactionbpaTransactionCategory.Eligibility, (int)BpaTransactionbpaTransactionType.NewMember, 0, true, usingEligibilityCategory,
                                defaultFundRef.Id, lastAgreement, owningUserTeamRef, planIsDollarBased));
                        }
                        memberWentToUnderReview = false;
                    }
                    else
                    {
                        // Set the Current Eligibility values
                        memberPlanCurrentEligibilityValue = (int)MemberPlanBpaCurrentEligibility.UnderReview;
                        memberPlanBenefitMonth = startWorkMonth.AddMonths(eligibilityOffset);

                        requestRetroprocessing.Requests.Add(MemberPlanRetroprocessingHelper_Optimized.CreateUnderReviewTransaction(tracingService, memberPlan.Id,
                            bankBalances, usingEligibilityCategory, startWorkMonth,
                            startWorkMonth.AddMonths(eligibilityOffset), (int)BpaTransactionbpaTransactionCategory.Eligibility,
                            (int)BpaTransactionbpaTransactionType.UnderReview, defaultFundRef.Id, lastAgreement, owningUserTeamRef, planIsDollarBased));
                        memberWentToUnderReview = true;
                    }
                    if (isSelfPayCounterUpdate)
                        selfPayCounter = 0;
                    inactiveMonthCounter = 0;
                    #endregion
                }
                else if (currentEligibility == (int)MemberPlanBpaCurrentEligibility.Frozen)
                {
                    memberPlanCurrentEligibilityValue = (int)MemberPlanBpaCurrentEligibility.Frozen;

                }
                #endregion
            }



            #region ---- RESERVE TRANSFER ----
            if (usingEligibilityCategory != null)
            {
                //tracingService.Trace($@"Inside RESERVE TRANSFER {startWorkMonth}");
                // Verify this month already has a reserve transfer or not
                // This probably needs to be verified
                bool isReserveExists = MemberPlanRetroprocessingHelper_Optimized.IsReserveExists(tracingService, transactions, memberPlan.Id, startWorkMonth);
                if (isReserveExists == false)
                {
                    // Get all of the totals of the banks from CRM
                    decimal currentSumDollarHour = 0;
                    if (bankBalances.ContainsKey("dollarhours") == true)
                        currentSumDollarHour = bankBalances["dollarhours"];

                    if (currentSumDollarHour > maxMemberBank)
                    {
                        decimal reserveTransferAmount = currentSumDollarHour - maxMemberBank;
                        if ((reserveTransferAmount > 0) && (memberWentToUnderReview == false) && (currentEligibility != (int)MemberPlanBpaCurrentEligibility.Frozen))
                        {

                            requestRetroprocessing.Requests.Add(MemberPlanRetroprocessingHelper_Optimized.CreateReserveTransferHour(tracingService,
                                memberPlan.Id, startWorkMonth, startWorkMonth.AddMonths(eligibilityOffset),
                                0, (reserveTransferAmount * -1), defaultFundRef.Id, owningUserTeamRef));

                            // Update Banks - Drawing from hours bank
                            bankBalances = MemberPlanRetroprocessingHelper_Optimized.UpdateBanks(tracingService, bankBalances, (reserveTransferAmount * -1), 0, 0, 0, false);

                            tracingService.Trace($@"insert month {startWorkMonth}");

                            // Find current month target from requestRetroprocessing
                            reserveTransactionWorkMonth.Add(startWorkMonth);
                        }
                    }
                }
            }


            #endregion

            #region ---- INACTIVE MONTH COUNTER LOGIC ----
            int consecutiveInactiveMonths = 0;

            if (hasECED)
            {
                inactiveMonthCounter = 0;
                lastEligibilityDate = startWorkMonth;
            }
            else
            {
                if (inactiveMonthCounter == 1 || isReinstating)
                {
                    DateTime end = startWorkMonth.AddMonths(-1);
                    DateTime lastActivityDate = DateTime.MinValue;
                    Entity lastIA = MemberPlanRetroprocessingHelper_Optimized.FetchLastEligiblityContributionMonth(tracingService, transactions, startWorkMonth, memberPlan.Id);
                    if (lastIA != null && lastIA.Attributes.Contains("bpa_transactiondate"))
                    {
                        lastActivityDate = lastIA.GetAttributeValue<DateTime>("bpa_transactiondate");
                    }

                    if (lastActivityDate == DateTime.MinValue && lastEligibilityDate == DateTime.MinValue)
                    {
                        // Retrive directly from Member plan
                        consecutiveInactiveMonths = memberPlan.Contains("bpa_consecutiveinactivemonths") ? memberPlan.GetAttributeValue<int>("bpa_consecutiveinactivemonths") : 0;
                    }
                    else
                    {
                        if (lastActivityDate > lastEligibilityDate)
                            consecutiveInactiveMonths = end.Month + end.Year * 12 - (lastActivityDate.Month + lastActivityDate.Year * 12);
                        else
                            consecutiveInactiveMonths = end.Month + end.Year * 12 - (lastEligibilityDate.Month + lastEligibilityDate.Year * 12);
                    }
                }
            }
            #endregion

            // Set variables
            inactiveMonthCounter = inactiveMonthCounter + consecutiveInactiveMonths;
            selfPayCounter = selfPayCounter + countSelfPay;

            #region ---- CREATE INACTIVE AND RESERVE TRANSFER TRANSACTION ----

            // Create 'Inactive' eligibility transactions
            // Create 'Reserve Transfer' eligibility transactions
            // Also check if there are any extra transactions in the future and delete them
            if (inactiveMonthCounter >= 0)
            {
                bool isOverMax = false;
                if (inactiveMonthCounter > maxInactiveCounter)
                {
                    inactiveMonthCounter = maxInactiveCounter;
                    isOverMax = true;
                }
                if (inactiveMonthCounter <= maxInactiveCounter)
                {
                    // NOTE:  Updating totals on MemberPlan is now moved to the end of the loop
                    if (inactiveMonthCounter > 0)
                    {
                        if (inactiveMonthCounter == maxInactiveCounter)
                        {
                            EntityCollection inactiveNReserveTransactions = MemberPlanRetroprocessingHelper_Optimized.AlreadyHasInactiveAndReserveTransactions(
                                tracingService, transactions, memberPlan.Id, startWorkMonth.AddMonths(1));
                            if (inactiveNReserveTransactions != null && inactiveNReserveTransactions.Entities.Count > 0)
                            {
                                foreach (Entity t in inactiveNReserveTransactions.Entities)
                                {
                                    requestRetroprocessing.Requests.Add(MemberPlanRetroprocessingHelper_Optimized.DeleteTransaction(tracingService, t.Id));
                                }
                            }
                            if (isOverMax)
                            {
                                // Create 'Inactive' eligibility transactions
                                // Create 'Reserve Transfer' eligibility transactions

                                // Create the Reserve Transfer Transaction and then the regular inactive transaction
                                requestRetroprocessing.Requests.Add(MemberPlanRetroprocessingHelper_Optimized.CreateReserveTransferTransaction(tracingService,
                                    memberPlan.Id, startWorkMonth,
                                    startWorkMonth.AddMonths(eligibilityOffset), string.Empty, (int)BpaTransactionbpaTransactionCategory.Eligibility,
                                    (int)BpaTransactionbpaTransactionType.ReserveTransfer, 0, true, usingEligibilityCategory, bankBalances, defaultFundRef.Id,
                                    lastAgreement, owningUserTeamRef));

                                // Update Banks - Drawing from dollar bank
                                bankBalances = MemberPlanRetroprocessingHelper_Optimized.UpdateBanks(tracingService, bankBalances, 0, 0, 0, 0, true);

                                requestRetroprocessing.Requests.Add(MemberPlanRetroprocessingHelper_Optimized.CreateTransaction(tracingService, memberPlan.Id,
                                    bankBalances, startWorkMonth,
                                    startWorkMonth.AddMonths(eligibilityOffset), null, (int)BpaTransactionbpaTransactionCategory.Eligibility,
                                    (int)BpaTransactionbpaTransactionType.Inactive, 0, true, usingEligibilityCategory,
                                    defaultFundRef.Id, lastAgreement, owningUserTeamRef, planIsDollarBased));

                                // Delete Transaction for that month
                                requestRetroprocessing.Requests.Add(MemberPlanRetroprocessingHelper_Optimized.DeleteTransactionNotInBenefit(tracingService, transactions, memberPlan.Id, startWorkMonth));

                            }
                            else
                            {
                                // Create the Reserve Transfer Transaction and then the regular inactive transaction
                                requestRetroprocessing.Requests.Add(MemberPlanRetroprocessingHelper_Optimized.CreateReserveTransferTransaction(tracingService,
                                    memberPlan.Id, startWorkMonth.AddMonths(1),
                                    startWorkMonth.AddMonths(eligibilityOffset + 1), string.Empty, (int)BpaTransactionbpaTransactionCategory.Eligibility,
                                    (int)BpaTransactionbpaTransactionType.ReserveTransfer, 0, true, usingEligibilityCategory, bankBalances, defaultFundRef.Id,
                                    lastAgreement, owningUserTeamRef));

                                // Update Banks - Drawing from dollar bank
                                bankBalances = MemberPlanRetroprocessingHelper_Optimized.UpdateBanks(tracingService, bankBalances, 0, 0, 0, 0, true);

                                requestRetroprocessing.Requests.Add(MemberPlanRetroprocessingHelper_Optimized.CreateTransaction(tracingService, memberPlan.Id,
                                    bankBalances, startWorkMonth.AddMonths(1),
                                    startWorkMonth.AddMonths(eligibilityOffset + 1), null, (int)BpaTransactionbpaTransactionCategory.Eligibility,
                                    (int)BpaTransactionbpaTransactionType.Inactive, 0, true, usingEligibilityCategory,
                                    defaultFundRef.Id, lastAgreement, owningUserTeamRef, planIsDollarBased));
                            }

                            // Update Contact current eligibility to 'INACTIVE'
                            // Set the Current Eligibility values
                            memberPlanCurrentEligibilityValue = (int)MemberPlanBpaCurrentEligibility.Inactive;
                            memberPlanBenefitMonth = startWorkMonth.AddMonths(eligibilityOffset);

                            isMemberBecomeInActive = true;
                        }
                    }
                }

            }
            #endregion
            // NOTE:  Updating totals on MemberPlan is now moved to the end of the loop
            updatedBankBalances = bankBalances;
        }
        #endregion
    }
}