﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;

#endregion

namespace BPA.Benefit.Management.CRM.Plugins.Helper
{
    public class RecalculationTransactionHelper
    {
        public static Entity FetchPreviousMonthEligibility(IOrganizationService service, Guid memberId, DateTime workMonth)
        {
            Entity transaction = null;

            string fetchxml = @"
                    <fetch>
                      <entity name='bpa_transaction' >
                        <all-attributes/>
                        <filter type='and' >
                          <condition attribute='statecode' operator='eq' value='0' />
                          <condition attribute='bpa_memberplanid' operator='eq' value='" + memberId + @"' />
                          <condition attribute='bpa_transactiondate' operator='ge' value='" + workMonth.ToShortDateString() + @"' />
                          <condition attribute='bpa_transactiondate' operator='le' value='" + workMonth.AddDays(1).ToShortDateString() + @"' />
                          <condition attribute='bpa_transactioncategory' operator='eq' value='" + (int)BpaTransactionbpaTransactionCategory.Eligibility + @"' />
                          <condition attribute='bpa_transactiontype' operator='in' >
                            <value>" + (int)BpaTransactionbpaTransactionType.InBenefit + @"</value>
                            <value>" + (int)BpaTransactionbpaTransactionType.NotInBenefit + @"</value>
                            <value>" + (int)BpaTransactionbpaTransactionType.SelfPayInBenefit + @"</value>
                            <value>" + (int)BpaTransactionbpaTransactionType.NewMember + @"</value>
                            <value>" + (int)BpaTransactionbpaTransactionType.FundAssistedInBenefit + @"</value>
                            <value>" + (int)BpaTransactionbpaTransactionType.UnderReview + @"</value>
                            <value>" + (int)BpaTransactionbpaTransactionType.TransferOut + @"</value>
                            <value>" + (int)BpaTransactionbpaTransactionType.Frozen + @"</value>
                            <value>" + (int)BpaTransactionbpaTransactionType.ReInstating + @"</value>
                          </condition>
                        </filter>
                      </entity>
                    </fetch>
                ";
            EntityCollection transactions = service.RetrieveMultiple(new FetchExpression(fetchxml));
            if (transactions != null && transactions.Entities.Count > 0)
            {
                transaction = transactions.Entities.ToList().FirstOrDefault();
            }
            return transaction;
        }

        public static Entity FetchMaxEligibilityWorkMonth(IOrganizationService service, Guid memberId)
        {
            Entity transaction = null;

            string fetchxml = @"
                    <fetch aggregate='true' >
                      <entity name='bpa_transaction' >
                        <attribute name='bpa_transactiondate' alias='bpa_transactiondate' aggregate='max' />
                        <filter type='and' >
                          <condition attribute='statecode' operator='eq' value='0' />
                          <condition attribute='bpa_memberplanid' operator='eq' value='" + memberId + @"' />
                          <condition attribute='bpa_transactioncategory' operator='eq' value='" + (int)BpaTransactionbpaTransactionCategory.Eligibility + @"' />
                          <condition attribute='bpa_transactiontype' operator='in' >
                            <value>" + (int)BpaTransactionbpaTransactionType.InBenefit + @"</value>
                            <value>" + (int)BpaTransactionbpaTransactionType.NotInBenefit + @"</value>
                            <value>" + (int)BpaTransactionbpaTransactionType.SelfPayInBenefit + @"</value>
                            <value>" + (int)BpaTransactionbpaTransactionType.NewMember + @"</value>
                            <value>" + (int)BpaTransactionbpaTransactionType.FundAssistedInBenefit + @"</value>
                            <value>" + (int)BpaTransactionbpaTransactionType.UnderReview + @"</value>
                            <value>" + (int)BpaTransactionbpaTransactionType.TransferOut + @"</value>
                            <value>" + (int)BpaTransactionbpaTransactionType.Frozen + @"</value>
                            <value>" + (int)BpaTransactionbpaTransactionType.ReInstating + @"</value>
                          </condition>
                        </filter>
                      </entity>
                    </fetch>
                ";
            EntityCollection transactions = service.RetrieveMultiple(new FetchExpression(fetchxml));
            if (transactions != null && transactions.Entities.Count > 0)
            {
                transaction = transactions.Entities[0];
            }
            return transaction;
        }
        public static EntityCollection FetchAllTransactions(IOrganizationService service, Guid memberId,
            DateTime workMonth)
        {
            string fetchxml = @" 
                    <fetch>
                      <entity name='bpa_transaction' >
                        <attribute name='bpa_dollarhour' />
                        <attribute name='bpa_currenteligibilitycategory' />
                        <attribute name='bpa_hours' />
                        <attribute name='bpa_memberplanid' />
                        <attribute name='bpa_transactionid' />
                        <attribute name='bpa_transactiontype' />
                        <attribute name='bpa_wages' />
                        <attribute name='bpa_benefitmonth' />
                        <attribute name='bpa_transactiondate' />
                        <attribute name='bpa_dollarhour' />
                        <attribute name='bpa_reciprocaltransfer' />
                        <filter type='and' >
                          <condition attribute='statecode' operator='eq' value='0' />
                          <condition attribute='bpa_memberplanid' operator='eq' value='" + memberId + @"' />
                          <condition attribute='bpa_transactiondate' operator='ge' value='" +
                              workMonth.ToShortDateString() + @"' />
                          <condition attribute='bpa_transactiondate' operator='le' value='" +
                              workMonth.AddMonths(1).AddDays(-1).ToShortDateString() + @"' />
                          <condition attribute='bpa_useforcalculation' operator='eq' value='1' />
                        </filter>
                        <order attribute='bpa_transactiontype' />
                        <link-entity name='bpa_submission' from='bpa_submissionid' to='bpa_submission' link-type='outer'  >
                          <attribute name='bpa_accountagreementid' />
                          <attribute name='bpa_submissionid' />
                        </link-entity>
                      </entity>
                    </fetch>
                ";

            //<condition attribute='bpa_transactiontype' operator='not-null' />

            EntityCollection transactions = service.RetrieveMultiple(new FetchExpression(fetchxml));

            return transactions;
        }
        public static EntityCollection FetchAllFutureTransactions(IOrganizationService service, Guid MemberId, DateTime WorkMonth)
        {
            string fetchxml = $@" 
                    <fetch>
                      <entity name='bpa_transaction' >
                        <attribute name='bpa_dollarhour' />
                        <attribute name='bpa_currenteligibilitycategory' />
                        <attribute name='bpa_hours' />
                        <attribute name='bpa_memberplanid' />
                        <attribute name='bpa_transactionid' />
                        <attribute name='bpa_transactiontype' />
                        <attribute name='bpa_wages' />
                        <attribute name='bpa_benefitmonth' />
                        <attribute name='bpa_transactiondate' />
                        <attribute name='bpa_dollarhour' />
                        <attribute name='bpa_reciprocaltransfer' />
                        <filter type='and' >
                          <condition attribute='statecode' operator='eq' value='0' />
                          <condition attribute='bpa_memberplanid' operator='eq' value='{MemberId.ToString()}' />
                          <condition attribute='bpa_transactiondate' operator='ge' value='{ WorkMonth.ToShortDateString()}' />
                          
                        </filter>
                        <order attribute='bpa_transactiontype' />
                      </entity>
                    </fetch>
                ";
            //<condition attribute='bpa_transactiontype' operator='not-null' />

            EntityCollection transactions = service.RetrieveMultiple(new FetchExpression(fetchxml));

            return transactions;
        }
        public static void SetTransactionStatus(IOrganizationService service, Guid tranasactionId, bool isActive)
        {
            SetStateRequest state = new SetStateRequest();
            if (isActive)
            {
                state.State = new OptionSetValue(0);
                state.Status = new OptionSetValue(1);
            }
            else
            {
                state.State = new OptionSetValue(1);
                state.Status = new OptionSetValue(2);
            }
            state.EntityMoniker = new EntityReference(PluginHelper.BpaTransaction, tranasactionId);

            // Execute the Request
            SetStateResponse stateSet = (SetStateResponse)service.Execute(state);
        }

        public static Guid CreateUnderReviewTransaction(IOrganizationService service, Guid memberId,
            EntityReference eligibilityCategory, DateTime workMonth, DateTime benefitMonth, int transactionCategory,
            int transactionType, Guid defaultFundId, EntityReference lastAgreement)
        {
            Guid transId = Guid.Empty;
            Dictionary<string, decimal> endingBalances = FetchEndingBalance(service, memberId, benefitMonth, defaultFundId);

            Entity underReviewTransaction = new Entity(PluginHelper.BpaTransaction);
            underReviewTransaction["bpa_memberplanid"] = new EntityReference(PluginHelper.BpaMemberplan, memberId);
            underReviewTransaction["bpa_transactiondate"] = workMonth;
            underReviewTransaction["bpa_benefitmonth"] = benefitMonth;
            underReviewTransaction["bpa_transactioncategory"] = new OptionSetValue(transactionCategory);
            underReviewTransaction["bpa_transactiontype"] = new OptionSetValue(transactionType);

            //UnderReviewTransaction["bpa_endinghourbalance"] = EndingHours;
            //UnderReviewTransaction["bpa_endingdollarbalance"] = EndingDollar;

            if (endingBalances.ContainsKey("hours"))
                underReviewTransaction["bpa_endinghourbalance"] = endingBalances["hours"];
            if (endingBalances.ContainsKey("dollarhours"))
                underReviewTransaction["bpa_endingdollarbalance"] = endingBalances["dollarhours"];
            if (endingBalances.ContainsKey("selfpaydollar"))
                underReviewTransaction["bpa_endingselfpaybalance"] = new Money(endingBalances["selfpaydollar"]);

            if (endingBalances.ContainsKey("secondarydollar"))
                underReviewTransaction["bpa_secondarydollarbalance"] = new Money(endingBalances["secondarydollar"]);

            if (eligibilityCategory != null)
                underReviewTransaction["bpa_currenteligibilitycategory"] = eligibilityCategory;
            underReviewTransaction["bpa_useforcalculation"] = true;
            underReviewTransaction["bpa_name"] = Guid.NewGuid().ToString();

            if (lastAgreement != null)
                underReviewTransaction["bpa_subsectorid"] = lastAgreement;
            transId = service.Create(underReviewTransaction);

            return transId;
        }

        public static decimal GetSumDollarHour(IOrganizationService service, Guid employeeId, DateTime workmonth,
            Guid defaultFundId)
        {
            decimal sumDollarHour = 0;
            string fetchxml = @"
                    <fetch aggregate='true' >
                      <entity name='bpa_transaction' >
                        <attribute name='bpa_dollarhour' alias='bpa_dollarhour' aggregate='sum' />
                        <filter>
                          <condition attribute='bpa_memberplanid' operator='eq' value='" + employeeId + @"' />
                          <condition attribute='statecode' operator='eq' value='0' />
                          <condition attribute='bpa_transactiondate' operator='le' value='" + workmonth.AddMonths(1).AddDays(-1).ToShortDateString() + @"' />
                          <condition attribute='bpa_useforcalculation' operator='eq' value='1' />
                          <condition attribute='bpa_transactiontype' operator='not-in' >
                                <value>" + ((int)BpaTransactionbpaTransactionType.SelfPay).ToString() + @"</value>
                                <value>" + ((int)BpaTransactionbpaTransactionType.SelfPayBankAdjustment).ToString() + @"</value>
                                <value>" + ((int)BpaTransactionbpaTransactionType.SelfPayDeposit).ToString() + @"</value>
                                <value>" + ((int)BpaTransactionbpaTransactionType.SelfPayDraw).ToString() + @"</value>
                                <value>" + ((int)BpaTransactionbpaTransactionType.SelfPayInBenefit).ToString() + @"</value>
                                <value>" + ((int)BpaTransactionbpaTransactionType.SelfPayMaxConsecutiveReach).ToString() + @"</value>
                                <value>" + ((int)BpaTransactionbpaTransactionType.SelfPayRefund).ToString() + @"</value>
                          </condition>
                        </filter>
                      </entity>
                    </fetch>
                ";

            //<condition attribute='bpa_fund' operator='eq' value='" + DefaultFundId.ToString() + @"' />
            //<condition attribute='bpa_transactiontype' operator='not-null' />

            EntityCollection entityCollection = service.RetrieveMultiple(new FetchExpression(fetchxml));
            if (entityCollection != null && entityCollection.Entities.Count > 0)
            {
                Entity sum = entityCollection.Entities[0];

                //if (sum.Attributes.Contains("bpa_dollarhour"))
                if (sum.Attributes.Contains("bpa_dollarhour") &&
                    ((AliasedValue)sum.Attributes["bpa_dollarhour"]).Value != null)
                    sumDollarHour = ((Money)sum.GetAttributeValue<AliasedValue>("bpa_dollarhour").Value).Value;
            }
            return sumDollarHour;
        }

        public static Dictionary<string, decimal> FetchEndingBalance(IOrganizationService service, Guid contactId,
            DateTime benefitMonth, Guid defaultFundId)
        {
            Dictionary<string, decimal> endingbalance = new Dictionary<string, decimal>();
            
            string fetchXml = $@"
                <fetch aggregate='true' >
                    <entity name='bpa_transaction' >
                    <attribute name='bpa_hours' alias='bpa_hours' aggregate='sum' />
	                <attribute name='bpa_dollarhour' alias='bpa_dollarhour' aggregate='sum' />
                    <filter type='and' >
                        <condition attribute='bpa_memberplanid' operator='eq' value='{contactId}' />
                        <condition attribute='statecode' operator='eq' value='0' />
                        <condition attribute='bpa_transactiondate' operator='le' value='{benefitMonth.AddMonths(1).AddDays(-1).ToShortDateString() }' />
                        <condition attribute='bpa_useforcalculation' operator='eq' value='1' />
                        <condition attribute='bpa_transactiontype' operator='not-in' >
                            <value>{((int)BpaTransactionbpaTransactionType.SelfPay).ToString()}</value>
                            <value>{((int)BpaTransactionbpaTransactionType.SelfPayBankAdjustment).ToString()}</value>
                            <value>{((int)BpaTransactionbpaTransactionType.SelfPayDeposit).ToString()}</value>
                            <value>{((int)BpaTransactionbpaTransactionType.SelfPayDraw).ToString()}</value>
                            <value>{((int)BpaTransactionbpaTransactionType.SelfPayInBenefit).ToString()}</value>
                            <value>{((int)BpaTransactionbpaTransactionType.SelfPayMaxConsecutiveReach).ToString()}</value>
                            <value>{((int)BpaTransactionbpaTransactionType.SelfPayRefund).ToString()}</value>
                        </condition>
                    </filter>
                    </entity>
                </fetch>            ";
            EntityCollection sums = service.RetrieveMultiple(new FetchExpression(fetchXml));

            if (sums != null && sums.Entities.Count > 0)
            {
                Entity sum = sums.Entities[0];
                decimal hours = 0, dollarhours = 0;

                //if (sum.Attributes.Contains("bpa_hours"))
                if (sum.Attributes.Contains("bpa_hours") && ((AliasedValue)sum.Attributes["bpa_hours"]).Value != null)
                    hours = (decimal)((AliasedValue)sum.Attributes["bpa_hours"]).Value;
                endingbalance.Add("hours", hours);

                //if (sum.Attributes.Contains("bpa_dollarhour"))
                if (sum.Attributes.Contains("bpa_dollarhour") &&
                    ((AliasedValue)sum.Attributes["bpa_dollarhour"]).Value != null)
                    dollarhours = ((Money)((AliasedValue)sum.Attributes["bpa_dollarhour"]).Value).Value;
                endingbalance.Add("dollarhours", dollarhours);
            }
            
            //"selfpay")
            fetchXml = $@"
                <fetch aggregate='true' >
                    <entity name='bpa_transaction' >
	                <attribute name='bpa_dollarhour' alias='bpa_dollarhour' aggregate='sum' />
                    <filter type='and' >
                        <condition attribute='bpa_memberplanid' operator='eq' value='{contactId}' />
                        <condition attribute='statecode' operator='eq' value='0' />
                        <condition attribute='bpa_transactiondate' operator='le' value='{benefitMonth.AddMonths(1).AddDays(-1).ToShortDateString()}' />
                        <condition attribute='bpa_useforcalculation' operator='eq' value='1' />
                        <condition attribute='bpa_transactiontype' operator='in' >
                        <value>{((int)BpaTransactionbpaTransactionType.SelfPay).ToString()}</value>
                        <value>{((int)BpaTransactionbpaTransactionType.SelfPayBankAdjustment).ToString()}</value>
                        <value>{((int)BpaTransactionbpaTransactionType.SelfPayDeposit).ToString()}</value>
                        <value>{((int)BpaTransactionbpaTransactionType.SelfPayDraw).ToString()}</value>
                        <value>{((int)BpaTransactionbpaTransactionType.SelfPayInBenefit).ToString()}</value>
                        <value>{((int)BpaTransactionbpaTransactionType.SelfPayMaxConsecutiveReach).ToString()}</value>
                        <value>{((int)BpaTransactionbpaTransactionType.SelfPayRefund).ToString()}</value>
                        </condition>
                    </filter>
                    </entity>
                </fetch>            ";
            sums = service.RetrieveMultiple(new FetchExpression(fetchXml));

            if (sums != null && sums.Entities.Count > 0)
            {
                Entity sum = sums.Entities[0];
                decimal dollarhours = 0;

                //if (sum.Attributes.Contains("bpa_dollarhour"))
                if (sum.Attributes.Contains("bpa_dollarhour") &&
                    ((AliasedValue)sum.Attributes["bpa_dollarhour"]).Value != null)
                    dollarhours = ((Money)((AliasedValue)sum.Attributes["bpa_dollarhour"]).Value).Value;
                endingbalance.Add("selfpaydollar", dollarhours);
            }

            // secondary dollar bank
            fetchXml = $@"
                <fetch aggregate='true' >
                    <entity name='bpa_transaction' >
	                <attribute name='bpa_dollarhour' alias='bpa_dollarhour' aggregate='sum' />
                    <filter type='and' >
                        <condition attribute='bpa_memberplanid' operator='eq' value='{contactId}' />
                        <condition attribute='statecode' operator='eq' value='0' />
                        <condition attribute='bpa_transactiondate' operator='le' value='{benefitMonth.AddMonths(1).AddDays(-1).ToShortDateString()}' />
                        <condition attribute='bpa_transactiontype' operator='in' >
                        <value>{((int)BpaTransactionbpaTransactionType.SecondaryDollarBankAdjustment).ToString()}</value>
                        <value>{((int)BpaTransactionbpaTransactionType.SecondaryDollarBankDraw).ToString()}</value>
                        </condition>
                    </filter>
                    </entity>
                </fetch>            ";
            sums = service.RetrieveMultiple(new FetchExpression(fetchXml));

            if (sums != null && sums.Entities.Count > 0)
            {
                Entity sum = sums.Entities[0];
                decimal dollarhours = 0;

                //if (sum.Attributes.Contains("bpa_dollarhour"))
                if (sum.Attributes.Contains("bpa_dollarhour") &&
                    ((AliasedValue)sum.Attributes["bpa_dollarhour"]).Value != null)
                    dollarhours = ((Money)((AliasedValue)sum.Attributes["bpa_dollarhour"]).Value).Value;
                endingbalance.Add("secondarydollar", dollarhours);
            }
            return endingbalance;
        }


        public static Dictionary<string, decimal> FetchEndingBalanceForInactive(IOrganizationService service,
            Guid contactId, DateTime benefitMonth, Guid defaultFundId)
        {
            Dictionary<string, decimal> endingbalance = new Dictionary<string, decimal>();

            string fetchXml = $@"
                <fetch aggregate='true' >
                  <entity name='bpa_transaction' >
                    <attribute name='bpa_hours' alias='bpa_hours' aggregate='sum' />
	                <attribute name='bpa_dollarhour' alias='bpa_dollarhour' aggregate='sum' />
                    <filter type='and' >
                      <condition attribute='bpa_memberplanid' operator='eq' value='{contactId}' />
                      <condition attribute='statecode' operator='eq' value='0' />
                      <condition attribute='bpa_transactiondate' operator='lt' value='{benefitMonth.ToShortDateString()}' />
                      <condition attribute='bpa_useforcalculation' operator='eq' value='1' />
                      <condition attribute='bpa_transactiontype' operator='not-in' >
                        <value>{((int)BpaTransactionbpaTransactionType.SelfPay).ToString()}</value>
                        <value>{((int)BpaTransactionbpaTransactionType.SelfPayBankAdjustment).ToString()}</value>
                        <value>{((int)BpaTransactionbpaTransactionType.SelfPayDeposit).ToString()}</value>
                        <value>{((int)BpaTransactionbpaTransactionType.SelfPayDraw).ToString()}</value>
                        <value>{((int)BpaTransactionbpaTransactionType.SelfPayInBenefit).ToString()}</value>
                        <value>{((int)BpaTransactionbpaTransactionType.SelfPayMaxConsecutiveReach).ToString()}</value>
                        <value>{((int)BpaTransactionbpaTransactionType.SelfPayRefund).ToString()}</value>
                      </condition>
                    </filter>
                  </entity>
                </fetch>            ";
            EntityCollection sums = service.RetrieveMultiple(new FetchExpression(fetchXml));

            if (sums != null && sums.Entities.Count > 0)
            {
                Entity sum = sums.Entities[0];
                decimal hours = 0, dollarhours = 0;

                //if (sum.Attributes.Contains("bpa_hours"))
                if (sum.Attributes.Contains("bpa_hours") && ((AliasedValue)sum.Attributes["bpa_hours"]).Value != null)
                    hours = (decimal)((AliasedValue)sum.Attributes["bpa_hours"]).Value;
                endingbalance.Add("hours", hours);

                //if (sum.Attributes.Contains("bpa_dollarhour"))
                if (sum.Attributes.Contains("bpa_dollarhour") &&
                    ((AliasedValue)sum.Attributes["bpa_dollarhour"]).Value != null)
                    dollarhours = ((Money)((AliasedValue)sum.Attributes["bpa_dollarhour"]).Value).Value;
                endingbalance.Add("dollarhours", dollarhours);
            }


            //SELF PAY
            fetchXml = @"
                <fetch aggregate='true' >
                    <entity name='bpa_transaction' >
	                <attribute name='bpa_dollarhour' alias='bpa_dollarhour' aggregate='sum' />
                    <filter type='and' >
                        <condition attribute='bpa_memberplanid' operator='eq' value='" + contactId + @"' />
                        <condition attribute='statecode' operator='eq' value='0' />
                        <condition attribute='bpa_transactiondate' operator='le' value='" + benefitMonth.ToShortDateString() + @"' />
                        <condition attribute='bpa_useforcalculation' operator='eq' value='1' />
                        <condition attribute='bpa_transactiontype' operator='in' >
                            <value>" + ((int)BpaTransactionbpaTransactionType.SelfPay).ToString() + @"</value>
                            <value>" + ((int)BpaTransactionbpaTransactionType.SelfPayBankAdjustment).ToString() + @"</value>
                            <value>" + ((int)BpaTransactionbpaTransactionType.SelfPayDeposit).ToString() + @"</value>
                            <value>" + ((int)BpaTransactionbpaTransactionType.SelfPayDraw).ToString() + @"</value>
                            <value>" + ((int)BpaTransactionbpaTransactionType.SelfPayInBenefit).ToString() + @"</value>
                            <value>" + ((int)BpaTransactionbpaTransactionType.SelfPayMaxConsecutiveReach).ToString() + @"</value>
                            <value>" + ((int)BpaTransactionbpaTransactionType.SelfPayRefund).ToString() + @"</value>
                        </condition>
                    </filter>
                    </entity>
                </fetch>            ";
            EntityCollection Sums = service.RetrieveMultiple(new FetchExpression(fetchXml));

            if (Sums != null && Sums.Entities.Count > 0)
            {
                Entity sum = Sums.Entities[0];
                decimal dollarhours = 0;

                //if (sum.Attributes.Contains("bpa_dollarhour"))
                if (sum.Attributes.Contains("bpa_dollarhour") &&
                    ((AliasedValue)sum.Attributes["bpa_dollarhour"]).Value != null)
                    dollarhours = ((Money)((AliasedValue)sum.Attributes["bpa_dollarhour"]).Value).Value;

                if (dollarhours != 0)
                    endingbalance.Add("selfpaydollar", dollarhours);
            }

            // secondary dollar bank
            fetchXml = $@"
                <fetch aggregate='true' >
                    <entity name='bpa_transaction' >
	                <attribute name='bpa_dollarhour' alias='bpa_dollarhour' aggregate='sum' />
                    <filter type='and' >
                        <condition attribute='bpa_memberplanid' operator='eq' value='{contactId}' />
                        <condition attribute='statecode' operator='eq' value='0' />
                        <condition attribute='bpa_transactiondate' operator='le' value='{benefitMonth.AddMonths(1).AddDays(-1).ToShortDateString()}' />
                        <condition attribute='bpa_transactiontype' operator='in' >
                        <value>{((int)BpaTransactionbpaTransactionType.SecondaryDollarBankAdjustment).ToString()}</value>
                        <value>{((int)BpaTransactionbpaTransactionType.SecondaryDollarBankDraw).ToString()}</value>
                        </condition>
                    </filter>
                    </entity>
                </fetch>            ";
            sums = service.RetrieveMultiple(new FetchExpression(fetchXml));

            if (sums != null && sums.Entities.Count > 0)
            {
                Entity sum = sums.Entities[0];
                decimal dollarhours = 0;

                //if (sum.Attributes.Contains("bpa_dollarhour"))
                if (sum.Attributes.Contains("bpa_dollarhour") &&
                    ((AliasedValue)sum.Attributes["bpa_dollarhour"]).Value != null)
                    dollarhours = ((Money)((AliasedValue)sum.Attributes["bpa_dollarhour"]).Value).Value;
                endingbalance.Add("secondarydollar", dollarhours);
            }
            return endingbalance;
        }

        public static Guid CreateTransaction(IOrganizationService service, Guid memberId, DateTime workMonth,
            DateTime benefitMonth, EntityReference fund, int transactionCategory, int transactionType,
            decimal dollarHours, bool isEligibilityTransaction, EntityReference eligibilityCategory, Guid defaultFundId, EntityReference lastAgreement)
        {
            Guid transactionId = Guid.Empty;
            Dictionary<string, decimal> endingBalances = FetchEndingBalance(service, memberId, workMonth, defaultFundId);

            Entity cTransaction = new Entity(PluginHelper.BpaTransaction);
            cTransaction["bpa_memberplanid"] = new EntityReference(PluginHelper.BpaMemberplan, memberId);
            cTransaction["bpa_transactiondate"] = workMonth;
            cTransaction["bpa_benefitmonth"] = benefitMonth;
            if ((fund != null) && (fund.Id != Guid.Empty))
                cTransaction[PluginHelper.BpaFund] = fund;
            cTransaction["bpa_transactioncategory"] = new OptionSetValue(transactionCategory);
            cTransaction["bpa_transactiontype"] = new OptionSetValue(transactionType);
            if (dollarHours != 0)
                cTransaction["bpa_dollarhour"] = new Money(dollarHours);
            if (isEligibilityTransaction)
            {
                if (endingBalances.ContainsKey("hours"))
                    cTransaction["bpa_endinghourbalance"] = endingBalances["hours"];
                if (endingBalances.ContainsKey("dollarhours"))
                    cTransaction["bpa_endingdollarbalance"] = endingBalances["dollarhours"];
                if (endingBalances.ContainsKey("selfpaydollar"))
                    cTransaction["bpa_endingselfpaybalance"] = new Money(endingBalances["selfpaydollar"]);
                if (endingBalances.ContainsKey("secondarydollar"))
                    cTransaction["bpa_secondarydollarbalance"] = new Money(endingBalances["secondarydollar"]);

            }
            if (eligibilityCategory != null)
                cTransaction["bpa_currenteligibilitycategory"] = eligibilityCategory;
            if( (transactionType == (int)BpaTransactionbpaTransactionType.SecondaryDollarBankAdjustment) || 
                (transactionType == (int)BpaTransactionbpaTransactionType.SecondaryDollarBankDraw))
                cTransaction["bpa_useforcalculation"] = false;
            else
                cTransaction["bpa_useforcalculation"] = true;

            cTransaction["bpa_name"] = Guid.NewGuid().ToString();
            if (lastAgreement != null)
                cTransaction["bpa_subsectorid"] = lastAgreement;
            transactionId = service.Create(cTransaction);
            return transactionId;
        }

        public static EntityCollection FetchAllEligibilityDrawTransactions(IOrganizationService service, Guid memberId,
            DateTime workMonth, int transactionType)
        {
            string fetchXml = @"
                    <fetch>
                      <entity name='bpa_transaction' >
                        <attribute name='bpa_transactionid' />
                        <filter type='and' >
                          <condition attribute='bpa_memberplanid' operator='eq' value='" + memberId + @"' />
                          <condition attribute='statecode' operator='eq' value='0' />
                          <condition attribute='bpa_transactiondate' operator='not-null' />
                          <condition attribute='bpa_transactiondate' operator='ge' value='" + workMonth.ToShortDateString() + @"' />
                          <condition attribute='bpa_transactiondate' operator='le' value='" + workMonth.AddMonths(1).AddDays(-1).ToShortDateString() + @"' />
                          <condition attribute='bpa_transactiontype' operator='eq' value='" + transactionType + @"' />
                          <condition attribute='bpa_useforcalculation' operator='eq' value='1' />
                        </filter>
                        <order attribute='createdon' descending='true' />
                      </entity>
                    </fetch>            ";
            EntityCollection transactions = service.RetrieveMultiple(new FetchExpression(fetchXml));
            return transactions;
        }
        
        public static Entity FetchLastEligiblityMonthExceptSelfPay(IOrganizationService service, ITracingService tracingService, DateTime WorkMonth, DateTime LastECMonth, Guid MemberId)
        {
            Entity entity = null;
            string fetchxml = @"
                    <fetch top='1' >
                      <entity name='bpa_transaction' >
                        <attribute name='bpa_transactiondate' />
                        <attribute name='bpa_transactionid' />
                        <filter type='and' >
                          <condition attribute='bpa_memberplanid' operator='eq' value='" + MemberId.ToString() + @"' />
                          <condition attribute='bpa_transactiondate' operator='gt' value='" + LastECMonth.ToShortDateString() + @"' />
                          <condition attribute='bpa_transactiondate' operator='lt' value='" + WorkMonth.ToShortDateString() + @"' />
                          <condition attribute='statecode' operator='eq' value='0' />
                          <condition attribute='bpa_transactiontype' operator='in' >
                            <value>" + ((int)BpaTransactionbpaTransactionType.InBenefit).ToString() + @"</value>
                            <value>" + ((int)BpaTransactionbpaTransactionType.NotInBenefit).ToString() + @"</value>
                            <value>" + ((int)BpaTransactionbpaTransactionType.NewMember).ToString() + @"</value>
                            <value>" + ((int)BpaTransactionbpaTransactionType.FundAssistedInBenefit).ToString() + @"</value>
                            <value>" + ((int)BpaTransactionbpaTransactionType.UnderReview).ToString() + @"</value>
                            <value>" + ((int)BpaTransactionbpaTransactionType.ReInstating).ToString() + @"</value>
                          </condition>
                        </filter>
                        <order attribute='bpa_transactiondate' descending='true' />
                      </entity>
                    </fetch>
                ";
            EntityCollection transactions = service.RetrieveMultiple(new FetchExpression(fetchxml));

            if (transactions != null && transactions.Entities.Count > 0)
            {
                entity = transactions.Entities[0];
            }
            return entity;
        }

        public static Entity FetchLastEligiblityMonthExceptSelfPay1(IOrganizationService service, ITracingService tracingService, DateTime startWorkMonth, Guid MemberId)
        {
            Entity entity = null;
            string fetchxml = @"
                    <fetch top='1' >
                      <entity name='bpa_transaction' >
                        <attribute name='bpa_transactiondate' />
                        <attribute name='bpa_transactionid' />
                        <filter type='and' >
                          <condition attribute='bpa_memberplanid' operator='eq' value='" + MemberId.ToString() + @"' />
                          <condition attribute='bpa_transactiondate' operator='lt' value='" + startWorkMonth.ToShortDateString() + @"' />
                          <condition attribute='statecode' operator='eq' value='0' />
                          <condition attribute='bpa_transactiontype' operator='in' >
                            <value>" + ((int)BpaTransactionbpaTransactionType.InBenefit).ToString() + @"</value>
                            <value>" + ((int)BpaTransactionbpaTransactionType.NotInBenefit).ToString() + @"</value>
                            <value>" + ((int)BpaTransactionbpaTransactionType.NewMember).ToString() + @"</value>
                            <value>" + ((int)BpaTransactionbpaTransactionType.FundAssistedInBenefit).ToString() + @"</value>
                            <value>" + ((int)BpaTransactionbpaTransactionType.UnderReview).ToString() + @"</value>
                            <value>" + ((int)BpaTransactionbpaTransactionType.ReInstating).ToString() + @"</value>
                          </condition>
                        </filter>
                        <order attribute='bpa_transactiondate' descending='true' />
                      </entity>
                    </fetch>
                ";
            EntityCollection transactions = service.RetrieveMultiple(new FetchExpression(fetchxml));

            if (transactions != null && transactions.Entities.Count > 0)
            {
                entity = transactions.Entities[0];
            }
            return entity;
        }

        public static void UpdateElibilityCategory(IOrganizationService service, Guid transactionId,
            EntityReference usingEligibilityCategoty)
        {
            Entity eligiblityCategory = service.Retrieve("bpa_eligibilitycategory", usingEligibilityCategoty.Id,
                new ColumnSet("bpa_name"));
            if (eligiblityCategory != null)
            {
                Entity mbtransaction = new Entity(PluginHelper.BpaTransaction);
                //mbtransaction["bpa_eligibilitycategory"] = eligiblityCategory.GetAttributeValue<string>("bpa_name");
                mbtransaction["bpa_currenteligibilitycategory"] = usingEligibilityCategoty;
                mbtransaction.Id = transactionId;
                service.Update(mbtransaction);
            }
        }

        public static Guid CreateFundAssistedTransactions(IOrganizationService service, Guid memberId,
            DateTime workMonth, DateTime benefitMonth, Money dollarHours, decimal? hour, int transactionCategory, int transactionType,
            EntityReference fund, EntityReference eligibilityCategory, Guid defaultFundId, EntityReference lastAgreement)
        {
            //Fund Assisted
            Entity fundAssisted = new Entity(PluginHelper.BpaTransaction);
            fundAssisted["bpa_memberplanid"] = new EntityReference(PluginHelper.BpaMemberplan, memberId);
            fundAssisted["bpa_transactiondate"] = workMonth;
            fundAssisted["bpa_benefitmonth"] = benefitMonth;

            if (hour != null)
                fundAssisted["bpa_hours"] = hour.Value;

            if (dollarHours != null)
                fundAssisted["bpa_dollarhour"] = dollarHours;
            fundAssisted["bpa_transactioncategory"] = new OptionSetValue(transactionCategory);
            fundAssisted["bpa_transactiontype"] = new OptionSetValue(transactionType);
            
            if ((fund != null) && (fund.Id != Guid.Empty))
                fundAssisted[PluginHelper.BpaFund] = fund;

            if (eligibilityCategory != null)
            {
                fundAssisted["bpa_currenteligibilitycategory"] = eligibilityCategory;
            }

            if (transactionType == (int)BpaTransactionbpaTransactionType.FundAssistedInBenefit)
            {
                Dictionary<string, decimal> endingBalances = FetchEndingBalance(service, memberId, workMonth, defaultFundId);
                if (endingBalances.ContainsKey("hours"))
                    fundAssisted["bpa_endinghourbalance"] = endingBalances["hours"];
                if (endingBalances.ContainsKey("dollarhours"))
                    fundAssisted["bpa_endingdollarbalance"] = endingBalances["dollarhours"];
                if (endingBalances.ContainsKey("selfpaydollar"))
                    fundAssisted["bpa_endingselfpaybalance"] = new Money(endingBalances["selfpaydollar"]);

                if (endingBalances.ContainsKey("secondarydollar"))
                    fundAssisted["bpa_secondarydollarbalance"] = new Money(endingBalances["secondarydollar"]);

            }
            fundAssisted["bpa_useforcalculation"] = true;
            fundAssisted["bpa_name"] = Guid.NewGuid().ToString();
            if (lastAgreement != null)
                fundAssisted["bpa_subsectorid"] = lastAgreement;

            return service.Create(fundAssisted);
        }
        
        public static decimal GetSumHours(IOrganizationService service, Guid employeeId, DateTime workmonth,
            Guid defaultFundId)
        {
            decimal sumHours = 0;
            string fetchxml = @"
                    <fetch aggregate='true' >
                      <entity name='bpa_transaction' >
                        <attribute name='bpa_hours' alias='bpa_hours' aggregate='sum' />
                        <filter>
                          <condition attribute='bpa_memberplanid' operator='eq' value='" + employeeId + @"' />
                          <condition attribute='statecode' operator='eq' value='0' />
                          <condition attribute='bpa_transactiondate' operator='le' value='" +
                              workmonth.AddMonths(1).AddDays(-1).ToShortDateString() + @"' />
                          <condition attribute='bpa_useforcalculation' operator='eq' value='1' />

                        </filter>
                      </entity>
                    </fetch>
                ";
            EntityCollection entityCollection = service.RetrieveMultiple(new FetchExpression(fetchxml));
            if (entityCollection != null && entityCollection.Entities.Count > 0)
            {
                Entity sum = entityCollection.Entities[0];
                if (sum.Attributes.Contains("bpa_hours") &&
                    sum.GetAttributeValue<AliasedValue>("bpa_hours").Value != null)
                    sumHours = (decimal)sum.GetAttributeValue<AliasedValue>("bpa_hours").Value;
            }

            return sumHours;
        }
        
        public static Guid CreateDrawTransaction_Hour(IOrganizationService service, Guid memberId, DateTime workMonth,
            DateTime benefitMonth, int transactionCategory, int transactionType, EntityReference fund, decimal hours,
            EntityReference currentEligibilityCategory, EntityReference lastAgreement)
        {
            Guid transId = Guid.Empty;

            Entity transaction = new Entity(PluginHelper.BpaTransaction);
            transaction["bpa_memberplanid"] = new EntityReference(PluginHelper.BpaMemberplan, memberId);
            transaction["bpa_transactiondate"] = workMonth;
            transaction["bpa_benefitmonth"] = benefitMonth;
            transaction["bpa_transactioncategory"] = new OptionSetValue(transactionCategory);
            transaction["bpa_transactiontype"] = new OptionSetValue(transactionType);

            //if (!string.IsNullOrEmpty(FundName) && FundName != Guid.Empty.ToString())
            //    transaction["bpa_fund"] = new EntityReference("bpa_fund", GetFundIdbyName(service, FundName));
            if ((fund != null) && (fund.Id != Guid.Empty))
                transaction["bpa_fund"] = fund;
            if (hours != 0)
                transaction["bpa_hours"] = hours;
            if (currentEligibilityCategory != null)
            {
                //transaction["bpa_eligibilitycategory"] = currentEligibilityCategory.Name;
                transaction["bpa_currenteligibilitycategory"] = currentEligibilityCategory;
            }
            transaction["bpa_useforcalculation"] = true;
            transaction["bpa_name"] = Guid.NewGuid().ToString();

            if (lastAgreement != null)
                transaction["bpa_subsectorid"] = lastAgreement;

            transId = service.Create(transaction);
            return transId;
        }
        
        public static Guid CreateTransaction(IOrganizationService service, Guid memberId, DateTime workMonth,
            DateTime benefitMonth, string fundName, int transactionCategory, int transactionType, decimal dollarHours,
            bool isEligibilityTransaction, EntityReference eligibilityCategory, bool isReservTransferCreate,
            Guid defaultFundId, EntityReference lastAgreement)
        {
            Guid transactionId = Guid.Empty;
            Dictionary<string, decimal> endingBalances = new Dictionary<string, decimal>();
            if (transactionType != (int)BpaTransactionbpaTransactionType.Inactive)
                endingBalances = FetchEndingBalance(service, memberId, workMonth, defaultFundId);
            else
                endingBalances = RecalculationTransactionHelper.FetchEndingBalanceForInactive(service, memberId, workMonth, defaultFundId);
            //EndingBalances = FetchEndingBalanceForInactive(service, MemberId, WorkMonth.AddMonths(-1), DefaultFundId);


            if (isReservTransferCreate)
            {
                Entity rTransaction = new Entity(PluginHelper.BpaTransaction);
                rTransaction["bpa_memberplanid"] = new EntityReference(PluginHelper.BpaMemberplan, memberId);
                rTransaction["bpa_transactiondate"] = workMonth;
                rTransaction["bpa_benefitmonth"] = benefitMonth;
                rTransaction["bpa_transactioncategory"] = new OptionSetValue(transactionCategory);
                rTransaction["bpa_transactiontype"] =
                    new OptionSetValue((int)BpaTransactionbpaTransactionType.ReserveTransfer);
                rTransaction["bpa_useforcalculation"] = true;
                if (defaultFundId != Guid.Empty)
                    rTransaction["bpa_fund"] = new EntityReference(PluginHelper.BpaFund, defaultFundId);
                if (endingBalances.ContainsKey("hours"))
                    rTransaction["bpa_hours"] = endingBalances["hours"]*-1;
                if (endingBalances.ContainsKey("dollarhours"))
                    rTransaction["bpa_dollarhour"] = new Money(endingBalances["dollarhours"]*-1);
                if (endingBalances.ContainsKey("selfpaydollar"))
                    rTransaction["bpa_endingselfpaybalance"] = new Money(endingBalances["selfpaydollar"]);

                if (endingBalances.ContainsKey("secondarydollar"))
                    rTransaction["bpa_secondarydollarbalance"] = new Money(endingBalances["secondarydollar"]);

                if (eligibilityCategory != null)
                    rTransaction["bpa_currenteligibilitycategory"] = eligibilityCategory;
                rTransaction["bpa_name"] = Guid.NewGuid().ToString();

                if (lastAgreement != null)
                    rTransaction["bpa_subsectorid"] = lastAgreement;
                service.Create(rTransaction);
            }

            if (transactionType != (int)BpaTransactionbpaTransactionType.Inactive)
                endingBalances = FetchEndingBalance(service, memberId, workMonth, defaultFundId);
            else
                endingBalances = RecalculationTransactionHelper.FetchEndingBalanceForInactive(service, memberId, workMonth, defaultFundId);
            //EndingBalances = FetchEndingBalanceForInactive(service, MemberId, WorkMonth.AddMonths(-1), DefaultFundId);


            Entity cTransaction = new Entity(PluginHelper.BpaTransaction);
            cTransaction["bpa_memberplanid"] = new EntityReference(PluginHelper.BpaMemberplan, memberId);
            cTransaction["bpa_transactiondate"] = workMonth;
            cTransaction["bpa_benefitmonth"] = benefitMonth;

            //if (!string.IsNullOrEmpty(FundName))
            //    C_transaction["bpa_fund"] = new EntityReference("bpa_fund", GetFundIdbyName(service, FundName));
            if (defaultFundId != Guid.Empty)
                cTransaction["bpa_fund"] = new EntityReference(PluginHelper.BpaFund, defaultFundId);
            cTransaction["bpa_transactioncategory"] = new OptionSetValue(transactionCategory);
            cTransaction["bpa_transactiontype"] = new OptionSetValue(transactionType);
            if (dollarHours != 0)
                cTransaction["bpa_dollarhour"] = new Money(dollarHours);
            cTransaction["bpa_useforcalculation"] = true;

            if (isEligibilityTransaction)
            {
                if (endingBalances.ContainsKey("hours"))
                    cTransaction["bpa_endinghourbalance"] = endingBalances["hours"];
                if (endingBalances.ContainsKey("dollarhours"))
                    cTransaction["bpa_endingdollarbalance"] = endingBalances["dollarhours"];
                if (endingBalances.ContainsKey("selfpaydollar"))
                    cTransaction["bpa_endingselfpaybalance"] = new Money(endingBalances["selfpaydollar"]);
                if (endingBalances.ContainsKey("secondarydollar"))
                    cTransaction["bpa_secondarydollarbalance"] = new Money(endingBalances["secondarydollar"]);

                if (eligibilityCategory != null)
                    cTransaction["bpa_currenteligibilitycategory"] = eligibilityCategory;
            }
            cTransaction["bpa_name"] = Guid.NewGuid().ToString();

            if (lastAgreement != null)
                cTransaction["bpa_subsectorid"] = lastAgreement;
            transactionId = service.Create(cTransaction);

            return transactionId;
        }
        
        public static EntityCollection AlreadyHasInactiveAndReserveTransactions(IOrganizationService service,
            Guid memberId, DateTime workMonth)
        {
            EntityCollection transactions = null;
            string fetchxml = @"
                    <fetch count='50' >
                      <entity name='bpa_transaction' >
                        <attribute name='bpa_transactionid' />
                        <attribute name='bpa_transactiontype' />
                        <filter>
                          <condition attribute='bpa_memberplanid' operator='eq' value='" + memberId + @"' />
                          <condition attribute='statecode' operator='eq' value='0' />
                          <condition attribute='bpa_transactiondate' operator='eq' value='" +
                              workMonth.ToShortDateString() + @"' />
	                       <condition attribute='bpa_transactiontype' operator='in' >
		                    <value>922070027</value>
		                    <value>922070010</value>
	                      </condition>
                        </filter>
                      </entity>
                    </fetch>
                ";
            transactions = service.RetrieveMultiple(new FetchExpression(fetchxml));

            return transactions;
        }
        
        public static void DeleteTransactionNotInBenefit(IOrganizationService service, Guid memberId, DateTime workMonth)
        {
            string fetchXml = @"
                <fetch >
                  <entity name='bpa_transaction' >
                    <attribute name='bpa_name' />
                    <filter type='and' >
                      <condition attribute='bpa_memberplanid' operator='eq' value='" + memberId + @"' />
                      <condition attribute='statecode' operator='eq' value='0' />
                      <condition attribute='bpa_transactiondate' operator='eq' value='" + workMonth.ToShortDateString() +
                              @"' />
                      <condition attribute='bpa_transactiontype' operator='in' >
                        <value>" + (int)BpaTransactionbpaTransactionType.NotInBenefit + @"</value>
                      </condition>
                    </filter>
                  </entity>
                </fetch>            ";
            EntityCollection transactions = service.RetrieveMultiple(new FetchExpression(fetchXml));
            if (transactions != null && transactions.Entities.Count > 0)
            {
                foreach (Entity e in transactions.Entities)
                {
                    //Entity trans = new Entity(PluginHelper.BpaTransaction);
                    //trans.Id = e.Id;
                    //trans["bpa_deleteme"] = true;
                    //service.Update(trans);
                    TransactionHelper.DeleteTransactions(service, e.Id);

                }

                //service.Delete("bpa_transaction", e.Id);
            }
        }
        
        public static Entity FetchLastEligiblityContributionMonth(IOrganizationService service, DateTime workMonth,
            Guid memberId)
        {
            Entity entity = null;
            string fetchxml = @"
                    <fetch top='1' >
                      <entity name='bpa_transaction' >
                        <attribute name='bpa_transactiondate' />
                        <attribute name='bpa_transactionid' />
                        <filter type='and' >
                          <condition attribute='bpa_memberplanid' operator='eq' value='" + memberId + @"' />
	                      <condition attribute='bpa_transactiondate' operator='lt' value='" + workMonth.ToShortDateString() +
                              @"' />
                          <condition attribute='statecode' operator='eq' value='0' />
                          <condition attribute='bpa_transactiontype' operator='in' >
                            <value>" + (int)BpaTransactionbpaTransactionType.EmployerContribution + @"</value>
                          </condition>
                        </filter>
                        <order attribute='bpa_transactiondate' descending='true' />
                      </entity>
                    </fetch>
                ";
            EntityCollection transactions = service.RetrieveMultiple(new FetchExpression(fetchxml));

            if (transactions != null && transactions.Entities.Count > 0)
            {
                entity = transactions.Entities[0];
            }
            return entity;
        }
        
        public static Entity FetchLastBenefitMonth(IOrganizationService service, DateTime workMonth, Guid memberId)
        {
            Entity entity = null;
            string fetchxml = @"
                    <fetch top='1' >
                      <entity name='bpa_transaction' >
                        <attribute name='bpa_transactiondate' />
                        <attribute name='bpa_transactionid' />
                        <filter type='and' >
                          <condition attribute='bpa_memberplanid' operator='eq' value='" + memberId + @"' />
	                      <condition attribute='bpa_transactiondate' operator='lt' value='" + workMonth.ToShortDateString() +
                              @"' />
                          <condition attribute='statecode' operator='eq' value='0' />
                          <condition attribute='bpa_transactiontype' operator='in' >
                            <value>" + (int)BpaTransactionbpaTransactionType.InBenefit + @"</value>
                            <value>" + (int)BpaTransactionbpaTransactionType.SelfPayInBenefit + @"</value>
                            <value>" + (int)BpaTransactionbpaTransactionType.FundAssistedInBenefit + @"</value>
                          </condition>
                        </filter>
                        <order attribute='bpa_transactiondate' descending='true' />
                      </entity>
                    </fetch>
                ";
            EntityCollection transactions = service.RetrieveMultiple(new FetchExpression(fetchxml));

            if (transactions != null && transactions.Entities.Count > 0)
            {
                entity = transactions.Entities[0];
            }
            return entity;
        }

        public static int GetHowManySelfPay(IOrganizationService service, ITracingService tracingService, DateTime LastEmployerContributionMonth, DateTime WorkMonth, Guid MemberId)
        {
            int CntSP = 0;
            string fetchxml = $@"
                    <fetch>
                      <entity name='bpa_transaction' >
                        <attribute name='bpa_transactiondate' />
                        <attribute name='bpa_transactionid' />
                        <filter type='and' >
                          <condition attribute='bpa_memberplanid' operator='eq' value='{MemberId.ToString()}' />
                          <condition attribute='bpa_transactiondate' operator='ge' value='{LastEmployerContributionMonth.ToShortDateString()}' />
                          <condition attribute='bpa_transactiondate' operator='lt' value='{WorkMonth.ToShortDateString()}' />
                          <condition attribute='statecode' operator='eq' value='0' />
                          <condition attribute='bpa_transactiontype' operator='in' >
                            <value>{((int)BpaTransactionbpaTransactionType.SelfPayInBenefit).ToString()}</value>
                          </condition>
                        </filter>
                        <order attribute='bpa_transactiondate' descending='true' />
                      </entity>
                    </fetch>
                ";
            EntityCollection transactions = service.RetrieveMultiple(new FetchExpression(fetchxml));

            if (transactions != null && transactions.Entities.Count > 0)
            {
                CntSP = transactions.Entities.Count;
            }
            return CntSP;
        }

        public static void DeleteTransaction(IOrganizationService service, Guid transactionid)
        {
            service.Delete(PluginHelper.BpaTransaction, transactionid);
            //Entity trans = new Entity(PluginHelper.BpaTransaction)
            //{
            //    Id = transactionid,
            //    ["bpa_deleteme"] = true
            //};
            //service.Update(trans);
        }

        public static bool HasFutureTransactions(IOrganizationService service, DateTime workMonth, Guid memberId)
        {
            bool isExists = false;

            string fetchxml = $@"<fetch >
                  <entity name='bpa_transaction' >
                    <attribute name='bpa_name' />
                    <filter type='and' >
                      <condition attribute='statecode' operator='eq' value='0' />
                      <condition attribute='bpa_memberplanid' operator='eq' value='{memberId.ToString()}' />
                      <condition attribute='bpa_transactiondate' operator='gt' value= '{workMonth.ToShortDateString()}' />
                      <condition attribute='bpa_transactiontype' operator='in' >
                        <value>{((int)BpaTransactionbpaTransactionType.EmployerContribution).ToString()}</value>
                        <value>{((int)BpaTransactionbpaTransactionType.EligibilityDraw).ToString()} </value>
                        <value>{((int)BpaTransactionbpaTransactionType.FundAssisted).ToString()}</value>
                        <value>{((int)BpaTransactionbpaTransactionType.FundAssistedInBenefit).ToString()}</value>
                        <value>{((int)BpaTransactionbpaTransactionType.SelfPay).ToString()}</value>
                        <value>{((int)BpaTransactionbpaTransactionType.SelfPayDraw).ToString()}</value>
                        <value>{((int)BpaTransactionbpaTransactionType.SelfPayInBenefit).ToString()}</value>
                        <value>{((int)BpaTransactionbpaTransactionType.TransferIn).ToString()}</value>
                        <value>{((int)BpaTransactionbpaTransactionType.InBenefit).ToString()}</value>
                        <value>{((int)BpaTransactionbpaTransactionType.NotInBenefit).ToString()}</value>
                        <value>{((int)BpaTransactionbpaTransactionType.NewMember).ToString()}</value>
                        <value>{((int)BpaTransactionbpaTransactionType.UnderReview).ToString()}</value>
                        <value>{((int)BpaTransactionbpaTransactionType.SelfPayInBenefit).ToString()}</value>
                        <value>{((int)BpaTransactionbpaTransactionType.ReInstating).ToString()}</value>
                        <value>{((int)BpaTransactionbpaTransactionType.HourBankAdjustment).ToString()}</value>
                        <value>{((int)BpaTransactionbpaTransactionType.DollarBankAdjustment).ToString()}</value>
                      </condition>
                    </filter>
                    <order attribute='bpa_transactiontype' />
                  </entity>
                </fetch>";
            EntityCollection transactions = service.RetrieveMultiple(new FetchExpression(fetchxml));
            if (transactions != null && transactions.Entities.Count > 0)
                isExists = true;

            return isExists;
        }

        public static DateTime FutureAtivityMonth(IOrganizationService service, DateTime workMonth, Guid memberId)
        {
            DateTime futureMonth = DateTime.MinValue;
            string fetchXml = $@"
                <fetch aggregate='true' >
                  <entity name='bpa_transaction' >
                    <attribute name='bpa_transactiondate' alias='bpa_transactiondate' aggregate='min' />
                    <filter>
                      <condition attribute='bpa_memberplanid' operator='eq' value='{memberId.ToString()}' />
                      <condition attribute='bpa_transactiondate' operator='gt' value=' {workMonth.ToShortDateString()}' />
                      <condition attribute='statecode' operator='eq' value='0' />
                      <condition attribute='bpa_transactiontype' operator='in' >
                        <value>{((int)BpaTransactionbpaTransactionType.EmployerContribution).ToString()}</value>
                        <value>{((int)BpaTransactionbpaTransactionType.HourBankAdjustment).ToString()}</value>
                        <value>{((int)BpaTransactionbpaTransactionType.DollarBankAdjustment).ToString()}</value>
                      </condition>
                    </filter>
                  </entity>
                </fetch>
                ";
            EntityCollection transactions = service.RetrieveMultiple(new FetchExpression(fetchXml));
            if (transactions != null && transactions.Entities.Count > 0)
            {
                Entity transaction = transactions.Entities[0];

                if (transaction.Attributes.Contains("bpa_transactiondate") && ((AliasedValue)transaction.Attributes["bpa_transactiondate"]).Value != null)
                    futureMonth = ((DateTime)((AliasedValue)transaction.Attributes["bpa_transactiondate"]).Value);
            }

            if (futureMonth != DateTime.MinValue)
                futureMonth = new DateTime(futureMonth.Year, futureMonth.Month, 1).AddMonths(-1);

            return futureMonth;
        }



        public static bool IsReserveExists(IOrganizationService service, Guid memberId, DateTime workMonth)
        {
            bool isExists = false;
            string fetchXml = @"
                <fetch >
                  <entity name='bpa_transaction' >
                    <attribute name='bpa_name' />
                    <filter type='and' >
                      <condition attribute='bpa_memberplanid' operator='eq' value='" + memberId + @"' />
                      <condition attribute='statecode' operator='eq' value='0' />
                      <condition attribute='bpa_transactiondate' operator='eq' value='" + workMonth.ToShortDateString() +
                              @"' />
                      <condition attribute='bpa_transactiontype' operator='in' >
                        <value>" + (int)BpaTransactionbpaTransactionType.ReserveTransfer + @"</value>
                      </condition>
                    </filter>
                  </entity>
                </fetch>            ";
            EntityCollection transactions = service.RetrieveMultiple(new FetchExpression(fetchXml));
            if (transactions != null && transactions.Entities.Count > 0)
                isExists = true;

            return isExists;
        }


        public static decimal GetSumAmount(IOrganizationService service, Guid memberId, DateTime workMonth, int transactionType)
        {
            decimal sumAmount = 0;
            string fetchxml = @"
                    <fetch aggregate='true' >
                      <entity name='bpa_transaction' >
                        <attribute name='bpa_dollarhour' alias='bpa_dollarhour' aggregate='sum' />
                        <filter>
                          <condition attribute='statecode' operator='eq' value='0' />
                          <condition attribute='bpa_useforcalculation' operator='eq' value='1' />
                          <condition attribute='bpa_memberplanid' operator='eq' value='" + memberId + @"' />
                          <condition attribute='bpa_transactiontype' operator='eq' value='" + transactionType + @"' />
                          <condition attribute='bpa_transactiondate' operator='le' value='" +
                              workMonth.AddMonths(1).AddDays(-1).ToShortDateString() + @"' />
                        </filter>
                      </entity>
                    </fetch>
                ";

            EntityCollection entityCollection = service.RetrieveMultiple(new FetchExpression(fetchxml));
            if (entityCollection != null && entityCollection.Entities.Count > 0)
            {
                Entity sum = entityCollection.Entities[0];

                //if (sum.Attributes.Contains("bpa_dollarhour"))
                if (sum.Attributes.Contains("bpa_dollarhour") &&
                    ((AliasedValue)sum.Attributes["bpa_dollarhour"]).Value != null)
                    sumAmount = ((Money)sum.GetAttributeValue<AliasedValue>("bpa_dollarhour").Value).Value;
            }
            return sumAmount;
        }


        public static Guid CreatReserveTransfer(IOrganizationService service, Guid memberId, DateTime workMonth,
            DateTime benefitMonth, decimal reserveTransferAmount, Guid defaultFundId)
        {
            Entity reserveTransferTransaction = new Entity(PluginHelper.BpaTransaction);
            reserveTransferTransaction["bpa_memberplanid"] = new EntityReference(PluginHelper.BpaMemberplan, memberId);
            reserveTransferTransaction["bpa_transactiondate"] = workMonth;
            reserveTransferTransaction["bpa_benefitmonth"] = benefitMonth;
            reserveTransferTransaction["bpa_dollarhour"] = new Money(reserveTransferAmount);
            reserveTransferTransaction["bpa_transactiontype"] =
                new OptionSetValue((int)BpaTransactionbpaTransactionType.ReserveTransfer);
            if (defaultFundId != Guid.Empty)
                reserveTransferTransaction["bpa_fund"] = new EntityReference(PluginHelper.BpaFund, defaultFundId);
            reserveTransferTransaction["bpa_useforcalculation"] = true;
            return service.Create(reserveTransferTransaction);
        }

        public static Guid CreatReserveTransfer_Hour(IOrganizationService service, Guid memberId, DateTime workMonth,
            DateTime benefitMonth, decimal reserveTransferAmount, Guid defaultFundId)
        {
            Entity reserveTransferTransaction = new Entity(PluginHelper.BpaTransaction);
            reserveTransferTransaction["bpa_memberplanid"] = new EntityReference(PluginHelper.BpaMemberplan, memberId);
            reserveTransferTransaction["bpa_transactiondate"] = workMonth;
            reserveTransferTransaction["bpa_benefitmonth"] = benefitMonth;
            reserveTransferTransaction["bpa_hours"] = reserveTransferAmount;
            reserveTransferTransaction["bpa_transactiontype"] =
                new OptionSetValue((int)BpaTransactionbpaTransactionType.ReserveTransfer);
            if (defaultFundId != Guid.Empty)
                reserveTransferTransaction["bpa_fund"] = new EntityReference(PluginHelper.BpaFund, defaultFundId);
            reserveTransferTransaction["bpa_useforcalculation"] = true;
            return service.Create(reserveTransferTransaction);
        }

        public static Guid FetchEligibilityTransactionForWorkMonth(IOrganizationService service, DateTime workMonth, Guid memberId)
        {
            Guid transactionid = Guid.Empty;

            string fetchxml = @"<fetch >
                  <entity name='bpa_transaction' >
                    <attribute name='bpa_transactionid' />
                    <filter type='and' >
                      <condition attribute='statecode' operator='eq' value='0' />
                      <condition attribute='bpa_memberplanid' operator='eq' value='" + memberId + @"' />
                      <condition attribute='bpa_transactiondate' operator='eq' value=' " + workMonth.ToShortDateString() +
                              @"' />
                      <condition attribute='bpa_transactiontype' operator='in' >
	                        <value>" + (int)BpaTransactionbpaTransactionType.InBenefit + @"</value>
	                        <value>" + (int)BpaTransactionbpaTransactionType.NotInBenefit + @"</value>
	                        <value>" + (int)BpaTransactionbpaTransactionType.SelfPayInBenefit + @"</value>
                            <value>" + (int)BpaTransactionbpaTransactionType.NewMember + @"</value>
	                        <value>" + (int)BpaTransactionbpaTransactionType.FundAssistedInBenefit + @"</value>
	                        <value>" + (int)BpaTransactionbpaTransactionType.UnderReview + @"</value>
	                        <value>" + (int)BpaTransactionbpaTransactionType.ReInstating + @"</value>
                      </condition>
                    </filter>
                    <order attribute='createdon' descending='true'  />
                  </entity>
                </fetch>";
            EntityCollection transactions = service.RetrieveMultiple(new FetchExpression(fetchxml));

            if (transactions != null && transactions.Entities.Count > 0)
            {
                transactionid = transactions.Entities[0].Id;
            }

            return transactionid;
        }

        public static void UpdateEndingBalanace(IOrganizationService service, DateTime workMonth, Guid memberId, Guid defaultFundId)
        {
            Guid transactionId = FetchEligibilityTransactionForWorkMonth(service, workMonth, memberId);
            if (transactionId != Guid.Empty)
            {
                Dictionary<string, decimal> endingBalances = FetchEndingBalance(service, memberId, workMonth,
                    defaultFundId);
                Entity transaction = new Entity("bpa_transaction");
                transaction.Id = transactionId;
                if (endingBalances.ContainsKey("hours"))
                    transaction["bpa_endinghourbalance"] = endingBalances["hours"];
                if (endingBalances.ContainsKey("dollarhours"))
                    transaction["bpa_endingdollarbalance"] = endingBalances["dollarhours"];
                if (endingBalances.ContainsKey("selfpaydollar"))
                    transaction["bpa_endingselfpaybalance"] = new Money(endingBalances["selfpaydollar"]);

                if (endingBalances.ContainsKey("secondarydollar"))
                    transaction["bpa_secondarydollarbalance"] = new Money(endingBalances["secondarydollar"]);

                service.Update(transaction);
            }
        }


        public static decimal GetSumOfHours(IOrganizationService service, Guid memberId, DateTime workMonth,
            int transactionType)
        {
            decimal sumAmount = 0;
            string fetchxml = @"
                    <fetch aggregate='true' >
                      <entity name='bpa_transaction' >
                        <attribute name='bpa_hours' alias='bpa_hours' aggregate='sum' />
                        <filter>
                          <condition attribute='statecode' operator='eq' value='0' />
                          <condition attribute='bpa_useforcalculation' operator='eq' value='1' />
                          <condition attribute='bpa_memberplanid' operator='eq' value='" + memberId + @"' />
                          <condition attribute='bpa_transactiontype' operator='eq' value='" + transactionType + @"' />
                          <condition attribute='bpa_transactiondate' operator='le' value='" +
                              workMonth.AddMonths(1).AddDays(-1).ToShortDateString() + @"' />
                        </filter>
                      </entity>
                    </fetch>
                ";

            EntityCollection entityCollection = service.RetrieveMultiple(new FetchExpression(fetchxml));
            if (entityCollection != null && entityCollection.Entities.Count > 0)
            {
                Entity sum = entityCollection.Entities[0];
                if (sum.Attributes.Contains("bpa_hours") &&
                    sum.GetAttributeValue<AliasedValue>("bpa_hours").Value != null)
                    sumAmount = (decimal)sum.GetAttributeValue<AliasedValue>("bpa_hours").Value;
            }
            return sumAmount;
        }
    }
}