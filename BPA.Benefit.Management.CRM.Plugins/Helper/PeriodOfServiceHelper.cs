﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BPA.Benefit.Management.CRM.Plugins.Helper
{
    public class PeriodOfServiceHelper
    {

        public static Entity FetchPeriodOfServiceByMemberIdAndWorkMonth(IOrganizationService service, ITracingService tracingService, Guid memberPlanId, DateTime workMonth)
        {
            //Guid id = Guid.Empty;
            Entity periodOfService = null;
            string fetchXml = $@"
                <fetch>
                  <entity name='bpa_periodofservice' >
                    <attribute name='bpa_name' />
                    <attribute name='bpa_startdate' />
                    <filter>
                      <condition attribute='statuscode' operator='in' value='1' >
                        <value>{(int)BpaPeriodOfServiceStatusCode.Active}</value>
                        <value>{(int)BpaPeriodOfServiceStatusCode.Qualifying}</value>
                      </condition>
                      <condition attribute='bpa_startdate' operator='lt' value='{workMonth.ToShortDateString()}' />
                    </filter>
                    <link-entity name='bpa_memberplan' from='bpa_memberplanid' to='bpa_memberplanid' >
                      <filter>
                        <condition attribute='bpa_memberplanid' operator='eq' value='{memberPlanId.ToString()}' />
                      </filter>
                    </link-entity>
                  </entity>
                </fetch>
                ";
            EntityCollection collection = service.RetrieveMultiple(new FetchExpression(fetchXml));
            if(collection != null && collection.Entities.Count > 0)
                periodOfService = collection.Entities[0];

            return periodOfService;
        }


        public static Guid Create(IOrganizationService service, ITracingService tracingService, Guid memberPlanId, DateTime workMonth)
        {
            Entity pos = new Entity(PluginHelper.BpaPeriodOfService)
            {
                ["bpa_memberplanid"] = new EntityReference(PluginHelper.BpaMemberplan, memberPlanId),
                ["bpa_startdate"] = workMonth
            };

            return service.Create(pos);
        }
    }
}
