﻿#region

using System;
using System.Collections.Generic;
using BPA.Benefit.Management.CRM.Plugins.Helper;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;

#endregion

namespace BPA.Benefit.Management.CRM.Plugins
{
    /// <summary>
    ///     SubmissionOnCreatePre PlugIn.
    /// </summary>
    public class SubmissionOnCreatePreValidation : Plugin
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="SubmissionOnCreatePre" /> class.
        /// </summary>
        public SubmissionOnCreatePreValidation()
            : base(typeof(SubmissionOnCreatePreValidation))
        {
            RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(10, "Create",
                PluginHelper.BpaSubmission, ExecutePreSubmissionCreate));

            // Note : you can register for more events here if this plugin is not specific to an individual entity and message combination.
            // You may also need to update your RegisterFile.crmregister plug-in registration file to reflect any change.
        }

        /// <summary>
        ///     Executes the plug-in.
        /// </summary>
        /// <param name="localContext">
        ///     The <see cref="Plugin.LocalPluginContext" /> which contains the
        ///     <see cref="IPluginExecutionContext" />,
        ///     <see cref="IOrganizationService" />
        ///     and <see cref="ITracingService" />
        /// </param>
        /// <remarks>
        ///     For improved performance, Microsoft Dynamics CRM caches plug-in instances.
        ///     The plug-in's Execute method should be written to be stateless as the constructor
        ///     is not called for every invocation of the plug-in. Also, multiple system threads
        ///     could execute the plug-in at the same time. All per invocation state information
        ///     is stored in the context. This means that you should not use global variables in plug-ins.
        /// </remarks>
        protected void ExecutePreSubmissionCreate(LocalPluginContext localContext)
        {
            // DONE - TODO JK - Add comments and trace statements.  Check variables for null exceptions

            // If local context not passed in, raise error
            if (localContext == null)
                throw new ArgumentNullException($"Local context not passed in.");

            // Create the context variables
            IPluginExecutionContext context = localContext.PluginExecutionContext;
            ITracingService tracingService = localContext.TracingService;
            IOrganizationService service = localContext.OrganizationService;

            // If Target is not passed in, raise error
            tracingService.Trace("If Target is not passed in, raise error");
            if (!context.InputParameters.Contains(PluginHelper.Target) ||
                !(context.InputParameters[PluginHelper.Target] is Entity))
                throw new ArgumentNullException($"Target is not of type Entity.");

            // Create the target entity
            tracingService.Trace("Create the target entity");
            Entity entity = (Entity)context.InputParameters[PluginHelper.Target];
            

            /*EntityReference employerRef = null;
            if (entity.Attributes.Contains("bpa_accountagreementid"))
            {
                employerRef = entity.GetAttributeValue<EntityReference>("bpa_accountagreementid");
                tracingService.Trace("After Employer = " + employerRef.Id);
            }
            
            // Get Ower from Account Agreement
            Entity AccountAgreementRef = AccountAgreementHelper.FetchAccountDetail(service, tracingService, employerRef.Id);
            EntityReference owner = null;
            if (AccountAgreementRef != null)
            {
                owner = AccountAgreementRef.Contains("ownerid") ? AccountAgreementRef.GetAttributeValue<EntityReference>("ownerid") : null;
                if (owner != null)
                    entity["ownerid"] = owner;
            }*/
        }

    }
}