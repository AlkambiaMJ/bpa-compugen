﻿#region

using System;
using BPA.Benefit.Management.CRM.Plugins.Helper;
using Microsoft.Xrm.Sdk;

#endregion

namespace BPA.Benefit.Management.CRM.Plugins
{
    /// <summary>
    ///     AccountOnUpdatePost Plugin.
    /// </summary>
    public class AccountOnUpdatePost : Plugin
    {

        /// <summary>
        ///     Initializes a new instance of the <see cref="AccountOnUpdatePost" /> class.
        /// </summary>
        public AccountOnUpdatePost()
            : base(typeof(AccountOnUpdatePost))
        {
            RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(40, "Update", PluginHelper.Account, ExecutePostAccountUpdate));

            // Note : you can register for more events here if this plugin is not specific to an individual entity and message combination.
            // You may also need to update your RegisterFile.crmregister plug-in registration file to reflect any change.
        }

        /// <summary>
        ///     Executes the plug-in.
        /// </summary>
        /// <param name="localContext">
        ///     The <see cref="Plugin.LocalPluginContext" /> which contains the
        ///     <see cref="IPluginExecutionContext" />,
        ///     <see cref="IOrganizationService" />
        ///     and <see cref="ITracingService" />
        /// </param>
        /// <remarks>
        ///     For improved performance, Microsoft Dynamics CRM caches plug-in instances.
        ///     The plug-in's Execute method should be written to be stateless as the constructor
        ///     is not called for every invocation of the plug-in. Also, multiple system threads
        ///     could execute the plug-in at the same time. All per invocation state information
        ///     is stored in the context. This means that you should not use global variables in plug-ins.
        /// </remarks>
        protected void ExecutePostAccountUpdate(LocalPluginContext localContext)
        {
            // If local context not passed in, raise error
            if (localContext == null)
                throw new ArgumentNullException($"Local context not passed in.");

            // Create the context variables
            IPluginExecutionContext context = localContext.PluginExecutionContext;
            IOrganizationService service = localContext.OrganizationService;
            ITracingService tracingService = localContext.TracingService;

            // If Target is not passed in, raise error
            tracingService.Trace("If Target is not passed in, raise error");
            if (!context.InputParameters.Contains(PluginHelper.Target) ||
                !(context.InputParameters[PluginHelper.Target] is Entity))
                throw new ArgumentNullException($"Target is not of type Entity.");

            // Create the target entity & postImage
            tracingService.Trace("Create the target entity & postImage");
            Entity entity = (Entity)context.InputParameters[PluginHelper.Target];

            // If the fullname or SIN Number have changed
            tracingService.Trace("If the Name has changed");
            if (!entity.Contains("name")) return;
            

            // Fetch all Member Plans
            tracingService.Trace("Fetch all Account Agreements");
            EntityCollection Collection = AccountAgreementHelper.FetchAllAccountAgreementByAccountId(service, tracingService, entity.Id);
            tracingService.Trace("Account Agreements Count = " + Collection.Entities.Count);
            if (Collection.Entities.Count <= 0) return;

            string name = entity.GetAttributeValue<string>("name");

            // Loop though each Member Plan
            tracingService.Trace("Loop though each Member Plan");
            foreach (Entity aa in Collection.Entities)
            {
                // Update the Member Plan record with the new full name and SIN
                tracingService.Trace("Update the Account Agreement record with the new name ");

                PluginHelper.UpdateField(service, tracingService, aa.Id, PluginHelper.AccountAgreement, "bpa_name", name);
                tracingService.Trace("UPDATED Account Agreement");

            }
        }
    }
}
