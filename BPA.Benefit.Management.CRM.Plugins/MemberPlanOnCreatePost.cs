﻿#region

using System;
using BPA.Benefit.Management.CRM.Plugins.Helper;
using Microsoft.Xrm.Sdk;

#endregion

namespace BPA.Benefit.Management.CRM.Plugins
{
    /// <summary>
    ///     MemberPlanAdjustmentOnCreatePost PlugIn.
    /// </summary>
    public class MemberPlanOnCreatePost : Plugin
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="MemberPlanOnCreatePost" /> class.
        /// </summary>
        public MemberPlanOnCreatePost()
            : base(typeof(MemberPlanOnCreatePost))
        {
            RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(40, "Create",
                PluginHelper.BpaMemberplan, PostExecuteCreate));

            // Note : you can register for more events here if this plugin is not specific to an individual entity and message combination.
            // You may also need to update your RegisterFile.crmregister plug-in registration file to reflect any change.
        }

        /// <summary>
        ///     Executes the plug-in.
        /// </summary>
        /// <param name="localContext">
        ///     The <see cref="Plugin.LocalPluginContext" /> which contains the
        ///     <see cref="IPluginExecutionContext" />,
        ///     <see cref="IOrganizationService" />
        ///     and <see cref="ITracingService" />
        /// </param>
        /// <remarks>
        ///     For improved performance, Microsoft Dynamics CRM caches plug-in instances.
        ///     The plug-in's Execute method should be written to be stateless as the constructor
        ///     is not called for every invocation of the plug-in. Also, multiple system threads
        ///     could execute the plug-in at the same time. All per invocation state information
        ///     is stored in the context. This means that you should not use global variables in plug-ins.
        /// </remarks>
        protected void PostExecuteCreate(LocalPluginContext localContext)
        {
            // If local context not passed in, raise error
            if (localContext == null)
                throw new ArgumentNullException($"Local context not passed in.");

            // Create the context variables
            IPluginExecutionContext context = localContext.PluginExecutionContext;
            IOrganizationService service = localContext.OrganizationService;
            ITracingService tracingService = localContext.TracingService;

            // If Target is not passed in, raise error
            tracingService.Trace("If Target is not passed in, raise error");
            if (!context.InputParameters.Contains(PluginHelper.Target) ||
                !(context.InputParameters[PluginHelper.Target] is Entity))
                throw new ArgumentNullException($"Target is not of type Entity.");

            // Create the target entity & postImage
            tracingService.Trace("Create the target entity & postImage");
            Entity entity = (Entity)context.InputParameters[PluginHelper.Target];
            Entity postImageEntity = context.PostEntityImages != null &&
                                     context.PostEntityImages.Contains(PluginHelper.PostImageAlias)
                ? context.PostEntityImages[PluginHelper.PostImageAlias]
                : null;
            if (postImageEntity == null) return;

            // Following method called only when new Member Plan Details created 
            tracingService.Trace("Following method called only when new Member Plan Details created");

            // If the postImage contains the Member record and Plan (sector) record
            tracingService.Trace("If the postImage contains the Member record and Plan (sector) record");
            if (!postImageEntity.Attributes.Contains("bpa_memberplanid") ||
                postImageEntity.Attributes["bpa_memberplanid"] == null ||
                !postImageEntity.Attributes.Contains("bpa_planid") || postImageEntity.Attributes["bpa_planid"] == null)
                return;
            EntityReference memberRef = entity.Contains("bpa_contactid")
                ? entity.GetAttributeValue<EntityReference>("bpa_contactid")
                : postImageEntity.GetAttributeValue<EntityReference>("bpa_contactid");

            // Find Last Created Member Plan Detail
            tracingService.Trace("Find Last Created Member Plan");
            Entity planDetail = ContactHelper.FetchLastMemberPlan(service, tracingService, memberRef.Id, entity.Id);
            tracingService.Trace("Fetch Last Created Member Plan");

            if (planDetail == null) return;
            tracingService.Trace("In Member Plan Detail");

            EntityCollection dependents = ContactHelper.FetchAllDependents(service, tracingService,
                planDetail.Id);
            if (dependents == null || dependents.Entities.Count <= 0) return;
            foreach (Entity dependent in dependents.Entities)
            {
                // Remove all references
                tracingService.Trace("Remove all references");
                dependent.Id = Guid.Empty;
                dependent.Attributes.Remove("bpa_dependantid");
                dependent.Attributes.Remove("bpa_memberplanid");
                dependent.Attributes.Remove("bpa_issyncronize");

                // Add Relationship
                tracingService.Trace("Add Relationship");
                dependent["bpa_iscopy"] = true;
                dependent["bpa_memberplanid"] = new EntityReference(PluginHelper.BpaMemberplan, entity.Id);

                // Create the record
                tracingService.Trace("Create the record");
                service.Create(dependent);
            }
        }
    }
}