﻿#region

using System;
using BPA.Benefit.Management.CRM.Plugins.Helper;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;

#endregion

namespace BPA.Benefit.Management.CRM.Plugins
{
    /// <summary>
    ///     AgreementOnCreatePost Plugin.
    /// </summary>
    public class AgreementOnCreatePost : Plugin
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="AgreementOnCreatePost" /> class.
        /// </summary>
        public AgreementOnCreatePost(string unsecureString, string secureString)
            : base(typeof(AgreementOnCreatePost), unsecureString,  secureString)
        {
            RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(40, "Create",
                PluginHelper.BpaSubsector, ExecuteOnCreateAgreement));

            // Note : you can register for more events here if this plugin is not specific to an individual entity and message combination.
            // You may also need to update your RegisterFile.crmregister plug-in registration file to reflect any change.
        }

        /// <summary>
        ///     Executes the plug-in.
        /// </summary>
        /// <param name="localContext">
        ///     The <see cref="Plugin.LocalPluginContext" /> which contains the
        ///     <see cref="IPluginExecutionContext" />,
        ///     <see cref="IOrganizationService" />
        ///     and <see cref="ITracingService" />
        /// </param>
        /// <remarks>
        ///     For improved performance, Microsoft Dynamics CRM caches plug-in instances.
        ///     The plug-in's Execute method should be written to be stateless as the constructor
        ///     is not called for every invocation of the plug-in. Also, multiple system threads
        ///     could execute the plug-in at the same time. All per invocation state information
        ///     is stored in the context. This means that you should not use global variables in plug-ins.
        /// </remarks>
        protected void ExecuteOnCreateAgreement(LocalPluginContext localContext)
        {
            // If local context not passed in, raise error
            if (localContext == null)
                throw new ArgumentNullException($"Local context not passed in.");

            // Create the context variables
            IPluginExecutionContext context = localContext.PluginExecutionContext;
            IOrganizationService service = localContext.OrganizationService;
            ITracingService tracingService = localContext.TracingService;

            // If Target is not passed in, raise error
            tracingService.Trace("If Target is not passed in, raise error");
            if (!context.InputParameters.Contains(PluginHelper.Target) ||
                !(context.InputParameters[PluginHelper.Target] is Entity))
                throw new ArgumentNullException($"Target is not of type Entity.");

            // Create the target entity
            tracingService.Trace("Create the target entity");
            Entity entity = (Entity)context.InputParameters[PluginHelper.Target];

            // Create a new default job classification and link it to the new Agreement
            tracingService.Trace("Create a new default job classification and link it to the new Agreement");
            string jobClassification = string.Empty;
            try
            {
                jobClassification = PluginConfiguration[PluginHelper.DefaultJobClassification];
            }
            catch (Exception ex)
            {
                // Get value from Database
                jobClassification = ConfigurationHelper.FetchValueId(service, PluginHelper.DefaultJobClassification);
            }
            if (string.IsNullOrEmpty(jobClassification))
            {
                tracingService.Trace("ERROR FROM AgreementOnCreatePost Plugin - Default Job Classification Could not Found.");
                throw new InvalidPluginExecutionException(OperationStatus.Failed, $@"The given key was not present in the dictionary. Please contact BPA Administrator");
            }
            Guid labourRoleId = LabourRoleHelper.Create(service, tracingService, entity.Id, jobClassification);


            //if (entity.Contains("bpa_sector"))
            //{
            //    tracingService.Trace("Create Default Fund");
            //    // 2. Get the default fund and create Rate
            //    EntityReference planRef = entity.GetAttributeValue<EntityReference>("bpa_sector");
                
            //    //Entity plan = PlanHelper.FetchPlanAndDefaultFund(service, tracingService, planRef.Id, )

            //    if (plan != null)
            //    {
            //        bool isBenefitPlan = plan.GetAttributeValue<bool>("bpa_benefitplan");

            //        if(isBenefitPlan)
            //        {
            //            // Get default WelFare Fund from Trust
            //        }
            //        // 3. associate above rate to default Job Classification
            //    }
            //}

        }
    }
}