﻿using BPA.Benefit.Management.CRM.Plugins.Helper;
using Microsoft.Xrm.Sdk;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BPA.Benefit.Management.CRM.Plugins
{
    public class MemberOnCreatePre : Plugin
    {
        private static object _lockingObject = new object();
        /// <summary>
        /// Initializes a new instance of the <see cref="PreAttendanceCreate"/> class.
        /// </summary>
        public MemberOnCreatePre()
            : base(typeof(MemberOnCreatePre))
        {
            base.RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(20, "Create", PluginHelper.bpa_member, new Action<LocalPluginContext>(ExecutePreMemberCreate)));

            // Note : you can register for more events here if this plugin is not specific to an individual entity and message combination.
            // You may also need to update your RegisterFile.crmregister plug-in registration file to reflect any change.
        }
        
        //THIS PLUG-IN MOVED TO CONTACT LEVEL - SIN GENERATION
        protected void ExecutePreMemberCreate(LocalPluginContext localContext)
        {
            return;
        }


    }
}
