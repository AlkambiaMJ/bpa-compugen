﻿#region

using System;
using BPA.Benefit.Management.CRM.Plugins.Helper;
using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;

#endregion

namespace BPA.Benefit.Management.CRM.Plugins
{
    /// <summary>
    ///     PaymentOnCreateUpdatePost PlugIn.
    /// </summary>
    public class PaymentOnCreateUpdatePost : Plugin
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="PaymentOnCreateUpdatePost" /> class.
        /// </summary>
        //public PaymentOnCreateUpdatePost()
        //    : base(typeof(PaymentOnCreateUpdatePost))
        //{
        public PaymentOnCreateUpdatePost(string unsecureString, string secureString)
            : base(typeof(PaymentOnCreateUpdatePost), unsecureString, secureString)
        {
            RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(40, "Create", "bpa_payment",
                ExecutePaymentOnCreateUpdatePost));
            RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(40, "Update", "bpa_payment",
                ExecutePaymentOnCreateUpdatePost));

            // Note : you can register for more events here if this plugin is not specific to an individual entity and message combination.
            // You may also need to update your RegisterFile.crmregister plug-in registration file to reflect any change.
        }

        /// <summary>
        ///     Executes the plug-in.
        /// </summary>
        /// <param name="localContext">
        ///     The <see cref="Plugin.LocalPluginContext" /> which contains the
        ///     <see cref="IPluginExecutionContext" />,
        ///     <see cref="IOrganizationService" />
        ///     and <see cref="ITracingService" />
        /// </param>
        /// <remarks>
        ///     For improved performance, Microsoft Dynamics CRM caches plug-in instances.
        ///     The plug-in's Execute method should be written to be stateless as the constructor
        ///     is not called for every invocation of the plug-in. Also, multiple system threads
        ///     could execute the plug-in at the same time. All per invocation state information
        ///     is stored in the context. This means that you should not use global variables in plug-ins.
        /// </remarks>
        protected void ExecutePaymentOnCreateUpdatePost(LocalPluginContext localContext)
        {
            // If local context not passed in, raise error
            if (localContext == null)
                throw new ArgumentNullException($"Local context not passed in.");

            // Create the context variables
            IPluginExecutionContext context = localContext.PluginExecutionContext;
            IOrganizationService service = localContext.OrganizationService;
            ITracingService tracingService = localContext.TracingService;
            IServiceProvider serviceProvider = localContext.ServiceProvider;

            // If Target is not passed in, raise error
            tracingService.Trace("If Target is not passed in, raise error");
            if (!context.InputParameters.Contains(PluginHelper.Target) ||
                !(context.InputParameters[PluginHelper.Target] is Entity))
                throw new ArgumentNullException($"Target is not of type Entity.");
            Entity entity = (Entity)context.InputParameters[PluginHelper.Target];

            // Create the target postImage
            tracingService.Trace("Create the target postImage");
            Entity postImageEntity = context.PostEntityImages != null &&
                                     context.PostEntityImages.Contains(PluginHelper.PostImageAlias)
                ? context.PostEntityImages[PluginHelper.PostImageAlias]
                : null;
            if (postImageEntity == null) return;

            // Create the target postImage
            tracingService.Trace("Create the target preImage");
            Entity preImageEntity = context.PreEntityImages != null &&
                                     context.PreEntityImages.Contains(PluginHelper.PostImageAlias)
                ? context.PreEntityImages[PluginHelper.PostImageAlias]
                : null;

            //Fetch Admin REcord
            //Assigned Organization service to CRM Admin User for Deletion of transaction
            //string crmAdminUserId = PluginConfiguration[PluginHelper.ConfigCrmAdmin];
            string crmAdminUserId = string.Empty;
            try
            {
                crmAdminUserId = PluginConfiguration[PluginHelper.ConfigCrmAdmin];
            }
            catch (Exception ex)
            {
                // Get value from Database
                crmAdminUserId = ConfigurationHelper.FetchValueId(service, PluginHelper.ConfigCrmAdmin);
            }
            if (string.IsNullOrEmpty(crmAdminUserId))
            {
                tracingService.Trace("ERROR FROM PaymentOnCreateUpdatePost Plugin - CRM Admin USer Could not Found.");
                throw new InvalidPluginExecutionException(OperationStatus.Failed, $@"The given key was not present in the dictionary. Please contact BPA Administrator");
            }
            IOrganizationServiceFactory serviceFactory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
            IOrganizationService serviceAdmin = serviceFactory.CreateOrganizationService(new Guid(crmAdminUserId));
            tracingService.Trace($"Assigned to Organization Service");

            Boolean readyForProcessing = false;
            localContext.Trace("Get the Ready for Processing boolean.");
            if (entity.Contains("bpa_readyforprocessing"))
            {
                // Get the Member's Plan from either the target or preImage
                localContext.Trace("Getting Member Plan.");
                EntityReference memberPlanRef = null;
                if (entity.Contains("bpa_memberplanid") && entity.Attributes["bpa_memberplanid"] != null)
                    memberPlanRef = (EntityReference)entity.Attributes["bpa_memberplanid"];
                else if (postImageEntity.Contains("bpa_memberplanid") && postImageEntity.Attributes["bpa_memberplanid"] != null)
                    memberPlanRef = (EntityReference)postImageEntity.Attributes["bpa_memberplanid"];
                
                readyForProcessing = (Boolean)entity.Attributes["bpa_readyforprocessing"];

                // If the "ready for processing" boolean is set to true
                if (readyForProcessing)
                {
                    // Get the payment type value
                    OptionSetValue paymentType = null;
                    if (postImageEntity.Contains("bpa_paymenttype"))
                    {
                        //GET ALL VALUES FROM POST IMAGE
                        paymentType = (OptionSetValue)postImageEntity.Attributes["bpa_paymenttype"];
                        DateTime activityStartDate = DateTime.MinValue;

                        if (postImageEntity.Contains("bpa_activitystartdate"))
                            activityStartDate = postImageEntity.GetAttributeValue<DateTime>("bpa_activitystartdate");
                        else
                            activityStartDate = entity.GetAttributeValue<DateTime>("bpa_activitystartdate");

                        Money chequeAmount = new Money(0);
                        if (paymentType.Value == 922070005)//Pre-Paid Legal
                        {
                            if (preImageEntity.Contains("bpa_paymentamount"))
                                chequeAmount = preImageEntity.GetAttributeValue<Money>("bpa_paymentamount");
                        }
                        else
                        {
                            if (postImageEntity.Contains("bpa_paymentamount"))
                                chequeAmount = postImageEntity.GetAttributeValue<Money>("bpa_paymentamount");
                            else
                                chequeAmount = entity.GetAttributeValue<Money>("bpa_paymentamount");
                        }

                        if (chequeAmount != null && chequeAmount.Value > 0)
                            chequeAmount = new Money(chequeAmount.Value * -1);

                        DateTime paymentdate = DateTime.MinValue;
                        if (postImageEntity.Contains("bpa_paymentdate"))
                            paymentdate = postImageEntity.GetAttributeValue<DateTime>("bpa_paymentdate");
                        else
                            paymentdate = entity.GetAttributeValue<DateTime>("bpa_paymentdate");
                        Guid fundId = Guid.Empty;

                        // Generate Transaction and update Rollups
                        switch (paymentType.Value)
                        {
                            case (int)PaymentBatchPaymentType.ParentalLeave: // Parental Leave
                            case (int)PaymentBatchPaymentType.JuryDuty: // Jury Duty
                            case (int)PaymentBatchPaymentType.BereavmentLeave: // Bereavment Leave
                            case (int)PaymentBatchPaymentType.PrePaidLegal: // Pre-Paid Legal
                            case (int)PaymentBatchPaymentType.DentalClinic: // Dental Clinic

                                // There is no bank activity, so nothing else to do
                                localContext.Trace("Payment is for Parental Leave, Jury Duty, Pre-Paid Legal, Dental Clinic or Bereavment Leave.");
                                break;

                            case (int)PaymentBatchPaymentType.InterimVacation: // Interim Vacation
                                // Update the vacation bank and recalcuate
                                localContext.Trace("Payment is for Interim Vacation.");

                                //Find out the fund 
                                fundId = FundHelper.FindDefaultFundByFundType(service, tracingService, (int)BpaFundbpaFundType.VacationPay, memberPlanRef.Id);

                                // Use TransactionHelper.CreateTransaction and link back to the payment record
                                TransactionHelper.CreateTransactionWithPayment(service, tracingService, memberPlanRef, activityStartDate,
                                    (int)BpaTransactionbpaTransactionCategory.MemberPayment, (int)BpaTransactionbpaTransactionType.VacationDraw,
                                    chequeAmount, new EntityReference(PluginHelper.BpaPayment, entity.Id), paymentdate, fundId);

                                // Trigger the recalcuate of the vacation bank
                                PluginHelper.TriggerRollup(service, tracingService, PluginHelper.BpaMemberplan, memberPlanRef.Id, "bpa_vacationpaybank");
                                break;
                            case (int)PaymentBatchPaymentType.AnnualVacation: // Annual Vacation

                                // Update the vacation bank and recalcuate
                                localContext.Trace("Payment is for Annual Vacation.");

                                //Find out the fund 
                                fundId = FundHelper.FindDefaultFundByFundType(service, tracingService, (int)BpaFundbpaFundType.VacationPay, memberPlanRef.Id);

                                // Use TransactionHelper.CreateTransaction and link back to the payment record
                                TransactionHelper.CreateTransactionWithPayment(service, tracingService, memberPlanRef, activityStartDate,
                                    (int)BpaTransactionbpaTransactionCategory.MemberPayment, (int)BpaTransactionbpaTransactionType.AnnualVacationDraw,
                                    chequeAmount, new EntityReference(PluginHelper.BpaPayment, entity.Id), paymentdate, fundId);

                                // Trigger the recalcuate of the vacation bank
                                PluginHelper.TriggerRollup(service, tracingService, PluginHelper.BpaMemberplan, memberPlanRef.Id, "bpa_vacationpaybank");
                                break;

                            case (int)PaymentBatchPaymentType.SelfPayRefund: // Self-Pay Refund

                                // Update the Member's self-pay bank and recalcuate
                                localContext.Trace("Payment is for Member's Self-pay Refund.");
                                fundId = FundHelper.FindDefaultFundByFundType(service, tracingService, (int)BpaFundbpaFundType.Welfare, memberPlanRef.Id);

                                // Create the member transaction to decrement their self-pay bank
                                TransactionHelper.CreateTransactionWithPayment(service, tracingService, memberPlanRef, activityStartDate,
                                    (int)BpaTransactionbpaTransactionCategory.MemberPayment, (int)BpaTransactionbpaTransactionType.SelfPayRefund,
                                    chequeAmount, new EntityReference(PluginHelper.BpaPayment, entity.Id), paymentdate, fundId);

                                // Trigger the recalcuate of the self-pay bank
                                PluginHelper.TriggerRollup(service, tracingService, PluginHelper.BpaMemberplan, memberPlanRef.Id, "bpa_selfpaybank");
                                
                                break;
                        }
                    }
                }
                else
                {
                    // If the "ready for processing" boolean is set to false
                    // Get the payment type value
                    OptionSetValue paymentType = null;
                    if (postImageEntity.Contains("bpa_paymenttype"))
                    {
                        paymentType = (OptionSetValue)postImageEntity.Attributes["bpa_paymenttype"];

                        // Delete Transactions and update Rollups
                        switch (paymentType.Value)
                        {
                            case (int)PaymentBatchPaymentType.ParentalLeave: // Parental Leave
                            case (int)PaymentBatchPaymentType.JuryDuty: // Jury Duty
                            case (int)PaymentBatchPaymentType.BereavmentLeave: // Bereavment Leave
                            case (int)PaymentBatchPaymentType.PrePaidLegal: // Pre-Paid Legal
                            case (int)PaymentBatchPaymentType.DentalClinic: // Dental Clinic

                                // There is no bank activity, so nothing else to do
                                localContext.Trace("Payment is for Parental Leave, Jury Duty, Pre-Paid Legal, Dental Clinic or Bereavment Leave.");
                                break;

                            case (int)PaymentBatchPaymentType.InterimVacation: // Interim Vacation
                            case (int)PaymentBatchPaymentType.AnnualVacation: // Annual Vacation

                                // Update the vacation bank and recalcuate
                                localContext.Trace("Payment is for Interim and Annual Vacation.");

                                // Delete Transaction which is related to Payment
                                EntityCollection transactions = TransactionHelper.FetchAllTransactionsByPaymentId(service, tracingService, entity.Id);
                                foreach (Entity transaction in transactions.Entities)
                                    serviceAdmin.Delete(PluginHelper.BpaTransaction, transaction.Id);

                                //TransactionHelper.DeleteTransactions(service, transaction.Id);

                                // Trigger the recalcuate of the vacation bank
                                PluginHelper.TriggerRollup(service, tracingService, PluginHelper.BpaMemberplan, memberPlanRef.Id, "bpa_vacationpaybank");
                                break;

                            case (int)PaymentBatchPaymentType.SelfPayRefund: // Self-Pay Refund

                                // Update the Member's self-pay bank and recalcuate
                                localContext.Trace("Payment is for Member's Self-pay Refund.");

                                // Delete Transaction which is related to Payment
                                EntityCollection transactionsSelfPay = TransactionHelper.FetchAllTransactionsByPaymentId(service, tracingService, entity.Id);
                                foreach (Entity transaction in transactionsSelfPay.Entities)
                                    TransactionHelper.DeleteTransactions(service, transaction.Id);

                                // Trigger the recalcuate of the self-pay bank
                                PluginHelper.TriggerRollup(service, tracingService, PluginHelper.BpaMemberplan, memberPlanRef.Id, "bpa_selfpaybank");
                                break;
                        }
                    }
                }

                // If the Payment is linked to a Payment Batch (which it should be)
                // Update the Batch Total (if it isn't a Batch Processing Payment)
                if (postImageEntity.Contains("bpa_batchprocessingid") &&
                postImageEntity.Attributes["bpa_batchprocessingid"] != null) return;

                // Search for an existing Payment Batch
                if (!postImageEntity.Contains("bpa_paymentbatchid") ||
                postImageEntity.Attributes["bpa_paymentbatchid"] == null) return;

                // If the Payment is set to Cancelled, don't bother updating the Payment Batch Totals
                if (postImageEntity.Contains("statusreason") &&
                (int)postImageEntity.Attributes["statusreason"] == (int)BpaPaymentStatusCode.Cancelled) return;

                // Get the active Payment Batch Record
                localContext.Trace("Get the active Payment Batch Record");
                EntityReference paymentBatchRef = (EntityReference)postImageEntity.Attributes["bpa_paymentbatchid"];

                // Update the payment batch total
                // Trigger rollups
                localContext.Trace("Update the Payment Batches total");
                PluginHelper.TriggerRollup(service, tracingService, paymentBatchRef.LogicalName, paymentBatchRef.Id,
                    "bpa_batchtotal");
                PluginHelper.TriggerRollup(service, tracingService, paymentBatchRef.LogicalName, paymentBatchRef.Id,
                    "bpa_paymentcount");
                localContext.Trace("Completed refresh of rollups");
            }
        }
    }
}