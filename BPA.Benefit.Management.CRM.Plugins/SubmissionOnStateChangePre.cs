﻿#region

using System;
using BPA.Benefit.Management.CRM.Plugins.Helper;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;

#endregion

namespace BPA.Benefit.Management.CRM.Plugins
{
    /// <summary>
    ///     SubmissionOnStateChangePre PlugIn.
    /// </summary>
    public class SubmissionOnStateChangePre : Plugin
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="SubmissionOnStateChangePre" /> class.
        /// </summary>
        public SubmissionOnStateChangePre()
            : base(typeof(SubmissionOnStateChangePre))
        {
            RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(20, "SetState",
                PluginHelper.BpaSubmission, ExecuteStateChangeUpdate));
            RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(20, "SetStateDynamicEntity",
                PluginHelper.BpaSubmission, ExecuteStateChangeUpdate));

            // Note : you can register for more events here if this plugin is not specific to an individual entity and message combination.
            // You may also need to update your RegisterFile.crmregister plug-in registration file to reflect any change.
        }

        /// <summary>
        ///     Executes the plug-in.
        /// </summary>
        /// <param name="localContext">
        ///     The <see cref="Plugin.LocalPluginContext" /> which contains the
        ///     <see cref="IPluginExecutionContext" />,
        ///     <see cref="IOrganizationService" />
        ///     and <see cref="ITracingService" />
        /// </param>
        /// <remarks>
        ///     For improved performance, Microsoft Dynamics CRM caches plug-in instances.
        ///     The plug-in's Execute method should be written to be stateless as the constructor
        ///     is not called for every invocation of the plug-in. Also, multiple system threads
        ///     could execute the plug-in at the same time. All per invocation state information
        ///     is stored in the context. This means that you should not use global variables in plug-ins.
        /// </remarks>
        protected void ExecuteStateChangeUpdate(LocalPluginContext localContext)
        {
            // If local context not passed in, raise error
            if (localContext == null)
                throw new ArgumentNullException($"Local context not passed in.");

            // Create the context variables
            IPluginExecutionContext context = localContext.PluginExecutionContext;
            IOrganizationService service = localContext.OrganizationService;
            ITracingService tracingService = localContext.TracingService;

            // If EntityReference is not passed in, raise error
            tracingService.Trace("If Target is not passed in, raise error");
            if (!context.InputParameters.Contains(PluginHelper.EntityMoniker) ||
                !(context.InputParameters[PluginHelper.EntityMoniker] is EntityReference))
                throw new ArgumentNullException($"EntityReference is not passed in.");

            // Create the EntityReference
            tracingService.Trace("Create the EntityReference");
            EntityReference targetEntity = (EntityReference)context.InputParameters[PluginHelper.EntityMoniker];
            if (targetEntity == null)
                throw new ArgumentNullException($"EntityReference is not defined.");

            // If the name is not the Submission entity value
            tracingService.Trace("If the name is not the Submission entity value");
            if (targetEntity.LogicalName != PluginHelper.BpaSubmission)
                return;

            // Get the record's state & status
            tracingService.Trace("Get the record's state & status");
            OptionSetValue status = (OptionSetValue)context.InputParameters["Status"];

            // Switch on the status value
            tracingService.Trace("Switch on the status value");
            switch (status.Value)
            {
                case (int)SubmissionStatusCode.Completed:
                    // Completed
                    tracingService.Trace("Completed");

                    Entity submission = service.Retrieve(PluginHelper.BpaSubmission, targetEntity.Id,
                        new ColumnSet(true));

                    #region Delinquency Calculation Logic

                    // 1. Check if the Submission is in "Complete" status
                    // 2. Get Delinquency Rule
                    // 3. Get Submission Due from the Target
                    // 4. Calculate interest by number of days
                    // 5. Save interest amount back to .... ??

                    if (submission.Attributes.Contains("bpa_submissionstatus") &&
                        submission.GetAttributeValue<OptionSetValue>("bpa_submissionstatus").Value == 922070003)
                    {
                        tracingService.Trace("1. Submission is Completed. Starting Delinquency Routine");

                        tracingService.Trace("2. Getting Delinquency Rule By Submission");

                        Entity delinquyencyRule = SubmissionHelper.GetDelinquencyRuleBySubmissionId(service, tracingService,
                            submission.Id);

                        //We have a rule
                        if (delinquyencyRule != null)
                        {
                            tracingService.Trace("3. We have a matching rule");

                            decimal interestRateP1 = delinquyencyRule.GetAttributeValue<Decimal>("bpa_period1interestrate");
                            decimal interestRateP2 = delinquyencyRule.GetAttributeValue<Decimal>("bpa_period2interestrate");

                            int gracePeriod = delinquyencyRule.GetAttributeValue<Int32>("bpa_period1grace");
                            int remittanceDays = delinquyencyRule.GetAttributeValue<Int32>("bpa_remittancedays");

                            int interestApplicableOnP1 = delinquyencyRule.GetAttributeValue<Int32>("bpa_period1interestapplicableon");
                            int interestApplicableOnP2 = delinquyencyRule.GetAttributeValue<Int32>("bpa_period2interestapplicableon");
                            
                            
                            //By this point the UpdateSubmissionAmounts method would have been done
                            //and the submission entity will have the proper amounts

                            DateTime submissionDueDate = submission.GetAttributeValue<DateTime>("bpa_submissiondate")
                                .AddMonths(1).AddDays(remittanceDays-1);
                            
                            //Get number of days this submission is behind
                            int daysBehind = (int)(DateTime.Now - submissionDueDate).TotalDays;

                            //If the number of days delayed is not 0 and exceeds the grace period then we have to charge interest
                            if (daysBehind > 0 && daysBehind > gracePeriod)
                            {
                                //daysBehind += interestApplicableOnP1;

                                //If the days behind flow over to period 2 then we'll have two interest amounts
                                if (interestRateP2 > decimal.Zero /*&& daysBehind > interestApplicableOnP1*/)
                                { 
                                    tracingService.Trace("4. Calculating interest for two periods");

                                    //Get how many days out of the total days behind falls within P1
                                    int daysBehindP1 =0;
                                    int daysBehindP2 =0;

                                    //There is overflow from P1 and will need to go over to P2
                                    if (daysBehind > interestApplicableOnP1)
                                    {
                                        daysBehindP1 = interestApplicableOnP1;
                                        daysBehindP2 = daysBehind - interestApplicableOnP1;
                                    }
                                    //The days behind is within P1 or exactly equal to P1 so use only daysbehind
                                    else if (daysBehind <= interestApplicableOnP1)
                                    {
                                        daysBehindP1 = daysBehind;
                                    }                   

                                    decimal submissionDue =
                                       submission.GetAttributeValue<Money>("bpa_submissionamount").Value;

                                    decimal dailyInterestP1 = submissionDue * (((interestRateP1 / 100) * 12) / 365);
                                    decimal dailyInterestP2 = submissionDue * (((interestRateP2 / 100) * 12) / 365);
                                    decimal interestDueP1 = dailyInterestP1 * (int)daysBehindP1;
                                    decimal interestDueP2 = dailyInterestP2 * (int)daysBehindP2;
                                    decimal initialInterestAmount = decimal.Zero;

                                    //Check if there is an initial interest rate for this rule, this will be the penalty rate applied to the submission amount initially before
                                    //adding amounts for period 1.
                                    if (delinquyencyRule.Attributes.Contains("bpa_initialpenaltyinterestrate") && delinquyencyRule.GetAttributeValue<Decimal>("bpa_initialpenaltyinterestrate") > 0)
                                    {
                                        decimal initialInterestRate = delinquyencyRule.GetAttributeValue<Decimal>("bpa_initialpenaltyinterestrate");
                                        initialInterestAmount = (initialInterestRate / 100) * submissionDue;
                                    }

                                    //Set the submission interest charge to the interest due
                                    Entity submissionToUpdate = new Entity("bpa_submission");
                                    submissionToUpdate.Id = submission.Id;
                                    submissionToUpdate["bpa_interestcharge"] = new Money(initialInterestAmount + interestDueP1 + interestDueP2);
                                    submissionToUpdate["bpa_submissionduedate"] = submissionDueDate;

                                    service.Update(submissionToUpdate);
                                }
                                else
                                {
                                    //Only one period exists

                                    tracingService.Trace("4. Calculating interest for single period P1");

                                    decimal submissionDue =
                                        submission.GetAttributeValue<Money>("bpa_submissionamount").Value;

                                    decimal dailyInterest = submissionDue * (((interestRateP1 / 100) * 12) / 365);
                                    decimal interestDue = dailyInterest*(int)daysBehind;
                                    decimal initialInterestAmount = decimal.Zero;

                                    //Check if there is an initial interest rate for this rule, this will be the penalty rate applied to the submission amount initially before
                                    //adding amounts for period 1.
                                    if (delinquyencyRule.Attributes.Contains("bpa_initialpenaltyinterestrate") && delinquyencyRule.GetAttributeValue<Decimal>("bpa_initialpenaltyinterestrate") > 0)
                                    {
                                        decimal initialInterestRate = delinquyencyRule.GetAttributeValue<Decimal>("bpa_initialpenaltyinterestrate");
                                        initialInterestAmount = (initialInterestRate / 100) * submissionDue;
                                    }

                                    //Set the submission interest charge to the interest due
                                    Entity submissionToUpdate = new Entity("bpa_submission");
                                    submissionToUpdate.Id = submission.Id;
                                    submissionToUpdate["bpa_interestcharge"] = new Money(initialInterestAmount+interestDue);
                                    submissionToUpdate["bpa_submissionduedate"] = submissionDueDate;

                                    service.Update(submissionToUpdate);
                                }
                            }
                        }
                    }

                    #endregion


                    break;
                case (int)SubmissionStatusCode.Cancelled:
                    //Cancelled
                    tracingService.Trace("In Case of 'Cancelled' ");
                    Entity preImageEntity = context.PreEntityImages != null && context.PreEntityImages.Contains(PluginHelper.PreImageAlias) ? context.PreEntityImages[PluginHelper.PreImageAlias] : null;
                    if (preImageEntity == null)
                        throw new ArgumentNullException($"preImage entity is not defined.");

                    OptionSetValue preStatus = (OptionSetValue)preImageEntity.GetAttributeValue<OptionSetValue>("bpa_submissionstatus");

                    if(preStatus.Value != (int)SubmissionStatus.Pending)
                    {
                        throw new InvalidPluginExecutionException(OperationStatus.Failed, "The submission you are attempting to cancel is not in 'Pending' status. The submission can not be cancelled at this time.");
                    }
                    
                    break;
            }
        }
    }
}