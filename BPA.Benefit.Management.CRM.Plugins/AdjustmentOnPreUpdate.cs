﻿#region

using System;
using BPA.Benefit.Management.CRM.Plugins.Helper;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;

#endregion

namespace BPA.Benefit.Management.CRM.Plugins
{
    /// <summary>
    ///     AdjustmentOnCreatePost Plugin.
    /// </summary>
    public class AdjustmentOnPreUpdate : Plugin
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="AdjustmentOnPreUpdate" /> class.
        /// </summary>
        public AdjustmentOnPreUpdate()
            : base(typeof(AdjustmentOnPreUpdate))
        {
            RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(20, PluginHelper.Update,
                PluginHelper.BpaSubmissionadjustment, ExecutePreAdjustmentUpdate));

            // Note : you can register for more events here if this plugin is not specific to an individual entity and message combination.
            // You may also need to update your RegisterFile.crmregister plug-in registration file to reflect any change.
        }

        /// <summary>
        ///     Executes the plug-in.
        /// </summary>
        /// <param name="localContext">
        ///     The <see cref="Plugin.LocalPluginContext" /> which contains the
        ///     <see cref="IPluginExecutionContext" />,
        ///     <see cref="IOrganizationService" />
        ///     and <see cref="ITracingService" />
        /// </param>
        /// <remarks>
        ///     For improved performance, Microsoft Dynamics CRM caches plug-in instances.
        ///     The plug-in's Execute method should be written to be stateless as the constructor
        ///     is not called for every invocation of the plug-in. Also, multiple system threads
        ///     could execute the plug-in at the same time. All per invocation state information
        ///     is stored in the context. This means that you should not use global variables in plug-ins.
        /// </remarks>
        protected void ExecutePreAdjustmentUpdate(LocalPluginContext localContext)
        {
            // If local context not passed in, raise error
            if (localContext == null)
                throw new ArgumentNullException($"Local context not passed in.");

            // Create the context variables
            IPluginExecutionContext context = localContext.PluginExecutionContext;
            IOrganizationService service = localContext.OrganizationService;
            ITracingService tracingService = localContext.TracingService;

            // If Target is not passed in, raise error
            tracingService.Trace("If Target is not passed in, raise error.");
            if (!context.InputParameters.Contains(PluginHelper.Target) ||
                !(context.InputParameters[PluginHelper.Target] is Entity))
                throw new ArgumentNullException($"Target is not of type Entity.");

            // Create the target entity
            tracingService.Trace("Create the target entity");
            Entity entity = (Entity)context.InputParameters["Target"];

            // Get Pre Image 
            Entity preImageEntity = context.PreEntityImages != null && context.PreEntityImages.Contains(PluginHelper.PreImageAlias)
                                            ? context.PreEntityImages[PluginHelper.PreImageAlias] : null;
            if (preImageEntity == null)
                throw new ArgumentNullException($"preImage entity is not defined.");


            OptionSetValue type = new OptionSetValue(0);
            if (entity.Contains("bpa_type"))
                type = entity.GetAttributeValue<OptionSetValue>("bpa_type");
            else if (preImageEntity.Contains("bpa_type"))
                type = preImageEntity.GetAttributeValue<OptionSetValue>("bpa_type");

            // If Type is NSF
            tracingService.Trace("If Type is NSF");
            if (entity.Contains("bpa_type") && type.Value == (int)AdjustmentbpaType.Nsf)
            {
                EntityReference submission = (entity.Contains("bpa_submission") ? entity.GetAttributeValue<EntityReference>("bpa_submission") : null);
                if (submission == null)
                    return;

                //REtrive Amount paid for selected submission
                Entity submisisondetail = service.Retrieve(PluginHelper.BpaSubmission, submission.Id, new ColumnSet(new string[] { "bpa_submissionpaid", "bpa_submissionstatus" }));
                if (submisisondetail == null)
                    return;
                //Validate Submission Completed or not?
                OptionSetValue submissionstatus = (submisisondetail.Contains("bpa_submissionstatus") ? submisisondetail.GetAttributeValue<OptionSetValue>("bpa_submissionstatus") : new OptionSetValue(0));

                if (submissionstatus.Value != (int)SubmissionStatus.Completed)
                    throw new InvalidPluginExecutionException(OperationStatus.Failed, "Selected Submisison is not completed.");
                //Set Adjustment Amount
                Money paidAmount = (submisisondetail.Contains("bpa_submissionpaid") ? submisisondetail.GetAttributeValue<Money>("bpa_submissionpaid") : new Money(0));
                entity["bpa_amount"] = paidAmount;
            }

            // If you update the message please make sure you also update on "ON Create PRE' Plugin
            if (type.Value == (int)AdjustmentbpaType.InterestPaid)
            {
                if (entity.Contains("bpa_amount") && entity.GetAttributeValue<Money>("bpa_amount").Value > 0)
                    throw new InvalidPluginExecutionException(OperationStatus.Failed, $@"Interest Paid amount must be less than 0.");
            }
            else if (type.Value == (int)AdjustmentbpaType.InterestWaived)
            {
                if (entity.Contains("bpa_amount") && entity.GetAttributeValue<Money>("bpa_amount").Value > 0)
                    throw new InvalidPluginExecutionException(OperationStatus.Failed, $@"Intereset Waived amount must be less than 0.");
            }
            else if (type.Value == (int)AdjustmentbpaType.InterestRedirection)
            {
                if (entity.Contains("bpa_amount") && entity.GetAttributeValue<Money>("bpa_amount").Value < 0)
                    throw new InvalidPluginExecutionException(OperationStatus.Failed, $@"Interest Redirection amount must be greater than 0.");
            }
        }
    }
}