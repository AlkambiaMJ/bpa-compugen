﻿#region

using System;
using System.Linq;
using BPA.Benefit.Management.CRM.Plugins.Helper;
using Microsoft.Xrm.Sdk;

#endregion

namespace BPA.Benefit.Management.CRM.Plugins
{
    /// <summary>
    ///     ContactOnUpdatePost Plugin.
    /// </summary>
    public class ContactOnUpdatePost : Plugin
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="ContactOnUpdatePost" /> class.
        /// </summary>
        public ContactOnUpdatePost()
            : base(typeof(ContactOnUpdatePost))
        {
            RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(40, "Update",
                PluginHelper.Contact, ExecutePostContactUpdate));

            // Note : you can register for more events here if this plugin is not specific to an individual entity and message combination.
            // You may also need to update your RegisterFile.crmregister plug-in registration file to reflect any change.
        }

        /// <summary>
        ///     Executes the plug-in.
        /// </summary>
        /// <param name="localContext">
        ///     The <see cref="Plugin.LocalPluginContext" /> which contains the
        ///     <see cref="IPluginExecutionContext" />,
        ///     <see cref="IOrganizationService" />
        ///     and <see cref="ITracingService" />
        /// </param>
        /// <remarks>
        ///     For improved performance, Microsoft Dynamics CRM caches plug-in instances.
        ///     The plug-in's Execute method should be written to be stateless as the constructor
        ///     is not called for every invocation of the plug-in. Also, multiple system threads
        ///     could execute the plug-in at the same time. All per invocation state information
        ///     is stored in the context. This means that you should not use global variables in plug-ins.
        /// </remarks>
        protected void ExecutePostContactUpdate(LocalPluginContext localContext)
        {
            // If local context not passed in, raise error
            if (localContext == null)
                throw new ArgumentNullException($"Local context not passed in.");

            // Create the context variables
            IPluginExecutionContext context = localContext.PluginExecutionContext;
            IOrganizationService service = localContext.OrganizationService;
            ITracingService tracingService = localContext.TracingService;

            // If Target is not passed in, raise error
            tracingService.Trace("If Target is not passed in, raise error");
            if (!context.InputParameters.Contains(PluginHelper.Target) ||
                !(context.InputParameters[PluginHelper.Target] is Entity))
                throw new ArgumentNullException($"Target is not of type Entity.");

            // Create the target entity & postImage
            tracingService.Trace("Create the target entity & postImage");
            Entity entity = (Entity)context.InputParameters[PluginHelper.Target];
            Entity postImageEntity = context.PostEntityImages != null &&
                                     context.PostEntityImages.Contains(PluginHelper.PostImageAlias)
                ? context.PostEntityImages[PluginHelper.PostImageAlias]
                : null;
            if (postImageEntity == null) return;

            // If the fullname or SIN Number have changed
            tracingService.Trace("If the fullname or SIN Number have changed");
            if (!entity.Contains("fullname") && !entity.Contains("bpa_socialinsurancenumber")) return;

            // Update the name field in the child member plans 
            // Get all the values from the Contact record
            tracingService.Trace("Get all the values from the Contact record");
            string fullname = entity.Contains("fullname") ? entity.GetAttributeValue<string>("fullname") : postImageEntity.GetAttributeValue<string>("fullname");
            tracingService.Trace("Fullname =" + fullname);
            string socialinsurancenumber = entity.Contains("bpa_socialinsurancenumber") ? entity.GetAttributeValue<string>("bpa_socialinsurancenumber") : postImageEntity.GetAttributeValue<string>("bpa_socialinsurancenumber");
            tracingService.Trace("SIN =" + socialinsurancenumber);

            // Fetch all Member Plans
            tracingService.Trace("Fetch all Member Plans");
            EntityCollection planCollections = ContactHelper.FetchMemberPlans(service, tracingService, entity.Id);
            tracingService.Trace("Member Plan Count = " + planCollections.Entities.Count);
            if (planCollections.Entities.Count <= 0) return;

            // Loop though each Member Plan
            tracingService.Trace("Loop though each Member Plan");
            foreach (Entity mp in planCollections.Entities)
            {
                // Update the Member Plan record with the new full name and SIN
                tracingService.Trace("Update the Member Plan record with the new full name and SIN");
               
                Entity memberplan = new Entity(PluginHelper.BpaMemberplan)
                {
                    Id = mp.Id,
                    ["bpa_name"] = fullname,
                    ["bpa_socialinsurancenumber"] = socialinsurancenumber
                };

                if (entity.Contains("bpa_socialinsurancenumber"))
                    memberplan
                        ["bpa_sinnoformat"] = new String(entity.GetAttributeValue<String>("bpa_socialinsurancenumber").ToCharArray().Where(char.IsDigit).ToArray());

                service.Update(memberplan);
                tracingService.Trace("End updating member plan");

                // update Employment Records
                // 1. Retrive all Employment Records
                EntityCollection employmentReocrds = EmploymentHelper.FetchAllEmploymentRecords(service, tracingService, mp.Id);

                if(employmentReocrds != null && employmentReocrds.Entities.Count > 0)
                {
                    foreach (Entity employmentReocrd in employmentReocrds.Entities)
                    {
                        tracingService.Trace("Update the Employment Records");
                        Entity employment = new Entity(PluginHelper.BpaEmployment)
                        {
                            Id = employmentReocrd.Id,
                            ["bpa_name"] = fullname,
                        };
                        service.Update(employment);
                        tracingService.Trace("Updated Employment Records");

                        // UPDATE SUBMISSION DETAIL RECORDS WHICH ARE ONLY ACTIVE

                        EntityCollection submissionDetails = SubmissionDetailHelper.FetchAllSubmissionDetails(service, tracingService, employmentReocrd.Id);
                        if (submissionDetails != null && submissionDetails.Entities.Count > 0)
                        {
                            foreach (Entity submissionDetail in submissionDetails.Entities)
                            {
                                tracingService.Trace("Update the Submission Detail Records");
                                Entity sd = new Entity(PluginHelper.BpaSubmissiondetail)
                                {
                                    Id = submissionDetail.Id,
                                    ["bpa_name"] = fullname,
                                };
                                service.Update(sd);
                                tracingService.Trace("Updated Submission Detail Records");

                            }
                        }
                    }
                }

            }
        }
    }
}