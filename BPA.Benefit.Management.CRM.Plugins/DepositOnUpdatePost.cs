﻿#region

using System;
using BPA.Benefit.Management.CRM.Plugins.Helper;
using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;

#endregion

namespace BPA.Benefit.Management.CRM.Plugins
{
    /// <summary>
    ///     DepositOnUpdatePost PlugIn.
    /// </summary>
    public class DepositOnUpdatePost : Plugin
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="DepositOnUpdatePost" /> class.
        /// </summary>
        public DepositOnUpdatePost(string unsecureString, string secureString)
            : base(typeof(DepositOnUpdatePost), unsecureString, secureString)
        {
            RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(40, "Update",
                PluginHelper.BpaDeposit, ExecutePostDepositUpdate));

            // Note : you can register for more events here if this plugin is not specific to an individual entity and message combination.
            // You may also need to update your RegisterFile.crmregister plug-in registration file to reflect any change.
        }

        /// <summary>
        ///     Executes the plug-in.
        /// </summary>
        /// <param name="localContext">
        ///     The <see cref="Plugin.LocalPluginContext" /> which contains the
        ///     <see cref="IPluginExecutionContext" />,
        ///     <see cref="IOrganizationService" />
        ///     and <see cref="ITracingService" />
        /// </param>
        /// <remarks>
        ///     For improved performance, Microsoft Dynamics CRM caches plug-in instances.
        ///     The plug-in's Execute method should be written to be stateless as the constructor
        ///     is not called for every invocation of the plug-in. Also, multiple system threads
        ///     could execute the plug-in at the same time. All per invocation state information
        ///     is stored in the context. This means that you should not use global variables in plug-ins.
        /// </remarks>
        protected void ExecutePostDepositUpdate(LocalPluginContext localContext)
        {
            // If local context not passed in, raise error
            if (localContext == null)
                throw new ArgumentNullException($"Local context not passed in.");

            // Create the context variables
            IPluginExecutionContext context = localContext.PluginExecutionContext;
            IOrganizationService service = localContext.OrganizationService;
            ITracingService tracingService = localContext.TracingService;

            // If Target is not passed in, raise error
            tracingService.Trace("If Target is not passed in, raise error");
            if (!context.InputParameters.Contains(PluginHelper.Target) ||
                !(context.InputParameters[PluginHelper.Target] is Entity))
                throw new ArgumentNullException($"Target is not of type Entity.");

            // Create the target entity & postImage
            tracingService.Trace("Create the target entity & postImage");
            Entity entity = (Entity)context.InputParameters[PluginHelper.Target];
            Entity postImageEntity = context.PostEntityImages != null &&
                                     context.PostEntityImages.Contains(PluginHelper.PostImageAlias)
                ? context.PostEntityImages[PluginHelper.PostImageAlias]
                : null;
            if (postImageEntity == null) return;

            // Submission: We are just updating 'bpa_hasdeposited' so we can show field as locked
            // Get the Deposit Status
            tracingService.Trace("Get the Deposit Status");
            if (!entity.Attributes.Contains("bpa_depositstatus")) return;
            int depositstatus = entity.GetAttributeValue<OptionSetValue>("bpa_depositstatus").Value;

            // Status = Deposited
            // If the Deposit isn't completed, then return
            tracingService.Trace("If the Deposit isn't completed, then return");
            if (depositstatus != 922070001) return;

            // TESTING - Record the processing start time
            DateTime retroprocessingStartTime = DateTime.Now;

            // Change All linked Submissions to Inactive
            tracingService.Trace("Change All linked Submissions to Inactive");
            EntityCollection submissions = SubmissionHelper.FetchAllSubmissionsByDepositId(service, tracingService, entity.Id, true);
            if (submissions != null && submissions.Entities.Count > 0)
            {
                tracingService.Trace($@"DepositOnUpdatePost : {DateTime.Now}");

                // Get the Completed Stage and Process for the Submission entity
                tracingService.Trace("Get the Completed Stage and Process for the Submission entity");
                string stageId = string.Empty, processId = string.Empty;
                try
                {
                    stageId = PluginConfiguration[PluginHelper.ConfigSubmissionCompletedStage];
                }
                catch (Exception ex)
                {
                    // Get value from Database
                    stageId = ConfigurationHelper.FetchValueId(service, PluginHelper.ConfigSubmissionCompletedStage);
                }
                try
                {
                    processId = PluginConfiguration[PluginHelper.ConfigSubmissionBusinessProcessFlow];
                }
                catch (Exception ex)
                {
                    // Get value from Database
                    processId = ConfigurationHelper.FetchValueId(service, PluginHelper.ConfigSubmissionBusinessProcessFlow);
                }


                if ((string.IsNullOrEmpty(stageId)) || (string.IsNullOrEmpty(processId)))
                {
                    tracingService.Trace("ERROR FROM DepositOnUpdatePost Plugin - CRM ADmin USer Could not Found.");
                    throw new InvalidPluginExecutionException(OperationStatus.Failed, $@"The given key was not present in the dictionary. Please contact BPA Administrator");
                }
                
                //string stageId = (PluginConfiguration != null && PluginConfiguration.ContainsKey(PluginHelper.ConfigSubmissionCompletedStage) ? PluginConfiguration[PluginHelper.ConfigSubmissionCompletedStage] : string.Empty);
                //string processId = (PluginConfiguration != null && PluginConfiguration.ContainsKey(PluginHelper.ConfigSubmissionBusinessProcessFlow) ? PluginConfiguration[PluginHelper.ConfigSubmissionBusinessProcessFlow] : string.Empty);

                // Loop though all linked Submissions and make them Inactive
                tracingService.Trace("Loop though all linked Submissions and make them Inactive");
                foreach (Entity e in submissions.Entities)
                {
                    tracingService.Trace($@"DepositOnUpdatePost - inside submission loop: {DateTime.Now}");

                    tracingService.Trace("Updateing  stage and Processid");
                    int statecode = e.GetAttributeValue<OptionSetValue>("statecode").Value;
                    int statuscode = e.GetAttributeValue<OptionSetValue>("statuscode").Value;

                    e.Attributes["bpa_hasdeposited"] = true;
                    e.Attributes["bpa_submissionstatus"] = new OptionSetValue((int)SubmissionStatus.Completed);
                    if ((stageId != Guid.Empty.ToString()) && (processId != Guid.Empty.ToString()))
                    {
                        e.Attributes["stageid"] = new Guid(stageId);
                        e.Attributes["processid"] = new Guid(processId);
                    }
                    e.Attributes.Remove("statecode");
                    e.Attributes.Remove("statuscode");
                    service.Update(e);

                    tracingService.Trace($@"DepositOnUpdatePost - update submission Stage and: {DateTime.Now}");


                    tracingService.Trace("After Stage & Process update");
                    if(statecode == 0)
                    {
                        tracingService.Trace("Inside If condition");

                        // Deactivate the Submission record
                        // The record must first be set to processing (an active state)
                        // and then to Completed (an inactive state)
                        tracingService.Trace("Deactivate the Submission record - setting state to Processing");
                        SetStateRequest setStateRequest = new SetStateRequest
                        {
                            EntityMoniker = new EntityReference(
                                PluginHelper.BpaSubmission, e.Id),
                            State = new OptionSetValue(0),
                            Status = new OptionSetValue(922070003) // 922070003 - Processing
                        };
                        service.Execute(setStateRequest);
                        tracingService.Trace($@"DepositOnUpdatePost - update submission Processing status : {DateTime.Now}");

                        // Setting to Inactive state
                        tracingService.Trace("Deactivate the Submission record - setting state to Complete");
                        SetStateRequest setStateRequest1 = new SetStateRequest
                        {
                            EntityMoniker = new EntityReference(
                                PluginHelper.BpaSubmission, e.Id),
                            State = new OptionSetValue(1),
                            Status = new OptionSetValue(2)
                        };
                        service.Execute(setStateRequest1);
                        tracingService.Trace($@"DepositOnUpdatePost - update submission INACTIVE status : {DateTime.Now}");

                    }
                }
            }

            // Update the Deposit status to Complete
            tracingService.Trace("Update the Deposit status to Complete");
            SetStateRequest setStateRequest2 = new SetStateRequest
            {
                EntityMoniker = new EntityReference(PluginHelper.BpaDeposit, entity.Id),
                State = new OptionSetValue(1), // Inactive state
                Status = new OptionSetValue(2) // Complete status 
            };
            service.Execute(setStateRequest2);
            tracingService.Trace($@"DepositOnUpdatePost - update Deposit Status : {DateTime.Now}");


            // TESTING - Calculate time difference from start processing to end processing time
            DateTime endTime = DateTime.Now;
            double retroprocessingTotalMinutes = endTime.Subtract(retroprocessingStartTime).Minutes;
            double retroprocessingTotalSeconds = endTime.Subtract(retroprocessingStartTime).Seconds;
            double retroprocessingTotalMilliSeconds = endTime.Subtract(retroprocessingStartTime).Milliseconds;

            string executionTime = "Deposit Completed Minutes: " + retroprocessingTotalMinutes.ToString() +
                " Seconds: " + retroprocessingTotalSeconds.ToString() +
                " Milliseconds: " + retroprocessingTotalMilliSeconds.ToString() + Environment.NewLine;

            //throw new InvalidPluginExecutionException(OperationStatus.Failed, executionTime);


        }
    }
}