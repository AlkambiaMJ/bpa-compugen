﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Activities;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Workflow;
using Microsoft.Crm.Sdk.Messages;

public class Workflow1 : CodeActivity
{
    protected override void Execute(CodeActivityContext Context)
    {
        IWorkflowContext WorkflowContext = Context.GetExtension<IWorkflowContext>();
        IOrganizationServiceFactory ServiceFactory = Context.GetExtension<IOrganizationServiceFactory>();

    }

    [RequiredArgument]
    [Input("Select an email to send")]
    [ReferenceTarget("email")]
    public InArgument<EntityReference> EmailReference { get; set; }
}

