﻿#region

using System;
using BPA.Benefit.Management.CRM.Plugins.Helper;
using Microsoft.Xrm.Sdk;

#endregion


namespace BPA.Benefit.Management.CRM.Plugins
{

    public class PlanBeneficiaryOnCreatePre : Plugin
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="DependentOnCreatePre" /> class.
        /// </summary>
        public PlanBeneficiaryOnCreatePre()
            : base(typeof(PlanBeneficiaryOnCreatePre))
        {
            RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(20, "Create",
                PluginHelper.BpaPlanBeneficiary, ExecutePluginLogic));

            // Note : you can register for more events here if this plugin is not specific to an individual entity and message combination.
            // You may also need to update your RegisterFile.crmregister plug-in registration file to reflect any change.
        }

        /// <summary>
        ///     Executes the plug-in.
        /// </summary>
        /// <param name="localContext">
        ///     The <see cref="Plugin.LocalPluginContext" /> which contains the
        ///     <see cref="IPluginExecutionContext" />,
        ///     <see cref="IOrganizationService" />
        ///     and <see cref="ITracingService" />
        /// </param>
        /// <remarks>
        ///     For improved performance, Microsoft Dynamics CRM caches plug-in instances.
        ///     The plug-in's Execute method should be written to be stateless as the constructor
        ///     is not called for every invocation of the plug-in. Also, multiple system threads
        ///     could execute the plug-in at the same time. All per invocation state information
        ///     is stored in the context. This means that you should not use global variables in plug-ins.
        /// </remarks>
        protected void ExecutePluginLogic(LocalPluginContext localContext)
        {
            // If local context not passed in, raise error
            if (localContext == null)
                throw new ArgumentNullException($"Local context not passed in.");

            // Create the context variables
            IPluginExecutionContext context = localContext.PluginExecutionContext;
            IOrganizationService service = localContext.OrganizationService;
            ITracingService tracingService = localContext.TracingService;

            // If Target is not passed in, raise error
            tracingService.Trace("If Target is not passed in, raise error");
            if (!context.InputParameters.Contains(PluginHelper.Target) ||
                !(context.InputParameters[PluginHelper.Target] is Entity))
                throw new ArgumentNullException($"Target is not of type Entity.");

            // Create the target entity & preImage
            tracingService.Trace("Create the target entity");
            Entity entity = (Entity)context.InputParameters[PluginHelper.Target];

            // Get the parent Dependent 
            tracingService.Trace("Get the Dependent");

            if (!entity.Contains("bpa_beneficiaryid") || !entity.Contains("bpa_memberplanid")) return;

            EntityReference dependant = entity.GetAttributeValue<EntityReference>("bpa_beneficiaryid");
            EntityReference memberPlan = entity.GetAttributeValue<EntityReference>("bpa_memberplanid");

            if (PlanBeneficiaryHelper.IsBeneficiaryAlredyExists(service, tracingService, dependant.Id, memberPlan.Id))
                throw new InvalidPluginExecutionException(OperationStatus.Failed, "You are adding a Beneficiary that is already associated to this Member Plan Record. Your request can not be processed.");

        }

    }
}
