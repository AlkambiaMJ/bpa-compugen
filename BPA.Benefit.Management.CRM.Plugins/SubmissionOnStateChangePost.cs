﻿#region

using System;
using BPA.Benefit.Management.CRM.Plugins.Helper;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;

#endregion

namespace BPA.Benefit.Management.CRM.Plugins
{
    /// <summary>
    ///     SubmissionOnStateChangePost PlugIn.
    /// </summary>
    public class SubmissionOnStateChangePost : Plugin
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="SubmissionOnStateChangePost" /> class.
        /// </summary>
        //public SubmissionOnStateChangePost()
        //    : base(typeof(SubmissionOnStateChangePost))
        public SubmissionOnStateChangePost(string unsecureString, string secureString)
            : base(typeof(SubmissionOnStateChangePost), unsecureString, secureString)
        {
            RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(40, "SetState",
                PluginHelper.BpaSubmission, ExecuteStateChangeUpdate));
            RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(40, "SetStateDynamicEntity",
                PluginHelper.BpaSubmission, ExecuteStateChangeUpdate));

            // Note : you can register for more events here if this plugin is not specific to an individual entity and message combination.
            // You may also need to update your RegisterFile.crmregister plug-in registration file to reflect any change.
        }

        /// <summary>
        ///     Executes the plug-in.
        /// </summary>
        /// <param name="localContext">
        ///     The <see cref="Plugin.LocalPluginContext" /> which contains the
        ///     <see cref="IPluginExecutionContext" />,
        ///     <see cref="IOrganizationService" />
        ///     and <see cref="ITracingService" />
        /// </param>
        /// <remarks>
        ///     For improved performance, Microsoft Dynamics CRM caches plug-in instances.
        ///     The plug-in's Execute method should be written to be stateless as the constructor
        ///     is not called for every invocation of the plug-in. Also, multiple system threads
        ///     could execute the plug-in at the same time. All per invocation state information
        ///     is stored in the context. This means that you should not use global variables in plug-ins.
        /// </remarks>
        protected void ExecuteStateChangeUpdate(LocalPluginContext localContext)
        {
            // If local context not passed in, raise error
            if (localContext == null)
                throw new ArgumentNullException($"Local context not passed in.");

            // Create the context variables
            IPluginExecutionContext context = localContext.PluginExecutionContext;
            IOrganizationService service = localContext.OrganizationService;
            ITracingService tracingService = localContext.TracingService;
            IServiceProvider serviceprovider = localContext.ServiceProvider;
            Guid executeUser = context.InitiatingUserId; // Get execute User

            // If EntityReference is not passed in, raise error
            tracingService.Trace("If Target is not passed in, raise error");
            if (!context.InputParameters.Contains(PluginHelper.EntityMoniker) ||
                !(context.InputParameters[PluginHelper.EntityMoniker] is EntityReference))
                throw new ArgumentNullException($"EntityReference is not passed in.");

            // Create the EntityReference
            tracingService.Trace("Create the EntityReference");
            EntityReference targetEntity = (EntityReference)context.InputParameters[PluginHelper.EntityMoniker];
            if (targetEntity == null)
                throw new ArgumentNullException($"EntityReference is not defined.");

            // If the name is not the Submission entity value
            tracingService.Trace("If the name is not the Submission entity value");
            if (targetEntity.LogicalName != PluginHelper.BpaSubmission)
                return;

            // Get the record's state & status
            tracingService.Trace("Get the record's state & status");
            OptionSetValue status = (OptionSetValue)context.InputParameters["Status"];

            // Switch on the status value
            tracingService.Trace("Switch on the status value");
            switch (status.Value)
            {
                case (int)SubmissionStatusCode.Completed:
                    // Completed
                    tracingService.Trace("Completed");

                    // If Interest (Delinquency) value is > $0
                    // Generate an Employer Adjustment of type "Interest Charge"
                    // Get the Interest Charge Money value
                    tracingService.Trace("Get the Interest Charge Money value");
                    Entity submission = service.Retrieve(PluginHelper.BpaSubmission, targetEntity.Id,
                        new ColumnSet(true));

                    // Does Interest Charge exist and is > 0
                    tracingService.Trace("Does Interest Charge exist and is > 0");
                    if (submission.Contains("bpa_interestcharge") &&
                        ((Money)submission.Attributes["bpa_interestcharge"]).Value > 0)
                    {
                        // Create a new Employer Adjustment (bpa_submissionadjustment) record of type 'Interest Charged'
                        tracingService.Trace("Create a new Employer Adjustment (bpa_submissionadjustment) record of type 'Interest Charged'");
                        Entity adjustment = new Entity("bpa_submissionadjustment");

                        // Add Submission Reference
                        tracingService.Trace("Add Submission Reference");
                        adjustment.Attributes.Add("bpa_submission",
                            new EntityReference(PluginHelper.BpaSubmission, submission.Id));

                        // Add Employer Reference
                        tracingService.Trace("Add Employer Reference");
                        EntityReference employerRef = new EntityReference();
                        if (submission.Contains("bpa_accountagreementid"))
                            employerRef = (EntityReference)submission.Attributes["bpa_accountagreementid"];
                        adjustment.Attributes.Add("bpa_accountagreementid", employerRef);

                        // Add Adjustment Type (Interest Charge - 922,070,004)
                        tracingService.Trace("Add Adjustment Type (Interest Charge - 922,070,004)");
                        adjustment.Attributes.Add("bpa_type", new OptionSetValue(922070004));

                        // Add Amount
                        tracingService.Trace("Add Amount");
                        adjustment.Attributes.Add("bpa_amount", (Money)submission.Attributes["bpa_interestcharge"]);

                        // Add Description
                        tracingService.Trace("Add Description");
                        adjustment.Attributes.Add("bpa_description", "Interest Charge");

                        // Add Date
                        tracingService.Trace("Add Date");
                        DateTime lastWorkMonth = DateTime.Today.AddMonths(-1);
                        lastWorkMonth = new DateTime(lastWorkMonth.Year, lastWorkMonth.Month, 1);
                        adjustment.Attributes.Add("bpa_workmonth", lastWorkMonth);

                        // Save the new record
                        tracingService.Trace("Save the new record");
                        Guid adjustmentId = service.Create(adjustment);

                        PluginHelper.DeactivateRecord(service, PluginHelper.BpaSubmissionadjustment, adjustmentId, (int)AdjustmentStatusCode.Complete);
                        PluginHelper.TriggerRollup(service, tracingService, PluginHelper.AccountAgreement, employerRef.Id, "bpa_totalinterestcharge");
                    }

                    // Deactivate child records
                    tracingService.Trace("Deactivate child records");
                    //DeactivateContributions(service, tracingService, targetEntity.Id);
                    //DeactivateSubmissionDetail(service, tracingService, targetEntity.Id);
                    break;

                case (int)SubmissionStatusCode.Cancelled:
                    tracingService.Trace("Cancelled");
                    Entity preImageEntity = context.PreEntityImages != null && context.PreEntityImages.Contains(PluginHelper.PreImageAlias) ? context.PreEntityImages[PluginHelper.PreImageAlias] : null;
                    if (preImageEntity == null)
                        throw new ArgumentNullException($"preImage entity is not defined.");

                    // Get all Submission Detail and Inactive 
                    EntityCollection submissionDetails = SubmissionHelper.FetchAllSubmissionDetailBySubmissionId(service, tracingService, targetEntity.Id, true, true);
                    if (submissionDetails == null || submissionDetails.Entities.Count <= 0) return;
                    foreach (Entity submissionDetail in submissionDetails.Entities)
                        PluginHelper.DeactivateRecord(service, PluginHelper.BpaSubmissiondetail, submissionDetail.Id, 2); // 2 - Inactive

                    EntityReference deposit = preImageEntity.Contains("bpa_depositid") ? preImageEntity.GetAttributeValue<EntityReference>("bpa_depositid") : null;
                    if(deposit != null)
                        PluginHelper.TriggerRollup(service, tracingService, PluginHelper.BpaDeposit, deposit.Id, "bpa_submissiontotal");

                    break;

            }
        }
        
    }
}