﻿#region

using System;
using BPA.Benefit.Management.CRM.Plugins.Helper;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;

#endregion

namespace BPA.Benefit.Management.CRM.Plugins
{
    /// <summary>
    ///     PaymentOnCreateUpdatePre PlugIn.
    /// </summary>
    public class PaymentOnCreateUpdatePre : Plugin
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="PaymentOnCreateUpdatePre" /> class.
        /// </summary>
        public PaymentOnCreateUpdatePre()
            : base(typeof(PaymentOnCreateUpdatePre))
        {
            RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(20, "Create", "bpa_payment",
                ExecutePaymentOnCreateUpdatePre));
            RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(20, "Update", "bpa_payment",
                ExecutePaymentOnCreateUpdatePre));

            // Note : you can register for more events here if this plugin is not specific to an individual entity and message combination.
            // You may also need to update your RegisterFile.crmregister plug-in registration file to reflect any change.
        }

        /// <summary>
        ///     Executes the plug-in.
        /// </summary>
        /// <param name="localContext">
        ///     The <see cref="Plugin.LocalPluginContext" /> which contains the
        ///     <see cref="IPluginExecutionContext" />,
        ///     <see cref="IOrganizationService" />
        ///     and <see cref="ITracingService" />
        /// </param>
        /// <remarks>
        ///     For improved performance, Microsoft Dynamics CRM caches plug-in instances.
        ///     The plug-in's Execute method should be written to be stateless as the constructor
        ///     is not called for every invocation of the plug-in. Also, multiple system threads
        ///     could execute the plug-in at the same time. All per invocation state information
        ///     is stored in the context. This means that you should not use global variables in plug-ins.
        /// </remarks>
        protected void ExecutePaymentOnCreateUpdatePre(LocalPluginContext localContext)
        {
            // If local context not passed in, raise error

            if (localContext == null)
                throw new ArgumentNullException($"Local context not passed in.");

            // Create the context variables
            IPluginExecutionContext context = localContext.PluginExecutionContext;
            IOrganizationService service = localContext.OrganizationService;
            ITracingService tracingService = localContext.TracingService;

            // If Target is not passed in, raise error
            tracingService.Trace("If Target is not passed in, raise error");
            if (!context.InputParameters.Contains(PluginHelper.Target) ||
                !(context.InputParameters[PluginHelper.Target] is Entity))
                throw new ArgumentNullException($"Target is not of type Entity.");

            // Create the target entity & preImage
            //Only get the preImage if the message is update
            tracingService.Trace("Create the target entity & preImage");
            Entity entity = (Entity)context.InputParameters[PluginHelper.Target];
            Entity preImage = context.MessageName.ToLower() == PluginHelper.Update.ToLower() &&
                              context.PreEntityImages != null &&
                              context.PreEntityImages.Contains(PluginHelper.PreImageAlias)
                ? context.PreEntityImages[PluginHelper.PreImageAlias]
                : null;
            if (context.MessageName.ToLower() == PluginHelper.Update.ToLower() && preImage == null)
                throw new ArgumentNullException($"preImage entity is not defined.");

            // Verify that we have a Trust and Fund
            localContext.Trace("Verifing that Trust and Payment Type is set.");
            EntityReference trustRef = null;
            EntityReference planRef = null;
            EntityReference fundRef = null;
            EntityReference payeeRef = null;
            EntityReference memberPlanRef = null;
            OptionSetValue paymentType = null;
            Boolean readyForProcessing = false;
            int methodOfPayment = 922070001; //Default method is by cheque

            // If the Payment is cancelled
            // Turn the ready for processing flag off
            if (entity.Contains("statuscode") == true)
            {
                localContext.Trace("Status code has changed.");
                OptionSetValue status = (OptionSetValue)entity.Attributes["statuscode"];
                if (status.Value == (int)BpaPaymentStatusCode.Cancelled)
                {
                    localContext.Trace("Payment is cancelled, setting Ready for Processing to false and removing link to Payment Batch");

                    localContext.Trace("Status code is cancelled.");
                    if (entity.Contains("bpa_readyforprocessing"))
                        entity.Attributes["bpa_readyforprocessing"] = false;
                    else
                        entity.Attributes.Add("bpa_readyforprocessing", false);

                    //Clear out the link to the Payment Batch
                    entity.Attributes.Add("bpa_paymentbatchid", null);
                }
            }

            //If a method of payment is defined use that, otherwise use cheque
            if (entity.Contains("bpa_paymentmethod"))
                methodOfPayment = entity.GetAttributeValue<OptionSetValue>("bpa_paymentmethod").Value;

            // If the "ready for processing" boolean is set
            localContext.Trace("Get the Ready for Processing boolean.");
            if (entity.Contains("bpa_readyforprocessing") == true)
            {
                readyForProcessing = (Boolean)entity.Attributes["bpa_readyforprocessing"];
                if (readyForProcessing == true)
                {
                    // STEP 1: Verify all of the information needed to generate a cheque is available

                    // Get the Plan reference
                    localContext.Trace("Get the Plan reference.");
                    if (entity.Contains("bpa_planid") && entity.Attributes["bpa_planid"] != null)
                        planRef = (EntityReference)entity.Attributes["bpa_planid"];
                    else if (preImage.Contains("bpa_planid") && preImage.Attributes["bpa_planid"] != null)
                        planRef = (EntityReference)preImage.Attributes["bpa_planid"];

                    // Raise error that the Member Plan is not defined
                    if (planRef == null)
                    {
                        localContext.Trace("Raise error that the Plan is not defined.");
                        throw new InvalidPluginExecutionException(OperationStatus.Failed, "Plan must be defined.");
                    }

                    // Get the Trust reference
                    localContext.Trace("Get the Trust reference.");
                    if (entity.Contains("bpa_trustid") && entity.Attributes["bpa_trustid"] != null)
                        trustRef = (EntityReference)entity.Attributes["bpa_trustid"];
                    else if (preImage != null && preImage.Contains("bpa_trustid") && preImage.Attributes["bpa_trustid"] != null)
                        trustRef = (EntityReference)preImage.Attributes["bpa_trustid"];

                    // If the Trust is null set it via the Plan
                    if (trustRef == null)
                    {
                        localContext.Trace("The Trust is null set it via the Plan.");
                        Entity plan = localContext.OrganizationService.Retrieve("bpa_sector", planRef.Id,
                            new ColumnSet("bpa_trust"));
                        if (plan != null)
                        {
                            trustRef = (EntityReference)plan.Attributes["bpa_trust"];
                            if (entity.Contains("bpa_trustid") == false)
                                entity.Attributes.Add("bpa_trustid", trustRef);
                            else
                                entity.Attributes["bpa_trustid"] = trustRef;
                        }
                    }

                    // Get the Payment Type
                    localContext.Trace("Get the Payment Type reference.");
                    if (entity.Contains("bpa_paymenttype") && entity.Attributes["bpa_paymenttype"] != null)
                        paymentType = (OptionSetValue)entity.Attributes["bpa_paymenttype"];
                    else if (preImage.Contains("bpa_paymenttype") && preImage.Attributes["bpa_paymenttype"] != null)
                        paymentType = (OptionSetValue)preImage.Attributes["bpa_paymenttype"];

                    if (trustRef == null || paymentType == null)
                    {
                        // Raise error that Trust or Payment Type is not defined
                        localContext.Trace("Raise error that Trust or Payment Type is not defined.");
                        throw new InvalidPluginExecutionException(OperationStatus.Failed,
                            "Trust and Payment Type must be defined.");
                    }

                    // Get the Member's Plan from either the target or preImage
                    localContext.Trace("Getting Member Plan.");
                    if (entity.Contains("bpa_memberplanid") && entity.Attributes["bpa_memberplanid"] != null)
                        memberPlanRef = (EntityReference)entity.Attributes["bpa_memberplanid"];
                    else if (preImage.Contains("bpa_memberplanid") && preImage.Attributes["bpa_memberplanid"] != null)
                        memberPlanRef = (EntityReference)preImage.Attributes["bpa_memberplanid"];

                    if (memberPlanRef == null)
                    {
                        // Raise error that Member Plan is not defined
                        localContext.Trace("Raise error that Member Plan is not defined.");
                        throw new InvalidPluginExecutionException(OperationStatus.Failed,
                            "Member Plan must be defined for this type of payment.");
                    }

                    // Get the Payee (Contact) reference
                    localContext.Trace("Get the Payee (Contact) reference.");
                    if (entity.Contains("bpa_payeeid") && entity.Attributes["bpa_payeeid"] != null)
                        payeeRef = (EntityReference)entity.Attributes["bpa_payeeid"];
                    else if (preImage != null && preImage.Contains("bpa_payeeid") && preImage.Attributes["bpa_payeeid"] != null)
                        payeeRef = (EntityReference)preImage.Attributes["bpa_payeeid"];

                    // If the Payee (Contact) is null set it via the Member Plan
                    if (payeeRef == null)
                    {
                        localContext.Trace("The Payee (Contact) is null set it via the Member Plan Detail.");
                        Entity member = localContext.OrganizationService.Retrieve("bpa_memberplan", memberPlanRef.Id,
                            new ColumnSet("bpa_contactid"));
                        if (member != null)
                        {
                            payeeRef = (EntityReference)member.Attributes["bpa_contactid"];
                            if (entity.Contains("bpa_contactid") == false)
                                entity.Attributes.Add("bpa_payeeid", payeeRef);
                            else
                                entity.Attributes["bpa_payeeid"] = payeeRef;
                        }
                    }

                    // Go get the fund payment type record
                    localContext.Trace("Find the proper FundPaymentType first.");
                    QueryExpression queryFundPaymentType = new QueryExpression
                    {
                        EntityName = "bpa_fundpaymenttype",
                        ColumnSet = new ColumnSet(true),
                        Criteria =
                        {
                            Filters =
                            {
                                new FilterExpression
                                {
                                    FilterOperator = LogicalOperator.And,
                                    Conditions =
                                    {
                                        new ConditionExpression("bpa_paymenttype", ConditionOperator.Equal, paymentType.Value),
                                        new ConditionExpression("statecode", ConditionOperator.Equal, 0)
                                    }
                                }
                            }
                        }
                    };

                    // Adding Link to the Fund Entity
                    localContext.Trace("Adding Link to the Fund Entity");
                    LinkEntity link = queryFundPaymentType.AddLink("bpa_fund", "bpa_fundid", "bpa_fundid", JoinOperator.Inner);
                    link.EntityAlias = "fund";
                    queryFundPaymentType.Criteria.AddCondition("fund", "bpa_trust", ConditionOperator.Equal, trustRef.Id);
                    queryFundPaymentType.Criteria.AddCondition("fund", "statecode", ConditionOperator.Equal, 0);
                    queryFundPaymentType.PageInfo.ReturnTotalRecordCount = true;
                    EntityCollection fundPaymentTypes = service.RetrieveMultiple(queryFundPaymentType);

                    // No Fund Payment Type found for this Payment, raise error
                    if (fundPaymentTypes == null || fundPaymentTypes.TotalRecordCount == 0)
                        throw new InvalidPluginExecutionException(OperationStatus.Failed,
                            "No Fund has been defined for the Trust and Payment Type.  Please create a Fund Payment Type record for this Trust before creating a Payment record.");

                    // If there is more than one Fund Payment Type defined,
                    // Show an error too
                    if (fundPaymentTypes.Entities.Count > 1)
                    {
                        localContext.Trace("Entity Count: " + fundPaymentTypes.Entities.Count);
                        throw new InvalidPluginExecutionException(OperationStatus.Failed,
                            "More than one Fund has been defined for this Trust and Payment Type.  Please delete all extra Fund Payment Type records for this Trust before creating a Payment record.");
                    }

                    // Found the proper Fund Payment Type.  Populate all of the attributes we need
                    localContext.Trace("Found the Fund Payment Type.");
                    Entity fundPaymentType = fundPaymentTypes.Entities[0];
                    localContext.Trace("Populating Fund Reference.");
                    fundRef = fundPaymentType.Contains("bpa_fundid")
                        ? (EntityReference)fundPaymentType.Attributes["bpa_fundid"]
                        : null;
                    localContext.Trace("Populating Daily Rate.");
                    Money dailyRate = fundPaymentType.Contains("bpa_dailyrate")
                        ? (Money)fundPaymentType.Attributes["bpa_dailyrate"]
                        : null;
                    localContext.Trace("Populating maximum days payable.");
                    int maximumDaysPayable = fundPaymentType.Contains("bpa_maximumdayspayable")
                        ? (int)fundPaymentType.Attributes["bpa_maximumdayspayable"]
                        : 0;

                    // Verify the values passed in are correct
                    Money paymentAmount = new Money();
                    switch (paymentType.Value)
                    {
                        case (int)BpaPaymentPaymentType.AnnualVacation:
                            //In the case of annual vacation payments, there will be an end date or an "up to" date
                            //where the payment is the sum of all the vacation contributions to the work month specified.
                            //The code will get the sum of all the transactions that are:
                            // - Of type "Vacation Contribution" 
                            // - Not associated to a Payment record
                            // - Transaction work month  less than or equal what is passed in from the batch job
                            // - 
                            
                            //EDIT: No code needed here as the Annual Vacation payment will always be created by the Batch request which will set the payment amount.

                            //decimal totalVacationTransactions = TransactionHelper.GetTotalVacationTransactions(service, tracingService, memberPlanRef.Id);

                            //if (entity.Contains("bpa_paymentamount"))
                            //    paymentAmount = entity.GetAttributeValue<Money>("bpa_paymentamount");

                            break;
                        case (int)BpaPaymentPaymentType.InterimVacation: // Interim Vacation

                            // Verify that there is money in the vacation bank for this member
                            localContext.Trace("Retrieving Contact record.");
                            Entity memberPlanDetail = service.Retrieve(PluginHelper.BpaMemberplan, memberPlanRef.Id,
                                new ColumnSet(true));
                            localContext.Trace("Getting the vacation pay.");
                            decimal vacationBank = memberPlanDetail.Contains("bpa_vacationpaybank")
                                ? memberPlanDetail.GetAttributeValue<Money>("bpa_vacationpaybank").Value
                                : 0;

                            if (vacationBank <= 0)
                            {
                                localContext.Trace("No vacation money for this Member.");
                                throw new InvalidPluginExecutionException(OperationStatus.Canceled,
                                    "There is no vacation balance available for this Member.");
                            }

                            // Get the payment amount from either the target or PreImage
                            localContext.Trace("Getting the payment amount.");

                            // Set the payment amount to be the same as the vacation bank
                            paymentAmount.Value = vacationBank;
                            if (entity.Contains("bpa_paymentamount") == false)
                                entity.Attributes.Add("bpa_paymentamount", paymentAmount);
                            else
                                entity.Attributes["bpa_paymentamount"] = paymentAmount;

                            // Set Current activity date is current work month
                            entity["bpa_activitystartdate"] = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1);

                            break;

                        case 922070001: // Parental Leave
                        case 922070002: // Jury Duty
                        case 922070003: // Bereavment Leave

                            // Verify that the user hasn't gone over the maximums
                            localContext.Trace("Getting the number of days the user wants to set.");
                            int numberOfDays = 0;
                            if (entity.Contains("bpa_numberofdays") && entity.Attributes["bpa_numberofdays"] != null)
                                numberOfDays = (int)entity.Attributes["bpa_numberofdays"];
                            else if (preImage != null && preImage.Contains("bpa_numberofdays") && preImage.Attributes["bpa_numberofdays"] != null)
                                numberOfDays = (int)preImage.Attributes["bpa_numberofdays"];

                            // Raise error that they typed in too many days
                            if (numberOfDays > maximumDaysPayable && maximumDaysPayable != 0)
                            {
                                localContext.Trace("The user typed in too many days.");
                                throw new InvalidPluginExecutionException(OperationStatus.Canceled,
                                    "The maximum number of days allowed is " + maximumDaysPayable
                                    + ".  Please choose a value equal to or less than the maximum.");
                            }

                            // If daily rate is null, raise an error
                            if (dailyRate == null)
                            {
                                localContext.Trace("The daily rate is null.");
                                throw new InvalidPluginExecutionException(OperationStatus.Failed,
                                    "The daily rate for this Payment Type is undefined.  Please add a daily rate for this Trust and Payment Type.");
                            }

                            // Calculate the payment amount and add it to the record
                            localContext.Trace("Calculate the payment amount and add it to the record.");
                            Money calcPaymentAmount = dailyRate;
                            calcPaymentAmount.Value = calcPaymentAmount.Value * numberOfDays;

                            paymentAmount = calcPaymentAmount;

                            if (entity.Contains("bpa_paymentamount") == false)
                                entity.Attributes.Add("bpa_paymentamount", calcPaymentAmount);
                            else
                                entity.Attributes["bpa_paymentamount"] = calcPaymentAmount;
                            break;

                    }

                    if (fundRef == null)
                    {
                        // Raise error that the Fund is not defined
                        localContext.Trace("Fund is not defined.");
                        throw new InvalidPluginExecutionException(OperationStatus.Failed, "Fund must be defined.");
                    }

                    // STEP 2: Populate all of the Cheque information
                    // Leave the Cheque number and date blank, as this will be populated when the Payment Batch
                    // is set to complete

                    // Get the Payee Info
                    Entity payee = new Entity();
                    if (payeeRef != null)
                        payee = service.Retrieve("contact", payeeRef.Id, new ColumnSet(true));

                    // Get the Fund Info
                    Entity fund = new Entity();
                    if (fundRef != null)
                        fund = service.Retrieve("bpa_fund", fundRef.Id, new ColumnSet(true));

                    // Populate Member and Cheque details
                    localContext.Trace("Populate Cheque details");
                    // Populate this on closing of the batch entity.Attributes.Add("bpa_chequedate", DateTime.Now.Date);
                    // If the payment type is Pre-Paid Legal or Dental then the amount entered by the user is the one the goes on the cheque.
                    //if (paymentType.Value == 922070005)//Pre-Paid Legal
                    //entity.Attributes.Add("bpa_chequeamount", preImage.GetAttributeValue<Money>("bpa_paymentamount"));
                    //else
                    //    entity.Attributes.Add("bpa_chequeamount", entity.GetAttributeValue<Money>("bpa_paymentamount"));

                    if (preImage != null)
                        entity.Attributes.Add("bpa_chequeamount", preImage.GetAttributeValue<Money>("bpa_paymentamount"));
                    else if (entity.Attributes.Contains("bpa_paymentamount"))
                        entity.Attributes.Add("bpa_chequeamount", entity.GetAttributeValue<Money>("bpa_paymentamount"));


                    // Populate Pay To Details
                    localContext.Trace("Populate the Pay details");
                    entity.Attributes.Add("bpa_paymentto",
                        payee.Contains("fullname") ? (string)payee.Attributes["fullname"] : string.Empty);
                    entity.Attributes.Add("bpa_paymentaddressline1",
                        payee.Contains("address1_line1") ? (string)payee.Attributes["address1_line1"] : string.Empty);
                    entity.Attributes.Add("bpa_paymentaddressline2",
                        payee.Contains("address1_line2") ? (string)payee.Attributes["address1_line2"] : string.Empty);
                    entity.Attributes.Add("bpa_paymentaddressline3",
                        payee.Contains("address1_line3") ? (string)payee.Attributes["address1_line3"] : string.Empty);
                    entity.Attributes.Add("bpa_paymentcity",
                        payee.Contains("address1_city") ? (string)payee.Attributes["address1_city"] : string.Empty);
                    entity.Attributes.Add("bpa_paymentprovince",
                        payee.Contains("address1_stateorprovince")
                            ? (string)payee.Attributes["address1_stateorprovince"]
                            : string.Empty);
                    entity.Attributes.Add("bpa_paymentpostalcode",
                        payee.Contains("address1_postalcode")
                            ? (string)payee.Attributes["address1_postalcode"]
                            : string.Empty);

                    // Populate Bank Details (from Fund)
                    // Found the proper fund.  Add the Bank information
                    localContext.Trace("Found the Fund and adding bank details");
                    entity.Attributes.Add("bpa_bankname",
                        fund.Contains("bpa_bankname") ? (string)fund.Attributes["bpa_bankname"] : string.Empty);
                    entity.Attributes.Add("bpa_bankemail",
                        fund.Contains("bpa_bankemail") ? (string)fund.Attributes["bpa_bankemail"] : string.Empty);
                    entity.Attributes.Add("bpa_banktelephone",
                        fund.Contains("bpa_bankphone") ? (string)fund.Attributes["bpa_bankphone"] : string.Empty);
                    entity.Attributes.Add("bpa_banknumber",
                        fund.Contains("bpa_banknumber") ? (string)fund.Attributes["bpa_banknumber"] : string.Empty);
                    entity.Attributes.Add("bpa_banktransitnumber",
                        fund.Contains("bpa_banktransitnumber")
                            ? (string)fund.Attributes["bpa_banktransitnumber"]
                            : string.Empty);
                    entity.Attributes.Add("bpa_bankaccountnumber",
                        fund.Contains("bpa_bankaccountnumber")
                            ? (string)fund.Attributes["bpa_bankaccountnumber"]
                            : string.Empty);
                    entity.Attributes.Add("bpa_designationnumber",
                        fund.Contains("bpa_designationnumber")
                            ? (string)fund.Attributes["bpa_designationnumber"]
                            : string.Empty);
                    entity.Attributes.Add("bpa_chequeamountspelled", FundHelper.TranslateCurrency(paymentAmount.Value));

                    // STEP 3: See if there is an existing Payment Batch record
                    // If not, create one and link it to this new Payment record and update the total
                    // If there is, just link this record to it and update the total
                    // Adding a new condition for payment method so there is a batch for cheque and another for EFT for the same trust
                    if ((entity.Contains("bpa_paymentbatchid") == false && preImage == null) ||
                        (entity.Contains("bpa_paymentbatchid") == false && preImage != null &&
                         preImage.Contains("bpa_paymentbatchid") == false))
                    {
                        // Get the active Payment Batch Record
                        localContext.Trace("Looking for an active Payment Batch record");
                        QueryExpression batchQuery = new QueryExpression
                        {
                            EntityName = PluginHelper.BpaPaymentBatch, // "bpa_paymentbatch",
                            ColumnSet = new ColumnSet(true)
                        };

                        // Filter against the Trust and payment method
                        batchQuery.Criteria.AddCondition("bpa_trustid", ConditionOperator.Equal, trustRef.Id);
                        batchQuery.Criteria.AddCondition("bpa_paymentmethod", ConditionOperator.Equal, methodOfPayment);

                        // Filter against the Fund
                        batchQuery.Criteria.AddCondition("bpa_fundid", ConditionOperator.Equal, fundRef.Id);
                        batchQuery.Criteria.AddCondition("statuscode", ConditionOperator.Equal, 1); // and is still pending = 1
                        batchQuery.PageInfo.ReturnTotalRecordCount = true;
                        localContext.Trace("Built the query");

                        EntityCollection availableBatches = localContext.OrganizationService.RetrieveMultiple(batchQuery);
                        localContext.Trace("Ran the query");

                        // Define the payment batch reference
                        EntityReference paymentBatchRef = null;

                        // If no Payment Batch was found
                        if (availableBatches.TotalRecordCount == 0)
                        {
                            // No Batch found, go make a new one
                            localContext.Trace("No Batch found.  Going to make one.");
                            Entity paymentBatch = new Entity("bpa_paymentbatch");

                            // Get the currency of the Payment and set that on the Payment Batch record
                            EntityReference currencyRef = new EntityReference();
                            if (entity.Contains("transactioncurrencyid") && entity.Attributes["transactioncurrencyid"] != null)
                                currencyRef = (EntityReference)entity.Attributes["transactioncurrencyid"];
                            else if (preImage != null && preImage.Contains("transactioncurrencyid") && preImage.Attributes["transactioncurrencyid"] != null)
                                currencyRef = (EntityReference)preImage.Attributes["transactioncurrencyid"];
                            paymentBatch.Attributes.Add("transactioncurrencyid", currencyRef);

                            // Add the Trust and Fund attributes
                            paymentBatch.Attributes.Add("bpa_trustid", trustRef);
                            paymentBatch.Attributes.Add("bpa_fundid", fundRef);

                            //Add Method of Payment
                            paymentBatch.Attributes.Add("bpa_paymentmethod", new OptionSetValue(methodOfPayment));

                            // Create the new Payment Batch record
                            Guid paymentBatchId = localContext.OrganizationService.Create(paymentBatch);
                            localContext.Trace("Created Payment Batch record");

                            // Link to the new Payment Batch record
                            localContext.Trace("Going to link to the new Payment Batch");
                            paymentBatchRef = new EntityReference("bpa_paymentbatch", paymentBatchId);
                        }
                        else
                        {
                            // Get the record and link the Payment to it
                            localContext.Trace("Going to link to the existing Payment Batch");
                            paymentBatchRef = new EntityReference("bpa_paymentbatch", availableBatches.Entities[0].Id);
                        }

                        // Get the payment and link it to the Payment Batch field
                        entity.Attributes["bpa_paymentbatchid"] = paymentBatchRef;

                        if (context.MessageName == PluginHelper.Create)
                        {
                            if (!entity.Contains("bpa_name"))
                                entity["bpa_name"] = Guid.NewGuid().ToString();
                        }
                    }
                }
                else
                {
                    // Ready for Processing flag is set to false
                    // Clear the Cheque Information 
                    localContext.Trace("Clear the cheque details");
                    entity.Attributes.Add("bpa_chequenumber", null);
                    entity.Attributes.Add("bpa_chequedate", null);
                    entity.Attributes.Add("bpa_chequeamount", null);
                    entity.Attributes.Add("bpa_chequeamountspelled", string.Empty);

                    entity.Attributes.Add("bpa_paymentto", string.Empty);
                    entity.Attributes.Add("bpa_paymentaddressline1", string.Empty);
                    entity.Attributes.Add("bpa_paymentaddressline2", string.Empty);
                    entity.Attributes.Add("bpa_paymentaddressline3", string.Empty);
                    entity.Attributes.Add("bpa_paymentcity", string.Empty);
                    entity.Attributes.Add("bpa_paymentprovince", string.Empty);
                    entity.Attributes.Add("bpa_paymentpostalcode", string.Empty);

                    entity.Attributes.Add("bpa_bankname", string.Empty);
                    entity.Attributes.Add("bpa_bankemail", string.Empty);
                    entity.Attributes.Add("bpa_banktelephone", string.Empty);
                    entity.Attributes.Add("bpa_banknumber", string.Empty);
                    entity.Attributes.Add("bpa_banktransitnumber", string.Empty);
                    entity.Attributes.Add("bpa_bankaccountnumber", string.Empty);
                    entity.Attributes.Add("bpa_designationnumber", string.Empty);

                }
            }
            else
            {
                //Payment is not ready for processing. Need to verify that the selected Payment Type is associated with the Fund

                localContext.Trace("Get the Payment Type reference.");
                if (entity.Contains("bpa_paymenttype") && entity.Attributes["bpa_paymenttype"] != null)
                    paymentType = (OptionSetValue)entity.Attributes["bpa_paymenttype"];
                else if (preImage.Contains("bpa_paymenttype") && preImage.Attributes["bpa_paymenttype"] != null)
                    paymentType = (OptionSetValue)preImage.Attributes["bpa_paymenttype"];

                // Get the Plan reference
                localContext.Trace("Get the Plan reference.");
                if (entity.Contains("bpa_planid") && entity.Attributes["bpa_planid"] != null)
                    planRef = (EntityReference)entity.Attributes["bpa_planid"];
                else if (preImage.Contains("bpa_planid") && preImage.Attributes["bpa_planid"] != null)
                    planRef = (EntityReference)preImage.Attributes["bpa_planid"];

                // If the Trust is null set it via the Plan
                localContext.Trace("The Trust is null set it via the Plan.");
                Entity plan = localContext.OrganizationService.Retrieve("bpa_sector", planRef.Id,
                    new ColumnSet("bpa_trust"));
                if (plan != null)
                {
                    trustRef = (EntityReference)plan.Attributes["bpa_trust"];
                    if (entity.Contains("bpa_trustid") == false)
                        entity.Attributes.Add("bpa_trustid", trustRef);
                    else
                        entity.Attributes["bpa_trustid"] = trustRef;
                }

                // Go get the fund payment type record
                localContext.Trace("Find the proper FundPaymentType first.");
                QueryExpression queryFundPaymentType = new QueryExpression
                {
                    EntityName = "bpa_fundpaymenttype",
                    ColumnSet = new ColumnSet(true),
                    Criteria =
                        {
                            Filters =
                            {
                                new FilterExpression
                                {
                                    FilterOperator = LogicalOperator.And,
                                    Conditions =
                                    {
                                        new ConditionExpression("bpa_paymenttype", ConditionOperator.Equal, paymentType.Value),
                                        new ConditionExpression("statecode", ConditionOperator.Equal, 0)
                                    }
                                }
                            }
                        }
                };

                // Adding Link to the Fund Entity
                localContext.Trace("Adding Link to the Fund Entity");
                LinkEntity link = queryFundPaymentType.AddLink("bpa_fund", "bpa_fundid", "bpa_fundid", JoinOperator.Inner);
                link.EntityAlias = "fund";
                queryFundPaymentType.Criteria.AddCondition("fund", "bpa_trust", ConditionOperator.Equal, trustRef.Id);
                queryFundPaymentType.Criteria.AddCondition("fund", "statecode", ConditionOperator.Equal, 0);
                queryFundPaymentType.PageInfo.ReturnTotalRecordCount = true;
                EntityCollection fundPaymentTypes = service.RetrieveMultiple(queryFundPaymentType);

                // No Fund Payment Type found for this Payment, raise error
                if (fundPaymentTypes == null || fundPaymentTypes.TotalRecordCount == 0)
                    throw new InvalidPluginExecutionException(OperationStatus.Failed,
                        "No Fund has been defined for the Trust and Payment Type.  Please create a Fund Payment Type record for this Trust before creating a Payment record.");

            }
        }
    }
}