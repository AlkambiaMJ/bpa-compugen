﻿#region

using System;
using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using BPA.Benefit.Management.CRM.Plugins.Helper;

#endregion

namespace BPA.Benefit.Management.CRM.Plugins
{
    /// <summary>
    ///     PaymentBatchOnSetStatePre PlugIn.
    /// </summary>
    public class PaymentBatchOnSetStatePre : Plugin
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="PaymentBatchOnSetStatePre" /> class.
        /// </summary>
        public PaymentBatchOnSetStatePre()
            : base(typeof(PaymentBatchOnSetStatePre))
        {
            RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(20, "SetState",
                "bpa_paymentbatch", ExecutePaymentBatchOnSetStatePre));
            RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(20, "SetStateDynamicEntity",
                "bpa_paymentbatch", ExecutePaymentBatchOnSetStatePre));

            // Note : you can register for more events here if this plugin is not specific to an individual entity and message combination.
            // You may also need to update your RegisterFile.crmregister plug-in registration file to reflect any change.
        }

        /// <summary>
        ///     Executes the plug-in.
        /// </summary>
        /// <param name="localContext">
        ///     The <see cref="Plugin.LocalPluginContext" /> which contains the
        ///     <see cref="IPluginExecutionContext" />,
        ///     <see cref="IOrganizationService" />
        ///     and <see cref="ITracingService" />
        /// </param>
        /// <remarks>
        ///     For improved performance, Microsoft Dynamics CRM caches plug-in instances.
        ///     The plug-in's Execute method should be written to be stateless as the constructor
        ///     is not called for every invocation of the plug-in. Also, multiple system threads
        ///     could execute the plug-in at the same time. All per invocation state information
        ///     is stored in the context. This means that you should not use global variables in plug-ins.
        /// </remarks>
        protected void ExecutePaymentBatchOnSetStatePre(LocalPluginContext localContext)
        {
            // If local context not passed in, raise error
            if (localContext == null)
                throw new ArgumentNullException($"Local context not passed in.");

            // Create the context variables
            IPluginExecutionContext context = localContext.PluginExecutionContext;
            IOrganizationService service = localContext.OrganizationService;
            ITracingService tracingService = localContext.TracingService;

            // If EntityReference is not passed in, raise error
            tracingService.Trace("If Target is not passed in, raise error");
            if (!context.InputParameters.Contains(PluginHelper.EntityMoniker) ||
                !(context.InputParameters[PluginHelper.EntityMoniker] is EntityReference))
                throw new ArgumentNullException($"EntityReference is not passed in.");

            // Create the EntityReference
            tracingService.Trace("Create the EntityReference");
            EntityReference targetEntity = (EntityReference)context.InputParameters[PluginHelper.EntityMoniker];
            if (targetEntity == null)
                throw new ArgumentNullException($"EntityReference is not defined.");

            OptionSetValue state = (OptionSetValue)context.InputParameters["State"];
            OptionSetValue status = (OptionSetValue)context.InputParameters["Status"];

            // If status is Cancelled
            if (status.Value == (int)PaymentBatchStatus.Cancelled)
            {
                // Loop through all of the related Active Payments and set them to Cancelled
                tracingService.Trace("Payment Batch status is Cancelled.");
                QueryExpression queryExpression = new QueryExpression
                {
                    EntityName = "bpa_payment",
                    ColumnSet = new ColumnSet(true),
                    Criteria =
                    {
                        Filters =
                        {
                            new FilterExpression
                            {
                                FilterOperator = LogicalOperator.And,
                                Conditions =
                                {
                                    new ConditionExpression("bpa_paymentbatchid", ConditionOperator.Equal, context.PrimaryEntityId),
                                    new ConditionExpression("statecode", ConditionOperator.Equal, 0)
                                }
                            }
                        }
                    }
                };

                EntityCollection payments = service.RetrieveMultiple(queryExpression);

                // For each Payment record linked, set it to cancelled
                tracingService.Trace("For each Payment record linked, set it to cancelled");
                foreach (Entity payment in payments.Entities)
                {
                    // Deactivate the Payment record
                    // This will trigger the individual Payments to clean themselves up
                    tracingService.Trace("Deactivating Payment as Cancelled: " + payment.Id.ToString());
                    PluginHelper.DeactivateRecord(service, PluginHelper.BpaPayment, payment.Id, (int)BpaPaymentStatusCode.Cancelled); // Cancelled status
                }
            }
            else if(status.Value == (int)PaymentBatchStatus.Completed)
            {
                // Verify all Active Payment record have the "Ready for Processing" flag set 
                tracingService.Trace("Payment Batch status is Completed.");
                QueryExpression queryPaymentReady = new QueryExpression
                {
                    EntityName = "bpa_payment",
                    ColumnSet = new ColumnSet("bpa_paymentid"),
                    Criteria =
                    {
                        Filters =
                        {
                            new FilterExpression
                            {
                                FilterOperator = LogicalOperator.And,
                                Conditions =
                                {
                                    new ConditionExpression("bpa_paymentbatchid", ConditionOperator.Equal, context.PrimaryEntityId),
                                    new ConditionExpression("statecode", ConditionOperator.Equal, 0),
                                    new ConditionExpression("bpa_readyforprocessing", ConditionOperator.Equal, false)
                                }
                            }
                        }
                    }
                };

                queryPaymentReady.PageInfo.ReturnTotalRecordCount = true;
                EntityCollection paymentsNotReady = service.RetrieveMultiple(queryPaymentReady);
                if (paymentsNotReady.TotalRecordCount > 0)
                {
                    throw new InvalidPluginExecutionException(OperationStatus.Canceled,
                        "There are still Payments in the batch that are not set to 'Ready for Processing'.  Set all Payments ready before closing the Payment Batch.");
                }

                // Get the last cheque number from the Fund record
                tracingService.Trace("Getting the last cheque number.");
                Entity paymentBatch = service.Retrieve(targetEntity.LogicalName, targetEntity.Id, new ColumnSet(true));
                EntityReference fundRef = (EntityReference)paymentBatch.Attributes["bpa_fundid"];
                Entity fund = service.Retrieve(fundRef.LogicalName, fundRef.Id, new ColumnSet(true));

                int lastChequeNumber = 1;
                int minimumChequeNumber = 1;
                int maximumChequeNumber = 999999;
                if (fund.Attributes.Contains("bpa_lastchequenumber") == true && fund.Attributes["bpa_lastchequenumber"] != null)
                    lastChequeNumber = (int)fund.Attributes["bpa_lastchequenumber"];
                if (fund.Attributes.Contains("bpa_minumumchequenumber") == true && fund.Attributes["bpa_minumumchequenumber"] != null)
                    minimumChequeNumber = (int)fund.Attributes["bpa_minumumchequenumber"];
                if (fund.Attributes.Contains("bpa_maximumchequenumber") == true && fund.Attributes["bpa_maximumchequenumber"] != null)
                    maximumChequeNumber = (int)fund.Attributes["bpa_maximumchequenumber"];

                // Loop though all Payment records
                tracingService.Trace("Getting Payment records to loop though.");
                QueryExpression queryPayment = new QueryExpression
                {
                    EntityName = "bpa_payment",
                    ColumnSet = new ColumnSet("bpa_chequedate", "bpa_chequenumber"),
                    Criteria =
                    {
                        Filters =
                        {
                            new FilterExpression
                            {
                                FilterOperator = LogicalOperator.And,
                                Conditions =
                                {
                                    new ConditionExpression("bpa_paymentbatchid", ConditionOperator.Equal, context.PrimaryEntityId),
                                    new ConditionExpression("statecode", ConditionOperator.Equal, 0),
                                    new ConditionExpression("bpa_readyforprocessing", ConditionOperator.Equal, true)
                                }
                            }
                        }
                    }
                };

                EntityCollection payments = service.RetrieveMultiple(queryPayment);
                tracingService.Trace("Got the list of Payments to Complete.");

                foreach (Entity payment in payments.Entities)
                {
                    // Set the Cheque date and cheque number
                    tracingService.Trace("Updating Payment: " + payment.Id.ToString());
                    payment.Attributes["bpa_chequedate"] = new DateTime(DateTime.Today.Year, DateTime.Today.Month, DateTime.Today.Day);
                    payment.Attributes["bpa_chequenumber"] = lastChequeNumber.ToString().PadLeft(6,'0');
                    payment.Attributes["statuscode"] = new OptionSetValue((int)BpaPaymentStatusCode.Processing);
                    lastChequeNumber++;
                    service.Update(payment);

                    // Set the payment to complete
                    tracingService.Trace("Deactivating Payment as Complete: " + payment.Id.ToString());
                    PluginHelper.DeactivateRecord(service, PluginHelper.BpaPayment, payment.Id, (int)BpaPaymentStatusCode.Completed); // Completed status

                    // If we have reached the last cheque number, reset to 1
                    if (lastChequeNumber > maximumChequeNumber)
                        lastChequeNumber = minimumChequeNumber;
                }

                // Save the new last cheque number
                tracingService.Trace("Updating Fund with new Last Cheque number.");
                fund.Attributes["bpa_lastchequenumber"] = lastChequeNumber;
                service.Update(fund);
            }
        }
    }
}