﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using BPA.Benefit.Management.CRM.Plugins.Helper;
using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Sdk.Messages;
using System.ServiceModel;

#endregion

namespace BPA.Benefit.Management.CRM.Plugins
{
    /// <summary>
    ///     SubmissionOnCreatePost PlugIn.
    /// </summary>
    public class SubmissionOnUpdatePost : Plugin
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="SubmissionOnUpdatePost" /> class.
        /// </summary>
        public SubmissionOnUpdatePost(string unsecureString, string secureString)
            : base(typeof(SubmissionOnUpdatePost), unsecureString, secureString)
        {
            RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(40, "Update",
                PluginHelper.BpaSubmission, ExecutePostSubmissionUpdate));

            // Note : you can register for more events here if this plugin is not specific to an individual entity and message combination.
            // You may also need to update your RegisterFile.crmregister plug-in registration file to reflect any change.
        }

        /// <summary>
        ///     Executes the plug-in.
        /// </summary>
        /// <param name="localContext">
        ///     The <see cref="Plugin.LocalPluginContext" /> which contains the
        ///     <see cref="IPluginExecutionContext" />,
        ///     <see cref="IOrganizationService" />
        ///     and <see cref="ITracingService" />
        /// </param>
        /// <remarks>
        ///     For improved performance, Microsoft Dynamics CRM caches plug-in instances.
        ///     The plug-in's Execute method should be written to be stateless as the constructor
        ///     is not called for every invocation of the plug-in. Also, multiple system threads
        ///     could execute the plug-in at the same time. All per invocation state information
        ///     is stored in the context. This means that you should not use global variables in plug-ins.
        /// </remarks>

        protected void ExecutePostSubmissionUpdate(LocalPluginContext localContext)
        { 
            // If local context not passed in, raise error
            if (localContext == null)
                throw new ArgumentNullException($"Local context not passed in.");

            // Create the context variables
            IPluginExecutionContext context = localContext.PluginExecutionContext;
            ITracingService tracingService = localContext.TracingService;
            IOrganizationService service = localContext.OrganizationService;
            IServiceProvider serviceProvider = localContext.ServiceProvider;

            tracingService.Trace($@"SubmissionOnUpdatePost - Start: {DateTime.Now}");


            // If Target is not passed in, raise error
            tracingService.Trace("If Target is not passed in, raise error");
            if (!context.InputParameters.Contains(PluginHelper.Target) ||
                !(context.InputParameters[PluginHelper.Target] is Entity))
                throw new ArgumentNullException($"Target is not of type Entity.");

            // Create the target entity & preImage
            tracingService.Trace("Create the target entity & preImage");
            Entity entity = (Entity)context.InputParameters[PluginHelper.Target];
            Entity preImageEntity = context.PreEntityImages != null &&
                                    context.PreEntityImages.Contains(PluginHelper.PreImageAlias)
                ? context.PreEntityImages[PluginHelper.PreImageAlias] : null;
            if (preImageEntity == null)
                throw new ArgumentNullException($"preImage entity is not defined.");
            Entity postImageEntity = context.PostEntityImages != null &&
                                    context.PostEntityImages.Contains(PluginHelper.PostImageAlias)
                ? context.PostEntityImages[PluginHelper.PostImageAlias] : null;
            if (postImageEntity == null)
                throw new ArgumentNullException($"postImage entity is not defined.");

            // Get Default Funds from TRUST
            List<Entity> defaultFundList = new List<Entity>();

            // Find and Get Default Welfare Fund
            EntityReference defaultFundRef = null;

            //Get Execute user, Get Crm Admin User and create IORganizationService for CrM ADmin 
            Guid executeUser = context.UserId;
            //string crmAdminUserId = PluginConfiguration[PluginHelper.ConfigCrmAdmin];
            string crmAdminUserId = string.Empty;
            try
            {
                crmAdminUserId = PluginConfiguration[PluginHelper.ConfigCrmAdmin];
            }
            catch (Exception ex)
            {
                // Get value from Database
                crmAdminUserId = ConfigurationHelper.FetchValueId(service, PluginHelper.ConfigCrmAdmin);
            }
            if (string.IsNullOrEmpty(crmAdminUserId))
            {
                tracingService.Trace("ERROR FROM SubmissionOnUpdatePost Plugin - CRM Admin USer Could not Found.");
                throw new InvalidPluginExecutionException(OperationStatus.Failed, $@"The given key was not present in the dictionary. Please contact BPA Administrator");
            }
            IOrganizationServiceFactory serviceFactory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
            IOrganizationService serviceAdmin = serviceFactory.CreateOrganizationService(new Guid(crmAdminUserId));
            OptionSetValue submissiontype = postImageEntity.GetAttributeValue<OptionSetValue>("bpa_submissiontype");

            if (postImageEntity.Contains("bpa_trustid"))
            {
                tracingService.Trace($"Find out default fund");
                Entity defaultWelFareFund = null;

                // Fetch all default funds
                EntityReference trustRef = postImageEntity.Contains("bpa_trustid") ? postImageEntity.GetAttributeValue<EntityReference>("bpa_trustid") : null;
                if (trustRef != null)
                    defaultFundList = TrustHelper.FetchAllDefaultFunds(service, tracingService, trustRef.Id);

                // Get Plan from submission
                EntityReference planRef = postImageEntity.Contains("bpa_plan") ? postImageEntity.GetAttributeValue<EntityReference>("bpa_plan") : null;
                if (planRef != null)
                {
                    tracingService.Trace($"Inside Plan Infor");

                    // Get plan Info
                    Entity planInfo = PlanHelper.FetchPlanInformation(service, tracingService, planRef.Id);
                    bool isBenefitPlan = planInfo.Contains("bpa_benefitplan") ? planInfo.GetAttributeValue<bool>("bpa_benefitplan") : false;
                    bool isPensionPlan = planInfo.Contains("bpa_pensionplan") ? planInfo.GetAttributeValue<bool>("bpa_pensionplan") : false;
                    tracingService.Trace($"Benefit Plan {isBenefitPlan}, Pension Plan {isPensionPlan}");

                    if (!isBenefitPlan && !isPensionPlan)
                        throw new InvalidPluginExecutionException(OperationStatus.Failed, $@"Plan must be either 'Benefit Plan' or 'Pension Plan'. Please contact BPA Administrator.");

                    if (defaultFundList != null)
                    {
                        if (isBenefitPlan)
                            defaultWelFareFund = defaultFundList.Where(x => x.GetAttributeValue<OptionSetValue>("bpa_fundtype").Value == (int)BpaFundbpaFundType.Welfare).FirstOrDefault();
                        else if (isPensionPlan)
                            defaultWelFareFund = defaultFundList.Where(x => x.GetAttributeValue<OptionSetValue>("bpa_fundtype").Value == (int)BpaFundbpaFundType.Pension).FirstOrDefault();

                        if (defaultWelFareFund != null)
                            defaultFundRef = new EntityReference(PluginHelper.BpaFund, defaultWelFareFund.Id);
                    }
                }
                else if (submissiontype.Value == (int)SubmissionSubmissionType.RetireeMonthlySubmission)
                {
                    tracingService.Trace($"Plan is Empty");
                    if (defaultFundList != null)
                    {
                        defaultWelFareFund = defaultFundList.Where(x => x.GetAttributeValue<OptionSetValue>("bpa_fundtype").Value == (int)BpaFundbpaFundType.Welfare).FirstOrDefault();

                        if (defaultWelFareFund != null)
                            defaultFundRef = new EntityReference(PluginHelper.BpaFund, defaultWelFareFund.Id);
                    }
                }
            }
            // Set default fund if null
            if (defaultFundRef == null)
            {
                throw new InvalidPluginExecutionException(OperationStatus.Failed, $@"Trust does not have default fund assigned. Please contact BPA Administrator.");
            }

            if (context.Depth < 2)
            {
                tracingService.Trace($"Context.Depth = {context.Depth}");

                //this method only fire when Submission Status = NSF
                tracingService.Trace($"Calling FetchAndUpdateContribution Method");
                FetchAndUpdateContribution(context, service, serviceAdmin, tracingService, defaultFundRef);

                //this method only fire when entity contains attribute 'bpa_submissionpaid"
                tracingService.Trace($"Calling UpdateStatusVarianceAndClosingBalance Method");
                UpdateStatusVarianceAndClosingBalance(serviceProvider, service, serviceAdmin, tracingService, context.UserId,
                    entity, postImageEntity, defaultFundRef, preImageEntity);
                tracingService.Trace("Completed UpdateStatusVarianceAndClosingBalance");
            }
            //if (context.Depth < 3)
            //{
            //    tracingService.Trace("Before CreateTransactionsAndContributions");

            //    // This is just for creating all TRANSACTIONS AND CONTRIBUTIONS
            //    CreateTransactionsAndContributions(localContext, serviceAdmin, entity, postImageEntity, defaultFundRef, defaultFundList, preImageEntity);
            //    tracingService.Trace("After CreateTransactionsAndContributions");
            //}

            tracingService.Trace($@"SubmissionOnUpdatePost - End: {DateTime.Now}");
            tracingService.Trace($@"=========================={context.Depth}======================== ");

        }

        protected void UpdateContributionsAmount(IOrganizationService service, IOrganizationService serviceAdmin, ITracingService tracingService,
            Entity entity, Entity postImageEntity, int submissionStatus, EntityReference defaultFundRef)
        {
            tracingService.Trace($"Inside UpdateContributionsAmount");
            if (!entity.Attributes.Contains("bpa_submissionpaid") || entity.Attributes["bpa_submissionpaid"] == null ||
                !postImageEntity.Attributes.Contains("bpa_submissionamount") ||
                postImageEntity.Attributes["bpa_submissionamount"] == null)
            {

                tracingService.Trace($"bpa_submissionpaid or bpa_submissionamount does not contains");
                return;
            }

            OptionSetValue submissiontype = postImageEntity.GetAttributeValue<OptionSetValue>("bpa_submissiontype");
            if (submissiontype.Value == (int)SubmissionSubmissionType.RetireeMonthlySubmission)
                return;

            decimal submissionamount = ((Money)postImageEntity.Attributes["bpa_submissionamount"]).Value;
            tracingService.Trace($"submissionamount = {submissionamount.ToString()}");
            decimal submissionpaid = ((Money)entity.Attributes["bpa_submissionpaid"]).Value;
            tracingService.Trace($"submissionpaid = {submissionpaid.ToString()}");
            decimal submissionVariance = submissionamount - submissionpaid;
            tracingService.Trace($"submissionVariance = {submissionVariance.ToString()}");

            Money submissionDue = postImageEntity.Attributes.Contains("bpa_submissionamount")
                ? postImageEntity.GetAttributeValue<Money>("bpa_submissionamount")
                : new Money(0);
            tracingService.Trace($"submissionDue = {submissionDue.Value.ToString()}");

            Money submissionPaid = entity.Attributes.Contains("bpa_submissionpaid")
                ? entity.GetAttributeValue<Money>("bpa_submissionpaid")
                : new Money(0);
            tracingService.Trace($"submissionPaid = {submissionPaid.Value.ToString()}");

            Money totalSubmissionDue = postImageEntity.Attributes.Contains("bpa_totalsubmissiondue")
                ? postImageEntity.GetAttributeValue<Money>("bpa_totalsubmissiondue")
                : new Money(0);
            tracingService.Trace($"totalSubmissionDue = {totalSubmissionDue.Value.ToString()}");
            OptionSetValue submissionType = postImageEntity.Contains("bpa_submissiontype")
                ? postImageEntity.GetAttributeValue<OptionSetValue>("bpa_submissiontype")
                : new OptionSetValue(0);
            tracingService.Trace($"submissionType = {submissionType.Value.ToString()}");

            decimal currentVariance = totalSubmissionDue.Value - submissionPaid.Value;
            tracingService.Trace($"currentVariance = {currentVariance.ToString()}");

            EntityCollection contributions = ContributionHelper.FetchAllContributions(serviceAdmin, tracingService, entity.Id, false);
            if (contributions == null && contributions.Entities.Count <= 0)
            {
                tracingService.Trace($"No Contribution record found so program return ");
                return;
            }
            bool alreadyPaidHnW = false;
            foreach (Entity contribution in contributions.Entities)
            {
                // Get all values from Contribution
                Guid contributionId = contribution.Id;
                decimal contributionAmount = 0;
                decimal contributionPaid = 0;
                EntityReference fund = null;

                if (contribution.Attributes.Contains("bpa_contributionamount"))
                    contributionAmount = contribution.GetAttributeValue<Money>("bpa_contributionamount").Value;
                if (contribution.Attributes.Contains("bpa_fund"))
                    fund = contribution.GetAttributeValue<EntityReference>("bpa_fund");

                // Checking all Status
                if (currentVariance == 0)
                {
                    tracingService.Trace($"Inside currentVariance = 0");

                    if ((fund != null) && (fund.Id == defaultFundRef.Id) && !alreadyPaidHnW) // && (contributionAmount != 0))
                    {
                        contributionPaid = contributionAmount + (-1 * submissionVariance);
                        alreadyPaidHnW = true;
                    }
                    else
                        contributionPaid = submissionType.Value == (int)SubmissionSubmissionType.EmployerMoneyOnly ? submissionPaid.Value : contributionAmount;

                    ContributionHelper.UpdateContribution(serviceAdmin, contributionId, contributionPaid);
                }
                else if (submissionStatus == (int)SubmissionStatus.Nsf)
                {
                    tracingService.Trace($"Inside Status = NSF");
                    contributionPaid = 0;
                    ContributionHelper.UpdateContribution(serviceAdmin, contributionId, contributionPaid);
                }
                else if ((currentVariance < 0) || (currentVariance > 0))
                {
                    tracingService.Trace("Inside  ((currentVariance < 0) || (currentVariance > 0))");
                    if ((fund != null) && (fund.Id == defaultFundRef.Id) && !alreadyPaidHnW) // && (contributionAmount != 0))
                    {
                        contributionPaid = contributionAmount + -1 * submissionVariance;
                        alreadyPaidHnW = true;
                    }
                    else
                        contributionPaid = submissionType.Value == (int)SubmissionSubmissionType.EmployerMoneyOnly ? submissionPaid.Value : contributionAmount;

                    ContributionHelper.UpdateContribution(serviceAdmin, contributionId, contributionPaid);
                }
            }
            tracingService.Trace("Completed UpdateContributionsAmount Method");
        }

        void UpdateStatusVarianceAndClosingBalance(IServiceProvider serviceProvider, IOrganizationService service, IOrganizationService serviceAdmin,
            ITracingService tracingService, Guid userId, Entity entity, Entity postImageEntity,
            EntityReference defaultFundRef, Entity preImageEntity)
        {
            tracingService.Trace("Inside UpdateStatusVarianceAndClosingBalance.");

            OptionSetValue paymentType = new OptionSetValue(0);
            if (entity.Contains("bpa_paymentmethod"))
                paymentType = entity.GetAttributeValue<OptionSetValue>("bpa_paymentmethod");
            else if (postImageEntity.Contains("bpa_paymentmethod"))
                paymentType = postImageEntity.GetAttributeValue<OptionSetValue>("bpa_paymentmethod");

            EntityReference deposit = null;
            Guid depositId = Guid.Empty;
            if (entity.Contains("bpa_depositid"))
                deposit = entity.GetAttributeValue<EntityReference>("bpa_depositid");
            tracingService.Trace("Triggering Deposit submission roll up");

            //Trigger Roll up in deposit
            if ((paymentType.Value != (int)SubmissionPaymentType.NoPayment) && (deposit != null))
                PluginHelper.TriggerRollup(service, tracingService, PluginHelper.BpaDeposit, deposit.Id, "bpa_submissiontotal");
            tracingService.Trace("Ending Deposit submission roll up");

            if (!entity.Attributes.Contains("bpa_submissionpaid")) return;


            Money submissionPaid = entity.Attributes.Contains("bpa_submissionpaid")
                ? entity.GetAttributeValue<Money>("bpa_submissionpaid")
                : new Money(0);
            tracingService.Trace("After value - 1");
            if (submissionPaid == null)
                return;
            Money submissionDue = postImageEntity.Attributes.Contains("bpa_submissionamount")
                ? postImageEntity.GetAttributeValue<Money>("bpa_submissionamount")
                : new Money(0);
            tracingService.Trace("After value - 2");
            Money totalSubmissionDue = postImageEntity.Attributes.Contains("bpa_totalsubmissiondue")
                ? postImageEntity.GetAttributeValue<Money>("bpa_totalsubmissiondue")
                : new Money(0);
            tracingService.Trace("After value - 3");
            OptionSetValue submissionstatus = postImageEntity.Attributes.Contains("bpa_submissionstatus")
                ? postImageEntity.GetAttributeValue<OptionSetValue>("bpa_submissionstatus")
                : null;
            tracingService.Trace("After value - 4");
            EntityReference employerRef = postImageEntity.Attributes.Contains("bpa_accountagreementid")
                ? postImageEntity.GetAttributeValue<EntityReference>("bpa_accountagreementid")
                : null;
            tracingService.Trace("After value - 5");
            DateTime workMonth = postImageEntity.Attributes.Contains("bpa_submissiondate")
                ? postImageEntity.GetAttributeValue<DateTime>("bpa_submissiondate")
                : DateTime.MinValue;
            tracingService.Trace("After value - 5");
            //OptionSetValue submissionType = imageEntity.Attributes.Contains("new_submissiontype") ? imageEntity.GetAttributeValue<OptionSetValue>("new_submissiontype") : new OptionSetValue(0);
            tracingService.Trace("After value - 7");
            OptionSetValue submissionTypeNew = postImageEntity.Attributes.Contains("bpa_submissiontype")
                ? postImageEntity.GetAttributeValue<OptionSetValue>("bpa_submissiontype")
                : new OptionSetValue(0);
            tracingService.Trace("After value - 8");

            // Validate if Pre Image already contains Deposit Id
            EntityReference depositRef = postImageEntity.Attributes.Contains("bpa_depositid")
                ? postImageEntity.GetAttributeValue<EntityReference>("bpa_depositid")
                : null;

            tracingService.Trace("After value - 9");
            tracingService.Trace("After Getting all values.");
            DateTime CreatedOn = DateTime.Now;

            decimal submissionVariance = submissionDue.Value - submissionPaid.Value;
            decimal currentVariance = totalSubmissionDue.Value - submissionPaid.Value;
            tracingService.Trace("Before UpdateContributionsAmount");

            //Update contribution amount
            UpdateContributionsAmount(service, serviceAdmin, tracingService, entity, postImageEntity,
                (int)SubmissionStatus.PendingDeposit, defaultFundRef);
            tracingService.Trace("After UpdateContributionsAmount");

            tracingService.Trace("After Deposit FETCH/Created");
            // Set Value
            Entity submission = new Entity(PluginHelper.BpaSubmission) { Id = entity.Id };
            submission["bpa_submissionvariance"] = new Money(submissionVariance);
            submission["bpa_closingvariance"] = new Money(currentVariance);
            DateTime paymentReceivedDate = DateTime.MinValue;
            paymentReceivedDate = entity.Contains("bpa_paymentrecieveddate") ? entity.GetAttributeValue<DateTime>("bpa_paymentrecieveddate") : DateTime.MinValue;
            if (paymentReceivedDate == DateTime.MinValue)
                submission["bpa_paymentrecieveddate"] = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);


            service.Update(submission);
            tracingService.Trace("Submission Successfully Updated");

            tracingService.Trace("Account Update will start");

            // Update Total Variance on Account/Employer Form 
            decimal OpeningBalance = SubmissionHelper.GetOpeningVariance(service, tracingService, employerRef.Id, CreatedOn);

            AccountAgreementHelper.UpdateAccountVariance(service, tracingService, employerRef.Id, OpeningBalance);
            tracingService.Trace("Account Update Successfully Completed.");

            tracingService.Trace("Completed method UpdateStatusVarianceAndClosingBalance");
        }

        //Execute Contribution 
        protected void FetchAndUpdateContribution(IPluginExecutionContext context, IOrganizationService service, IOrganizationService serviceAdmin,
            ITracingService tracing, EntityReference DefaultFund)
        {
            tracing.Trace("Inside FetchAndUpdateContribution Method");
            Entity target = (Entity)context.InputParameters["Target"];
            Entity postImageEntity = context.PostEntityImages != null &&
                                     context.PostEntityImages.Contains(PluginHelper.PostImageAlias)
                ? context.PostEntityImages[PluginHelper.PostImageAlias] : null;

            //Get User Id and Business Unit Id
            Guid CurrentUserId = context.InitiatingUserId;
            Guid BusinessUnitId = context.BusinessUnitId;

            if (target.LogicalName.Equals("bpa_submission"))
            {
                try
                {
                    OptionSetValue submissionStatus = null;
                    decimal submissionamount = 0;
                    decimal submissionpaid = 0;
                    tracing.Trace("Before getting values");
                    if (postImageEntity.Attributes.Contains("bpa_submissionamount") && postImageEntity.Attributes["bpa_submissionamount"] != null
                        && postImageEntity.Attributes.Contains("bpa_submissionpaid") && postImageEntity.Attributes["bpa_submissionpaid"] != null)
                    {
                        submissionamount = ((Money)postImageEntity.Attributes["bpa_submissionamount"]).Value;
                        submissionpaid = ((Money)postImageEntity.Attributes["bpa_submissionpaid"]).Value;
                        submissionStatus = (OptionSetValue)postImageEntity.Attributes["bpa_submissionstatus"];
                    }
                    tracing.Trace("After getting values");

                    if (submissionStatus != null && (submissionStatus.Value == (int)SubmissionStatus.Nsf))
                    {
                        tracing.Trace("Inside submissionStatus is not null and Status = NSF");

                        EntityReference NSFFund = null;
                        decimal nsffee = 0;
                        if (postImageEntity.Attributes.Contains("bpa_accountagreementid"))
                        {
                            EntityReference employer =
                                postImageEntity.GetAttributeValue<EntityReference>("bpa_accountagreementid");

                            //Find NSF Fund and NSF Fee FROM TRUST
                            tracing.Trace("Find NSF Fund and Fee from Trust");
                            Entity NSFFundEntity = AccountAgreementHelper.FetchNsfFundDetailByEmployerId(service, employer.Id);

                            //NSF Fund
                            if (NSFFundEntity.Attributes.Contains("bpa_nsffund"))
                                NSFFund = NSFFundEntity.GetAttributeValue<EntityReference>("bpa_nsffund");
                            else
                                NSFFund = new EntityReference("bpa_nsffund", Guid.Empty);
                            tracing.Trace($"NSF Fund {NSFFund.Id}");

                            nsffee = NSFFundEntity.GetAttributeValue<decimal>("bpa_nsffee");
                            tracing.Trace($"NSF Fee {nsffee.ToString()}");
                        }

                        //CREATE -  3.Create New Associated Contribution, Contribution Type = NSF
                        if (!IsExistsNSFContribution(serviceAdmin, tracing, target.Id))
                        {
                            tracing.Trace($"Inside NSF Contribution does not exists.");

                            CreateNSFContribution(context, service, serviceAdmin, tracing, nsffee, NSFFund);
                            RecalculateSubmissionDue(service, tracing, target.Id, submissionamount, nsffee);
                        }

                    } //end of if (submissionStatus != null && submissionStatus.Value == 922070007)
                }
                catch (Exception ex)
                {
                    new Exception("FROM PostSubmissionStatusChange Plugin: \r\n" + ex.StackTrace);
                }
            } // end of if (target.LogicalName.Equals(EntityName))
        }

        protected void CreateNSFContribution(IPluginExecutionContext context, IOrganizationService service, IOrganizationService serviceAdmin, ITracingService tracing,
            decimal NSFFee, EntityReference NSFFund)
        {
            tracing.Trace($"Inside CreateNSFContribution Method");

            Entity target = (Entity)context.InputParameters["Target"];
            if (target.LogicalName.Equals("bpa_submission"))
            {
                try
                {

                    //1. FETCH NSF FUND VALUE FROM CONFIGURATION
                    //string nsfFundId = ConfigurationHelper.FetchNSFFund(service, "NSF Fee Name");
                    Entity postImageEntity = context.PostEntityImages != null &&
                                             context.PostEntityImages.Contains("PostImage")
                        ? context.PostEntityImages["PostImage"] : null;

                    EntityReference employer = null;
                    DateTime submissiondate = DateTime.MinValue;
                    tracing.Trace($"Getting all values");

                    Guid submissionId = target.Id;
                    if (postImageEntity.Attributes.Contains("bpa_accountagreementid"))
                        employer = postImageEntity.GetAttributeValue<EntityReference>("bpa_accountagreementid");
                    tracing.Trace($"Employer {employer.Id}");

                    if (postImageEntity.Attributes.Contains("bpa_submissiondate"))
                        submissiondate = postImageEntity.GetAttributeValue<DateTime>("bpa_submissiondate");
                    tracing.Trace($"Submission Date {submissiondate.ToShortDateString()}");


                    //Set Field
                    tracing.Trace($"Create Contribution record");

                    Entity contribution = new Entity("bpa_contribution");
                    contribution["bpa_submission"] = new EntityReference("bpa_submission", submissionId);
                    contribution["bpa_accountagreementid"] = employer;
                    if (submissiondate != DateTime.MinValue)
                        contribution["bpa_contributiondate"] = submissiondate;
                    if (NSFFund.Id != Guid.Empty)
                        contribution["bpa_fund"] = new EntityReference("bpa_fund", NSFFund.Id); // new Guid(nsfFundId));
                    contribution["bpa_contributiontype"] = new OptionSetValue(922070005);
                    contribution["bpa_contributionamount"] = new Money(NSFFee);

                    serviceAdmin.Create(contribution);
                    tracing.Trace($"Successfully Contribution record created");

                }
                catch (Exception ex)
                {
                    throw ex;
                }
            } // end of if (target.LogicalName.Equals(EntityName))
        }

        protected bool IsExistsNSFContribution(IOrganizationService service, ITracingService tracing, Guid submissionId)
        {
            tracing.Trace("Inside IsExistsNSFContribution");
            bool IsExists = false;
            EntityCollection contributions = ContributionHelper.FetchAllContributions(service, tracing, submissionId, true);
            if (contributions.Entities.Count > 0)
            {
                IsExists = true;
            }
            tracing.Trace($"IsExistsNSFContribution return = {IsExists}");
            return IsExists;
        }

        protected void RecalculateSubmissionDue(IOrganizationService service, ITracingService tracing, Guid submissionId,
            decimal submissionAmount, decimal nsfFee)
        {
            tracing.Trace($"Inside RecalculateSubmissionDue");
            Entity submission = new Entity("bpa_submission");
            submission["bpa_submissionamount"] = new Money(submissionAmount + nsfFee);
            submission["bpa_submissionpaid"] = new Money(0);
            submission["bpa_submissionvariance"] = new Money(submissionAmount + nsfFee);
            submission.Id = submissionId;
            service.Update(submission);
            tracing.Trace($"Update Submission Record");
        }

        protected void CreateTransactionsAndContributions(LocalPluginContext context, IOrganizationService serviceAdmin, Entity entity, Entity ImageEntity,
            EntityReference DefaultFund, List<Entity> DefaultFundList, Entity preimageentity)
        {
            IOrganizationService service = context.OrganizationService;
            IServiceProvider serviceprovider = context.ServiceProvider;
            ITracingService tracingService = context.TracingService;

            tracingService.Trace("Inside CreateTransactionsAndContributions");

            OptionSetValue SubmissionStatus = new OptionSetValue(0);
            if (entity.Attributes.Contains("bpa_submissionstatus"))
            {
                tracingService.Trace("Inside If -1");

                SubmissionStatus = entity.GetAttributeValue<OptionSetValue>("bpa_submissionstatus");

                OptionSetValue Pre_SubmissionStatus = new OptionSetValue(0);
                if (preimageentity.Contains("bpa_submissionstatus"))
                    Pre_SubmissionStatus = preimageentity.GetAttributeValue<OptionSetValue>("bpa_submissionstatus");

                
                else if ((SubmissionStatus.Value == (int)Helper.SubmissionStatus.Completed) &&
                         (Pre_SubmissionStatus.Value == (int)Helper.SubmissionStatus.Verification))
                {
                    
                }
                else if ((SubmissionStatus.Value == (int)Helper.SubmissionStatus.Verification) &&
                                         (Pre_SubmissionStatus.Value == (int)Helper.SubmissionStatus.Pending))
                { }
                else if ((SubmissionStatus.Value == (int)Helper.SubmissionStatus.Verification) &&
                         (Pre_SubmissionStatus.Value == (int)Helper.SubmissionStatus.PendingDeposit))
                { }
                else if ((SubmissionStatus.Value == (int)Helper.SubmissionStatus.PendingDeposit) &&
                                         (Pre_SubmissionStatus.Value == (int)Helper.SubmissionStatus.Verification))
                { } //end of else if ((SubmissionStatus.Value == (int)Submission_Status.Completed)             
                if ((SubmissionStatus.Value == (int)Helper.SubmissionStatus.Completed) &&
                         (Pre_SubmissionStatus.Value == (int)Helper.SubmissionStatus.PendingDeposit))
                { } //end of else if ((SubmissionStatus.Value == (int)Submission_Status.Completed) 
            } //end of else if(SubmissionStatus.Value == (int)Submission_Status.Verification)
        } //END OF CreateTransactionsAndContributions METHOD


    }
}