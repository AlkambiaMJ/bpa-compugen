﻿#region

using System;
using BPA.Benefit.Management.CRM.Plugins.Helper;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using System.Collections.Generic;

#endregion

namespace BPA.Benefit.Management.CRM.Plugins
{
    /// <summary>
    ///     MemberPlanAdjustmentOnCreatePost PlugIn.
    /// </summary>
    public class MemberPlanAdjustmentOnStateChangePost : Plugin
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="MemberPlanAdjustmentOnCreatePost" /> class.
        /// </summary>
        public MemberPlanAdjustmentOnStateChangePost(string unsecureString, string secureString)
            : base(typeof(MemberPlanAdjustmentOnStateChangePost), unsecureString, secureString)
        {
            RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(40, "SetState", PluginHelper.BpaMemberPlanadjustment, ExecuteStateChangeUpdate));
            RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(40, "SetStateDynamicEntity", PluginHelper.BpaMemberPlanadjustment, ExecuteStateChangeUpdate));

            // Note : you can register for more events here if this plugin is not specific to an individual entity and message combination.
            // You may also need to update your RegisterFile.crmregister plug-in registration file to reflect any change.
        }

        /// <summary>
        ///     Executes the plug-in.
        /// </summary>
        /// <param name="localContext">
        ///     The <see cref="Plugin.LocalPluginContext" /> which contains the
        ///     <see cref="IPluginExecutionContext" />,
        ///     <see cref="IOrganizationService" />
        ///     and <see cref="ITracingService" />
        /// </param>
        /// <remarks>
        ///     For improved performance, Microsoft Dynamics CRM caches plug-in instances.
        ///     The plug-in's Execute method should be written to be stateless as the constructor
        ///     is not called for every invocation of the plug-in. Also, multiple system threads
        ///     could execute the plug-in at the same time. All per invocation state information
        ///     is stored in the context. This means that you should not use global variables in plug-ins.
        /// </remarks>
        /// 
        protected void ExecuteStateChangeUpdate(LocalPluginContext localContext)
        {
            // DONE - TODO JK - Add trace statement after each comment

            // If local context not passed in, raise error
            if (localContext == null)
                throw new ArgumentNullException($"Local context not passed in.");

            // Create the context variables
            IPluginExecutionContext context = localContext.PluginExecutionContext;
            IOrganizationService service = localContext.OrganizationService;
            ITracingService tracingService = localContext.TracingService;
            IServiceProvider serviceProvider = localContext.ServiceProvider;

            // If Target is not passed in, raise error
            tracingService.Trace("If Target is not passed in, raise error");
            if (!context.InputParameters.Contains(PluginHelper.EntityMoniker) ||
                !(context.InputParameters[PluginHelper.EntityMoniker] is EntityReference))
                throw new ArgumentNullException($"EntityReference is not passed in.");

            // Create the EntityReference
            tracingService.Trace("Create the EntityReference");
            EntityReference targetEntity = (EntityReference)context.InputParameters[PluginHelper.EntityMoniker];
            if (targetEntity == null)
                throw new ArgumentNullException($"EntityReference is not defined.");
            //Get Pre Image
            Entity preImageEntity = context.PreEntityImages != null && context.PreEntityImages.Contains(PluginHelper.PreImageAlias) ? context.PreEntityImages[PluginHelper.PreImageAlias] : null;

            if (preImageEntity == null)
                throw new ArgumentNullException($"preImage entity is not defined. (MemberPlanAdjustmentOnStateChangePost Plguin) ");

            //Adjustment Type & Member Plan
            OptionSetValue adjustmentCategory = preImageEntity.GetAttributeValue<OptionSetValue>("bpa_adjustmentcategory");
            OptionSetValue adjustmentType = preImageEntity.GetAttributeValue<OptionSetValue>("bpa_benefitadjustmenttype");
            EntityReference memberPlan = preImageEntity.Contains("bpa_memberplanid") ? preImageEntity.GetAttributeValue<EntityReference>("bpa_memberplanid") : null;
            
            // Get the record's state & status
            tracingService.Trace("Get the record's state & status");
            OptionSetValue status = (OptionSetValue)context.InputParameters["Status"];

            if (status.Value == (int)MemberAdjustmentStatusReason.Active)
                throw new InvalidPluginExecutionException(OperationStatus.Failed, @"You are not allowed to activate Member Adjustment as selected adjustment either 'Paid/Completed' or 'Cancelled'.");

            // This is is only for Benefit Adjustment
            if (adjustmentCategory.Value == (int)MemberPlanAdjustmentCategory.PensionAdjustment) return;

            if (adjustmentCategory.Value == (int)MemberPlanAdjustmentCategory.BenefitAdjustment)
            {

                bool executeMultiple = false;
                string executeMultipleRequest = "0";
                try
                {
                    executeMultipleRequest = PluginConfiguration[PluginHelper.ConfigExecuteMultipleRequest];
                }
                catch(Exception ex)
                {
                    executeMultipleRequest = "0";
                }
                // Get Execute User
                executeMultiple = (executeMultipleRequest == "1");



                Guid executeUser = context.UserId;
                //string crmAdminUserId = PluginConfiguration[PluginHelper.ConfigCrmAdmin];
                string crmAdminUserId = string.Empty;
                try
                {
                    crmAdminUserId = PluginConfiguration[PluginHelper.ConfigCrmAdmin];
                }
                catch (Exception ex)
                {
                    // Get value from Database
                    crmAdminUserId = ConfigurationHelper.FetchValueId(service, PluginHelper.ConfigCrmAdmin);
                }
                if (string.IsNullOrEmpty(crmAdminUserId))
                {
                    tracingService.Trace("ERROR FROM MemberPlanAdjustmentOnStateChangePost Plugin - CRM Admin USer Could not Found.");
                    throw new InvalidPluginExecutionException(OperationStatus.Failed, $@"The given key was not present in the dictionary. Please contact BPA Administrator");
                }

                IOrganizationServiceFactory serviceFactory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
                IOrganizationService serviceAdmin = serviceFactory.CreateOrganizationService(new Guid(crmAdminUserId));

                #region ------------ Benefit Adjustment ------------------
                // Switch on the status value
                tracingService.Trace("Switch on the status value");
                switch (status.Value)
                {
                    case (int)MemberAdjustmentStatusReason.Completed:

                        //Get the owner of Adjustment record
                        EntityReference owner = (preImageEntity.Contains("ownerid") ? preImageEntity.GetAttributeValue<EntityReference>("ownerid") : null);

                        if (adjustmentType.Value == (int)MemberPlanBenefitAdjustmentType.TransferReciprocalOut)
                        {
                            #region -------------- ReciprocalTransfer Out ---------------

                            //Get all values from Pre Image
                            tracingService.Trace("Get the Values");
                            //Entity adjustment = service.Retrieve(PluginHelper.BpaMemberPlanadjustment, targetEntity.Id, new ColumnSet(true));
                            DateTime workMonth = preImageEntity.Contains("bpa_workmonth") ? preImageEntity.GetAttributeValue<DateTime>("bpa_workmonth") : DateTime.MinValue;
                            EntityReference homeLocal = preImageEntity.Contains("bpa_homelocalid") ? preImageEntity.GetAttributeValue<EntityReference>("bpa_homelocalid") : null;
                            decimal reciprocalHours = preImageEntity.Contains("bpa_reciprocalhours") ? preImageEntity.GetAttributeValue<decimal>("bpa_reciprocalhours") : 0;
                            Money reciprocalDollars = preImageEntity.Contains("bpa_reciprocaldollars") ? preImageEntity.GetAttributeValue<Money>("bpa_reciprocaldollars") : new Money(0);

                            EntityReference lastAgreement = TransactionHelper.FetchLastContributionAgreement(service, tracingService, memberPlan.Id, workMonth);

                            //Create Transfer REcord
                            Guid reciprocalId = TransactionHelper.CreateReserveTransaction(serviceAdmin, tracingService, memberPlan.Id, workMonth, (int)BpaTransactionbpaTransactionType.Reciprocal,
                                homeLocal.Id, (int)BpaTransactionbpaTransactionCategory.BenefitAdjustment, targetEntity.Id, true, DateTime.MinValue, reciprocalHours, reciprocalDollars, lastAgreement); //Transfer

                            //Update Ending balance from that month and onward
                            DateTime startDate = workMonth;
                            DateTime lastDate = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddMonths(-1);

                            while (startDate <= lastDate)
                            {
                                //Fetch All Transactions which are eligibility status of that month and Update Ending Dollar and Ending Hours
                                EntityCollection transactions = TransactionHelper.FetchAllTransactionsByMonth(service, tracingService, memberPlan.Id, startDate);

                                if (transactions != null && transactions.Entities.Count > 0)
                                {
                                    Dictionary<string, decimal> endingBalances1 = TransactionHelper.FetchEndingBalanceWhenLateContribution(service, memberPlan.Id, startDate);
                                    foreach (Entity tran in transactions.Entities)
                                    {
                                        int tt = tran.GetAttributeValue<OptionSetValue>("bpa_transactiontype").Value;

                                        if (tt != (int)BpaTransactionbpaTransactionType.TransferOut) continue;

                                        //Update Transaction
                                        Entity t = new Entity(PluginHelper.BpaTransaction)
                                        {
                                            Id = tran.Id
                                        };
                                        if (endingBalances1.ContainsKey("hours"))
                                            t["bpa_endinghourbalance"] = null; //endingBalances1["hours"];
                                        if (endingBalances1.ContainsKey("dollarhours"))
                                            t["bpa_endingdollarbalance"] = null; //endingBalances1["dollarhours"];
                                        if (endingBalances1.ContainsKey("selfpaydollar"))
                                            t["bpa_endingselfpaybalance"] = null; //new Money(endingBalances1["selfpaydollar"]);

                                        if (endingBalances1.ContainsKey("secondarydollar"))
                                            t["bpa_secondarydollarbalance"] = null; //new Money(endingBalances1["secondarydollar"]);

                                        serviceAdmin.Update(t);
                                    }

                                }

                                startDate = startDate.AddMonths(1);
                            }

                            //Update roll ups
                            PluginHelper.TriggerRollup(service, tracingService, PluginHelper.BpaMemberplan, memberPlan.Id, "bpa_contributiondollarbank");
                            PluginHelper.TriggerRollup(service, tracingService, PluginHelper.BpaMemberplan, memberPlan.Id, "bpa_contributionhourbank");
                            PluginHelper.TriggerRollup(service, tracingService, PluginHelper.BpaMemberplan, memberPlan.Id, "bpa_selfpaybank");

                            #endregion
                        }
                        else if (adjustmentType.Value == (int)MemberPlanBenefitAdjustmentType.OtherBenefit)
                        {
                            #region ------------- OTHER ------------------

                            DateTime adjustmentDate = (preImageEntity.Contains("bpa_adjustmentdate") ? preImageEntity.GetAttributeValue<DateTime>("bpa_adjustmentdate") : DateTime.MinValue);
                            adjustmentDate = new DateTime(adjustmentDate.Year, adjustmentDate.Month, 1);
                            //Validate get value
                            if ((memberPlan == null) || (adjustmentDate == DateTime.MinValue))
                                throw new InvalidPluginExecutionException("Either Member or Adjustment date is blank. Please enter value.");

                            // Getting all values
                            Money dollars = new Money(0);
                            tracingService.Trace($"Before dollars: {dollars.Value.ToString()}");
                            if (preImageEntity.Contains("bpa_dollars"))
                                dollars = preImageEntity.GetAttributeValue<Money>("bpa_dollars");
                            tracingService.Trace($"After Dollar {dollars.Value.ToString()}");

                            Money vacationPay = new Money(0);
                            if (preImageEntity.Contains("bpa_vacationpay"))
                                vacationPay = preImageEntity.GetAttributeValue<Money>("bpa_vacationpay");
                            tracingService.Trace($"After Vacation Pay {vacationPay.Value.ToString()}");

                            Money selfPay = new Money(0);
                            if (preImageEntity.Contains("bpa_selfpay"))
                                selfPay = preImageEntity.GetAttributeValue<Money>("bpa_selfpay");
                            tracingService.Trace($"After Self-Pay {selfPay.Value.ToString()}");

                            decimal hours = 0;
                            tracingService.Trace($"BEFORE hour {hours.ToString()}");
                            if (preImageEntity.Contains("bpa_hours"))
                                hours = preImageEntity.GetAttributeValue<decimal>("bpa_hours");
                            tracingService.Trace($"After hour {hours.ToString()}");


                            Money secondaryDollarBank = new Money(0);
                            tracingService.Trace($"BEFORE Secondary Dollar Bank {secondaryDollarBank.ToString()}");
                            if (preImageEntity.Contains("bpa_secondarydollarbank"))
                                secondaryDollarBank = preImageEntity.GetAttributeValue<Money>("bpa_secondarydollarbank");
                            tracingService.Trace($"After Secondary Dollar Bank {secondaryDollarBank.ToString()}");

                            bool IsIgnoreZeroBalance = false;
                            tracingService.Trace($"BEFORE Is Ignore Zero Balance: {IsIgnoreZeroBalance}");
                            if (preImageEntity.Contains("bpa_ignorezerobalance"))
                                IsIgnoreZeroBalance = preImageEntity.GetAttributeValue<bool>("bpa_ignorezerobalance");
                            tracingService.Trace($"After Is Ignore Zero Balance:  {IsIgnoreZeroBalance}");


                            //find Member Eligibility Category
                            EntityReference elibilityCategory = TransactionHelper.FetchCurrentEligibilityCategory(service, tracingService, memberPlan.Id, adjustmentDate);
                            EntityReference lastAgreement = TransactionHelper.FetchLastContributionAgreement(service, tracingService, memberPlan.Id, adjustmentDate);

                            Guid transactionId = Guid.Empty;
                            string defaultCurrency = string.Empty;

                            
                            try
                            {
                                defaultCurrency = PluginConfiguration[PluginHelper.ConfigDefaultCurrency];
                            }
                            catch (Exception ex)
                            {
                                // Get value from Database
                                defaultCurrency = ConfigurationHelper.FetchValueId(service, PluginHelper.ConfigDefaultCurrency);
                            }
                            if (string.IsNullOrEmpty(defaultCurrency))
                            {
                                tracingService.Trace("ERROR FROM Plugin - CRM Default Currency Key Could not Found.");
                                throw new InvalidPluginExecutionException(OperationStatus.Failed, $@"The given key was not present in the dictionary. Please contact BPA Administrator");
                            }


                            //Dollar value is not nulll then create Dollar Bank Adjustment
                            if (dollars.Value != 0)
                            {
                                tracingService.Trace($"Dollar Bank Adjustment");
                                transactionId = TransactionHelper.CreateTransaction(serviceAdmin, tracingService, memberPlan, adjustmentDate,
                                    (int)BpaTransactionbpaTransactionCategory.BenefitAdjustment,
                                    (int)BpaTransactionbpaTransactionType.DollarBankAdjustment, targetEntity.Id, hours, dollars, elibilityCategory,
                                    lastAgreement, defaultCurrency, owner);

                                tracingService.Trace($"Dollar Bank Adjustment Roll up");
                                PluginHelper.TriggerRollup(serviceAdmin, tracingService, PluginHelper.BpaMemberplan, memberPlan.Id, "bpa_contributiondollarbank");
                            }
                            //Vacation value is not nulll then create Vacation adjustment
                            if (vacationPay.Value != 0)
                            {
                                tracingService.Trace($"Vacation Bank Adjustment");
                                transactionId = TransactionHelper.CreateTransaction(serviceAdmin, tracingService, memberPlan, adjustmentDate,
                                    (int)BpaTransactionbpaTransactionCategory.BenefitAdjustment,
                                    (int)BpaTransactionbpaTransactionType.VacationPayAdjustment, targetEntity.Id, hours, vacationPay, null, lastAgreement,
                                    defaultCurrency, owner);

                                tracingService.Trace($"Vacation Bank Rollup");
                                PluginHelper.TriggerRollup(serviceAdmin, tracingService, PluginHelper.BpaMemberplan, memberPlan.Id, "bpa_vacationpaybank");
                            }

                            if (selfPay.Value != 0)
                            {
                                tracingService.Trace($"Self pay Adjustment");
                                transactionId = TransactionHelper.CreateTransaction(serviceAdmin, tracingService, memberPlan, adjustmentDate,
                                    (int)BpaTransactionbpaTransactionCategory.BenefitAdjustment,
                                    (int)BpaTransactionbpaTransactionType.SelfPayBankAdjustment, targetEntity.Id, hours, selfPay, elibilityCategory, lastAgreement,
                                    defaultCurrency, owner);

                                tracingService.Trace($"Self pay Adjustment Rollup");
                                PluginHelper.TriggerRollup(serviceAdmin, tracingService, PluginHelper.BpaMemberplan, memberPlan.Id, "bpa_selfpaybank");
                            }

                            if (hours != 0)
                            {
                                tracingService.Trace($"Hour Bank Adjutment");
                                transactionId = TransactionHelper.CreateTransaction(serviceAdmin, tracingService, memberPlan, adjustmentDate,
                                    (int)BpaTransactionbpaTransactionCategory.BenefitAdjustment,
                                    (int)BpaTransactionbpaTransactionType.HourBankAdjustment, targetEntity.Id, hours, dollars, elibilityCategory, lastAgreement,
                                    defaultCurrency, owner);

                                tracingService.Trace($"Hour Bank Rollup");
                                PluginHelper.TriggerRollup(serviceAdmin, tracingService, PluginHelper.BpaMemberplan, memberPlan.Id, "bpa_contributionhourbank");
                            }


                            if (secondaryDollarBank.Value != 0)
                            {
                                tracingService.Trace($"Secondary Dollar Bank Adjutment");
                                transactionId = TransactionHelper.CreateTransaction(serviceAdmin, tracingService, memberPlan, adjustmentDate,
                                    (int)BpaTransactionbpaTransactionCategory.BenefitAdjustment,
                                    (int)BpaTransactionbpaTransactionType.SecondaryDollarBankAdjustment, targetEntity.Id, hours, secondaryDollarBank, elibilityCategory, 
                                    lastAgreement, defaultCurrency, owner);

                                tracingService.Trace($"Secondary Dollar Bank Rollup");
                                PluginHelper.TriggerRollup(serviceAdmin, tracingService, PluginHelper.BpaMemberplan, memberPlan.Id, "bpa_secondarydollarbank");
                            }

                            // This flage checking added beacuse when we migrate the member we need to run the retro processing even hours are zero
                            if (!IsIgnoreZeroBalance)
                            {
                                //transactionId = Guid.Empty;
                                if (transactionId == Guid.Empty)
                                    throw new InvalidPluginExecutionException(OperationStatus.Failed, $@"Adjustment could not be completed as no associated transaction has been Generated. Please try again. If problem persists please contact a BPA Administrator.");
                            }

                            if (!ContactHelper.FetchMemberStatus(service, memberPlan.Id))
                            {
                                // Run MemberPlan Retro processing
                                // New optimized process
                                string retroprocessingMessage = MemberPlanRetroprocessing.RecalculateTransactions(context, serviceProvider, service, tracingService,
                                    crmAdminUserId, executeMultiple, memberPlan, adjustmentDate, false, false, null);

                                //throw new InvalidPluginExecutionException("TEsting");
                                // Old Process
                                //string retroprocessingMessage = MemberRetroprocessing_New.RecalculateTransaction(service, tracingService, memberPlan,
                                //    adjustmentDate, false, false, null);

                                // FOR TESTING ONLY
                                //if (!string.IsNullOrEmpty(retroprocessingMessage))
                                //throw new InvalidPluginExecutionException(retroprocessingMessage);
                            }
                            #endregion
                        }
                        break;
                }
                #endregion
            }
        }
    }
}