﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using BPA.Benefit.Management.CRM.Plugins.Helper;

namespace BPA.Benefit.Management.CRM.Plugins
{
    public class ContactOnCreatePost : Plugin
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ContactOnCreatePost"/> class.
        /// </summary>
        public ContactOnCreatePost()
            : base(typeof(ContactOnCreatePost))
        {
            base.RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(40, "Create", PluginHelper.contact, new Action<LocalPluginContext>(ExecutePostContactCreate)));

            // Note : you can register for more events here if this plugin is not specific to an individual entity and message combination.
            // You may also need to update your RegisterFile.crmregister plug-in registration file to reflect any change.
        }

        /// <summary>
        /// Executes the plug-in.
        /// </summary>
        /// <param name="localContext">The <see cref="LocalPluginContext"/> which contains the
        /// <see cref="IPluginExecutionContext"/>,
        /// <see cref="IOrganizationService"/>
        /// and <see cref="ITracingService"/>
        /// </param>
        /// <remarks>
        /// For improved performance, Microsoft Dynamics CRM caches plug-in instances.
        /// The plug-in's Execute method should be written to be stateless as the constructor
        /// is not called for every invocation of the plug-in. Also, multiple system threads
        /// could execute the plug-in at the same time. All per invocation state information
        /// is stored in the context. This means that you should not use global variables in plug-ins.
        /// </remarks>
        protected void ExecutePostContactCreate(LocalPluginContext localContext)
        {
            return;
            //if (localContext == null)
            //    throw new ArgumentNullException("localContext");

            //// Create the context variables
            //IPluginExecutionContext context = localContext.PluginExecutionContext;
            //IOrganizationService service = localContext.OrganizationService;
            //ITracingService tracingService = localContext.TracingService;
            //IServiceProvider serviceProvider = localContext.ServiceProvider;

            //if (context.InputParameters.Contains(PluginHelper.Target) && context.InputParameters[PluginHelper.Target] is Entity)
            //{
            //    // Create the target entity and postimage
            //    tracingService.Trace("Create the target entity and postimage");
            //    Entity entity = (Entity)context.InputParameters["Target"];
            //    Entity postImageEntity = (context.PostEntityImages != null && context.PostEntityImages.Contains(PluginHelper.postImageAlias)) ? context.PostEntityImages[PluginHelper.postImageAlias] : null;

            //    //Get All Information and Copy into Member Plan Details
            //    CopyAllGeneralInformation(serviceProvider, entity, postImageEntity);

            //    // Following method called only when Member is "RETIREE"
            //    tracingService.Trace("Following method called only when Member is 'RETIREE'");
            //    ContactHelper.CreateNewMemberTransactions(service, entity, postImageEntity);

            //    // Following method called only when new Member Plan Details created 
            //    tracingService.Trace("Following method called only when new Member Plan Details created");
            //    CloneDependentAndBeneficiary(service, tracingService, entity, postImageEntity);
            //}
        }

        //protected void CloneDependentAndBeneficiary(IOrganizationService service, ITracingService tracingService, Entity entity, Entity imageEntity)
        //{
        //    // If the postImage contains the Member record and Plan (sector) record
        //    tracingService.Trace("If the postImage contains the Member record and Plan (sector) record");
        //    if ((imageEntity.Attributes.Contains("bpa_member") && imageEntity.Attributes["bpa_member"] != null) &&
        //        (imageEntity.Attributes.Contains("bpa_sector") && imageEntity.Attributes["bpa_sector"] != null))
        //    {
        //        EntityReference Member = imageEntity.GetAttributeValue<EntityReference>("bpa_member");
        //        EntityReference Sector = imageEntity.GetAttributeValue<EntityReference>("bpa_sector");

        //        // TODOJK: Remove this commented out code

        //        // Obtain the execution context from the service provider.
        //        //IPluginExecutionContext context = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));

        //        //// Obtain the organization service reference.
        //        //IOrganizationServiceFactory serviceFactory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
        //        //IOrganizationService service = serviceFactory.CreateOrganizationService(context.UserId);
        //        //Guid _callerId = PluginHelper.FetchDefaultUser(service, Sector.Id);
        //        //service = serviceFactory.CreateOrganizationService(_callerId); //Impersonate Service

        //        // Find Last Created Member Plan Detail
        //        tracingService.Trace("Find Last Created Member Plan Detail");
        //        Entity PlanDetail = ContactHelper.FetchLastMemberPlanDetail(service, Member.Id, entity.Id);
        //        if (PlanDetail != null)
        //        {
        //            EntityCollection dependents = ContactHelper.FetchAllDependents(service, PlanDetail.Id);
        //            if (dependents != null && dependents.Entities.Count > 0)
        //            {
        //                foreach (Entity dependent in dependents.Entities)
        //                {
        //                    // Remove all references
        //                    tracingService.Trace("Remove all references");
        //                    dependent.Id = Guid.Empty;
        //                    dependent.Attributes.Remove("bpa_dependantid");
        //                    dependent.Attributes.Remove("bpa_memberplan");
        //                    dependent.Attributes.Remove("bpa_issyncronize");

        //                    // Add Relationship
        //                    tracingService.Trace("Add Relationship");
        //                    dependent["bpa_iscopy"] = true;
        //                    dependent["bpa_memberplan"] = new EntityReference("contact", entity.Id);

        //                    // Create the record
        //                    tracingService.Trace("Create the record");
        //                    service.Create(dependent);
        //                }
        //            }
        //        }
        //    }
        //}


        //protected void CopyAllGeneralInformation(IServiceProvider serviceProvider, Entity entity, Entity imageEntity)
        //{
        //    if ((imageEntity.Attributes.Contains("bpa_member") && imageEntity.Attributes["bpa_member"] != null) &&
        //        (imageEntity.Attributes.Contains("bpa_sector") && imageEntity.Attributes["bpa_sector"] != null))
        //    {
        //        EntityReference Member = imageEntity.GetAttributeValue<EntityReference>("bpa_member");
        //        EntityReference Sector = imageEntity.GetAttributeValue<EntityReference>("bpa_sector");

        //        // Obtain the execution context from the service provider.
        //        IPluginExecutionContext context = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));

        //        // Obtain the organization service reference.
        //        IOrganizationServiceFactory serviceFactory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
        //        IOrganizationService service = serviceFactory.CreateOrganizationService(context.UserId);
        //        //Guid _callerId = PluginHelper.FetchDefaultUser(service, Sector.Id);
        //        //service = serviceFactory.CreateOrganizationService(_callerId); //Impersonate Service

        //        Entity postimage = service.Retrieve(PluginHelper.bpa_member, Member.Id, new ColumnSet(true));

        //        if (postimage != null)
        //        {
        //            //Get Value
        //            string sin = postimage.GetAttributeValue<string>("bpa_socialinsuranceno");
        //            DateTime dateofbirth = postimage.GetAttributeValue<DateTime>("bpa_dateofbirth");
        //            OptionSetValue gender = postimage.GetAttributeValue<OptionSetValue>("bpa_gender");
        //            string jobtitle = postimage.GetAttributeValue<string>("bpa_jobtitle");
        //            //string email = postimage.GetAttributeValue<string>("emailaddress");
        //            string homephone = postimage.GetAttributeValue<string>("bpa_homephone");
        //            string mobilephone = postimage.GetAttributeValue<string>("bpa_mobilephone");
        //            string businessphone = postimage.GetAttributeValue<string>("bpa_businessphone");
        //            string fax = postimage.GetAttributeValue<string>("bpa_fax");
        //            OptionSetValue maritalstatus = postimage.GetAttributeValue<OptionSetValue>("bpa_maritalstatus");
        //            DateTime marriagedate = postimage.GetAttributeValue<DateTime>("bpa_dateofmarriage");
        //            OptionSetValue smoker = postimage.GetAttributeValue<OptionSetValue>("bpa_smoker");
        //            string street1 = postimage.GetAttributeValue<string>("bpa_street1");
        //            string street2 = postimage.GetAttributeValue<string>("bpa_street2");
        //            string street3 = postimage.GetAttributeValue<string>("bpa_street3");
        //            string city = postimage.GetAttributeValue<string>("bpa_city");
        //            string stateprovince = postimage.GetAttributeValue<string>("bpa_stateprovince");
        //            string zippostalcode = postimage.GetAttributeValue<string>("bpa_zippostalcode");
        //            string countryregion = postimage.GetAttributeValue<string>("bpa_countryregion");

        //            //Set Value
        //            Entity contact = new Entity(PluginHelper.contact);
        //            contact.Id = entity.Id;
        //            contact["jobtitle"] = jobtitle;
        //            //contact["emailaddress1"] = email;
        //            contact["telephone2"] = homephone;
        //            contact["mobilephone"] = mobilephone;
        //            contact["telephone1"] = businessphone;
        //            contact["fax"] = fax;
        //            contact["bpa_socialinsurancenumber"] = sin;
        //            if (dateofbirth != DateTime.MinValue)
        //                contact["birthdate"] = dateofbirth;
        //            contact["familystatuscode"] = maritalstatus;
        //            if (marriagedate != DateTime.MinValue)
        //                contact["anniversary"] = marriagedate;
        //            contact["address1_line1"] = street1;
        //            contact["address1_line2"] = street2;
        //            contact["address1_line3"] = street3;
        //            contact["address1_city"] = city;
        //            contact["address1_stateorprovince"] = stateprovince;
        //            contact["address1_postalcode"] = zippostalcode;
        //            contact["address1_country"] = countryregion;
        //            contact["gendercode"] = gender;
        //            contact["bpa_smoker"] = smoker;
        //            service.Update(contact);

        //        }
        //    }
        //}

    }
}
