﻿#region

using System;
using BPA.Benefit.Management.CRM.Plugins.Helper;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;

#endregion

namespace BPA.Benefit.Management.CRM.Plugins
{
    /// <summary>
    ///     SubmissionOnStateChangePost PlugIn.
    /// </summary>
    public class SubmissionOnStateChangePostAsync : Plugin
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="SubmissionOnStateChangePost" /> class.
        /// </summary>
        public SubmissionOnStateChangePostAsync()
            : base(typeof(SubmissionOnStateChangePostAsync))
        {
            RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(40, "SetState",
                PluginHelper.BpaSubmission, ExecuteStateChangeUpdate));
            RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(40, "SetStateDynamicEntity",
                PluginHelper.BpaSubmission, ExecuteStateChangeUpdate));

            // Note : you can register for more events here if this plugin is not specific to an individual entity and message combination.
            // You may also need to update your RegisterFile.crmregister plug-in registration file to reflect any change.
        }

        /// <summary>
        ///     Executes the plug-in.
        /// </summary>
        /// <param name="localContext">
        ///     The <see cref="Plugin.LocalPluginContext" /> which contains the
        ///     <see cref="IPluginExecutionContext" />,
        ///     <see cref="IOrganizationService" />
        ///     and <see cref="ITracingService" />
        /// </param>
        /// <remarks>
        ///     For improved performance, Microsoft Dynamics CRM caches plug-in instances.
        ///     The plug-in's Execute method should be written to be stateless as the constructor
        ///     is not called for every invocation of the plug-in. Also, multiple system threads
        ///     could execute the plug-in at the same time. All per invocation state information
        ///     is stored in the context. This means that you should not use global variables in plug-ins.
        /// </remarks>
        protected void ExecuteStateChangeUpdate(LocalPluginContext localContext)
        {
            // If local context not passed in, raise error
            if (localContext == null)
                throw new ArgumentNullException($"Local context not passed in.");

            // Create the context variables
            IPluginExecutionContext context = localContext.PluginExecutionContext;
            IOrganizationService service = localContext.OrganizationService;
            ITracingService tracingService = localContext.TracingService;

            // If EntityReference is not passed in, raise error
            tracingService.Trace("If Target is not passed in, raise error");
            if (!context.InputParameters.Contains(PluginHelper.EntityMoniker) ||
                !(context.InputParameters[PluginHelper.EntityMoniker] is EntityReference))
                throw new ArgumentNullException($"EntityReference is not passed in.");

            // Create the EntityReference
            tracingService.Trace("Create the EntityReference");
            EntityReference targetEntity = (EntityReference)context.InputParameters[PluginHelper.EntityMoniker];
            if (targetEntity == null)
                throw new ArgumentNullException($"EntityReference is not defined.");

            // If the name is not the Submission entity value
            tracingService.Trace("If the name is not the Submission entity value");
            if (targetEntity.LogicalName != PluginHelper.BpaSubmission)
                return;

            // Get the record's state & status
            tracingService.Trace("Get the record's state & status");
            OptionSetValue status = (OptionSetValue)context.InputParameters["Status"];

            // Switch on the status value
            tracingService.Trace("Switch on the status value");
            switch (status.Value)
            {
                case (int)SubmissionStatusCode.Completed:
                case (int)SubmissionStatusCode.Cancelled:
                    tracingService.Trace("Completed / Cancelled");
                    // Deactivate child records
                    tracingService.Trace("Deactivate child records - Conributions");
                    DeactivateContributions(service, tracingService, targetEntity.Id);

                    tracingService.Trace("Deactivate child records - Submission Detail");
                    DeactivateSubmissionDetail(service, tracingService, targetEntity.Id);
                    break;
            }
        }

        // Deactivate Contribution records linked to this Submission
        private static void DeactivateContributions(IOrganizationService service, ITracingService tracingService, Guid submissionId)
        {
            // Get all contributions 
            EntityCollection contributions = ContributionHelper.FetchAllContributionsBySubmissionId(service, submissionId);
            if (contributions == null || contributions.Entities.Count <= 0) return;
            foreach (Entity contribution in contributions.Entities)
                PluginHelper.DeactivateRecord(PluginHelper.BpaContribution, contribution.Id, service);
        }

        // Deactivate Submission Detail records linked to this Submission
        private static void DeactivateSubmissionDetail(IOrganizationService service, ITracingService tracingService, Guid submissionId)
        {
            // Get all Submission Details
            EntityCollection submissiondetails = SubmissionHelper.FetchAllSubmissionDetailBySubmissionId(service,
                tracingService, submissionId, true);
            if (submissiondetails == null || submissiondetails.Entities.Count <= 0) return;
            foreach (Entity submissiondetail in submissiondetails.Entities)
                PluginHelper.DeactivateRecord(PluginHelper.BpaSubmissiondetail, submissiondetail.Id, service);
        }
    }
}