﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BPA.Benefit.Management.CRM.Plugins.Helper;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;

namespace BPA.Benefit.Management.CRM.Plugins
{
    public class ExecuteDateOverlapPreValidation : IPlugin
    {
        public void Execute(IServiceProvider serviceProvider)
        {
            IPluginExecutionContext context = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));
            IOrganizationServiceFactory serviceFactory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
            // Create service with context of current user
            IOrganizationService service = serviceFactory.CreateOrganizationService(context.UserId);
            //create tracing service
            ITracingService tracingService = (ITracingService)serviceProvider.GetService(typeof(ITracingService));


            if (!context.InputParameters.Contains(PluginHelper.Target) ||
                !(context.InputParameters[PluginHelper.Target] is Entity))
                throw new ArgumentNullException($"Target is not of type Entity.");

            // Create the target entity
            tracingService.Trace("Create the target entity");
            Entity TargetEntity = (Entity)context.InputParameters[PluginHelper.Target];

            DateTime startDate = DateTime.MinValue;
            DateTime endDate = DateTime.MinValue;
            bool IsUpdateMessage = false;
            Entity preImageEntity = null;

            if (context.MessageName == PluginHelper.Update)
            {
                IsUpdateMessage = true;
                preImageEntity = context.PreEntityImages != null && context.PreEntityImages.Contains(PluginHelper.PreImageAlias)
                                                            ? context.PreEntityImages[PluginHelper.PreImageAlias] : null;
                if (preImageEntity == null)
                    throw new ArgumentNullException($"preImage entity is not defined.");
            }
            EntityCollection collection = null;
            if (TargetEntity.LogicalName.ToLower() == PluginHelper.BpaEligibilitycategorydetail)
            {
                #region ------------- Eligibility Category Detail ------------------

                // Get all values from Entity
                if (TargetEntity.Contains("bpa_effectivedate"))
                    startDate = TargetEntity.GetAttributeValue<DateTime>("bpa_effectivedate");
                else if (IsUpdateMessage && preImageEntity.Contains("bpa_effectivedate"))
                    startDate = preImageEntity.GetAttributeValue<DateTime>("bpa_effectivedate");

                if (TargetEntity.Contains("bpa_expirationdate"))
                    endDate = TargetEntity.GetAttributeValue<DateTime>("bpa_expirationdate");
                else if (IsUpdateMessage && preImageEntity.Contains("bpa_expirationdate"))
                    endDate = preImageEntity.GetAttributeValue<DateTime>("bpa_expirationdate");

                EntityReference eligibilityCategoryRef = null;
                if (TargetEntity.Contains("bpa_eligibilitycategory"))
                    eligibilityCategoryRef = TargetEntity.GetAttributeValue<EntityReference>("bpa_eligibilitycategory");
                else if (IsUpdateMessage && preImageEntity.Contains("bpa_eligibilitycategory"))
                    eligibilityCategoryRef = preImageEntity.GetAttributeValue<EntityReference>("bpa_eligibilitycategory");

                // Validate Period of Service not overlap with other period of Service
                tracingService.Trace("ExecuteDateOverlapValidation: Inside Eligibility Category Detail");

                collection = EligibilityCategoryHelper.FetchAllEligibilityDetail(service, tracingService, eligibilityCategoryRef.Id);

                CheckOverlapDate(tracingService, IsUpdateMessage, TargetEntity.LogicalName.ToLower(), TargetEntity.Id, collection, startDate, endDate);

                #endregion
            }
            else if (TargetEntity.LogicalName.ToLower() == PluginHelper.BpaRate)
            {
                #region ----------- Contribution Rate ---------------

                // Get all values from Entity
                if (TargetEntity.Contains("bpa_effectivedate"))
                    startDate = TargetEntity.GetAttributeValue<DateTime>("bpa_effectivedate");
                else if (IsUpdateMessage && preImageEntity.Contains("bpa_effectivedate"))
                    startDate = preImageEntity.GetAttributeValue<DateTime>("bpa_effectivedate");

                if (TargetEntity.Contains("bpa_expirydate"))
                    endDate = TargetEntity.GetAttributeValue<DateTime>("bpa_expirydate");
                else if (IsUpdateMessage && preImageEntity.Contains("bpa_expirydate"))
                    endDate = preImageEntity.GetAttributeValue<DateTime>("bpa_expirydate");

                EntityReference agreementRef = null;
                if (TargetEntity.Contains("bpa_subsector"))
                    agreementRef = TargetEntity.GetAttributeValue<EntityReference>("bpa_subsector");
                else if (IsUpdateMessage && preImageEntity.Contains("bpa_subsector"))
                    agreementRef = preImageEntity.GetAttributeValue<EntityReference>("bpa_subsector");

                EntityReference fundRef = null;
                if (TargetEntity.Contains("bpa_fund"))
                    fundRef = TargetEntity.GetAttributeValue<EntityReference>("bpa_fund");
                else if (IsUpdateMessage && preImageEntity.Contains("bpa_fund"))
                    fundRef = preImageEntity.GetAttributeValue<EntityReference>("bpa_fund");

                EntityReference taxableFundRef = null;
                if (TargetEntity.Contains("bpa_taxablefund"))
                    taxableFundRef = TargetEntity.GetAttributeValue<EntityReference>("bpa_taxablefund");
                else if (IsUpdateMessage && preImageEntity.Contains("bpa_taxablefund"))
                    taxableFundRef = preImageEntity.GetAttributeValue<EntityReference>("bpa_taxablefund");

                tracingService.Trace("ExecuteDateOverlapValidation: Inside Contribution Rate ");

                if (fundRef != null)
                {
                    collection = FundHelper.FetchAllFundByAgreementIdAndFundId(service, tracingService, agreementRef.Id, false, fundRef.Id);
                    CheckOverlapDate(tracingService, IsUpdateMessage, TargetEntity.LogicalName.ToLower(), TargetEntity.Id, collection, startDate, endDate);
                }

                if (taxableFundRef != null)
                {
                    collection = FundHelper.FetchAllFundByAgreementIdAndFundId(service, tracingService, agreementRef.Id, true, taxableFundRef.Id);
                    CheckOverlapDate(tracingService, IsUpdateMessage, TargetEntity.LogicalName.ToLower(), TargetEntity.Id, collection, startDate, endDate);
                }


                #endregion
            }
            
        }
        void CheckOverlapDate(ITracingService tracingService, bool isUpdate, string logicalName, Guid targetEntityId, EntityCollection collection, DateTime startDate, DateTime endDate)
        {
            string entityName = string.Empty;
            if (collection == null || collection.Entities.Count == 0) return;
            foreach (Entity entity in collection.Entities)
            {
                tracingService.Trace("Inside foreach loop");
                if (isUpdate && (entity.Id == targetEntityId))
                    continue;

                DateTime sDate = DateTime.MinValue;
                DateTime eDate = DateTime.MinValue;

                if (logicalName == PluginHelper.BpaEligibilitycategorydetail)
                {
                    entityName = "Eligibility Category Detail";
                    sDate = entity.GetAttributeValue<DateTime>("bpa_effectivedate");
                    eDate = entity.GetAttributeValue<DateTime>("bpa_expirationdate");
                }
                else if (logicalName == PluginHelper.BpaRate)
                {
                    entityName = "Rate";
                    sDate = entity.GetAttributeValue<DateTime>("bpa_effectivedate");
                    eDate = entity.GetAttributeValue<DateTime>("bpa_expirydate");
                }

                tracingService.Trace($"Entity Name: {entityName}, Start Date: {sDate}, End Date: {eDate} ");
                if (PluginHelper.DoesDateOverlap(sDate, eDate, startDate, endDate, tracingService))
                    throw new InvalidPluginExecutionException(OperationStatus.Failed, $@"Entered dates are overlap with another {entityName} record. Please verity and try again.");
            }
        }
    }
}
