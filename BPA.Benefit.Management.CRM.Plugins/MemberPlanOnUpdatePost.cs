﻿#region

using System;
using BPA.Benefit.Management.CRM.Plugins.Helper;
using Microsoft.Xrm.Sdk;

#endregion

namespace BPA.Benefit.Management.CRM.Plugins
{
    /// <summary>
    ///     MemberPlanOnUpdatePost PlugIn.
    /// </summary>
    public class MemberPlanOnUpdatePost : Plugin
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="MemberPlanOnUpdatePost" /> class.
        /// </summary>
        //public MemberPlanOnUpdatePost()
        //    : base(typeof(MemberPlanOnCreatePost))
        public MemberPlanOnUpdatePost(string unsecureString, string secureString)
            : base(typeof(MemberPlanOnUpdatePost), unsecureString, secureString)
        {
            RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(40, "Update",
                PluginHelper.BpaMemberplan, PostExecuteUpdate));

            // Note : you can register for more events here if this plugin is not specific to an individual entity and message combination.
            // You may also need to update your RegisterFile.crmregister plug-in registration file to reflect any change.
        }

        /// <summary>
        ///     Executes the plug-in.
        /// </summary>
        /// <param name="localContext">
        ///     The <see cref="Plugin.LocalPluginContext" /> which contains the
        ///     <see cref="IPluginExecutionContext" />,
        ///     <see cref="IOrganizationService" />
        ///     and <see cref="ITracingService" />
        /// </param>
        /// <remarks>
        ///     For improved performance, Microsoft Dynamics CRM caches plug-in instances.
        ///     The plug-in's Execute method should be written to be stateless as the constructor
        ///     is not called for every invocation of the plug-in. Also, multiple system threads
        ///     could execute the plug-in at the same time. All per invocation state information
        ///     is stored in the context. This means that you should not use global variables in plug-ins.
        /// </remarks>
        protected void PostExecuteUpdate(LocalPluginContext localContext)
        {
            // If local context not passed in, raise error
            if (localContext == null)
                throw new ArgumentNullException($"Local context not passed in.");

            // Create the context variables
            IPluginExecutionContext context = localContext.PluginExecutionContext;
            IOrganizationService service = localContext.OrganizationService;
            ITracingService tracingService = localContext.TracingService;
            IServiceProvider serviceProvider = localContext.ServiceProvider;

            // If Target is not passed in, raise error
            tracingService.Trace("If Target is not passed in, raise error");
            if (!context.InputParameters.Contains(PluginHelper.Target) ||
                !(context.InputParameters[PluginHelper.Target] is Entity))
                throw new ArgumentNullException($"Target is not of type Entity.");

            // Create the target entity & postImage
            tracingService.Trace("Create the target entity & postImage");
            Entity entity = (Entity)context.InputParameters[PluginHelper.Target];
            Entity postImageEntity = context.PostEntityImages != null &&
                                     context.PostEntityImages.Contains(PluginHelper.PostImageAlias)
                ? context.PostEntityImages[PluginHelper.PostImageAlias]
                : null;
            if (postImageEntity == null) return;


            //Fetch Admin REcord
            //Assigned Organization service to CRM Admin User for Deletion of transaction
            //string crmAdminUserId = PluginConfiguration[PluginHelper.ConfigCrmAdmin];
            string crmAdminUserId = string.Empty;
            try
            {
                crmAdminUserId = PluginConfiguration[PluginHelper.ConfigCrmAdmin];
            }
            catch (Exception ex)
            {
                // Get value from Database
                crmAdminUserId = ConfigurationHelper.FetchValueId(service, PluginHelper.ConfigCrmAdmin);
            }
            if (string.IsNullOrEmpty(crmAdminUserId))
            {
                tracingService.Trace("ERROR FROM MemberPlanOnUpdatePost Plugin - CRM Admin USer Could not Found.");
                throw new InvalidPluginExecutionException(OperationStatus.Failed, $@"The given key was not present in the dictionary. Please contact BPA Administrator");
            }

            IOrganizationServiceFactory serviceFactory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
            IOrganizationService serviceAdmin = serviceFactory.CreateOrganizationService(new Guid(crmAdminUserId));

            // Following method called only when Member is "RETIREE"
            tracingService.Trace("Following method called only when Member is 'RETIREE'");
            ContactHelper.CreateNewMemberTransactions(serviceAdmin, tracingService, entity, postImageEntity);


            #region -------------- Run RETRO PROCESSNG MANUALLY -------

            if ((entity.Contains("bpa_retroprocessingdate")))
            {
                //Trigger Rollup after Retro Processing done
                PluginHelper.TriggerRollup(serviceAdmin, tracingService, "bpa_memberplan", entity.Id, "bpa_contributiondollarbank");
                PluginHelper.TriggerRollup(serviceAdmin, tracingService, "bpa_memberplan", entity.Id, "bpa_contributionhourbank");
                PluginHelper.TriggerRollup(serviceAdmin, tracingService, "bpa_memberplan", entity.Id, "bpa_selfpaybank");
                PluginHelper.TriggerRollup(serviceAdmin, tracingService, "bpa_memberplan", entity.Id, "bpa_secondarydollarbank");

            }

            #endregion

        }
    }
}