﻿#region

using System;
using BPA.Benefit.Management.CRM.Plugins.Helper;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;

#endregion

namespace BPA.Benefit.Management.CRM.Plugins
{
    /// <summary>
    ///     EligibilityCategoryDetailOnCreateUpdatePre PlugIn.
    ///     This Plugin is used for validation of Eligibility Category is Reinstatement and Dollar/Hours value must be Greter or Equal of Deduction rate
    /// </summary>
    public class EligibilityCategoryDetailOnCreateUpdatePre : Plugin
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="EligibilityCategoryDetailOnCreateUpdatePre" /> class.
        /// </summary>
        public EligibilityCategoryDetailOnCreateUpdatePre() : base(typeof(EligibilityCategoryDetailOnCreateUpdatePre))
        {
            RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(20, "Create",
                PluginHelper.BpaEligibilitycategorydetail, ExecuteOnCreateUpdatePre));
            RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(20, "Update",
                PluginHelper.BpaEligibilitycategorydetail, ExecuteOnCreateUpdatePre));
            
            // Note : you can register for more events here if this plugin is not specific to an individual entity and message combination.
            // You may also need to update your RegisterFile.crmregister plug-in registration file to reflect any change.
        }

        /// <summary>
        ///     Executes the plug-in.
        /// </summary>
        /// <param name="localContext">
        ///     The <see cref="Plugin.LocalPluginContext" /> which contains the
        ///     <see cref="IPluginExecutionContext" />,
        ///     <see cref="IOrganizationService" />
        ///     and <see cref="ITracingService" />
        /// </param>
        /// <remarks>
        ///     For improved performance, Microsoft Dynamics CRM caches plug-in instances.
        ///     The plug-in's Execute method should be written to be stateless as the constructor
        ///     is not called for every invocation of the plug-in. Also, multiple system threads
        ///     could execute the plug-in at the same time. All per invocation state information
        ///     is stored in the context. This means that you should not use global variables in plug-ins.
        /// </remarks>

        protected void ExecuteOnCreateUpdatePre(LocalPluginContext localContext)
        {
            // DONE - TODO JK - Add comments and trace statements below

            // If local context not passed in, raise error
            if (localContext == null)
                throw new ArgumentNullException($"Local context not passed in.");

            // Create the context variables
            IPluginExecutionContext context = localContext.PluginExecutionContext;
            IOrganizationService service = localContext.OrganizationService;
            ITracingService tracingService = localContext.TracingService;

            // If Target is not passed in, raise error
            tracingService.Trace("If Target is not passed in, raise error");
            if (!context.InputParameters.Contains(PluginHelper.Target) ||
                !(context.InputParameters[PluginHelper.Target] is Entity))
                throw new ArgumentNullException($"Target is not of type Entity.");

            // Create the target entity & preImage
            tracingService.Trace("Create the target entity & preImage");
            Entity entity = (Entity)context.InputParameters[PluginHelper.Target];
            Entity preImage = context.PreEntityImages != null &&
                                    context.PreEntityImages.Contains(PluginHelper.PreImageAlias)
                ? context.PreEntityImages[PluginHelper.PreImageAlias] : null;
            if (context.MessageName.ToLower() == PluginHelper.Update.ToLower() && preImage == null)
                throw new ArgumentNullException($"preImage entity is not defined.");

            // Get all value from entity or pre image
            EntityReference eligibilityCategoryRef = null;
            if (entity.Contains("bpa_eligibilitycategory"))
                eligibilityCategoryRef = entity.GetAttributeValue<EntityReference>("bpa_eligibilitycategory");
            else if ( (preImage != null) && preImage.Contains("bpa_eligibilitycategory"))
                eligibilityCategoryRef = preImage.GetAttributeValue<EntityReference>("bpa_eligibilitycategory");
            tracingService.Trace($"Get eligibilityCategoryRef {eligibilityCategoryRef.Id}  and Name {eligibilityCategoryRef.Id}");
            
            // Retrive data  
            if (eligibilityCategoryRef == null) return;
            Entity eligibilityCategory = service.Retrieve(PluginHelper.BpaEligibilitycategory,
                eligibilityCategoryRef.Id, new ColumnSet("bpa_planeligibilitytype", "bpa_reinstatement"));
            tracingService.Trace($"Retrive Eligibility Category");

            if (eligibilityCategory == null) return;
            bool isReinstatement = eligibilityCategory.GetAttributeValue<bool>("bpa_reinstatement");
            tracingService.Trace($"Reinstatement {isReinstatement}");

            OptionSetValue categoryType =
                eligibilityCategory.GetAttributeValue<OptionSetValue>("bpa_planeligibilitytype");
            tracingService.Trace($"Category Type {categoryType.Value}");
            
            //Validation by Hours / Dollar
            switch (categoryType.Value)
            {
                case (int)BpaEligibilityCategoryPlanEligiblityType.Hours:
                    tracingService.Trace($"Inside of Hours Case");
                    decimal hoursdeductionrate = 0;
                    if (entity.Contains("bpa_hoursdeductionrate"))
                        hoursdeductionrate = entity.GetAttributeValue<decimal>("bpa_hoursdeductionrate");
                    else if ((preImage != null) && preImage.Contains("bpa_hoursdeductionrate"))
                        hoursdeductionrate = preImage.GetAttributeValue<decimal>("bpa_hoursdeductionrate");
                    tracingService.Trace($"Hours Value {hoursdeductionrate.ToString()}");

                    decimal reinstatementhours = 0;
                    if (entity.Contains("bpa_reinstatementhours"))
                        reinstatementhours = entity.GetAttributeValue<decimal>("bpa_reinstatementhours");
                    else if ((preImage != null) && preImage.Contains("bpa_reinstatementhours"))
                        reinstatementhours = preImage.GetAttributeValue<decimal>("bpa_reinstatementhours");
                    tracingService.Trace($"Reinstatement Hours Value {reinstatementhours.ToString()}");

                    decimal hoursfundassistanceamount = 0;
                    if (entity.Contains("bpa_hoursfundassistanceamount"))
                        hoursfundassistanceamount = entity.GetAttributeValue<decimal>("bpa_hoursfundassistanceamount");
                    else if ((preImage != null) && preImage.Contains("bpa_hoursfundassistanceamount"))
                        hoursfundassistanceamount = preImage.GetAttributeValue<decimal>("bpa_hoursfundassistanceamount");
                    tracingService.Trace($"Hour Fund Assistance Value {hoursfundassistanceamount.ToString()}");
                    
                    //Reinstatement and Hour deduction validation
                    if (reinstatementhours < hoursdeductionrate)
                        throw new InvalidPluginExecutionException(OperationStatus.Canceled,
                            "Reinstatement Hours value must be greater or equal to Hours Deduction Rate");
                    
                    //Fund Assistance and Hour deduction validation
                    if ((hoursfundassistanceamount != 0) && (hoursfundassistanceamount != hoursdeductionrate) )
                        throw new InvalidPluginExecutionException(OperationStatus.Canceled,
                            "Fund Assistance amount must be equal to the deduction rate, 0 – (Null/Blank) in an hourly plan.  Please update Fund Assistance amount before saving.");
                    break;
                case (int)BpaEligibilityCategoryPlanEligiblityType.Dollar:
                    tracingService.Trace($"Inside of Dollar Case");
                    
                    Money dollardeductionrate = new Money(0);
                    if (entity.Contains("bpa_deductionrate"))
                        dollardeductionrate = entity.GetAttributeValue<Money>("bpa_deductionrate");
                    else if ((preImage != null) && preImage.Contains("bpa_deductionrate"))
                        dollardeductionrate = preImage.GetAttributeValue<Money>("bpa_deductionrate");
                    tracingService.Trace($"Dollar Value {dollardeductionrate.Value.ToString()}");

                    Money reinstatementdollar = new Money(0);
                    if (entity.Contains("bpa_reinstatementdollar"))
                        reinstatementdollar = entity.GetAttributeValue<Money>("bpa_reinstatementdollar");
                    else if ((preImage != null) && preImage.Contains("bpa_reinstatementdollar"))
                        reinstatementdollar = preImage.GetAttributeValue<Money>("bpa_reinstatementdollar");
                    tracingService.Trace($"Reinstatement Dollar Value {reinstatementdollar.Value.ToString()}");
                    
                    decimal dollarfundassistanceamount = 0;
                    if (entity.Contains("bpa_fundassistanceamount") && entity["bpa_fundassistanceamount"] != null)
                        dollarfundassistanceamount = entity.GetAttributeValue<Money>("bpa_fundassistanceamount").Value;
                    else if ((preImage != null) && preImage.Contains("bpa_fundassistanceamount"))
                        dollarfundassistanceamount = preImage.GetAttributeValue<Money>("bpa_fundassistanceamount").Value;
                    tracingService.Trace($"Hour Fund Assistance Value {dollarfundassistanceamount.ToString()}");

                    //Reinstatement and Dollar deduction validation
                    if (reinstatementdollar.Value < dollardeductionrate.Value)
                        throw new InvalidPluginExecutionException(OperationStatus.Canceled,
                            "Reinstatement Dollars value must be greater or equal to Dollars Deduction Rate");
                    
                    //Fund Assistance and Dollar deduction validation
                    if ((dollarfundassistanceamount != 0) && (dollarfundassistanceamount > dollardeductionrate.Value))
                        throw new InvalidPluginExecutionException(OperationStatus.Canceled,
                            "Fund Assistance amount must be less than or equal to the deduction rate and greater than or equal to 0 - (Null/Blank) in a dollar type plan.  Please update Fund Assistance amount before saving.");

                    break;
            }
        }
    }
}