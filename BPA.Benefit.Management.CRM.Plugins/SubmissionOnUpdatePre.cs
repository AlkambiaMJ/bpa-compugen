﻿#region

using System;
using System.Diagnostics;
using BPA.Benefit.Management.CRM.Plugins.Helper;
using Microsoft.Xrm.Sdk;
using System.Text;

#endregion

namespace BPA.Benefit.Management.CRM.Plugins
{
    /// <summary>
    ///     SubmissionOnUpdatePre PlugIn.
    /// </summary>
    public class SubmissionOnUpdatePre : Plugin
    {
        private Money SubmissionDue = new Money(Decimal.Zero);
        /// <summary>
        ///     Initializes a new instance of the <see cref="SubmissionOnUpdatePre" /> class.
        /// </summary>
        
        /// <summary>
        ///     Initializes a new instance of the <see cref="SubmissionOnUpdatePost" /> class.
        /// </summary>
        public SubmissionOnUpdatePre(string unsecureString, string secureString)
            : base(typeof(SubmissionOnUpdatePre), unsecureString, secureString)
        {
            RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(20, "Update",
                PluginHelper.BpaSubmission, ExecutePreSubmissionUpdate));

            // Note : you can register for more events here if this plugin is not specific to an individual entity and message combination.
            // You may also need to update your RegisterFile.crmregister plug-in registration file to reflect any change.
        }


        /// <summary>
        ///     Executes the plug-in.
        /// </summary>
        /// <param name="localContext">
        ///     The <see cref="Plugin.LocalPluginContext" /> which contains the
        ///     <see cref="IPluginExecutionContext" />,
        ///     <see cref="IOrganizationService" />
        ///     and <see cref="ITracingService" />
        /// </param>
        /// <remarks>
        ///     For improved performance, Microsoft Dynamics CRM caches plug-in instances.
        ///     The plug-in's Execute method should be written to be stateless as the constructor
        ///     is not called for every invocation of the plug-in. Also, multiple system threads
        ///     could execute the plug-in at the same time. All per invocation state information
        ///     is stored in the context. This means that you should not use global variables in plug-ins.
        /// </remarks>
        protected void ExecutePreSubmissionUpdate(LocalPluginContext localContext)
        {
            // DONE - TODO JK - Add comments and trace statements

            // If local context not passed in, raise error
            if (localContext == null)
                throw new ArgumentNullException($"Local context not passed in.");

            // Create the context variables
            IPluginExecutionContext context = localContext.PluginExecutionContext;
            ITracingService tracingService = localContext.TracingService;
            IOrganizationService service = localContext.OrganizationService;

            // If Target is not passed in, raise error
            tracingService.Trace("If Target is not passed in, raise error");
            if (!context.InputParameters.Contains(PluginHelper.Target) ||
                !(context.InputParameters[PluginHelper.Target] is Entity))
                throw new ArgumentNullException($"Target is not of type Entity.");

            // Create the target entity & preImage
            tracingService.Trace("Create the target entity & preImage");
            Entity entity = (Entity)context.InputParameters[PluginHelper.Target];
            Entity preImageEntity = context.PreEntityImages != null &&
                                    context.PreEntityImages.Contains(PluginHelper.PreImageAlias)
                ? context.PreEntityImages[PluginHelper.PreImageAlias]
                : null;
            if (preImageEntity == null)
                throw new ArgumentNullException($"preImage entity is not defined.");

            if (context.Depth > 3)
                return;

            //Check Submission Cancelled or not?
            if (entity.Contains("statecode") && entity.Contains("statuscode"))
            {
                OptionSetValue statecode = entity.Contains("statecode") ? entity.GetAttributeValue<OptionSetValue>("statecode") : new OptionSetValue(0);
                OptionSetValue statuscode = entity.Contains("statuscode") ? entity.GetAttributeValue<OptionSetValue>("statuscode") : new OptionSetValue(0);

                //Submission Inactive(1) and Cancelled( 922070002)
                if (statecode.Value == 1 && statuscode.Value == 922070002)
                {
                    entity["bpa_submissionstatus"] = new OptionSetValue((int)SubmissionStatus.Cancelled);
                }
            }



            if(entity.Contains("stageid") || entity.Contains("bpa_submissionpaid"))
            {

                Guid Current_StageId = entity.GetAttributeValue<Guid>("stageid");
                Guid Pre_StageId = preImageEntity.GetAttributeValue<Guid>("stageid");

                OptionSetValue Pre_status = preImageEntity.GetAttributeValue<OptionSetValue>("bpa_submissionstatus");
                Money submissionPaid1 = entity.Contains("bpa_submissionpaid") ? entity.GetAttributeValue<Money>("bpa_submissionpaid") : null;

                if ((Current_StageId == PluginHelper.SubmissionStageIdDepositPending)  || ((Pre_status.Value == (int)SubmissionStatus.Verification) && submissionPaid1 != null))
                {
                    StringBuilder sbError = new StringBuilder();

                    OptionSetValue pt = new OptionSetValue(0);
                    if (entity.Contains("bpa_paymentmethod"))
                        pt = entity.GetAttributeValue<OptionSetValue>("bpa_paymentmethod");
                    else if (preImageEntity.Contains("bpa_paymentmethod"))
                        pt = preImageEntity.GetAttributeValue<OptionSetValue>("bpa_paymentmethod");

                    OptionSetValue st = new OptionSetValue(0);
                    if (entity.Contains("bpa_submissiontype"))
                        st = entity.GetAttributeValue<OptionSetValue>("bpa_submissiontype");
                    else if (preImageEntity.Contains("bpa_submissiontype"))
                        st = preImageEntity.GetAttributeValue<OptionSetValue>("bpa_submissiontype");

                    if (st.Value != (int)SubmissionType.NoActivity)
                    {
                        if (pt == null || pt.Value == 0)
                            sbError.Append("- Payment Method");

                        // Validating Submission required field when user go from 'Verification' to 'Deposit Pending' when payment method is not 'No Payment'
                        if (pt != null && pt.Value != (int)SubmissionPaymentType.NoPayment)
                        {
                            Money submissionPaid = null;
                            if (entity.Contains("bpa_submissionpaid"))
                                submissionPaid = entity.GetAttributeValue<Money>("bpa_submissionpaid");
                            else if (preImageEntity.Contains("bpa_submissionpaid"))
                                submissionPaid = preImageEntity.GetAttributeValue<Money>("bpa_submissionpaid");

                            if (submissionPaid == null)
                                sbError.Append("<br />- Submission Amount Paid");

                            string paymentReference = string.Empty;
                            if (entity.Contains("bpa_paymentreferencenumber"))
                                paymentReference = entity.GetAttributeValue<string>("bpa_paymentreferencenumber");
                            else if (preImageEntity.Contains("bpa_paymentreferencenumber"))
                                paymentReference = preImageEntity.GetAttributeValue<string>("bpa_paymentreferencenumber");

                            if (string.IsNullOrEmpty(paymentReference))
                                sbError.Append("<br />- Payment Reference Number");

                            DateTime paymentReceivedDate = DateTime.MinValue;
                            if (entity.Contains("bpa_paymentrecieveddate"))
                                paymentReceivedDate = entity.GetAttributeValue<DateTime>("bpa_paymentrecieveddate");
                            else if (preImageEntity.Contains("bpa_paymentrecieveddate"))
                                paymentReceivedDate = preImageEntity.GetAttributeValue<DateTime>("bpa_paymentrecieveddate");

                            if (paymentReceivedDate == DateTime.MinValue)
                                sbError.Append("<br />- Payment Received Date");
                        }
                        if (!string.IsNullOrEmpty(sbError.ToString()))
                            throw new InvalidPluginExecutionException(OperationStatus.Failed, $@"Please provide values for the following fields before proceeding: <br />{sbError.ToString()}<br />");
                    }
                }
            }




            if (entity.Contains("bpa_submissionstatus"))
            {
               
                EntityReference employerRef = preImageEntity.Attributes.Contains("bpa_accountagreementid")
                    ? preImageEntity.GetAttributeValue<EntityReference>("bpa_accountagreementid")
                    : null;
                OptionSetValue SubmissionStatus = entity.GetAttributeValue<OptionSetValue>("bpa_submissionstatus");
                OptionSetValue Pre_SubmissionStatus = new OptionSetValue(0);
                if (preImageEntity.Contains("bpa_submissionstatus"))
                    Pre_SubmissionStatus = preImageEntity.GetAttributeValue<OptionSetValue>("bpa_submissionstatus");

                OptionSetValue paymentType = new OptionSetValue(0);
                if (entity.Attributes.Contains("bpa_paymentmethod"))
                    paymentType = entity.GetAttributeValue<OptionSetValue>("bpa_paymentmethod");
                else if (preImageEntity.Attributes.Contains("bpa_paymentmethod"))
                    paymentType = preImageEntity.GetAttributeValue<OptionSetValue>("bpa_paymentmethod");

                OptionSetValue submissionTypeNew = new OptionSetValue(0);
                if (entity.Attributes.Contains("bpa_submissiontype"))
                    submissionTypeNew = entity.GetAttributeValue<OptionSetValue>("bpa_submissiontype");
                else if (preImageEntity.Attributes.Contains("bpa_submissiontype"))
                    submissionTypeNew = preImageEntity.GetAttributeValue<OptionSetValue>("bpa_submissiontype");

                // Find the trust for that employer
                Guid trustId = AccountAgreementHelper.FetchTrustByAccountId(service, employerRef.Id);
                Guid depositId = Guid.Empty;
                               

                if ((SubmissionStatus.Value == (int)Helper.SubmissionStatus.PendingDeposit) &&
                        (Pre_SubmissionStatus.Value == (int)Helper.SubmissionStatus.Verification))
                {


                    if (entity.Contains("bpa_paymentmethod"))
                        paymentType = entity.GetAttributeValue<OptionSetValue>("bpa_paymentmethod");
                    else if (preImageEntity.Contains("bpa_paymentmethod"))
                        paymentType = preImageEntity.GetAttributeValue<OptionSetValue>("bpa_paymentmethod");

                    //if ((paymentType.Value == (int)SubmissionPaymentType.NoPayment))
                    //{
                    //    //DO NOT GENERATE/CREATE/VERIFIED
                    //}
                    //else
                    //{
                        if ((submissionTypeNew.Value == (int)SubmissionSubmissionType.EmployerRegular) 
                        || (submissionTypeNew.Value == (int)SubmissionSubmissionType.EmployerAdjustment)
                            || (submissionTypeNew.Value == (int)SubmissionSubmissionType.EmployerMoneyOnly)
                            || (submissionTypeNew.Value == (int)SubmissionSubmissionType.EmployerNoActivity)
                            || (submissionTypeNew.Value == (int)SubmissionSubmissionType.Member)
                             || (submissionTypeNew.Value == (int)SubmissionSubmissionType.MemberSelfPayRefund))
                        {
                            // Find is Deposit record exists
                            depositId = DepositHelper.IsDepositExists(service, trustId, context.UserId, paymentType.Value);
                            if (depositId == Guid.Empty)
                            {

                                string defaultCurrency = string.Empty;
                                try
                                {
                                    defaultCurrency = PluginConfiguration[PluginHelper.ConfigDefaultCurrency];
                                }
                                catch (Exception ex)
                                {
                                    // Get value from Database
                                    defaultCurrency = ConfigurationHelper.FetchValueId(service, PluginHelper.ConfigDefaultCurrency);
                                }
                                if (string.IsNullOrEmpty(defaultCurrency))
                                {
                                    tracingService.Trace("ERROR FROM SubmissionOnUpdatePre Plugin - CRM Default Currency Could not Found.");
                                    throw new InvalidPluginExecutionException(OperationStatus.Failed, $@"The given key was not present in the dictionary. Please contact BPA Administrator");
                                }
                                // Create Deposit 
                                depositId = DepositHelper.CreateDeposit(service, trustId, paymentType.Value, defaultCurrency);
                            }
                        }
                    //}

                    if (submissionTypeNew.Value != (int)SubmissionSubmissionType.RetireeMonthlySubmission)
                        entity["bpa_depositid"] = new EntityReference(PluginHelper.BpaDeposit, depositId);
                }
            }

            OptionSetValue submissiontype = preImageEntity.GetAttributeValue<OptionSetValue>("bpa_submissiontype");

            if ((submissiontype.Value == (int)SubmissionSubmissionTypeNew.EmployerAdjustment) ||
                (submissiontype.Value == (int)SubmissionSubmissionTypeNew.EmployerRegular)
                || (submissiontype.Value == (int)SubmissionSubmissionTypeNew.EmployerMoneyOnly) 
                || (submissiontype.Value == (int)SubmissionSubmissionTypeNew.Member)
                || (submissiontype.Value == (int)SubmissionSubmissionTypeNew.MemberSelfPayRefund))
            {
                tracingService.Trace($"Inside only employer submission");
                if (ValidateSubmission(service, tracingService, entity, preImageEntity))
                    throw new InvalidPluginExecutionException(
                        "You are attempting to change the status of a submission in which one of the two conditions exist.\n\t1. There are prior submissions in Pending or Verification Status. \n\t2. There are future submissions in 'Deposit Pending' or 'Completed' status.");
            }


            // This method set the Submission Amount, Opening Variance
            tracingService.Trace("set the Submission Amount, Opening Variance");
            SetSubmissionAmounts(service, tracingService, entity, preImageEntity);

            // Generate any Interest (Delinquency) charge if the Submission is late
            // Steps to Calculate Delinquency:
            // Get the Delinquency rule that is approprate for this Submission
            // Get the Submission due date and payment date
            // Run though the calculations to figure out the Delinquency amount
            // Create a Interest (Delinquency) value if the amount is > $0
            
            tracingService.Trace("End UpdateSubmissionAmounts - Method");
        }

        bool ValidateSubmission(IOrganizationService service, ITracingService tracingService, Entity entity,
            Entity preImageEntity)
        {
            tracingService.Trace($"Inside ValidateSubmission Method");
            bool IsValid = false;

            EntityReference Employer = preImageEntity.GetAttributeValue<EntityReference>("bpa_accountagreementid");
            DateTime CreatedOn = preImageEntity.GetAttributeValue<DateTime>("createdon");

            OptionSetValue SubmissionStatus = new OptionSetValue(0);
            OptionSetValue Pre_SubmissionStatus = new OptionSetValue(0);
            if (preImageEntity.Contains("bpa_submissionstatus"))
                Pre_SubmissionStatus = preImageEntity.GetAttributeValue<OptionSetValue>("bpa_submissionstatus");
            tracingService.Trace($"Pre_SubmissionStatus = {Pre_SubmissionStatus.Value}");

            if (entity.Contains("bpa_submissionstatus"))
            {
                tracingService.Trace($"Inside bpa_submissionstatus contains");
                SubmissionStatus = entity.GetAttributeValue<OptionSetValue>("bpa_submissionstatus");

                if (((Pre_SubmissionStatus.Value == (int)Helper.SubmissionStatus.Pending) && (SubmissionStatus.Value == (int)Helper.SubmissionStatus.Verification)) ||
                    ((Pre_SubmissionStatus.Value == (int)Helper.SubmissionStatus.Verification) && ((SubmissionStatus.Value == (int)Helper.SubmissionStatus.PendingDeposit) ||
                      (SubmissionStatus.Value == (int)Helper.SubmissionStatus.Completed))))
                {
                    tracingService.Trace($"Inside checking forward and backward");

                    //Check Forward - Is there any submission exists where Submission Status IN (verification, Deposit Pending, Completed)
                    if (SubmissionHelper.IsSubmissionExistsForward(service, tracingService, Employer.Id,
                        CreatedOn.ToLocalTime()))
                        IsValid = true;

                    //Check BackWard - Is there any submission exists where Submission Status IN (Pending, Verification)
                    if (SubmissionHelper.IsSubmissionExistsBackward(service, tracingService, Employer.Id,
                        CreatedOn.ToLocalTime()))
                        IsValid = true;
                }
            }
            
            return IsValid;
        }
        
        void SetSubmissionAmounts(IOrganizationService service, ITracingService tracingService, Entity entity,
            Entity preImageEntity)
        {
            tracingService.Trace("Inside SetSubmissionAmounts.");
            if (entity.Attributes.Contains("bpa_recalucatetotal"))
            {
                bool Recalucatetotal = false;
                Recalucatetotal = entity.GetAttributeValue<bool>("bpa_recalucatetotal");

                if (Recalucatetotal)
                {
                    tracingService.Trace($"bpa_recalucatetotal = {Recalucatetotal}");

                    EntityReference employerId = preImageEntity.Attributes.Contains("bpa_accountagreementid")
                        ? preImageEntity.GetAttributeValue<EntityReference>("bpa_accountagreementid") : null;
                    DateTime CreatedOn = preImageEntity.Attributes.Contains("createdon")
                        ? preImageEntity.GetAttributeValue<DateTime>("createdon") : DateTime.MinValue;
                    tracingService.Trace("1. Get All Values");

                    decimal SumHours = 0, SumHoursEarned = 0;
                    Money SumGrossWages = new Money(0);
                    Money SumDollars = new Money(0);

                    //Fire a query and get all totals of Hours, Gross wages and Dollars from Submission Details 
                    Entity SumResult = SubmissionHelper.FetchSubmissionTotal(service, tracingService, entity.Id);
                    if (SumResult != null)
                    {
                        tracingService.Trace($"Inside if (SumResult != null)");

                        if (SumResult.Attributes.Contains("bpa_dollars") &&
                            ((AliasedValue)SumResult.Attributes["bpa_dollars"]).Value != null)
                            SumDollars = (Money)((AliasedValue)SumResult.Attributes["bpa_dollars"]).Value;
                        if (SumResult.Attributes.Contains("bpa_grosswages") &&
                            ((AliasedValue)SumResult.Attributes["bpa_grosswages"]).Value != null)
                            SumGrossWages = (Money)((AliasedValue)SumResult.Attributes["bpa_grosswages"]).Value;
                        if (SumResult.Attributes.Contains("bpa_totalhours") &&
                            ((AliasedValue)SumResult.Attributes["bpa_totalhours"]).Value != null)
                            SumHours = (decimal)((AliasedValue)SumResult.Attributes["bpa_totalhours"]).Value;

                        if (SumResult.Attributes.Contains("bpa_hoursearned") && ((AliasedValue)SumResult.Attributes["bpa_hoursearned"]).Value != null)
                            SumHoursEarned = (decimal)((AliasedValue)SumResult.Attributes["bpa_hoursearned"]).Value;

                    }

                    //Find the total of Submission Due(Sum of all Conributions)
                    SubmissionDue = ContributionHelper.FetchContributionTotalBySubmissionId(service, entity.Id);
                    tracingService.Trace($"Submission Due {SubmissionDue.Value}");

                    decimal OpeningBalance = 0;
                    if (employerId != null && CreatedOn != DateTime.MinValue)
                    {
                        OpeningBalance = SubmissionHelper.GetOpeningVariance(service, tracingService, employerId.Id,
                            CreatedOn);
                        tracingService.Trace("2 Opening Balance - " + OpeningBalance);
                    }

                    //get previous intereset Balance
                    decimal previousInetersetBalance = SubmissionAdjustmentHelper.FetchPreviousInteresetBalance(service, tracingService, employerId.Id, CreatedOn, entity.Id);


                    tracingService.Trace("Setting values");

                    entity["bpa_totalhourssubmitted"] = SumHours;
                    entity["bpa_totalhoursearnedsubmitted"] = SumHoursEarned;
                    entity["bpa_totaldollarssubmitted"] = SumDollars;
                    entity["bpa_totalgrosswagessubmitted"] = SumGrossWages;
                    entity["bpa_submissionamount"] = SubmissionDue;
                    entity["bpa_openingvariance"] = new Money(OpeningBalance);
                    entity["bpa_totalsubmissiondue"] = new Money(SubmissionDue.Value + OpeningBalance);
                    entity["bpa_previousinteresetbalance"] = new Money(previousInetersetBalance);
                    entity["bpa_totalsubmissiondueincludinginterest"] = new Money(SubmissionDue.Value + OpeningBalance + previousInetersetBalance);
                    tracingService.Trace("Set value before submission - " + OpeningBalance);
                }
            }
        }
    }
}