﻿#region

using System;
using BPA.Benefit.Management.CRM.Plugins.Helper;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using System.Text;

#endregion

namespace BPA.Benefit.Management.CRM.Plugins
{
    /// <summary>
    ///     SubmissionDetailOnCreatePre PlugIn.
    /// </summary>
    public class SubmissionDetailOnCreatePre : Plugin
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="PaymentBatchOnUpdatePre" /> class.
        /// </summary>
        public SubmissionDetailOnCreatePre(string unsecureString, string secureString)
            : base(typeof(SubmissionDetailOnCreatePre), unsecureString, secureString)
        {
            RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(20, PluginHelper.Create,
                PluginHelper.BpaSubmissiondetail, ExecuteOnCreatePre));

            // Note : you can register for more events here if this plugin is not specific to an individual entity and message combination.
            // You may also need to update your RegisterFile.crmregister plug-in registration file to reflect any change.
        }

        /// <summary>
        ///     Executes the plug-in.
        /// </summary>
        /// <param name="localContext">
        ///     The <see cref="Plugin.LocalPluginContext" /> which contains the
        ///     <see cref="IPluginExecutionContext" />,
        ///     <see cref="IOrganizationService" />
        ///     and <see cref="ITracingService" />
        /// </param>
        /// <remarks>
        ///     For improved performance, Microsoft Dynamics CRM caches plug-in instances.
        ///     The plug-in's Execute method should be written to be stateless as the constructor
        ///     is not called for every invocation of the plug-in. Also, multiple system threads
        ///     could execute the plug-in at the same time. All per invocation state information
        ///     is stored in the context. This means that you should not use global variables in plug-ins.
        /// </remarks>
        protected void ExecuteOnCreatePre(LocalPluginContext localContext)
        {
            // If local context not passed in, raise error
            if (localContext == null)
                throw new ArgumentNullException($"Local context not passed in.");

            // Create the context variables
            IPluginExecutionContext context = localContext.PluginExecutionContext;
            IOrganizationService service = localContext.OrganizationService;
            ITracingService tracingService = localContext.TracingService;

            // If Target is not passed in, raise error
            tracingService.Trace("If Target is not passed in, raise error");
            if (!context.InputParameters.Contains(PluginHelper.Target) ||
                !(context.InputParameters[PluginHelper.Target] is Entity))
                throw new ArgumentNullException($"Target is not of type Entity.");

            // Create the target entity
            tracingService.Trace("Create the target entity");
            Entity entity = (Entity)context.InputParameters[PluginHelper.Target];

            // If the Complete Batch flag is checked
            localContext.Trace("Start the ExecuteOnCreatePre.");

            if (context.Depth > 1) return; 

            EntityReference employmentRecord = null, jobClassification = null, memberPlan1 = null;


            // Validation - for duplicate Same employment record 
            string validate = ValidateDuplicateDetail(service, tracingService, entity, out employmentRecord, out jobClassification, out memberPlan1);
            if (!string.IsNullOrEmpty(validate))
            {
                // we remarked this logic as  new requirement Task# 1343 - Multiple submisison details on a submission
                //throw new InvalidPluginExecutionException(OperationStatus.Canceled,
                //    $@"The Member {validate} you are adding already has a submission detail record created. Please select another Member.");
                entity["bpa_labourroleid"] = jobClassification;
                entity["bpa_employmentid"] = employmentRecord;
                entity["bpa_memberplanid"] = memberPlan1;
            }
            else
            {
                // Fetch Submission Detail
                EntityReference submissionRef = entity.GetAttributeValue<EntityReference>("bpa_submissionid");
                tracingService.Trace("Fetch Submission Detail");
                Entity submission = SubmissionHelper.FetchSubmissionDetail(service, tracingService, submissionRef.Id);
                EntityReference planRef = null;
                if (submission != null && submission.Contains("bpa_plan"))
                    planRef = submission.GetAttributeValue<EntityReference>("bpa_plan");
                if (planRef == null)
                    throw new InvalidPluginExecutionException(OperationStatus.Canceled, @"Plan is undefined.");


                string memberPlanIdName = string.Empty;

                // Check if Submission Detail - Submission Member Type is New Member
                tracingService.Trace("Check if Submission Detail - Submission Member Type is New Member");
                if (entity.Contains("bpa_membersin"))
                {
                    #region ---------------- NEW MEMBER --------------------

                    string firstname = string.Empty; // entity.GetAttributeValue<string>("bpa_memberfirstname");
                    string lastname = string.Empty; //entity.GetAttributeValue<string>("bpa_memberlastname");
                    string socialinsuranceno = entity.GetAttributeValue<string>("bpa_membersin");

                    DateTime submissionDate = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1);
                    if (submission != null && submission.Contains("bpa_submissiondate"))
                        submissionDate = submission.GetAttributeValue<DateTime>("bpa_submissiondate");
                    tracingService.Trace($"Fetch Submission Date {submissionDate.ToString()}");

                    // Fetch from plan
                    EntityReference trustRef = null;
                    Entity plan = service.Retrieve(PluginHelper.BpaSector, planRef.Id, new ColumnSet(true));
                    if (plan.Contains("bpa_trust"))
                        trustRef = plan.GetAttributeValue<EntityReference>("bpa_trust");
                    EntityReference agreementRef = null;
                    if (entity.Contains("bpa_agreementid"))
                        agreementRef = entity.GetAttributeValue<EntityReference>("bpa_agreementid");

                    //Fetch Union from Agreement
                    EntityReference HomeLocal = null;
                    Entity agreementInfo = service.Retrieve(PluginHelper.BpaSubsector, agreementRef.Id, new ColumnSet(true));
                    if (agreementInfo != null && agreementInfo.Contains("bpa_unionid"))
                        HomeLocal = agreementInfo.GetAttributeValue<EntityReference>("bpa_unionid");

                    //Fetch Default Labour role
                    EntityReference labourRole = null;
                    if (!entity.Contains("bpa_labourroleid"))
                    {
                        Guid labourrole = RateHelper.FetchLabourRoleByAgreementId(service, agreementRef.Id);
                        if (labourrole != Guid.Empty)
                            labourRole = new EntityReference(PluginHelper.BpaLabourrole, labourrole);
                    }
                    else
                        labourRole = entity.GetAttributeValue<EntityReference>("bpa_labourroleid");

                    // Fetch Employer Default Eligibility 
                    tracingService.Trace("Fetch Employer Default Eligibility");
                    EntityReference employerRef = entity.GetAttributeValue<EntityReference>("bpa_accountagreementid");
                    EntityReference defaultEligibilityRef = null;
                    Entity account = AccountAgreementHelper.FetchAccountDetail(service, tracingService, employerRef.Id);
                    if (account != null && account.Contains("bpa_defaulteligibilityid"))
                        defaultEligibilityRef = account.GetAttributeValue<EntityReference>("bpa_defaulteligibilityid");

                    bool isMemberExists = false;
                    Entity existsMember = null;
                    if (!string.IsNullOrEmpty(socialinsuranceno))
                    {
                        // Check SIN number in Hash SIN
                        tracingService.Trace($@"Check Hash SIN already in the database?- {socialinsuranceno}");
                        EntityCollection existContacts = MemberHelper.FetchContactByHashSIN(service, tracingService, socialinsuranceno);

                        if (existContacts != null && existContacts.Entities.Count > 1)
                        {
                            throw new InvalidPluginExecutionException(OperationStatus.Failed, "More than on contact found please clean up data or contact BPA Administrator.");
                        }
                        else if (existContacts != null && existContacts.Entities.Count == 1)
                        {
                            existsMember = existContacts.Entities[0];
                            socialinsuranceno = existsMember.Contains("bpa_socialinsurancenumber") ? existsMember.GetAttributeValue<string>("bpa_socialinsurancenumber") : socialinsuranceno;
                        }
                        else
                        {
                            //// Check this Social number is already in the database?
                            tracingService.Trace("MemberHelper.FetchContactBySIN --> Check this SIN is already in the database?");
                            //existsMember = MemberHelper.FetchMemberBySin(service, socialinsuranceno);
                            //CHECK BY SIN 

                            existContacts = MemberHelper.FetchContactBySIN(service, tracingService, socialinsuranceno);
                            if (existContacts != null && existContacts.Entities.Count > 1)
                            {
                                tracingService.Trace($@"POINT 1 : Checking more than on Contact As SIN- {socialinsuranceno}");


                                throw new InvalidPluginExecutionException(OperationStatus.Failed, "SIN you entered is associated more than one SIN. Please adjust record prior to submission.");
                            }
                            else if (existContacts != null && existContacts.Entities.Count == 1)
                            {
                                existsMember = existContacts.Entities[0];
                                socialinsuranceno = existsMember.Contains("bpa_socialinsurancenumber") ? existsMember.GetAttributeValue<string>("bpa_socialinsurancenumber") : socialinsuranceno;
                            }
                        }


                        if (existsMember != null)
                        {
                            firstname = existsMember.Contains("firstname") ? existsMember.GetAttributeValue<string>("firstname") : firstname;
                            lastname = existsMember.Contains("lastname") ? existsMember.GetAttributeValue<string>("lastname") : lastname;

                            isMemberExists = true;
                        }
                    }
                    tracingService.Trace($@"Is MEMBER Exists: {isMemberExists}");
                    if (isMemberExists)
                    {
                        // Does that member has Member Plan Detail?
                        tracingService.Trace("Does that member has Member Plan Detail?");
                        Entity existsMemberPlan = ContactHelper.FetchMemberPlan(service, tracingService, existsMember.Id, planRef.Id);
                        Guid employmentId;
                        Guid memberPlanDetailId;


                        string defaultCurrency = string.Empty;
                        try
                        {
                            defaultCurrency = PluginConfiguration[PluginHelper.ConfigDefaultCurrency];
                        }
                        catch (Exception ex)
                        {
                            // Get value from Database
                            defaultCurrency = ConfigurationHelper.FetchValueId(service, PluginHelper.ConfigDefaultCurrency);
                        }
                        if (string.IsNullOrEmpty(defaultCurrency))
                        {
                            tracingService.Trace("ERROR FROM AdjustmentOnPostUpdate Plugin - CRM Default Currency Could not Found.");
                            throw new InvalidPluginExecutionException(OperationStatus.Failed, $@"The given key was not present in the dictionary. Please contact BPA Administrator");
                        }
                        if (existsMemberPlan != null)
                        {
                            Entity memberPlanDetail = new Entity(PluginHelper.BpaMemberplan)
                            {
                                Id = existsMemberPlan.Id,
                                //["bpa_name"] = firstname + ' ' + lastname,
                                ["bpa_socialinsurancenumber"] = socialinsuranceno
                            };

                            //memberPlanDetail["bpa_contactid"] = new EntityReference(PluginHelper.contact, ExistsMember.Id);
                            service.Update(memberPlanDetail);

                            // Set Member Plan Detail value
                            tracingService.Trace("Set Member Plan Detail value");
                            memberPlanIdName = existsMemberPlan.GetAttributeValue<string>("bpa_name");
                            memberPlanDetailId = existsMemberPlan.Id;

                            // Does that member is employee of that Employer?
                            tracingService.Trace("Does that member is employee of that Employer?");
                            Entity existsEmployment = EmploymentHelper.IsEmploymentExists(service, existsMemberPlan.Id, account.Id);
                            if (existsEmployment != null)
                            {
                                // Get from Existing Employment REcord
                                if (!entity.Contains("bpa_labourroleid"))
                                    labourRole = existsEmployment.GetAttributeValue<EntityReference>("bpa_labourrole");

                                Entity employment = new Entity(PluginHelper.BpaEmployment)
                                {
                                    Id = existsEmployment.Id,
                                    //["bpa_name"] = firstname + ' ' + lastname,
                                    ["bpa_labourrole"] = entity.GetAttributeValue<EntityReference>("bpa_labourroleid")
                                };
                                service.Update(employment);

                                // Set Employment value
                                tracingService.Trace("Set Employment value");
                                employmentId = existsEmployment.Id;
                            }
                            else
                            {
                                // Create Employment 
                                tracingService.Trace("Create Employment");
                                employmentId = EmploymentHelper.CreateEmployment(service, employerRef,
                                    new EntityReference(PluginHelper.BpaMemberplan, memberPlanDetailId),
                                    submissionDate, labourRole,
                                    new OptionSetValue((int)EmployerWorkStatus.Employed), agreementRef, planRef);
                            }
                        }
                        else
                        {
                            // Create Member Plan Detail
                            tracingService.Trace("Create Member Plan Detail");
                            memberPlanDetailId = ContactHelper.CreateMemberPlan(service, tracingService, firstname,
                                lastname, firstname + ' ' + lastname,
                                new EntityReference(PluginHelper.Contact, existsMember.Id), planRef,
                                new OptionSetValue((int)MemberPlanBpaMemberplantype.Member), defaultEligibilityRef,
                                socialinsuranceno, HomeLocal, defaultCurrency);

                            // Create Employment Record
                            tracingService.Trace("Create Employment Record");
                            employmentId = EmploymentHelper.CreateEmployment(service, employerRef,
                                new EntityReference(PluginHelper.BpaMemberplan, memberPlanDetailId),
                                submissionDate, labourRole,
                                new OptionSetValue((int)EmployerWorkStatus.Employed), agreementRef, planRef);
                        }

                        // Set All Values 
                        tracingService.Trace("Set All Values");
                        //entity["bpa_memberfirstname"] = firstname;
                        //entity["bpa_memberlastname"] = lastname;
                        entity["bpa_labourroleid"] = labourRole;
                        entity["bpa_employmentid"] = new EntityReference(PluginHelper.BpaEmployment, employmentId);
                        entity["bpa_memberplanid"] = new EntityReference(PluginHelper.BpaMemberplan, memberPlanDetailId);
                        entity["bpa_membersin"] = socialinsuranceno;
                    }
                    else
                    {
                        throw new InvalidPluginExecutionException(OperationStatus.Failed, $@"Entered SIN does not exists in the system. Please create a contact and try again.");
                    }

                    #endregion
                }

                //sEt the Name
                if (string.IsNullOrEmpty(memberPlanIdName))
                {
                    EntityReference memberPlan = entity.Contains("bpa_memberplanid") ? entity.GetAttributeValue<EntityReference>("bpa_memberplanid") : null;
                    if (memberPlan != null)
                    {
                        memberPlanIdName = !string.IsNullOrEmpty(memberPlan.Name) ? memberPlan.Name : string.Empty;

                        if (string.IsNullOrEmpty(memberPlanIdName))
                        {
                            Entity memberPlanInfo = ContactHelper.FetchMemberPlanInformation(service, tracingService, memberPlan.Id);
                            if (memberPlanInfo != null && memberPlanInfo.Contains("bpa_name"))
                                memberPlanIdName = memberPlanInfo.GetAttributeValue<string>("bpa_name");
                        }
                    }
                }
                if (!string.IsNullOrEmpty(memberPlanIdName))
                    entity["bpa_name"] = memberPlanIdName;
            } //end of else
        } // end of ExecuteOnCreatePre

        string ValidateDuplicateDetail(IOrganizationService service, ITracingService tracingService, Entity entity, out EntityReference employmentRecord, out EntityReference jobClassification, out EntityReference memberPlan1)
        {
            bool isValidate = false;
            employmentRecord = null;
            jobClassification = null; memberPlan1 = null;
            EntityReference selectedEmploymentRef = entity.Contains("bpa_employmentid")
                ? entity.GetAttributeValue<EntityReference>("bpa_employmentid")
                : null;
            EntityReference selectedSubmissionRef = entity.Contains("bpa_submissionid")
                ? entity.GetAttributeValue<EntityReference>("bpa_submissionid")
                : null;

            //int submissionmembertype = entity.Contains("bpa_submissionmembertype") ? entity.GetAttributeValue<OptionSetValue>("bpa_submissionmembertype").Value : 0;
            string socialinsuranceno = entity.GetAttributeValue<string>("bpa_membersin");

            if (!string.IsNullOrEmpty(socialinsuranceno)) //submissionmembertype == (int)BpaSubmissionDetailBpaMemberType.NewMember && 
            {
                Entity existsMember = null;// MemberHelper.FetchMemberBySin(service, socialinsuranceno);
                tracingService.Trace($@"POINT 2 : Check Hash SIN already in the database? - {socialinsuranceno} - SIN");
                EntityCollection existContacts = MemberHelper.FetchContactByHashSIN(service, tracingService, socialinsuranceno);

                // CHECK BY HASH SIN 
                if (existContacts != null && existContacts.Entities.Count > 1)
                {
                    throw new InvalidPluginExecutionException(OperationStatus.Failed, "SIN you entered is associated more than one hash SIN. Please adjust record prior to submission.");
                }
                else if (existContacts != null && existContacts.Entities.Count == 1)
                {
                    existsMember = existContacts.Entities[0];
                    socialinsuranceno = existsMember.Contains("bpa_socialinsurancenumber") ? existsMember.GetAttributeValue<string>("bpa_socialinsurancenumber") : socialinsuranceno;
                }
                else
                {
                    //CHECK BY SIN 
                    existContacts = MemberHelper.FetchContactBySIN(service, tracingService, socialinsuranceno);
                    if (existContacts != null && existContacts.Entities.Count > 1)
                    {
                        throw new InvalidPluginExecutionException(OperationStatus.Failed, "SIN you entered is associated more than one SIN. Please adjust record prior to submission.");
                    }
                    else if (existContacts != null && existContacts.Entities.Count == 1)
                    {
                        existsMember = existContacts.Entities[0];
                        socialinsuranceno = existsMember.Contains("bpa_socialinsurancenumber") ? existsMember.GetAttributeValue<string>("bpa_socialinsurancenumber") : socialinsuranceno;
                    }
                }

                // FOUND MEMBER 
                if (existsMember != null)
                {
                    EntityReference submissionRef = entity.GetAttributeValue<EntityReference>("bpa_submissionid");

                    // Fetch Submission Detail
                    tracingService.Trace("Fetch Submission Detail");
                    Entity submission = SubmissionHelper.FetchSubmissionDetail(service, tracingService, submissionRef.Id);
                    EntityReference planRef = null;
                    if (submission != null && submission.Contains("bpa_plan"))
                        planRef = submission.GetAttributeValue<EntityReference>("bpa_plan");
                    if (planRef == null)
                        throw new InvalidPluginExecutionException(OperationStatus.Canceled, @"Plan is undefined.");

                    EntityReference account = submission.Contains("bpa_accountagreementid") ? submission.GetAttributeValue<EntityReference>("bpa_accountagreementid") : null;
                    Entity existsMemberPlan = ContactHelper.FetchMemberPlan(service, tracingService, existsMember.Id, planRef.Id);

                    if (existsMemberPlan != null)
                    {
                        memberPlan1 = new EntityReference(PluginHelper.BpaMemberplan, existsMemberPlan.Id); // Assign out parameter

                        Entity existsEmployment = EmploymentHelper.IsEmploymentExists(service, existsMemberPlan.Id, account.Id);
                        if (existsEmployment != null)
                        {
                            selectedEmploymentRef = new EntityReference(PluginHelper.BpaEmployment, existsEmployment.Id);
                            selectedEmploymentRef.Name = existsEmployment.GetAttributeValue<string>("bpa_name");

                            jobClassification = existsEmployment.Contains("bpa_labourrole") ? existsEmployment.GetAttributeValue<EntityReference>("bpa_labourrole") : null;

                            employmentRecord = selectedEmploymentRef;// Assign out parameter
                        }
                    }
                }
            }

            if (selectedEmploymentRef != null && selectedSubmissionRef != null)
                isValidate = SubmissionDetailHelper.IsSubmissionDetailExists(service, tracingService, selectedSubmissionRef.Id, selectedEmploymentRef.Id);

            string exitsMemberName = string.Empty;
            if (isValidate)
                exitsMemberName = $@" ({selectedEmploymentRef.Name}) ";
            return exitsMemberName;
        }

        //void SetEntityValues(IOrganizationService service, ITracingService tracingService, Entity entity, int submissionmembertype)
        //{
        //    EntityReference selectedEmploymentRef = entity.Contains("bpa_employmentid")
        //        ? entity.GetAttributeValue<EntityReference>("bpa_employmentid")
        //        : null;
        //    if (selectedEmploymentRef == null) return;
        //    EntityReference selectedLabourRoleRef = entity.Contains("bpa_labourroleid")
        //        ? entity.GetAttributeValue<EntityReference>("bpa_labourroleid")
        //        : null;

        //    // Fetch Employment record
        //    tracingService.Trace("Fetch Employment record");
        //    EntityReference fetchLabourRole = null;
        //    EntityReference fetchMemberPlan = null;
        //    Entity fetchEntity = EmploymentHelper.FetchDetail(service, tracingService, selectedEmploymentRef.Id);
        //    if (fetchEntity != null)
        //    {
        //        // Check Selected Labour Role and Employment job classification are same or not?
        //        tracingService.Trace("Check Selected Job Classification and Employment Job Classification are same or not?");
        //        fetchLabourRole = fetchEntity.Contains("bpa_labourrole")
        //            ? fetchEntity.GetAttributeValue<EntityReference>("bpa_labourrole")
        //            : null;
        //        fetchMemberPlan = fetchEntity.Contains("bpa_memberplanid")
        //            ? fetchEntity.GetAttributeValue<EntityReference>("bpa_memberplanid")
        //            : null;
        //    }

        //    // Labour Role cannot be blank
        //    tracingService.Trace("Job Classification cannot be blank");
        //    if (selectedLabourRoleRef == null && fetchLabourRole == null)
        //        throw new InvalidPluginExecutionException(OperationStatus.Canceled, "Job Classification cannot be blank.");

        //    // Set Value for Submission Detail
        //    tracingService.Trace("Set Value for Submission Detail");
        //    if (selectedLabourRoleRef == null)
        //        entity["bpa_labourroleid"] = fetchLabourRole;
        //    else if (selectedLabourRoleRef.Id != fetchLabourRole.Id)
        //        entity["bpa_labourroleid"] = selectedLabourRoleRef;

        //    entity["bpa_memberplanid"] = fetchMemberPlan;
        //    entity["bpa_name"] = fetchMemberPlan.Name;

        //    if ((submissionmembertype != (int)BpaSubmissionDetailBpaMemberType.MemberNotEmployed) &&
        //        (submissionmembertype != (int)BpaSubmissionDetailBpaMemberType.MemberEmployed)) return;

        //    // Update Employement Record
        //    tracingService.Trace("Update Employement Record");
        //    Entity update = new Entity(PluginHelper.BpaEmployment)
        //    { Id = selectedEmploymentRef.Id };
        //    if (submissionmembertype == (int)BpaSubmissionDetailBpaMemberType.MemberNotEmployed)
        //        update["bpa_workstatus"] = new OptionSetValue((int)EmployerWorkStatus.Employed);
        //    if (selectedLabourRoleRef != null && fetchLabourRole != null &&
        //        (selectedLabourRoleRef.Id != fetchLabourRole.Id))
        //        update["bpa_labourrole"] = selectedLabourRoleRef;
        //    service.Update(update);
        //}
    }
}