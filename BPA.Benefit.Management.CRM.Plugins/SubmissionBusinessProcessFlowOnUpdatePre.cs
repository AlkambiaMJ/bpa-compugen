﻿#region

using System;
using System.Diagnostics;
using BPA.Benefit.Management.CRM.Plugins.Helper;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;

#endregion


namespace BPA.Benefit.Management.CRM.Plugins
{
    public class SubmissionBusinessProcessFlowOnUpdatePre : Plugin
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="SubmissionOnUpdatePre" /> class.
        /// </summary>
        /// 
        public SubmissionBusinessProcessFlowOnUpdatePre()
            : base(typeof(SubmissionBusinessProcessFlowOnUpdatePre))
        {
            RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(20, "Update",
                PluginHelper.BpaSubmssionBusinessProcessFlow, ExecuteSubmissionBPFOnUpdatePre));

            // Note : you can register for more events here if this plugin is not specific to an individual entity and message combination.
            // You may also need to update your RegisterFile.crmregister plug-in registration file to reflect any change.
        }

        /// <summary>
        ///     Executes the plug-in.
        /// </summary>
        /// <param name="localContext">
        ///     The <see cref="Plugin.LocalPluginContext" /> which contains the
        ///     <see cref="IPluginExecutionContext" />,
        ///     <see cref="IOrganizationService" />
        ///     and <see cref="ITracingService" />
        /// </param>
        /// <remarks>
        ///     For improved performance, Microsoft Dynamics CRM caches plug-in instances.
        ///     The plug-in's Execute method should be written to be stateless as the constructor
        ///     is not called for every invocation of the plug-in. Also, multiple system threads
        ///     could execute the plug-in at the same time. All per invocation state information
        ///     is stored in the context. This means that you should not use global variables in plug-ins.
        /// </remarks>
        /// 

        protected void ExecuteSubmissionBPFOnUpdatePre(LocalPluginContext localContext)
        {
            // If local context not passed in, raise error
            if (localContext == null)
                throw new ArgumentNullException($"Local context not passed in.");

            // Create the context variables
            IPluginExecutionContext context = localContext.PluginExecutionContext;
            ITracingService tracingService = localContext.TracingService;
            IOrganizationService service = localContext.OrganizationService;

            // If Target is not passed in, raise error
            tracingService.Trace("If Target is not passed in, raise error");
            if (!context.InputParameters.Contains(PluginHelper.Target) ||
                !(context.InputParameters[PluginHelper.Target] is Entity))
                throw new ArgumentNullException($"Target is not of type Entity.");

            // Create the target entity & preImage
            tracingService.Trace("Create the target entity & preImage");
            Entity entity = (Entity)context.InputParameters[PluginHelper.Target];
            Entity preImageEntity = context.PreEntityImages != null &&
                                    context.PreEntityImages.Contains(PluginHelper.PreImageAlias)
                ? context.PreEntityImages[PluginHelper.PreImageAlias]
                : null;
            if (preImageEntity == null)
                throw new ArgumentNullException($"preImage entity is not defined.");


            if (entity.Contains("activestageid"))
            {
                tracingService.Trace("Inside entity.Contains('activestageid')");

                Guid Current_StageId = entity.GetAttributeValue<EntityReference>("activestageid").Id;
                Guid Pre_StageId = preImageEntity.GetAttributeValue<EntityReference>("activestageid").Id;

                EntityReference submission = null;
                //Get Submission Information
                if (entity.Contains("bpf_bpa_submissionid"))
                    submission = entity.GetAttributeValue<EntityReference>("bpf_bpa_submissionid");
                else if (preImageEntity.Contains("bpf_bpa_submissionid"))
                    submission = preImageEntity.GetAttributeValue<EntityReference>("bpf_bpa_submissionid");

                if (submission == null)
                    return;

                Entity Submission = service.Retrieve(PluginHelper.BpaSubmission, submission.Id, new ColumnSet(new string[] { "bpa_accountagreementid", "createdon", "bpa_submissiontype", "bpa_depositid", "statecode" }));

                //Validate Submission is not Inactive
                OptionSetValue statecode = Submission.GetAttributeValue<OptionSetValue>("statecode");
                if(statecode.Value == 1) // Inactive
                    throw new InvalidPluginExecutionException(OperationStatus.Failed, $@"Current submission is inactive you are not able to modified anything.");


                /*EntityReference Employer = Submission.GetAttributeValue<EntityReference>("bpa_accountagreementid");
                DateTime CreatedOn = Submission.GetAttributeValue<DateTime>("createdon");*/
                OptionSetValue submissiontype = Submission.GetAttributeValue<OptionSetValue>("bpa_submissiontype");
                
                


                if ((submissiontype.Value == (int)SubmissionSubmissionTypeNew.EmployerAdjustment) 
                    || (submissiontype.Value == (int)SubmissionSubmissionTypeNew.EmployerRegular)
                    || (submissiontype.Value == (int)SubmissionSubmissionTypeNew.EmployerMoneyOnly) 
                    || (submissiontype.Value == (int)SubmissionSubmissionTypeNew.EmployerNoActivity)
                    || (submissiontype.Value == (int)SubmissionSubmissionTypeNew.Member)
                    || (submissiontype.Value == (int)SubmissionSubmissionTypeNew.MemberSelfPayRefund))
                {
                    tracingService.Trace($"Inside only employer submission");
                    if (ValidateSubmission(service, tracingService, entity, preImageEntity, Submission))
                        throw new InvalidPluginExecutionException(
                            "You are attempting to change the status of a submission in which one of the two conditions exist.\n\t1. There are prior submissions in Pending or Verification Status. \n\t2. There are future submissions in 'Deposit Pending' or 'Completed' status.");
                }
            }

        }

        bool ValidateSubmission(IOrganizationService service, ITracingService tracingService, Entity entity, Entity preImage, Entity submission)
        {
            bool IsValid = false;

            Guid Current_StageId = entity.GetAttributeValue<EntityReference>("activestageid").Id;
            Guid Pre_StageId = preImage.GetAttributeValue<EntityReference>("activestageid").Id;


            EntityReference Employer = submission.GetAttributeValue<EntityReference>("bpa_accountagreementid");
            DateTime CreatedOn = submission.GetAttributeValue<DateTime>("createdon");

            if(Current_StageId == Pre_StageId) return false;
            if((Current_StageId == PluginHelper.SubmissionStageIdPending) && (Pre_StageId != PluginHelper.SubmissionStageIdVerify))
            {
                tracingService.Trace("Validating Stage Submission Completed");
                throw new InvalidPluginExecutionException(OperationStatus.Failed, $@"You can not proceed to 'Pending' status prior to going to 'Verification'.");
            }


            //JK Added Validation
            if ((Pre_StageId == PluginHelper.SubmissionStageIdComplete) && ((Current_StageId == PluginHelper.SubmissionStageIdPending) || 
                                                                            (Current_StageId == PluginHelper.SubmissionStageIdVerify) ||
                                                                            (Current_StageId == PluginHelper.SubmissionStageIdDepositPending)))
            {
                tracingService.Trace("Validating Stage Submission Completed");
                throw new InvalidPluginExecutionException(OperationStatus.Failed, $@"You are not allowed to go previous stage 'Pending', 'Verify' or 'Deposit Pending' as submission completed.");
            }

            if ((Pre_StageId == PluginHelper.SubmissionStageIdDepositPending) && (Current_StageId == PluginHelper.SubmissionStageIdVerify))
            {
                tracingService.Trace("Validating Stage IsSubmissionExistsForward");

                //Check Forward - Is there any submission exists where Submission Status IN (verification, Deposit Pending, Completed)
                if (SubmissionHelper.IsSubmissionExistsForward(service, tracingService, Employer.Id, CreatedOn.ToLocalTime()))
                    IsValid = true;

            }
            
            if ((Pre_StageId == PluginHelper.SubmissionStageIdPending) && (Current_StageId == PluginHelper.SubmissionStageIdVerify))
            {
                tracingService.Trace("Validating Stage IsSubmissionExistsBackward");

                //Check BackWard - Is there any submission exists where Submission Status IN (Pending, Verification)
                if (SubmissionHelper.IsSubmissionExistsBackward(service, tracingService, Employer.Id, CreatedOn.ToLocalTime()))
                    IsValid = true;
            }

            if ((Pre_StageId == PluginHelper.SubmissionStageIdDepositPending) && (Current_StageId == PluginHelper.SubmissionStageIdComplete))
            {
                tracingService.Trace("Validating Stage Deposit to Complteed");
                EntityReference depositId = submission.GetAttributeValue<EntityReference>("bpa_depositid");

                //Check Deposit closed or not?
                if (depositId == null)
                {
                    throw new InvalidPluginExecutionException(OperationStatus.Failed, $@"Deposit ID cannot be null/blank. Please contact administractor.");
                }
                else
                {
                    Entity deposit = service.Retrieve(PluginHelper.BpaDeposit, depositId.Id, new ColumnSet(true));
                    if (deposit != null)
                    {
                        int depositStatus = deposit.Contains("bpa_depositstatus") ? deposit.GetAttributeValue<OptionSetValue>("bpa_depositstatus").Value : 0;

                        if (depositStatus != (int)DepositDepositStatus.Deposited)
                            throw new InvalidPluginExecutionException(OperationStatus.Failed, $@"The Submission can not be completed until the associated Deposit is closed.");
                    }
                }

            }

            return IsValid;

        }
        
    }
}
