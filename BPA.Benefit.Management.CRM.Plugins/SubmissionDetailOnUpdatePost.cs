﻿#region

using System;
using BPA.Benefit.Management.CRM.Plugins.Helper;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using System.Collections.Generic;
using System.Linq;

#endregion

namespace BPA.Benefit.Management.CRM.Plugins
{
    /// <summary>
    ///     EligibilityCategoryDetailOnCreateUpdatePre PlugIn.
    ///     This Plugin is used for validation of Eligibility Category is Reinstatement and Dollar/Hours value must be Greter or Equal of Deduction rate
    /// </summary>
    public class SubmissionDetailOnUpdatePost : Plugin
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="SubmissionDetailOnCreateUpdatePost" /> class.
        /// </summary>
        public SubmissionDetailOnUpdatePost() : base(typeof(SubmissionDetailOnUpdatePost))
        {
            //RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(40, "Create", PluginHelper.BpaSubmissiondetail, ExecuteOnCreateUpdatePost));
            RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(40, "Update", PluginHelper.BpaSubmissiondetail, ExecuteOnUpdatePost));
            
            // Note : you can register for more events here if this plugin is not specific to an individual entity and message combination.
            // You may also need to update your RegisterFile.crmregister plug-in registration file to reflect any change.
        }

        /// <summary>
        ///     Executes the plug-in.
        /// </summary>
        /// <param name="localContext">
        ///     The <see cref="Plugin.LocalPluginContext" /> which contains the
        ///     <see cref="IPluginExecutionContext" />,
        ///     <see cref="IOrganizationService" />
        ///     and <see cref="ITracingService" />
        /// </param>
        /// <remarks>
        ///     For improved performance, Microsoft Dynamics CRM caches plug-in instances.
        ///     The plug-in's Execute method should be written to be stateless as the constructor
        ///     is not called for every invocation of the plug-in. Also, multiple system threads
        ///     could execute the plug-in at the same time. All per invocation state information
        ///     is stored in the context. This means that you should not use global variables in plug-ins.
        /// </remarks>

        protected void ExecuteOnUpdatePost(LocalPluginContext localContext)
        {
            // If local context not passed in, raise error
            if (localContext == null)
                throw new ArgumentNullException($"Local context not passed in.");

            // Create the context variables
            IPluginExecutionContext context = localContext.PluginExecutionContext;
            IOrganizationService service1 = localContext.OrganizationService;
            ITracingService tracingService = localContext.TracingService;
            IServiceProvider serviceProvider = localContext.ServiceProvider;

            string crmAdminUserId = PluginConfiguration[PluginHelper.ConfigCrmAdmin];
            IOrganizationServiceFactory serviceFactory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
            IOrganizationService service = serviceFactory.CreateOrganizationService(new Guid(crmAdminUserId));
            tracingService.Trace("Inside entity.Contains('activestageid')");

            // If Target is not passed in, raise error
            tracingService.Trace("If Target is not passed in, raise error");
            if (!context.InputParameters.Contains(PluginHelper.Target) ||
                !(context.InputParameters[PluginHelper.Target] is Entity))
                throw new ArgumentNullException($"Target is not of type Entity.");

            // Create the target entity & preImage
            tracingService.Trace("Create the target entity & preImage");
            Entity entity = (Entity)context.InputParameters[PluginHelper.Target];
            Entity preImage = context.PreEntityImages != null &&
                                    context.PreEntityImages.Contains(PluginHelper.PreImageAlias)
                ? context.PreEntityImages[PluginHelper.PreImageAlias] : null;
            bool IsUpdateMessage = false;
            if (context.MessageName.ToLower() == PluginHelper.Update.ToLower())
                IsUpdateMessage = true;
            if (context.MessageName.ToLower() == PluginHelper.Update.ToLower() && preImage == null)
                throw new ArgumentNullException($"preImage entity is not defined.");

            #region ------ Fetch all values from Submission Detail --------------------
            decimal icihours = 0, nonicihours = 0, totalhours = 0, grosswages = 0, dollars = 0;

            decimal pensionCredited = 0, pensionMemberRequired = 0, pensionMemberVoluntary = 0;

            EntityReference Submission = null;
            EntityReference Employer = null;
            EntityReference Agreement = null;
            OptionSetValue SubmissionType = new OptionSetValue(0);
            OptionSetValue FlatRateRule = null;

            EntityReference LabourRole = null;
            EntityReference Employment = null;
            EntityReference Contact = null;


            if(IsUpdateMessage)
            {
                if (preImage.Contains("bpa_submissionid"))
                    Submission = preImage.GetAttributeValue<EntityReference>("bpa_submissionid");
                if (preImage.Contains("bpa_accountagreementid"))
                    Employer = preImage.GetAttributeValue<EntityReference>("bpa_accountagreementid");
                if (preImage.Contains("bpa_agreementid"))
                    Agreement = preImage.GetAttributeValue<EntityReference>("bpa_agreementid");
                if (preImage.Contains("bpa_submissiontype"))
                    SubmissionType = preImage.GetAttributeValue<OptionSetValue>("bpa_submissiontype");
                if (preImage.Contains("bpa_employmentid"))
                    Employment = preImage.GetAttributeValue<EntityReference>("bpa_employmentid");
                if (preImage.Contains("bpa_memberplanid"))
                    Contact = preImage.GetAttributeValue<EntityReference>("bpa_memberplanid");
                if (preImage.Contains("bpa_labourroleid"))
                    LabourRole = preImage.GetAttributeValue<EntityReference>("bpa_labourroleid");
                if (preImage.Contains("bpa_icihours"))
                    icihours = preImage.GetAttributeValue<decimal>("bpa_icihours");
                if (preImage.Contains("bpa_nonicihours"))
                    nonicihours = preImage.GetAttributeValue<decimal>("bpa_nonicihours");
                if (preImage.Contains("bpa_totalhours"))
                    totalhours = preImage.GetAttributeValue<decimal>("bpa_totalhours");

                if (preImage.Contains("bpa_dollars") && preImage.GetAttributeValue<Money>("bpa_dollars") != null)
                    dollars = preImage.GetAttributeValue<Money>("bpa_dollars").Value;
                if (preImage.Contains("bpa_grosswages") && preImage.GetAttributeValue<Money>("bpa_grosswages") != null)
                    grosswages = preImage.GetAttributeValue<Money>("bpa_grosswages").Value;
                 if (preImage.Contains("bpa_flatraterule"))
                    FlatRateRule = preImage.GetAttributeValue<OptionSetValue>("bpa_flatraterule");
                if (preImage.Contains("bpa_pensioncredited"))
                    pensionCredited = preImage.GetAttributeValue<decimal>("bpa_pensioncredited");
                if (preImage.Contains("bpa_pensionmemberrequired") && preImage.GetAttributeValue<Money>("bpa_pensionmemberrequired") != null)
                    pensionMemberRequired = preImage.GetAttributeValue<Money>("bpa_pensionmemberrequired").Value;
                if (preImage.Contains("bpa_pensionmembervoluntary") && preImage.GetAttributeValue<Money>("bpa_pensionmembervoluntary") != null)
                    pensionMemberVoluntary = preImage.GetAttributeValue<Money>("bpa_pensionmembervoluntary").Value;
            }
            else
            {
                if (entity.Contains("bpa_submissionid"))
                    Submission = entity.GetAttributeValue<EntityReference>("bpa_submissionid");
                if (entity.Contains("bpa_accountagreementid"))
                    Employer = entity.GetAttributeValue<EntityReference>("bpa_accountagreementid");
                if (entity.Contains("bpa_agreementid"))
                    Agreement = entity.GetAttributeValue<EntityReference>("bpa_agreementid");
                if (entity.Contains("bpa_submissiontype"))
                    SubmissionType = entity.GetAttributeValue<OptionSetValue>("bpa_submissiontype");
                if (entity.Contains("bpa_employmentid"))
                    Employment = entity.GetAttributeValue<EntityReference>("bpa_employmentid");
                if (entity.Contains("bpa_memberplanid"))
                    Contact = entity.GetAttributeValue<EntityReference>("bpa_memberplanid");
                if (entity.Contains("bpa_labourroleid"))
                    LabourRole = entity.GetAttributeValue<EntityReference>("bpa_labourroleid");
                if (entity.Contains("bpa_icihours"))
                    icihours = entity.GetAttributeValue<decimal>("bpa_icihours");
                if (entity.Contains("bpa_nonicihours"))
                    nonicihours = entity.GetAttributeValue<decimal>("bpa_nonicihours");
                if (entity.Contains("bpa_totalhours"))
                    totalhours = entity.GetAttributeValue<decimal>("bpa_totalhours");

                if (entity.Contains("bpa_grosswages") && entity.GetAttributeValue<Money>("bpa_grosswages") != null)
                    grosswages = entity.GetAttributeValue<Money>("bpa_grosswages").Value;
                if (entity.Contains("bpa_dollars") && entity.GetAttributeValue<Money>("bpa_dollars") != null)
                    dollars = entity.GetAttributeValue<Money>("bpa_dollars").Value;
                if (entity.Contains("bpa_flatraterule"))
                    FlatRateRule = entity.GetAttributeValue<OptionSetValue>("bpa_flatraterule");

                if (entity.Contains("bpa_pensioncredited"))
                    pensionCredited = entity.GetAttributeValue<decimal>("bpa_pensioncredited");
                if (entity.Contains("bpa_pensionmemberrequired") && entity.GetAttributeValue<Money>("bpa_pensionmemberrequired") != null)
                    pensionMemberRequired = entity.GetAttributeValue<Money>("bpa_pensionmemberrequired").Value;
                if (entity.Contains("bpa_pensionmembervoluntary") && entity.GetAttributeValue<Money>("bpa_pensionmembervoluntary") != null)
                    pensionMemberVoluntary = entity.GetAttributeValue<Money>("bpa_pensionmembervoluntary").Value;

            }
            
            int SubSector_CalculationType = 0;
            SubSector_CalculationType = AgreementHelper.FetchAgreementCalculationType(service, tracingService, Agreement.Id);
            tracingService.Trace("Fetch SubSector_CalculationType - " + SubSector_CalculationType);

            if (FlatRateRule == null)
                FlatRateRule = new OptionSetValue((int)BpaSubmissionDetailBpaFlatRateRule.Charge);
            
            EntityReference owner = null;
            if (IsUpdateMessage && preImage.Contains("ownerid"))
                owner = preImage.GetAttributeValue<EntityReference>("ownerid");

            #endregion

            #region ------------- GET ALL VALUES FROM SUBMISSION AND PLAN(SECTOR) ----------------

            // Get Default Funds from TRUST
            List<Entity> defaultFundList = new List<Entity>();

            // Find and Get Default Welfare Fund
            EntityReference defaultFundRef = null;

            Entity submissionInfo = SubmissionHelper.FetchSubmissionDetail(service, tracingService, Submission.Id);
            EntityReference trustRef = submissionInfo.Contains("bpa_trustid") ? submissionInfo.GetAttributeValue<EntityReference>("bpa_trustid") : null;

            DateTime WorkMonth = DateTime.MinValue;
            if (submissionInfo.Contains("bpa_submissiondate"))
                WorkMonth = submissionInfo.GetAttributeValue<DateTime>("bpa_submissiondate");

            WorkMonth = new DateTime(WorkMonth.Year, WorkMonth.Month, WorkMonth.Day);

            EntityReference plan = null;
            if (submissionInfo.Contains("bpa_plan"))
                plan = submissionInfo.GetAttributeValue<EntityReference>("bpa_plan");
            
            bool IsLateORMultipleSubmission = false;
            DateTime _currentWorkMonth = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1).AddMonths(-1);
            int currentmonthsubmission = SubmissionHelper.GetSubmissionCountForMonth(service, tracingService, Employer.Id, WorkMonth);

            if ((_currentWorkMonth > WorkMonth) || (currentmonthsubmission > 1))
            {
                IsLateORMultipleSubmission = true;
            }
            //EntityReference planRef = SubmissionEntity.Contains("bpa_plan") ? SubmissionEntity.GetAttributeValue<EntityReference>("bpa_plan") : null;
            bool isOnlyPensionPlan = false;
            bool isPensionPlan = false;
            bool isBenefitPlan = false;
            Entity planInfo = PlanHelper.FetchPlanInformation(service, tracingService, plan.Id);
            if (planInfo != null)
            {
                isPensionPlan = planInfo.GetAttributeValue<bool>("bpa_pensionplan");
                isBenefitPlan = planInfo.GetAttributeValue<bool>("bpa_benefitplan");

                // Both balues is No then return (Do not do any thing)
                if (!isBenefitPlan && !isPensionPlan)
                    return;

                if (!isBenefitPlan && isPensionPlan)
                    isOnlyPensionPlan = true;


                // Fetch all default funds
                if (trustRef != null)
                    defaultFundList = TrustHelper.FetchAllDefaultFunds(service, tracingService, trustRef.Id);
                Entity defaultWelFareFund = null;
                if (defaultFundList != null)
                {
                    if (isBenefitPlan)
                        defaultWelFareFund = defaultFundList.Where(x => x.GetAttributeValue<OptionSetValue>("bpa_fundtype").Value == (int)BpaFundbpaFundType.Welfare).FirstOrDefault();
                    else if (isPensionPlan)
                        defaultWelFareFund = defaultFundList.Where(x => x.GetAttributeValue<OptionSetValue>("bpa_fundtype").Value == (int)BpaFundbpaFundType.Pension).FirstOrDefault();

                    if (defaultWelFareFund != null)
                        defaultFundRef = new EntityReference(PluginHelper.BpaFund, defaultWelFareFund.Id);
                }
            }
            
            OptionSetValue pensionPlanType = new OptionSetValue(0);
            OptionSetValue pensionBenefitCalculationType = new OptionSetValue(0);
            decimal benefitRate = 1;

            if (isPensionPlan)
            {
                // Fetch Pension Benefit Rate and Plan
                Entity planInfoWithBenefitRate = PlanHelper.FetchPlanInfoWithPensionBenfitRate(service, tracingService, plan.Id, WorkMonth);
                if (planInfoWithBenefitRate != null)
                {
                    pensionPlanType = planInfoWithBenefitRate.Contains("bpa_pensionplantype") ? planInfoWithBenefitRate.GetAttributeValue<OptionSetValue>("bpa_pensionplantype") : new OptionSetValue(0);

                    benefitRate = planInfoWithBenefitRate.Contains("bpa_planpensionbenefitrate1.bpa_dollaramountrate") ?
                        (decimal)planInfoWithBenefitRate.GetAttributeValue<AliasedValue>("bpa_planpensionbenefitrate1.bpa_dollaramountrate").Value : 0;

                    pensionBenefitCalculationType = planInfoWithBenefitRate.Contains("bpa_planpensionbenefitrate1.bpa_calculationtype") ?
                        (OptionSetValue)planInfoWithBenefitRate.GetAttributeValue<AliasedValue>("bpa_planpensionbenefitrate1.bpa_calculationtype").Value : new OptionSetValue(0);
                    if (pensionPlanType.Value == (int)PensionPlanType.DefinedContribution)
                        benefitRate = 0; // Task# 1292 - For DC plans the Accrued Benefit should be populated with $0
                }
            }
            #endregion

            #region ---------------- GET ALL VALUES FROM MEMBER PLAN -------------

            Entity memberPlanInfo = ContactHelper.FetchMemberPlanInformation(service, tracingService, Contact.Id);
            // Get member Eligibility Category
            EntityReference mpEligibilityCategory = memberPlanInfo.Contains("bpa_eligibilitycategoryid") ?
                memberPlanInfo.GetAttributeValue<EntityReference>("bpa_eligibilitycategoryid") : null;


            #endregion
            
            #region ------------ GET ALL ELIGIBILITY CATEGORY AND ELIGIBILITY CATEGORY DETAILS ----------------
            
            // Get all Eligibility Categories for this Member's Plan
            EntityCollection eligibilityCategories = MemberPlanRetroprocessingHelper_Optimized.FetchAllEligibilityCategoryByPlan(service,
                tracingService, plan.Id);
            if (eligibilityCategories.Entities.Count == 0)
            {
                // Raise an error that Eligibility Categories record is null
                throw new InvalidPluginExecutionException(OperationStatus.Failed, @"Cannot retrieve Eligibility Category records.");
            }

            EntityCollection eligibilityCategoryDetails = null;
            if (!isOnlyPensionPlan)
            {
                // Get all Eligibility Category Details for this Member's Plan
                eligibilityCategoryDetails = MemberPlanRetroprocessingHelper_Optimized.FetchAllEligibilityDetails(service, tracingService, plan.Id);
                if (eligibilityCategoryDetails.Entities.Count == 0)
                {
                    // Raise an error that Eligibility Categories record is null
                    throw new InvalidPluginExecutionException(OperationStatus.Failed, @"Cannot retrieve Eligibility Category Detail records.");
                }
            }

            #endregion
            
            //FETCH All RATES
            List<Rate> rates = RateHelper.GetRatesByLabourRoleId(service, tracingService, Employer.Id, WorkMonth, Agreement.Id, LabourRole.Id);

            
            tracingService.Trace("Inside If - 2");

            #region ----------------- THIS IS THE LOGIC FOR TRANSACTIONS ----------------------------------

            tracingService.Trace("BEfore Fetch VacationFundId");
            Entity VacationFund = defaultFundList.Where(x =>
                        x.GetAttributeValue<OptionSetValue>("bpa_fundtype").Value == (int)BpaFundbpaFundType.VacationPay).FirstOrDefault();
            Guid VacationFundId = Guid.Empty;
            if (VacationFund != null)
                VacationFundId = VacationFund.Id;
            tracingService.Trace("FETCH VacationFundId - " + VacationFundId);
                
            tracingService.Trace("Inside rates Count - " + rates.Count);

            #region ---------- Rate loop and create Transaction  -----------
            // Loop all rate 
            foreach (Rate rateT in rates)
            {
                tracingService.Trace("Rate Fund Id - " + rateT.FundId);
                tracingService.Trace("Rate Value - " + rateT.RateValue);
                tracingService.Trace("Rate Fund Type - " + rateT.FundType);
                decimal dollarhours = 0;

                if (SubSector_CalculationType == (int)BpaSubSectorBpaCalculationType.DollarBased ||
                    SubSector_CalculationType == (int)BpaSubSectorBpaCalculationType.Piecework ||
                    SubSector_CalculationType == (int)BpaSubSectorBpaCalculationType.Corporate)
                {
                    dollarhours = Convert.ToDecimal(dollars);
                    tracingService.Trace("1. Dollar  - " + dollarhours);
                }
                else if (SubSector_CalculationType == (int)BpaSubSectorBpaCalculationType.Flat)
                {
                    if (rateT.RateValue != null)
                        dollarhours = Convert.ToDecimal(totalhours);
                    tracingService.Trace("2. Dollar  - " + dollarhours);
                }
                else
                {
                    if (rateT.RateValue != null)
                        dollarhours = Convert.ToDecimal(totalhours);
                    tracingService.Trace("3. Dollar  - " + dollarhours);
                }

                if ((rateT.FundType.Value == (int)BpaFundbpaFundType.Welfare) && rateT.IsDefaultFund)
                {
                    tracingService.Trace("Inside DefaultFund - " + rateT.FundId);
                    if (SubSector_CalculationType == (int)BpaSubSectorBpaCalculationType.Flat)
                    {
                        if (rateT.RateValue != null)
                            dollarhours = 1; // Convert.ToDecimal(rateT.RateValue.Value);
                    }

                    #region ------------------ Employer Contribution / Late Transfer In Contribution ------------------------ 

                    bool HasTransferIn = false;
                    DateTime TransferInWorkMonth = DateTime.MinValue;
                    EntityReference CurrentEligibilityCategory = null;

                    //Following If condition added because for the Late Submission we have to used that month Eligibility Category not Current Eligibility Category
                    if (IsLateORMultipleSubmission)
                    {
                        //Get the Same Eligibility Category if member already has employer contribution
                        if (currentmonthsubmission > 1)
                        {
                            Guid _transId = TransactionHelper.FetchEligibilityTransactionForWorkMonth(service, WorkMonth, Contact.Id);
                            if (_transId != Guid.Empty)
                                CurrentEligibilityCategory = TransactionHelper.FetchEligiblityCategorty(service, _transId);
                        }

                        //CHECKING IS MEMBER HAS TRANSFER IN STATUS
                        if (_currentWorkMonth > WorkMonth)
                        {
                            Entity Entity_TransferIn = TransactionHelper.IsMemberTranferIn(service, Employer.Id, WorkMonth, (int)BpaTransactionbpaTransactionType.TransferIn);

                            if ((Entity_TransferIn != null) && (Entity_TransferIn.Attributes.Contains("bpa_transactiondate")))
                            {
                                TransferInWorkMonth = Entity_TransferIn.GetAttributeValue<DateTime>("bpa_transactiondate");
                                if ((TransferInWorkMonth != DateTime.MinValue) && (new DateTime(TransferInWorkMonth.Year, TransferInWorkMonth.Month, 1) > WorkMonth))
                                    HasTransferIn = true;
                                else
                                    HasTransferIn = false;
                            }

                        }
                    }

                    //FETCH ELIGIBILITY CATEGORY - if nothing find
                    if (CurrentEligibilityCategory == null)
                        CurrentEligibilityCategory = TransactionHelper.FetchCurrentEligibilityCategory(service, tracingService, Contact.Id, WorkMonth); //  .GetCurrentEligibilityCategory(employee.Id);

                    // GET and SET Eligibility Category and Eligibility Category Detail 
                    Entity EC = eligibilityCategories.Entities.Where(x => x.Id == mpEligibilityCategory.Id).FirstOrDefault();

                    Entity ECD = null;
                    if (!isOnlyPensionPlan)
                    {
                        ECD = MemberPlanRetroprocessingHelper_Optimized.FetchEligibilityDetail(tracingService, eligibilityCategoryDetails, EC.Id, WorkMonth);

                        //Member Eligibility Category Null So need to skip
                        if (EC == null || ECD == null)
                            throw new InvalidPluginExecutionException(OperationStatus.Failed, $@"Member Eligibility Category or Eligibility Category Detail is null for '{Contact.Name}'");
                    }
                    int eligibilityOffset = EC.Contains("bpa_eligibilityoffsetmonths") ? EC.GetAttributeValue<int>("bpa_eligibilityoffsetmonths") : 0;
                    DateTime BenefitMonth = DateTime.MinValue;

                    if (WorkMonth != DateTime.MinValue)
                    {
                        BenefitMonth = WorkMonth.AddMonths(eligibilityOffset);
                        BenefitMonth = new DateTime(BenefitMonth.Year, BenefitMonth.Month, 1);
                    }

                    if ((totalhours != 0) || (grosswages != 0) || (dollars != 0)
                            || (SubSector_CalculationType == (int)BpaSubSectorBpaCalculationType.Flat)
                            || (SubSector_CalculationType == (int)BpaSubSectorBpaCalculationType.Corporate))
                    {
                        if ((SubmissionType.Value == (int)SubmissionSubmissionType.EmployerRegular) ||
                                    (SubmissionType.Value == (int)SubmissionSubmissionType.EmployerAdjustment))
                        {
                            #region ---------------- Submission: Employer ----------------
                            bool isIgnore = false;
                            if (rateT.CalculationType == (int)RateCalculationType.FlatRate)
                            {
                                if (FlatRateRule.Value == (int)BpaSubmissionDetailBpaFlatRateRule.Charge)
                                    dollarhours = dollarhours * 1;
                                else if (FlatRateRule.Value == (int)BpaSubmissionDetailBpaFlatRateRule.Ignore)
                                {
                                    dollarhours = 0;
                                    isIgnore = true;
                                }
                                if (FlatRateRule.Value == (int)BpaSubmissionDetailBpaFlatRateRule.Remove)
                                    dollarhours = dollarhours * -1;
                            }

                            tracingService.Trace("Inside SubmissionType.EmployerRegular");
                            int currentEligibility = ContactHelper.FetchCurrentEligibility(service, tracingService, Contact.Id);
                            Guid EmployerContributionId = Guid.Empty;

                            if (!isIgnore)
                            {
                                tracingService.Trace("BEFORE ELSE EmployerContributionId");

                                EmployerContributionId = TransactionHelper.CreateContributionTransaction(service, tracingService,
                                        Submission.Id, Contact.Id, WorkMonth, BenefitMonth, totalhours, new Money(grosswages), LabourRole.Id,
                                        new Money(dollarhours * rateT.RateValue.Value), defaultFundRef.Id, rateT.RateValue.Value,
                                        (int)BpaTransactionbpaTransactionCategory.Contributions,
                                        (int)BpaTransactionbpaTransactionType.EmployerContribution,
                                        CurrentEligibilityCategory, icihours, nonicihours, 0, null, null, true, Agreement,
                                        Guid.Empty, (int)TransactionStatus.Pending, owner, entity.Id);
                                tracingService.Trace("AFTER ELSE EmployerContributionId - " + EmployerContributionId);
                                
                            }
                            #endregion
                        } // end of else if ((SubmissionType.Value == (int)SubmissionSubmissionType.EmployerRegular) || (SubmissionType.Value == (int)SubmissionSubmissionType.EmployerAdjustment))
                    }

                    #endregion
                }
                else if ((rateT.FundType.Value == (int)BpaFundbpaFundType.VacationPay) && rateT.IsDefaultFund)
                {
                    #region ------------- Vacation Fund logic ----------------

                    tracingService.Trace("in vacation");
                    decimal? vacationRate = null;
                    vacationRate = rateT.RateValue.Value;

                    if (vacationRate != null && vacationRate != 0 && grosswages != 0)
                    {
                        decimal VacationAmount = Convert.ToDecimal(grosswages) * vacationRate.Value;
                        bool isIgnore = false;
                        if (rateT.CalculationType == (int)RateCalculationType.FlatRate)
                        {
                            if (FlatRateRule.Value == (int)BpaSubmissionDetailBpaFlatRateRule.Charge)
                                VacationAmount = VacationAmount * 1;
                            else if (FlatRateRule.Value == (int)BpaSubmissionDetailBpaFlatRateRule.Ignore)
                            {
                                VacationAmount = 0;
                                isIgnore = true;
                            }
                            if (FlatRateRule.Value == (int)BpaSubmissionDetailBpaFlatRateRule.Remove)
                                VacationAmount = VacationAmount * -1;
                        }
                        if (!isIgnore)
                        {
                            Guid VacationContributionId =
                                TransactionHelper.CreateContributionTransaction(service, tracingService,
                                    Submission.Id, Contact.Id, WorkMonth, DateTime.MinValue,
                                    totalhours, new Money(grosswages), LabourRole.Id,
                                    new Money(dollarhours * vacationRate.Value), VacationFundId,
                                    vacationRate.Value,
                                    (int)BpaTransactionbpaTransactionCategory.Contributions,
                                    (int)BpaTransactionbpaTransactionType.VacationContribution,
                                    null, 0, 0, VacationAmount, null, null, false, Agreement, Guid.Empty,
                                    (int)TransactionStatus.Pending, owner, entity.Id);
                        }
                    }

                    #endregion
                }
                else if ((rateT.FundType.Value == (int)BpaFundbpaFundType.Pension))
                {
                    #region ---------------- PENSION TRANSACTION -------------------
                    int transactionType = 0;
                    decimal actualHours = 0, actualDollar = 0, adjustedHours = 0, accruedBenefitAmount = 0;

                    // Set Value for just for Pension Purpose
                    actualHours = totalhours; actualDollar = dollars;


                    if ((rateT.RateValue != null) && (rateT.CalculationType.Value != (int)RateCalculationType.Tax))
                    {
                        tracingService.Trace("rateT.RateValue = " + rateT.RateValue.Value);
                        tracingService.Trace("rateT.CalculationType = " + rateT.CalculationType.Value);
                        bool isIgnore = false;
                        bool calculateAccuredBenefit = false;
                        if (rateT.CalculationType == (int)RateCalculationType.PensionMemberRequiredDollar)
                        {
                            transactionType = (int)PensionTransactionType.MemberRequired;
                            dollarhours = pensionMemberRequired * rateT.RateValue.Value;
                            adjustedHours = 0;
                            calculateAccuredBenefit = false;
                            grosswages = 0;
                        }
                        else if (rateT.CalculationType == (int)RateCalculationType.PensionMemberVoluntaryDollar)
                        {
                            transactionType = (int)PensionTransactionType.MemberVoluntary;
                            dollarhours = pensionMemberVoluntary * rateT.RateValue.Value;
                            adjustedHours = 0;
                            calculateAccuredBenefit = false;
                            grosswages = 0;
                        }
                        else if (rateT.CalculationType == (int)RateCalculationType.PercentageGrossWages)
                        {
                            transactionType = (int)PensionTransactionType.PensionContribution;
                            dollarhours = grosswages * rateT.RateValue.Value;
                            calculateAccuredBenefit = true;
                        }
                        else if ((rateT.CalculationType == (int)RateCalculationType.Hourly)) // && rateT.HourMultiplier.Value != 0)
                        {
                            transactionType = (int)PensionTransactionType.PensionContribution;
                            adjustedHours = actualHours * rateT.HourMultiplier.Value;
                            dollarhours = adjustedHours * rateT.RateValue.Value;
                            calculateAccuredBenefit = true;
                        }
                        else if (rateT.CalculationType == (int)RateCalculationType.FlatRate)
                        {
                            if ((SubmissionType.Value == (int)SubmissionSubmissionType.EmployerAdjustment) ||
                                (SubmissionType.Value == (int)SubmissionSubmissionType.EmployerRegular))
                            {
                                if (FlatRateRule.Value == (int)BpaSubmissionDetailBpaFlatRateRule.Charge)
                                    dollarhours = rateT.RateValue.Value * 1;
                                else if (FlatRateRule.Value == (int)BpaSubmissionDetailBpaFlatRateRule.Ignore)
                                    dollarhours = 0;
                                else if (FlatRateRule.Value == (int)BpaSubmissionDetailBpaFlatRateRule.Remove)
                                    dollarhours = rateT.RateValue.Value * -1;
                            }
                            else
                                dollarhours = rateT.RateValue.Value;

                            transactionType = (int)PensionTransactionType.PensionContribution;
                            adjustedHours = actualHours;
                            calculateAccuredBenefit = true;
                        }
                        else
                        {
                            // Dollar Base
                            transactionType = (int)PensionTransactionType.PensionContribution;
                            dollarhours = dollars * rateT.RateValue.Value;
                            calculateAccuredBenefit = true;
                            adjustedHours = 0;
                        }

                        // Create Transaction 
                        if (!isIgnore)
                        {
                            // Check fund type is Pesion ?
                            Guid periodOfServiceId = Guid.Empty;
                            tracingService.Trace("dollarhours = " + dollarhours);
                            tracingService.Trace("Adjusted Hours = " + adjustedHours);

                            if (isPensionPlan)
                            {
                                if (calculateAccuredBenefit)
                                {
                                    if (pensionBenefitCalculationType.Value == (int)PensionBenefitCalculationType.Hour)
                                        accruedBenefitAmount = benefitRate * adjustedHours;
                                    else
                                        accruedBenefitAmount = benefitRate * dollarhours;
                                }
                                decimal _actualHours = 0;

                                if ((transactionType == (int)PensionTransactionType.MemberRequired) || (transactionType == (int)PensionTransactionType.MemberVoluntary))
                                {
                                    _actualHours = 0;
                                }
                                else
                                {
                                    _actualHours = actualHours;
                                }

                                PensionTransactionHelper.Create(service, tracingService, transactionType, Contact, periodOfServiceId,
                                    rateT.FundId.Value, null, Submission, WorkMonth, adjustedHours, rateT.RateValue.Value,
                                    new Money(dollarhours), new Money(grosswages), _actualHours, accruedBenefitAmount,
                                    PluginConfiguration[PluginHelper.ConfigDefaultCurrency]);
                            }

                            TransactionHelper.CreateContributionTransaction(service, tracingService,
                                    Submission.Id, Contact.Id, WorkMonth, DateTime.MinValue,
                                    adjustedHours, new Money(grosswages), LabourRole.Id,
                                    new Money(dollarhours), rateT.FundId.Value, rateT.RateValue.Value,
                                    (int)BpaTransactionbpaTransactionCategory.Contributions,
                                    (int)BpaTransactionbpaTransactionType.PensionContribution,
                                    null, icihours, nonicihours, 0, null, null, false, Agreement, periodOfServiceId,
                                    (int)TransactionStatus.Pending, owner, entity.Id);
                        }
                    }

                    #endregion
                }
                else
                {
                    #region ----------- Else logic --------------------

                    tracingService.Trace("in Else");
                    if (rateT.RateValue != null)
                    {
                        tracingService.Trace("rateT.RateValue = " + rateT.RateValue.Value);
                        tracingService.Trace("rateT.CalculationType = " + rateT.CalculationType.Value);
                        if (rateT.CalculationType.Value != (int)RateCalculationType.Tax)
                        {
                            bool isIgnore = false;
                            if (rateT.CalculationType == (int)RateCalculationType.FlatRate)
                            {
                                if ((SubmissionType.Value == (int)SubmissionSubmissionType.EmployerAdjustment) ||
                                    (SubmissionType.Value == (int)SubmissionSubmissionType.EmployerRegular))
                                {
                                    if (FlatRateRule.Value == (int)BpaSubmissionDetailBpaFlatRateRule.Charge)
                                        dollarhours = 1;
                                    else if (FlatRateRule.Value == (int)BpaSubmissionDetailBpaFlatRateRule.Ignore)
                                    {
                                        dollarhours = 0;
                                        isIgnore = true;
                                    }
                                    if (FlatRateRule.Value == (int)BpaSubmissionDetailBpaFlatRateRule.Remove)
                                        dollarhours = -1;
                                }
                                else
                                    dollarhours = 1;
                            }
                            else if (rateT.CalculationType.Value != (int)RateCalculationType.PercentageGrossWages)
                            {
                                dollarhours = grosswages;
                            }
                            if (!isIgnore)
                            {
                                // Check fund type is Pesion ?
                                int transactionType = 0;
                                Guid periodOfServiceId = Guid.Empty;

                                if (rateT.FundType.Value == (int)BpaFundbpaFundType.Pension)
                                    transactionType = (int)BpaTransactionbpaTransactionType.PensionContribution;
                                else
                                    transactionType = (int)BpaTransactionbpaTransactionType.EmployerContribution;

                                tracingService.Trace("dollarhours = " + dollarhours);
                                TransactionHelper.CreateContributionTransaction(service, tracingService,
                                    Submission.Id, Contact.Id, WorkMonth, DateTime.MinValue,
                                    totalhours, new Money(grosswages), LabourRole.Id,
                                    new Money(rateT.RateValue.Value * dollarhours),
                                    rateT.FundId.Value, rateT.RateValue.Value,
                                    (int)BpaTransactionbpaTransactionCategory.Contributions,
                                    transactionType, null, 0, 0, 0, null, null, false, Agreement, periodOfServiceId,
                                    (int)TransactionStatus.Pending, owner, entity.Id);
                            }
                        }
                    }

                    #endregion
                }

                tracingService.Trace("end of rate loop");
            }
            #endregion

            #endregion

            tracingService.Trace("Before  Last Work Month ");
            
        }
    }
}