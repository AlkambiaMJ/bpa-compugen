﻿#region

using System;
using System.Collections.Generic;
using BPA.Benefit.Management.CRM.Plugins.Helper;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;

#endregion

namespace BPA.Benefit.Management.CRM.Plugins
{
    /// <summary>
    ///     SubmissionOnCreatePre PlugIn.
    /// </summary>
    public class SubmissionOnCreatePre : Plugin
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="SubmissionOnCreatePre" /> class.
        /// </summary>
        public SubmissionOnCreatePre()
            : base(typeof(SubmissionOnCreatePre))
        {
            RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(20, "Create",
                PluginHelper.BpaSubmission, ExecutePreSubmissionCreate));

            // Note : you can register for more events here if this plugin is not specific to an individual entity and message combination.
            // You may also need to update your RegisterFile.crmregister plug-in registration file to reflect any change.
        }

        /// <summary>
        ///     Executes the plug-in.
        /// </summary>
        /// <param name="localContext">
        ///     The <see cref="Plugin.LocalPluginContext" /> which contains the
        ///     <see cref="IPluginExecutionContext" />,
        ///     <see cref="IOrganizationService" />
        ///     and <see cref="ITracingService" />
        /// </param>
        /// <remarks>
        ///     For improved performance, Microsoft Dynamics CRM caches plug-in instances.
        ///     The plug-in's Execute method should be written to be stateless as the constructor
        ///     is not called for every invocation of the plug-in. Also, multiple system threads
        ///     could execute the plug-in at the same time. All per invocation state information
        ///     is stored in the context. This means that you should not use global variables in plug-ins.
        /// </remarks>
        protected void ExecutePreSubmissionCreate(LocalPluginContext localContext)
        {
            // DONE - TODO JK - Add comments and trace statements.  Check variables for null exceptions

            // If local context not passed in, raise error
            if (localContext == null)
                throw new ArgumentNullException($"Local context not passed in.");

            // Create the context variables
            IPluginExecutionContext context = localContext.PluginExecutionContext;
            ITracingService tracingService = localContext.TracingService;
            IOrganizationService service = localContext.OrganizationService;

            // If Target is not passed in, raise error
            tracingService.Trace("If Target is not passed in, raise error");
            if (!context.InputParameters.Contains(PluginHelper.Target) ||
                !(context.InputParameters[PluginHelper.Target] is Entity))
                throw new ArgumentNullException($"Target is not of type Entity.");

            // Create the target entity
            tracingService.Trace("Create the target entity");
            Entity entity = (Entity)context.InputParameters[PluginHelper.Target];
            
            //Get all values from entity
            int submissionType = 0;
            if (entity.Attributes.Contains("bpa_submissiontype"))
                submissionType = entity.GetAttributeValue<OptionSetValue>("bpa_submissiontype").Value;

            DateTime workMonth = DateTime.MinValue;
            if (entity.Attributes.Contains("bpa_submissiondate"))
                workMonth = entity.GetAttributeValue<DateTime>("bpa_submissiondate");
            tracingService.Trace("After WorkMonth = " + workMonth.ToShortDateString());

            EntityReference trustRef = null;
            if (entity.Attributes.Contains("bpa_trustid"))
            {
                trustRef = entity.GetAttributeValue<EntityReference>("bpa_trustid");
                tracingService.Trace("After Trust = " + trustRef);
            }

            EntityReference planRef = null;
            if (entity.Attributes.Contains("bpa_plan"))
            {
                planRef = entity.GetAttributeValue<EntityReference>("bpa_plan");
                tracingService.Trace("After Plan = " + planRef);
            }

            EntityReference agreementRef = null;
            if (entity.Attributes.Contains("bpa_agreementid"))
            {
                agreementRef = entity.GetAttributeValue<EntityReference>("bpa_agreementid");
                tracingService.Trace("After Agreement = " + agreementRef);
            }

            EntityReference memberPlanRef = null;
            if (entity.Attributes.Contains("bpa_memberplanid"))
            {
                memberPlanRef = entity.GetAttributeValue<EntityReference>("bpa_memberplanid");
                if(memberPlanRef != null)
                    tracingService.Trace("After Plan Detail = " + memberPlanRef.Id);
            }

            EntityReference employerRef = null;
            if (entity.Attributes.Contains("bpa_accountagreementid"))
            {
                employerRef = entity.GetAttributeValue<EntityReference>("bpa_accountagreementid");
                tracingService.Trace("After Employer = " + employerRef.Id);
            }

            int submissionnumberofmonths = 0;
            if (entity.Attributes.Contains("bpa_submissionnumberofmonths"))
                submissionnumberofmonths = entity.GetAttributeValue<int>("bpa_submissionnumberofmonths");
            tracingService.Trace("After Submission Number of Month = " + submissionnumberofmonths);

            // Member Self-Pay Validation 4 Rules
            // Rules 1: Member'S current eligibility is New Member/Under Review/Transfer Out/Frozen/Inactive
            // Rule 2: Reach Maximum Self-Pay limit
            // Rule 3: Member must be in 'In-Benefit' for Previous Month 
            // Rule 4: User is already In-Benefit for Month

            if ((submissionType == (int)SubmissionSubmissionType.Member) && memberPlanRef != null)
            {
                #region -------------- Rules Validation ----------------------------
                tracingService.Trace("In Checking Submission Type " + submissionType);

                Entity contact = ContactHelper.FetchMemberPlan(service, tracingService, memberPlanRef.Id);
                tracingService.Trace("Fetch Contact");

                //Rules 1: Member'S current eligibility is New Member/Under Review/Transfer Out/Frozen/Inactive
                int currenteligibility = 0;
                if (contact.Attributes.Contains("bpa_currenteligibility"))
                    currenteligibility = contact.GetAttributeValue<OptionSetValue>("bpa_currenteligibility").Value;

                if ((currenteligibility == (int)ContactbpaCurrentEligibility.NewMember) ||
                    (currenteligibility == (int)ContactbpaCurrentEligibility.Frozen) ||
                    (currenteligibility == (int)ContactbpaCurrentEligibility.TransferOut) ||
                    (currenteligibility == (int)ContactbpaCurrentEligibility.UnderReview) ||
                    (currenteligibility == (int)ContactbpaCurrentEligibility.Inactive))
                {
                    throw new InvalidPluginExecutionException(OperationStatus.Canceled,
                        @"Member can not able to do Self Pay as his current eligibility is 'New Member'/'Frozen'/'Inactive'/'Transfer Out'/'Under Review'.");
                }
                tracingService.Trace("After Rule 1");

                //Rule 2: Reach Maximum Self-Pay limit
                int eligibilityMaxSelfPay = 0;
                if (contact.Attributes.Contains("bpa_eligibilitycategory1.bpa_maxconsecutivemonthsselfpay"))
                    eligibilityMaxSelfPay =
                        (int)
                            contact.GetAttributeValue<AliasedValue>(
                                "bpa_eligibilitycategory1.bpa_maxconsecutivemonthsselfpay").Value;

                int contactSelfPay = 0;
                if (contact.Attributes.Contains("bpa_consecutiveselfpaymonths"))
                    contactSelfPay = contact.GetAttributeValue<int>("bpa_consecutiveselfpaymonths");

                if (contactSelfPay + submissionnumberofmonths > eligibilityMaxSelfPay)
                {
                    throw new InvalidPluginExecutionException(OperationStatus.Canceled,
                        @"Member reach Maximum Consecutive Months Self Pay.");
                }
                tracingService.Trace("After Rule 2");

                int offset = (int)contact.GetAttributeValue<AliasedValue>("bpa_eligibilitycategory1.bpa_eligibilityoffsetmonths").Value;
                List<KeyValuePair<string, DateTime>> lsBenefitMonths =
                    TransactionHelper.FetchAllBenefitTransactions(service, memberPlanRef.Id);

                // Rule 3: Member must be in 'In-Benefit' for Previous Month 
                if (!TransactionHelper.IsMemberPaidLastMonth(service, memberPlanRef.Id, workMonth.AddMonths(offset + -1), true))
                    throw new InvalidPluginExecutionException(OperationStatus.Canceled,
                        "Member Cannot skip the Months when making a Self-Payment. Member must be in Benefit in the month prior to the month payment is being made for.");

                tracingService.Trace("After Rule 3");

                // Rule 4: User is already In-Benefit for Month
                if (lsBenefitMonths.Count > 0)
                {
                    DateTime startDate = workMonth.AddMonths(offset);
                    tracingService.Trace("After Rule 4");
                }
                #endregion

            }
            else if ((submissionType == (int)SubmissionSubmissionType.EmployerRegular) || (submissionType == (int)SubmissionSubmissionType.EmployerMoneyOnly) || 
                (submissionType == (int)SubmissionSubmissionType.EmployerNoActivity)   ||(submissionType == (int)SubmissionSubmissionType.EmployerAdjustment))
            {
                #region ------------ Rules Validation for Employer Submission ----------------
                tracingService.Trace("Inside EmployerRegular");

                // Validate future month
                DateTime calenderDate = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddMonths(-1);
                workMonth = new DateTime(workMonth.Year, workMonth.Month, 1);

                if (workMonth > calenderDate)
                    throw new InvalidPluginExecutionException(OperationStatus.Canceled,
                        @"The Work Month you are selecting is for a future month. Your Submission can not be processed. Please select a different month for your submission.");
                tracingService.Trace("After EmployerRegular Pre Checking");

                // Find out Account Deatil - introduction Date, Termination Date, and Account Status
                tracingService.Trace("Before Employer Deatil Fetch");
                Entity accountDetail = AccountAgreementHelper.FetchAccountDetail(service, tracingService, employerRef.Id);

                if (accountDetail != null)
                {
                    DateTime introductiondate = accountDetail.Contains("bpa_introductiondate")
                        ? accountDetail.GetAttributeValue<DateTime>("bpa_introductiondate")
                        : DateTime.MinValue;
                    introductiondate = new DateTime(introductiondate.Year, introductiondate.Month,
                        introductiondate.Day);
                    DateTime terminationdate = accountDetail.Contains("bpa_terminationdate")
                        ? accountDetail.GetAttributeValue<DateTime>("bpa_terminationdate")
                        : DateTime.MaxValue;
                    terminationdate = new DateTime(terminationdate.Year, terminationdate.Month, terminationdate.Day);
                    OptionSetValue accountstatus = accountDetail.Contains("bpa_accountstatus")
                        ? accountDetail.GetAttributeValue<OptionSetValue>("bpa_accountstatus")
                        : new OptionSetValue((int)AccountStatus.Open); //else open

                    if ((accountstatus.Value == (int)AccountStatus.Open) &&
                        ((introductiondate > workMonth) || (workMonth >= terminationdate))) //open
                        throw new InvalidPluginExecutionException(OperationStatus.Canceled,
                            "The Work Month you are selecting is not within the company Start and End date range. Please select another Work Month to proceed .If you require assistance please contact BPA administration Department at 416 - 240 - 7480 .");
                    if ((accountstatus.Value == (int)AccountStatus.Closed) &&
                        ((introductiondate < workMonth) || (workMonth >= terminationdate))) //Close
                        throw new InvalidPluginExecutionException(OperationStatus.Canceled,
                            "The account you are selecting is closed. Please contact BPA administration Department at 416-240-7480 to have this account reactivated..");
                }
#endregion
            }

            #region ------------ Set Values  Submission Detail Trust, Plan and Agreement--------------------
            //Set the values of Submission deail Trust, Plan and Agreement
            if ((submissionType == (int)SubmissionSubmissionType.EmployerRegular) || (submissionType == (int)SubmissionSubmissionType.EmployerMoneyOnly) || 
                (submissionType == (int)SubmissionSubmissionType.EmployerNoActivity) || (submissionType == (int)SubmissionSubmissionType.EmployerAdjustment))
            {
                // Fetch employer information
                if (entity.Contains("bpa_accountagreementid"))
                {
                    employerRef = entity.GetAttributeValue<EntityReference>("bpa_accountagreementid");
                    Entity account = service.Retrieve(PluginHelper.AccountAgreement, employerRef.Id, new ColumnSet("bpa_planid", "bpa_agreementid"));
                    if (account.Contains("bpa_planid"))
                    {
                        planRef = account.GetAttributeValue<EntityReference>("bpa_planid");
                        agreementRef = account.GetAttributeValue<EntityReference>("bpa_agreementid");

                        // Fetch from plan
                        Entity plan = service.Retrieve(PluginHelper.BpaSector, planRef.Id, new ColumnSet(true));
                        if (plan.Contains("bpa_trust"))
                            trustRef = plan.GetAttributeValue<EntityReference>("bpa_trust");

                        //Fetch Default Labour role
                        Guid labourrole = RateHelper.FetchLabourRoleByAgreementId(service, agreementRef.Id);
                        if (labourrole != Guid.Empty)
                            entity["bpa_labourroleid"] = new EntityReference(PluginHelper.BpaLabourrole,labourrole);
                    }

                    if (submissionType == (int)SubmissionSubmissionType.EmployerMoneyOnly)
                    {
                        entity["bpa_totalhourssubmitted"] = (decimal)0;
                        entity["bpa_totalhoursearnedsubmitted"] = (decimal)0;

                        entity["bpa_totaldollarssubmitted"] = new Money(0);
                        entity["bpa_totalgrosswagessubmitted"] = new Money(0);
                        entity["bpa_submissionamount"] = new Money(0);
                        entity["bpa_openingvariance"] = new Money(0);
                        entity["bpa_totalsubmissiondue"] = new Money(0);

                    }
                }
            }
            else if ((submissionType == (int)SubmissionSubmissionType.Member) ||
                     (submissionType == (int)SubmissionSubmissionType.MemberSelfPayRefund))
            {
                tracingService.Trace("Inside 2nd Member Self-pay and Self-pay refund");

                if (entity.Contains("bpa_memberplanid"))
                {
                    tracingService.Trace("Inside bpa_memberplanid");

                    memberPlanRef = entity.GetAttributeValue<EntityReference>("bpa_memberplanid");
                    Entity planDetail = service.Retrieve(PluginHelper.BpaMemberplan, memberPlanRef.Id, new ColumnSet("bpa_planid"));
                    if (planDetail.Contains("bpa_planid"))
                    {
                        planRef = planDetail.GetAttributeValue<EntityReference>("bpa_planid");
                        tracingService.Trace("Plan = " + planRef.Id);

                        // Fetch from plan
                        Entity accountAgreement = AccountAgreementHelper.FetchSelePayAccountAgreementByPlanId(service, tracingService, planRef.Id);
                        if (accountAgreement == null)
                            throw new InvalidPluginExecutionException(OperationStatus.Failed, $@"Selected member plan does not have any self pay employer agreement setup. Please contact BPA Administrator.");

                        if (accountAgreement.Contains("bpa_sector1.bpa_trust"))
                            trustRef = (EntityReference)accountAgreement.GetAttributeValue<AliasedValue>("bpa_sector1.bpa_trust").Value;
                        tracingService.Trace("Trust = " + trustRef.Id);
                        
                        employerRef = new EntityReference(PluginHelper.AccountAgreement, accountAgreement.Id); // accountAgreement.GetAttributeValue<EntityReference>("bpa_selfpayaccountid");
                        tracingService.Trace("self Pay Account = " + employerRef.Id);

                        if (accountAgreement.Contains("bpa_agreementid"))
                        {
                            agreementRef = accountAgreement.GetAttributeValue<EntityReference>("bpa_agreementid");

                            Guid labourrole = RateHelper.FetchLabourRoleByAgreementId(service, agreementRef.Id);
                            if (labourrole != Guid.Empty)
                                entity["bpa_labourroleid"] = new EntityReference(PluginHelper.BpaLabourrole, labourrole);
                        }
                        entity["bpa_accountagreementid"] = employerRef;
                    }
                }
            }

            if (submissionType != (int)SubmissionSubmissionType.RetireeMonthlySubmission)
            {
                entity["bpa_trustid"] = trustRef;
                entity["bpa_plan"] = planRef;
                entity["bpa_agreementid"] = agreementRef;
            }
            #endregion
        }

    }
}