﻿#region

using System;
using BPA.Benefit.Management.CRM.Plugins.Helper;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;

#endregion

namespace BPA.Benefit.Management.CRM.Plugins
{
    /// <summary>
    ///     SubmissionDetailOnUpdatePre PlugIn.
    /// </summary>
    public class SubmissionDetailOnUpdatePre : Plugin
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="SubmissionDetailOnUpdatePre" /> class.
        /// </summary>
        public SubmissionDetailOnUpdatePre()
            : base(typeof(SubmissionDetailOnUpdatePre))
        {
            RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(20, "Update",
                "bpa_submissiondetail", ExecutePreUpdate));

            // Note : you can register for more events here if this plugin is not specific to an individual entity and message combination.
            // You may also need to update your RegisterFile.crmregister plug-in registration file to reflect any change.
        }

        /// <summary>
        ///     Executes the plug-in.
        /// </summary>
        /// <param name="localContext">
        ///     The <see cref="Plugin.LocalPluginContext" /> which contains the
        ///     <see cref="IPluginExecutionContext" />,
        ///     <see cref="IOrganizationService" />
        ///     and <see cref="ITracingService" />
        /// </param>
        /// <remarks>
        ///     For improved performance, Microsoft Dynamics CRM caches plug-in instances.
        ///     The plug-in's Execute method should be written to be stateless as the constructor
        ///     is not called for every invocation of the plug-in. Also, multiple system threads
        ///     could execute the plug-in at the same time. All per invocation state information
        ///     is stored in the context. This means that you should not use global variables in plug-ins.
        /// </remarks>
        protected void ExecutePreUpdate(LocalPluginContext localContext)
        {
            // If local context not passed in, raise error
            if (localContext == null)
                throw new ArgumentNullException($"Local context not passed in.");

            // Create the context variables
            IPluginExecutionContext context = localContext.PluginExecutionContext;
            ITracingService tracingService = localContext.TracingService;
            IOrganizationService service = localContext.OrganizationService;

            // If Target is not passed in, raise error
            tracingService.Trace("If Target is not passed in, raise error");
            if (!context.InputParameters.Contains(PluginHelper.Target) ||
                !(context.InputParameters[PluginHelper.Target] is Entity))
                throw new ArgumentNullException($"Target is not of type Entity.");

            // Create the target entity & preImage
            tracingService.Trace("Create the target entity & preImage");
            Entity entity = (Entity)context.InputParameters[PluginHelper.Target];
            Entity preImageEntity = context.PreEntityImages != null &&
                                    context.PreEntityImages.Contains(PluginHelper.PreImageAlias)
                ? context.PreEntityImages[PluginHelper.PreImageAlias]
                : null;
            if (preImageEntity == null)
                throw new ArgumentNullException($"preImage entity is not defined.");

            //if (!entity.Contains("bpa_unemployed")) return;
            //bool isUnemployed = entity.GetAttributeValue<bool>("bpa_unemployed");
            bool isUnemployed = false;
            if (entity.Contains("bpa_unemployed"))
                isUnemployed = entity.GetAttributeValue<bool>("bpa_unemployed");
            else if (preImageEntity.Contains("bpa_unemployed"))
                isUnemployed = preImageEntity.GetAttributeValue<bool>("bpa_unemployed");

            EntityReference selectedEmploymentRef = preImageEntity.Contains("bpa_employmentid")
                ? preImageEntity.GetAttributeValue<EntityReference>("bpa_employmentid")
                : null;

            EntityReference labourRoleRef = null;
            if (entity.Contains("bpa_labourroleid"))
                labourRoleRef = entity.GetAttributeValue<EntityReference>("bpa_labourroleid");
            else if (preImageEntity.Contains("bpa_labourroleid"))
                labourRoleRef = preImageEntity.GetAttributeValue<EntityReference>("bpa_labourroleid");

            EntityReference submissionIdRef = null;
            if (entity.Contains("bpa_submissionid"))
                submissionIdRef = entity.GetAttributeValue<EntityReference>("bpa_submissionid");
            else if (preImageEntity.Contains("bpa_submissionid"))
                submissionIdRef = preImageEntity.GetAttributeValue<EntityReference>("bpa_submissionid");
            Entity submissionRef = null;
            bool isSubmssionFromFile = false;

            if(submissionIdRef != null)
            {
                submissionRef = service.Retrieve(PluginHelper.BpaSubmission, submissionIdRef.Id, new ColumnSet("bpa_submissionfromfile")) ;
                isSubmssionFromFile = submissionRef.Contains("bpa_submissionfromfile") ? submissionRef.GetAttributeValue<bool>("bpa_submissionfromfile") : false;
            }

            if (selectedEmploymentRef != null && !isSubmssionFromFile)
            {
                if (isUnemployed)
                {
                    // Create new Employment Record
                    tracingService.Trace("Create new Employment Record");
                    Entity update = new Entity(PluginHelper.BpaEmployment)
                    {
                        Id = selectedEmploymentRef.Id,
                        ["bpa_workstatus"] = new OptionSetValue((int)EmployerWorkStatus.NotEmployed)
                    };
                    if (labourRoleRef != null)
                        update["bpa_labourrole"] = labourRoleRef;
                    service.Update(update);

                    // Clear out all the value of that submission
                    tracingService.Trace("Clear out all the value of that submission");
                    entity["bpa_icihours"] = null;
                    entity["bpa_nonicihours"] = null;
                    entity["bpa_totalhours"] = null;
                    entity["bpa_hoursearned"] = null;
                    entity["bpa_grosswages"] = null;
                    entity["bpa_dollars"] = null;
                    entity["bpa_pensionmemberrequired"] = null;
                    entity["bpa_pensionmembervoluntary"] = null;
                }
                else
                {
                    // Update Employment Record
                    tracingService.Trace("Update Employment Record");
                    Entity update = new Entity(PluginHelper.BpaEmployment)
                    {
                        Id = selectedEmploymentRef.Id,
                        ["bpa_workstatus"] = new OptionSetValue((int)EmployerWorkStatus.Employed)
                    };
                    if(labourRoleRef != null)
                        update["bpa_labourrole"] = labourRoleRef;

                    service.Update(update);
                    entity["bpa_unemployed"] = false;

                }
            }

            //Checking Only when Submission Type is Employer Regular
            OptionSetValue submissionType = (entity.Contains("bpa_submissiontype")) ? entity.GetAttributeValue<OptionSetValue>("bpa_submissiontype")
                : preImageEntity.GetAttributeValue<OptionSetValue>("bpa_submissiontype");
            EntityReference agreement = (entity.Contains("bpa_agreementid")) ? entity.GetAttributeValue<EntityReference>("bpa_agreementid")
                : preImageEntity.GetAttributeValue<EntityReference>("bpa_agreementid");

            if ((submissionType.Value == (int)SubmissionSubmissionType.EmployerRegular) || (submissionType.Value == (int)SubmissionSubmissionType.EmployerAdjustment))
            {
                //Check Agreement Calculation Type
                int calculationType = AgreementHelper.FetchAgreementCalculationType(service, tracingService, agreement.Id);
                
                decimal? icihours = entity.Contains("bpa_icihours") ? entity.GetAttributeValue<decimal?>("bpa_icihours") : preImageEntity.GetAttributeValue<decimal>("bpa_icihours");
                if (icihours != null && icihours == 0)
                    icihours = null;
                decimal? nonicihours = entity.Contains("bpa_nonicihours") ? entity.GetAttributeValue<decimal?>("bpa_nonicihours") : preImageEntity.GetAttributeValue<decimal>("bpa_nonicihours");
                if (nonicihours != null && nonicihours == 0)
                    nonicihours = null;

                switch (calculationType)
                {
                    case (int)BpaSubSectorBpaCalculationType.Icinonici:
                        decimal sumHours = 0;
                        if (icihours != null)
                            sumHours += icihours.Value;
                        if (nonicihours != null)
                            sumHours += nonicihours.Value;

                        entity["bpa_totalhours"] = sumHours;
                        break;
                }
            }

        }
    }
}