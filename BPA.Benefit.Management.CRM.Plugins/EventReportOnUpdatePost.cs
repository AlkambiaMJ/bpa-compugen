﻿#region

using System;
using BPA.Benefit.Management.CRM.Plugins.Helper;
using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Query;

#endregion

namespace BPA.Benefit.Management.CRM.Plugins
{
    /// <summary>
    ///     EventReportOnUpdatePost PlugIn.
    ///     Once user completed Event Report activity than all associated Event also need to completed and make inactive.
    /// 
    /// </summary>
    public class EventReportOnUpdatePost : Plugin
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="EventReportOnUpdatePost" /> class.
        /// </summary>
        public EventReportOnUpdatePost()
            : base(typeof(EventReportOnUpdatePost))
        {
            RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(40, "Update",
                PluginHelper.BpaEventreport, ExecutePostEventReportUpdate));

            // Note : you can register for more events here if this plugin is not specific to an individual entity and message combination.
            // You may also need to update your RegisterFile.crmregister plug-in registration file to reflect any change.
        }

        /// <summary>
        ///     Executes the plug-in.
        /// </summary>
        /// <param name="localContext">
        ///     The <see cref="Plugin.LocalPluginContext" /> which contains the
        ///     <see cref="IPluginExecutionContext" />,
        ///     <see cref="IOrganizationService" />
        ///     and <see cref="ITracingService" />
        /// </param>
        /// <remarks>
        ///     For improved performance, Microsoft Dynamics CRM caches plug-in instances.
        ///     The plug-in's Execute method should be written to be stateless as the constructor
        ///     is not called for every invocation of the plug-in. Also, multiple system threads
        ///     could execute the plug-in at the same time. All per invocation state information
        ///     is stored in the context. This means that you should not use global variables in plug-ins.
        /// </remarks>
        protected void ExecutePostEventReportUpdate(LocalPluginContext localContext)
        {
            // If local context not passed in, raise error
            if (localContext == null)
                throw new ArgumentNullException($"Local context not passed in.");

            // Create the context variables
            IPluginExecutionContext context = localContext.PluginExecutionContext;
            IOrganizationService service = localContext.OrganizationService;
            ITracingService tracingService = localContext.TracingService;

            // If Target is not passed in, raise error
            tracingService.Trace("If Target is not passed in, raise error");
            if (!context.InputParameters.Contains(PluginHelper.Target) ||
                !(context.InputParameters[PluginHelper.Target] is Entity))
                throw new ArgumentNullException($"Target is not of type Entity.");

            // Create the target entity
            tracingService.Trace("Create the target entity");
            Entity entity = (Entity)context.InputParameters[PluginHelper.Target];

            //Get all data from entity
            bool isCompleted = false;
            if (entity.Attributes.Contains("bpa_iscompleted"))
                isCompleted = entity.GetAttributeValue<bool>("bpa_iscompleted");
            tracingService.Trace($"Completed {isCompleted}");

            DateTime completedDate = DateTime.MinValue;
            if (entity.Attributes.Contains("bpa_completeddate"))
                completedDate = entity.GetAttributeValue<DateTime>("bpa_completeddate");
            tracingService.Trace($"Completed Date {completedDate.ToString()}");

            //stop executing if is not completed
            if (!isCompleted) return;

            //Fetch all Associated event activity it might be more than 5000 so we used following method 
            tracingService.Trace($"Declared main EntityCollection");
            EntityCollection events = new EntityCollection();
            QueryExpression queryServicios = new QueryExpression
            {
                EntityName = PluginHelper.BpaEvent,
                ColumnSet = new ColumnSet(true),
                Criteria =
                {
                    Filters =
                    {
                        new FilterExpression
                        {
                            FilterOperator = LogicalOperator.And,
                            Conditions =
                            {
                                new ConditionExpression("bpa_eventreport", ConditionOperator.Equal, entity.Id), 
                                new ConditionExpression("statecode", ConditionOperator.Equal, 0) //Active
                            }
                        }
                    }
                }
            };
            tracingService.Trace($"After Query");

            int pageNumber = 1;
            RetrieveMultipleResponse multiResponse = new RetrieveMultipleResponse();
            tracingService.Trace($"Before Loop");
            //Making loop if it is more than 5000 records
            do
            {
                queryServicios.PageInfo.Count = 5000;
                queryServicios.PageInfo.PagingCookie = pageNumber == 1
                    ? null
                    : multiResponse.EntityCollection.PagingCookie;
                queryServicios.PageInfo.PageNumber = pageNumber++;

                RetrieveMultipleRequest multiRequest = new RetrieveMultipleRequest {Query = queryServicios};
                multiResponse = (RetrieveMultipleResponse)service.Execute(multiRequest);

                events.Entities.AddRange(multiResponse.EntityCollection.Entities);
            } while (multiResponse.EntityCollection.MoreRecords);
            tracingService.Trace($"After Loop and Total Records {events.Entities.Count} ");

            //update event records make it inactive/completed
            if (events.Entities.Count <= 0) return;
            foreach (Entity e in events.Entities)
            {
                SetStateRequest state = new SetStateRequest
                {
                    State = new OptionSetValue(1), // Complete
                    Status = new OptionSetValue(2), // Completed
                    EntityMoniker = new EntityReference(PluginHelper.BpaEvent, e.Id)
                };
                SetStateResponse stateSet = (SetStateResponse)service.Execute(state);
            }
        }
    }
}