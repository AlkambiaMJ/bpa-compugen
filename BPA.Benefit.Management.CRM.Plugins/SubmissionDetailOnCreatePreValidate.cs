﻿#region

using System;
using BPA.Benefit.Management.CRM.Plugins.Helper;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using System.Text;

#endregion

namespace BPA.Benefit.Management.CRM.Plugins
{
    /// <summary>
    ///     SubmissionDetailOnCreatePre PlugIn.
    /// </summary>
    public class SubmissionDetailOnCreatePreValidate : Plugin
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="SubmissionDetailOnCreatePreValidate" /> class.
        /// </summary>
        public SubmissionDetailOnCreatePreValidate(string unsecureString, string secureString)
            : base(typeof(SubmissionDetailOnCreatePreValidate), unsecureString, secureString)
        {
            RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(10, PluginHelper.Create,
                PluginHelper.BpaSubmissiondetail, ExecuteOnCreatePre));

            // Note : you can register for more events here if this plugin is not specific to an individual entity and message combination.
            // You may also need to update your RegisterFile.crmregister plug-in registration file to reflect any change.
        }

        /// <summary>
        ///     Executes the plug-in.
        /// </summary>
        /// <param name="localContext">
        ///     The <see cref="Plugin.LocalPluginContext" /> which contains the
        ///     <see cref="IPluginExecutionContext" />,
        ///     <see cref="IOrganizationService" />
        ///     and <see cref="ITracingService" />
        /// </param>
        /// <remarks>
        ///     For improved performance, Microsoft Dynamics CRM caches plug-in instances.
        ///     The plug-in's Execute method should be written to be stateless as the constructor
        ///     is not called for every invocation of the plug-in. Also, multiple system threads
        ///     could execute the plug-in at the same time. All per invocation state information
        ///     is stored in the context. This means that you should not use global variables in plug-ins.
        /// </remarks>
        protected void ExecuteOnCreatePre(LocalPluginContext localContext)
        {
            // If local context not passed in, raise error
            if (localContext == null)
                throw new ArgumentNullException($"Local context not passed in.");

            // Create the context variables
            IPluginExecutionContext context = localContext.PluginExecutionContext;
            IOrganizationService service = localContext.OrganizationService;
            ITracingService tracingService = localContext.TracingService;

            // If Target is not passed in, raise error
            tracingService.Trace("If Target is not passed in, raise error");
            if (!context.InputParameters.Contains(PluginHelper.Target) ||
                !(context.InputParameters[PluginHelper.Target] is Entity))
                throw new ArgumentNullException($"Target is not of type Entity.");

            // Create the target entity
            tracingService.Trace("Create the target entity");
            Entity entity = (Entity)context.InputParameters[PluginHelper.Target];

            // If the Complete Batch flag is checked
            localContext.Trace("Start the ExecuteOnCreatePre.");

            EntityReference recordOwner = entity.Contains("ownerid") ? entity.GetAttributeValue<EntityReference>("ownerid") : null;

            if (recordOwner != null && recordOwner.LogicalName != "team")
            {
                //"team"
                // Fetch Submission Detail
                EntityReference submissionRef = entity.Contains("bpa_submissionid") ? entity.GetAttributeValue<EntityReference>("bpa_submissionid") : null;
                tracingService.Trace("Fetch Submission Detail");

                if (submissionRef != null)
                {
                    Entity submission = SubmissionHelper.FetchSubmissionOwner(service, tracingService, submissionRef.Id);
                    EntityReference owner = submission.GetAttributeValue<EntityReference>("ownerid");
                    if (owner != null)
                        entity["ownerid"] = owner;
                }
            }
        }
        
    }
}