﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BPA.Benefit.Management.CRM.Plugins.Helper;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;

namespace BPA.Benefit.Management.CRM.Plugins
{
    public class ExecuteRetroProcessingAsync : IPlugin
    {
        public void Execute(IServiceProvider serviceProvider)
        {
            IPluginExecutionContext context = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));
            IOrganizationServiceFactory serviceFactory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
            // Create service with context of current user
            IOrganizationService service = serviceFactory.CreateOrganizationService(context.UserId);
            //create tracing service
            ITracingService tracingService = (ITracingService)serviceProvider.GetService(typeof(ITracingService));

            //To get access to the image of the Quote record
            EntityReference entityRef = context.InputParameters["Target"] as EntityReference;
            tracingService.Trace(" Plugin Called - ExecuteRetroProcessingAsync");
            //try
            //{
                // To All INput Parameters
                EntityReference recordId = context.InputParameters["RecordId"] as EntityReference;
                DateTime retroProcessingDate = (DateTime) context.InputParameters["RetroProcessingDate"] ;

                // Check member's plan is Benefit Plan if so than and then run retro processing 
                if (PlanHelper.IsBenefitPlan(service, tracingService, recordId.Id))
                {
                    tracingService.Trace("Plan is Benefit Plan and run retro processing");
                    Entity memberplan_reto = new Entity("bpa_memberplan")
                    {
                        Id = recordId.Id,
                        ["bpa_retroprocessingdate"] = retroProcessingDate
                    };
                    service.Update(memberplan_reto);
                    
                }
            //}
            //catch (Exception ex)
            //{
            //    
            //}
            //return;
        }

    }
}
