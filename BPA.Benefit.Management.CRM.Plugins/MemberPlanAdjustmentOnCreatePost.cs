﻿#region

using System;
using BPA.Benefit.Management.CRM.Plugins.Helper;
using Microsoft.Xrm.Sdk;

#endregion

namespace BPA.Benefit.Management.CRM.Plugins
{
    /// <summary>
    ///     MemberPlanAdjustmentOnCreatePost PlugIn.
    /// </summary>
    public class MemberPlanAdjustmentOnCreatePost : Plugin
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="MemberPlanAdjustmentOnCreatePost" /> class.
        /// </summary>
        public MemberPlanAdjustmentOnCreatePost()
            : base(typeof(MemberPlanAdjustmentOnCreatePost))
        {
            RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(40, "Create",
                PluginHelper.BpaMemberPlanadjustment, PostExecuteCreate));
            RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(40, "Update",
                PluginHelper.BpaMemberPlanadjustment, PostExecuteCreate));

            // Note : you can register for more events here if this plugin is not specific to an individual entity and message combination.
            // You may also need to update your RegisterFile.crmregister plug-in registration file to reflect any change.
        }

        /// <summary>
        ///     Executes the plug-in.
        /// </summary>
        /// <param name="localContext">
        ///     The <see cref="Plugin.LocalPluginContext" /> which contains the
        ///     <see cref="IPluginExecutionContext" />,
        ///     <see cref="IOrganizationService" />
        ///     and <see cref="ITracingService" />
        /// </param>
        /// <remarks>
        ///     For improved performance, Microsoft Dynamics CRM caches plug-in instances.
        ///     The plug-in's Execute method should be written to be stateless as the constructor
        ///     is not called for every invocation of the plug-in. Also, multiple system threads
        ///     could execute the plug-in at the same time. All per invocation state information
        ///     is stored in the context. This means that you should not use global variables in plug-ins.
        /// </remarks>
        /// 
        protected void PostExecuteCreate(LocalPluginContext localContext)
        {
            return;
            #region --- 
            // DONE - TODO JK - Add trace statement after each comment

            //// If local context not passed in, raise error
            //if (localContext == null)
            //    throw new ArgumentNullException($"Local context not passed in.");

            //// Create the context variables
            //IPluginExecutionContext context = localContext.PluginExecutionContext;
            //IOrganizationService service = localContext.OrganizationService;
            //ITracingService tracingService = localContext.TracingService;
            //IServiceProvider serviceProvider = localContext.ServiceProvider;

            //// If Target is not passed in, raise error
            //tracingService.Trace("If Target is not passed in, raise error");
            //if (!context.InputParameters.Contains(PluginHelper.Target) ||
            //    !(context.InputParameters[PluginHelper.Target] is Entity))
            //    throw new ArgumentNullException($"Target is not of type Entity.");

            //// Create the target entity & postImage
            //tracingService.Trace("Create the target entity & postImage");
            //Entity entity = (Entity)context.InputParameters[PluginHelper.Target];
            //Entity postImage = context.PostEntityImages != null &&
            //                         context.PostEntityImages.Contains(PluginHelper.PostImageAlias)
            //    ? context.PostEntityImages[PluginHelper.PostImageAlias] : null;
            //if (context.MessageName.ToLower() == PluginHelper.Update.ToLower() && postImage == null)
            //    throw new ArgumentNullException($"Post Image entity is not defined.");

            //// Get the current user id
            //Guid runningUserId = localContext.PluginExecutionContext.UserId;

            //// Following method called only when new Member Plan Details created 
            //tracingService.Trace("Get all data from Entity or Post Image");

            //EntityReference memberPlan = null;
            //if (entity.Contains("bpa_memberplanid"))
            //    memberPlan = entity.GetAttributeValue<EntityReference>("bpa_memberplanid");
            //else if (postImage != null && postImage.Contains("bpa_memberplanid"))
            //    memberPlan = postImage.GetAttributeValue<EntityReference>("bpa_memberplanid");
            //tracingService.Trace($"After Member Plan {memberPlan.Id}");


            ////bpa_adjustmenttype
            //OptionSetValue adjustmentType = new OptionSetValue(0);
            //if (entity.Contains("bpa_adjustmenttype"))
            //    adjustmentType = entity.GetAttributeValue<OptionSetValue>("bpa_adjustmenttype");
            //else if (postImage != null && postImage.Contains("bpa_adjustmenttype"))
            //    adjustmentType = postImage.GetAttributeValue<OptionSetValue>("bpa_adjustmenttype");

            //DateTime adjustmentDate = DateTime.MinValue;
            //if (adjustmentType.Value == (int)MemberPlanAdjustmentType.Other)
            //{
            //    #region ---------------- TYPE = OTHER --------------

            //    if (entity.Contains("bpa_adjustmentdate"))
            //        adjustmentDate = entity.GetAttributeValue<DateTime>("bpa_adjustmentdate");
            //    else if (postImage != null && postImage.Contains("bpa_adjustmentdate"))
            //        adjustmentDate = postImage.GetAttributeValue<DateTime>("bpa_adjustmentdate");
            //    tracingService.Trace($"After Adjustment Date {adjustmentDate.ToString()}");

            //    //Validate get value
            //    if ((memberPlan == null) || (adjustmentDate == DateTime.MinValue))
            //    {
            //        throw new InvalidPluginExecutionException(
            //            "Either Member or Adjustment date is blank. Please enter value.");

            //    }

            //    //Checking for Adjustment date is future month throw error
            //    if (adjustmentDate != DateTime.MinValue)
            //    {
            //        if (adjustmentDate >= new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1))
            //            throw new InvalidPluginExecutionException(
            //                "The Adjustment Month you are selecting is for a future month. Your Adjustment can not be processed. Please select a different month for your adjustment.");
            //    }

            //    adjustmentDate = new DateTime(adjustmentDate.Year, adjustmentDate.Month, 1);
            //    #endregion

            //    tracingService.Trace("Inside if (adjustmentType.Value == (int)MemberPlanAdjustmentType.Other)");
            //    #region ---------------- TYPE = OTHER --------------
            //    // Getting all values
            //    Money dollars = new Money(0);
            //    tracingService.Trace($"Before dollars: {dollars.Value.ToString()}");
            //    if (entity.Contains("bpa_dollars"))
            //        dollars = entity.GetAttributeValue<Money>("bpa_dollars");
            //    else if (postImage != null && postImage.Contains("bpa_dollars"))
            //        dollars = postImage.GetAttributeValue<Money>("bpa_dollars");
            //    tracingService.Trace($"After Dollar {dollars.Value.ToString()}");

            //    Money vacationPay = new Money(0);
            //    if (entity.Contains("bpa_vacationpay"))
            //        vacationPay = entity.GetAttributeValue<Money>("bpa_vacationpay");
            //    else if (postImage != null && postImage.Contains("bpa_vacationpay"))
            //        vacationPay = postImage.GetAttributeValue<Money>("bpa_vacationpay");
            //    tracingService.Trace($"After Vacation Pay {vacationPay.Value.ToString()}");

            //    Money selfPay = new Money(0);
            //    if (entity.Contains("bpa_selfpay"))
            //        selfPay = entity.GetAttributeValue<Money>("bpa_selfpay");
            //    else if (postImage != null && postImage.Contains("bpa_selfpay"))
            //        selfPay = postImage.GetAttributeValue<Money>("bpa_selfpay");
            //    tracingService.Trace($"After Self-Pay {selfPay.Value.ToString()}");

            //    decimal hours = 0;
            //    tracingService.Trace($"BEFORE hour {hours.ToString()}");

            //    if (entity.Contains("bpa_hours"))
            //        hours = entity.GetAttributeValue<decimal>("bpa_hours");
            //    else if (postImage != null && postImage.Contains("bpa_hours"))
            //        hours = postImage.GetAttributeValue<decimal>("bpa_hours");
            //    tracingService.Trace($"After hour {hours.ToString()}");

            //    //following code execute only when message is 'Update'. it will delete all the existing transation records
            //    if (context.MessageName == PluginHelper.Update)
            //    {
            //        #region ----- Update MEssage ----------
            //        tracingService.Trace($"In Update Message");

            //        // Find out all Transacton
            //        EntityCollection transactions = TransactionHelper.FetAllTransactionsByMemberAdjustment(service, tracingService, entity.Id);

            //        tracingService.Trace($"after fetch");
            //        if (transactions != null && transactions.Entities.Count > 0)
            //        {
            //            tracingService.Trace($"Found Transation record(s) {transactions.Entities.Count}");

            //            //Fetch Admin REcord
            //            string callerId = ConfigurationHelper.FetchValueId(service, PluginHelper.ConfigCrmAdmin);
            //            tracingService.Trace($"Admin Id {callerId.ToString()} ");

            //            //Assigned Organization service to CRM Admin User for Deletion of transaction
            //            IOrganizationServiceFactory serviceFactory =
            //                (IOrganizationServiceFactory)
            //                    serviceProvider.GetService(typeof(IOrganizationServiceFactory));
            //            service = serviceFactory.CreateOrganizationService(new Guid(callerId));
            //            tracingService.Trace($"Assigned to Organization Service");

            //            //Delete all trasactions
            //            tracingService.Trace($"Before deletion");
            //            foreach (Entity t in transactions.Entities)
            //                service.Delete(PluginHelper.BpaTransaction, t.Id);

            //            // Back to Execute Running User
            //            tracingService.Trace($"Assigned back Organization Service to Execute User");
            //            serviceFactory =
            //                (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
            //            service = serviceFactory.CreateOrganizationService(runningUserId);
            //        }
            //        #endregion
            //    }
            //    //Dollar value is not nulll then create Dollar Bank Adjustment
            //    if (dollars.Value != 0)
            //    {
            //        tracingService.Trace($"Dollar Bank Adjustment");
            //        TransactionHelper.CreateTransaction(service, tracingService, memberPlan, adjustmentDate,
            //            (int)BpaTransactionbpaTransactionCategory.Adjustment,
            //            (int)BpaTransactionbpaTransactionType.DollarBankAdjustment, entity.Id, hours, dollars);

            //        tracingService.Trace($"Dollar Bank Adjustment Roll up");
            //        PluginHelper.TriggerRollup(service, tracingService, PluginHelper.BpaMemberplan, memberPlan.Id,
            //            "bpa_contributiondollarbank");
            //    }
            //    //Vacation value is not nulll then create Vacation adjustment
            //    if (vacationPay.Value != 0)
            //    {
            //        tracingService.Trace($"Vacation Bank Adjustment");
            //        TransactionHelper.CreateTransaction(service, tracingService, memberPlan, adjustmentDate,
            //            (int)BpaTransactionbpaTransactionCategory.Adjustment,
            //            (int)BpaTransactionbpaTransactionType.VacationPayAdjustment, entity.Id, hours, dollars);

            //        tracingService.Trace($"Vacation Bank Rollup");
            //        PluginHelper.TriggerRollup(service, tracingService, PluginHelper.BpaMemberplan, memberPlan.Id,
            //            "bpa_vacationpaybank");
            //    }

            //    if (selfPay.Value != 0)
            //    {
            //        tracingService.Trace($"Self pay Adjustment");
            //        TransactionHelper.CreateTransaction(service, tracingService, memberPlan, adjustmentDate,
            //            (int)BpaTransactionbpaTransactionCategory.Adjustment,
            //            (int)BpaTransactionbpaTransactionType.SelfPayBankAdjustment, entity.Id, hours, dollars);

            //        tracingService.Trace($"Self pay Adjustment Rollup");
            //        PluginHelper.TriggerRollup(service, tracingService, PluginHelper.BpaMemberplan, memberPlan.Id,
            //            "bpa_selfpaybank");
            //    }

            //    if (hours != 0)
            //    {
            //        tracingService.Trace($"Hour Bank Adjutment");
            //        TransactionHelper.CreateTransaction(service, tracingService, memberPlan, adjustmentDate,
            //            (int)BpaTransactionbpaTransactionCategory.Adjustment,
            //            (int)BpaTransactionbpaTransactionType.HourBankAdjustment, entity.Id, hours, dollars);

            //        tracingService.Trace($"Hour Bank Rollup");
            //        PluginHelper.TriggerRollup(service, tracingService, PluginHelper.BpaMemberplan, memberPlan.Id,
            //            "bpa_contributionhourbank");
            //    }


            //    // Run Member Retro processing
            //    string retroprocessingMessage = MemberRetroprocessing.RecalculateTransaction(service, tracingService,
            //        memberPlan, adjustmentDate, false);
            //    if (!string.IsNullOrEmpty(retroprocessingMessage))
            //        throw new InvalidPluginExecutionException(retroprocessingMessage);
            //    #endregion
            //}

            #endregion
        }
    }
}