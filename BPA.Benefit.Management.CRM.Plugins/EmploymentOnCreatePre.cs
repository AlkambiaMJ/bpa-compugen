﻿#region

using System;
using System.Linq;
using BPA.Benefit.Management.CRM.Plugins.Helper;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;

#endregion

namespace BPA.Benefit.Management.CRM.Plugins
{
    /// <summary>
    ///     DepositOnUpdatePre PlugIn.
    /// </summary>
    public class EmploymentOnCreatePre : Plugin
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="DepositOnUpdatePre" /> class.
        /// </summary>
        public EmploymentOnCreatePre()
            : base(typeof(EmploymentOnCreatePre))
        {
            RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(20, "Create",
                PluginHelper.BpaEmployment, ExecutePreEmploymentCreate));

            // Note : you can register for more events here if this plugin is not specific to an individual entity and message combination.
            // You may also need to update your RegisterFile.crmregister plug-in registration file to reflect any change.
        }

        /// <summary>
        ///     Executes the plug-in.
        /// </summary>
        /// <param name="localContext">
        ///     The <see cref="Plugin.LocalPluginContext" /> which contains the
        ///     <see cref="IPluginExecutionContext" />,
        ///     <see cref="IOrganizationService" />
        ///     and <see cref="ITracingService" />
        /// </param>
        /// <remarks>
        ///     For improved performance, Microsoft Dynamics CRM caches plug-in instances.
        ///     The plug-in's Execute method should be written to be stateless as the constructor
        ///     is not called for every invocation of the plug-in. Also, multiple system threads
        ///     could execute the plug-in at the same time. All per invocation state information
        ///     is stored in the context. This means that you should not use global variables in plug-ins.
        /// </remarks>
        protected void ExecutePreEmploymentCreate(LocalPluginContext localContext)
        {
            // If local context not passed in, raise error
            if (localContext == null)
                throw new ArgumentNullException($"Local context not passed in.");

            // Create the context variables
            IPluginExecutionContext context = localContext.PluginExecutionContext;
            ITracingService tracingService = localContext.TracingService;
            IOrganizationService service = localContext.OrganizationService;

            // If Target is not passed in, raise error
            tracingService.Trace("If Target is not passed in, raise error");
            if (!context.InputParameters.Contains(PluginHelper.Target) ||
                !(context.InputParameters[PluginHelper.Target] is Entity))
                throw new ArgumentNullException($"Target is not of type Entity.");

            // Create the target entity & preImage
            tracingService.Trace("Create the target entity & preImage");
            Entity entity = (Entity)context.InputParameters[PluginHelper.Target];

            // Member Plan already exists for Current Employee? Bug# 1293
            EntityReference employer = entity.GetAttributeValue<EntityReference>("bpa_accountagreementid");
            EntityReference memberPlan = entity.GetAttributeValue<EntityReference>("bpa_memberplanid");

            if(employer != null && memberPlan != null)
            {
                tracingService.Trace("Validate Employment Record already exists");
                // Check Member Already exists in Employment Record?
                Entity employee = EmploymentHelper.IsEmploymentExists(service, memberPlan.Id, employer.Id);
                if (employee != null)
                    throw new InvalidPluginExecutionException(OperationStatus.Failed, $@"The member you are trying to add already has an employment record for this Employer. The record can not be saved.");
            }

            if (employer == null)
                throw new InvalidPluginExecutionException(OperationStatus.Failed, $@"Employer must be entered.");
            Entity account = service.Retrieve(PluginHelper.AccountAgreement, employer.Id, new ColumnSet("bpa_agreementid"));

            if (account == null)
                throw new InvalidPluginExecutionException(OperationStatus.Failed, $@"Employer does not configure properly. ");

            EntityReference agreement = null;

            if (!entity.Contains("bpa_agreementid"))
            {
                agreement = account.GetAttributeValue<EntityReference>("bpa_agreementid");
                entity["bpa_agreementid"] = agreement;
            }

            //Get Values of Job Specifiacation
            if (!entity.Contains("bpa_labourrole") && agreement != null)
            {
                Entity defaultRole = LabourRoleHelper.FetchDefaultLabourRole(service, agreement.Id);

                if (defaultRole == null)
                    return;

                //Set Value
                entity["bpa_labourrole"] = new EntityReference(PluginHelper.BpaLabourrole, defaultRole.Id);
            }
            
        }
    }
}