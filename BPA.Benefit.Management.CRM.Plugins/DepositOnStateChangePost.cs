﻿#region

using System;
using BPA.Benefit.Management.CRM.Plugins.Helper;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using System.Collections.Generic;

#endregion

namespace BPA.Benefit.Management.CRM.Plugins
{
    /// <summary>
    ///     MemberPlanAdjustmentOnCreatePost PlugIn.
    /// </summary>
    public class DepositOnStateChangePost : Plugin
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="MemberPlanAdjustmentOnCreatePost" /> class.
        /// </summary>
        public DepositOnStateChangePost(string unsecureString, string secureString)
            : base(typeof(DepositOnStateChangePost), unsecureString, secureString)
        {
            RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(40, "SetState", PluginHelper.BpaDeposit, ExecuteStateChangeUpdate));
            RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(40, "SetStateDynamicEntity", PluginHelper.BpaDeposit, ExecuteStateChangeUpdate));

            // Note : you can register for more events here if this plugin is not specific to an individual entity and message combination.
            // You may also need to update your RegisterFile.crmregister plug-in registration file to reflect any change.
        }

        /// <summary>
        ///     Executes the plug-in.
        /// </summary>
        /// <param name="localContext">
        ///     The <see cref="Plugin.LocalPluginContext" /> which contains the
        ///     <see cref="IPluginExecutionContext" />,
        ///     <see cref="IOrganizationService" />
        ///     and <see cref="ITracingService" />
        /// </param>
        /// <remarks>
        ///     For improved performance, Microsoft Dynamics CRM caches plug-in instances.
        ///     The plug-in's Execute method should be written to be stateless as the constructor
        ///     is not called for every invocation of the plug-in. Also, multiple system threads
        ///     could execute the plug-in at the same time. All per invocation state information
        ///     is stored in the context. This means that you should not use global variables in plug-ins.
        /// </remarks>
        /// 
        protected void ExecuteStateChangeUpdate(LocalPluginContext localContext)
        {
            // If local context not passed in, raise error
            if (localContext == null)
                throw new ArgumentNullException($"Local context not passed in.");

            // Create the context variables
            IPluginExecutionContext context = localContext.PluginExecutionContext;
            IOrganizationService service = localContext.OrganizationService;
            ITracingService tracingService = localContext.TracingService;
            IServiceProvider serviceProvider = localContext.ServiceProvider;

            // If Target is not passed in, raise error
            tracingService.Trace("If Target is not passed in, raise error");
            if (!context.InputParameters.Contains(PluginHelper.EntityMoniker) ||
                !(context.InputParameters[PluginHelper.EntityMoniker] is EntityReference))
                throw new ArgumentNullException($"EntityReference is not passed in.");

            // Create the EntityReference
            tracingService.Trace("Create the EntityReference");
            EntityReference targetEntity = (EntityReference)context.InputParameters[PluginHelper.EntityMoniker];
            if (targetEntity == null)
                throw new ArgumentNullException($"EntityReference is not defined.");

            // Get the record's state & status
            tracingService.Trace("Get the record's state & status");
            OptionSetValue status = (OptionSetValue)context.InputParameters["Status"];

            // only execute if Status is Inactive
            if (status.Value == 1) return;

            //Get Pre Image
            Entity preImageEntity = context.PreEntityImages != null && context.PreEntityImages.Contains(PluginHelper.PreImageAlias) ? context.PreEntityImages[PluginHelper.PreImageAlias] : null;

            if (preImageEntity == null)
                throw new ArgumentNullException($"preImage entity is not defined. (MemberPlanAdjustmentOnStateChangePost Plguin) ");
            int depositStatus = preImageEntity.Contains("bpa_depositstatus") ? preImageEntity.GetAttributeValue<OptionSetValue>("bpa_depositstatus").Value : 0;

            // only execute when deposite status is deposited
            if (depositStatus != (int)DepositDepositStatus.Deposited) return;


            // TESTING - Record the processing start time
            DateTime retroprocessingStartTime = DateTime.Now;

            // Change All linked Submissions to Inactive
            tracingService.Trace("Change All linked Submissions to Inactive");
            EntityCollection submissions = SubmissionHelper.FetchAllSubmissionsByDepositId(service, tracingService, targetEntity.Id, false);
            if (submissions != null && submissions.Entities.Count > 0)
            {
                // Loop though all linked Submissions and make them Inactive
                tracingService.Trace("Loop though all linked Submissions and make them Inactive");
                foreach (Entity submission in submissions.Entities)
                {
                    // Work month
                    DateTime workMonth = submission.Contains("bpa_submissiondate") ? submission.GetAttributeValue<DateTime>("bpa_submissiondate") : DateTime.MinValue;

                    // Get all Susmission detail 
                    EntityCollection SubmissionDetails = SubmissionHelper.FetchAllSubmissionDetailBySubmissionId(service, tracingService, submission.Id, false);
                    if (SubmissionDetails != null && SubmissionDetails.Entities.Count > 0)
                    {
                        foreach (Entity submissiondetail in SubmissionDetails.Entities)
                        {
                            Guid memberPlanId = submissiondetail.Contains("bpa_memberplanid") ? submissiondetail.GetAttributeValue<EntityReference>("bpa_memberplanid").Id : Guid.Empty;
                            tracingService.Trace($"Inside Member Plan Id - {memberPlanId}");

                            // Check member's plan is Benefit Plan if so than and then run retro processing 
                            if (PlanHelper.IsBenefitPlan(service, tracingService, memberPlanId))
                            {
                                try
                                {
                                    tracingService.Trace("Plan is Benefit Plan and run retro processing");
                                    Entity memberplan_reto = new Entity(PluginHelper.BpaMemberplan)
                                    {
                                        Id = memberPlanId,
                                        ["bpa_retroprocessingdate"] = workMonth
                                    };
                                    service.Update(memberplan_reto);
                                }
                                catch (Exception e) { }
                            }
                        }
                    }
                }
            }



            // TESTING - Calculate time difference from start processing to end processing time
            DateTime endTime = DateTime.Now;
            double retroprocessingTotalMinutes = endTime.Subtract(retroprocessingStartTime).Minutes;
            double retroprocessingTotalSeconds = endTime.Subtract(retroprocessingStartTime).Seconds;
            double retroprocessingTotalMilliSeconds = endTime.Subtract(retroprocessingStartTime).Milliseconds;

            string executionTime = "Retroprocessing Minutes: " + retroprocessingTotalMinutes.ToString() +
                " Seconds: " + retroprocessingTotalSeconds.ToString() +
                " Milliseconds: " + retroprocessingTotalMilliSeconds.ToString() + Environment.NewLine;

        }
    }
}