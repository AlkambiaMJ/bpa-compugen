﻿#region

using System;
using BPA.Benefit.Management.CRM.Plugins.Helper;
using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Query;

#endregion

namespace BPA.Benefit.Management.CRM.Plugins
{
    /// <summary>
    ///     EventReportOnCreatePost PlugIn.
    ///     On Creation of the Event Report record, fetch all the event record for same type and associate/update Event record
    /// </summary>
    public class EventReportOnCreatePost : Plugin
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="EventReportOnCreatePost" /> class.
        /// </summary>
        public EventReportOnCreatePost()
            : base(typeof(EventReportOnCreatePost))
        {
            RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(40, "Create",
                PluginHelper.BpaEventreport, ExecutePostEventReportCreate));

            // Note : you can register for more events here if this plugin is not specific to an individual entity and message combination.
            // You may also need to update your RegisterFile.crmregister plug-in registration file to reflect any change.
        }

        /// <summary>
        ///     Executes the plug-in.
        /// </summary>
        /// <param name="localContext">
        ///     The <see cref="Plugin.LocalPluginContext" /> which contains the
        ///     <see cref="IPluginExecutionContext" />,
        ///     <see cref="IOrganizationService" />
        ///     and <see cref="ITracingService" />
        /// </param>
        /// <remarks>
        ///     For improved performance, Microsoft Dynamics CRM caches plug-in instances.
        ///     The plug-in's Execute method should be written to be stateless as the constructor
        ///     is not called for every invocation of the plug-in. Also, multiple system threads
        ///     could execute the plug-in at the same time. All per invocation state information
        ///     is stored in the context. This means that you should not use global variables in plug-ins.
        /// </remarks>
        protected void ExecutePostEventReportCreate(LocalPluginContext localContext)
        {
            // If local context not passed in, raise error
            if (localContext == null)
                throw new ArgumentNullException($"Local context not passed in.");

            // Create the context variables
            IPluginExecutionContext context = localContext.PluginExecutionContext;
            IOrganizationService service = localContext.OrganizationService;
            ITracingService tracingService = localContext.TracingService;

            // If Target is not passed in, raise error
            tracingService.Trace("If Target is not passed in, raise error");
            if (!context.InputParameters.Contains(PluginHelper.Target) ||
                !(context.InputParameters[PluginHelper.Target] is Entity))
                throw new ArgumentNullException($"Target is not of type Entity.");

            // Create the target entity and get all data from Entity
            tracingService.Trace("Create the target entity");
            Entity entity = (Entity)context.InputParameters[PluginHelper.Target];

            int reportType = 0;
            if (entity.Attributes.Contains("bpa_reporttype"))
                reportType = entity.GetAttributeValue<OptionSetValue>("bpa_reporttype").Value;
            tracingService.Trace($"Report Type is {reportType.ToString()}");

            EntityReference trust = entity.Contains("bpa_trustid") ? entity.GetAttributeValue<EntityReference>("bpa_trustid") : null;
            if (trust == null) return;

            //Fetch all records from Event. USed following function to get more than 5000 records
            EntityCollection events = new EntityCollection();
            string fetchXml = $@"
                <fetch mapping='logical' output-format='xml-platform' version='1.0' >
                  <entity name='bpa_event' >
                    <filter>
                      <condition attribute='statecode' operator='eq' value='0' />
                      <condition attribute='statuscode' operator='eq' value='1' />
                      <condition attribute='bpa_eventreport' operator='null' />
                      <condition attribute='bpa_eventtype' operator='eq' value='{reportType.ToString()}' />
                    </filter>
                    <link-entity name='bpa_memberplan' from='bpa_memberplanid' to='bpa_memberplanid' >
                      <link-entity name='bpa_sector' from='bpa_sectorid' to='bpa_planid' >
                        <attribute name='bpa_trust' />
                        <filter>
                          <condition attribute='bpa_trust' operator='eq' value='{trust.Id}' />
                        </filter>
                      </link-entity>
                    </link-entity>
                  </entity>
                </fetch>
            ";
            tracingService.Trace($"fetch Query:\n {fetchXml}");

            FetchXmlToQueryExpressionRequest conversionRequest = new FetchXmlToQueryExpressionRequest
            {
                FetchXml = fetchXml
            };

            FetchXmlToQueryExpressionResponse conversionResponse = (FetchXmlToQueryExpressionResponse)service.Execute(conversionRequest);

            // Use the newly converted query expression to make a retrieve multiple
            // request to Microsoft Dynamics CRM.
            QueryExpression queryServicios = conversionResponse.Query;
            int pageNumber = 1;
            RetrieveMultipleResponse multiResponse = new RetrieveMultipleResponse();
            tracingService.Trace($"Declared RetrieveMultipleResponse");


            //Makeing loop if record cound is more than 5000 and add into collection
            do
            {
                queryServicios.PageInfo.Count = 5000;
                queryServicios.PageInfo.PagingCookie = pageNumber == 1
                    ? null
                    : multiResponse.EntityCollection.PagingCookie;
                queryServicios.PageInfo.PageNumber = pageNumber++;

                RetrieveMultipleRequest multiRequest = new RetrieveMultipleRequest {Query = queryServicios};
                tracingService.Trace($"Make Request");
                multiResponse = (RetrieveMultipleResponse)service.Execute(multiRequest);

                tracingService.Trace($"Adding into EntityCollection");
                events.Entities.AddRange(multiResponse.EntityCollection.Entities);
                tracingService.Trace($"Added into EntityCollection");
            } while (multiResponse.EntityCollection.MoreRecords);

            tracingService.Trace($"Out side of the loop and before Associating/Updating Total Record Count: {events.Entities.Count.ToString()}");
            // Associate event to event report
            foreach (Entity e in events.Entities)
            {
                Entity activity = new Entity(PluginHelper.BpaEvent)
                {
                    ["bpa_eventreport"] = new EntityReference("bpa_eventreport", entity.Id),
                    Id = e.Id
                };
                service.Update(activity);
            }
        }
    }
}